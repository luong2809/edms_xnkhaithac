﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FilePermissionDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class FilePermissionDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FilePermissionDAO"/> class.
        /// </summary>
        public FilePermissionDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<FilePermission> GetIQueryable()
        {
            return EDMsDataContext.FilePermissions;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<FilePermission> GetAll()
        {
            return EDMsDataContext.FilePermissions.ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public FilePermission GetById(int id)
        {
            return this.EDMsDataContext.FilePermissions.FirstOrDefault(ob => ob.ID == id);
        }
       
        #endregion

        #region GET ADVANCE

        /// <summary>
        /// The get by role id.
        /// </summary>
        /// <param name="roleId">
        /// The role id.
        /// </param>
        /// <returns>
        /// The <see cref="FilePermission"/>.
        /// </returns>
        public List<FilePermission> GetByRoleId(int roleId)
        {
            return this.EDMsDataContext.FilePermissions.Where(t => t.ObjectId == roleId && t.TypeID == 1).ToList();
        }
        public List<FilePermission> GetByUserId(int UserId)
        {
            return this.EDMsDataContext.FilePermissions.Where(t => t.ObjectId == UserId && t.TypeID == 2).ToList();
        }
        public List<FilePermission> GetByUserId(int UserId, int categoryId)
        {
            return this.EDMsDataContext.FilePermissions.Where(t => t.ObjectId == UserId && t.CategoryId == categoryId && t.TypeID == 2).ToList();
        }
        public List<FilePermission> GetByRoleId(int roleId, int categoryId)
        {
            return this.EDMsDataContext.FilePermissions.Where(t => t.ObjectId == roleId && t.CategoryId == categoryId && t.TypeID == 1).ToList();
        }
        public List<FilePermission> GetByUserIdList(int userid, int categoryId, int DocumentID)
        {
            return this.EDMsDataContext.FilePermissions.Where(t => t.ObjectId == userid && t.TypeID == 2 && t.CategoryId == categoryId && t.DocumentID == DocumentID).ToList();
        }
        public FilePermission GetByUserI(int UserId, int DocumentID)
        {
            return this.EDMsDataContext.FilePermissions.FirstOrDefault(t=>t.ObjectId== UserId && t.DocumentID== DocumentID && t.TypeID == 2);
        }
        public List<FilePermission> GetByRoleIdList(int roleId, int categoryId, int DocumentID)
        {
            return this.EDMsDataContext.FilePermissions.Where(t => t.ObjectId == roleId && t.TypeID == 1 && t.CategoryId == categoryId && t.DocumentID == DocumentID).ToList();
        }
        public List<FilePermission> GetByUserIdList(int UserId, int categoryId, List<int> DocumentID)//typeId ==2 is group
        {
            return this.EDMsDataContext.FilePermissions.Where(t => t.ObjectId == UserId && t.TypeID == 2 && t.CategoryId == categoryId && DocumentID.Contains(t.DocumentID.GetValueOrDefault())).ToList();
        }
        public List<FilePermission> GetByRoleIdList(int roleId, int categoryId, List<int> DocumentID)//typeId ==1 is group
        {
            return this.EDMsDataContext.FilePermissions.Where(t => t.ObjectId == roleId && t.TypeID==1 && t.CategoryId == categoryId && DocumentID.Contains(t.DocumentID.GetValueOrDefault())).ToList();
        }
        public List<FilePermission> GetByObjectId(int objectId, int type)
        {
            return this.EDMsDataContext.FilePermissions.Where(t => t.ObjectId == objectId && t.TypeID == type).ToList();
        }
        public List<FilePermission> GetAllByDocument(int dociment, int type)
        {
            return this.EDMsDataContext.FilePermissions.Where(t => t.DocumentID == dociment && t.TypeID == type).ToList();
        }
        public List<FilePermission> GetAllByFolder(int DocumentID)
        {
            return this.EDMsDataContext.FilePermissions.Where(t => t.DocumentID == DocumentID).ToList();
        }

        public List<FilePermission> GetByRoleIdList(List<int> roleId, int documentID)
        {
            return this.EDMsDataContext.FilePermissions.Where(t => roleId.Contains(t.ObjectId.Value) && t.TypeID == 1 && t.DocumentID == documentID).ToList();
        }

        public List<FilePermission> GetByRoleIdListCategory(List<int> roleId, int category)
        {
            return this.EDMsDataContext.FilePermissions.Where(t => roleId.Contains(t.ObjectId.Value) && t.TypeID == 1 && t.CategoryId == category).ToList();
        }
        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// Deletes the permissions.
        /// </summary>
        /// <param name="permissions">The permissions.</param>
        /// <returns></returns>
        public bool DeleteFilePermission(List<FilePermission> FilePermission)
        {
            try
            {
                foreach (var item in FilePermission)
                {
                    EDMsDataContext.DeleteObject(item);
                    EDMsDataContext.SaveChanges();
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }


        /// <summary>
        /// Adds the permissions.
        /// </summary>
        /// <param name="permissions">The permissions.</param>
        /// <returns></returns>
        public bool AddFilePermissions(List<FilePermission> FilePermission)
        {
            try
            {
                foreach (var item in FilePermission)
                {
                    EDMsDataContext.AddToFilePermissions(item);
                    EDMsDataContext.SaveChanges();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(FilePermission ob)
        {
            try
            {
                EDMsDataContext.AddToFilePermissions(ob);
                EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(FilePermission src)
        {
            try
            {
                FilePermission des = (from rs in this.EDMsDataContext.FilePermissions
                                where rs.ID == src.ID
                                select rs).First();

                des.CategoryId = src.CategoryId;
                des.DocumentID = src.DocumentID;
                des.ObjectId = src.ObjectId;
                des.TypeID = src.TypeID;
                des.TypeName = src.TypeName;
                des.FullPermission = src.FullPermission;
                des.ChangePermission = src.ChangePermission;
                des.Create = src.Create;
                des.Delete = src.Delete;
                des.Read = src.Read;
                des.Write = src.Write;
                des.NoAccess = src.NoAccess;
                des.LastUpdatedDate = src.LastUpdatedDate;
                des.LastUpdatedBy = src.LastUpdatedBy;
                des.ObjectIdName = src.ObjectIdName;
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(FilePermission src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
