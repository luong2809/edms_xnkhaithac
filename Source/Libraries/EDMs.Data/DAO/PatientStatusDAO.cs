﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDMs.Data.Entities;

namespace EDMs.Data.DAO
{   
    public class PatientStatusDAO : BaseDAO
    {
        public PatientStatusDAO() : base() { }

        #region GET (Basic)
        public IQueryable<PatientStatus> GetIQueryable()
        {
            return EDMsDataContext.PatientStatuses;
        }
        
        public List<PatientStatus> GetAll()
        {
            return EDMsDataContext.PatientStatuses.ToList<PatientStatus>();
        }

        public PatientStatus GetByID(int ID)
        {
            return EDMsDataContext.PatientStatuses.Where(ob => ob.Id == ID).FirstOrDefault<PatientStatus>();
        }
       
        #endregion

        #region GET ADVANCE
        
        #endregion

        #region Insert, Update, Delete
        public bool Insert(PatientStatus ob)
        {
            try
            {
                EDMsDataContext.AddToPatientStatuses(ob);
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(PatientStatus ob)
        {
            try
            {
                //PatientStatus _ob = GetByID(ob.Id);
                //if (_ob != null)
                //{
                //    EDMsDataContext.ApplyCurrentValues(_ob.EntityKey.EntitySetName, ob);
                //    EDMsDataContext.SaveChanges();
                //    return true;
                //}
                //else
                //    return false;

                PatientStatus _ob;

                _ob = (from rs in EDMsDataContext.PatientStatuses
                       where rs.Id == ob.Id
                       select rs).First();

                //_ob.Id = ob.Id;
                _ob.Name = ob.Name;
                _ob.Description = ob.Description;
                _ob.LastUpdate = ob.LastUpdate;
                _ob.UpdateBy = ob.UpdateBy;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(PatientStatus ob)
        {
            try
            {
                PatientStatus _ob = GetByID(ob.Id);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                PatientStatus _ob = GetByID(ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
