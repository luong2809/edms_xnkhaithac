﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDMs.Data.Entities;

namespace EDMs.Data.DAO
{   
    public class PermissionDAO : BaseDAO
    {
        public PermissionDAO() : base() { }

        #region GET (Basic)
        public IQueryable<Permission> GetIQueryable()
        {
            return EDMsDataContext.Permissions;
        }

        public List<Permission> GetAll()
        {
            return EDMsDataContext.Permissions.ToList();
        }

        public Permission GetById(int id)
        {
            return EDMsDataContext.Permissions.FirstOrDefault(ob => ob.Id == id);
        }
       
        #endregion

        #region Get (Advances)

        public List<Permission> GetByRoleId(int roleId)
        {
            return EDMsDataContext.Permissions.Where(ob => ob.RoleId == roleId).ToList();
        }

        public List<Permission> GetByRoleId(int roleId, int specialParent)
        {
            return EDMsDataContext.Permissions.ToArray().Where(ob => ob.RoleId == roleId && 
                                                                    ob.Menu.ParentId == specialParent).ToList();
        }

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// Deletes the permissions.
        /// </summary>
        /// <param name="permissions">The permissions.</param>
        /// <returns></returns>
        public bool DeletePermissions(List<Permission> permissions)
        {
            try
            {
                foreach (var item in permissions)
                {
                    EDMsDataContext.DeleteObject(item);
                    EDMsDataContext.SaveChanges();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Adds the permissions.
        /// </summary>
        /// <param name="permissions">The permissions.</param>
        /// <returns></returns>
        public bool AddPermissions(List<Permission> permissions)
        {
            try
            {
                foreach (var item in permissions)
                {
                    EDMsDataContext.AddToPermissions(item);
                    EDMsDataContext.SaveChanges();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        #endregion

    }
}
