﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WaitingSyncDataDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class WaitingSyncDataDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WaitingSyncDataDAO"/> class.
        /// </summary>
        public WaitingSyncDataDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<WaitingSyncData> GetIQueryable()
        {
            return EDMsDataContext.WaitingSyncDatas;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<WaitingSyncData> GetAll()
        {
            return EDMsDataContext.WaitingSyncDatas.ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public WaitingSyncData GetById(int id)
        {
            return this.EDMsDataContext.WaitingSyncDatas.FirstOrDefault(ob => ob.ID == id);
        }
       
        #endregion

        #region GET ADVANCE

        #endregion

        #region Insert, Update, Delete
        public bool Update(WaitingSyncData src)
        {
            try
            {
                var des = (from rs in this.EDMsDataContext.WaitingSyncDatas
                                where rs.ID == src.ID
                                select rs).First();

                des.IsSynced = src.IsSynced;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
