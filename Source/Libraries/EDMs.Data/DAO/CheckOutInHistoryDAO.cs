﻿namespace EDMs.Data.DAO
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    public class CheckOutInHistoryDAO : BaseDAO
    {
        public CheckOutInHistoryDAO() : base() { }

        #region GET (Basic)
        public IQueryable<CheckOutInHistory> GetIQueryable()
        {
            return EDMsDataContext.CheckOutInHistories;
        }

        public List<CheckOutInHistory> GetAll()
        {
            return EDMsDataContext.CheckOutInHistories.ToList<CheckOutInHistory>();
        }

        public List<CheckOutInHistory> GetAllByDocId(int docId)
        {
            return
                EDMsDataContext.CheckOutInHistories.Where(t => t.DocumentId == docId).OrderByDescending(t => t.AtTime).
                    ToList();
        }


        public CheckOutInHistory GetByID(int ID)
        {
            return EDMsDataContext.CheckOutInHistories.Where(ob => ob.ID == ID).FirstOrDefault<CheckOutInHistory>();
        }

        #endregion

        #region GET ADVANCE

        #endregion

        #region Insert, Update, Delete
        public bool Insert(CheckOutInHistory ob)
        {
            try
            {
                EDMsDataContext.AddToCheckOutInHistories(ob);
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(CheckOutInHistory ob)
        {
            try
            {
                CheckOutInHistory _ob = GetByID(ob.ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                CheckOutInHistory _ob = GetByID(ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
