﻿namespace EDMs.Data.DAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    public class AppointmentDAO : BaseDAO
    {
        public AppointmentDAO() : base() { }

        #region GET (Basic)
        public IQueryable<Appointment> GetIQueryable()
        {
            return EDMsDataContext.Appointments;
        }
        
        public List<Appointment> GetAll()
        {
            return EDMsDataContext.Appointments.ToList<Appointment>();
        }

        public Appointment GetByID(int ID)
        {
            return EDMsDataContext.Appointments.Where(ob => ob.Id == ID).FirstOrDefault<Appointment>();
        }
       
        #endregion

        #region GET ADVANCE

        public List<Appointment> GetAppointment(DateTime fromDate, DateTime toDate)
        {
            return EDMsDataContext.Appointments.Where(obj => obj.AppointmentDate >= fromDate && obj.AppointmentDate <= toDate).ToList<Appointment>();
        }

        public string GetMaxCode()
        {
            return EDMsDataContext.Appointments.Max(obj => obj.Code);
        }

        /// <summary>
        /// The get child appointment by parent apppointment id.
        /// </summary>
        /// <param name="parentApppointmentId">
        /// The parent apppointment id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Appointment> GetChildAppointmentByParentApppointmentId(int parentApppointmentId)
        {
            return EDMsDataContext.Appointments.Where(ob => ob.ParentAppointmentId == parentApppointmentId).ToList<Appointment>();
        } 
        #endregion

        #region Insert, Update, Delete
        public bool Insert(Appointment ob)
        {
            try
            {
                EDMsDataContext.AddToAppointments(ob);
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(Appointment ob)
        {
            try
            {
                Appointment _ob;

                _ob = (from rs in EDMsDataContext.Appointments
                       where rs.Id == ob.Id
                       select rs).First();

                _ob.PatientId = ob.PatientId;

                _ob.AppointmentDate = ob.AppointmentDate;
                _ob.AppointmentDuration = ob.AppointmentDuration;
                _ob.AppointmentStatusId = ob.AppointmentStatusId;

                _ob.CancelBy = ob.CancelBy;
                _ob.CancelDate = ob.CancelDate;
                _ob.CancelTime = ob.CancelTime;

                _ob.Code = ob.Code;
                _ob.SetDisplayName = ob.SetDisplayName;
                _ob.LastCall = ob.LastCall;
                _ob.NextCall = ob.NextCall;
                _ob.NumberCall = ob.NumberCall;
               
                _ob.Start = ob.Start;
                _ob.End = ob.End;

                _ob.ResourceId = ob.ResourceId;
                _ob.ServiceTypeId = ob.ServiceTypeId;

                // add new
                _ob.Description = ob.Description;
                _ob.Reminder = ob.Reminder;
                _ob.ReminderId = ob.ReminderId;
                _ob.RecurrenceRuleText = ob.RecurrenceRuleText;
                _ob.RoomId = ob.RoomId;

                _ob.ResourceOthers = ob.ResourceOthers;

                _ob.LastUpdate = ob.LastUpdate;
                _ob.UpdateBy = ob.UpdateBy;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(Appointment ob)
        {
            try
            {
                Appointment _ob = GetByID(ob.Id);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                Appointment _ob = GetByID(ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
