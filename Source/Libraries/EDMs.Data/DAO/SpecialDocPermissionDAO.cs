﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SpecialDocPermissionDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class SpecialDocPermissionDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpecialDocPermissionDAO"/> class.
        /// </summary>
        public SpecialDocPermissionDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<SpecialDocPermission> GetIQueryable()
        {
            return EDMsDataContext.SpecialDocPermissions;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<SpecialDocPermission> GetAll()
        {
            return EDMsDataContext.SpecialDocPermissions.ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public SpecialDocPermission GetById(int id)
        {
            return this.EDMsDataContext.SpecialDocPermissions.FirstOrDefault(ob => ob.ID == id);
        }
       
        #endregion

        #region GET ADVANCE

        /// <summary>
        /// The get by role id.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>
        /// The <see cref="SpecialDocPermission"/>.
        /// </returns>
        public List<SpecialDocPermission> GetByUserId(int userId)
        {
            return this.EDMsDataContext.SpecialDocPermissions.Where(t => t.UserId == userId).ToList();
        }

        //public List<SpecialDocPermission> GetByRoleId(int roleId, string categoryId)
        //{
        //    return this.EDMsDataContext.SpecialDocPermissions.Where(t => t.RoleId == roleId && t.CategoryIdList == categoryId).ToList();
        //}

        public SpecialDocPermission GetByUserId(int userId, int folderId)
        {
            return this.EDMsDataContext.SpecialDocPermissions.FirstOrDefault(t => t.UserId == userId  && t.FolderId == folderId);
        }

        /// <summary>
        /// The get all by folder.
        /// </summary>
        /// <param name="folderId">
        /// The folder id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<SpecialDocPermission> GetAllByFolder(int folderId)
        {
            return this.EDMsDataContext.SpecialDocPermissions.Where(t => t.FolderId == folderId).ToList();
        }

        public List<SpecialDocPermission> GetAllByDocId(int docId)
        {
            return this.EDMsDataContext.SpecialDocPermissions.Where(t => t.DocId == docId).ToList();
        }

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// Deletes the permissions.
        /// </summary>
        /// <returns></returns>
        public bool DeleteSpecialDocPermission(List<SpecialDocPermission> SpecialDocPermission)
        {
            try
            {
                foreach (var item in SpecialDocPermission)
                {
                    EDMsDataContext.DeleteObject(item);
                    EDMsDataContext.SaveChanges();
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }


        /// <summary>
        /// Adds the permissions.
        /// </summary>
        /// <param name="permissions">The permissions.</param>
        /// <returns></returns>
        public bool AddSpecialDocPermissions(List<SpecialDocPermission> SpecialDocPermission)
        {
            try
            {
                foreach (var item in SpecialDocPermission)
                {
                    EDMsDataContext.AddToSpecialDocPermissions(item);
                    EDMsDataContext.SaveChanges();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public int? Insert(SpecialDocPermission ob)
        {
            try
            {
                EDMsDataContext.AddToSpecialDocPermissions(ob);
                EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(SpecialDocPermission src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    EDMsDataContext.DeleteObject(des);
                    EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
