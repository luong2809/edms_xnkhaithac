﻿using EDMs.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EDMs.Data.DAO
{
    public class Patient_UsedServiceDAO:BaseDAO
    {
        public Patient_UsedServiceDAO() : base() { }

        #region GET (Basic)
        public IQueryable<UsedService> GetIQueryable()
        {
            return EDMsDataContext.UsedServices;
        }
        
        public List<UsedService> GetAll()
        {
            return EDMsDataContext.UsedServices.ToList<UsedService>();
        }

        public UsedService GetByID(int ID)
        {
            return EDMsDataContext.UsedServices.Where(ob => ob.ID == ID).FirstOrDefault<UsedService>();
        }

       
        #endregion

        #region GET ADVANCE

        /// <summary>
        /// Find By Any expression
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public List<UsedService> Find(Expression<Func<UsedService, bool>> where)
        {
            return EDMsDataContext.UsedServices.Where(where).ToList<UsedService>();
        }
        #endregion

        #region Insert, Update, Delete
        public bool Insert(UsedService ob)
        {
            try
            {
                EDMsDataContext.AddToUsedServices(ob);
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(UsedService ob)
        {
            try
            {
                UsedService _ob;

                _ob = (from rs in EDMsDataContext.UsedServices
                       where rs.ID == ob.ID
                       select rs).First();
                
                _ob.ServiceTypeID = ob.ServiceTypeID;
                _ob.UserID = ob.UserID;
                _ob.CustomerID = ob.CustomerID;
                _ob.UsedDate = ob.UsedDate;
                _ob.NextDate = ob.NextDate;
                _ob.NumberOfTimes = ob.NumberOfTimes;
                _ob.ResourceID = ob.ResourceID;
                _ob.Content = ob.Content;
                _ob.Description = ob.Description;
                _ob.UpdatedDate = ob.UpdatedDate;
                _ob.UpdatedBy = ob.UpdatedBy;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(UsedService ob)
        {
            try
            {
                UsedService _ob = GetByID(ob.ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                UsedService _ob = GetByID(ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
