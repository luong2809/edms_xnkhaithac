﻿using EDMs.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EDMs.Data.DAO
{
    public class Patient_DescriptionDAO:BaseDAO
    {
        public Patient_DescriptionDAO() : base() { }

        #region GET (Basic)
        public IQueryable<Description> GetIQueryable()
        {
            return EDMsDataContext.Descriptions;
        }
        
        public List<Description> GetAll()
        {
            return EDMsDataContext.Descriptions.ToList<Description>();
        }

        public Description GetByID(int ID)
        {
            return EDMsDataContext.Descriptions.Where(ob => ob.ID == ID).FirstOrDefault<Description>();
        }

       
        #endregion

        #region GET ADVANCE

        /// <summary>
        /// Find By Any expression
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public List<Description> Find(Expression<Func<Description, bool>> where)
        {
            return EDMsDataContext.Descriptions.Where(where).ToList<Description>();
        }
        #endregion

        #region Insert, Update, Delete
        public bool Insert(Description ob)
        {
            try
            {
                EDMsDataContext.AddToDescriptions(ob);
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(Description ob)
        {
            try
            {
                Description _ob;

                _ob = (from rs in EDMsDataContext.Descriptions
                       where rs.ID == ob.ID
                       select rs).First();

                _ob.UserID = ob.UserID;
                _ob.CustomerID = ob.CustomerID;
                _ob.Content = ob.Content;
                _ob.UpdatedDate = ob.UpdatedDate;
                _ob.UpdatedBy = ob.UpdatedBy;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(Description ob)
        {
            try
            {
                Description _ob = GetByID(ob.ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                Description _ob = GetByID(ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
