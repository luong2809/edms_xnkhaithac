﻿using EDMs.Data.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDMs.Data.Entities
{
    public partial class Resource
    {
        public User User
        {
            get
            {
                var dao = new UserDAO();
                return dao.GetByResourceId(Id);
            }
        }
    }
}
