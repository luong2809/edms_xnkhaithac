﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Appointment.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the Appointment type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.Entities
{
    using System.Linq;

    using DAO;

    /// <summary>
    /// The appointment.
    /// </summary>
    public partial class Attention
    {
        /// <summary>
        /// Gets ResourceGroup.
        /// </summary>
        public ToList To
        {
            get
            {
                var tolistDAO = new ToListDAO();
                return tolistDAO.GetById(this.ToListID.GetValueOrDefault());
            }
        }
    }
}