﻿namespace EDMs.Data.Entities
{
    using EDMs.Data.DAO;

    public partial class ReceivedFrom
    {
        public Category Category
        {
            get
            {
                var dao = new CategoryDAO();
                return dao.GetById(this.CategoryId.GetValueOrDefault());
            }
        }
    }
}
