﻿using EDMs.Data.DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace EDMs.Data.Entities
{
    public partial class Patient
    {
        public PatientStatus PatientStatus
        {
            get
            {
                PatientStatusDAO dao = new PatientStatusDAO();
                return dao.GetByID(this.PatientStatusId.Value);
            }
        }

        public string ExtensionName
        {
            get { return string.Format("{0} - {1}", this.SSN, this.FullName); }
        }
    }
}
