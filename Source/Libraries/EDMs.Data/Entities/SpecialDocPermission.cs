﻿using EDMs.Data.DAO;

namespace EDMs.Data.Entities
{
    public partial class SpecialDocPermission
    {
        /// <summary>
        /// Gets the menu.
        /// </summary>
        /// <value>
        /// The menu.
        /// </value>
        public User UserObj
        {
            get
            {
                var dao = new UserDAO();
                return dao.GetByID(this.UserId.GetValueOrDefault());
            }
        }

    }
}
