﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDMs.SyncData.DAO;
using EDMs.SyncData.Entities;

namespace EDMs.SyncBusiness.Services
{
    public class UserService
    {      
        private readonly UserDAO repo;

        public UserService()
        {
            repo = new UserDAO();
        }
        #region GET (Basic)
        /// <summary>
        /// Get All User
        /// </summary>
        /// <returns></returns>
        public List<User> GetAll()
        {
            return repo.GetAll().ToList();
            //return patientDAO.GetAll();
        }

        /// <summary>
        /// Get User By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public User GetByID(int ID)
        {
            return repo.GetByID( ID);
            //return patientDAO.GetByID(ID);
        }
        #endregion

        #region Get (Advances)

        public List<User> GetAllByRoleId(int roleId)
        {
            return repo.GetAllByRoleId(roleId);
        }

        public User GetUserByUsername(string username)
        {
            return repo.GetUserByUsername(username);
        }

        public User GetByResourceId(int resourceId)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.ResourceId == resourceId);
        }

        public List<User> GetSpecialListUser(List<int> roleIds)
        {
            return this.repo.GetSpecialListUser(roleIds);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="newPassword">The new password.</param>
        /// <returns></returns>
        public bool ChangePassword(int userId, string newPassword)
        {
            return repo.ChangePassword(userId, newPassword);
        }

        /// <summary>
        /// Insert User
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Insert(User bo)
        {
            try
            {
                return repo.Insert(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Update User
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(User bo)
        {
            try
            {
                return repo.Update(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete User
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(User bo)
        {
            try
            {
                return repo.Delete(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete User By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                return repo.Delete(ID);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        /// <summary>
        /// Checks the exists.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="userName">Name of the user.</param>
        /// <returns></returns>
        public bool CheckExists(int? userId, string userName)
        {
            return repo.CheckExists(userId, userName);
        }
    }
}
