﻿namespace EDMs.SyncBusiness.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.SyncData.DAO;
    using EDMs.SyncData.Entities;

    public class CheckOutInHistoryService
    {       
         private readonly CheckOutInHistoryDAO repo;

        public CheckOutInHistoryService()
        {
            repo = new CheckOutInHistoryDAO();
        }
        #region GET (Basic)
        /// <summary>
        /// Get All CheckOutInHistory
        /// </summary>
        /// <returns></returns>
        public List<CheckOutInHistory> GetAll()
        {
            return repo.GetAll().ToList();
            //return patientDAO.GetAll();
        }

        /// <summary>
        /// Get CheckOutInHistory By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public CheckOutInHistory GetByID(int ID)
        {
            return repo.GetByID( ID);
            //return patientDAO.GetByID(ID);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert CheckOutInHistory
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Insert(CheckOutInHistory bo)
        {
            try
            {
                return repo.Insert(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<CheckOutInHistory> GetAllByDocId(int docId)
        {
            return this.repo.GetAllByDocId(docId);
        }

        /// <summary>
        /// Delete CheckOutInHistory
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(CheckOutInHistory bo)
        {
            try
            {
                return repo.Delete(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete CheckOutInHistory By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                return repo.Delete(ID);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
