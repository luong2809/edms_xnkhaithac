﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDMs.SyncData.DAO;
using EDMs.SyncData.Entities;

namespace EDMs.SyncBusiness.Services
{
    public class RoleService
    {      
        private readonly RoleDAO repo;

        public RoleService()
        {
            repo = new RoleDAO();
        }
        #region GET (Basic)
        /// <summary>
        /// Get All Role
        /// </summary>
        /// <returns></returns>
        public List<Role> GetAll()
        {
            return repo.GetAll().ToList();
            //return patientDAO.GetAll();
        }

        /// <summary>
        /// Get Role By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Role GetByID(int ID)
        {
            return repo.GetByID( ID);
            //return patientDAO.GetByID(ID);
        }
        #endregion

        #region Get (Advances)

        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Role
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Insert(Role bo)
        {
            try
            {
                return repo.Insert(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Update Role
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(Role bo)
        {
            try
            {
                return repo.Update(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Role
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(Role bo)
        {
            try
            {
                return repo.Delete(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Role By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                return repo.Delete(ID);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
