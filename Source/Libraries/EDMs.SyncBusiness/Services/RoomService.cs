﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDMs.SyncData.DAO;
using EDMs.SyncData.Entities;

namespace EDMs.SyncBusiness.Services
{
    public class RoomService
    {     
        private readonly RoomDAO repo;

        public RoomService()
        {
            repo = new RoomDAO();
        }
        #region GET (Basic)
        /// <summary>
        /// Get All Room
        /// </summary>
        /// <returns></returns>
        public List<Room> GetAll()
        {
            return repo.GetAll().ToList();
            //return patientDAO.GetAll();
        }

        /// <summary>
        /// Get Room By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Room GetByID(int ID)
        {
            return repo.GetByID(ID);
            //return patientDAO.GetByID(ID);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Room
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Insert(Room bo)
        {
            try
            {
                return repo.Insert(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Update Room
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(Room bo)
        {
            try
            {
                return repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Room
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(Room bo)
        {
            try
            {
                return repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Room By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                return repo.Delete(ID);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
