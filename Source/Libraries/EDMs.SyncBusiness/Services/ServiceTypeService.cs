﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDMs.SyncData.DAO;
using EDMs.SyncData.Entities;

namespace EDMs.SyncBusiness.Services
{
    public class ServiceTypeService
    {
        private readonly ServiceTypeDAO repo;

        public ServiceTypeService()
        {
            repo = new ServiceTypeDAO();
        }
        #region GET (Basic)
        /// <summary>
        /// Get All ServiceType
        /// </summary>
        /// <returns></returns>
        public List<ServiceType> GetAll()
        {
            return repo.GetAll().ToList();
            //return patientDAO.GetAll();
        }

        /// <summary>
        /// Get ServiceType By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public ServiceType GetByID(int ID)
        {
            return repo.GetByID( ID);
            //return patientDAO.GetByID(ID);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert ServiceType
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Insert(ServiceType bo)
        {
            try
            {
                return repo.Insert(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Update ServiceType
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(ServiceType bo)
        {
            try
            {
                return repo.Update(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete ServiceType
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(ServiceType bo)
        {
            try
            {
                return repo.Delete(bo);             
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete ServiceType By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                return repo.Delete(ID);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
