﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDMs.SyncData.DAO;
using EDMs.SyncData.Entities;

namespace EDMs.SyncBusiness.Services
{
    public class MenuService
    {      
        private readonly MenuDAO repo;

        public MenuService()
        {
            repo = new MenuDAO();
        }
        #region GET (Basic)
        /// <summary>
        /// Get All Role
        /// </summary>
        /// <returns></returns>
        public List<Menu> GetAll()
        {
            return repo.GetAll().ToList();
        }

        /// <summary>
        /// Get Role By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Menu GetByID(int ID)
        {
            return repo.GetByID( ID);
        }
        #endregion

        #region Get (Advances)

        /// <summary>
        /// Gets all menus by type.
        /// </summary>
        /// <param name="menuType">Type of the menu.</param>
        /// <returns></returns>
        public List<Menu> GetAllMenusByType(int menuType)
        {
            return repo.GetAllMenusByType(menuType);
        }

        /// <summary>
        /// Gets all related permitted menu items.
        /// </summary>
        /// <param name="roleId">The role id.</param>
        /// <param name="menuType">Type of the menu.</param>
        /// <returns></returns>
        public List<Menu> GetAllRelatedPermittedMenuItems(int roleId, int menuType)
        {
            return repo.GetAllRelatedPermittedMenuItems(roleId, menuType);
        }

        #endregion

        #region Insert, Update, Delete
        
        #endregion
    }
}
