﻿namespace EDMs.Business.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class DocPropertiesService
    {

        private List<Property> properties = new List<Property>()
            {
                new Property { ID = 1, Name = "Doc Number" },
                new Property { ID = 2, Name = "Title" },
                new Property { ID = 3, Name = "Revision" },
                new Property { ID = 4, Name = "Date" },
                new Property { ID = 5, Name = "From/To" },
                new Property { ID = 6, Name = "Discipline" },
                new Property { ID = 7, Name = "Document Type" },
                new Property { ID = 8, Name = "Platform" },
                new Property { ID = 9, Name = "Remark" },
                new Property { ID=10 , Name="Contractor"},
                new Property { ID=11 , Name="Tag"},
            };

        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<Property> GetAll()
        {
            return this.properties;
        }
    }

    /// <summary>
    /// The property.
    /// </summary>
    public class Property
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }
    }
}
