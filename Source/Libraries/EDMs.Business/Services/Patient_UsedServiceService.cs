﻿using EDMs.Data.DAO;
using EDMs.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EDMs.Business.Services
{
    public class Patient_UsedServiceService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly Patient_UsedServiceDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="PatientService"/> class.
        /// </summary>
        public Patient_UsedServiceService()
        {
            this.repo = new Patient_UsedServiceDAO();
        }

        #region Find (Advances)
        /// <summary>
        /// Find UsedService with expression
        /// </summary>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public List<UsedService> Find(Expression<Func<UsedService, bool>> where)
        {
            return repo.Find(where);
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Patient
        /// </summary>
        /// <returns></returns>
        public List<UsedService> GetAll()
        {
            return repo.GetAll();
        }

        /// <summary>
        /// Get Patient By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public UsedService GetByID(int ID)
        {
            return repo.GetByID(ID);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Patient
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Insert(UsedService bo)
        {
            try
            {
                return repo.Insert(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Update Patient
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(UsedService bo)
        {
            try
            {
                return repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Patient
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(UsedService bo)
        {
            try
            {
                return repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Patient By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                return repo.Delete(ID);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
