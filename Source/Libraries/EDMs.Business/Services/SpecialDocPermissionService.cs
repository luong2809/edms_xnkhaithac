﻿namespace EDMs.Business.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class SpecialDocPermissionService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly SpecialDocPermissionDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecialDocPermissionService"/> class.
        /// </summary>
        public SpecialDocPermissionService()
        {
            this.repo = new SpecialDocPermissionDAO();
        }


        public bool DeleteSpecialDocPermission(List<SpecialDocPermission> SpecialDocPermission)
        {
            return this.repo.DeleteSpecialDocPermission(SpecialDocPermission);
        }

        public bool AddSpecialDocPermissions(List<SpecialDocPermission> SpecialDocPermission)
        {
            return this.repo.AddSpecialDocPermissions(SpecialDocPermission);
        }
        #region Get (Advances)
        //public List<SpecialDocPermission> GetByRoleId(int roleId, string categoryId)
        //{
        //    return this.repo.GetByRoleId(roleId, categoryId);
        //}
        public SpecialDocPermission GetByUserId(int userId, int folderId)
        {
            return this.repo.GetByUserId(userId, folderId);
        }

        /// <summary>
        /// The get all by folder.
        /// </summary>
        /// <param name="folderId">
        /// The folder id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<SpecialDocPermission> GetAllByFolder(int folderId)
        {
            return this.repo.GetAllByFolder(folderId);
        }

        public List<SpecialDocPermission> GetAllByDocId(int docId)
        {
            return this.repo.GetAllByDocId(docId);
        }

        public List<SpecialDocPermission> GetAllByUser(int userId, List<int> folderIds)
        {
            return this.repo.GetAll().Where(t => t.UserId == userId &&
                folderIds.Contains(t.FolderId.GetValueOrDefault())).ToList();
        }

        public List<SpecialDocPermission> GetAllByUser(int userId)
        {
            return this.repo.GetAll().Where(t => t.UserId == userId).ToList();
        }

        public List<SpecialDocPermission> GetAllNotByUser(int userId)
        {
            return this.repo.GetAll().Where(t => t.UserId != userId).ToList();
        }

        //public List<int> GetAllGroupId(string folder)
        //{
        //    return this.repo.GetAllByFolder(folder).Select(t => t.RoleId.GetValueOrDefault()).Distinct().ToList();
        //}

        /// <summary>
        /// The get by role id.
        /// </summary>
        /// <param name="roleId">
        /// The role id.
        /// </param>
        /// <returns>
        /// The <see cref="SpecialDocPermission"/>.
        /// </returns>
        public List<SpecialDocPermission> GetByUserId(int userId)
        {
            return this.repo.GetByUserId(userId);
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<SpecialDocPermission> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public SpecialDocPermission GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(SpecialDocPermission bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(SpecialDocPermission bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

    }
}
