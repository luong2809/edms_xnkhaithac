﻿namespace EDMs.Business.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class DocumentService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly DocumentDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentService"/> class.
        /// </summary>
        public DocumentService()
        {
            this.repo = new DocumentDAO();
        }

        public List<Document> GetAllCheckOutDocument()
        {
            return this.repo.GetAllCheckOutDocument();
        }

        public List<Document> GetAllCheckoutDocumentByRold(int roleId)
        {
            return this.repo.GetAllCheckoutDocumentByRold(roleId);
        }

        public List<Document> GetTop100New(List<int> folderIds)
        {
            return this.repo.GetTop100New(folderIds.Take(500).ToList());
        }

        public List<Document> GetTop100New()
        {
            return this.repo.GetTop100New();
        }
        #region Get (Advances)

        /// <summary>
        /// The get specific document.
        /// </summary>
        /// <param name="listDocId">
        /// The list doc id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Document> GetSpecificDocument(List<int> listDocId)
        {
            return this.repo.GetSpecificDocument(listDocId);
        }

        /// <summary>
        /// The get specific document.
        /// </summary>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        /// <param name="dirName">
        /// The dir name.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Document> GetSpecificDocument(string fileName, string dirName)
        {
            return this.repo.GetSpecificDocument(fileName, dirName);
        }

        public Document GetFilter(string docNo, string transNo, int revId)
        {
            return this.repo.GetAll().FirstOrDefault(t =>
                !t.IsDelete.GetValueOrDefault()
                && t.DocumentNumber == docNo
                && t.RevisionID == revId
                && !string.IsNullOrEmpty(t.TransmittalNumber)
                && t.TransmittalNumber.Trim() == transNo);
        }

        /// <summary>
        /// The get all by folder.
        /// </summary>
        /// <param name="folderId">
        /// The folder id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>. Document
        /// </returns>
        public List<Document> GetAllByFolder(int folderId)
        {
            return this.repo.GetAllByFolder(folderId);
        }

        public List<Document> GetAllDeleteByFolder(int folderId)
        {
            return this.repo.GetAllDeleteByFolder(folderId);
        }

        public List<Document> GetAllByCategory(int categoryId)
        {
            return this.repo.GetAllByCategory(categoryId);
        }

        public List<Document> GetAllByCategory(List<Category> listCategory)
        {
            if (listCategory.Count == 0)
            {
                return this.repo.GetAll().Where(t => t.IsLeaf == true && t.IsDelete == false).ToList();
            }

            return this.repo.GetAll().Where(t => t.IsLeaf == true && t.IsDelete == false && listCategory.Any(x => x.ID == t.CategoryID)).ToList();
        }

        public List<Document> GetAllByFolder(List<int> folderIds)
        {
            return this.repo.GetAllByFolder(folderIds);
        }

        public List<Document> GetAllDelete()
        {
            return this.repo.GetAllDelete();
        }

        public List<Document> GetAllByFolderForMoveFolder(List<int> folderIds)
        {
            return this.repo.GetAllByFolderForMoveFolder(folderIds);
        }

        /// <summary>
        /// The get all relate document.
        /// </summary>
        /// <param name="docId">
        /// The doc id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Document> GetAllRelateDocument(int docId)
        {
            return this.repo.GetAllRelateDocument(docId);
        }


        public bool GetExistAnotherDocUseFile(string filePath, int docId)
        {
            return this.repo.GetExistAnotherDocUseFile(filePath, docId);
        }
        /// <summary>
        /// The get all doc revision.
        /// </summary>
        /// <param name="parentId">
        /// The parent id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Document> GetAllDocRevision(int parentId)
        {
            return this.repo.GetAllDocRevision(parentId);
        }

        /// <summary>
        /// The quick search.
        /// </summary>
        /// <param name="keyword">
        /// The keyword.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Document> QuickSearch(string keyword, int folId)
        {
            return this.repo.QuickSearch(keyword, folId);
        }

        public List<Document> AdvanceSearch(int categoryId, List<string> docName)
        {
            return this.repo.AdvanceSearch(categoryId, docName);
        }

        public List<Document> SearchDocument(List<int> folderPermission, string name, string docNumber, string title, string rev, string fromto, string discipline, string doctype, string platform, string remark, string contractor, string tag)
        {
            return this.repo.SearchDocument(folderPermission, name, docNumber, title, rev, fromto, discipline, doctype, platform, remark, contractor, tag);
        }

        public List<Document> SearchDocument(List<int> folderPermission, string searchAll)
        {
            return this.repo.SearchDocument(
                folderPermission,
                searchAll.Split(' ').ToArray());
        }

        public List<Document> SearchDocumentAll(int category, string name, string docNumber, string title, string rev, string fromto, string discipline, string doctype, string platform, string remark, string contractor, string tag)
        {
            return this.repo.SearchDocumentAll(category, name, docNumber, title, rev, fromto, discipline, doctype, platform, remark, contractor, tag);
        }

        public List<Document> SearchDocumentAll(int category, string searchAll)
        {
            return this.repo.SearchDocumentAll(
                category,
                searchAll.Split(' ').ToArray());
        }

        /// <summary>
        /// The get all doc version.
        /// </summary>
        /// <param name="docId">
        /// The doc id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Document> GetAllDocVersion(int docId)
        {
            return this.repo.GetAllDocVersion(docId);

        }

        /// <summary>
        /// The is document exist.
        /// </summary>
        /// <param name="folderId">
        /// The folder id.
        /// </param>
        /// <param name="documentName">
        /// The document name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsDocumentExist(int folderId, string documentName)
        {
            return this.repo.IsDocumentExist(folderId, documentName);
        }

        /// <summary>
        /// The is document exist update.
        /// </summary>
        /// <param name="folderId">
        /// The folder id.
        /// </param>
        /// <param name="docName">
        /// The doc name.
        /// </param>
        /// <param name="docId">
        /// The doc id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsDocumentExistUpdate(int folderId, string docName, int docId)
        {
            return this.repo.IsDocumentExistUpdate(folderId, docName, docId);
        }

        /// <summary>
        /// The get specific document.
        /// </summary>
        /// <param name="folderId">
        /// The folder id.
        /// </param>
        /// <param name="documentName">
        /// The document name.
        /// </param>
        /// <returns>
        /// The <see cref="Document"/>.
        /// </returns>
        public Document GetSpecificDocument(int folderId, string documentName)
        {
            return this.repo.GetSpecificDocument(folderId, documentName);
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<Document> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public Document GetById(int id)
        {
            return this.repo.GetById(id);
        }

        public Document GetByFileName(string fileName)
        {
            return this.repo.GetByFileName(fileName);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(Document bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(Document bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(Document bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

    }
}
