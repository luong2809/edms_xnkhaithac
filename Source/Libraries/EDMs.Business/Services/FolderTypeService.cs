﻿namespace EDMs.Business.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class FolderTypeService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly FolderTypeDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="FolderTypeService"/> class.
        /// </summary>
        public FolderTypeService()
        {
            this.repo = new FolderTypeDAO();
        }

        #region Get (Advances)
        ////public List<FolderType> GetAllWithoutDeparment()
        ////{
        ////    return this.repo.GetAll().Where(t => t.RoleId == 0 || t.RoleId == null).ToList();
        ////}

        ////public List<FolderType> GetAllByDeparment(int deparmentId)
        ////{
        ////    return this.repo.GetAll().Where(t => t.RoleId == deparmentId).ToList();
        ////}
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<FolderType> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        public FolderType GetByFolder(int folderId)
        {
            return
                this.repo.GetByFolder(folderId);
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public FolderType GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(FolderType bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(FolderType bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(FolderType bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
