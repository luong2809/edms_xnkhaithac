﻿using EDMs.Data.DAO;
using EDMs.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EDMs.Business.Services
{
    public class Patient_ContactService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly Patient_ContactDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="PatientService"/> class.
        /// </summary>
        public Patient_ContactService()
        {
            this.repo = new Patient_ContactDAO();
        }

        #region Find (Advances)
        /// <summary>
        /// Find Contact with expression
        /// </summary>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public List<Contact> Find(Expression<Func<Contact, bool>> where)
        {
            return repo.Find(where);
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Patient
        /// </summary>
        /// <returns></returns>
        public List<Contact> GetAll()
        {
            return repo.GetAll();
        }

        /// <summary>
        /// Get Patient By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Contact GetByID(int ID)
        {
            return repo.GetByID(ID);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Patient
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Insert(Contact bo)
        {
            try
            {
                return repo.Insert(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Update Patient
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(Contact bo)
        {
            try
            {
                return repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Patient
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(Contact bo)
        {
            try
            {
                return repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Patient By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                return repo.Delete(ID);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
