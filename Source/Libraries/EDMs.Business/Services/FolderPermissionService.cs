﻿
namespace EDMs.Business.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO;
    using EDMs.Data.Entities;
   public class FolderPermissionService
    {/// <summary>
     /// The repo.
     /// </summary>
        private readonly FolderPermissionDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="FolderPermissionService"/> class.
        /// </summary>
        public FolderPermissionService()
        {
            this.repo = new FolderPermissionDAO();
        }
        public List<FolderPermission> GetAllByFolder(int FolderId)
        {
            return this.repo.GetAllByFolder(FolderId);
        }
        public List<FolderPermission> GetAllByFolder( List<int> FolderId)
        {
            return this.repo.GetAllByFolder(FolderId);
        }
      
        public List<FolderPermission> GetByObjectId(int objectId, int type)
        {
            return this.repo.GetByObjectId(objectId, type);
        }
        public List<FolderPermission> GetAllByFolder(int FolderId, int type)
        {
            return this.repo.GetAllByFolder(FolderId, type);
        }
        /// <summary>
        /// The get by role id.
        /// </summary>
        /// <param name="roleId">
        /// The role id.
        /// </param>
        /// <returns>
        /// The <see cref="FolderPermission"/>.
        /// </returns>
        public List<FolderPermission> GetByRoleId(int roleId)
        {
            return this.repo.GetByRoleId(roleId);
        }
        public List<FolderPermission> GetFolderByRoleIdList(int category, List<int> grouplist)
        {
            return this.repo.GetFolderByRoleIdList(category, grouplist);
        }
        public List<FolderPermission> GetFolderByRoleIdList(List<int> grouplist)
        {
            return this.repo.GetFolderByRoleIdList(grouplist);
        }
        public bool AddGroupDataPermissions(List<FolderPermission> groupDataPermission)
        {
            return this.repo.AddGroupDataPermissions(groupDataPermission);
        }
        public bool DeletePermission(List<FolderPermission> groupDataPermission)
        {
            return this.repo.DeleteFolderPermission(groupDataPermission);
        }
        public List<FolderPermission> GetCategoryByUsser(int userlist)
        {
            return this.repo.GetCategoryByUsser(userlist);
        }
        public List<FolderPermission> GetCategory(List<int> roleId)
        {
            return this.repo.GetCategory(roleId);
        }
        public List<FolderPermission> GetByRoleId(int categoryId,int roleId)
        {
            return this.repo.GetByRoleId(categoryId,roleId);
        }
        public List<FolderPermission> GetByUserId(int userid)
        {
            return this.repo.GetByUserId(userid);
        }
        public List<FolderPermission> GetByUserId(int userid, int categoryid)
        {
            return this.repo.GetByUserId(userid, categoryid);
        }
        public List<FolderPermission> GetByRoleIdList(int roleId, int categoryId, int FolderId)
        {
            return this.repo.GetByRoleIdList(roleId, categoryId, FolderId);
        }
        public List<FolderPermission> GetByRoleIdList(List<int> roleId, int FolderId)
        {
            return this.repo.GetByRoleIdList(roleId, FolderId);
        }
        public List<FolderPermission> GetByUserIdList(int UserId, int categoryId, int FolderId)
        {
            return this.repo.GetByUserIdList(UserId, categoryId, FolderId);
        }
        public FolderPermission GetByFolderUserId(int UserId, int FolderId)
        {
            return this.repo.GetByUserIdList(UserId, FolderId);
        }
        public List<FolderPermission> GetByUserIdList(int UserId, int categoryId, List<int> FolderId)//typeId ==2 is group
        {
            return this.repo.GetByUserIdList(UserId, categoryId, FolderId);
        }
        public List<FolderPermission> GetByRoleIdList(int roleId, int categoryId, List<int> FolderId)//typeId ==1 is group
        {
            return this.repo.GetByRoleIdList(roleId, categoryId, FolderId);
        }

        #region GET (Basic)
        /// <summary>
        /// Get All Categories GetByUserId
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<FolderPermission> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public FolderPermission GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(FolderPermission bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(FolderPermission bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(FolderPermission bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
