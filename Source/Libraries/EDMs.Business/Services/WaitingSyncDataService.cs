﻿using System;

namespace EDMs.Business.Services
{
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class WaitingSyncDataService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly WaitingSyncDataDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="WaitingSyncDataService"/> class.
        /// </summary>
        public WaitingSyncDataService()
        {
            this.repo = new WaitingSyncDataDAO();
        }

        #region Get (Advances)
        public List<WaitingSyncData> GetAllUnSync()
        {
            return this.repo.GetAll().Where(t => !t.IsSynced.GetValueOrDefault()).ToList();
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<WaitingSyncData> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public WaitingSyncData GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        public bool Update(WaitingSyncData bo)
        {
            try
            {
                return repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

    }
}
