﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDMs.Data.DAO;
using EDMs.Data.Entities;

namespace EDMs.Business.Services
{
    public class PermissionService
    {      
        private readonly PermissionDAO repo;

        public PermissionService()
        {
            repo = new PermissionDAO();
        }
        #region GET (Basic)
        /// <summary>
        /// Get All Role
        /// </summary>
        /// <returns></returns>
        public List<Permission> GetAll()
        {
            return repo.GetAll().ToList();
            //return patientDAO.GetAll();
        }

        /// <summary>
        /// Get Role By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Permission GetByID(int id)
        {
            return repo.GetById(id);
            //return patientDAO.GetByID(ID);
        }
        #endregion

        #region Get (Advances)

        public List<Permission> GetByRoleId(int roleId)
        {
            return repo.GetByRoleId(roleId);
        }

        public List<Permission> GetByRoleId(int roleId, int specialParent)
        {
            return this.repo.GetByRoleId(roleId, specialParent);
        }

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// Deletes the permissions.
        /// </summary>
        /// <param name="permissions">The permissions.</param>
        /// <returns></returns>
        public bool DeletePermissions(List<Permission> permissions)
        {
            return repo.DeletePermissions(permissions);
        }

        /// <summary>
        /// Adds the permissions.
        /// </summary>
        /// <param name="permissions">The permissions.</param>
        /// <returns></returns>
        public bool AddPermissions(List<Permission> permissions)
        {
            return repo.AddPermissions(permissions);
        }

        #endregion
        
    }
}
