﻿namespace EDMs.Business.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO;
    using EDMs.Data.Entities;

    public class AppointmentService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly AppointmentDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="AppointmentService"/> class.
        /// </summary>
        public AppointmentService()
        {
            this.repo = new AppointmentDAO();
        }

        #region Get (Advances)

        /// <summary>
        /// The get appointment by day.
        /// </summary>
        /// <param name="dateTime">
        /// The date time.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Appointment> GetAppointmentByDay(DateTime dateTime)
        {
            return this.GetAppointment(dateTime, dateTime);
        }

        /// <summary>
        /// The get appointment by week.
        /// </summary>
        /// <param name="dateTime">
        /// The date time.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Appointment> GetAppointmentByWeek(DateTime dateTime)
        {
            return this.GetAppointment(this.GetFirstDayOfWeek(dateTime), this.GetLastDayOfWeek(dateTime));
        }

        /// <summary>
        /// The get appointment by month.
        /// </summary>
        /// <param name="dateTime">
        /// The date time.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Appointment> GetAppointmentByMonth(DateTime dateTime)
        {
            var fromDate = new DateTime(dateTime.Year, dateTime.Month, 1);
            var toDate = new DateTime(dateTime.Year, dateTime.Month, DateTime.DaysInMonth(dateTime.Year, dateTime.Month));
            return this.GetAppointment(fromDate, toDate);
        }

        /// <summary>
        /// The get appointment.
        /// </summary>
        /// <param name="fromDate">
        /// The from date.
        /// </param>
        /// <param name="toDate">
        /// The to date.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        private List<Appointment> GetAppointment(DateTime fromDate,DateTime toDate)
        {
            fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);
            toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day,23,59, 59);
            return this.repo.GetAppointment(fromDate,toDate);
        }

        /// <summary>
        /// The get max code.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetMaxCode()
        {
            return this.repo.GetMaxCode();
        }

        /// <summary>
        /// The get child appointments by parent apppointment id.
        /// </summary>
        /// <param name="parentApppointmentId">
        /// The parent apppointment id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Appointment> GetChildAppointmentByParentApppointmentId(int parentApppointmentId)
        {
            return this.repo.GetChildAppointmentByParentApppointmentId(parentApppointmentId);
        }
        
        /// <summary>
        /// Returns the first day of the week that the specified date
        /// is in.
        /// </summary>
        /// <param name="dayInWeek">
        /// The day In Week.
        /// </param>
        /// <returns>
        /// The <see cref="DateTime"/>.
        /// </returns>
        private DateTime GetFirstDayOfWeek(DateTime dayInWeek)
        {
            var firstDay = DayOfWeek.Monday;
            DateTime firstDayInWeek = dayInWeek.Date;
            while (firstDayInWeek.DayOfWeek != firstDay)
                firstDayInWeek = firstDayInWeek.AddDays(-1); 
            return firstDayInWeek;
        }

        /// <summary>
        /// Returns the first day of the week that the specified date
        /// is in.
        /// </summary>
        /// <param name="dayInWeek">
        /// The day In Week.
        /// </param>
        /// <returns>
        /// The <see cref="DateTime"/>.
        /// </returns>
        private DateTime GetLastDayOfWeek(DateTime dayInWeek)
        {
            DayOfWeek lastDay = DayOfWeek.Sunday;
            DateTime lastDayInWeek = dayInWeek.Date;
            while (lastDayInWeek.DayOfWeek != lastDay)
                lastDayInWeek = lastDayInWeek.AddDays(1);
            return lastDayInWeek;
        }

        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Appointment
        /// </summary>
        /// <returns></returns>
        public List<Appointment> GetAll()
        {
            return this.repo.GetAll().ToList();
            //return patientDAO.GetAll();
        }

        /// <summary>
        /// Get Appointment By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Appointment GetByID(int ID)
        {
            return this.repo.GetByID(ID);
            //return patientDAO.GetByID(ID);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Appointment
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Insert(Appointment bo)
        {
            try
            {
                return this.repo.Insert(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Update Appointment
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(Appointment bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Appointment
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(Appointment bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Appointment By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                return this.repo.Delete(ID);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}
