﻿using EDMs.SyncData.DAO;

namespace EDMs.SyncData.Entities
{
    public partial class SpecialDocPermission
    {
        /// <summary>
        /// Gets the menu.
        /// </summary>
        /// <value>
        /// The menu.
        /// </value>
        public User UserObj
        {
            get
            {
                var dao = new UserDAO();
                return dao.GetByID(this.UserId.GetValueOrDefault());
            }
        }

    }
}
