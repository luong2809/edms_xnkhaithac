﻿namespace EDMs.SyncData.Entities
{
    using EDMs.SyncData.DAO;

    public partial class CheckOutInHistory
    {
        public string FullUserName
        {
            get
            {
                var dao = new UserDAO();
                var userObj = dao.GetByID(this.ByUserId.GetValueOrDefault());
                return userObj != null ? userObj.FullName : string.Empty;
            }
        }

        public DCR DCRObj
        {
            get
            {
                var dao = new DCRDAO();
                var dcrObj = dao.GetById(this.DCRId.GetValueOrDefault());
                return dcrObj;

            }
        }



        public string GroupName
        {
            get
            {
                var dao = new RoleDAO();
                var roleObj = dao.GetByID(this.ByRoleId.GetValueOrDefault());
                return roleObj != null ? roleObj.Name : string.Empty;

            }
        }
    }
}
