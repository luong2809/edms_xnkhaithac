﻿using EDMs.SyncData.DAO;

namespace EDMs.SyncData.Entities
{
    public partial class User
    {
        /// <summary>
        /// Gets the role.
        /// </summary>
        /// <value>
        /// The role.
        /// </value>
        public Role Role
        {
            get
            {
                var dao = new RoleDAO();
                return RoleId != null ? dao.GetByID(RoleId.Value) : null;
            }
        }

        /// <summary>
        /// Gets the resource.
        /// </summary>
        /// <value>
        /// The resource.
        /// </value>
        public Resource Resource
        {
            get
            {
                var dao = new ResourceDAO();
                return ResourceId != null ? dao.GetByID(ResourceId.Value) : null;
            }
        }

        public string UserNameFullName
        {
            get
            {
                if (string.IsNullOrEmpty(this.FullName))
                {
                    return this.Username;
                }

                return this.Username + " - " + this.FullName;
            }
        }
    }
}
