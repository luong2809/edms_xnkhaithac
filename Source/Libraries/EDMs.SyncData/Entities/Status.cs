﻿using EDMs.SyncData.DAO;

namespace EDMs.SyncData.Entities
{
    public partial class Status
    {
        public Category Category
        {
            get
            {
                var dao = new CategoryDAO();
                return dao.GetById(this.CategoryId.GetValueOrDefault());
            }
        }
    }
}
