﻿namespace EDMs.SyncData.Entities
{
    /// <summary>
    /// The role.
    /// </summary>
    public partial class Role
    {
        private int parentId;

        public int ParentId
        {
            get
            {
                return this.parentId;
            }
            set
            {
                this.parentId = value;
            }
        }
    }
}
