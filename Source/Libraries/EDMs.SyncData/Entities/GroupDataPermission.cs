﻿namespace EDMs.SyncData.Entities
{
    using EDMs.SyncData.DAO;

    /// <summary>
    /// The role.
    /// </summary>
    public partial class GroupDataPermission
    {
        /// <summary>
        /// Gets the group name.
        /// </summary>
        public string GroupName
        {
            get
            {
                var roledDao = new RoleDAO();
                var roleObj = roledDao.GetByID(this.RoleId.GetValueOrDefault());
                return roleObj != null ? roleObj.Name : string.Empty;
            }
        }
    }
}
