﻿using EDMs.SyncData.DAO;

namespace EDMs.SyncData.Entities
{
    public partial class Resource
    {
        public ResourceGroup ResourceGroup {
            get 
            { 
                var dao = new ResourceGroupDAO();
                return dao.GetByID(this.ResourceGroupId.GetValueOrDefault());
            }
        }

        public User User
        {
            get
            {
                var dao = new UserDAO();
                return dao.GetByResourceId(Id);
            }
        }
    }
}
