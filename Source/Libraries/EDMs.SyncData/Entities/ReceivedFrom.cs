﻿using EDMs.SyncData.DAO;

namespace EDMs.SyncData.Entities
{
    public partial class ReceivedFrom
    {
        public Category Category
        {
            get
            {
                var dao = new CategoryDAO();
                return dao.GetById(this.CategoryId.GetValueOrDefault());
            }
        }
    }
}
