﻿using EDMs.SyncData.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDMs.SyncData.Entities
{
    public partial class NotificationRule
    {
        /// <summary>
        /// Gets the role.
        /// </summary>
        /// <value>
        /// The role.
        /// </value>
        public Discipline Discipline
        {
            get
            {
                var dao = new DisciplineDAO();
                return dao.GetById(this.DisciplineID.GetValueOrDefault());
            }
        }
    }
}
