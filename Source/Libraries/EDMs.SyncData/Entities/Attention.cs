﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Appointment.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the Appointment type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using EDMs.SyncData.DAO;
using EDMs.SyncData.DAO;

namespace EDMs.SyncData.Entities
{
    /// <summary>
    /// The appointment.
    /// </summary>
    public partial class Attention
    {
        /// <summary>
        /// Gets ResourceGroup.
        /// </summary>
        public ToList To
        {
            get
            {
                var tolistDAO = new ToListDAO();
                return tolistDAO.GetById(this.ToListID.GetValueOrDefault());
            }
        }
    }
}