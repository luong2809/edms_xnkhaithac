﻿namespace EDMs.SyncData.Entities
{
    using EDMs.SyncData.DAO;

    public partial class DocumentType
    {
        public Category Category
        {
            get
            {
                var dao = new CategoryDAO();
                return dao.GetById(this.CategoryId.GetValueOrDefault());
            }
        }
    }
}
