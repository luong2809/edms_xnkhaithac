﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDMs.SyncData.Entities;

namespace EDMs.SyncData.DAO
{   
    public class EmployeeDAO : BaseDAO
    {
        public EmployeeDAO() : base() { }

        #region GET (Basic)
        public IQueryable<Employee> GetIQueryable()
        {
            return EDMsDataContext.Employees;
        }
        
        public List<Employee> GetAll()
        {
            return EDMsDataContext.Employees.ToList<Employee>();
        }

        public Employee GetByID(int ID)
        {
            return EDMsDataContext.Employees.Where(ob => ob.Id == ID).FirstOrDefault<Employee>();
        }
       
        #endregion

        #region GET ADVANCE
        
        #endregion

        #region Insert, Update, Delete
        public bool Insert(Employee ob)
        {
            try
            {
                EDMsDataContext.AddToEmployees(ob);
                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(Employee ob)
        {
            try
            {
                //Employee _ob = GetByID(ob.Id);
                //if (_ob != null)
                //{
                //    EDMsDataContext.ApplyCurrentValues(_ob.EntityKey.EntitySetName, ob);
                //    EDMsDataContext.SaveChanges();
                //    return true;
                //}
                //else
                //    return false;

                Employee _ob;

                _ob = (from rs in EDMsDataContext.Employees
                       where rs.Id == ob.Id
                       select rs).First();

               // _ob.Id = ob.Id;
                _ob.Address1 = ob.Address1;
                _ob.Address2 = ob.Address2;
                _ob.CellPhone = ob.CellPhone;
                _ob.City = ob.City;
                _ob.DateOfBirth = ob.DateOfBirth;
                _ob.Email = ob.Email;
                _ob.FirstName = ob.FirstName;
                _ob.Generation = ob.Generation;
                _ob.HomePhone = ob.HomePhone;
                _ob.LastName = ob.LastName;
                _ob.MailLabel = ob.MailLabel;
                _ob.MarialStatus = ob.MarialStatus;
                _ob.MiddleName = ob.MiddleName;
                _ob.Sex = ob.Sex;
                _ob.SSN = ob.SSN;
                _ob.StateCode = ob.StateCode;
                _ob.StateName = ob.StateName;
                _ob.WorkPhone = ob.WorkPhone;
                _ob.ZipCode = ob.ZipCode;

                _ob.LastUpdate = ob.LastUpdate;
                _ob.UpdateBy = ob.UpdateBy;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(Employee ob)
        {
            try
            {
                Employee _ob = GetByID(ob.Id);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                Employee _ob = GetByID(ID);
                if (_ob != null)
                {
                    EDMsDataContext.DeleteObject(_ob);
                    EDMsDataContext.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
