﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDMs.SyncData.Entities;

namespace EDMs.SyncData.DAO
{   
    public class MenuDAO : BaseDAO
    {
        public MenuDAO() : base() { }

        #region GET (Basic)
        public IQueryable<Menu> GetIQueryable()
        {
            return EDMsDataContext.Menus;
        }
        
        public List<Menu> GetAll()
        {
            return EDMsDataContext.Menus.ToList();
        }

        public Menu GetByID(int ID)
        {
            return EDMsDataContext.Menus.FirstOrDefault(ob => ob.Id == ID);
        }
       
        #endregion

        #region Get (Advances)

        /// <summary>
        /// Gets the permitted menus by role id.
        /// </summary>
        /// <param name="roleId">The role id.</param>
        /// <returns></returns>
        private List<Menu> GetPermittedMenusByRoleId(int roleId)
        {
            var permissions = EDMsDataContext.Permissions.Where(permission => permission.RoleId == roleId);
            return permissions.Select(permission => EDMsDataContext.Menus.FirstOrDefault(ob => 
                ob.Id == permission.MenuId)).Where(menu => menu != null).ToList();
        }

        /// <summary>
        /// Gets the type of all menus by.
        /// </summary>
        /// <param name="menuType">Type of the menu.</param>
        /// <returns></returns>
        public List<Menu> GetAllMenusByType(int menuType)
        {
            return EDMsDataContext.Menus.Where(x => x.Type == menuType).ToList();
        }

        /// <summary>
        /// Gets all parents menu items by children.
        /// </summary>
        /// <param name="children">The children.</param>
        /// <returns></returns>
        private IEnumerable<Menu> GetAllParentsMenuItemsByChildren(IEnumerable<Menu> children)
        {
            var parents = new List<Menu>();
            foreach (var child in children)
            {
                var temp = child;
                while (temp.ParentId != null)
                {
                    var parentMenu = GetByID(temp.ParentId.Value);
                    temp = parentMenu;

                    if (parents.All(x => x.Id != parentMenu.Id))
                    {
                        parents.Add(parentMenu);
                    }
                }
            }
            return parents;
        }

        /// <summary>
        /// Gets all related permitted menu items.
        /// </summary>
        /// <param name="roleId">The role id.</param>
        /// <param name="menuType">Type of the menu.</param>
        /// <returns></returns>
        public List<Menu> GetAllRelatedPermittedMenuItems(int roleId, int menuType)
        {
            //Get all menu have permitted for current role.
            var menus = GetPermittedMenusByRoleId(roleId);

            if(menus != null)
            {
                //Gets the menus by type.
                menus = menus.Where(menu => menu.Type == menuType).ToList();

                //Gets and adds all parent menu items into the list.
                menus.AddRange(GetAllParentsMenuItemsByChildren(menus));
                return menus;
            }
            return new List<Menu>();
        }


        #endregion

        #region Insert, Update, Delete
        
        #endregion
    }
}
