﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SharedDocumentDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using EDMs.SyncData.DAO;
using EDMs.SyncData.Entities;
using EDMs.SyncData.Entities;

namespace SPF.Data.DAO
{
    /// <summary>
    /// The category dao.
    /// </summary>
    public class SharedDocumentDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SharedDocumentDAO"/> class.
        /// </summary>
        public SharedDocumentDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<SharedDocument> GetIQueryable()
        {
            return this.EDMsDataContext.SharedDocuments;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<SharedDocument> GetAll()
        {
            return this.EDMsDataContext.SharedDocuments.ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public SharedDocument GetById(int id)
        {
            return this.EDMsDataContext.SharedDocuments.FirstOrDefault(ob => ob.ID == id);
        }
       
        #endregion

        #region GET ADVANCE

        #endregion

        #region Insert, Update, Delete

        public bool Update(SharedDocument src)
        {
            try
            {
                var des = (from rs in this.EDMsDataContext.SharedDocuments
                                where rs.ID == src.ID
                                select rs).First();

                des.Name = src.Name;
                des.FilePath = src.FilePath;

                des.UpdatedBy = src.UpdatedBy;
                des.UpdatedDate = src.UpdatedDate;

                EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see>
        ///       <cref>int?</cref>
        ///     </see> .
        /// </returns>
        public int? Insert(SharedDocument ob)
        {
            try
            {
                this.EDMsDataContext.AddToSharedDocuments(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        
        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(SharedDocument src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(int ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
