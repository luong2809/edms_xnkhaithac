﻿//using System;
//using System.Collections.Generic;
//using System.Configuration;
//using System.Data;
//using System.Data.Objects;
//using System.Linq;
//using System.Linq.Expressions;
//using System.Text;
//using EDMs.SyncData.Entities;

//namespace EDMs.SyncData.DAO
//{
//    /// <summary>
//    /// A generic repository for working with data in the database
//    /// Sample: IRepository<Employee> repository = new DataRepository<Employee>();
//    /// </summary>
//    /// <typeparam name="T">A POCO that represents an Entity Framework entity</typeparam>
//    public class Repository<T> : IRepository<T> where T : class
//    {
//        private EDMsEntities _entities;
//        public EDMsEntities Context
//        {
//            get { return _entities; }
//            set { _entities = value; }
//        }
//        public Repository()
//        {
//            _entities = new EDMsEntities();
//        }

//        public virtual T GetByID(int ID)
//        {
//            return _entities.Set<T>().Find(ID);
//        }

//        public virtual IQueryable<T> GetAll()
//        {
//            IQueryable<T> query = _entities.Set<T>();
//            return query;
//        }

//        public IQueryable<T> Find(Expression<Func<T, bool>> predicate)
//        {
//            IQueryable<T> query = _entities.Set<T>().Where(predicate);
//            return query;
//        }

//        public virtual void Add(T entity)
//        {
//            _entities.Set<T>().Add(entity);
//        }

//        public virtual void Delete(T entity)
//        {
//            _entities.Set<T>().Remove(entity);
//        }

//        public virtual void Edit(T entity)
//        {
//            _entities.Entry(entity).State = System.Data.EntityState.Modified;
//        }

//        public virtual void Save()
//        {
//            _entities.SaveChanges();
//        }

//        /// <summary>
//        /// Releases all resources used by the WarrantManagement.DataExtract.Dal.ReportDataBase
//        /// </summary>
//        public void Dispose()
//        {
//            Dispose(true);
//            GC.SuppressFinalize(this);
//        }
//        /// <summary>
//        /// Releases all resources used by the WarrantManagement.DataExtract.Dal.ReportDataBase
//        /// </summary>
//        /// <param name="disposing">A boolean value indicating whether or not to dispose managed resources</param>
//        protected virtual void Dispose(bool disposing)
//        {
//            if (disposing) {
//                if (Context != null) {
//                    Context.Dispose();
//                    Context = null;
//                }
//            }
//        }
//    }
//}
