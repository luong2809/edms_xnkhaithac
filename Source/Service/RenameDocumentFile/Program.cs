﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RenameDocumentFile
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter folder Path: ");
            var folderPath = Console.ReadLine();

            Console.WriteLine("Enter main file name: ");
            var mainFileName = Console.ReadLine();

            Console.WriteLine("Enter lenght of sequence: ");
            var lenghtOfSequence = Console.ReadLine();
            var count = 1;
            if (Directory.Exists(folderPath))
            {
                var fileList = new DirectoryInfo(folderPath).GetFiles();
                foreach (var fileInfo in fileList)
                {
                    var strSequence = GetSequenceString(count, Convert.ToInt32(lenghtOfSequence));
                    File.Move(fileInfo.FullName, fileInfo.DirectoryName + "\\" + mainFileName + strSequence + fileInfo.Extension);

                    count += 1;
                }
            }

        }

        static string GetSequenceString(int sequence, int lenght)
        {
            var strSequence = sequence.ToString();
            var missingLenght = lenght - sequence.ToString().Length;
            for (int i = 0; i < missingLenght; i++)
            {
                strSequence = "0" + strSequence;
            }

            return strSequence;
        }
    }
}
