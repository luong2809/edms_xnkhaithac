﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImportUser
{
    using System.Configuration;
    using System.IO;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;

    class Program
    {
        /// <summary>
        /// The user service.
        /// </summary>
        private static UserService userService = new UserService();

        /// <summary>
        /// The resource service.
        /// </summary>
        private static ResourceService resourceService = new ResourceService();

        /// <summary>
        /// The main.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        static void Main(string[] args)
        {
            var groupId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("GroupId"));
            var userList = File.ReadAllLines(ConfigurationSettings.AppSettings.Get("userListFile")).ToList();

            ////Directory.CreateDirectory(rootPath + listFolder[1]);
            foreach (var user in userList)
            {
                if (!string.IsNullOrEmpty(user))
                {
                    var userName = user.Split('$')[1].Trim().Split('@')[0].ToLower();
                    var resource = new Resource
                    {
                        FullName = user.Split('$')[0].Trim(),
                        Email = user.Split('$')[1].Trim(),
                    };

                    if (!userService.CheckExists(null, userName))
                    {
                        var resourceId = resourceService.Insert(resource);
                        if (resourceId != null)
                        {
                            var userobj = new User
                                {
                                    Username = userName,
                                    ResourceId = resourceId,
                                    Password = "d56171087a3dcb9972919b28051e08f0",
                                    FullName = resource.FullName,
                                    Email = resource.Email,
                                    RoleId = groupId
                                };
                            userService.Insert(userobj);
                            Console.WriteLine("Insert user: {0}", userName);
                        }
                    }
                }
            }

            Console.WriteLine("Completed....!!!");
            Console.ReadLine();
        }
    }
}
