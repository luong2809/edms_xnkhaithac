﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using EDMs.Business.Services;

namespace RemoveDuplicateDoc
{
    class Program
    {
        private static DocumentService documentService = new DocumentService();

        static void Main(string[] args)
        {
            var dataset = new DataSet();
            using (var conn = new SqlConnection(ConfigurationSettings.AppSettings["SiteSqlServer"]))
            {
                using (var cmd = new SqlCommand("GetDuplicateDoc", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    using (var da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(dataset);
                    }

                    conn.Close();
                }
            }

            var folderIds = dataset.Tables[0].AsEnumerable().Select(t => t[4]).Distinct().ToList();
            foreach (var folderId in folderIds)
            {
                var docListOfFolder = documentService.GetAllByFolder(Convert.ToInt32(folderId), true);
                foreach (DataRow dataRow in dataset.Tables[0].Rows)
                {
                    var temp = docListOfFolder.Where(t => t.Name == dataRow[0].ToString()).ToList();
                    if (temp.Count > 1)
                    {
                        var revPath = @"G:\BDPOC DOCLIB" + temp[0].RevisionFilePath;
                        var filePath = @"G:\BDPOC DOCLIB" + temp[0].FilePath;
                        revPath = revPath.Replace("/", @"\");
                        filePath = filePath.Replace("/", @"\");
                        if (!File.Exists(revPath) && File.Exists(filePath))
                        {
                            Console.WriteLine("Copy file: {0}", filePath);
                            File.Copy(filePath, revPath);
                        }

                        for (int i = 1; i < temp.Count(); i++)
                        {
                            Console.WriteLine("RemoveDoc: {0}", temp[i].DocumentNumber);
                            temp[i].IsDelete = true;
                            documentService.Update(temp[i]);
                        }
                    }
                }
            }
        }
    }
}
