﻿namespace CreateFolder
{
    using System;
    using System.IO;
    using System.Linq;

    using ZetaLongPaths;

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Data file path: ");
            var filePath = Console.ReadLine();

            Console.WriteLine("Directory to create: ");
            var rootDirectory = Console.ReadLine();

            if (File.Exists(filePath))
            {
                var listFolder = File.ReadAllLines(filePath).Select(t => t.Replace("/", @"\")).ToList();
                Console.WriteLine("Created folder: ");

                foreach (var folder in listFolder)
                {
                    var fullPath = rootDirectory + @"\" + folder;
                    Console.WriteLine(fullPath);
                    ZlpIOHelper.CreateDirectory(fullPath);
                }
            }

            Console.ReadLine();
        }
    }
}
