﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CopyRevToLeafDoc
{
    using System.IO;

    using EDMs.Business.Services;

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Data file path: ");
            var filePath = Console.ReadLine();

            var documentService = new DocumentService();
            var folderService = new FolderService();

            Console.WriteLine("Directory to copy file: ");
            var rootDirectory = Console.ReadLine();

            if (File.Exists(filePath))
            {
                var folderListObj = File.ReadAllLines(filePath).Select(folderService.GetByDirName).ToList();
                var folderIds = folderListObj.Where(t => t != null).Select(t => t.ID).ToList();
                var docList = documentService.GetAllByFolder(folderIds, true);

                foreach (var document in docList)
                {
                    var revPath = rootDirectory + document.RevisionFilePath.Replace("/", @"\");
                    var docPath = rootDirectory + document.FilePath.Replace("/", @"\");

                    if (File.Exists(revPath))
                    {
                        Console.WriteLine(document.Name);
                        try
                        {
                            if (!Directory.Exists(rootDirectory + document.FilePath.Replace("/", @"\")))
                            {
                                Directory.CreateDirectory(rootDirectory + document.FilePath.Replace("/", @"\"));
                            }

                            File.Copy(revPath, docPath);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                    }
                    else
                    {
                        Console.WriteLine("Can't find revision history file path: '{0}'.", revPath);
                    }
                }
            }
            else
            {
                Console.WriteLine("Can't find data file.");
            }

            Console.ReadLine();
        }
    }
}
