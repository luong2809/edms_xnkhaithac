﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using Aspose.Cells;
using EDMs.Business.Services;

namespace FilterDuplicateDocFromMasterList
{
    class Program
    {
        private static DocumentService documentService = new DocumentService();

        private static RevisionService revisionService = new RevisionService();

        static void Main(string[] args)
        {
            var workbook = new Workbook();
            workbook.Open(@"C:\Users\hongnguyen\Desktop\EDMS - Xí Nghiệp Cơ Điện\MasterList from BDPOC\Master Document Register - BDPOC Engineering Documents_new.xls");
            var wsData = workbook.Worksheets[0];

            var dtData = wsData.Cells.ExportDataTable(14, 1, wsData.Cells.MaxRow, 8);

            var filterData = dtData.AsEnumerable()
                .Where(t => !string.IsNullOrEmpty(t["Column1"].ToString())
                            && !string.IsNullOrEmpty(t["Column5"].ToString())
                            && t["Column5"].ToString().Split('\n').Count() !=
                            t["Column5"].ToString().Split('\n').Distinct().Count());

            var workbook1 = new Workbook();
            workbook1.Open(@"C:\Users\hongnguyen\Desktop\EDMS - Xí Nghiệp Cơ Điện\MasterList from BDPOC\Master Document Register - BDPOC Engineering Documents_new - Compaired_template.xls");
            var wsData1 = workbook1.Worksheets[0];
            var dtdata1 = filterData.CopyToDataTable();

            wsData1.Cells.ImportDataTable(dtdata1, false, 14, 1, dtdata1.Rows.Count, 7, false);
            workbook1.Save(@"C:\Users\hongnguyen\Desktop\EDMS - Xí Nghiệp Cơ Điện\MasterList from BDPOC\Master Document Register - BDPOC Engineering Documents_new - Compaired.xls");
            var temp = new List<string>();

            //foreach (DataRow dataRow in filterData)
            //{
            //    var docNo = dataRow["Column1"].ToString();
            //    Console.WriteLine(docNo);
            //    var rev = dataRow["Column5"].ToString().Trim();
            //    var trans = dataRow["Column7"].ToString().Trim();
            //    var revPart = rev.Split('\n');
            //    var transpart = trans.Split('\n');
            //    if (transpart.Count() >= revPart.Count())
            //    {
            //        for (int i = 0; i < revPart.Count(); i++)
            //        {
            //            var revobj = revisionService.GetByName(revPart[i]) ?? revisionService.GetByName("Rev." + revPart[i]);
            //            var docObj = documentService.GetFilter(docNo, transpart[i], revobj != null ? revobj.ID : 0);

            //            if (docObj == null)
            //            {
            //                var text = docNo + "$" + (revobj != null ? revobj.Name : "--") + "$" + transpart[i];

            //                using (StreamWriter sw = File.AppendText(@"C:\Users\hongnguyen\Desktop\EDMS - Xí Nghiệp Cơ Điện\MasterList from BDPOC\DocDuplicateRev_Filter_OldSystem_Compaired.txt"))
            //                {
            //                    sw.WriteLine(text);
            //                }

            //                temp.Add(text);
            //                Console.WriteLine(text);
            //            }
            //        }
            //    }
            //}
        }
    }
}
