﻿using System;
using System.Diagnostics;
using System.Web;

namespace OpenFolder
{
    class Program
    {
        static void Main(string[] args)
        {
            //var temp = HttpUtility.HtmlDecode(@"openfolder:\\hongnguyen-PC\DocumentLibrary\AdminHR\New%20Folder%201");
            if (args.Length > 0)
            {
                //Console.WriteLine(args[0].Replace("openfolder:", string.Empty));
                Process.Start(args[0].Replace("openfolder:", string.Empty).Replace("%20", " "));    
            }
        }
    }
}