﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ZetaLongPaths;
namespace CopySpecialFiles
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {


                //var tempFile =
                //    new FileInfo(
                //        @"C:\Workspace\BD-EDMSmini\Dev\Web\EDMs.Web\DocumentResource\TransmittalGenerate\Template\temp.txt");

                Console.WriteLine("Enter source folder:");
                var sourcePath = Console.ReadLine();

                Console.WriteLine("Enter file type:");
                var extention = Console.ReadLine();
                Console.WriteLine("Enter destination folder:");
                var destinationMainPath = Console.ReadLine();

                var sourceDirectory = new ZlpDirectoryInfo(sourcePath);
                //foreach (var directoryInfo in sourceDirectory.GetDirectories("*.*", SearchOption.AllDirectories).OrderBy(t => t.FullName))
                //{
                //    var desPath = destinationMainPath + directoryInfo.FullName.Replace(sourceDirectory.FullName, string.Empty);
                //}

                var fileList =
                    sourceDirectory.GetFiles("*." + extention, SearchOption.AllDirectories)
                        //.Where(t => !t.DirectoryName.Contains(@"DocumentLibrary\Operations\03. Technical Documents"))
                        .OrderBy(t => t.DirectoryName);
                Console.WriteLine("Copying files:");
                foreach (var fileInfo in fileList)
                {
                    var desPath = destinationMainPath +
                                  fileInfo.DirectoryName.Replace(sourceDirectory.FullName, string.Empty);

                    if (!Directory.Exists(desPath))
                    {
                        Directory.CreateDirectory(desPath);
                    }

                    fileInfo.CopyTo(desPath + @"\" + fileInfo.Name, true);
                    //File.Copy(fileInfo.FullName, desPath + @"\" + fileInfo.Name);
                    Console.WriteLine(fileInfo.FullName);
                }

                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }
    }
}
