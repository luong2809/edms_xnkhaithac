﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using EDMs.Business.Services;
using EDMs.Data.Entities;

namespace UpdateActiveDocOffshore
{
    public partial class UpdateActiveDoc : ServiceBase
    {
        private string MainPath = ConfigurationSettings.AppSettings.Get("FolderServer");
        private DocumentService documentService;
        private System.Timers.Timer timerSync = null;
        public UpdateActiveDoc()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            //// Min
            timerSync = new System.Timers.Timer(60 * 1000 * Convert.ToInt32(ConfigurationSettings.AppSettings.Get("TimeSequence")));
            timerSync.Enabled = true;
            timerSync.Elapsed += new System.Timers.ElapsedEventHandler(Timer_Fired);
            timerSync.AutoReset = true;
            timerSync.Start();

            //this.EventLog.WriteEntry("Check Active Doc service started");
        }

        protected override void OnStop()
        {
            //timerSync.Dispose();
            //this.EventLog.WriteEntry("Check Active Doc service stoped");
        }

        private void Timer_Fired(object sender, EventArgs e)
        {
            try
            {
                timerSync.Stop();
                UpdateActiveDocument();
            }
            finally
            {
                timerSync.Start();
            }
        }

        private void UpdateActiveDocument()
        {
            documentService = new DocumentService();
            var docInactiveList = documentService.GetAll().Where(t => !t.IsActive.GetValueOrDefault() && !t.IsDelete.GetValueOrDefault());
            foreach (var document in docInactiveList)
            {

                // DocumentLibrary/AdminHR/03. Test Data/TEST002.docx
                var fileServerPath = this.MainPath + document.FilePath.Replace("/", @"\");
                if (File.Exists(fileServerPath))
                {
                    var fileInfo = new FileInfo(fileServerPath);
                    if (document.ParentID == null)
                    {
                        document.IsLeaf = true;
                        document.IsActive = true;

                        this.documentService.Update(document);
                        //this.EventLog.WriteEntry("Check Active Doc service enable doc: " + document.DocumentNumber);
                    }
                    else
                    {
                        var allRevision = this.documentService.GetAllDocRevision(document.ParentID.Value);
                        var oldLeafRev = allRevision.FirstOrDefault(t => t.IsLeaf.GetValueOrDefault());
                        if (oldLeafRev != null && oldLeafRev.ModifiedDateDocFile < fileInfo.LastWriteTime)
                        {
                            oldLeafRev.IsLeaf = false;

                            document.IsLeaf = true;
                            document.IsActive = true;

                            this.documentService.Update(document);
                            this.documentService.Update(oldLeafRev);
                            //this.EventLog.WriteEntry("Check Active Doc service enable doc: " + document.DocumentNumber);
                        }
                    }
                }
                
            }
        }
    }
}
