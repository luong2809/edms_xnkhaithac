﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="">
//   
// </copyright>
// <summary>
//   The program.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Runtime.CompilerServices;

namespace TestFolderWatcher
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;

    using Aspose.Cells;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    /// <summary>
    /// The program.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// The is create.
        /// </summary>
        private bool isCreate = false;

        /// <summary>
        /// The file icon.
        /// </summary>
        private static Dictionary<string, string> fileIcon = new Dictionary<string, string>()
        {
            {"pptx", "images/powerpointFile.png"},
            {"ppt", "images/powerpointFile.png"},
            {"doc", "images/wordfile.png"},
            {"docx", "images/wordfile.png"},
            {"dotx", "images/wordfile.png"},
            {"xls", "images/excelfile.png"},
            {"xlsx", "images/excelfile.png"},
            {"pdf", "images/pdffile.png"},
            {"7z", "images/7z.png"},
            {"dwg", "images/dwg.png"},
            {"dxf", "images/dxf.png"},
            {"rar", "images/rar.png"},
            {"zip", "images/zip.png"},
            {"txt", "images/txt.png"},
            {"xml", "images/xml.png"},
            {"xlsm", "images/excelfile.png"},
            {"bmp", "images/bmp.png"},
        };

        private static FolderService folderService = new FolderService();

        private static DocumentService documentService = new DocumentService();

        private static RevisionService revisionService = new RevisionService();
        private static StatusService statusService = new StatusService();
        private static DisciplineService disciplineService = new DisciplineService();
        private static DocumentTypeService documentTypeService = new DocumentTypeService();
        private static ReceivedFromService receivedFromService = new ReceivedFromService();
        private static UserService userService = new UserService();
        private static ResourceService resourceService = new ResourceService();
        private static string MainPath = ConfigurationSettings.AppSettings.Get("FolderServer");
        private static System.Timers.Timer timerSync = null;
        /// <summary>
        /// The file extensions.
        /// </summary>
        private static List<string> fileExtensions =
            ConfigurationSettings.AppSettings.Get("Extension").Split(',').ToList();

        /// <summary>
        /// The main.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        private static void Main(string[] args)
        {

            documentService = new DocumentService();
            // Min
            timerSync = new System.Timers.Timer(60 * 1000 * Convert.ToInt32(ConfigurationSettings.AppSettings.Get("TimeSequence")));
            timerSync.Enabled = true;
            timerSync.Elapsed += new System.Timers.ElapsedEventHandler(Timer_Fired);
            timerSync.AutoReset = true;
            timerSync.Start();


            ////DirectoryEntry searchRoot = new DirectoryEntry("LDAP://10.17.31.11", "doclib", "123456a@doc");
            ////DirectorySearcher directorySearcher = new DirectorySearcher(searchRoot);

            ////directorySearcher.Filter = "(&(objectClass=user))";
            ////string text = "sAMAccountName";

            ////directorySearcher.PropertiesToLoad.Add("cn");
            ////directorySearcher.PropertiesToLoad.Add(text);
            ////directorySearcher.PropertiesToLoad.Add("memberOf");

            ////SearchResultCollection searchResultCollection = directorySearcher.FindAll();
            ////foreach (SearchResult searchResult in searchResultCollection)
            ////{

            ////    if (searchResult.Properties["cn"].Count > 0 && searchResult.Properties[text].Count > 0)
            ////    {
            ////        Console.WriteLine(searchResult.Properties["cn"][0].ToString() + " - " + searchResult.Properties[text][0].ToString() + " - " + searchResult.Properties["memberOf"][0].ToString());
            ////    }
            ////}
            Console.ReadLine();
        }

        private static void Timer_Fired(object sender, EventArgs e)
        {
            try
            {
                timerSync.Stop();
                UpdateActiveDocument();
            }
            finally
            {
                timerSync.Start();
            }
        }

        private static void UpdateActiveDocument()
        {
            var docInactiveList = documentService.GetAll().Where(t => !t.IsActive.GetValueOrDefault() && !t.IsDelete.GetValueOrDefault());
            foreach (var document in docInactiveList)
            {

                // DocumentLibrary/AdminHR/03. Test Data/TEST002.docx
                var fileServerPath = MainPath + document.FilePath.Replace("/", @"\");
                if (File.Exists(fileServerPath))
                {
                    var fileInfo = new FileInfo(fileServerPath);
                    if (document.ParentID == null)
                    {
                        document.IsLeaf = true;
                        document.IsActive = true;

                        documentService.Update(document);
                    }
                    else
                    {
                        var allRevision = documentService.GetAllDocRevision(document.ParentID.Value);
                        var oldLeafRev = allRevision.FirstOrDefault(t => t.IsLeaf.GetValueOrDefault());
                        if (oldLeafRev != null && oldLeafRev.ModifiedDateDocFile < fileInfo.LastWriteTime)
                        {
                            oldLeafRev.IsLeaf = false;

                            document.IsLeaf = true;
                            document.IsActive = true;

                            documentService.Update(document);
                            documentService.Update(oldLeafRev);
                        }
                    }
                }

            }
        }

        static void FilterMissingDoc()
        {
            var workbook = new Workbook();
            workbook.Open(@"C:\Users\hongnguyenx\Desktop\BDPOC data\GetMissingDocWithSameRev\Master Document Register - BDPOC Engineering Documents_New.xls");
            var wsData = workbook.Worksheets[0];

            var dtData = wsData.Cells.ExportDataTable(14, 1, wsData.Cells.MaxRow, 8);

            var filterData = dtData.AsEnumerable()
                .Where(t => !string.IsNullOrEmpty(t["Column1"].ToString())
                            && !string.IsNullOrEmpty(t["Column5"].ToString())
                            && t["Column5"].ToString().Split('\n').Count() !=
                            t["Column5"].ToString().Split('\n').Distinct().Count());

            var temp = new List<string>();

            foreach (DataRow dataRow in filterData)
            {
                var docNo = dataRow["Column1"].ToString();
                Console.WriteLine(docNo);
                var rev = dataRow["Column5"].ToString().Trim();
                var trans = dataRow["Column7"].ToString().Trim();
                var revPart = rev.Split('\n');
                var transpart = trans.Split('\n');
                if (transpart.Count() >= revPart.Count())
                {
                    for (int i = 0; i < revPart.Count(); i++)
                    {
                        var revobj = revisionService.GetByName(revPart[i]) ?? revisionService.GetByName("Rev." + revPart[i]);
                        var docObj = documentService.GetFilter(docNo, transpart[i], revobj != null ? revobj.ID : 0);

                        if (docObj == null)
                        {
                            var text = docNo + "$" + (revobj != null ? revobj.Name : "--") + "$" + transpart[i];

                            using (StreamWriter sw = File.AppendText(@"C:\Users\hongnguyenx\Desktop\BDPOC data\GetMissingDocWithSameRev\DocDuplicateRev_Filter_OldSystem_Compaired.txt"))
                            {
                                sw.WriteLine(text);
                            }

                            temp.Add(text);
                            Console.WriteLine(text);
                        }
                    }
                }
            }
        }

        static void CreateValidation(string formular, Validations objValidations, int startRow, int endRow, int startColumn, int endColumn)
        {
            // Create a new validation to the validations list.
            Validation validation = objValidations[objValidations.Add()];

            // Set the validation type.
            validation.Type = Aspose.Cells.ValidationType.List;

            // Set the operator.
            validation.Operator = OperatorType.None;

            // Set the in cell drop down.
            validation.InCellDropDown = true;

            // Set the formula1.
            validation.Formula1 = "=" + formular;

            // Enable it to show error.
            validation.ShowError = true;

            // Set the alert type severity level.
            validation.AlertStyle = ValidationAlertType.Stop;

            // Set the error title.
            validation.ErrorTitle = "Error";

            // Set the error message.
            validation.ErrorMessage = "Please select a color from the list";

            // Specify the validation area.
            CellArea area;
            area.StartRow = startRow;
            area.EndRow = endRow;
            area.StartColumn = startColumn;
            area.EndColumn = endColumn;

            // Add the validation area.
            validation.AreaList.Add(area);

            ////return validation;
        }


        /// <summary>
        /// The fsw_ renamed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        static void fsw_Renamed(object sender, RenamedEventArgs e)
        {
            try
            {
                if (e.FullPath.IndexOf(@"RevisionHistory", StringComparison.Ordinal) == -1 &&
                    e.Name.LastIndexOf(".", StringComparison.Ordinal) != -1)
                {
                    var fullPath = e.OldName;
                    var fileExt = fullPath.Substring(
                        fullPath.LastIndexOf(".") + 1, fullPath.Length - fullPath.LastIndexOf(".") - 1);

                    if (fileExtensions.Contains(fileExt.ToLower()))
                    {
                        var lastPosition = fullPath.LastIndexOf(@"\");
                        var oldFileName = fullPath.Substring(lastPosition + 1, fullPath.Length - lastPosition - 1);
                        var oldPath = "DocumentLibrary/" + fullPath.Substring(0, lastPosition).Replace(@"\", "/");

                        var newFileName = e.Name.Substring(lastPosition + 1, e.Name.Length - lastPosition - 1);

                        var listDocRename = documentService.GetSpecificDocument(oldFileName, oldPath);
                        foreach (var document in listDocRename)
                        {
                            ////var oldRevisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                            ////                      + "DocumentLibrary/RevisionHistory/" + document.RevisionFileName;

                            document.Name = newFileName;
                            document.FileNameOriginal = newFileName;
                            document.LastUpdatedDate = DateTime.Now;

                            if (!string.IsNullOrEmpty(document.RevisionName))
                            {
                                document.RevisionFileName = document.RevisionName + "_" + newFileName;
                            }
                            else
                            {
                                document.RevisionFileName = document.DocIndex + "_" + newFileName;
                            }

                            document.FilePath = document.FilePath.Replace(oldFileName, newFileName);
                            ////document.RevisionFilePath = document.RevisionFilePath.Replace(oldFileName, newFileName);

                            ////var newRevisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                            ////                      + "DocumentLibrary/RevisionHistory/" + document.RevisionFileName;

                            ////File.Move(oldRevisionPath, newRevisionPath);

                            documentService.Update(document);
                        }

                        ////Console.WriteLine("Renamed: FileName - {0}, ChangeType - {1}, Old FileName - {2}", e.Name, e.ChangeType, e.OldName);
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        /// <summary>
        /// The fsw_ deleted.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        static void fsw_Deleted(object sender, FileSystemEventArgs e)
        {
            if (e.FullPath.IndexOf(@"RevisionHistory", StringComparison.Ordinal) == -1 &&
                e.Name.LastIndexOf(".", StringComparison.Ordinal) != -1)
            {
                var fullPath = e.Name;
                var fileExt = fullPath.Substring(
                    fullPath.LastIndexOf(".", StringComparison.Ordinal) + 1, fullPath.Length - fullPath.LastIndexOf(".", StringComparison.Ordinal) - 1);

                if (fileExtensions.Contains(fileExt))
                {
                    try
                    {
                        var lastPosition = fullPath.LastIndexOf(@"\");
                        var fileName = fullPath.Substring(lastPosition + 1, fullPath.Length - lastPosition - 1);
                        var path = "DocumentLibrary/" + fullPath.Substring(0, lastPosition).Replace(@"\", "/");

                        var listDocDelete = documentService.GetSpecificDocument(fileName, path);
                        foreach (var document in listDocDelete)
                        {
                            document.IsDelete = true;
                            ////documentService.Update(document);

                            ////var revisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                            ////                   + "DocumentLibrary/RevisionHistory/" + document.RevisionFileName;
                            ////var fileTemp = new FileInfo(revisionPath);
                            ////fileTemp.Delete();
                        }

                        Console.WriteLine("Deleted: FileName - {0}, ChangeType - {1}", e.Name, e.ChangeType);
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine(exception.InnerException.Message);
                    }
                }
            }
        }

        /// <summary>
        /// The fsw_ changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        static void fsw_Changed(object sender, FileSystemEventArgs e)
        {
            if (e.FullPath.IndexOf(@"RevisionHistory", StringComparison.Ordinal) == -1 &&
                e.Name.LastIndexOf(".", StringComparison.Ordinal) != -1)
            {
                var fullPath = e.Name;
                var fileExt = fullPath.Substring(
                    fullPath.LastIndexOf(".") + 1, fullPath.Length - fullPath.LastIndexOf(".") - 1);

                if (fileExtensions.Contains(fileExt.ToLower()))
                {
                    var lastPosition = fullPath.LastIndexOf(@"\");
                    var fileName = fullPath.Substring(lastPosition + 1, fullPath.Length - lastPosition - 1);
                    var path = fullPath.Substring(0, lastPosition).Replace(@"\", "/");

                    var objFolder = folderService.GetByDirName("DocumentLibrary/" + path);
                    if (objFolder != null)
                    {
                        var docLeaf = documentService.GetSpecificDocument(objFolder.ID, fileName);

                        var objDoc = new Document()
                            {
                                Name = docLeaf.Name,
                                DocumentNumber = docLeaf.DocumentNumber,
                                Title = docLeaf.Title,
                                DocumentTypeID = docLeaf.DocumentTypeID,
                                StatusID = docLeaf.StatusID,
                                DisciplineID = docLeaf.DisciplineID,
                                ReceivedFromID = docLeaf.ReceivedFromID,
                                ReceivedDate = docLeaf.ReceivedDate,
                                TransmittalNumber = docLeaf.TransmittalNumber,
                                LanguageID = docLeaf.LanguageID,
                                Well = docLeaf.Well,
                                Remark = docLeaf.Remark,
                                KeyWords = docLeaf.KeyWords,
                                
                                IsLeaf = true,
                                DocIndex = docLeaf.DocIndex + 1,
                                IsDelete = false,
                                FolderID = docLeaf.FolderID,
                                DirName = docLeaf.DirName,
                                FileExtension = docLeaf.FileExtension,
                                FileExtensionIcon = docLeaf.FileExtensionIcon,
                                FileNameOriginal = docLeaf.FileNameOriginal,
                                FilePath = docLeaf.FilePath,
                                CreatedDate = DateTime.Now
                            };

                        if (docLeaf.ParentID == null)
                        {
                            objDoc.ParentID = docLeaf.ID;
                        }
                        else
                        {
                            objDoc.ParentID = docLeaf.ParentID;
                        }

                        objDoc.RevisionFileName = objDoc.DocIndex + "_" + objDoc.FileNameOriginal;
                        objDoc.RevisionFilePath = docLeaf.RevisionFilePath.Replace(
                            docLeaf.RevisionFileName, objDoc.RevisionFileName);
                        // Copy to revision folder
                        var revisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                                           + "DocumentLibrary/RevisionHistory/";
                        var tempFile = new FileInfo(e.FullPath);


                        ////tempFile.CopyTo(revisionPath + objDoc.RevisionFileName, true);

                        docLeaf.IsLeaf = false;

                        documentService.Update(docLeaf);
                        documentService.Insert(objDoc);
                    }

                    Console.WriteLine("Changed: FileName - {0}, ChangeType - {1}", e.Name, e.ChangeType);
                }
            }
        }

        /// <summary>
        /// The fsw_ created.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        static void fsw_Created(object sender, FileSystemEventArgs e)
        {
            if (e.FullPath.IndexOf(@"RevisionHistory", StringComparison.Ordinal) == -1 && 
                e.Name.LastIndexOf(".", StringComparison.Ordinal) != -1)
            {
                var fullPath = e.Name;
                var fileExt = fullPath.Substring(
                    fullPath.LastIndexOf(".") + 1, fullPath.Length - fullPath.LastIndexOf(".") - 1);

                if (fileExtensions.Contains(fileExt.ToLower()))
                {
                    try
                    {
                        var lastPosition = fullPath.LastIndexOf(@"\");
                        var fileName = fullPath.Substring(lastPosition + 1, fullPath.Length - lastPosition - 1);
                        var path = fullPath.Substring(0, lastPosition).Replace(@"\", "/");

                        var objFolder = folderService.GetByDirName("DocumentLibrary/" + path);

                        var objDoc = new Document()
                            {
                                Name = fileName,
                                DocIndex = 1,
                                CreatedDate = DateTime.Now,
                                IsLeaf = true,
                                IsDelete = false
                            };
                        objDoc.RevisionFileName = objDoc.DocIndex + "_" + fileName;
                        objDoc.FilePath = "/bdpocedms/DocumentLibrary/" + path + "/" + fileName;
                        objDoc.RevisionFilePath = "/bdpocedms/DocumentLibrary/RevisionHistory/" + objDoc.DocIndex + "_" + fileName;
                        objDoc.FileExtension = fileExt;
                        objDoc.FileExtensionIcon = fileIcon.ContainsKey(fileExt.ToLower())
                                                       ? fileIcon[fileExt.ToLower()]
                                                       : "images/otherfile.png";
                        objDoc.FileNameOriginal = fileName;
                        objDoc.DirName = "DocumentLibrary/" + path;

                        if (objFolder != null)
                        {
                            objDoc.FolderID = objFolder.ID;
                        }

                        // Copy to revision folder
                        var revisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                                           + "DocumentLibrary/RevisionHistory/";
                        var tempFile = new FileInfo(e.FullPath);


                        ////tempFile.CopyTo(revisionPath + objDoc.RevisionFileName, true);
                        documentService.Insert(objDoc);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.InnerException.Message);
                    }

                    Console.WriteLine("Created: FileName - {0}, ChangeType - {1}", e.Name, e.ChangeType);
                }
            }
        }

        /// <summary>
        /// The create folder.
        /// </summary>
        static void CreateFolder()
        {
            var rootPath = ConfigurationSettings.AppSettings.Get("rootPath");
            var listFolder = File.ReadAllLines(ConfigurationSettings.AppSettings.Get("listFolder")).ToList();

            ////Directory.CreateDirectory(rootPath + listFolder[1]);
            foreach (var folder in listFolder)
            {
                if (!string.IsNullOrEmpty(folder))
                {
                    try
                    {
                        //Directory.CreateDirectory(rootPath + folder);
                        Console.WriteLine("Create: '" + rootPath + folder + "'");
                        var listSubfolder = folder.Split('/').Where(t => !string.IsNullOrEmpty(t)).ToList();
                        var dirFather = listSubfolder[0] + "/" + listSubfolder[1];

                        for (int i = 2; i < listSubfolder.Count; i++)
                        {
                            var folFa = folderService.GetByDirName(dirFather);
                            var folChild = folderService.GetByDirName(dirFather + "/" + listSubfolder[i]);
                            if (folChild == null)
                            {
                                folChild = new Folder
                                {
                                    Name = listSubfolder[i],
                                    DirName = dirFather + "/" + listSubfolder[i],
                                    ParentID = folFa.ID,
                                    CategoryID = folFa.CategoryID,
                                    Description = listSubfolder[i]
                                };

                                folderService.Insert(folChild);
                            }

                            dirFather += "/" + listSubfolder[i];
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error: '" + ex.Message + "'");
                    }
                }
            }
        }
    }
}
