﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceProcess;
using EDMs.Business.Services;
using EDMs.Data.Entities;

namespace SyncToOffShore
{
    public partial class SyncData : ServiceBase
    {
        private SyncConfigService syncConfigService;
        private SyncConfig configObj;
        private WaitingSyncDataService waitingSyncDataService;
        private System.Timers.Timer timerSync = null;
        private SqlConnection conn;
        private Dictionary<string, DayOfWeek> myDic = new Dictionary<string, DayOfWeek>()
        {
            {"1", DayOfWeek.Monday},
            {"2", DayOfWeek.Tuesday},
            {"3", DayOfWeek.Wednesday},
            {"4", DayOfWeek.Thursday},
            {"5", DayOfWeek.Friday},
            {"6", DayOfWeek.Saturday},
            {"7", DayOfWeek.Sunday},
        };
        public SyncData()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            syncConfigService = new SyncConfigService();
            configObj = syncConfigService.GetAll().FirstOrDefault();
            if (configObj != null)
            {
                if (!string.IsNullOrEmpty(configObj.ConnectToServer))
                {
                    conn = new SqlConnection(configObj.ConnectToServer);
                }
                if (configObj.Type == 1)
                {
                    var arrData = configObj.Config.Split('$');
                    if (arrData.Any())
                    {
                        switch (arrData[1])
                        {
                            case "1": // Min
                                timerSync = new System.Timers.Timer(60*1000*Convert.ToInt32(arrData[0]));
                                break;
                            case "2": // Hour
                                timerSync = new System.Timers.Timer(60*60*1000*Convert.ToInt32(arrData[0]));
                                break;
                        }

                        timerSync.Enabled = true;
                        timerSync.Elapsed += new System.Timers.ElapsedEventHandler(Timer_Fired);
                        timerSync.AutoReset = true;
                        timerSync.Start();

                        this.EventLog.WriteEntry("Doclib Sync data service started");
                    }

                }
                else if (configObj.Type == 2)
                {
                    var arrData = configObj.Config.Split('$');
                    if (arrData.Any())
                    {
                        timerSync = new System.Timers.Timer(60 * 1000);
                        timerSync.Enabled = true;
                        timerSync.Elapsed += new System.Timers.ElapsedEventHandler(Timer_Fired);
                        timerSync.AutoReset = true;
                        timerSync.Start();

                        this.EventLog.WriteEntry("Doclib Sync data service started");
                    }
                }
            }
        }

        protected override void OnStop()
        {
            timerSync.Dispose();
            this.EventLog.WriteEntry("Doclib Sync data service stoped");
        }

        private void Timer_Fired(object sender, EventArgs e)
        {
            try
            {
                timerSync.Stop();
                if (configObj.Type == 1)
                {
                    SyncDataProcess();
                }
                else if (configObj.Type == 2)
                {
                    if (TimeToRun())
                    {
                        SyncDataProcess();
                    }
                }
            }
            finally
            {
                timerSync.Start();
            }
        }

        private bool TimeToRun()
        {
            var arrData = configObj.Config.Split('$');
            if (arrData.Any())
            {
                switch (arrData[0])
                {
                    case "Day":
                        return DateTime.Now.Hour == Convert.ToInt32(arrData[1]);
                        break;
                    case "Week":
                        return DateTime.Now.DayOfWeek == myDic[arrData[1]];
                        break;
                    case "Month":
                        return DateTime.Now.Day == Convert.ToInt32(arrData[1]);
                        break;
                }
            }

            return false;
        }

        private void SyncDataProcess()
        {
            waitingSyncDataService = new WaitingSyncDataService();
            var syncDataList = waitingSyncDataService.GetAllUnSync();

            foreach (var waitingSyncData in syncDataList)
            {
                var cmd = new SqlCommand("SyncData_" + waitingSyncData.ObjectName, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@p_ActionType", SqlDbType.Int).Value = waitingSyncData.ActionTypeID;
                cmd.Parameters.Add("@p_ObjId", SqlDbType.Int).Value = waitingSyncData.ObjectID;
                try
                {
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();

                    waitingSyncData.IsSynced = true;
                    waitingSyncDataService.Update(waitingSyncData);
                }
                catch (Exception ex)
                {
                    this.EventLog.WriteEntry("Doclib Sync data service have error: " + ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
        }
    }
}
