﻿using System.ComponentModel;
using System.ServiceProcess;
using System.Configuration;

namespace EDMSFolderWatcher
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;

    using Temp.IO;

    public partial class EDMSFolderWatcher : ServiceBase
    {
        /// <summary>
        /// The monitor path.
        /// </summary>
        private readonly string monitorPath = ConfigurationSettings.AppSettings.Get("Directory");

        /// <summary>
        /// The file icon.
        /// </summary>
        private static readonly Dictionary<string, string> fileIcon = new Dictionary<string, string>()
                {
                    { "pptx", "images/powerpointFile.png" },
                    { "ppt", "images/powerpointFile.png" },
                    { "doc", "images/wordfile.png" },
                    { "docx", "images/wordfile.png" },
                    { "dotx", "images/wordfile.png" },
                    { "xls", "images/excelfile.png" },
                    { "xlsx", "images/excelfile.png" },
                    { "pdf", "images/pdffile.png" },
                    { "7z", "images/7z.png" },
                    { "dwg", "images/dwg.png" },
                    { "dxf", "images/dxf.png" },
                    { "rar", "images/rar.png" },
                    { "zip", "images/zip.png" },
                    { "txt", "images/txt.png" },
                    { "xml", "images/xml.png" },
                    { "xlsm", "images/excelfile.png" },
                    { "mp4", "images/video.png" },
                    { "webm", "images/video.png" },
                    { "mkv", "images/video.png" },
                    { "flv", "images/video.png" },
                    { "avi", "images/video.png" },
                    { "mov", "images/video.png" },
                    { "wmv", "images/video.png" },
                    { "mpeg", "images/video.png" },
                    { "jpg", "images/picture.png" },
                    { "png", "images/picture.png" },
                };

        /// <summary>
        /// The folder service.
        /// </summary>
        private  FolderService folderService;

        /// <summary>
        /// The document service.
        /// </summary>
        private  DocumentService documentService;

        private GroupDataPermissionService groupDataPermissionService;

        private UserDataPermissionService userDataPermissionService;

        /// <summary>
        /// The file extensions.
        /// </summary>
        private readonly List<string> fileExtensions = ConfigurationSettings.AppSettings.Get("Extension").Split(',').ToList();

        /// <summary>
        /// The folder watcher.
        /// </summary>
        private MyFileSystemWatcher folderWatcher;

        /// <summary>
        /// The is action.
        /// </summary>
        private bool isAction = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="EDMSFolderWatcher"/> class.
        /// </summary>
        public EDMSFolderWatcher()
        {
            InitializeComponent();

            
        }

        /// <summary>
        /// The on start.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        protected override void OnStart(string[] args)
        {
            this.isAction = true;
            this.folderWatcher = new MyFileSystemWatcher(this.monitorPath, "*.*")
                {
                    Interval = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("WatcherInterval")),
                    InternalBufferSize = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("WatcherBufferSize"))
                };

            this.folderWatcher.Created += new System.IO.FileSystemEventHandler(fsw_TotalChanged);
            this.folderWatcher.Changed += new System.IO.FileSystemEventHandler(fsw_TotalChanged);
            this.folderWatcher.Deleted += new System.IO.FileSystemEventHandler(fsw_TotalChanged);
            this.folderWatcher.Renamed += new System.IO.RenamedEventHandler(fsw_Renamed);
            this.folderWatcher.EnableRaisingEvents = true;
            EventLog.WriteEntry("EDMS folder watcher service is started. Watching on '" + this.monitorPath + "'");
        }

        protected override void OnStop()
        {
            this.folderWatcher.Dispose();
            
            EventLog.WriteEntry("EDMS folder watcher service is stoped.");
        }

        protected override void OnCustomCommand(int command)
        {
            switch (command)
            {
                case 129:
                    this.isAction = true;
                    break;
                case 128:
                    this.isAction = false;
                    break;
            }

            this.EventLog.WriteEntry(this.isAction ? "Action is enabled." : "Action is disabled.");
        }

        /// <summary>
        /// The fsw_ renamed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void fsw_Renamed(object sender, RenamedEventArgs e)
        {
            try
            {
                if (this.isAction && e.FullPath.IndexOf(@"RevisionHistory", StringComparison.Ordinal) == -1 
                    && e.Name.LastIndexOf(".", StringComparison.Ordinal) != -1)
                    //&& File.Exists(e.FullPath))
                {
                    folderService = new FolderService();
                    documentService = new DocumentService();

                    var fullPath = e.OldName;
                    var fileExt = fullPath.Substring(
                        fullPath.LastIndexOf(".") + 1, fullPath.Length - fullPath.LastIndexOf(".") - 1);

                    if (fileExtensions.Contains(fileExt.ToLower()))
                    {
                        var lastPosition = fullPath.LastIndexOf(@"\");
                        var oldFileName = fullPath.Substring(lastPosition + 1, fullPath.Length - lastPosition - 1);
                        var oldPath = "DocumentLibrary/" + fullPath.Substring(0, lastPosition).Replace(@"\", "/");

                        var newFileName = e.Name.Substring(lastPosition + 1, e.Name.Length - lastPosition - 1);

                        var listDocRename = documentService.GetSpecificDocument(oldFileName, oldPath);
                        foreach (var document in listDocRename)
                        {
                            ////var oldRevisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                            ////                      + "DocumentLibrary/RevisionHistory/" + document.RevisionFileName;

                            document.Name = newFileName;
                            document.FileNameOriginal = newFileName;
                            document.LastUpdatedDate = DateTime.Now;

                            if (!string.IsNullOrEmpty(document.RevisionName))
                            {
                                document.RevisionFileName = document.RevisionName + "_" + newFileName;
                            }
                            else
                            {
                                document.RevisionFileName = document.DocIndex + "_" + newFileName;
                            }

                            document.FilePath = document.FilePath.Replace(oldFileName, newFileName);
                            ////document.RevisionFilePath = document.RevisionFilePath.Replace(oldFileName, newFileName);

                            ////var newRevisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                            ////                      + "DocumentLibrary/RevisionHistory/" + document.RevisionFileName;

                            ////File.Move(oldRevisionPath, newRevisionPath);

                            this.documentService.Update(document);
                        }

                        this.EventLog.WriteEntry("Change file Name from: $" + e.OldName + "$ to: $" + e.Name + "$ Path: " + oldPath);

                        ////Console.WriteLine("Renamed: FileName - {0}, ChangeType - {1}, Old FileName - {2}", e.Name, e.ChangeType, e.OldName);
                    }
                }
                ////else if (Directory.Exists(e.FullPath))
                ////{
                ////    folderService = new FolderService();

                ////    var folderName = e.Name.Substring(e.Name.LastIndexOf(@"\") + 1,
                ////        e.Name.Length - e.Name.LastIndexOf(@"\") - 1);
                ////    var oldDirName = "DocumentLibrary/" + e.OldName.Replace(@"\", "/");
                ////    var oldFolder = folderService.GetByDirName(oldDirName);
                ////    if (oldFolder != null)
                ////    {
                ////        oldFolder.Name = folderName;
                ////        oldFolder.Description = folderName;
                ////        oldFolder.DirName = oldFolder.DirName.Substring(0, oldFolder.DirName.LastIndexOf("/")) + "/" + folderName;
                ////        folderService.Update(oldFolder);
                ////    }
                ////}
            }
            catch (Exception exception)
            {
                EventLog.WriteEntry(exception.Message);
            }
        }

        /// <summary>
        /// The fsw_ deleted.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void fsw_Deleted(object sender, FileSystemEventArgs e)
        {
            try
            {
                if (this.isAction && e.FullPath.IndexOf(@"RevisionHistory", StringComparison.Ordinal) == -1 &&
                    e.Name.LastIndexOf(".", StringComparison.Ordinal) != -1)
                {
                    var fullPath = e.Name;
                    var fileExt = fullPath.Substring(
                        fullPath.LastIndexOf(".", StringComparison.Ordinal) + 1, fullPath.Length - fullPath.LastIndexOf(".", StringComparison.Ordinal) - 1);

                    if (this.fileExtensions.Contains(fileExt.ToLower()))
                    {
                        var lastPosition = fullPath.LastIndexOf(@"\", StringComparison.Ordinal);
                        var fileName = fullPath.Substring(lastPosition + 1, fullPath.Length - lastPosition - 1);
                        var path = "DocumentLibrary/" + fullPath.Substring(0, lastPosition).Replace(@"\", "/");

                        var listDocDelete = this.documentService.GetSpecificDocument(fileName, path);
                        foreach (var document in listDocDelete)
                        {
                            document.IsDelete = true;
                            documentService.Update(document);

                            ////var revisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                            ////                   + "DocumentLibrary/RevisionHistory/" + document.RevisionFileName;
                            ////var fileTemp = new FileInfo(revisionPath);
                            ////fileTemp.Delete();
                        }

                        this.EventLog.WriteEntry("Delete file: $" + e.Name + "$ Path: " + path);
                        ////Console.WriteLine("Deleted: FileName - {0}, ChangeType - {1}", e.Name, e.ChangeType);
                    }
                }
            }
            catch (Exception exception)
            {
                EventLog.WriteEntry(exception.Message);
            }
        }

        /// <summary>
        /// The fsw_ changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void fsw_Changed(object sender, FileSystemEventArgs e)
        {
            try
            {
                if (this.isAction && e.FullPath.IndexOf(@"RevisionHistory", StringComparison.Ordinal) == -1
                    && e.Name.LastIndexOf(".", StringComparison.Ordinal) != -1)
                {
                    var fullPath = e.Name;
                    var fileExt = fullPath.Substring(
                        fullPath.LastIndexOf(".") + 1, fullPath.Length - fullPath.LastIndexOf(".") - 1);

                    if (fileExtensions.Contains(fileExt.ToLower()))
                    {
                        var lastPosition = fullPath.LastIndexOf(@"\");
                        var fileName = fullPath.Substring(lastPosition + 1, fullPath.Length - lastPosition - 1);
                        var path = fullPath.Substring(0, lastPosition).Replace(@"\", "/");
                        var revisionFileName = DateTime.Now.ToString("ddMMyyhhmmss") + "_" + fileName;

                        var objFolder = folderService.GetByDirName("DocumentLibrary/" + path);
                        if (objFolder != null)
                        {
                            var docLeaf = documentService.GetSpecificDocument(objFolder.ID, fileName);
                            if (docLeaf == null)
                            {
                                var objDoc = new Document()
                                {
                                    Name = fileName,
                                    DocIndex = 1,
                                    CreatedDate = DateTime.Now,
                                    IsLeaf = true,
                                    IsDelete = false
                                };
                                objDoc.RevisionFileName = fileName;
                                objDoc.FilePath = "/DocumentLibrary/" + path + "/" + fileName;
                                objDoc.RevisionFilePath = "/DocumentLibrary/RevisionHistory/" + revisionFileName;
                                objDoc.FileExtension = fileExt;
                                objDoc.FileExtensionIcon = fileIcon.ContainsKey(fileExt.ToLower())
                                                               ? fileIcon[fileExt.ToLower()]
                                                               : "images/otherfile.png";
                                objDoc.FileNameOriginal = fileName;
                                objDoc.DirName = "DocumentLibrary/" + path;

                                objDoc.FolderID = objFolder.ID;
                                objDoc.CategoryID = objFolder.CategoryID;
                                objDoc.IsActive = false;
                                // Copy to revision folder
                                var revisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                                                   + "DocumentLibrary/RevisionHistory/";

                                ////var tempFile = new FileInfo(e.FullPath);
                                ////tempFile.CopyTo(revisionPath + objDoc.RevisionFileName, true);

                                ////File.Copy(e.FullPath, revisionPath + objDoc.RevisionFileName, true);

                                this.documentService.Insert(objDoc);

                                this.EventLog.WriteEntry("Create document: $" + fileName + "$ Path: " + objDoc.DirName);
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                EventLog.WriteEntry(exception.Message);
            }
        }

        private void fsw_TotalChanged(object sender, FileSystemEventArgs e)
        {
            try
            {
                var docFileInfo = new FileInfo(e.FullPath);
                if (this.isAction && e.FullPath.IndexOf(@"RevisionHistory") == -1
                    && e.Name.LastIndexOf(".") != -1)
                    //&& File.Exists(e.FullPath))
                {
                    var fullPath = e.Name;
                    var fileExt = fullPath.Substring(
                        fullPath.LastIndexOf(".") + 1, fullPath.Length - fullPath.LastIndexOf(".") - 1);

                    if (fileExtensions.Contains(fileExt.ToLower()))
                    {
                        folderService = new FolderService();
                        documentService = new DocumentService();
                        
                        var lastPosition = fullPath.LastIndexOf(@"\");
                        var fileName = fullPath.Substring(lastPosition + 1, fullPath.Length - lastPosition - 1);
                        var path = "DocumentLibrary/" + fullPath.Substring(0, lastPosition).Replace(@"\", "/");
                        var revisionFileName = DateTime.Now.ToString("ddMMyyhhmmss") + "_" + fileName;

                        var objFolder = folderService.GetByDirName(path);
                        if (objFolder != null && !fileName.Contains("~$"))
                        {

                            Document objDoc;
                            switch (e.ChangeType)
                            {
                                case WatcherChangeTypes.Changed:
                                    var docLeafChanged = documentService.GetSpecificDocument(objFolder.ID, fileName);
                                    if (docLeafChanged == null)
                                    {
                                        objDoc = new Document()
                                        {
                                            Name = fileName,
                                            DocIndex = 1,
                                            CreatedDate = DateTime.Now,
                                            IsLeaf = true,
                                            IsDelete = false
                                        };
                                        objDoc.RevisionFileName = fileName;
                                        objDoc.FilePath = "/" + path + "/" + fileName;
                                        objDoc.RevisionFilePath = "/DocumentLibrary/RevisionHistory/" + revisionFileName;
                                        objDoc.FileExtension = fileExt;
                                        objDoc.FileExtensionIcon = fileIcon.ContainsKey(fileExt.ToLower())
                                                                       ? fileIcon[fileExt.ToLower()]
                                                                       : "images/otherfile.png";
                                        objDoc.FileNameOriginal = fileName;
                                        objDoc.DirName = path;

                                        objDoc.FolderID = objFolder.ID;
                                        objDoc.CategoryID = objFolder.CategoryID;
                                        objDoc.IsActive = false;
                                        objDoc.ModifiedDateDocFile = docFileInfo.LastWriteTime;
                                        this.documentService.Insert(objDoc);

                                        this.EventLog.WriteEntry("Create document: $" + fileName + "$ Path: " + objDoc.DirName);
                                    }
                                    break;
                                case WatcherChangeTypes.Created:
                                    var docLeafCreated = documentService.GetSpecificDocument(objFolder.ID, fileName);
                                    if (docLeafCreated == null)
                                    {
                                        objDoc = new Document()
                                        {
                                            Name = fileName,
                                            DocIndex = 1,
                                            CreatedDate = DateTime.Now,
                                            IsLeaf = true,
                                            IsDelete = false
                                        };
                                        objDoc.RevisionFileName = fileName;
                                        objDoc.FilePath = "/" + path + "/" + fileName;
                                        objDoc.RevisionFilePath = "/DocumentLibrary/RevisionHistory/" + revisionFileName;
                                        objDoc.FileExtension = fileExt;
                                        objDoc.FileExtensionIcon = fileIcon.ContainsKey(fileExt.ToLower())
                                            ? fileIcon[fileExt.ToLower()]
                                            : "images/otherfile.png";
                                        objDoc.FileNameOriginal = fileName;
                                        objDoc.DirName = path;

                                        if (objFolder != null)
                                        {
                                            objDoc.FolderID = objFolder.ID;
                                            objDoc.CategoryID = objFolder.CategoryID;
                                        }

                                        objDoc.IsActive = false;
                                        objDoc.ModifiedDateDocFile = docFileInfo.LastWriteTime;
                                        this.documentService.Insert(objDoc);
                                        this.EventLog.WriteEntry("Create document: $" + fileName + "$ Path: " +
                                                                 objDoc.DirName);
                                    }
                                    break;
                                case WatcherChangeTypes.Deleted:
                                    var listDocDelete = this.documentService.GetSpecificDocument(fileName, path);
                                    foreach (var document in listDocDelete)
                                    {
                                        document.IsDelete = true;
                                        documentService.Update(document);
                                    }

                                    this.EventLog.WriteEntry("Delete file: $" + e.Name + "$ Path: " + path);
                                    break;
                            }
                        }
                    }
                }
                ////else if (Directory.Exists(e.FullPath))
                ////{

                ////    folderService = new FolderService();
                ////    documentService = new DocumentService();
                ////    groupDataPermissionService = new GroupDataPermissionService();
                ////    userDataPermissionService = new UserDataPermissionService();

                ////    var folderName = e.Name.Substring(e.Name.LastIndexOf(@"\") + 1,
                ////        e.Name.Length - e.Name.LastIndexOf(@"\") - 1);

                ////    switch (e.ChangeType)
                ////    {
                ////        case WatcherChangeTypes.Created:
                ////            var parentDirName = "DocumentLibrary/" + e.Name.Substring(0, e.Name.LastIndexOf(@"\")).Replace(@"\", "/");
                ////            var parentFolder = folderService.GetByDirName(parentDirName);
                ////            if (parentFolder != null)
                ////            {
                ////                var folder = new Folder()
                ////                {
                ////                    Name = folderName,
                ////                    Description = folderName,
                ////                    CategoryID = parentFolder.CategoryID,
                ////                    ParentID = parentFolder.ID,
                ////                    DirName = parentFolder.DirName + "/" + folderName,
                ////                    CreatedBy = 1,
                ////                    CreatedDate = DateTime.Now
                ////                };

                ////                var parentFolderGroupPermission =
                ////                this.groupDataPermissionService.GetAllByFolder(parentFolder.ID.ToString());
                ////                var parentFolderUserPermission =
                ////                    this.userDataPermissionService.GetAllByFolder(parentFolder.ID);

                ////                var folId = this.folderService.Insert(folder);

                ////                var groupPermission =
                ////                    parentFolderGroupPermission.Select(
                ////                        t =>
                ////                        new GroupDataPermission()
                ////                        {
                ////                            RoleId = t.RoleId,
                ////                            FolderIdList = folId.ToString(),
                ////                            CategoryIdList = t.CategoryIdList,
                ////                            IsFullPermission = t.IsFullPermission
                ////                        }).ToList();
                ////                this.groupDataPermissionService.AddGroupDataPermissions(groupPermission);

                ////                var userPermission = parentFolderUserPermission.Select(t => new UserDataPermission()
                ////                {
                ////                    UserId = t.UserId,
                ////                    RoleId = t.RoleId,
                ////                    FolderId = folId,
                ////                    CategoryId = t.CategoryId,
                ////                    IsFullPermission = t.IsFullPermission
                ////                }).ToList();
                ////                this.userDataPermissionService.AddUserDataPermissions(userPermission);
                ////            }
                ////            break;

                ////        case WatcherChangeTypes.Deleted:
                ////            break;
                ////    }
                ////}
            }
            catch (Exception exception)
            {
                EventLog.WriteEntry(exception.Message);
            }
        }

        /// <summary>
        /// The fsw_ created.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void fsw_Created(object sender, FileSystemEventArgs e)
        {
            try
            {
                if (this.isAction && e.FullPath.IndexOf(@"RevisionHistory", StringComparison.Ordinal) == -1 
                    && e.Name.LastIndexOf(".", StringComparison.Ordinal) != -1)
                {
                    var fullPath = e.Name;
                    var fileExt = fullPath.Substring(
                        fullPath.LastIndexOf(".") + 1, fullPath.Length - fullPath.LastIndexOf(".") - 1);

                    if (fileExtensions.Contains(fileExt.ToLower()))
                    {
                        var lastPosition = fullPath.LastIndexOf(@"\");
                        var fileName = fullPath.Substring(lastPosition + 1, fullPath.Length - lastPosition - 1);
                        var path = fullPath.Substring(0, lastPosition).Replace(@"\", "/");
                        var revisionFileName = DateTime.Now.ToString("ddMMyyhhmmss") + "_" + fileName;

                        var objFolder = this.folderService.GetByDirName("DocumentLibrary/" + path);
                        
                        var objDoc = new Document()
                            {
                                Name = fileName,
                                DocIndex = 1,
                                CreatedDate = DateTime.Now,
                                IsLeaf = true,
                                IsDelete = false
                            };
                        objDoc.RevisionFileName = fileName;
                        objDoc.FilePath = "/DocumentLibrary/" + path + "/" + fileName;
                        objDoc.RevisionFilePath = "/DocumentLibrary/RevisionHistory/" + revisionFileName;
                        objDoc.FileExtension = fileExt;
                        objDoc.FileExtensionIcon = fileIcon.ContainsKey(fileExt.ToLower())
                                                       ? fileIcon[fileExt.ToLower()]
                                                       : "images/otherfile.png";
                        objDoc.FileNameOriginal = fileName;
                        objDoc.DirName = "DocumentLibrary/" + path;

                        if (objFolder != null)
                        {
                            objDoc.FolderID = objFolder.ID;
                            objDoc.CategoryID = objFolder.CategoryID;
                        }

                        // Copy to revision folder
                        var revisionPath = e.FullPath.Substring(0, e.FullPath.IndexOf("DocumentLibrary"))
                                           + "DocumentLibrary/RevisionHistory/";

                        ////var tempFile = new FileInfo(e.FullPath);
                        ////tempFile.CopyTo(revisionPath + objDoc.RevisionFileName, true);

                        ////File.Copy(e.FullPath, revisionPath + objDoc.RevisionFileName, true);

                        objDoc.IsActive = false;
                        this.documentService.Insert(objDoc);
                        this.EventLog.WriteEntry("Create document: $" + fileName + "$ Path: " + objDoc.DirName);
                        ////Console.WriteLine("Created: FileName - {0}, ChangeType - {1}", e.Name, e.ChangeType);
                    }
                }
            }
            catch (Exception exception)
            {
                EventLog.WriteEntry(exception.Message);
            }
        }
    }
}
