﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Data.SqlClient;
using System.Drawing;
using System.ServiceProcess;
using EDMs.Web.Utilities;

namespace EDMs.Web
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// Class customer
    /// </summary>
    public partial class SyncConfigPage : Page
    {

        private readonly SyncConfigService syncConfigService = new SyncConfigService();

        /// <summary>
        /// The permission service.
        /// </summary>
        private readonly PermissionService permissionService = new PermissionService();

        /// <summary>
        /// The group data permission service.
        /// </summary>
        private readonly GroupDataPermissionService groupDataPermissionService = new GroupDataPermissionService();

        /// <summary>
        /// The category service.
        /// </summary>
        private readonly CategoryService categoryService = new CategoryService();

        private readonly UserDataPermissionService userDataPermissionService = new UserDataPermissionService();

        private List<int> AdminGroup
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("GroupAdminList").Split(',').Select(t => Convert.ToInt32(t)).ToList();
            }
        }
        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var categoryPermission = this.groupDataPermissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault()).Select(t => t.CategoryIdList).ToList();
                categoryPermission = categoryPermission.Union(this.userDataPermissionService.GetByUserId(UserSession.Current.User.Id).Select(t => t.CategoryId.ToString())).Distinct().ToList();
                var listCategory = this.AdminGroup.Contains(UserSession.Current.RoleId)
                                    ? this.categoryService.GetAll()
                                    : this.categoryService.GetAll().Where(t => categoryPermission.Any(x => x == t.ID.ToString())).ToList();
                if (listCategory.Any())
                {
                    foreach (var category in listCategory)
                    {
                        category.ParentId = -1;
                    }

                    listCategory.Insert(0, new Category() { ID = -1, Name = "DOCUMENT GROUP" });

                    this.radPbCategories.DataSource = listCategory;
                    this.radPbCategories.DataFieldParentID = "ParentId";
                    this.radPbCategories.DataFieldID = "Id";
                    this.radPbCategories.DataValueField = "Id";
                    this.radPbCategories.DataTextField = "Name";
                    this.radPbCategories.DataBind();
                    this.radPbCategories.Items[0].Expanded = true;

                    foreach (RadPanelItem item in this.radPbCategories.Items[0].Items)
                    {
                        item.ImageUrl = @"Images/category2.png";
                        item.NavigateUrl = "DocumentsHome.aspx?doctype=" + item.Value;
                    }
                }

                this.LoadListPanel();
                this.LoadSystemPanel();

                this.LoadSyncConfigInfo();
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                
            }
        }


        protected void CustomerMenu_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            // Delete old sync config
            var configList = this.syncConfigService.GetAll();
            foreach (var syncConfig in configList)
            {
                this.syncConfigService.Delete(syncConfig);
            }

            // Add new sync config
            var syncConfigObj = new SyncConfig();
            var connstr = @"Data Source=" + this.txtServerName.Text.Trim() + ";Initial Catalog=BDPOC_DOCLIB;User ID=" +
                         this.txtUserName.Text.Trim() + ";Password=" + this.txtPass.Text.Trim() + ";";
            
            syncConfigObj.ConnectToServer = connstr;
            syncConfigObj.InstanceName = this.txtServerName.Text.Trim();
            syncConfigObj.UserName = this.txtUserName.Text.Trim();
            syncConfigObj.Password = this.txtPass.Text.Trim();
            
            if (rbtnRealTime.Checked)
            {
                syncConfigObj.Type = 1;
                syncConfigObj.Config = this.txtRealTimeValue.Value.ToString() + "$" + this.ddlRealTimeType.SelectedValue;
            }
            else
            {
                syncConfigObj.Type = 2;
                if (this.ddlEveryDayHour.SelectedValue != "0")
                {
                    syncConfigObj.Config = "Day$" + this.ddlEveryDayHour.SelectedValue;
                }
                else if (this.ddlEveryWeekDay.SelectedValue != "0")
                {
                    syncConfigObj.Config = "Week$" + this.ddlEveryWeekDay.SelectedValue;
                }
                else if (this.ddlEveryMonthDay.SelectedValue != "0")
                {
                    syncConfigObj.Config = "Month$" + this.ddlEveryMonthDay.SelectedValue;
                }
            }

            this.syncConfigService.Insert(syncConfigObj);

            var syncService = new ServiceController("SyncDocLibDataToOffShore");
            if (Utility.ServiceIsAvailable("SyncDocLibDataToOffShore"))
            {
                syncService.Stop();
                syncService.Start();
            }
        }

        private void LoadListPanel()
        {
            var listId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("ListID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), listId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "LIST" });

                this.radPbList.DataSource = permissions;
                this.radPbList.DataFieldParentID = "ParentId";
                this.radPbList.DataFieldID = "Id";
                this.radPbList.DataValueField = "Id";
                this.radPbList.DataTextField = "MenuName";
                this.radPbList.DataBind();
                this.radPbList.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbList.Items[0].Items)
                {
                    item.ImageUrl = @"Images/listmenu.png";
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                }
            }
        }

        private void LoadSystemPanel()
        {
            var systemId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("SystemID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), systemId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "SYSTEM" });

                this.radPbSystem.DataSource = permissions;
                this.radPbSystem.DataFieldParentID = "ParentId";
                this.radPbSystem.DataFieldID = "Id";
                this.radPbSystem.DataValueField = "Id";
                this.radPbSystem.DataTextField = "MenuName";
                this.radPbSystem.DataBind();
                this.radPbSystem.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbSystem.Items[0].Items)
                {
                    item.ImageUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Icon;
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                    if (item.Text == "Sync Offshore Data Config")
                    {
                        item.Selected = true;
                    }
                }
            }
        }

        protected void btnCheckConnection_OnClick(object sender, EventArgs e)
        {
            //Data Source=RNDDEPT\MSSQLSERVER2012;Initial Catalog=BDPOC_DOCLIB;User ID=sa;Password=qwe123QWE!@#;
            var connstr = @"Data Source=" + this.txtServerName.Text.Trim() + ";Initial Catalog=BDPOC_DOCLIB;User ID=" +
                         this.txtUserName.Text.Trim() + ";Password=" + this.txtPass.Text.Trim() + ";";
            var conn = new SqlConnection(connstr);
            try
            {
                conn.Open();
                conn.Close();
                this.lblConnectionStatus.Visible = true;
                this.lblConnectionStatus.ForeColor = Color.Blue;
                this.lblConnectionStatus.Text = "Connect success to Offshore DB server.";

            }
            catch (Exception)
            {
                this.lblConnectionStatus.Visible = true;
                this.lblConnectionStatus.ForeColor = Color.Red;
                this.lblConnectionStatus.Text = "Can't connect to Offshore DB server.";
            }
        }

        private void LoadSyncConfigInfo()
        {
            var syncConfigObj = this.syncConfigService.GetAll().FirstOrDefault();
            if (syncConfigObj != null)
            {
                this.txtServerName.Text = syncConfigObj.InstanceName;
                this.txtUserName.Text = syncConfigObj.UserName;
                this.txtPass.Text = syncConfigObj.Password;

                if (syncConfigObj.Type == 1)
                {
                    this.rbtnRealTime.Checked = true;
                    var arrData = syncConfigObj.Config.Split('$');
                    if (arrData.Any())
                    {
                        this.txtRealTimeValue.Value = Convert.ToDouble(arrData[0]);
                        this.ddlRealTimeType.SelectedValue = arrData[1];
                    }
                }
                else if (syncConfigObj.Type == 2)
                {
                    this.rbtnSchedule.Checked = true;
                    var arrData = syncConfigObj.Config.Split('$');
                    if (arrData.Any())
                    {
                        switch (arrData[0])
                        {
                            case "Day":
                                this.ddlEveryDayHour.SelectedValue = arrData[1];
                                break;
                            case "Week":
                                this.ddlEveryWeekDay.SelectedValue = arrData[1];
                                break;
                            case "Month":
                                this.ddlEveryMonthDay.SelectedValue = arrData[1];
                                break;
                        }
                    }
                }
            }
        }

        protected void rbtnRealTime_OnCheckedChanged(object sender, EventArgs e)
        {
            if (this.rbtnRealTime.Checked)
            {
                this.ddlEveryDayHour.SelectedValue = "0";
                this.ddlEveryDayHour.Enabled = false;

                this.ddlEveryMonthDay.SelectedValue = "0";
                this.ddlEveryMonthDay.Enabled = false;

                this.ddlEveryWeekDay.SelectedValue = "0";
                this.ddlEveryWeekDay.Enabled = false;

                this.txtRealTimeValue.Value = null;
                this.txtRealTimeValue.ReadOnly = false;
            }
            else if (this.rbtnSchedule.Checked)
            {
                this.txtRealTimeValue.Value = null;
                this.txtRealTimeValue.ReadOnly = true;

                this.ddlEveryDayHour.SelectedValue = "0";
                this.ddlEveryDayHour.Enabled = true;

                this.ddlEveryMonthDay.SelectedValue = "0";
                this.ddlEveryMonthDay.Enabled = true;

                this.ddlEveryWeekDay.SelectedValue = "0";
                this.ddlEveryWeekDay.Enabled = true;
            }
        }

        protected void ddlEveryDayHour_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.ddlEveryMonthDay.SelectedValue = "0";
            this.ddlEveryMonthDay.Enabled = false;

            this.ddlEveryWeekDay.SelectedValue = "0";
            this.ddlEveryWeekDay.Enabled = false;
        }

        protected void ddlEveryWeekDay_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.ddlEveryDayHour.SelectedValue = "0";
            this.ddlEveryDayHour.Enabled = false;

            this.ddlEveryMonthDay.SelectedValue = "0";
            this.ddlEveryMonthDay.Enabled = false;
        }

        protected void ddlEveryMonthDay_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.ddlEveryWeekDay.SelectedValue = "0";
            this.ddlEveryWeekDay.Enabled = false;

            this.ddlEveryDayHour.SelectedValue = "0";
            this.ddlEveryDayHour.Enabled = false;
        }
    }
}

