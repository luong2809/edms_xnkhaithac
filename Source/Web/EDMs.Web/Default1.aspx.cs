﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.Calendar;
using EDMs.Business.Services;
using EDMs.Data.Entities;
using EDMs.Web.Utilities.Sessions;

namespace EDMs.Web
{
    using System.Diagnostics.CodeAnalysis;

    public partial class _Default1 : Page
    {
        /// <summary>
        /// The _app service.
        /// </summary>
        private readonly AppointmentService _appService;

        /// <summary>
        /// The _resource service.
        /// </summary>
        private readonly ResourceService _resourceService;

        /// <summary>
        /// The resource for room.
        /// </summary>
        private const string resourceForRoom = "Room";

        /// <summary>
        /// The resource for doctor.
        /// </summary>
        private const string resourceForDoctor = "Doctor";

        /// <summary>
        /// The resource for service type.
        /// </summary>
        private const string resourceForServiceType = "ServiceType";

        /// <summary>
        /// The position panel bar item for room.
        /// </summary>
        private const int positionPanelBarItemForRoom = 0;

        /// <summary>
        /// The position panel bar item for doctor.
        /// </summary>
        private const int positionPanelBarItemForDoctor = 1;

        /// <summary>
        /// The position panel bar item for service type.
        /// </summary>
        private const int positionPanelBarItemForServiceType = 2;

        /// <summary>
        /// The list of room.
        /// </summary>
        private List<Room> ListOfRoom;

        /// <summary>
        /// The list of resource.
        /// </summary>
        private List<EDMs.Data.Entities.Resource> ListOfResource;

        /// <summary>
        /// The current user login id.
        /// </summary>
        protected int currentUserLoginId = 0;

        public _Default1()
        {
            _appService = new AppointmentService();
            _resourceService = new ResourceService();
        }

        protected void Page_Init(object sender, EventArgs e)
        {

            this.currentUserLoginId = UserSession.Current.User.Id;

            RadScheduler1.DataKeyField = "Id";
            RadScheduler1.DataSubjectField = "SetDisplayName";
            RadScheduler1.DataReminderField = "Reminder";
            RadScheduler1.DataStartField = "Start";
            RadScheduler1.DataEndField = "End";

            RadScheduler1.SelectedDate = DateTime.Today;
            RadCalendar1.SelectedDate = DateTime.Today;

            // if (!UserSession.Current.IsResource)
            // {
            // change Scheduler View: Days/Weeks/Months
            RadScheduler1.SelectedView = SchedulerViewType.DayView;

            // 1. group by ?
            RadScheduler1.GroupBy = resourceForRoom;
            RadScheduler1.GroupingDirection = GroupingDirection.Horizontal;

            // Visible group by at left panel
            // RadPanelItem item;
            // for (int i = PanelBar1.Items.Count - 1; i >= 0; i--)
            // {
            // item = PanelBar1.Items[i];
            // item.Expanded = true;

            // if (item.Value == this.resourceForRoom)
            // {
            // item.Visible = false;
            // }
            // }
            if (btnViewByDoctor.Checked)
            {
                PanelBar1.Items[positionPanelBarItemForDoctor].Visible = true;
                PanelBar1.Items[positionPanelBarItemForDoctor].Expanded = true;
                PanelBar1.Items[positionPanelBarItemForRoom].Visible = false;
                // 1. group by ?
                RadScheduler1.GroupBy = resourceForDoctor;
                RadScheduler1.GroupingDirection = GroupingDirection.Horizontal;
            }
            else
                PanelBar1.Items[positionPanelBarItemForDoctor].Visible = false;

            // Tan.Le Add - Từ từ implement phần ServiceType (multiple service type khó -> ẩn)
            PanelBar1.Items[positionPanelBarItemForServiceType].Visible = false;

            // }
            // if (PanelBar1.Items.Count > 0)
            // {
            // PanelBar1.Items[0].Expanded = true;
            // }
            PanelBar1.ExpandMode = PanelBarExpandMode.FullExpandedItem;

            RadScheduler1.ReminderDismiss += RadScheduler1_ReminderDismiss;
            RadScheduler1.ReminderSnooze += RadScheduler1_ReminderSnooze;
        }

        protected void RadScheduler1_ReminderSnooze(object sender, ReminderSnoozeEventArgs e)
        {
            var apptInfo = _appService.GetByID(int.Parse(e.Appointment.ID.ToString()));

            apptInfo.ReminderId = e.SnoozeMinutes;
            apptInfo.Reminder = e.Reminder.ToString();

            apptInfo.UpdateBy = UserSession.Current.User.Id;
            apptInfo.LastUpdate = DateTime.Now;

            // add Reminder to Appointment
            _appService.Update(apptInfo);

            // reload Scheduler after update success
            this.ReloadSchedule();
        }

        protected void RadScheduler1_ReminderDismiss(object sender, ReminderDismissEventArgs e)
        {
            var apptInfo = _appService.GetByID(int.Parse(e.ModifiedAppointment.ID.ToString()));

            apptInfo.ReminderId = null;
            apptInfo.Reminder = null;

            apptInfo.UpdateBy = UserSession.Current.User.Id;
            apptInfo.LastUpdate = DateTime.Now;

            // add Reminder to Appointment
            _appService.Update(apptInfo);

            // reload Scheduler after update success
            this.ReloadSchedule();
        }

        private void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindResourceRoom();
                BindResourceDoctor();

                // BindResourceServiceType();
                this.ReloadSchedule();

                
                //this.RadScheduler1.AppointmentTemplate = string.Empty;
            }
        }

        private void BindResourceRoom()
        {
            var roomService = new RoomService();
            var list = roomService.GetAll();

            var control = PanelBar1.Items[positionPanelBarItemForRoom].Items[0].FindControl("ListBoxRoom") as RadListBox;
            if (control != null)
            {
                control.DataSource = list;
                control.DataTextField = "Name";
                control.DataValueField = "Id";
                control.DataBind();
            }

            Session["ListOfRoom"] = list;
            
            // make all items will check  [depend  on current user login, it's wil auto check Room]
            foreach (RadListBoxItem item in control.Items)
            {
                item.Checked = true;
            }

            BindResourceRoomToScheduler();
        }
        
        private void BindResourceDoctor()
        {
            var resourceService = new ResourceService();
            var list = resourceService.FindByIsResource(true); // get Bac si

            var control = PanelBar1.Items[positionPanelBarItemForDoctor].Items[0].FindControl("ListBoxResource") as RadListBox;
            if (control != null)
            {
                control.DataSource = list;
                control.DataTextField = "FullName";
                control.DataValueField = "Id";
                control.DataBind();
            }

            Session["ListOfResource"] = list;

            // make all items will check [depend  on current user login, it's wil auto check Doctor]
            if (UserSession.Current.IsResource)
            {
                foreach (RadListBoxItem item in control.Items)
                {
                    if (UserSession.Current.User.IsAdmin != null && UserSession.Current.User.IsAdmin.Value == true)
                    {
                        if (item != null)
                        {
                            item.Checked = true;
                        }
                    }
                    else
                    {
                        if (UserSession.Current.ResourceId.ToString() == item.Value)
                        {
                            item.Checked = true;
                        }
                    }
                }
            }
            else
            {
                foreach (RadListBoxItem item in control.Items)
                {
                    // if (UserSession.Current.User.IsAdmin != null && UserSession.Current.User.IsAdmin.Value == true)
                    item.Checked = true;
                }
            }

            BindResourceDoctorToScheduler();
        }

        private void BindResourceServiceType()
        {
            var serviceTypeService = new ServiceTypeService();
            var list = serviceTypeService.GetAll();

            var control = PanelBar1.Items[positionPanelBarItemForServiceType].Items[0].FindControl("ListBoxServiceType") as RadListBox;
            if (control != null)
            {
                control.DataSource = list;
                control.DataTextField = "Name";
                control.DataValueField = "Id";
                control.DataBind();

                // make all items will check  [depend  on current user login, it's wil auto check Dich Vu]
                foreach (RadListBoxItem item in control.Items)
                {
                    item.Checked = true;
                }
            }

            var services = new Telerik.Web.UI.ResourceType();
            services.DataSource = list;
            services.ForeignKeyField = "ServiceTypeId";
            services.KeyField = "Id";
            services.TextField = "Name";
            services.Name = resourceForServiceType;
            RadScheduler1.ResourceTypes.Add(services);

            // mapping Resource with style resource scheduler
            foreach (var item in list)
            {
                var mapping = new ResourceStyleMapping();
                mapping.Type = resourceForServiceType;
                mapping.Key = item.Id.ToString();
                mapping.BorderColor = System.Drawing.ColorTranslator.FromHtml(item.Color);
                mapping.ApplyCssClass = "ResourceServiceType";
                RadScheduler1.ResourceStyles.Add(mapping);
            }
        }

        protected void RadScheduler1_NavigationComplete(object sender, SchedulerNavigationCompleteEventArgs e)
        {
            RadCalendar1.FocusedDate = RadScheduler1.SelectedDate;
            RadCalendar1.SelectedDate = RadScheduler1.SelectedDate;
            this.ReloadSchedule();
        }

        protected void RadCalendar1_DefaultViewChanged(object sender, DefaultViewChangedEventArgs e)
        {
            this.ReloadSchedule();
        }

        protected void RadCalendar1_SelectionChanged(object sender, SelectedDatesEventArgs e)
        {
            var selectedDate = (DateTime)RadCalendar1.SelectedDate;
            var minDate = new DateTime(2010, 1, 1);

            if (selectedDate < minDate)
            {
                selectedDate = DateTime.Now;
            }

            RadScheduler1.SelectedDate = selectedDate;
            this.ReloadSchedule();
        }

        protected void RadScheduler1_AppointmentDataBound(object sender, SchedulerEventArgs e)
        {
            // e.Appointment.ToolTip = e.Appointment.Attributes["Code"] + " - " + e.Appointment.Attributes["SetDisplayName"];
            e.Appointment.Visible = false;

            // Room
            // because Room already remove out PanelBar1 control
            if (UserSession.Current.IsResource)
            {
                var listBoxRoom = (PanelBar1.Items[positionPanelBarItemForRoom].Items[0].FindControl("ListBoxRoom") as RadListBox);
                foreach (RadListBoxItem chkBox in listBoxRoom.CheckedItems)
                {
                    if (chkBox.Checked)
                    {
                        // Telerik.Web.UI.Resource userRes = e.Appointment.Resources.GetResource(this.resourceForRoom, int.Parse(chkBox.Value));
                        var userRes = RadScheduler1.Resources.GetResource(resourceForRoom, int.Parse(chkBox.Value));
                        if (userRes != null)
                        {
                            e.Appointment.Visible = true;
                        }
                    }
                }
            }


            // Resource Doctor
            var listBoxResource = PanelBar1.Items[positionPanelBarItemForDoctor].Items[0].FindControl("ListBoxResource") as RadListBox;
            foreach (RadListBoxItem chkBox in listBoxResource.CheckedItems)
            {
                if (chkBox.Checked)
                {
                    // Telerik.Web.UI.Resource userRes = e.Appointment.Resources.GetResource(this.resourceForDoctor, int.Parse(chkBox.Value));
                    var userRes = RadScheduler1.Resources.GetResource(resourceForDoctor, int.Parse(chkBox.Value));
                    if (userRes != null)
                    {
                        e.Appointment.Visible = true;
                    }
                }
            }

            // Resource Service
            var listBoxServiceType = PanelBar1.Items[positionPanelBarItemForServiceType].Items[0].FindControl("ListBoxServiceType") as RadListBox;
            foreach (RadListBoxItem chkBox in listBoxServiceType.CheckedItems)
            {
                if (chkBox.Checked)
                {
                    // Telerik.Web.UI.Resource userRes = e.Appointment.Resources.GetResource(this.resourceForServiceType, int.Parse(chkBox.Value));
                    var userRes = RadScheduler1.Resources.GetResource(resourceForServiceType, int.Parse(chkBox.Value));
                    if (userRes != null)
                    {
                        e.Appointment.Visible = true;
                    }
                }
            }
        }

        private static void FilterAppointment(Telerik.Web.UI.Appointment appointment, ICheckBoxControl checkBox, int resourceId)
        {
            if (appointment.Resources.GetResource("Name", resourceId) != null && checkBox.Checked)
            {
                appointment.Visible = true;
            }
        }

        protected void RadScheduler1_AppointmentDelete(object sender, SchedulerCancelEventArgs e)
        {
            try
            {
                var id = Convert.ToInt16(e.Appointment.ID);

                // Delete All Child Appt
                var listChildAppts = _appService.GetChildAppointmentByParentApppointmentId(id);
                if (listChildAppts.Any())
                {
                    foreach (var listChildAppt in listChildAppts)
                    {
                        _appService.Delete(listChildAppt.Id);
                    }
                }

                // Delete Parent
                _appService.Delete(id);

                // reload Scheduler after delete success
                this.ReloadSchedule();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void RadScheduler1_AppointmentUpdate(object sender, AppointmentUpdateEventArgs e)
        {
            try
            {
                var apptInfo = _appService.GetByID(int.Parse(e.Appointment.ID.ToString()));

                if (e.ModifiedAppointment.Attributes["NextCall"] != string.Empty && Convert.ToDateTime(e.ModifiedAppointment.Attributes["NextCall"]).ToString("MM/dd/yyyy") != "01/01/0001")
                {
                    apptInfo.NextCall = Convert.ToDateTime(e.ModifiedAppointment.Attributes["NextCall"]);
                }

                if (e.ModifiedAppointment.Attributes["LastCall"] != string.Empty && Convert.ToDateTime(e.ModifiedAppointment.Attributes["LastCall"]).ToString("MM/dd/yyyy") != "01/01/0001")
                {
                    apptInfo.LastCall = Convert.ToDateTime(e.ModifiedAppointment.Attributes["LastCall"]);
                }

                if (Convert.ToDateTime(e.ModifiedAppointment.Start).ToString("MM/dd/yyyy") != "01/01/0001")
                {
                    apptInfo.Start = Convert.ToDateTime(e.ModifiedAppointment.Start);
                    apptInfo.AppointmentDate = Convert.ToDateTime(e.ModifiedAppointment.Start);
                }

                if (Convert.ToDateTime(e.ModifiedAppointment.End).ToString("MM/dd/yyyy") != "01/01/0001")
                {
                    apptInfo.End = Convert.ToDateTime(e.ModifiedAppointment.End);
                }

                apptInfo.NumberCall = Convert.ToInt16(e.ModifiedAppointment.Attributes["NumberCall"]);
                apptInfo.Code = e.ModifiedAppointment.Attributes["Code"];
                apptInfo.SetDisplayName = e.ModifiedAppointment.Subject;
                apptInfo.PatientId = Convert.ToInt16(e.ModifiedAppointment.Attributes["PatientId"]);

                apptInfo.ServiceTypeId = e.ModifiedAppointment.Attributes["ServiceTypeId"];
                apptInfo.AppointmentStatusId = Convert.ToInt16(e.ModifiedAppointment.Attributes["AppointmentStatusId"]);

                if (e.ModifiedAppointment.Resources.Count > 0)
                {
                    var res = e.ModifiedAppointment.Resources.GetResourceByType(resourceForRoom);
                    if (res != null)
                    {
                        apptInfo.RoomId = Convert.ToInt16(e.ModifiedAppointment.Resources[0].Key);
                    }

                    res = e.ModifiedAppointment.Resources.GetResourceByType(resourceForDoctor);
                    if (res != null)
                    {
                        apptInfo.ResourceId = Convert.ToInt16(e.ModifiedAppointment.Resources[1].Key);
                    }
                }
                else
                {
                    apptInfo.ResourceId = Convert.ToInt16(e.ModifiedAppointment.Attributes["ResourceId"]);
                }

                // new
                if (!string.IsNullOrEmpty(e.ModifiedAppointment.Attributes["ReminderId"]))
                {
                    apptInfo.ReminderId = Convert.ToInt16(e.ModifiedAppointment.Attributes["ReminderId"]);
                }

                if (!string.IsNullOrEmpty(e.ModifiedAppointment.Attributes["RoomId"]))
                {
                    apptInfo.RoomId = Convert.ToInt16(e.ModifiedAppointment.Attributes["RoomId"]);
                }

                apptInfo.Reminder = e.ModifiedAppointment.Reminders.ToString();
                apptInfo.RecurrenceRuleText = e.ModifiedAppointment.Attributes["RecurrenceRuleText"];
                apptInfo.Description = e.ModifiedAppointment.Attributes["Description"];
                apptInfo.ResourceOthers = e.ModifiedAppointment.Attributes["ResourceOthers"];

                apptInfo.UpdateBy = UserSession.Current.User.Id;
                apptInfo.LastUpdate = DateTime.Now;
                 
                // Xoa tat ca child Appt sau do insert lai
                this.ProcessAddDeleteChildAppointment(apptInfo);

                // add Reminder to Appointment
                _appService.Update(apptInfo);

                // reload Scheduler after update success
                this.ReloadSchedule();
                
            }
            catch (Exception exe)
            {
                Console.Write(exe.Message);
                throw exe;
            }
        }

        protected void RadScheduler1_AppointmentInsert(object sender, SchedulerCancelEventArgs e)
        {
            try
            {
                var apptInfo = new EDMs.Data.Entities.Appointment();

                if (e.Appointment.Attributes["NextCall"] != string.Empty && Convert.ToDateTime(e.Appointment.Attributes["NextCall"]).ToString("MM/dd/yyyy") != "01/01/0001")
                {
                    apptInfo.NextCall = Convert.ToDateTime(e.Appointment.Attributes["NextCall"]);
                }

                if (e.Appointment.Attributes["LastCall"] != string.Empty && Convert.ToDateTime(e.Appointment.Attributes["LastCall"]).ToString("MM/dd/yyyy") != "01/01/0001")
                {
                    apptInfo.LastCall = Convert.ToDateTime(e.Appointment.Attributes["LastCall"]);
                }

                if (Convert.ToDateTime(e.Appointment.Start).ToString("MM/dd/yyyy") != "01/01/0001")
                {
                    apptInfo.Start = Convert.ToDateTime(e.Appointment.Start);
                    apptInfo.AppointmentDate = Convert.ToDateTime(e.Appointment.Start);
                }

                if (Convert.ToDateTime(e.Appointment.End).ToString("MM/dd/yyyy") != "01/01/0001")
                {
                    apptInfo.End = Convert.ToDateTime(e.Appointment.End);
                }

                apptInfo.NumberCall = Convert.ToInt16(e.Appointment.Attributes["NumberCall"]);
                apptInfo.Code = e.Appointment.Attributes["Code"];
                apptInfo.SetDisplayName = e.Appointment.Subject;
                apptInfo.PatientId = Convert.ToInt16(e.Appointment.Attributes["PatientId"]);

                apptInfo.ServiceTypeId = e.Appointment.Attributes["ServiceTypeId"];
                apptInfo.AppointmentStatusId = Convert.ToInt16(e.Appointment.Attributes["AppointmentStatusId"]);
                apptInfo.ResourceId = Convert.ToInt16(e.Appointment.Attributes["ResourceId"]);

                // new
                if (!string.IsNullOrEmpty(e.Appointment.Attributes["ReminderId"]))
                {
                    apptInfo.ReminderId = Convert.ToInt16(e.Appointment.Attributes["ReminderId"]);
                }

                if (!string.IsNullOrEmpty(e.Appointment.Attributes["RoomId"]))
                {
                    apptInfo.RoomId = Convert.ToInt16(e.Appointment.Attributes["RoomId"]);
                }

                apptInfo.Reminder = e.Appointment.Reminders.ToString();
                apptInfo.RecurrenceRuleText = e.Appointment.Attributes["RecurrenceRuleText"];
                apptInfo.Description = e.Appointment.Attributes["Description"];

                apptInfo.LastUpdate = DateTime.Now;
                apptInfo.UpdateBy = UserSession.Current.User.Id;
                apptInfo.CreatedBy = apptInfo.UpdateBy;
                apptInfo.IsBlock = true; // đồng thời block lịch của họ lại để xem và quản lý. 

                // Khi tạo lịch hẹn có thể cho phép chọn những người liên quan, ví dụ: Y tá, tư vấn,... đồng thời block lịch của họ lại để xem và quản lý. 
                var resourceOthers = e.Appointment.Attributes["ResourceOthers"];
                if (!string.IsNullOrEmpty(resourceOthers))
                {
                    apptInfo.ResourceOthers = resourceOthers;
                    _appService.Insert(apptInfo); // insert main Appt

                    // after insert main Appt, insert child Appt
                    foreach (var resourceOther in resourceOthers.Split(','))
                    {
                        this.InsertChildAppointment(apptInfo, resourceOther);
                    }
                }
                else
                {
                    _appService.Insert(apptInfo);
                }

                // reload Scheduler after save success
                this.ReloadSchedule();
            }
            catch (Exception exe)
            {
                throw exe;
            }
        }

        /// <summary>
        /// The reload schedule.
        /// </summary>
        private void ReloadSchedule()
        {
            var selectedDate = (DateTime)RadCalendar1.SelectedDate;
            var oldDate = new DateTime(2010, 1, 1);

            if (selectedDate == null || selectedDate < oldDate)
            {
                selectedDate = DateTime.Now;
            }

            List<EDMs.Data.Entities.Appointment> listAppointment = _appService.GetAppointmentByMonth(selectedDate);

            RadScheduler1.DataSource = listAppointment;
            RadScheduler1.DataBind();
        }

        protected void ListBoxResource_ItemCheck(object sender, RadListBoxItemEventArgs e)
        {
            if (btnViewByDoctor.Checked)
                BindResourceDoctorToScheduler();
            else
                BindResourceRoomToScheduler();

            this.ReloadSchedule();
        }

        private void BindResourceDoctorToScheduler()
        {
            var control = (PanelBar1.Items[positionPanelBarItemForDoctor].Items[0].FindControl("ListBoxResource") as RadListBox);
            if (control.CheckedItems.Count == 0)
                return;

            ListOfResource = Utilities.Utility.DeepClone((List<EDMs.Data.Entities.Resource>)Session["ListOfResource"]);
            foreach (RadListBoxItem item in control.Items)
            {
                if (item.Checked == false)
                {
                    ListOfResource.RemoveAll(x => x.Id == int.Parse(item.Value));
                }
            }

            ResourceType resourceDoctor = RadScheduler1.ResourceTypes.SingleOrDefault(x => x.Name == resourceForDoctor);
            RadScheduler1.ResourceTypes.Remove(resourceDoctor);

            var doctor = new Telerik.Web.UI.ResourceType();
            doctor.DataSource = ListOfResource;
            doctor.ForeignKeyField = "ResourceId";
            doctor.KeyField = "Id";
            doctor.TextField = "FullName";
            doctor.Name = resourceForDoctor;
            RadScheduler1.ResourceTypes.Add(doctor);

            // mapping Resource with style resource scheduler
            foreach (var item in ListOfResource)
            {
                var mapping = new ResourceStyleMapping();
                mapping.Type = resourceForDoctor;
                mapping.Key = item.Id.ToString();
                mapping.BackColor = System.Drawing.ColorTranslator.FromHtml(item.Color);
                RadScheduler1.ResourceStyles.Add(mapping);
            }
        }

        private void BindResourceRoomToScheduler()
        {
            var control = (PanelBar1.Items[positionPanelBarItemForRoom].Items[0].FindControl("ListBoxRoom") as RadListBox);
            if (control.CheckedItems.Count == 0)
                return;

            ListOfRoom = Utilities.Utility.DeepClone((List<EDMs.Data.Entities.Room>)Session["ListOfRoom"]);
            //ListOfResource = Utilities.Utility.DeepClone((List<EDMs.Data.Entities.Resource>)Session["ListOfResource"]);
            foreach (RadListBoxItem item in control.Items)
            {
                if (item.Checked == false)
                {
                    ListOfRoom.RemoveAll(x => x.Id == int.Parse(item.Value));
                }
            }
            ResourceType resourceRoom = RadScheduler1.ResourceTypes.SingleOrDefault(x => x.Name == resourceForRoom);
            RadScheduler1.ResourceTypes.Remove(resourceRoom);

            var services = new Telerik.Web.UI.ResourceType();
            services.DataSource = ListOfRoom;
            services.ForeignKeyField = "RoomId";
            services.KeyField = "Id";
            services.TextField = "Name";
            services.Name = resourceForRoom;
            RadScheduler1.ResourceTypes.Add(services);

            // mapping Resource with style resource scheduler
            ResourceStyleMapping mapping;
            foreach (var item in ListOfRoom)
            {
                mapping = new ResourceStyleMapping();
                mapping.Type = resourceForRoom;
                mapping.Key = item.Id.ToString();
                //mapping.BackColor = System.Drawing.ColorTranslator.FromHtml(item.Color);
                RadScheduler1.ResourceStyles.Add(mapping);
            }
        }

        protected void RadScheduler1_ResourceHeaderCreated(object sender, ResourceHeaderCreatedEventArgs e)
        {
            Panel ResourceImageWrapper = e.Container.FindControl("ResourceImageWrapper") as Panel;
            ResourceImageWrapper.CssClass = "Resource" + e.Container.Resource.Key.ToString();
            //ResourceImageWrapper.BorderColor  =  e.Container.Resource.
            Label lblHeader = e.Container.FindControl("lblHeader") as Label;
            lblHeader.Text = e.Container.Resource.Text;
        }

        protected void btnViewByRoom_CheckedChanged(object sender, EventArgs e)
        {
            if (btnViewByRoom.Checked)
            {
                PanelBar1.Items[positionPanelBarItemForRoom].Visible = true;
                PanelBar1.Items[positionPanelBarItemForRoom].Expanded = true;
                PanelBar1.Items[positionPanelBarItemForDoctor].Visible = false;
                // 1. group by ?
                RadScheduler1.GroupBy = resourceForRoom;
                RadScheduler1.GroupingDirection = GroupingDirection.Horizontal;
            }
            else
            {
                PanelBar1.Items[positionPanelBarItemForRoom].Visible = false;
            }
            this.ReloadSchedule();
        }

        /// <summary>
        /// The btn view by doctor_ checked changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnViewByDoctor_CheckedChanged(object sender, EventArgs e)
        {
            if (btnViewByDoctor.Checked)
            {
                PanelBar1.Items[positionPanelBarItemForDoctor].Visible = true;
                PanelBar1.Items[positionPanelBarItemForDoctor].Expanded = true;
                PanelBar1.Items[positionPanelBarItemForRoom].Visible = false;
                // 1. group by ?
                RadScheduler1.GroupBy = resourceForDoctor;
                RadScheduler1.GroupingDirection = GroupingDirection.Horizontal;
            }
            else
            {
                PanelBar1.Items[positionPanelBarItemForDoctor].Visible = false;
            }
            this.ReloadSchedule();
        }

        /// <summary>
        /// The rad scheduler 1_ on appointment created.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadScheduler1_OnAppointmentCreated(object sender, AppointmentCreatedEventArgs e)
        {
            var lblAppointmentTemplate = (Label)e.Container.FindControl("lblAppointmentTemplate");
            var appt = e.Appointment;
            var templete = string.Empty;

            // Begin -----------------------------------------------------------------------
            templete = "<div>" + appt.Attributes["Code"] + "</div>";
            templete += "<div style='font-style: italic;'>" + appt.Attributes["SetDisplayName"] + "</div>";
            appt.ToolTip = appt.Attributes["Code"] + " - " + appt.Attributes["SetDisplayName"];

            // End -----------------------------------------------------------------

            // if (string.IsNullOrEmpty(appt.Attributes["ResourceOthers"]))
            // {
            // // for Tooltip
            // appt.ToolTip = appt.Attributes["Code"] + " - " + appt.Attributes["SetDisplayName"];

            // // create header Appt
            // templete = "<div>" + appt.Attributes["Code"] + "</div>";
            // templete += "<div style='font-style: italic;'>" + appt.Attributes["SetDisplayName"] + "</div>";
            // //templete += "<div style='font-style: italic;'>" + Convert.ToDateTime(appt.Attributes["Start"]).ToShortTimeString() + " - " + Convert.ToDateTime(appt.Attributes["End"]).ToShortTimeString() + "</div>";
            // }
            // else
            // {
            // var res = this._resourceService.GetByID(Convert.ToInt16(appt.Attributes["ResourceOthers"]));

            // if (res != null)
            // {
            // // create header Appt
            // templete = "Lịch hẹn '" + appt.Attributes["SetDisplayName"] + "' phải có sự tham gia của Y tá/Tư vấn viên: " + res.FullName;

            // // for Tooltip
            // appt.ToolTip = templete;
            // }
            // }
            lblAppointmentTemplate.Text = templete;
        }

        /// <summary>
        /// The process add delete child appointment.
        /// </summary>
        /// <param name="apptInfo">
        /// The appt Info.
        /// </param>
        private void ProcessAddDeleteChildAppointment(EDMs.Data.Entities.Appointment apptInfo)
        {
            var listApptChild = _appService.GetChildAppointmentByParentApppointmentId(apptInfo.Id);
            var resourceOthers = apptInfo.ResourceOthers;

            // Delete All childs
            if (listApptChild != null && listApptChild.Any())
            {
                foreach (var appointment in listApptChild)
                {
                    _appService.Delete(appointment.Id);
                }
            }

            // Insert childs
            foreach (var resourceOther in resourceOthers.Split(','))
            {
                this.InsertChildAppointment(apptInfo, resourceOther);
            }
        }

        /// <summary>
        /// The insert child appointment.
        /// </summary>
        /// <param name="parentApptInfo">
        /// The parent appt info.
        /// </param>
        /// <param name="resourceOther">
        /// The resource other.
        /// </param>
        private void InsertChildAppointment(EDMs.Data.Entities.Appointment parentApptInfo, string resourceOther = null)
        {
            var apptChild = new EDMs.Data.Entities.Appointment();

            if (!string.IsNullOrEmpty(resourceOther))
            {
                apptChild.ResourceId = Convert.ToInt16(resourceOther);
            }

            apptChild.ParentAppointmentId = parentApptInfo.Id; // keep parent ApptId

            apptChild.AppointmentDate = parentApptInfo.AppointmentDate;
            apptChild.AppointmentDuration = parentApptInfo.AppointmentDuration;
            apptChild.AppointmentStatusId = parentApptInfo.AppointmentStatusId;
            apptChild.CancelBy = parentApptInfo.CancelBy;
            apptChild.CancelDate = parentApptInfo.CancelDate;
            apptChild.CancelTime = parentApptInfo.CancelTime;
            apptChild.Code = parentApptInfo.Code;
            apptChild.Description = parentApptInfo.Description;
            apptChild.End = parentApptInfo.End;
            apptChild.IsApprove = parentApptInfo.IsApprove;
           
            apptChild.LastCall = parentApptInfo.LastCall;
            apptChild.NextCall = parentApptInfo.NextCall;
            apptChild.NumberCall = parentApptInfo.NumberCall;
            apptChild.PatientId = parentApptInfo.PatientId;
            apptChild.RecurrenceRuleText = parentApptInfo.RecurrenceRuleText;
            apptChild.Reminder = parentApptInfo.Reminder;
            apptChild.ReminderId = parentApptInfo.ReminderId;
            apptChild.RoomId = parentApptInfo.RoomId;
            apptChild.ServiceTypeId = parentApptInfo.ServiceTypeId;
            apptChild.SetDisplayName = parentApptInfo.SetDisplayName;
            apptChild.Start = parentApptInfo.Start;

            apptChild.LastUpdate = DateTime.Now;
            apptChild.UpdateBy = UserSession.Current.User.Id;
            apptChild.CreatedBy = apptChild.UpdateBy;

            apptChild.IsBlock = parentApptInfo.IsBlock;

            _appService.Insert(apptChild);
        }


    }
}