﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="EDMs.Web.Default" EnableViewState="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!--[if gte IE 8]>
        <style type="text/css">
            #ctl00_ContentPlaceHolder2_grdDocument_ctl00_Header{table-layout:auto !important;}
            #ctl00_ContentPlaceHolder2_grdDocument_ctl00{table-layout:auto !important;}
        </style>
    <![endif]-->

    <style type="text/css">
        /*Custom CSS of Grid documents for FF browser*/
        /*#ctl00_ContentPlaceHolder2_grdDocument_ctl00_Header{table-layout:auto !important;}
        #ctl00_ContentPlaceHolder2_grdDocument_ctl00{table-layout:auto !important;}*/
        /*End*/
        @-moz-document url-prefix() {
            #ctl00_ContentPlaceHolder2_grdDocument_ctl00_Header {
                table-layout: auto !important;
            }

            #ctl00_ContentPlaceHolder2_grdDocument_ctl00 {
                table-layout: auto !important;
            }
        }

        .RadGrid .rgRow td, .RadGrid .rgAltRow td, .RadGrid .rgEditRow td, .RadGrid .rgFooter td, .RadGrid .rgFilterRow td, .RadGrid .rgHeader, .RadGrid .rgResizeCol, .RadGrid .rgGroupHeader td {
            padding-left: 1px !important;
            padding-right: 2px !important;
            vertical-align: top;
        }

        .RadGrid .rgSelectedRow {
            background-image: none !important;
            background-color: coral !important;
        }

        /*Hide change page size control*/
        /*div.RadGrid .rgPager .rgAdvPart     
        {     
        display:none;        sssssss
        }*/

        a.tooltip {
            outline: none;
            text-decoration: none;
        }

            a.tooltip strong {
                line-height: 30px;
            }

            a.tooltip:hover {
                text-decoration: none;
            }

            a.tooltip span {
                z-index: 10;
                display: none;
                padding: 14px 20px;
                margin-top: -30px;
                margin-left: 5px;
                width: 240px;
                line-height: 16px;
            }

            a.tooltip:hover span {
                display: inline;
                position: absolute;
                color: #111;
                border: 1px solid #DCA;
                background: #fffAF0;
            }

        .callout {
            z-index: 20;
            position: absolute;
            top: 30px;
            border: 0;
            left: -12px;
        }

        /*CSS3 extras*/
        a.tooltip span {
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            -moz-box-shadow: 5px 5px 8px #CCC;
            -webkit-box-shadow: 5px 5px 8px #CCC;
            box-shadow: 5px 5px 8px #CCC;
        }

        .rgMasterTable {
            table-layout: auto;
        }

        .tableProperties{
            font-family: Arial, sans-serif;
            width:100%;
            color:#4c607a;
        }
        .tdProperties {
            font-size: 8pt;
        }

        .thProperties {
            font-size: 8.5pt;
             color:#4c607a;
        }
        #ctl00_ContentPlaceHolder2_radTreeFolder {
            overflow: visible !important;
        }

        #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_grdDocumentPanel, #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_divContainerPanel {
            height: 100% !important;
        }

        #ctl00_ContentPlaceHolder2_RadPageView1, #ctl00_ContentPlaceHolder2_RadPageView2,
        #ctl00_ContentPlaceHolder2_RadPageView3, #ctl00_ContentPlaceHolder2_RadPageView4,
        #ctl00_ContentPlaceHolder2_RadPageView5 {
            height: 100% !important;
        }

        #divContainerLeft {
            width: 25%;
            float: left;
            margin: 5px;
            height: 99%;
            border-right: 1px dotted green;
            padding-right: 5px;
        }

        #divContainerRight {
            width: 100%;
            float: right;
            margin-top: 5px;
            height: 99%;
        }

        .dotted {
            border: 1px dotted #000;
            border-style: none none dotted;
            color: #fff;
            background-color: #fff;
        }

        .exampleWrapper {
            width: 100%;
            height: 100%;
            /*background: transparent url(images/background.png) no-repeat top left;*/
            position: relative;
        }

        .tabStrip {
            position: absolute;
            top: 0px;
            left: 0px;
        }

        .multiPage {
            position: absolute;
            top: 30px;
            left: 0px;
            color: white;
            width: 100%;
            height: 100%;
        }

        /*Fix RadMenu and RadWindow z-index issue*/
        .radwindow {
            z-index: 8000 !important;
        }

        .TemplateMenu {
            z-index: 10;
            padding-bottom: 20px;
            padding-left: 223px;
        }

        .accordion dt a {
            letter-spacing: -0.03em;
            line-height: 1.2;
            margin: 0.5em auto 0.6em;
            padding: 0;
            text-align: left;
            text-decoration: none;
            display: block;
        }

        .accordion dt span {
            color: #085B8F;
            border-bottom: 2px solid #46A3D3;
            font-size: 1.5em;
            font-weight: bold;
            letter-spacing: -0.03em;
            line-height: 1.2;
            /*margin: 0.5em auto 0.6em;*/
            padding: 0;
            text-align: left;
            text-decoration: none;
            display: block;
        }
        .accordionLabel{
            border: none!important;
            font-size: 8.5pt !important;
             color:#4c607a !important;
        }
    </style>
    <telerik:RadPanelBar ID="radPbSearch" runat="server" Width="100%">
        <Items>
            <telerik:RadPanelItem Text="ADVANCE SEARCH" runat="server" Expanded="True" Width="100%">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Advance Search" ImageUrl="Images/search.gif" NavigateUrl="Search.aspx" />
                </Items>
            </telerik:RadPanelItem>
        </Items>
    </telerik:RadPanelBar>
    <telerik:RadPanelBar ID="radPbCategories" runat="server" Width="100%" />
    <telerik:RadPanelBar ID="RadPanelBar1" runat="server" Width="100%">
        <Items>
            <telerik:RadPanelItem Text="TRANSMITTAL" runat="server" Expanded="True" Width="100%" Visible="false">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Transmittal" ImageUrl="Images/Transmittal.png" NavigateUrl="Transmittal.aspx" />
                </Items>
            </telerik:RadPanelItem>
        </Items>
    </telerik:RadPanelBar>

    <telerik:RadPanelBar ID="radPbList" runat="server" Width="100%" />
    <telerik:RadPanelBar ID="radPbSystem" runat="server" Width="100%" />

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <telerik:RadSplitter ID="RadSplitter4" runat="server" Orientation="Horizontal" Width="100%">
        <telerik:RadPane ID="RadPane2" runat="server" Scrollable="false" Scrolling="None">
            <telerik:RadSplitter ID="Radsplitter3" runat="server" Orientation="Horizontal">
                <telerik:RadPane ID="Radpane4" runat="server" Scrolling="None">
                    <dl class="accordion">
                        <dt style="width: 100%;">
                            <span>New Documents:</span>
                        </dt>
                    </dl>
                    <telerik:RadGrid ID="grdDocument" runat="server" AllowPaging="True"
                        AutoGenerateColumns="False" CellPadding="0" CellSpacing="0"
                        GridLines="None" Height="65%"
                        OnNeedDataSource="grdDocument_OnNeedDataSource"
                        PageSize="100" Style="outline: none">
                        <GroupingSettings CaseSensitive="False"></GroupingSettings>
                        <MasterTableView
                            ClientDataKeyNames="ID" DataKeyNames="ID" EditMode="InPlace" Font-Size="8pt">
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <Columns>
                                <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />
                                <telerik:GridTemplateColumn AllowFiltering="false" UniqueName="DownloadColumn">
                                    <HeaderStyle Width="2%" />
                                    <ItemStyle HorizontalAlign="Center" Width="2%" />
                                    <ItemTemplate>
                                        <div runat="server" id="ViewForFolder" visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsFolder")) %>'>
                                            <asp:Image ID="Image1" runat="server"
                                                ImageUrl='<%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon") %>' />
                                        </div>
                                        <div runat="server" id="ViewForDoc" visible='<%# !Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsFolder")) %>'>
                                            <div runat="server" id="DocumentFile" visible='<%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon")!=null %>'>
                                                <asp:Image ID="CallLink" runat="server"
                                                    ImageUrl=' <%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon") != null && System.IO.File.Exists(Server.MapPath( DataBinder.Eval(Container.DataItem, "FilePath").ToString()))? DataBinder.Eval(Container.DataItem, "FileExtensionIcon") : "~/Images/error.png" %>'
                                                    ToolTip=' <%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon") != null && System.IO.File.Exists(Server.MapPath( DataBinder.Eval(Container.DataItem, "FilePath").ToString()))? "Download File" : "No File" %>'
                                                    Style="cursor: pointer;" AlternateText="Download document" />
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <%--3--%>
                                <telerik:GridTemplateColumn UniqueName="Name" HeaderText="File Name"
                                    DataField="Name" ShowFilterIcon="False" FilterControlWidth="97%" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                    <HeaderStyle Width="12%" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" Width="12%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblFileName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>'
                                            ForeColor="Blue" Style="cursor: pointer" />
                                        <asp:Image ID="newicon" runat="server" ImageUrl="Images/new.png" />
                                        <telerik:RadToolTip Skin="Simple" runat="server" ID="dirNameToolTip" RelativeTo="Element"
                                            AutoCloseDelay="20000" ShowDelay="0" Position="BottomRight" Visible="false"
                                            Width="600px" Height="70px" HideEvent="LeaveTargetAndToolTip" TargetControlID="lblFileName" IsClientID="False"
                                            Animation="Fade" Text='<%# "<b >Folder: </b>" + DataBinder.Eval(Container.DataItem, "DirName") %>'>
                                        </telerik:RadToolTip>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:HiddenField ID="Name" runat="server" Value='<%# Eval("Name") %>' />
                                        <asp:TextBox ID="txtName" runat="server" Width="100%"></asp:TextBox>
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>
                                <%--4--%>
                                <telerik:GridBoundColumn HeaderText="Document No." UniqueName="DocumentNumber"
                                    FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    DataField="DocumentNumber">
                                    <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                    <ItemStyle HorizontalAlign="Left" Width="10%" />
                                </telerik:GridBoundColumn>
                                <%--5--%>
                                <telerik:GridBoundColumn HeaderText="Title" UniqueName="Title"
                                    FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    DataField="Title">
                                    <HeaderStyle HorizontalAlign="Center" Width="12%" />
                                    <ItemStyle HorizontalAlign="Left" Width="12%" />
                                </telerik:GridBoundColumn>
                                <%--6--%>
                                <%-- <telerik:GridBoundColumn HeaderText="Revision" UniqueName="Revision" Visible="False"
                                    FilterControlWidth="95%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                    DataField="RevisionName">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                    <ItemStyle HorizontalAlign="Left" Width="5%"/>
                                </telerik:GridBoundColumn>--%>
                                <%--7--%>
                                <%--<telerik:GridBoundColumn DataField="StatusName" HeaderText="Status" UniqueName="Status" 
                                    FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" Visible="False" CurrentFilterFunction="Contains">
                                    <HeaderStyle HorizontalAlign="Center" Width="6%" />
                                    <ItemStyle HorizontalAlign="Left" Width="6%"/>
                                </telerik:GridBoundColumn>--%>
                                <%--8--%>
                                <%--  <telerik:GridBoundColumn DataField="DisciplineName" HeaderText="Discipline" UniqueName="Discipline" Visible="False"
                                    FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                    <HeaderStyle HorizontalAlign="Center" Width="6%" />
                                    <ItemStyle HorizontalAlign="Left" Width="6%"/>
                                </telerik:GridBoundColumn>--%>
                                <%--9--%>
                                <telerik:GridBoundColumn DataField="DocumentTypeName" HeaderText="Document Type" UniqueName="DocumentType"
                                    FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                    <HeaderStyle HorizontalAlign="Center" Width="9%" />
                                    <ItemStyle HorizontalAlign="Left" Width="9%" />
                                </telerik:GridBoundColumn>
                                <%--10--%>
                                <%-- <telerik:GridBoundColumn DataField="ReceivedFromName" HeaderText="Received From" Visible="False" UniqueName="ReceivedFrom" AllowFiltering="False">
                                    <HeaderStyle HorizontalAlign="Center" Width="6%" />
                                    <ItemStyle HorizontalAlign="Left" Width="6%"/>
                                </telerik:GridBoundColumn>--%>
                                <%--11--%>
                                <telerik:GridBoundColumn DataField="CreatedUser.FullName" HeaderText="Created By" UniqueName="CreatedUser" AllowFiltering="False">
                                    <HeaderStyle HorizontalAlign="Center" Width="8%" />
                                    <ItemStyle HorizontalAlign="Left" Width="8%" />
                                </telerik:GridBoundColumn>
                                <%--12--%>
                                <telerik:GridTemplateColumn HeaderText="Date" UniqueName="CreatedDate" AllowFiltering="False">
                                    <HeaderStyle HorizontalAlign="Center" Width="6%" />
                                    <ItemStyle HorizontalAlign="Center" Width="6%" />
                                    <ItemTemplate>
                                        <%# Eval("CreatedDate","{0:dd/MM/yyyy}") %>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <%--13--%>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings Selecting-AllowRowSelect="true" AllowColumnHide="True">
                            <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="500" UseStaticHeaders="True" />
                        </ClientSettings>
                    </telerik:RadGrid>
                    <dl class="accordion">
                      <div style="width:100%;">
                          <div style="width:73%; float:left">
                        <dt style="width:100%; ">
                            <span>Shared Document:</span>
                            <a> Is Coming</a>
                            <div>
                                <telerik:RadListView runat="server" ID="lbSharedDocument">
	                                <ItemTemplate>
		                                <a href='<%#Eval("FilePath")%>' target="_blank">
                                        <asp:Image ID="Image1" runat="server" ImageUrl='~/Images/documents.png' 
                                            Style="cursor: pointer; padding-left: 10px" AlternateText="Download document" /> 
                                            <%#Eval("Name")%>
                                        </a>
	                                </ItemTemplate>
                                </telerik:RadListView>
                            </div>
                        </dt>
                              </div>
                          <div style="width:25%; float:left; padding-left:15px;">
                            <dt style=" width:100%; ">
                            <span>Admin Infor:</span>
                                <asp:Panel ID="panleAdmin" runat="server">
                                <table class="tableProperties">
                                    <tr style="display:none;">
                                         <th class="thProperties" style="width:30%; text-align:right;">UserName:</th>
                                        <th class="thProperties" style="width:70%; text-align:left; font-weight:bold;">Admin</th>
                                    </tr>
                                      <tr style="padding-top:5px;">
                                         <th class="thProperties" style="width:30%;text-align:right;">FullName:</th>
                                        <th class="thProperties" style="width:70%;text-align:left; font-weight:bold;"><asp:Label CssClass="accordionLabel" ID="txtfullname" runat="server"></asp:Label></th>
                                    </tr>
                                     <tr style="padding-top:5px;">
                                         <th class="thProperties" style="width:30%; text-align:right;">Email:</th>
                                        <th class="thProperties" style="width:70%; text-align:left; font-weight:bold;"><asp:Label CssClass="accordionLabel" ID="txtemail" runat="server"></asp:Label></th>
                                    </tr>
                                     <tr style="padding-top:5px;">
                                         <th class="thProperties" style="width:30%; text-align:right;">Phone:</th>
                                        <th class="thProperties" style="width:70%; text-align:left; font-weight:bold;"><asp:Label CssClass="accordionLabel" ID="txtphone" runat="server"></asp:Label></th>
                                    </tr>
                                </table> </asp:Panel>
                            <div>
                               
                            </div>
                        </dt>
                              </div>

                      </div>
                    </dl>
                </telerik:RadPane>
            </telerik:RadSplitter>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <span style="display: none">
        <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer">
            <ClientEvents OnRequestStart="onRequestStart"></ClientEvents>
            <AjaxSettings>
            </AjaxSettings>
        </telerik:RadAjaxManager>
    </span>

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="VideoView" runat="server" Title="Video Viewer" OnClientBeforeClose="OnClientBeforeClose"
                VisibleStatusbar="false" Height="550" Width="800"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" />
    <asp:HiddenField runat="server" ID="FolderContextMenuAction" />
    <asp:HiddenField runat="server" ID="lblFolderId" />
    <asp:HiddenField runat="server" ID="lblDocId" />
    <asp:HiddenField runat="server" ID="lblCategoryId" />
    <input type="hidden" id="radGridClickedRowIndex" name="radGridClickedRowIndex" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script src="Scripts/jquery-1.7.1.js"></script>
        <script type="text/javascript">

            var radDocuments;

            function ShowFilter(obj) {
                if (obj.checked) {
                    $find('<%=grdDocument.ClientID %>').get_masterTableView().showFilterItem();
                } else {
                    $find('<%=grdDocument.ClientID %>').get_masterTableView().hideFilterItem();
                }
            }

            function refreshGrid() {
                var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView();
                masterTable.rebind();
            }

            function refreshTreeFolder() {
                ajaxManager.ajaxRequest("RebindTreeFolder");
            }

            function ExportGrid() {
                var masterTable = $find("<%=grdDocument.ClientID %>").get_masterTableView();
                masterTable.exportToExcel('PIN_list.xls');
                return false;
            }

            function GetGridObject(sender, eventArgs) {
                radDocuments = sender;
            }

            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("ExportTo") >= 0) {
                    args.set_enableAjax(false);
                }
            }

            function onColumnHidden(sender) {

                var masterTableView = sender.get_masterTableView().get_element();
                masterTableView.style.tableLayout = "auto";
                //window.setTimeout(function () { masterTableView.style.tableLayout = "auto"; }, 0);
            }

            // Undocked and Docked event slide bar Tree folder
            function OnClientUndocked(sender, args) {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;

                radDocuments.get_masterTableView().showColumn(9);
                radDocuments.get_masterTableView().showColumn(10);
                radDocuments.get_masterTableView().showColumn(11);
                radDocuments.get_masterTableView().showColumn(12);

                radDocuments.get_masterTableView().showColumn(13);
                radDocuments.get_masterTableView().showColumn(14);
                radDocuments.get_masterTableView().showColumn(15);
                //radDocuments.get_masterTableView().showColumn(16);

                ////if (selectedFolder != "") {
                ////    ajaxManager.ajaxRequest("ListAllDocuments");
                ////}
            }

            function OnClientDocked(sender, args) {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;

                radDocuments.get_masterTableView().hideColumn(9);
                radDocuments.get_masterTableView().hideColumn(10);
                radDocuments.get_masterTableView().hideColumn(11);
                radDocuments.get_masterTableView().hideColumn(12);

                radDocuments.get_masterTableView().hideColumn(13);
                radDocuments.get_masterTableView().hideColumn(14);
                radDocuments.get_masterTableView().hideColumn(15);
                //radDocuments.get_masterTableView().hideColumn(16);

                ////if (selectedFolder != "") {
                ////    ajaxManager.ajaxRequest("TreeView");
                ////}
            }


        </script>
        <script type="text/javascript">
            /* <![CDATA[ */
            var toolbar;
            var searchButton;
            var ajaxManager;

            function pageLoad() {
                $('iframe').load(function () { //The function below executes once the iframe has finished loading<---true dat, althoug Is coppypasta from I don't know where
                    //alert($('iframe').contents());
                });

                ajaxManager = $find("<%=ajaxCustomer.ClientID %>");
            }

            function ShowVideoForm(id) {
                var owd = $find("<%=VideoView.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/ViewVideoFile.aspx?docId=" + id, "VideoView");

                // window.radopen("Controls/Customers/CustomerEditForm.aspx?patientId=" + id, "CustomerDialog");
                //  return false;
            }

            function OnClientBeforeClose(sender, args) {
                var oWnd = $find("<%= RadWindowManager1.ClientID %>").getWindowByName("VideoView");
                oWnd.get_contentFrame().contentWindow.CloseVideo();
            }
            /* ]]> */
        </script>
    </telerik:RadCodeBlock>
</asp:Content>
