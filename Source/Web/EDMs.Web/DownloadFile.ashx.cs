﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using Telerik.Windows.Zip;
using ICSharpCode.SharpZipLib.Zip;

namespace EDMs.Web
{

using Business.Services;
using Data.Entities;
using Utilities;
using Utilities.Sessions;


    using Telerik.Web.UI;
    /// <summary>
    /// Summary description for DownloadFile
    /// </summary>
    public class DownloadFile : IHttpHandler
    {
        public readonly DocumentService documentService = new DocumentService();
        private readonly FolderService folderService = new FolderService();
        private readonly GroupFunctionService _GroupfunctionService = new GroupFunctionService();
        private readonly FolderPermissionService _FolderPermisstionService = new FolderPermissionService();
        private List<int> AdminGroup
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("GroupAdminList").Split(',').Select(t => Convert.ToInt32(t)).ToList();
            }
        }
        private List<int> GroupFunctionList
        {
            get
            {
                return this._GroupfunctionService.GetByRoleId(Convert.ToInt32(HttpContext.Current.Request.QueryString["userId"])).Select(t => t.ID).ToList();
            }
        }
        public void ProcessRequest(HttpContext context)
        {
            System.Web.HttpRequest request = System.Web.HttpContext.Current.Request;
            var type = request.QueryString["type"];
            // context.Response.ContentType = "text/plain";
            // context.Response.Write("Hello World");
            if (type == "1")
            {
                var filepath = request.QueryString["ListID"];


                var serverTotalDocPackPath = context.Server.MapPath("~/Exports/DocPack/" + DateTime.Now.ToBinary() + "_DocPack.rar");
                var docPack = ZipPackage.CreateFile(serverTotalDocPackPath);

                foreach (var selectedItem in filepath.Split('$').Where(t => !string.IsNullOrEmpty(t)).Select(t => Convert.ToInt32(t)).ToList())
                {
                    var document = this.documentService.GetById(selectedItem);
                    if (File.Exists(context.Server.MapPath(document.FilePath)))
                    {
                        docPack.Add(context.Server.MapPath(document.FilePath));
                    }
                }


                FileInfo ObjArchivo = new System.IO.FileInfo(serverTotalDocPackPath);
                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.ClearContent();
                response.Clear();
                response.ContentType = "text/plain";
                response.AddHeader("Content-Disposition",
                                   "attachment; filename=" + ObjArchivo.Name + ";");
                response.TransmitFile(ObjArchivo.FullName);
                response.Flush();
                System.IO.File.Delete(serverTotalDocPackPath);
                response.End();
                //Delete your local file


            }
           
            else if (type == "3")
            {
                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                var path = context.Server.MapPath("~/Exports/DocPack/"+context.Request.QueryString["path"]);
                var serverTotalDocPackPath =path;
                DirectoryInfo di = new DirectoryInfo(path);
                response.ClearContent();
                response.Clear();
                response.ContentType = "application/zip";
                response.AddHeader("Content-Disposition",
                                   "attachment; filename=" + di.Name+ ";");
                response.TransmitFile(serverTotalDocPackPath);
                response.Flush();
                System.IO.File.Delete(serverTotalDocPackPath);
                response.End();
            }
        }

        private List<int> GetAllChildren(int parent, List<Folder> folderList, ref List<int> childNodes)
        {
            var childFolderList = folderList.Where(t => t.ParentID == parent).ToList();
            if (childFolderList.Any())
            {
                childNodes.AddRange(childFolderList.Select(t => t.ID));
                foreach (var childFolder in childFolderList)
                {
                    this.GetAllChildren(childFolder.ID, folderList, ref childNodes);
                }
            }

            return childNodes.Distinct().ToList();
        }
        public  void ZipFolder(Folder CurrentFolder, ICSharpCode.SharpZipLib.Zip.ZipOutputStream zStream, List<Folder> ListFolder)
        {
            var SubFolders = ListFolder.Where(t => t.ParentID == CurrentFolder.ID).ToList(); //Directory.GetDirectories(CurrentFolder.);

            //calls the method recursively for each subfolder
            foreach (var Folder in SubFolders)
            {
               
                ZipFolder(Folder, zStream, ListFolder);
              
            }

            string relativePath = CurrentFolder.DirName.Substring(CurrentFolder.DirName.Length) + "/";

            //the path "/" is not added or a folder will be created
            //at the root of the file
            if (relativePath.Length > 1)
            {
                ZipEntry dirEntry;
                dirEntry = new ZipEntry(relativePath);
                dirEntry.DateTime = DateTime.Now;
            }

            //adds all the files in the folder to the zip
            var listDocuments = this.documentService.GetAllByFolder(CurrentFolder.ID);
            foreach (var file in listDocuments)
            {

                AddFileToZip(zStream, relativePath, HttpContext.Current.Server.MapPath(file.FilePath));
            }
        }
        private  void AddFileToZip(ICSharpCode.SharpZipLib.Zip.ZipOutputStream zStream, string relativePath, string file)
        {
            byte[] buffer = new byte[4096];

            //the relative path is added to the file in order to place the file within
            //this directory in the zip
            string fileRelativePath = (relativePath.Length > 1 ? relativePath : string.Empty) + Path.GetFileName(file);

            ZipEntry entry = new ZipEntry(fileRelativePath);
            entry.DateTime = DateTime.Now;
            zStream.PutNextEntry(entry);

            using (FileStream fs = File.OpenRead(file))
            {
                int sourceBytes;
                do
                {
                    sourceBytes = fs.Read(buffer, 0, buffer.Length);
                    zStream.Write(buffer, 0, sourceBytes);
                } while (sourceBytes > 0);
            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}