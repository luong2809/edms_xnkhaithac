﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web
{
    using System;
    using System.Linq;
    using System.Web.UI;

    using EDMs.Business.Services;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// Class customer
    /// </summary>
    public partial class _System : Page
    {
        
        #region Fields

        private PermissionService _permissionService;
        private MenuService _menuService;

        /// <summary>
        /// Gets the menu id.
        /// </summary>
        /// <value>
        /// The menu id.
        /// </value>
        private int MenuId
        {
            get
            {
                int menuId;
                if (int.TryParse(Request["menuid"], out menuId))
                {
                    return menuId;
                }
                return -1;
            }
        }

        /// <summary>
        /// Gets the selected menu value.
        /// </summary>
        /// <value>
        /// The selected menu value.
        /// </value>
        private string SelectedMenuValue
        {
            get { return Request["selected"]; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes a new instance of the <see cref="_System"/> class.
        /// </summary>
        public _System()
        {
            _permissionService = new PermissionService();
            _menuService = new MenuService();
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Loads the control from tree.
        /// </summary>
        /// <param name="selectedNode">The selected node.</param>
        /// <returns></returns>
        private void LoadControlFromTree(string selectedNodeValue)
        {
            if (phdUser.HasControls()) phdUser.Controls.Clear();
            if (phdRole.HasControls()) phdRole.Controls.Clear();
            if (phdPermission.HasControls()) phdPermission.Controls.Clear();

            Control control;
            switch (selectedNodeValue)
            {
                case "mnuUsers":
                    control = LoadControl(GlobalConsts.SystemUserControlPath);
                    phdUser.Controls.Add(control);
                    break;
                case "mnuRoles":
                    control = LoadControl(GlobalConsts.SystemRoleControlPath);
                    phdRole.Controls.Add(control);
                    break;
                case "mnuPermissions":
                    control = LoadControl(GlobalConsts.SystemPermissionControlPath);
                    phdPermission.Controls.Add(control);
                    break;
                case "mnuDataPermissions":
                    control = LoadControl(GlobalConsts.SystemPermissionDataControlPath);
                    phdPermission.Controls.Add(control);
                    break;
            }
        }

        /// <summary>
        /// Loads the left menu tree.
        /// </summary>
        private void LoadLeftMenuTree()
        {
            var permittedMenus = _permissionService.GetByRoleId(UserSession.Current.RoleId).Select(x => x.Menu);
            var permittedLeftMenus = permittedMenus.Where(x => x.Type == 2 && (x.ParentId == MenuId)).ToList();


            //To add the system parent menu node
            var parentNode = _menuService.GetByID(MenuId);
            permittedLeftMenus.Add(parentNode);
            permittedLeftMenus = permittedLeftMenus.OrderBy(x => x.Priority).ToList();

            treeSystem.DataSource = permittedLeftMenus;
            treeSystem.DataBind();
        }

        #endregion

        #region Events

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                LoadLeftMenuTree();

                if (!String.IsNullOrEmpty(SelectedMenuValue))
                {
                    var node = treeSystem.GetAllNodes().FirstOrDefault(x => x.Value == SelectedMenuValue);
                    if(node != null)
                    {
                        node.Selected = true;
                    }
                    LoadControlFromTree(SelectedMenuValue);
                }
            }
            else
            {
                if (treeSystem.SelectedNode != null)
                {
                    LoadControlFromTree(treeSystem.SelectedNode.Value);
                }
            }
        }

        protected void treeSystem_OnNodeClick(object sender, RadTreeNodeEventArgs e)
        {
        }

        #endregion

    }
}
