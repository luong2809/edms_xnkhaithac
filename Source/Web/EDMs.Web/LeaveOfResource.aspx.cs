﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeaveOfResource.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class Resource
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web
{
    using System;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Business.Services;
    using Utilities.Sessions;
    using Telerik.Web.UI;

    /// <summary>
    /// Class Resource
    /// </summary>
    public partial class LeaveOfResource : Page
    {
        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ((RadPane)
                 Master.FindControl("RadSplitter1").FindControl("contentPane").FindControl("RadSplitter2").FindControl(
                     "leftPane"))
                    .Collapsed = true;
            }
        }

        /// <summary>
        /// The load Resource by Resource type.
        /// </summary>
        /// <param name="isbind">
        /// The isbind.
        /// </param>
        protected void LoadResourceByResourceType(bool isbind = false)
        {
            var appointmentService = new AppointmentService();
            grdAppointment.DataSource =
                appointmentService.GetAll().Where(
                    t => t.IsBlock.GetValueOrDefault() && t.CreatedBy.GetValueOrDefault() == UserSession.Current.User.Id);

            if (isbind)
                grdAppointment.DataBind();
        }

        /// <summary>
        /// The search resource.
        /// </summary>
        /// <param name="search">
        /// The search.
        /// </param>
        /// <param name="isbind">
        /// The isbind.
        /// </param>
        protected void SearchResource(string search, bool isbind = false)
        {
            // var resourceService = new ResourceService();
            // if (treeReourceType.SelectedNode != null)
            // {
            // if (!string.IsNullOrEmpty(treeReourceType.SelectedNode.Value))
            // {
            // var ResourceType = treeReourceType.SelectedNode.Value;

            // if (ResourceType == string.Empty || int.Parse(ResourceType) == 0)
            // {
            // grdAppointment.DataSource = resourceService.FindByFullName(search);
            // }
            // else
            // {
            // int type = int.Parse(ResourceType);
            // grdAppointment.DataSource = resourceService.FindByFullName(search,type);
            // }
            // if (isbind)
            // grdAppointment.DataBind();
            // }
            // }
            // else
            // {
            // grdAppointment.DataSource = resourceService.FindByFullName(search);

            // if (isbind)
            // grdAppointment.DataBind();
            // }
        }

        /// <summary>
        /// The rad tree view 1_ node click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadTreeView1_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Node.Value))
            {
                LoadResourceByResourceType(true);
            }
        }

        /// <summary>
        /// The load control.
        /// </summary>
        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdAppointment_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            LoadResourceByResourceType();
        }

        /// <summary>
        /// Grid KhacHang item created
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdAppointment_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var editLink = (Image) e.Item.FindControl("EditLink");
                editLink.Attributes["href"] = "#";
                editLink.Attributes["onclick"] = string.Format("return ShowEditForm('{0}','{1}');", 
                                                               e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex][
                                                                   "Id"], e.Item.ItemIndex);
            }
        }

        /// <summary>
        /// The rad grid 1_ pre render.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadGrid1_PreRender(object sender, EventArgs e)
        {
            (grdAppointment.MasterTableView.GetColumn("FirstName") as GridBoundColumn).Visible = false;
            (grdAppointment.MasterTableView.GetColumn("GridButtonColumn") as GridButtonColumn).Visible = true;
        }


        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                grdAppointment.MasterTableView.SortExpressions.Clear();
                grdAppointment.MasterTableView.GroupByExpressions.Clear();
                grdAppointment.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                grdAppointment.MasterTableView.SortExpressions.Clear();
                grdAppointment.MasterTableView.GroupByExpressions.Clear();
                grdAppointment.MasterTableView.CurrentPageIndex = grdAppointment.MasterTableView.PageCount - 1;
                grdAppointment.Rebind();
            }
            else
            {
                SearchResource(e.Argument, true);
            }
        }

        /// <summary>
        /// grdAppointment Page Size Changed
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdAppointment_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem) e.Item;
            int appointmentId = Convert.ToInt32(item.GetDataKeyValue("Id").ToString());
            var appointmentService = new AppointmentService();
            appointmentService.Delete(appointmentId);
        }

        /// <summary>
        /// The grd appointment_ item command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdAppointment_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string abc = e.CommandName;
        }
    }
}