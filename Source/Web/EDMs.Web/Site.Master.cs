﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Site.Master.cs" company="">
//   
// </copyright>
// <summary>
//   The site master.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Configuration;

namespace EDMs.Web
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Web.Utilities.Enums;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;
    using System.Web.Security;

    /// <summary>
    /// The site master.
    /// </summary>
    public partial class SiteMaster : MasterPage
    {
        #region Fields

        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;
        private PermissionService _permissionService;
        private MenuService _menuService;

        private readonly GroupDataPermissionService groupDataPermissionService;

        private readonly CategoryService categoryService;

        private readonly DocumentService documentService;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the role id on session.
        /// </summary>
        /// <value>
        /// The role id.
        /// </value>
        public int RoleId
        {
            get { return UserSession.Current.User.RoleId != null ? UserSession.Current.User.RoleId.Value : -1; }
        }

        #endregion

        #region Methods

        public SiteMaster()
        {
            this._permissionService = new PermissionService();
            this._menuService = new MenuService();
            this.groupDataPermissionService = new GroupDataPermissionService();
            this.categoryService=new CategoryService();
            this.documentService = new DocumentService();
        }

        #endregion

        #region Events

        /// <summary>
        /// Handles the Init event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!UserSession.Current.IsAvailable)
            {
                if (Request.RawUrl != "/Controls/System/Login.aspx")
                {
                    Session.Add("ReturnURL", Request.RawUrl);
                }
                Response.Redirect("~/Controls/System/Login.aspx");
            }

            Page.PreLoad += master_Page_PreLoad;
        }

        /// <summary>
        /// Handles the PreLoad event of the master_Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.InvalidOperationException">Validation of Anti-XSRF token failed.</exception>
        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? string.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? string.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.RadSearchBox.MinFilterLength = 100;
                if (UserSession.Current.IsAvailable)
                {
                    var lblFullName = this.menu.Items[0].FindControl("lblFullName") as Label;
                    if (lblFullName != null)
                    {
                        lblFullName.Text = "Welcome, " + UserSession.Current.User.FullName + ".";
                    }

                    this.LoadLeftMenu();
                    this.LoadMainMenu();

                    if (ConfigurationManager.AppSettings["EnableLDAP"] == "true")
                    {
                        this.menu.Items[0].Items[0].Visible = false;
                        this.menu.Items[0].Items[1].Visible = false;
                    }

                }
            }
        }

        /// <summary>
        /// Handles the OnItemClick event of the menu control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RadMenuEventArgs"/> instance containing the event data.</param>
        protected void menu_OnItemClick(object sender, RadMenuEventArgs e)
        {
            switch (e.Item.Value)
            {
                case "LogoutCommand":
                    
                    Session.Abandon();
                    FormsAuthentication.SignOut();
                    UserSession.DestroySession();
                    Response.Redirect("~/Controls/System/Login.aspx");
                    Response.StatusCode = 401;
                    Response.StatusDescription = "Unauthorized";
                    Response.Expires = 0;
                    Response.Cache.SetNoStore();
                    Response.AppendHeader("Pragma", "no-cache");
                    Response.End();
                    Session.Clear();
                    break;
            }
        }


        #endregion

        #region Helpers

        /// <summary>
        /// Loads the main menu.
        /// </summary>
        private void LoadMainMenu()
        {
            var menus = this._menuService.GetAllRelatedPermittedMenuItems(RoleId, (int)MenuType.TopMenu).OrderBy(t => t.Priority).ToList();

            this.MainMenu.DataSource = menus;
            this.MainMenu.DataTextField = "Description";
            this.MainMenu.DataNavigateUrlField = "Url";
            this.MainMenu.DataFieldID = "Id";
            this.MainMenu.DataValueField = "Id";
            this.MainMenu.DataFieldParentID = "ParentId";
            this.MainMenu.DataBind();

            //if (Session["SelectedMainMenu"] != null)
            //{
            //    this.MainMenu.Items[Convert.ToInt32(Session["SelectedMainMenu"])].Selected = true;
            //    Session.Remove("SelectedMainMenu");
            //}

            this.SetIcon(this.MainMenu.Items, menus);
        }

        /// <summary>
        /// The set icon.
        /// </summary>
        /// <param name="menu">
        /// The menu.
        /// </param>
        /// <param name="menuData">
        /// The menu data.
        /// </param>
        private void SetIcon(RadMenuItemCollection menu, List<EDMs.Data.Entities.Menu> menuData)
        {
            foreach (RadMenuItem menuItem in menu)
            {
                if (!string.IsNullOrEmpty(menuItem.Value))
                {
                    var tempMenu = menuData.FirstOrDefault(t => t.Id == Convert.ToInt32(menuItem.Value));
                    if (tempMenu != null)
                    {
                        menuItem.ImageUrl = tempMenu.Icon;
                    }
                }

                if (menuItem.Items.Count > 0)
                {
                    this.SetIcon(menuItem.Items, menuData);
                }
            }
        }
        /// <summary>
        /// Loads the left menu.
        /// </summary>
        private void LoadLeftMenu()
        {
            var menus = _menuService.GetAllRelatedPermittedMenuItems(RoleId, (int)MenuType.LeftMenu);
            if (menus == null)
            {
                //bottomLeftPane.Collapsed = true; 
                return;
            }
            
            //Gets root parents only
            menus = menus.Where(x => x.ParentId == null).ToList();
            
            if (menus.Count == 0)
            {
                //bottomLeftPane.Collapsed = true;
                return;
            }
            //LeftMenu.DataSource = menus;
            //LeftMenu.DataTextField = "Description";
            //LeftMenu.DataNavigateUrlField = "Url";
            //LeftMenu.DataFieldID = "Id";
            //LeftMenu.DataBind();
        }

        #endregion

        protected void RadSearchBox_Search(object sender, SearchBoxEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Value))
            {
                var docObj = this.documentService.GetById(Convert.ToInt32(e.Value));
                if (docObj != null)
                {
                    Response.Redirect("~/QuickSearch.aspx?text=" + e.Text + "&ctgr=" + docObj.CategoryID);
                }
            }

            Response.Redirect("~/QuickSearch.aspx?text=" + e.Text);
        }
    }
}
