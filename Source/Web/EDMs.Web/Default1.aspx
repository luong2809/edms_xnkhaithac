﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default1.aspx.cs" Inherits="EDMs.Web._Default1" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="scheduler" TagName="AdvancedForm" Src="AdvancedForm.ascx"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="CSS/style.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .rlbGroup
        {
            border: none !important;
        }
        .ResourceServiceType
        {
            border-left: 4px solid !important;
        }
    </style>

<%-- ReSharper disable UnusedLocals --%>
    <script type="text/javascript">
        function OnClientAppointmentMoveEnd(sender, eventArgs) {
            var selectedResource = eventArgs.get_targetSlot().get_resource();

            var scheduler = $find('<%=RadScheduler1.ClientID %>');
            var drKey = selectedResource.get_key();
            var drName = selectedResource.get_text();

            eventArgs.get_appointment().get_attributes().setAttribute('ResourceId', drKey);
            eventArgs.get_appointment()._resources = selectedResource;
        }

        function OnClientAppointmentDeleting(sender, eventArgs) {
            var currentUserLoginId = <%= currentUserLoginId %>;
            var createdById = parseInt(eventArgs.get_appointment().get_attributes().getAttribute('CreatedBy'));
            var isBlock = eventArgs.get_appointment().get_attributes().getAttribute('IsBlock');
             
            if (currentUserLoginId !== createdById) {
                window.radalert('Bạn không có quyền xóa lịch khám này, bởi vì nó đang được người khác sửa dụng hoặc không phải bạn tạo ra.', 400, 120, 'Thông báo');
                eventArgs.set_cancel(true);
            } else if (Boolean(isBlock) === true) {
                window.radalert('Bạn không có quyền xóa lịch khám này, bởi vì nó đang bị block.', 350, 100, 'Thông báo');
                eventArgs.set_cancel(true);
            }
        }

        function OnClientAppointmentMoveStart(sender, eventArgs) {
            var parentAppointmentId = eventArgs.get_appointment().get_attributes().getAttribute('ParentAppointmentId');
            
            if (parentAppointmentId != undefined) {
                eventArgs.set_cancel(true);
            }
        }

        function OnClientAppointmentResizeStart(sender, eventArgs) {
            var parentAppointmentId = eventArgs.get_appointment().get_attributes().getAttribute('ParentAppointmentId');
            
            if (parentAppointmentId != undefined) {
                eventArgs.set_cancel(true);
            }
        }
        
    </script>
<%-- ReSharper restore UnusedLocals --%>
    <telerik:RadWindowManager runat="server" id="RadWindowManager1"></telerik:RadWindowManager>
    <telerik:RadCodeBlock runat="server">
        <telerik:RadAjaxManager runat="Server" ID="RadAjaxManager1">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="RadCalendar1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadScheduler1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="RadScheduler1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadScheduler1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ListBoxResource">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadScheduler1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ListBoxServiceType">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadScheduler1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ListBoxRoom">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadScheduler1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="PanelBar1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadScheduler1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnViewByRoom">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="PanelBar1" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="RadScheduler1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnViewByDoctor">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="PanelBar1" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="RadScheduler1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>

        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" />
    </telerik:RadCodeBlock>
    <div class="calendar-container">
        <telerik:RadCalendar runat="server"
            ID="RadCalendar1"
            AutoPostBack="True"
            EnableMultiSelect="False"
            CultureInfo="vi-VN"
            DayNameFormat="FirstTwoLetters"
            OnSelectionChanged="RadCalendar1_SelectionChanged"
            OnDefaultViewChanged="RadCalendar1_DefaultViewChanged"
            ViewSelectorText="x">

            <FastNavigationSettings
                CancelButtonCaption="Hủy"
                OkButtonCaption="Chấp nhận"
                TodayButtonCaption="Hôm nay">
            </FastNavigationSettings>

            <SpecialDays>
                <telerik:RadCalendarDay Date="" Repeatable="Today">
                    <ItemStyle CssClass="rcToday" />
                </telerik:RadCalendarDay>
            </SpecialDays>

            <WeekendDayStyle CssClass="rcWeekend"></WeekendDayStyle>

            <CalendarTableStyle CssClass="rcMainTable"></CalendarTableStyle>

            <OtherMonthDayStyle CssClass="rcOtherMonth"></OtherMonthDayStyle>

            <OutOfRangeDayStyle CssClass="rcOutOfRange"></OutOfRangeDayStyle>

            <DisabledDayStyle CssClass="rcDisabled"></DisabledDayStyle>

            <SelectedDayStyle CssClass="rcSelected"></SelectedDayStyle>

            <DayOverStyle CssClass="rcHover"></DayOverStyle>

            <FastNavigationStyle CssClass="RadCalendarMonthView RadCalendarMonthView_Default"></FastNavigationStyle>

            <ViewSelectorStyle CssClass="rcViewSel"></ViewSelectorStyle>

        </telerik:RadCalendar>
    </div>
    <telerik:RadCodeBlock runat="server">
        <telerik:RadPanelBar runat="server" ID="PanelBar1" Width="100%" ExpandMode="MultipleExpandedItems" AllowCollapseAllItems="true" Height="220">
            <Items>
                <telerik:RadPanelItem runat="server" Value="Room" Text="Phòng">
                    <Items>
                        <telerik:RadPanelItem runat="server">
                            <ItemTemplate>
                                <div class="rpCheckBoxPanel">
                                    <telerik:RadListBox ID="ListBoxRoom"
                                        OnItemCheck="ListBoxResource_ItemCheck"
                                        runat="server"
                                        CheckBoxes="true"
                                        AutoPostBack="true"
                                        Width="100%">
                                        <ItemTemplate>
                                            <%# Eval("Name") %>
                                        </ItemTemplate>
                                    </telerik:RadListBox>
                                </div>
                            </ItemTemplate>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelItem>

                <telerik:RadPanelItem runat="server" Value="Doctor" Text="Bác Sĩ" Expanded="true">
                    <Items>
                        <telerik:RadPanelItem runat="server">
                            <ItemTemplate>
                                <div class="rpCheckBoxPanel">
                                    <telerik:RadListBox ID="ListBoxResource"
                                        OnItemCheck="ListBoxResource_ItemCheck"
                                        runat="server"
                                        CheckBoxes="true"
                                        AutoPostBack="true"
                                        Width="100%">
                                        <ItemTemplate>
                                            <img style="width: 10px; height: 10px; background-color: <%# Eval("Color") %>" />
                                            <%# Eval("FullName") %>
                                        </ItemTemplate>
                                    </telerik:RadListBox>
                                </div>
                            </ItemTemplate>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelItem>

                <telerik:RadPanelItem runat="server" Value="ServiceType" Text="Dịch Vụ">
                    <Items>
                        <telerik:RadPanelItem runat="server">
                            <ItemTemplate>
                                <div class="rpCheckBoxPanel">
                                    <telerik:RadListBox ID="ListBoxServiceType"
                                        OnItemCheck="ListBoxResource_ItemCheck"
                                        runat="server"
                                        AutoPostBack="true"
                                        CheckBoxes="true"
                                        Width="100%">
                                        <ItemTemplate>
                                            <img style="width: 10px; height: 10px; background-color: <%# Eval("Color") %>" />
                                            <%# Eval("Name") %>
                                        </ItemTemplate>
                                    </telerik:RadListBox>
                                </div>
                            </ItemTemplate>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelItem>
            </Items>
        </telerik:RadPanelBar>
    </telerik:RadCodeBlock>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="classDiv" style="margin: 2px;">
        <telerik:RadButton ID="btnViewByRoom" runat="server" ToggleType="Radio" ButtonType="StandardButton" Text="Xem theo Phòng" OnCheckedChanged="btnViewByRoom_CheckedChanged"
            GroupName="StandardButton" AutoPostBack="true">
            <ToggleStates>
                <telerik:RadButtonToggleState PrimaryIconCssClass="rbToggleRadioChecked"></telerik:RadButtonToggleState>
                <telerik:RadButtonToggleState PrimaryIconCssClass="rbToggleRadio"></telerik:RadButtonToggleState>
            </ToggleStates>
        </telerik:RadButton>
        <telerik:RadButton ID="btnViewByDoctor" runat="server" ToggleType="Radio" Checked="true" Text="Xem theo Bác sĩ" OnCheckedChanged="btnViewByDoctor_CheckedChanged"
            GroupName="StandardButton" ButtonType="StandardButton" AutoPostBack="true">
            <ToggleStates>
                <telerik:RadButtonToggleState PrimaryIconCssClass="rbToggleRadioChecked"></telerik:RadButtonToggleState>
                <telerik:RadButtonToggleState PrimaryIconCssClass="rbToggleRadio"></telerik:RadButtonToggleState>
            </ToggleStates>
        </telerik:RadButton>
        <%--<telerik:RadButton ID="RadButton18" runat="server" ToggleType="Radio" GroupName="StandardButton" Text="Xem theo dịch vụ"
               ButtonType="StandardButton" AutoPostBack="true">
               <ToggleStates>
                    <telerik:RadButtonToggleState  PrimaryIconCssClass="rbToggleRadioChecked">
                    </telerik:RadButtonToggleState>
                    <telerik:RadButtonToggleState  PrimaryIconCssClass="rbToggleRadio">
                    </telerik:RadButtonToggleState>
               </ToggleStates>
          </telerik:RadButton>--%>
    </div>
    <telerik:RadScheduler runat="server"
        ID="RadScheduler1"
        ShowFooter="False"
        Height="100%"
        Width="100%"
        TimelineView-UserSelectable="false"
        
        OnNavigationComplete="RadScheduler1_NavigationComplete"
        OnAppointmentDataBound="RadScheduler1_AppointmentDataBound"
        OnAppointmentDelete="RadScheduler1_AppointmentDelete"
        OnAppointmentUpdate="RadScheduler1_AppointmentUpdate"
        OnAppointmentInsert="RadScheduler1_AppointmentInsert"
       
        OnAppointmentCreated="RadScheduler1_OnAppointmentCreated"
        OnClientAppointmentDeleting="OnClientAppointmentDeleting"
        OnClientAppointmentMoveEnd="OnClientAppointmentMoveEnd"
        OnClientAppointmentMoveStart="OnClientAppointmentMoveStart"
        OnClientAppointmentResizeStart="OnClientAppointmentResizeStart"

        AppointmentContextMenuSettings-EnableDefault="true"
        Culture="vi-VN"
        Reminders-Enabled="true"
        SelectedView="WeekView"
        WeekView-DayEndTime="20:00:00"
        WeekView-DayStartTime="07:00:00"
        DayStartTime="07:00:00"
        DayEndTime="20:00:00"
        FirstDayOfWeek="Monday"
        LastDayOfWeek="Sunday"
        EnableDatePicker="False"
        Localization-Save="Lưu"
        Localization-Cancel="Hủy bỏ"
        HoursPanelTimeFormat="H:mm"
        ShowAllDayRow="False"
        StartInsertingInAdvancedForm="True"
        StartEditingInAdvancedForm="True"
        EditFormDateFormat="dd/MM/yyyy"
        EditFormTimeFormat="HH:mm"
        EnableRecurrenceSupport="False"
        EnableCustomAttributeEditing="true"
        OverflowBehavior="Auto"
        CustomAttributeNames="Code,SetDisplayName,PatientId,NextCall,LastCall,NumberCall,ServiceTypeId,AppointmentStatusId,ResourceId,Description,Reminder, ReminderId,RecurrenceRuleText,CreatedBy, UpdateBy, LastUpdate, IsBlock, ResourceOthers, ParentAppointmentId">

        <AdvancedForm Modal="true" MaximumHeight="700" Width="800"></AdvancedForm>
        <TimelineView UserSelectable="False" />
        <AppointmentContextMenuSettings EnableDefault="True" />
        <%--<ResourceHeaderTemplate>
            <asp:Panel ID="ResourceImageWrapper" runat="server" CssClass="ResCustomClass">
                <asp:Label ID="lblHeader" runat="server"></asp:Label>
            </asp:Panel>
        </ResourceHeaderTemplate>--%>
        <Localization
            AdvancedSubjectRequired="Tên không được để trống"
            HeaderDay="Xem theo Ngày"
            HeaderWeek="Xem theo Tuần"
            HeaderMonth="Xem theo Tháng"
            Save="Lưu"
            Cancel="Hủy bỏ"
            AdvancedNewAppointment="Thêm mới lịch khám"
            AdvancedEditAppointment="Sửa lịch khám"
            AdvancedSubject="Tiêu đề"
            AdvancedAllDayEvent="Cả ngày"
            HeaderToday="Hôm nay"
            AdvancedFrom="Ngày bắt đầu"
            AdvancedTo="Ngày kết thúc"
            AdvancedStartTimeBeforeEndTime="Giờ kết thúc phải lớn hơn giờ bắt đầu."
            AdvancedEndDateRequired="Ngày kết thúc không được để trống"
            AdvancedEndTimeRequired="Giờ kết thúc không được để trống"
            AdvancedStartDateRequired="Ngày bắt đầu không được để trống"
            AdvancedStartTimeRequired="Giờ kết thúc không được để trống"
            ConfirmDeleteText="Bạn có muốn xóa lịch khám này?"
            ConfirmDeleteTitle="Thông báo"
            ShowAdvancedForm="Nâng cao"
            AdvancedCalendarToday="Hôm nay"
            AdvancedCalendarCancel="Hủy bỏ"
            AdvancedCalendarOK="Chấp nhận"
            ContextMenuAddAppointment="Thêm mới lịch khám"
            ContextMenuDelete="Xóa"
            ContextMenuEdit="Sửa"
            ReminderHours="giờ"
            ReminderHour="giờ"
            ReminderMinutes="phút"
            ReminderWeeks="tuần"
            ReminderWeek="tuần"
            ReminderDays="ngày"
            ReminderNone="Không thiết lập"
            ReminderDismiss="Xóa reminder này"
            ReminderDismissAll="Xóa tất cả reminder"
            ReminderOpenItem="Xem reminder"
            ReminderOverdue="quá hạn"
            ReminderSnooze="Chấp nhận"
            ReminderSnoozeHint="Chọn số phút để được nhắc nhở một lần nữa"
            ReminderBeforeStart="trước khi bắt đầu" 
            ReminderDueIn="còn"
            ReminderMinute="phút"/>
        <%--<AppointmentTemplate>
            <div>
                <%# Eval("Code") %>
            </div>
            <div style="font-style: italic;">
                <%# Eval("SetDisplayName") %>
            </div>
            <div style="font-style: italic;">
                <%# Eval("Start", "{0:HH:mm}") %> - <%# Eval("End", "{0:HH:mm}") %>
            </div>
            <div style="">
            </div>
        </AppointmentTemplate>--%>
        <AppointmentTemplate>
            <asp:Label runat="server" ID="lblAppointmentTemplate"/>
        </AppointmentTemplate>

        <AdvancedEditTemplate>
            <scheduler:AdvancedForm runat="server" ID="AdvancedEditForm1"
                Mode="Edit"
                Code='<%# Bind("Code") %>'
                SetDisplayName='<%# Bind("SetDisplayName") %>'
                PatientId='<%# Bind("PatientId") %>'
                NextCall='<%# Bind("NextCall") %>'
                LastCall='<%# Bind("LastCall") %>'
                NumberCall='<%# Bind("NumberCall") %>'
                ServiceTypeId='<%# Bind("ServiceTypeId") %>'
                AppointmentStatusId='<%# Bind("AppointmentStatusId") %>'
                ResourceId='<%# Bind("ResourceId") %>'
                Start='<%# Bind("Start") %>'
                End='<%# Bind("End") %>'
                Reminder='<%# Bind("Reminder") %>'
                ReminderId='<%# Bind("ReminderId") %>'
                Description='<%# Bind("Description") %>'
                RoomId='<%# Bind("RoomId") %>'
                RecurrenceRuleText='<%# Bind("RecurrenceRuleText") %>' 
                CreatedBy='<%# Bind("CreatedBy") %>' 
                UpdateBy='<%# Bind("UpdateBy") %>'
                LastUpdate='<%# Bind("LastUpdate") %>'
                IsBlock='<%# Bind("IsBlock") %>'
                ResourceOthers='<%# Bind("ResourceOthers") %>'
                ParentAppointmentId='<%# Bind("ParentAppointmentId") %>'
                />
        </AdvancedEditTemplate>
        <AdvancedInsertTemplate>
            <scheduler:AdvancedForm runat="server" ID="AdvancedInsertForm1"
                Mode="Insert"
                Code='<%# Bind("Code") %>'
                SetDisplayName='<%# Bind("SetDisplayName") %>'
                PatientId='<%# Bind("PatientId") %>'
                NextCall='<%# Bind("NextCall") %>'
                LastCall='<%# Bind("LastCall") %>'
                NumberCall='<%# Bind("NumberCall") %>'
                ServiceTypeId='<%# Bind("ServiceTypeId") %>'
                AppointmentStatusId='<%# Bind("AppointmentStatusId") %>'
                ResourceId='<%# Bind("ResourceId") %>'
                Start='<%# Bind("Start") %>'
                End='<%# Bind("End") %>'
                Reminder='<%# Bind("Reminder") %>'
                ReminderId='<%# Bind("ReminderId") %>'
                Description='<%# Bind("Description") %>'
                RoomId='<%# Bind("RoomId") %>'
                RecurrenceRuleText='<%# Bind("RecurrenceRuleText") %>' 
                CreatedBy='<%# Bind("CreatedBy") %>' 
                UpdateBy='<%# Bind("UpdateBy") %>'
                LastUpdate='<%# Bind("LastUpdate") %>'
                IsBlock='<%# Bind("IsBlock") %>'
                ResourceOthers='<%# Bind("ResourceOthers") %>'
                ParentAppointmentId='<%# Bind("ParentAppointmentId") %>'
                />
        </AdvancedInsertTemplate>
    </telerik:RadScheduler>
</asp:Content>
