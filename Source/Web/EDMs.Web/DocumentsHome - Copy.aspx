﻿<%@ Page Title="EDMS - Nhiệt điện Mông Dương" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DocumentsHome.aspx.cs" Inherits="EDMs.Web.DocumentsHome" EnableViewState="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!--[if gte IE 8]>
        <style type="text/css">
            #ctl00_ContentPlaceHolder2_grdDocument_ctl00_Header{table-layout:auto !important;}
            #ctl00_ContentPlaceHolder2_grdDocument_ctl00{table-layout:auto !important;}
        </style>
    <![endif]-->
    <script type="text/javascript" src="Scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="Content/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="Content/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="Content/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
    <style type="text/css">
        /*Custom CSS of Grid documents for FF browser*/
        /*#ctl00_ContentPlaceHolder2_grdDocument_ctl00_Header{table-layout:auto !important;}
        #ctl00_ContentPlaceHolder2_grdDocument_ctl00{table-layout:auto !important;}*/
        /*End*/
        @-moz-document url-prefix() {
            #ctl00_ContentPlaceHolder2_grdDocument_ctl00_Header {
                table-layout: auto !important;
            }

            #ctl00_ContentPlaceHolder2_grdDocument_ctl00 {
                table-layout: auto !important;
            }
        }

        .RadGrid .rgRow td, .RadGrid .rgAltRow td, .RadGrid .rgEditRow td, .RadGrid .rgFooter td, .RadGrid .rgFilterRow td, .RadGrid .rgHeader, .RadGrid .rgResizeCol, .RadGrid .rgGroupHeader td {
            padding-left: 1px !important;
            padding-right: 2px !important;
            vertical-align: top;
        }

        /*Hide change page size control*/
        /*div.RadGrid .rgPager .rgAdvPart     
        {     
        display:none;
        }*/


        .RadGrid .rgSelectedRow {
            background-image: none !important;
            background-color: coral !important;
        }

        .rtPlus, .rtMinus {
            background-image: url('Images/plus-minus-icon1.png') !important;
            cursor: pointer;
        }

        .rtIn {
            cursor: pointer;
        }

        .new1 .rtPlus, .new1 .rtMinus {
            background-image: url('Images/plus-minus-icon2.png') !important;
        }

        .new2 .rtPlus, .new2 .rtMinus {
            background-image: url('Images/plus-minus-icon3.png') !important;
        }


        a.tooltip {
            outline: none;
            text-decoration: none;
        }

            a.tooltip strong {
                line-height: 30px;
            }

            a.tooltip:hover {
                text-decoration: none;
            }

            a.tooltip span {
                z-index: 10;
                display: none;
                padding: 14px 20px;
                margin-top: -30px;
                margin-left: 5px;
                width: 240px;
                line-height: 16px;
            }

            a.tooltip:hover span {
                display: inline;
                position: absolute;
                color: #111;
                border: 1px solid #DCA;
                background: #fffAF0;
            }

        .callout {
            z-index: 20;
            position: absolute;
            top: 30px;
            border: 0;
            left: -12px;
        }

        /*CSS3 extras*/
        a.tooltip span {
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            -moz-box-shadow: 5px 5px 8px #CCC;
            -webkit-box-shadow: 5px 5px 8px #CCC;
            box-shadow: 5px 5px 8px #CCC;
        }

        .rgMasterTable {
            table-layout: auto;
        }


        #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_grdDocumentPanel, #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_divContainerPanel {
            height: 100% !important;
        }

        #ctl00_ContentPlaceHolder2_RadPageView1, #ctl00_ContentPlaceHolder2_RadPageView2,
        #ctl00_ContentPlaceHolder2_RadPageView3, #ctl00_ContentPlaceHolder2_RadPageView4,
        #ctl00_ContentPlaceHolder2_RadPageView5 {
            height: 100% !important;
        }

        #divContainerLeft {
            width: 25%;
            float: left;
            margin: 5px;
            height: 99%;
            border-right: 1px dotted green;
            padding-right: 5px;
        }

        #divContainerRight {
            width: 100%;
            float: right;
            margin-top: 5px;
            height: 99%;
        }

        .dotted {
            border: 1px dotted #000;
            border-style: none none dotted;
            color: #fff;
            background-color: #fff;
        }

        .exampleWrapper {
            width: 100%;
            height: 100%;
            /*background: transparent url(images/background.png) no-repeat top left;*/
            position: relative;
        }

        .tabStrip {
            position: absolute;
            top: 0px;
            left: 0px;
        }

        .multiPage {
            position: absolute;
            top: 30px;
            left: 0px;
            color: white;
            width: 100%;
            height: 100%;
        }

        /*Fix RadMenu and RadWindow z-index issue*/
        .radwindow {
            z-index: 8000 !important;
        }

        .TemplateMenu {
            z-index: 10;
            padding-bottom: 20px;
            padding-left: 223px;
        }
    </style>
    <telerik:RadPanelBar ID="radPbSearch" runat="server" Width="100%">
        <Items>
            <telerik:RadPanelItem Text="ADVANCE SEARCH" runat="server" Expanded="True" Width="100%">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Advance Search" ImageUrl="Images/search.gif" />
                </Items>
            </telerik:RadPanelItem>
        </Items>
    </telerik:RadPanelBar>
    <telerik:RadPanelBar ID="radPbCategories" runat="server" Width="100%">
        <%-- OnItemClick="radPbCategories_ItemClick" OnClientItemClicking="radPbCategories_OnClientItemClicking"
            <Items>
            <telerik:RadPanelItem Text="BIENDONG POC DOCUMENTS" runat="server" Expanded="True" >
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Admin & HR" ImageUrl="Images/Document.png" Value="2" />
                    <telerik:RadPanelItem runat="server" Text="Drilling & Completions" Value="3" ImageUrl="Images/Document.png" />
                    <telerik:RadPanelItem runat="server" Text="Finance & Accounting" Value="4" ImageUrl="Images/Document.png" />
                    <telerik:RadPanelItem runat="server" Text="HSE" Value="5" ImageUrl="Images/Document.png" />
                    <telerik:RadPanelItem runat="server" Text="Operations" Value="6" ImageUrl="Images/Document.png"/>
                    <telerik:RadPanelItem runat="server" Text="Project Procedures" Value="7" ImageUrl="Images/Document.png" />
                    <telerik:RadPanelItem runat="server" Text="Sub Surface" Value="8" ImageUrl="Images/Document.png" />
                </Items>
            </telerik:RadPanelItem>
        </Items>--%>
    </telerik:RadPanelBar>
   <%--<telerik:RadPanelBar ID="RadPanelBar1" runat="server" Width="100%">
        <Items>
            <telerik:RadPanelItem Text="TRANSMITTAL" runat="server" Expanded="True" Width="100%">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Transmittal" ImageUrl="Images/Transmittal.png" NavigateUrl="Transmittal.aspx" />
                </Items>
            </telerik:RadPanelItem>
        </Items>
    </telerik:RadPanelBar>--%>
    <telerik:RadPanelBar ID="radPbList" runat="server" Width="100%" />
    <telerik:RadPanelBar ID="radPbSystem" runat="server" Width="100%" />
    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" Skin="" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <telerik:RadSplitter ID="RadSplitter4" runat="server" Orientation="Horizontal" Width="100%">
        <telerik:RadPane ID="RadPane3" runat="server" Height="30px" Scrollable="false" Scrolling="None">

            <telerik:RadToolBar ID="radViewSettingBar" runat="server" Width="30%" OnClientButtonClicking="radViewSettingBar_OnClientButtonClicking" dir="rtl">
                <Items>
                    <telerik:RadToolBarDropDown runat="server" Text="View settings" ImageUrl="~/Images/viewSetting.png">
                        <Buttons>
                            <telerik:RadToolBarButton runat="server" Text="Tree view" Value="1" ImageUrl="~/Images/treeview.png"></telerik:RadToolBarButton>
                            <telerik:RadToolBarButton runat="server" Text="List all documents" Value="2" ImageUrl="~/Images/listAll.png"></telerik:RadToolBarButton>
                        </Buttons>
                    </telerik:RadToolBarDropDown>

                    <telerik:RadToolBarButton runat="server" IsSeparator="true" />
                </Items>
            </telerik:RadToolBar>

            <telerik:RadToolBar ID="CustomerMenu" runat="server" Width="70%" OnClientButtonClicking="OnClientButtonClicking">
                <Items>
                    <telerik:RadToolBarDropDown runat="server" Text="New" ImageUrl="~/Images/addNew.png" Visible="False">
                        <Buttons>
                            <telerik:RadToolBarButton runat="server" Text="Document" Value="1" ImageUrl="~/Images/addDocument.png"></telerik:RadToolBarButton>
                            <telerik:RadToolBarButton runat="server" Text="Multi documents" Value="2" ImageUrl="~/Images/addmulti.png"></telerik:RadToolBarButton>
                        </Buttons>
                    </telerik:RadToolBarDropDown>
                    <telerik:RadToolBarButton runat="server" IsSeparator="true" Visible="False" />

                    <telerik:RadToolBarDropDown runat="server" Text="Action" Visible="False" ImageUrl="~/Images/action.png">
                        <Buttons>
                            <telerik:RadToolBarButton runat="server" Text="Send notifications" Value="3" ImageUrl="~/Images/sendnotification.png" Visible="false"></telerik:RadToolBarButton>
                            <telerik:RadToolBarButton runat="server" Text="Send mail" Value="3" ImageUrl="~/Images/email.png"></telerik:RadToolBarButton>
                            <telerik:RadToolBarButton runat="server" IsSeparator="true" />
                            <telerik:RadToolBarButton runat="server" Text="Export master list" Value="5" ImageUrl="~/Images/exexcel.png" />
                            <telerik:RadToolBarButton runat="server" Text="Import data file" Value="7" ImageUrl="~/Images/import.png" />
                            <telerik:RadToolBarButton runat="server" IsSeparator="true" Visible="false" />
                            <telerik:RadToolBarButton runat="server" Text="Move files" Value="6" ImageUrl="~/Images/Move.png" Visible="false" />
                            <telerik:RadToolBarButton runat="server" Text="View explorer" Value="4" ImageUrl="~/Images/expview.png" Visible="false" />
                            <telerik:RadToolBarButton runat="server" IsSeparator="true" />
                            <telerik:RadToolBarButton runat="server" Text="Checkout selected documents" Value="8" ImageUrl="~/Images/checkout.png" Visible="false" />
                            <telerik:RadToolBarButton runat="server" Text="Checkin selected documents" Value="8" ImageUrl="~/Images/checkin.png" Visible="false" />
                            <telerik:RadToolBarButton runat="server" Text="Download multi documents" Value="3" ImageUrl="~/Images/download.png" />
                            <telerik:RadToolBarButton runat="server" Text="Up folder" Value="1" ImageUrl="~/Images/download.png" />
                            <%--<telerik:RadToolBarButton runat="server" Text="Up excel" Value="1" ImageUrl="~/Images/download.png" />--%>
                            <%--<telerik:RadToolBarButton runat="server" IsSeparator="true" />
                            <telerik:RadToolBarButton runat="server" Text="Permit Special documents selected" Value="PermitDocs" ImageUrl="~/Images/datap.png" />--%>
                        </Buttons>
                    </telerik:RadToolBarDropDown>
                    <telerik:RadToolBarButton runat="server" IsSeparator="true" />

                    <telerik:RadToolBarButton runat="server" Value="IsFilter">
                        <ItemTemplate>
                            <asp:CheckBox ID="ckbEnableFilter" runat="server" Text="Enable filter list document" AutoPostBack="True"
                                OnCheckedChanged="ckbEnableFilter_CheckedChange" />
                        </ItemTemplate>
                    </telerik:RadToolBarButton>
                </Items>
            </telerik:RadToolBar>
        </telerik:RadPane>
        <telerik:RadPane ID="RadPane2" runat="server" Scrollable="false" Scrolling="None">
            <telerik:RadSplitter ID="Radsplitter3" runat="server" Orientation="Horizontal">
                <telerik:RadPane ID="Radpane4" runat="server" Scrolling="None">
                    <telerik:RadSplitter ID="Radsplitter10" runat="server" Orientation="Vertical">
                        <telerik:RadPane ID="Radpane1" runat="server" Scrolling="Both" Width="270" MinWidth="270">
                            <telerik:RadSlidingZone ID="SlidingZone1" runat="server" Width="20px" DockedPaneId="RadSlidingPane1">
                                <telerik:RadSlidingPane ID="RadSlidingPane1" runat="server"
                                    Title="Folder Tree" Width="320" Height="100%">
                                    <telerik:RadTreeView ID="radTreeFolder" runat="server" Width="100%" Height="100%" ShowLineImages="False"
                                        OnNodeEdit="radTreeFolder_NodeEdit"
                                        OnContextMenuItemClick="radTreeFolder_ContextMenuItemClick"
                                        OnNodeClick="radTreeFolder_NodeClick"
                                        OnNodeDataBound="radTreeFolder_OnNodeDataBound"
                                        OnClientContextMenuItemClicking="onClientContextMenuItemClicking"
                                        OnClientContextMenuShowing="onClientContextMenuShowing"
                                        OnClientNodeExpanded="rtvExplore_OnNodeExpandedCollapsed"
                                        OnClientNodeCollapsed="rtvExplore_OnNodeExpandedCollapsed"
                                        OnClientNodeClicking="onNodeClicking">
                                        <ContextMenus>
                                            <telerik:RadTreeViewContextMenu ID="MainContextMenu" runat="server">
                                                <Items>
                                                    <telerik:RadMenuItem Value="New" Text="New folder" ImageUrl="Images/addfolder.png" />
                                                    <telerik:RadMenuItem Value="Rename" Text="Rename" ImageUrl="Images/rename.png" />
                                                    <telerik:RadMenuItem Value="Delete" Text="Delete" ImageUrl="Images/deletefolder.png" />
                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                    <%--<telerik:RadMenuItem Value="MoveFolder" Text="Move" ImageUrl="Images/move.png"/>--%>
                                                    <telerik:RadMenuItem Value="Permission" Visible="true" Text="Permission" ImageUrl="Images/datap.png" />
                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                    <telerik:RadMenuItem Value="CopyPath" Visible="true" Text="Copy File Path" ImageUrl="Images/datap.png" />
                                                </Items>
                                            </telerik:RadTreeViewContextMenu>
                                        </ContextMenus>
                                        <DataBindings>
                                            <telerik:RadTreeNodeBinding Expanded="false"></telerik:RadTreeNodeBinding>
                                        </DataBindings>
                                    </telerik:RadTreeView>
                                </telerik:RadSlidingPane>
                            </telerik:RadSlidingZone>
                        </telerik:RadPane>
                        <telerik:RadSplitBar ID="Radsplitbar1" runat="server">
                        </telerik:RadSplitBar>
                        <telerik:RadPane ID="Radpane6" runat="server" Scrolling="None">
                            <telerik:RadGrid ID="grdDocument" runat="server" AllowPaging="True"
                                AutoGenerateColumns="False" CellPadding="0" CellSpacing="0"
                                GridLines="None" Height="100%" AllowMultiRowSelection="True"
                                OnDeleteCommand="grdDocument_DeleteCommand"
                                OnNeedDataSource="grdDocument_OnNeedDataSource"
                                OnItemCommand="grdDocument_ItemCommand"
                                OnItemDataBound="grdDocument_ItemDataBound"
                                PageSize="100" Style="outline: none">
                                <%--<ExportSettings ExportOnlyData="true" FileName="PINList" IgnorePaging="true" OpenInNewWindow="true"> 
                                        <Excel Format="Html" FileExtension="xls" /> 
                                    </ExportSettings> --%>
                                <GroupingSettings CaseSensitive="False"></GroupingSettings>
                                <MasterTableView
                                    ClientDataKeyNames="ID" DataKeyNames="ID" CommandItemDisplay="Top" EditMode="InPlace" Font-Size="8pt">
                                    <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Refresh Data" ShowExportToExcelButton="true" />
                                    <%--<CommandItemTemplate>
                                            <asp:LinkButton ID="lnlExportToExcel" runat="server" OnClientClick="return ExportGrid()" 
                                                Visible='true'><img class="middle" alt="" src="Images/exexcel2.png" /></asp:LinkButton>
                                        </CommandItemTemplate>--%>

                                    <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; Documents." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <%--<GroupByExpressions>
                                            <telerik:GridGroupByExpression>
                                                <SelectFields>
                                                    <telerik:GridGroupByField FieldName="DirName"></telerik:GridGroupByField>
                                                </SelectFields>
                                                <GroupByFields>
                                                    <telerik:GridGroupByField FieldName="DirName"></telerik:GridGroupByField>
                                                </GroupByFields>
                                            </telerik:GridGroupByExpression>
                                        </GroupByExpressions>--%>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />
                                        <telerik:GridBoundColumn DataField="DisciplineID" UniqueName="DisciplineID" Visible="True" Display="False" />
                                        <telerik:GridBoundColumn DataField="IsCheckOut" UniqueName="IsCheckOut" Visible="True" Display="False" />
                                        <telerik:GridBoundColumn DataField="IsFolder" UniqueName="IsFolder" Display="False" />
                                        <telerik:GridBoundColumn DataField="FilePath" UniqueName="FilePath" Visible="False" />
                                        <telerik:GridBoundColumn DataField="CurrentCheckoutByRole" UniqueName="CurrentCheckoutByRole" Visible="True" Display="False" />
                                        <%--2--%>
                                        <%--<telerik:GridTemplateColumn UniqueName="IsSelected" AllowFiltering="false" Display="false">
                                                <HeaderStyle Width="24"  />
                                                <ItemStyle HorizontalAlign="Center"/>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="IsSelected" runat="server" />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>--%>
                                        <telerik:GridClientSelectColumn UniqueName="IsSelected">
                                            <HeaderStyle Width="24" />
                                            <ItemStyle HorizontalAlign="Center" Width="24" />
                                        </telerik:GridClientSelectColumn>
                                        <%-- <telerik:GridEditCommandColumn ButtonType="ImageButton" EditImageUrl="~/Images/edit.png" 
                                                UpdateImageUrl="~/Images/ok.png" CancelImageUrl="~/Images/delete.png" UniqueName="EditColumn">
                                                <HeaderStyle Width="4%"  />
                                                <ItemStyle HorizontalAlign="Center" Width="4%"/>
                                            </telerik:GridEditCommandColumn>--%>
                                        <telerik:GridTemplateColumn AllowFiltering="False" UniqueName="Status">
                                            <HeaderStyle Width="30" HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Image runat="server" Style="padding-left: 7px;" ID="btncheckout" ToolTip="Document is check out"
                                                    ImageUrl="~/Images/check-out.png" Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsCheckOut")) %>' />
                                                <asp:Image runat="server" Style="padding-left: 7px;" ID="btncheckin" ToolTip="Document is check in"
                                                    ImageUrl="~/Images/check-in.png" Visible='<%# !Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsCheckOut")) %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <%--3--%>
                                        <telerik:GridTemplateColumn AllowFiltering="False" Display="False">
                                            <HeaderStyle Width="30" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <a href='javascript:ShowEditForm(<%# DataBinder.Eval(Container.DataItem, "ID") %>)' style="text-decoration: none; color: blue">
                                                    <asp:Image ID="EditLink" runat="server" ImageUrl="~/Images/edit.png" Style="cursor: pointer;" AlternateText="Edit properties" />
                                                </a>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <%--4--%>
                                        <telerik:GridTemplateColumn AllowFiltering="False" UniqueName="DeleteColumn">
                                            <HeaderStyle Width="30" HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <!--BtnDeleteClick// ||
                                                (Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsCheckOut")) && DataBinder.Eval(Container.DataItem, "CurrentCheckoutByRole").ToString() == CurrentRoleId.Value)-->
                                                <asp:ImageButton runat="server" Style="padding-left: 7px; cursor: pointer;" ID="btnDelete"
                                                    ImageUrl="~/Images/delete.png" CommandName="Delete"
                                                    Visible='<%# !Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsCheckOut")) %>'
                                                    OnClientClick="return confirm('Do you want to delete document?');" />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <%--<telerik:GridButtonColumn UniqueName="DeleteColumn" CommandName="Delete" ConfirmText="Do you want to delete document?" ButtonType="ImageButton" ImageUrl="~/Images/delete.png">
                                                <HeaderStyle Width="2%" />
                                                 <ItemStyle HorizontalAlign="Center" Width="2%"  />
                                            </telerik:GridButtonColumn>--%>
                                        <%--5--%>
                                        <telerik:GridTemplateColumn AllowFiltering="false" UniqueName="DownloadColumn">
                                            <HeaderStyle Width="30" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <div runat="server" id="ViewForFolder" visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsFolder")) %>'>
                                                    <asp:Image ID="Image1" runat="server"
                                                        ImageUrl='<%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon") %>' />
                                                </div>
                                                <div runat="server" id="ViewForDoc" visible='<%# !Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsFolder")) %>'>
                                                    <div runat="server" id="DocumentFile" visible='<%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon")!=null %>'>
                                                        <a download='<%# DataBinder.Eval(Container.DataItem, "Name") %>'
                                                            href='<%# DataBinder.Eval(Container.DataItem, "FilePath") %>' target="_blank">
                                                            <asp:Image ID="CallLink" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon") %>'
                                                                Style="cursor: pointer;" AlternateText="Download document" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>

                                        <telerik:GridTemplateColumn AllowFiltering="False" UniqueName="Viewfile">
                                            <HeaderStyle Width="30" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <div runat="server" id="ViewFilePDF"  visible='<%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon")!=null &&  DataBinder.Eval(Container.DataItem, "FileExtensionIcon").ToString() == "images/pdffile.png" %>'>
                                                    <a href='javascript:ShowViewerFile(<%# DataBinder.Eval(Container.DataItem, "ID") %>)' style="text-decoration: none; color: blue">
                                                        <asp:Image ID="CallLink2" runat="server" ImageUrl="~/Images/expview.png" Style="cursor: pointer; border-color: transparent" AlternateText="Viewer File PDF" ToolTip="View file" />
                                                    <a />
                                                </div>
                                                <div runat="server" id="VideoFile" visible='<%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon")!=null &&  DataBinder.Eval(Container.DataItem, "FileExtensionIcon").ToString() == "images/video.png" %>'>
                                                    <a href='javascript:ShowVideoForm(<%# DataBinder.Eval(Container.DataItem, "ID") %>)' style="text-decoration: none; color: blue">
                                                        <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/expview.png" Style="cursor: pointer; border-color: transparent" AlternateText="Viewer Video" ToolTip="View Video" />
                                                    </a>
                                                </div>
                                                <div runat="server" id="PictureFile" visible='<%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon")!=null && DataBinder.Eval(Container.DataItem, "FileExtensionIcon").ToString() == "images/picture.png" %>'>
                                                    <a href='javascript:ShowImageForm(<%# DataBinder.Eval(Container.DataItem, "ID") %>)'>
                                                        <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/expview.png" Style="cursor: pointer; border-color: transparent" AlternateText='<%# DataBinder.Eval(Container.DataItem, "Title") %>'  ToolTip="View Image" />
                                                    </a>
                                                </div>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>

                                        <%--6--%>
                                        <telerik:GridTemplateColumn UniqueName="Name" HeaderText="File Name"
                                            DataField="Name" ShowFilterIcon="False" FilterControlWidth="97%" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle Width="170" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <div runat="server" id="ViewNameForFolder" visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsFolder")) %>'>
                                                    <a href='javascript:ShowChildFolder(<%# DataBinder.Eval(Container.DataItem, "ID") %>)' style="text-decoration: none; color: blue">
                                                        <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>'></asp:Label>
                                                    </a>
                                                </div>
                                                <div runat="server" id="ViewNameForDoc" visible='<%# !Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsFolder")) %>'>
                                                    <a href='javascript:ShowEditPropertises(<%# DataBinder.Eval(Container.DataItem, "ID") %>)' style="text-decoration: none; color: blue">
                                                        <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>'></asp:Label>
                                                    </a>
                                                    <asp:Image ID="newicon" runat="server" ImageUrl="Images/new.png" Visible='<%# (DateTime.Now - Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "CreatedDate"))).TotalHours < 24 %>' />
                                                </div>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <%--7--%>
                                        <telerik:GridBoundColumn HeaderText="Document Number" UniqueName="DocumentNumber"
                                            FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                            DataField="DocumentNumber">
                                            <HeaderStyle HorizontalAlign="Center" Width="170" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <%--8--%>
                                        <telerik:GridBoundColumn HeaderText="Title" UniqueName="Title"
                                            FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                            DataField="Title">
                                            <HeaderStyle HorizontalAlign="Center" Width="300" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <%--9--%>
                                        <telerik:GridBoundColumn HeaderText="Revision" UniqueName="Revision"
                                            FilterControlWidth="95%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                            DataField="RevisionName">
                                            <HeaderStyle HorizontalAlign="Center" Width="60" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </telerik:GridBoundColumn>
                                        <%--10--%>
                                        <telerik:GridBoundColumn DataField="StatusName" HeaderText="Status" UniqueName="Status" Display="False"
                                            FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="120" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <%--11--%>
                                        <telerik:GridBoundColumn DataField="DisciplineName" HeaderText="Discipline" UniqueName="Discipline" Display="False"
                                            FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="150" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <%--12--%>
                                        <telerik:GridBoundColumn DataField="DocumentTypeName" HeaderText="Document Type" UniqueName="DocumentType"
                                            FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="150" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <%--13--%>
                                        <telerik:GridTemplateColumn HeaderText="Expiry Date" UniqueName="ValidDate" AllowFiltering="False" Display="False">
                                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Eval("ValidDate","{0:dd/MM/yyyy}") %>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <%--14--%>
                                        <telerik:GridBoundColumn DataField="ReceivedFromName" Display="False" HeaderText="Received From" UniqueName="ReceivedFrom" AllowFiltering="False">
                                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <%--15--%>
                                        <telerik:GridBoundColumn DataField="TransmittalNumber" HeaderText="Trans No." UniqueName="TransmittalNumber" Display="False" AllowFiltering="False">
                                            <HeaderStyle HorizontalAlign="Center" Width="160" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <%--16--%>
                                        <telerik:GridTemplateColumn HeaderText="Received Date" Display="False" UniqueName="ReceivedDate" AllowFiltering="False">
                                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Eval("ReceivedDate","{0:dd/MM/yyyy}") %>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <%--17--%>
                                        <telerik:GridBoundColumn DataField="Remark" HeaderText="Remark" UniqueName="Remark" AllowFiltering="False">
                                            <HeaderStyle HorizontalAlign="Center" Width="100" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Well" HeaderText="Well" UniqueName="Well" AllowFiltering="False" Visible="False">
                                            <HeaderStyle HorizontalAlign="Center" Width="150" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="OrderNo" HeaderText="Order" UniqueName="OrderNo" AllowFiltering="False">
                                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn HeaderText="Dispatch Date" UniqueName="DateLetterIn" AllowFiltering="False">
                                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Eval("DateLetterIn","{0:dd/MM/yyyy}") %>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Date Outgoing" UniqueName="DateLetterOut" AllowFiltering="False" Display="False">
                                            <HeaderStyle HorizontalAlign="Center" Width="80" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Eval("DateLetterOut","{0:dd/MM/yyyy}") %>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>

                                        
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings Selecting-AllowRowSelect="true" AllowColumnHide="True">
                                    <ClientEvents OnRowContextMenu="RowContextMenu"
                                        OnRowClick="RowClick"
                                        OnGridCreated="GetGridObject" />
                                    <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="500" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </telerik:RadPane>
                    </telerik:RadSplitter>
                </telerik:RadPane>
                <%--<telerik:RadSplitBar ID="Radsplitbar4" runat="server" CollapseMode="Both">
                </telerik:RadSplitBar>
                <telerik:RadPane ID="Radpane5" runat="server" Scrolling="None" style="height: 100%" >
                    <div runat="server" id="divContainer" style="width: 100%; height: 100%;">
                        <input type="hidden" id="lblDocID" runat="server" />
                        <div id="divContainerRight">
                            <div class="exampleWrapper">
                                <%--<div id="divLoading" style="display:none;top:30px;left:0px; position:relative;">Loading</div>
                                <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" 
                                    CssClass="multiPage" >
                                    <telerik:RadPageView ID="RadPageView2"  runat="server" CssClass="EDMsRadPageView2"/>
                                    <telerik:RadPageView ID="RadPageView3"  runat="server" CssClass="EDMsRadPageView3"/>
                                </telerik:RadMultiPage>
                                <telerik:RadTabStrip ID="RadTabStrip1" SelectedIndex="0" Width="100%"
                                    CssClass="tabStrip" runat="server" MultiPageID="RadMultiPage1"
                                    OnTabClick="RadTabStrip1_TabClick">
                                    <Tabs>
                                        <telerik:RadTab Text="Revision history">
                                        </telerik:RadTab>
                                        <telerik:RadTab Text="Version history">
                                        </telerik:RadTab>
                                    </Tabs>
                                </telerik:RadTabStrip>
                            </div>
                        </div>
                    </div>
                </telerik:RadPane>--%>
            </telerik:RadSplitter>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <telerik:RadContextMenu ID="radMenu" runat="server"
        EnableRoundedCorners="true" EnableShadows="true" OnClientItemClicking="gridMenuClicking" OnClientShowing="gridContextMenuShowing">
        <Items>
            <telerik:RadMenuItem Text="Edit properties" ImageUrl="~/Images/edit.png" Value="EditDocument" />
            <telerik:RadMenuItem IsSeparator="True" />
            <telerik:RadMenuItem Text="Revision history" ImageUrl="~/Images/revision.png" Value="RevisionHistory" />
            <%--<telerik:RadMenuItem Text="Version history" ImageUrl="~/Images/history.png" Value="VersionHistory" Visible="False" />
            <telerik:RadMenuItem Text="Checkin/out history" ImageUrl="~/Images/lock3.png" Value="CheckOutInHistory" />
            <telerik:RadMenuItem IsSeparator="True" Visible="False" />
            <telerik:RadMenuItem Text="Check out" ImageUrl="~/Images/checkout.png" Value="CheckOut" />
            <telerik:RadMenuItem Text="Check in" ImageUrl="~/Images/checkin.png" Value="CheckIn" />--%>
            <telerik:RadMenuItem IsSeparator="True" />
            <telerik:RadMenuItem Text="Permission" ImageUrl="~/Images/datap.png" Value="Permission" />
            <telerik:RadMenuItem Text="Attach File" ImageUrl="~/Images/revision.png" Value="AttachFile" Visible="false" />
        </Items>
    </telerik:RadContextMenu>
    <span style="display: none">
        <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <ClientEvents OnRequestStart="onRequestStart"></ClientEvents>
            <AjaxSettings>

                <%--<telerik:AjaxSetting AjaxControlID="radPbCategories">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="radTreeFolder" LoadingPanelID="RadAjaxLoadingPanel2"/>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"/>
                    </UpdatedControls>
                </telerik:AjaxSetting>--%>

                <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="radTreeFolder" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="radMenu">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="radTreeFolder">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="radTreeFolder" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="CustomerMenu"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="grdDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContainer" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="radViewSettingBar">
                    <UpdatedControls>
                        <%--<telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>--%>
                        <telerik:AjaxUpdatedControl ControlID="SlidingZone1" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ckbEnableFilter">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
    </span>



    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="CustomerDialog" runat="server" Title="Document Information"
                VisibleStatusbar="false" Height="500" Width="650"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="ViewerFile" runat="server" Title="Viewer File"
                VisibleStatusbar="false" Height="600" Width="1250" MinHeight="600" MinWidth="1250" MaxHeight="600" MaxWidth="1250" 
                Left="50px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="VideoView" runat="server" Title="Video Viewer" OnClientBeforeClose="OnClientBeforeClose"
                VisibleStatusbar="false" Height="550" Width="800"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="ImageView" runat="server" Title="Image Viewer" VisibleStatusbar="false" Height="550" Width="800"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="CheckOut" runat="server" Title="Checkout Document"
                VisibleStatusbar="false" Height="250" Width="650"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="MoveFolder" runat="server" Title="Move Files" OnClientClose="refreshGrid"
                VisibleStatusbar="false" Height="500" Width="650"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="FolderPermission" runat="server" Title="Folder Permission"
                VisibleStatusbar="false" Height="600" Width="650"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="DocPermission" runat="server" Title="Document Permission"
                VisibleStatusbar="false" Height="600" Width="650"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="UploadMulti" runat="server" Title="Create multiple documents"
                VisibleStatusbar="true" Height="520" MinHeight="520" MaxHeight="520" Width="640" MinWidth="640" MaxWidth="640"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true" OnClientClose="refreshGrid">
            </telerik:RadWindow>

            <telerik:RadWindow ID="RevisionDialog" runat="server" Title="Revision history" OnClientClose="refreshGrid"
                VisibleStatusbar="false" Height="600" Width="1250" MinHeight="600" MinWidth="1250" MaxHeight="600" MaxWidth="1250"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="AttachFileDialog" runat="server" Title="Attach File" OnClientClose="refreshGrid"
                VisibleStatusbar="false" Height="600" Width="1250" MinHeight="600" MinWidth="1250" MaxHeight="600" MaxWidth="1250"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="SendMail" runat="server" Title="Send mail"
                VisibleStatusbar="false" Height="560" Width="992" MinHeight="560" MinWidth="992" MaxHeight="560" MaxWidth="992"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="VersionHistory" runat="server" Title="Checkout/in history"
                VisibleStatusbar="false" Height="500" Width="1000" MinHeight="500" MinWidth="1000" MaxHeight="500" MaxWidth="1000"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="ImportData" runat="server" Title="Upload data file"
                VisibleStatusbar="false" Height="350" Width="480"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="ExportData" runat="server" Title="Export master list"
                VisibleStatusbar="false" Height="350" Width="480"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="CopyPath" runat="server" Title="Copy to Clipboard"
                VisibleStatusbar="false" Height="130" Width="480"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="CheckIn" runat="server" Title="Check in document"
                VisibleStatusbar="false" Height="350" Width="480"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" />
    <asp:HiddenField runat="server" ID="IsAdminGroup" />
    <asp:HiddenField runat="server" ID="IsUpdatePermission" />
    <asp:HiddenField runat="server" ID="FolderContextMenuAction" />
    <asp:HiddenField runat="server" ID="lblFolderId" />
    <asp:HiddenField runat="server" ID="lblDocId" />
    <asp:HiddenField runat="server" ID="lblCategoryId" />
    <asp:HiddenField runat="server" ID="isCheckOut" />
    <asp:HiddenField runat="server" ID="CurrentRoleId" />
    <asp:HiddenField runat="server" ID="CurrentCheckoutRoleId" />
    <input type="hidden" id="radGridClickedRowIndex" name="radGridClickedRowIndex" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">


        <%--<script src="Scripts/jquery-1.7.1.js"></script>--%>


        <script type="text/javascript">
            $(document).ready(function () {
                $("a#example7").fancybox({
                    'titlePosition': 'inside'
                });


            });
        </script>
        <script type="text/javascript">



            var radDocuments;

            function ShowFilter(obj) {
                if (obj.checked) {
                    $find('<%=grdDocument.ClientID %>').get_masterTableView().showFilterItem();
                } else {
                    $find('<%=grdDocument.ClientID %>').get_masterTableView().hideFilterItem();
                }
            }

            function refreshGrid() {
                var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView();
                masterTable.rebind();
            }

            function refreshTreeFolder() {
                ajaxManager.ajaxRequest("RebindTreeFolder");
            }

            function ExportGrid() {
                var masterTable = $find("<%=grdDocument.ClientID %>").get_masterTableView();
                masterTable.exportToExcel('PIN_list.xls');
                return false;
            }

            function GetGridObject(sender, eventArgs) {
                radDocuments = sender;
            }

            function onRequestStart(sender, args) {

                //alert(args.get_eventTarget());
                if (args.get_eventTarget().indexOf("ExportTo") >= 0 || args.get_eventTarget().indexOf("ajaxCustomer") >= 0) {
                    args.set_enableAjax(false);
                }
            }

            function onColumnHidden(sender) {

                var masterTableView = sender.get_masterTableView().get_element();
                masterTableView.style.tableLayout = "auto";
                //window.setTimeout(function () { masterTableView.style.tableLayout = "auto"; }, 0);
            }

            // Undocked and Docked event slide bar Tree folder
            function OnClientUndocked(sender, args) {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;

                ////radDocuments.get_masterTableView().showColumn(9);
                radDocuments.get_masterTableView().showColumn(10);
                radDocuments.get_masterTableView().showColumn(11);
                radDocuments.get_masterTableView().showColumn(12);

                radDocuments.get_masterTableView().showColumn(14);
                radDocuments.get_masterTableView().showColumn(15);
                radDocuments.get_masterTableView().showColumn(16);
                //radDocuments.get_masterTableView().showColumn(16);
                radDocuments.get_masterTableView().showColumn(17);


                ////if (selectedFolder != "") {
                ////    ajaxManager.ajaxRequest("ListAllDocuments");
                ////}
            }

            function OnClientDocked(sender, args) {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;

                ////radDocuments.get_masterTableView().hideColumn(9);
                radDocuments.get_masterTableView().hideColumn(10);
                radDocuments.get_masterTableView().hideColumn(11);
                radDocuments.get_masterTableView().hideColumn(12);

                radDocuments.get_masterTableView().hideColumn(14);
                radDocuments.get_masterTableView().hideColumn(15);
                radDocuments.get_masterTableView().hideColumn(16);
                //radDocuments.get_masterTableView().hideColumn(16);
                radDocuments.get_masterTableView().hideColumn(17);


                ////if (selectedFolder != "") {
                ////    ajaxManager.ajaxRequest("TreeView");
                ////}
            }

            function RowClick(sender, eventArgs) {
                var Id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblDocId.ClientID %>").value = Id;
            }

            function gridMenuClicking(sender, args) {
                var itemValue = args.get_item().get_value();
                var docId = document.getElementById("<%= lblDocId.ClientID %>").value;
                var folderId = document.getElementById("<%= lblFolderId.ClientID %>").value;
                var ischeckout = document.getElementById("<%= isCheckOut.ClientID %>").value;
                if (ischeckout != "True" || itemValue == "CheckIn") {
                    switch (itemValue) {
                        case "RevisionHistory":
                            var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
                            var owd = $find("<%=RevisionDialog.ClientID %>");
                            owd.Show();
                            owd.setUrl("Controls/Document/RevisionHistory.aspx?docId=" + docId + "&categoryId=" + categoryId, "RevisionDialog");
                            break;
                        case "AttachFile":
                            var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
                            var owd = $find("<%= AttachFileDialog.ClientID %>");
                            owd.Show();
                            owd.setUrl("Controls/Document/AttachDocument.aspx?docId=" + docId + "&folderId=" + folderId, "AttachFileDialog");
                            break;
                        case "VersionHistory":
                            var owd = $find("<%=VersionHistory.ClientID %>");
                            owd.Show();
                            owd.setUrl("Controls/Document/VersionHistory.aspx?docId=" + docId, "VersionHistory");
                            break;
                        case "EditDocument":
                            ShowEditForm(docId);
                            break;
                        case "CheckOut":
                            var owd = $find("<%=CheckOut.ClientID %>");
                            owd.Show();
                            owd.setUrl("Controls/Document/CheckOutDocument.aspx?docId=" + docId, "CheckOut");
                            break;
                        case "CheckIn":
                            //ajaxManager.ajaxRequest("CheckInDocument");
                            var owd = $find("<%=CheckIn.ClientID %>");
                            owd.Show();
                            owd.setUrl("Controls/Document/Checkin.aspx?docId=" + docId, "CheckIn");
                            break;
                        case "CheckOutInHistory":
                            var owd = $find("<%=VersionHistory.ClientID %>");
                            owd.Show();
                            owd.setUrl("Controls/Document/CheckOutInHistoryList.aspx?docId=" + docId, "VersionHistory");
                            break;
                        case "Permission":
                            var owd = $find("<%=DocPermission.ClientID %>");
                            owd.Show();
                            owd.setUrl("Controls/Document/SpecialDocumentPermission.aspx?docId=" + docId + "&folderId=" + folderId, "DocPermission");
                            break;
                    }
                }
                else { alert("Document is check out!") }
            }

            function onClientContextMenuItemClicking(sender, args) {
                var menuItem = args.get_menuItem();
                var treeNode = args.get_node();
                menuItem.get_menu().hide();

                switch (menuItem.get_value()) {
                    case "Rename":
                        treeNode.startEdit();
                        break;
                    case "Delete":
                        var result = confirm("Are you sure you want to delete the folder: " + treeNode.get_text());
                        args.set_cancel(!result);
                        break;
                    case "MoveFolder":
                        var owd = $find("<%=MoveFolder.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/MoveFileAndFolder.aspx?mode=1&folderId=" + treeNode.get_value(), "MoveFolder");
                        break;
                    case "Permission":
                        ////var childNodeIds = "";

                        ////if (treeNode._hasChildren() == true) {
                        ////    var allNodes = treeNode.get_allNodes();
                        ////    for (var i = 0; i < allNodes.length; i++) {
                        ////        childNodeIds += allNodes[i].get_value() + "*";

                        ////    }
                        ////}
                        ////alert(treeNode.get_value());

                        ////Set_Cookie("allchildfolder", childNodeIds, 1);

                        ////alert(childNodeIds);
                        ////alert(document.cookie);

                        var owd = $find("<%=FolderPermission.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/FolderPermission.aspx?folderId=" + treeNode.get_value(), "FolderPermission");
                        break;
                    case "CopyPath":
                        var owd = $find("<%=CopyPath.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/CopyPath.aspx?folderId=" + treeNode.get_value(), "FolderPermission");
                        break;
                }
            }

            function gridContextMenuShowing(menu, args) {
                var isCheckOut = document.getElementById("<%= isCheckOut.ClientID %>").value;
                var currentCheckoutRoleId = document.getElementById("<%= CurrentCheckoutRoleId.ClientID %>").value;
                var currentRoleId = document.getElementById("<%= CurrentRoleId.ClientID %>").value;
                var IsAdminGroup = document.getElementById("<%= IsAdminGroup.ClientID %>").value;

                ////alert(currentRoleId);
                ////alert(currentCheckoutRoleId);
                //if (isCheckOut == "True") {


                //    if (currentCheckoutRoleId == currentRoleId) {
                //        menu.get_allItems()[0].enable();
                //        menu.get_allItems()[2].enable();
                //        menu.get_allItems()[5].disable();
                //        menu.get_allItems()[6].enable();
                //    }
                //    else {
                //        menu.get_allItems()[0].disable();
                //        menu.get_allItems()[2].disable();
                //        menu.get_allItems()[5].disable();
                //        menu.get_allItems()[6].disable();
                //    }
                //}
                //else {
                //    menu.get_allItems()[5].enable();
                //    menu.get_allItems()[6].disable();


                //}

                //if (IsAdminGroup == "true") {
                //    menu.get_allItems()[7].show();
                //    menu.get_allItems()[8].show();
                //} else {
                //    menu.get_allItems()[7].hide();
                //    menu.get_allItems()[8].hide();
                //}
            }

            function onClientContextMenuShowing(sender, args) {
                var treeNode = args.get_node();
                var flag = treeNode.get_category();
                //alert(flag);
                var allNodes = treeNode.get_allNodes();
                var isUpdatePermission = document.getElementById("<%= IsUpdatePermission.ClientID %>").value;
                if (isUpdatePermission == "false") {
                    args.get_menu().get_items().getItem(0).set_enabled(false);
                    args.get_menu().get_items().getItem(1).set_enabled(false);
                    args.get_menu().get_items().getItem(2).set_enabled(false);
                    args.get_menu().get_items().getItem(3).set_enabled(false);
                    args.get_menu().get_items().getItem(4).set_enabled(false);
                    //args.get_menu().get_items().getItem(5).set_enabled(false);
                } else {
                    //if (allNodes.length == 0) {
                    //    //0 refer to the Context Menu item index
                    //    args.get_menu().get_items().getItem(4).set_enabled(true);
                    //}
                    //else {
                    //    args.get_menu().get_items().getItem(4).set_enabled(false);
                    //}

                    if (treeNode.get_level() == 0) {
                        args.get_menu().get_items().getItem(0).set_enabled(true);

                        args.get_menu().get_items().getItem(1).set_enabled(false);
                        args.get_menu().get_items().getItem(2).set_enabled(false);

                        args.get_menu().get_items().getItem(3).set_enabled(true);
                        args.get_menu().get_items().getItem(4).set_enabled(true);
                    }
                    else {
                        args.get_menu().get_items().getItem(0).set_enabled(true);
                        args.get_menu().get_items().getItem(1).set_enabled(true);
                        args.get_menu().get_items().getItem(2).set_enabled(true);
                        args.get_menu().get_items().getItem(3).set_enabled(true);
                        args.get_menu().get_items().getItem(4).set_enabled(true);
                    }
                }
            }

            function rtvExplore_OnNodeExpandedCollapsed(sender, eventArgs) {
                var allNodes = eventArgs._node.get_treeView().get_allNodes();

                var i;
                var selectedNodes = "";

                for (i = 0; i < allNodes.length; i++) {
                    if (allNodes[i].get_expanded())
                        selectedNodes += allNodes[i].get_value() + "*";
                }

                Set_Cookie("expandedNodes", selectedNodes, 30);
            }



            function Set_Cookie(name, value, expires, path, domain, secure) {
                // set time, it's in milliseconds
                var today = new Date();
                today.setTime(today.getTime());

                /*
                if the expires variable is set, make the correct 
                expires time, the current script below will set 
                it for x number of days, to make it for hours, 
                delete * 24, for minutes, delete * 60 * 24
                */
                if (expires) {
                    expires = expires * 1000 * 60 * 60 * 24;
                }
                var expires_date = new Date(today.getTime() + (expires));

                document.cookie = name + "=" + escape(value) +
                ((expires) ? ";expires=" + expires_date.toGMTString() : "") +
                ((path) ? ";path=" + path : "") +
                ((domain) ? ";domain=" + domain : "") +
                ((secure) ? ";secure" : "");
            }
        </script>
        <script type="text/javascript">
            /* <![CDATA[ */
            var toolbar;
            var searchButton;
            var ajaxManager;

            function pageLoad() {
                $('iframe').load(function () { //The function below executes once the iframe has finished loading<---true dat, althoug Is coppypasta from I don't know where
                    //alert($('iframe').contents());
                });

                toolbar = $find("<%= CustomerMenu.ClientID %>");
                ajaxManager = $find("<%=ajaxCustomer.ClientID %>");

                searchButton = toolbar.findButtonByCommandName("doSearch");

                $telerik.$(".searchtextbox")
                    .bind("keypress", function (e) {
                        searchButton.set_imageUrl("images/search.gif");
                        searchButton.set_value("search");
                    });
            }

            function ShowEditForm(id) {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/DocumentInfoEditForm.aspx?docId=" + id + "&folId=" + selectedFolder + "&categoryId=" + categoryId, "CustomerDialog");

                // window.radopen("Controls/Customers/CustomerEditForm.aspx?patientId=" + id, "CustomerDialog");
                //  return false;
            }

            function ShowViewerFile(Id) {
                
               // if (path.toLowerCase().search(".pdf")) {
                var owd = $find("<%= ViewerFile.ClientID %>");
                owd.Show();
                owd.moveTo(120, 60);
                owd.setUrl("ViewerFilePDF.aspx?DocId=" + Id + "&File=0", "ViewerFile");
                //}
               
             }

            function ShowVideoForm(id) {
                var owd = $find("<%=VideoView.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/ViewVideoFile.aspx?docId=" + id, "VideoView");

                // window.radopen("Controls/Customers/CustomerEditForm.aspx?patientId=" + id, "CustomerDialog");
                //  return false;
            }
            function ShowImageForm(id) {
                var owd = $find("<%=ImageView.ClientID %>");
                owd.Show();
                owd.maximize(true);
                owd.setUrl("Controls/Document/ViewImage.aspx?docId=" + id, "ImageView");

                // window.radopen("Controls/Customers/CustomerEditForm.aspx?patientId=" + id, "CustomerDialog");
                //  return false;
            }
            function ShowEditPropertises(id) {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/DocumentInfoEditForm.aspx?docId=" + id + "&folId=" + selectedFolder + "&categoryId=" + categoryId, "CustomerDialog");
            }
            function ShowChildFolder(folderId) {
                ajaxManager.ajaxRequest("ShowChildFolder_" + folderId);
            }



            function ShowInsertForm() {

                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Customers/CustomerEditForm.aspx", "CustomerDialog");

                //window.radopen("Controls/Customers/CustomerEditForm.aspx", "CustomerDialog");
                //return false;
            }



            function refreshGrid(arg) {
                //alert(arg);
                if (!arg) {
                    ajaxManager.ajaxRequest("Rebind");
                }
                else {
                    ajaxManager.ajaxRequest("RebindAndNavigate");
                }
            }

            function refreshTab(arg) {
                $('.EDMsRadPageView' + arg + ' iframe').attr('src', $('.EDMsRadPageView' + arg + ' iframe').attr('src'));
            }

            function RowDblClick(sender, eventArgs) {
                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Customers/ViewCustomerDetails.aspx?docId=" + eventArgs.getDataKeyValue("Id"), "CustomerDialog");
                // window.radopen("Controls/Customers/ViewCustomerDetails.aspx?patientId=" + eventArgs.getDataKeyValue("Id"), "CustomerDialog");
            }

            function onNodeClicking(sender, args) {
                var folderValue = args.get_node().get_value();
                document.getElementById("<%= lblFolderId.ClientID %>").value = folderValue;
            }

            function radPbCategories_OnClientItemClicking(sender, args) {
                var item = args.get_item();
                var categoryId = item.get_value();
                document.getElementById("<%= lblCategoryId.ClientID %>").value = categoryId;

            }

            function radViewSettingBar_OnClientButtonClicking(sender, args) {
                var button = args.get_item();
                var strText = button.get_text();
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                var paneID = "<%= RadSlidingPane1.ClientID %>";
                var slidingZone = $find("<%=SlidingZone1.ClientID %>");
                var dockedPaneId = slidingZone.get_dockedPaneId();

                if (strText.toLowerCase() == "list all documents") {
                    if (selectedFolder == "") {
                        alert("Please choice one folder to view list documents");
                        return false;
                    }
                    else {
                        slidingZone.undockPane(paneID);
                        ajaxManager.ajaxRequest("ListAllDocuments");
                    }
                }

                if (strText.toLowerCase() == "tree view") {
                    if (selectedFolder == "") {
                        alert("Please choice one folder to view documents");
                        return false;
                    }
                    else {
                        if (dockedPaneId == null || dockedPaneId != paneID) {
                            slidingZone.collapsePane(paneID);
                            slidingZone.dockPane(paneID);
                        }
                        ajaxManager.ajaxRequest("TreeView");
                    }
                }
                return false;
            }

            function OnClientButtonClicking(sender, args) {
                var button = args.get_item();
                var strText = button.get_text();
                var strValue = button.get_value();

                var grid = $find("<%= grdDocument.ClientID %>");
                var customerId = null;
                var customerName = "";

                //if (grid.get_masterTableView().get_selectedItems().length > 0) {
                //    var selectedRow = grid.get_masterTableView().get_selectedItems()[0];
                //    customerId = selectedRow.getDataKeyValue("Id");
                //    //customerName = selectedRow.Items["FullName"]; 
                //    //customerName = grid.get_masterTableView().getCellByColumnUniqueName(selectedRow, "FullName").innerHTML;
                //}Send notifications window.open('file://Shared Folder/Users')

                if (strText.toLowerCase() == "send notifications") {
                    var grid = $find("<%=grdDocument.ClientID %>");
                    var masterTable = grid.get_masterTableView();
                    var number = 0;
                    var selectedRows = masterTable.get_selectedItems();
                    for (var i = 0; i < selectedRows.length; i++) {
                        number++;
                    }
                    if (number == 0) {
                        alert("Please select documents to send notification");
                    }
                    else {
                        ajaxManager.ajaxRequest("SendNotification");
                    }
                }

                if (strText.toLowerCase() == "import data file") {
                    var owd = $find("<%=ImportData.ClientID %>");
                    owd.Show();
                    owd.setUrl("Controls/Document/ImportData.aspx", "ImportData");
                }

                if (strText.toLowerCase() == "export master list") {
                    <%--var owd = $find("<%=ExportData.ClientID %>");
                    owd.Show();
                    owd.setUrl("Controls/Document/ExportMasterList.aspx", "ExportData");--%>

                    ajaxManager.ajaxRequest("ExportMasterList");
                }

                if (strText.toLowerCase() == "move files") {
                    var grid = $find("<%=grdDocument.ClientID %>");
                    var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                    var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
                    var masterTable = grid.get_masterTableView();
                    var number = 0;
                    var listId = "";
                    var selectedRows = masterTable.get_selectedItems();
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        number++;
                        row.getDataKeyValue("ID") + ",";
                    }
                    if (number == 0) {
                        alert("Please select documents to move");
                    }
                    else {
                        var owd = $find("<%=MoveFolder.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/MoveFileAndFolder.aspx?mode=2&docIds=" + listId + "&selectedFolder=" + selectedFolder + "&categoryId=" + categoryId, "MoveFolder");
                    }
                }

                if (strText.toLowerCase() == "checkout selected documents") {
                    var grid = $find("<%=grdDocument.ClientID %>");
                    var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                    var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
                    var masterTable = grid.get_masterTableView();
                    var number = 0;
                    var listId = "";
                    var selectedRows = masterTable.get_selectedItems();
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        number++;
                        listId += row.getDataKeyValue("ID") + ",";
                    }
                    if (number == 0) {
                        alert("Please select documents to checkout");
                    }
                    else {
                        var owd = $find("<%=CheckOut.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/CheckOutDocument.aspx?docId=" + listId, "CheckOut");
                    }
                }

                if (strText.toLowerCase() == "checkin selected documents") {
                    var grid = $find("<%=grdDocument.ClientID %>");
                    var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                    var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
                    var masterTable = grid.get_masterTableView();
                    var number = 0;
                    var listId = "";
                    var selectedRows = masterTable.get_selectedItems();
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        number++;
                        listId += row.getDataKeyValue("ID") + ",";
                    }
                    if (number == 0) {
                        alert("Please select documents to checkin");
                    }
                    else {
                        ajaxManager.ajaxRequest("CheckinMultiDocument");
                    }
                }

                if (strText.toLowerCase() == "download multi documents") {
                    var grid = $find("<%=grdDocument.ClientID %>");
                    var masterTable = grid.get_masterTableView();

                    var listId = "";

                    var selectedRows = masterTable.get_selectedItems();
                    if (selectedRows.length == 0) {
                        alert("Please select documents to download");
                    }
                    if (number == 0) {
                        alert("Please select documents to download");
                    }
                    else if (number > 100) {
                        alert("The maximum file download at once time is 100. Current you select " + number + "files.");
                    }
                    else {
                        ajaxManager.ajaxRequest("DownloadMulti");
                    }
                }

                if (strText.toLowerCase() == "up folder") {
                    ajaxManager.ajaxRequest("UpFolder");
                }

                if (strText.toLowerCase() == "send mail") {
                    var grid = $find("<%=grdDocument.ClientID %>");
                    var masterTable = grid.get_masterTableView();
                    var number = 0;
                    var listId = "";
                    var selectedRows = masterTable.get_selectedItems();
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        number++;
                        listId += row.getDataKeyValue("ID") + ",";
                    }
                    if (number == 0) {
                        alert("Please select documents to send mail");
                    }
                    else {
                        var owd = $find("<%=SendMail.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/SendMail.aspx?listDoc=" + listId, "SendMail");
                    }
                }

                if (strText.toLowerCase() == "document") {
                    var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
                    var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                    if (selectedFolder == "") {
                        alert("Please choice one folder to create new document");
                        return false;
                    }

                    var owd = $find("<%=CustomerDialog.ClientID %>");
                    owd.Show();
                    owd.setUrl("Controls/Document/DocumentInfoEditForm.aspx?folId=" + selectedFolder + "&categoryId=" + categoryId, "CustomerDialog");

                }

                if (strText.toLowerCase() == "multi documents") {
                    var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                    if (selectedFolder == "") {
                        alert("Please choice one folder to create new document.");
                        return false;
                    }

                    var owd = $find("<%=UploadMulti.ClientID %>");
                    owd.Show();
                    owd.setUrl("Controls/Document/UploadDragDrop.aspx?folId=" + selectedFolder, "UploadMulti");
                }

                if (strText.toLowerCase() == "export excel") {
                    var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                    if (selectedFolder == "") {
                        alert("Please choice one folder to export master list.");
                        return false;
                    }
                    else {
                        ajaxManager.ajaxRequest("ExportMasterList");
                    }
                }

                if (strText == "Thêm mới") {
                    return ShowInsertForm();
                }
                else if (strText == "Import dữ liệu") {
                    return ShowImportForm();
                }

                else {
                    var commandName = args.get_item().get_commandName();
                    if (commandName == "doSearch") {
                        var searchTextBox = sender.findButtonByCommandName("searchText").findControl("txtSearch");
                        if (searchButton.get_value() == "clear") {
                            searchTextBox.set_value("");
                            searchButton.set_imageUrl("images/search.gif");
                            searchButton.set_value("search");
                        }

                        performSearch(searchTextBox);
                    } else if (commandName == "reply") {
                        window.radopen(null, "Edit");
                    }
                }
            }

            function performSearch(searchTextBox) {
                if (searchTextBox.get_value()) {
                    searchButton.set_imageUrl("images/clear.gif");
                    searchButton.set_value("clear");
                }

                ajaxManager.ajaxRequest(searchTextBox.get_value());
            }
            function onTabSelecting(sender, args) {
                if (args.get_tab().get_pageViewID()) {
                    args.get_tab().set_postBack(false);
                }
            }

            function RowContextMenu(sender, eventArgs) {
                var grid = sender;
                var MasterTable = grid.get_masterTableView(); var row = MasterTable.get_dataItems()[eventArgs.get_itemIndexHierarchical()];
                var cell = MasterTable.getCellByColumnUniqueName(row, "IsCheckOut");
                var cellCurrentCheckoutRoleId = MasterTable.getCellByColumnUniqueName(row, "CurrentCheckoutByRole");

                var isCheckOut = cell.innerHTML == "&nbsp;" ? false : cell.innerHTML;
                document.getElementById("<%= isCheckOut.ClientID %>").value = isCheckOut;

                var CurrentCheckoutRoleId = cellCurrentCheckoutRoleId.innerHTML;
                document.getElementById("<%= CurrentCheckoutRoleId.ClientID %>").value = CurrentCheckoutRoleId;
                //alert(isCheckOut);

                var menu = $find("<%=radMenu.ClientID %>");
                var evt = eventArgs.get_domEvent();

                if (evt.target.tagName == "INPUT" || evt.target.tagName == "A") {
                    return;
                }

                var index = eventArgs.get_itemIndexHierarchical();
                document.getElementById("radGridClickedRowIndex").value = index;

                var Id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblDocId.ClientID %>").value = Id;

                sender.get_masterTableView().selectItem(sender.get_masterTableView().get_dataItems()[index].get_element(), true);
                menu.show(evt);
                evt.cancelBubble = true;
                evt.returnValue = false;

                if (evt.stopPropagation) {
                    evt.stopPropagation();
                    evt.preventDefault();
                }
            }

            function OnClientBeforeClose(sender, args) {
                var oWnd = $find("<%= RadWindowManager1.ClientID %>").getWindowByName("VideoView");
                oWnd.get_contentFrame().contentWindow.CloseVideo();
            }
        </script>
    </telerik:RadCodeBlock>
</asp:Content>
