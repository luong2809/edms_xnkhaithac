﻿<%@ Page Title="EDMS - Xi Nghiep Khai Thac" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="EDMs.Web.Search" EnableViewState="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="Content/styles.css" rel="stylesheet" type="text/css" />
    <!--[if gte IE 8]>
        <style type="text/css">
            #ctl00_ContentPlaceHolder2_grdDocument_ctl00_Header{table-layout:auto !important;}
            #ctl00_ContentPlaceHolder2_grdDocument_ctl00{table-layout:auto !important;}
        </style>
    <![endif]-->

    <style type="text/css">
        /*Custom CSS of Grid documents for FF browser*/
        /*#ctl00_ContentPlaceHolder2_grdDocument_ctl00_Header{table-layout:auto !important;}
        #ctl00_ContentPlaceHolder2_grdDocument_ctl00{table-layout:auto !important;}*/
        /*End*/
        @-moz-document url-prefix() {
            #ctl00_ContentPlaceHolder2_grdDocument_ctl00_Header {
                table-layout: auto !important;
            }

            #ctl00_ContentPlaceHolder2_grdDocument_ctl00 {
                table-layout: auto !important;
            }
        }

        .RadGrid .rgRow td, .RadGrid .rgAltRow td, .RadGrid .rgEditRow td, .RadGrid .rgFooter td, .RadGrid .rgFilterRow td, .RadGrid .rgHeader, .RadGrid .rgResizeCol, .RadGrid .rgGroupHeader td {
            padding-left: 1px !important;
            padding-right: 2px !important;
            vertical-align: top;
        }


        html, body, form {
            overflow: hidden;
        }

        .accordion dt a {
            color: #085B8F;
            border-bottom: 2px solid #46A3D3;
            font-size: 1.5em;
            font-weight: bold;
            letter-spacing: -0.03em;
            line-height: 1.2;
            margin: 0.5em auto 0.6em;
            padding: 0;
            text-align: left;
            text-decoration: none;
            display: block;
        }

        .accordion dt span {
            color: #085B8F;
            border-bottom: 2px solid #46A3D3;
            font-size: 1.5em;
            font-weight: bold;
            letter-spacing: -0.03em;
            line-height: 1.2;
            margin: 0.5em auto 0.6em;
            padding: 0;
            text-align: left;
            text-decoration: none;
            display: block;
        }

        .qlcbTooltip {
            line-height: 1.8;
            padding-right: 5px;
            text-align: right;
        }

        .qlcbFormItem input[type="text"], .qlcbFormItem textarea, .qlcbFormItem select {
            border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;
            border-style: solid;
            border-width: 1px 1px 1px 5px;
            color: #000000;
            float: left;
            font: 12px "segoe ui";
            margin: 0;
            padding: 2px 5px 3px;
            vertical-align: middle;
        }

        html body .riSingle .riTextBox, html body .riSingle .riTextBox[type="text"] {
            border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3 !important;
            border-style: solid !important;
            border-width: 1px 1px 1px 5px !important;
            color: #000000 !important;
            float: left !important;
            font: 12px "segoe ui" !important;
            margin: 0 !important;
            padding: 2px 5px 3px !important;
            vertical-align: middle !important;
        }

        div.RadPicker input.qlcbFormRequired1[type="text"], div.RadPicker_Default input.qlcbFormRequired1[type="text"] {
            border-left-color: Red !important;
            border-left-width: 5px !important;
        }

        .qlcbFormItem input.min25Percent[type="text"], div.qlcbFormItem textarea.min25Percent {
            min-width: 235px;
        }

        .qlcbFormItem input.minFullWidth[type="text"], div.qlcbFormItem textarea.minFullWidth {
            min-width: 626px;
        }

        .qlcbFormItem select.min25Percent {
            min-width: 250px;
        }

        .qlcbFormItem input.min50Percent[type="text"], div.qlcbFormItem textarea.min50Percent, div.qlcbFormItem select.min50Percent {
            min-width: 50%;
        }

        .qlcbFormItem input.qlcbFormRequired[type="text"], div.qlcbFormItem textarea.qlcbFormRequired, div.qlcbFormItem select.qlcbFormRequired {
            border-left-color: #FF0000;
            border-left-width: 5px;
        }

        .qlcbFormItem input.qlcbFormUPPERCASE[type="text"], div.qlcbFormItem textarea.qlcbFormUPPERCASE {
            text-transform: uppercase;
        }

        .qlcbFormItem input[type="text"], div.qlcbFormItem textarea, div.qlcbFormItem select {
            border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;
            border-style: solid;
            border-width: 1px 1px 1px 5px;
            color: #000000;
            float: left;
            font: 12px "segoe ui";
            margin: 0;
            padding: 2px 5px 3px;
            vertical-align: middle;
        }

            .qlcbFormItem input[type="text"]:hover, div.qlcbFormItem select:hover {
                border-color: #000000 #000000 #000000 #46A3D3;
            }

            .qlcbFormItem input.qlcbFormRequired[type="text"]:hover, div.qlcbFormItem select.qlcbFormRequired:hover {
                border-color: #000000 #000000 #000000 #FF0000;
            }

        .RadPicker, div.RadPicker_Default {
            display: inline !important;
            float: left !important;
        }

        .min25Percent {
            min-width: 217px;
        }

        .RadGrid .rgSelectedRow {
            background-image: none !important;
            background-color: coral !important;
        }

        a.tooltip {
            outline: none;
            text-decoration: none;
        }

            a.tooltip strong {
                line-height: 30px;
            }

            a.tooltip:hover {
                text-decoration: none;
            }

            a.tooltip span {
                z-index: 10;
                display: none;
                padding: 14px 20px;
                margin-top: -30px;
                margin-left: 5px;
                width: 240px;
                line-height: 16px;
            }

            a.tooltip:hover span {
                display: inline;
                position: absolute;
                color: #111;
                border: 1px solid #DCA;
                background: #fffAF0;
            }

        .callout {
            z-index: 20;
            position: absolute;
            top: 30px;
            border: 0;
            left: -12px;
        }

        /*CSS3 extras*/
        a.tooltip span {
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            -moz-box-shadow: 5px 5px 8px #CCC;
            -webkit-box-shadow: 5px 5px 8px #CCC;
            box-shadow: 5px 5px 8px #CCC;
        }

        .rgMasterTable {
            table-layout: auto;
        }

        #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_grdDocumentPanel, #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_divContainerPanel {
            height: 100% !important;
        }

        #ctl00_ContentPlaceHolder2_RadPageView1, #ctl00_ContentPlaceHolder2_RadPageView2,
        #ctl00_ContentPlaceHolder2_RadPageView3, #ctl00_ContentPlaceHolder2_RadPageView4,
        #ctl00_ContentPlaceHolder2_RadPageView5 {
            height: 100% !important;
        }

        #divContainerLeft {
            width: 25%;
            float: left;
            margin: 5px;
            height: 99%;
            border-right: 1px dotted green;
            padding-right: 5px;
        }

        #divContainerRight {
            width: 100%;
            float: right;
            margin-top: 5px;
            height: 99%;
        }

        .dotted {
            border: 1px dotted #000;
            border-style: none none dotted;
            color: #fff;
            background-color: #fff;
        }

        .exampleWrapper {
            width: 100%;
            height: 100%;
            /*background: transparent url(images/background.png) no-repeat top left;*/
            position: relative;
        }

        .tabStrip {
            position: absolute;
            top: 0px;
            left: 0px;
        }

        .multiPage {
            position: absolute;
            top: 30px;
            left: 0px;
            color: white;
            width: 100%;
            height: 100%;
        }

        /*Fix RadMenu and RadWindow z-index issue*/
        .radwindow {
            z-index: 8000 !important;
        }

        .TemplateMenu {
            z-index: 10;
        }

        /*Hide change page size control*/
        div.RadGrid .rgPager .rgAdvPart {
            display: none;
        }

        .RadGrid .rgRow td, .RadGrid .rgAltRow td, .RadGrid .rgEditRow td, .RadGrid .rgFooter td, .RadGrid .rgFilterRow td, .RadGrid .rgHeader, .RadGrid .rgResizeCol, .RadGrid .rgGroupHeader td {
            padding-left: 1px !important;
            padding-right: 1px !important;
        }
    </style>
    <telerik:RadPanelBar ID="radPbSearch" runat="server" Width="100%">
        <Items>
            <telerik:RadPanelItem Text="ADVANCE SEARCH" runat="server" Expanded="True" Width="100%" Visible="false">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Advance Search" ImageUrl="Images/search.gif" Selected="True" NavigateUrl="Search.aspx" />
                </Items>
            </telerik:RadPanelItem>
        </Items>
    </telerik:RadPanelBar>
    <telerik:RadPanelBar ID="radPbCategories" runat="server" Width="100%" />

    <telerik:RadPanelBar ID="RadPanelBar1" runat="server" Width="100%" Visible="false">
        <Items>
            <telerik:RadPanelItem Text="TRANSMITTAL" runat="server" Expanded="True" Width="100%" Visible="false">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Transmittal" ImageUrl="Images/Transmittal.png" NavigateUrl="Transmittal.aspx" />
                </Items>
            </telerik:RadPanelItem>
        </Items>
    </telerik:RadPanelBar>

    <telerik:RadPanelBar ID="radPbList" runat="server" Width="100%" Visible="false" />
    <telerik:RadPanelBar ID="radPbSystem" runat="server" Width="100%" Visible="false" />

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <telerik:RadSplitter ID="RadSplitter4" runat="server" Orientation="Horizontal" Width="100%">
        <telerik:RadPane ID="RadPane3" runat="server" Height="255" Scrollable="false" Scrolling="None">
            <dl class="accordion">
                <dt style="width: 100%;">
                    <span>Advance Search</span>
                </dt>
            </dl>
            <table style="width: 100%; padding-bottom: 5px">
                <tr class="qlcbFormItem">
                    <td style="width: 13%" class="qlcbTooltip"><span>Category</span></td>
                    <td style="width: 23%">
                        <asp:DropDownList ID="ddlCategory" runat="server" CssClass="min25Percent" Width="250px" />
                    </td>
                    <td style="width: 15%" class="qlcbTooltip"><span style="color: red">Search</span></td>
                    <td style="width: 23%">
                        <%--<asp:RadioButtonList ID="rblSearch" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rblSearch_SelectedIndexChanged" RepeatDirection="Horizontal">
                            <asp:ListItem Text="All" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Detail" Value="2"></asp:ListItem>
                        </asp:RadioButtonList>--%>
                        <asp:RadioButton ID="rbAll" runat="server" Text="All" Checked="true" AutoPostBack="true" OnCheckedChanged="rbAll_CheckedChanged" />
                        <asp:RadioButton ID="rbDetail" runat="server" Text="Detail" AutoPostBack="true" OnCheckedChanged="rbDetail_CheckedChanged" />
                    </td>
                    <td style="width: 15%" class="qlcbTooltip">
                        <asp:Button runat="server" ID="btnSearch" OnClick="btnSearch_Click" Text="Search" /></td>
                </tr>
            </table>
            <asp:Panel runat="server" ID="pnSearchAll">
                <table style="width: 98%; padding-bottom: 5px">
                    <tr class="qlcbFormItem">
                        <td style="width: 15%" class="qlcbTooltip"><span>Search all fields</span></td>
                        <td>
                            <asp:TextBox ID="txtSearchAll" runat="server" CssClass="min25Percent" Width="200px" />
                        </td>
                        <td style="width: 12%" class="qlcbTooltip"></td>
                        <td style="width: 23%"></td>
                        <td style="width: 15%" class="qlcbTooltip"></td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnSearchDetail" Visible="false">
                <table style="width: 98%; padding-bottom: 5px">
                    <tr class="qlcbFormItem">
                        <td style="width: 15%" class="qlcbTooltip"><span>Filename</span></td>
                        <td>
                            <asp:TextBox ID="txtFileName" runat="server" CssClass="min25Percent" Width="200px" />
                        </td>
                        <td style="width: 12%" class="qlcbTooltip"><span>Document Number</span></td>
                        <td id="Index1" runat="server" style="width: 23%">
                            <asp:TextBox ID="txtDocumentNumber" runat="server" CssClass="min25Percent" Width="200px" />
                        </td>
                        <td style="width: 15%" class="qlcbTooltip"></td>
                    </tr>
                    <tr class="qlcbFormItem">
                        <td style="width: 15%" class="qlcbTooltip"><span>Title</span></td>
                        <td id="Index2" runat="server">
                            <asp:TextBox ID="txtTitle" runat="server" CssClass="min25Percent" Width="200px" />
                        </td>
                        <td style="width: 12%" class="qlcbTooltip"><span>Revision</span></td>
                        <td id="Index3" runat="server" style="width: 23%">
                            <asp:TextBox ID="txtRevision" runat="server" CssClass="min25Percent" Width="200px" />
                        </td>
                        <td style="width: 15%" class="qlcbTooltip"></td>
                    </tr>
                    <tr class="qlcbFormItem">
                        <td style="width: 15%" class="qlcbTooltip"><span>From/To</span></td>
                        <td id="Index5" runat="server">
                            <asp:TextBox ID="txtFromTo" runat="server" CssClass="min25Percent" Width="200px" />
                        </td>
                        <td style="width: 12%" class="qlcbTooltip"><span>Discipline</span></td>
                        <td id="Index6" runat="server" style="width: 23%">
                            <asp:TextBox ID="txtDiscipline" runat="server" CssClass="min25Percent" Width="200px" />
                        </td>
                        <td style="width: 15%" class="qlcbTooltip"></td>
                    </tr>
                    <tr class="qlcbFormItem">
                        <td style="width: 15%" class="qlcbTooltip"><span>Doc Type</span></td>
                        <td id="Index7" runat="server">
                            <asp:TextBox ID="txtDoctype" runat="server" CssClass="min25Percent" Width="200px" />
                        </td>
                        <td style="width: 12%" class="qlcbTooltip"><span>Platform</span></td>
                        <td id="Index8" runat="server" style="width: 23%">
                            <asp:TextBox ID="txtPlatform" runat="server" CssClass="min25Percent" Width="200px" />
                        </td>
                        <td style="width: 15%" class="qlcbTooltip"></td>
                    </tr>
                    <tr class="qlcbFormItem">
                        <td style="width: 15%" class="qlcbTooltip"><span>Remark</span></td>
                        <td id="Index9" runat="server">
                            <asp:TextBox ID="txtRemark" runat="server" CssClass="min25Percent" Width="200px" />
                        </td>
                        <td style="width: 12%" class="qlcbTooltip"><span>Contractor</span></td>
                        <td id="Index10" runat="server" style="width: 23%">
                            <asp:TextBox ID="txtContractor" runat="server" CssClass="min25Percent" Width="200px" />
                        </td>
                        <td style="width: 15%" class="qlcbTooltip"></td>
                    </tr>
                    <tr class="qlcbFormItem">
                        <td style="width: 15%" class="qlcbTooltip"><span>Tag</span></td>
                        <td id="Index11" runat="server">
                            <asp:TextBox ID="txtTag" runat="server" CssClass="min25Percent" Width="200px" />
                        </td>
                        <td style="width: 12%" class="qlcbTooltip"><span></span></td>
                        <td style="width: 23%"></td>
                        <td style="width: 15%" class="qlcbTooltip"></td>
                    </tr>
                </table>
            </asp:Panel>

        </telerik:RadPane>
        <telerik:RadPane ID="RadPane2" runat="server" Scrollable="false" Scrolling="None">
            <telerik:RadSplitter ID="Radsplitter3" runat="server" Orientation="Horizontal">
                <telerik:RadPane ID="RadpaneSearchAction" runat="server" Scrolling="None" Height="30">
                    <telerik:RadToolBar ID="CustomerMenu" runat="server" Width="100%" OnClientButtonClicking="OnClientButtonClicking">
                        <Items>
                            <telerik:RadToolBarDropDown runat="server" Text="Action" Visible="true" ImageUrl="~/Images/action.png">
                                <Buttons>
                                    <telerik:RadToolBarButton runat="server" Text="Send notifications" Value="3" ImageUrl="~/Images/sendnotification.png" Visible="false" />
                                    <telerik:RadToolBarButton runat="server" Text="Send mail" Value="3" ImageUrl="~/Images/email.png" />
                                </Buttons>
                            </telerik:RadToolBarDropDown>
                        </Items>
                    </telerik:RadToolBar>
                </telerik:RadPane>

                <telerik:RadPane ID="Radpane4" runat="server" Scrolling="None">
                    <telerik:RadGrid ID="grdDocument" runat="server" AllowPaging="True" Height="100%"
                        AutoGenerateColumns="False" CellPadding="0" CellSpacing="0"
                        GridLines="None" AllowMultiRowSelection="True"
                        OnDeleteCommand="grdDocument_DeleteCommand"
                        OnNeedDataSource="grdDocument_OnNeedDataSource"
                        PageSize="100" Style="outline: none">
                        <MasterTableView ClientDataKeyNames="ID" DataKeyNames="ID,FolderID" EditMode="InPlace" Font-Size="8pt">
                            <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; Documents." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <Columns>
                                <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />
                                <telerik:GridBoundColumn DataField="FolderID" UniqueName="FolderID" Visible="False" />

                                <%--<telerik:GridEditCommandColumn UniqueName="EditColumn" ButtonType="ImageButton" EditImageUrl="~/Images/edit.png" UpdateImageUrl="~/Images/ok.png" CancelImageUrl="~/Images/delete.png" >
                                <HeaderStyle Width="4%"  />
                                <ItemStyle HorizontalAlign="Center" Width="4%"/>
                            </telerik:GridEditCommandColumn>--%>

                                <telerik:GridClientSelectColumn UniqueName="IsSelected">
                                    <HeaderStyle Width="30px" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridClientSelectColumn>

                                <%--  <telerik:GridTemplateColumn UniqueName="EditColumn" Display="False">
                                <HeaderStyle Width="2%"  />
                                <ItemStyle HorizontalAlign="Center" Width="2%"/>
                                <ItemTemplate>
                                    <a href='javascript:ShowEditForm(<%# DataBinder.Eval(Container.DataItem, "ID") %>, <%# DataBinder.Eval(Container.DataItem, "FolderID") %>)' style="text-decoration: none; color:blue">
                                        <asp:Image ID="EditLink" runat="server" ImageUrl="~/Images/edit.png" Style="cursor: pointer;" AlternateText="Edit properties" />
                                    </a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>--%>

                                <%--  <telerik:GridButtonColumn UniqueName="DeleteColumn" CommandName="Delete" Display="False"
                                ConfirmText="Do you want to delete document?" ButtonType="ImageButton" ImageUrl="~/Images/delete.png">
                                <HeaderStyle Width="2%" />
                                    <ItemStyle HorizontalAlign="Center" Width="2%"  />
                            </telerik:GridButtonColumn>--%>

                                <telerik:GridTemplateColumn UniqueName="DownloadColumn">
                                    <HeaderStyle Width="30px" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <a download='<%# DataBinder.Eval(Container.DataItem, "Name") %>'
                                            href='<%# DataBinder.Eval(Container.DataItem, "FilePath") %>' target="_blank">
                                            <asp:Image ID="CallLink" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon") %>'
                                                Style="cursor: pointer;" AlternateText="Download document" />
                                        </a>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn UniqueName="Name" HeaderText="File Name">
                                    <HeaderStyle Width="300" HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblFileName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>' ForeColor="Blue" Style="cursor: pointer" />
                                        <asp:Image ID="newicon" runat="server" ImageUrl="Images/new.png" Visible='<%# (DateTime.Now - Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "CreatedDate"))).TotalHours < 24 %>' />
                                        <telerik:RadToolTip Skin="Simple" runat="server" ID="dirNameToolTip" RelativeTo="Element"
                                            AutoCloseDelay="20000" ShowDelay="0" Position="BottomRight"
                                            Width="600px" Height="70px" HideEvent="LeaveTargetAndToolTip" TargetControlID="lblFileName" IsClientID="False"
                                            Animation="Fade" Text='<%# "<b>Folder: </b>" + DataBinder.Eval(Container.DataItem, "DirName") %>'>
                                        </telerik:RadToolTip>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:HiddenField ID="Name" runat="server" Value='<%# Eval("Name") %>' />
                                        <asp:TextBox ID="txtName" runat="server" Width="100%"></asp:TextBox>
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="DocumentNumber" HeaderText="Document Number" UniqueName="Index1" AllowFiltering="true"
                                    FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                    <HeaderStyle HorizontalAlign="Center" Width="200" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Title" HeaderText="Title" UniqueName="Index2" AllowFiltering="true"
                                    FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                    <HeaderStyle HorizontalAlign="Center" Width="300" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="RevisionName" HeaderText="Rev" UniqueName="Index3" AllowFiltering="true"
                                    FilterControlWidth="95%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" DataFormatString="{0:dd/MM/yyyy}">
                                    <HeaderStyle HorizontalAlign="Center" Width="200" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Date" HeaderText="Date" UniqueName="Index4" AllowFiltering="true"
                                    FilterControlWidth="95%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" DataFormatString="{0:dd/MM/yyyy}">
                                    <HeaderStyle HorizontalAlign="Center" Width="200" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FromTo" HeaderText="From/To" UniqueName="Index5" AllowFiltering="true"
                                    FilterControlWidth="95%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" DataFormatString="{0:dd/MM/yyyy}">
                                    <HeaderStyle HorizontalAlign="Center" Width="200" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DocumentTypeName" HeaderText="Document Type" UniqueName="Index6" AllowFiltering="true"
                                    FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                    <HeaderStyle HorizontalAlign="Center" Width="150" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DisciplineName" HeaderText="Discipline" UniqueName="Index7" AllowFiltering="true"
                                    FilterControlWidth="95%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                    <HeaderStyle HorizontalAlign="Center" Width="200" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Platform" HeaderText="Platform" UniqueName="Index8" AllowFiltering="true"
                                    FilterControlWidth="95%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" DataFormatString="{0:dd/MM/yyyy}">
                                    <HeaderStyle HorizontalAlign="Center" Width="200" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Remark" HeaderText="Remark" UniqueName="Index9" AllowFiltering="true"
                                    FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                    <HeaderStyle HorizontalAlign="Center" Width="150" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Remark" HeaderText="Remark" UniqueName="Index10" AllowFiltering="true"
                                    FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                    <HeaderStyle HorizontalAlign="Center" Width="150" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Remark" HeaderText="Remark" UniqueName="Index11" AllowFiltering="true"
                                    FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                    <HeaderStyle HorizontalAlign="Center" Width="150" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings Selecting-AllowRowSelect="true" AllowColumnHide="True">
                            <Resizing EnableRealTimeResize="True" ResizeGridOnColumnResize="True" ClipCellContentOnResize="false"></Resizing>
                            <Selecting AllowRowSelect="true" />
                            <ClientEvents OnGridCreated="GetGridObject" />
                            <ClientEvents OnRowContextMenu="RowContextMenu" OnRowClick="RowClick"></ClientEvents>
                            <Scrolling AllowScroll="True" SaveScrollPosition="True" UseStaticHeaders="True" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </telerik:RadPane>
            </telerik:RadSplitter>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <span style="display: none">

        <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnSearch">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="grdDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="rbAll">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="rbDetail"></telerik:AjaxUpdatedControl>
                        <telerik:AjaxUpdatedControl ControlID="pnSearchAll" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                        <telerik:AjaxUpdatedControl ControlID="pnSearchDetail" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="rbDetail">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="rbAll"></telerik:AjaxUpdatedControl>
                        <telerik:AjaxUpdatedControl ControlID="pnSearchAll" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                        <telerik:AjaxUpdatedControl ControlID="pnSearchDetail" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
    </span>
    <telerik:RadContextMenu ID="radMenu" runat="server"
        EnableRoundedCorners="true" EnableShadows="true" OnClientItemClicking="gridMenuClicking" OnClientShowing="gridContextMenuShowing">
        <Items>
            <telerik:RadMenuItem Text="Revision history" ImageUrl="~/Images/revision.png" Value="RevisionHistory" />

            <telerik:RadMenuItem Text="Version history" ImageUrl="~/Images/history.png" Value="VersionHistory" />

            <telerik:RadMenuItem Text="Retrieve Duplicate Revision" ImageUrl="~/Images/retrieve.png" Value="RetrieveDoc" />

            <%--<telerik:RadMenuItem IsSeparator="True">
            </telerik:RadMenuItem>
            <telerik:RadMenuItem Text="Check out" ImageUrl="~/Images/checkout.png" Value="CheckOut">
            </telerik:RadMenuItem>
            <telerik:RadMenuItem Text="Check in" ImageUrl="~/Images/checkin.png" Value="CheckIn">
            </telerik:RadMenuItem>
                OnClientClose="refreshGrid"
            --%>
        </Items>
    </telerik:RadContextMenu>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="CustomerDialog" runat="server" Title="Document Information"
                VisibleStatusbar="false" Height="690" Width="650"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="RevisionDialog" runat="server" Title="Revision history"
                VisibleStatusbar="false" Height="600" Width="1200" MinHeight="600" MinWidth="1200" MaxHeight="600" MaxWidth="1200"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="VersionHistory" runat="server" Title="Version history"
                VisibleStatusbar="false" Height="332" Width="1200" MinHeight="332" MinWidth="1200" MaxHeight="332" MaxWidth="1200"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="SendMail" runat="server" Title="Send mail"
                VisibleStatusbar="false" Height="560" Width="992" MinHeight="560" MinWidth="992" MaxHeight="560" MaxWidth="992"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true" />

        </Windows>
    </telerik:RadWindowManager>

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" />
    <asp:HiddenField runat="server" ID="FolderContextMenuAction" />
    <asp:HiddenField runat="server" ID="lblFolderId" />
    <asp:HiddenField runat="server" ID="lblDocId" />
    <asp:HiddenField runat="server" ID="IsAdmin" />

    <input type="hidden" id="radGridClickedRowIndex" name="radGridClickedRowIndex" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script src="Scripts/jquery-1.7.1.js"></script>
        <script type="text/javascript">

            var radDocuments;

            function refreshGrid() {
                var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView();
                masterTable.rebind();
            }

            function GetGridObject(sender, eventArgs) {
                radDocuments = sender;
            }

            function RowClick(sender, eventArgs) {
                var Id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblDocId.ClientID %>").value = Id;
            }

            function gridContextMenuShowing(menu, args) {
                var isAdmin = document.getElementById("<%= IsAdmin.ClientID %>").value;
                if (isAdmin == "true") {
                    menu.get_allItems()[2].show();
                } else {
                    menu.get_allItems()[2].hide();
                }
            }

            function gridMenuClicking(sender, args) {
                var itemValue = args.get_item().get_value();
                var docId = document.getElementById("<%= lblDocId.ClientID %>").value;



                switch (itemValue) {
                    case "RevisionHistory":
                        var owd = $find("<%=RevisionDialog.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/RevisionHistory.aspx?docId=" + docId, "RevisionDialog");
                        break;
                    case "VersionHistory":
                        var owd = $find("<%=VersionHistory.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/VersionHistory.aspx?docId=" + docId, "VersionHistory");
                        break;
                    case "RetrieveDoc":
                        var owd = $find("<%=CustomerDialog.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/RetrieveDocumentForm.aspx?docId=" + docId, "CustomerDialog");
                        break;
                }
            }
        </script>
        <script type="text/javascript">
            /* <![CDATA[ */
            var toolbar;
            var searchButton;
            var ajaxManager;

            function pageLoad() {
                $('iframe').load(function () { //The function below executes once the iframe has finished loading<---true dat, althoug Is coppypasta from I don't know where
                    //alert($('iframe').contents());
                });

                ajaxManager = $find("<%=ajaxCustomer.ClientID %>");
            }

            function ShowEditForm(id, folId) {
                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/DocumentInfoEditForm.aspx?docId=" + id + "&folId=" + folId, "CustomerDialog");

                // window.radopen("Controls/Customers/CustomerEditForm.aspx?patientId=" + id, "CustomerDialog");
                //  return false;
            }
            function ShowInsertForm() {

                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Customers/CustomerEditForm.aspx", "CustomerDialog");

                //window.radopen("Controls/Customers/CustomerEditForm.aspx", "CustomerDialog");
                //return false;
            }



            function refreshGrid(arg) {
                //alert(arg);
                if (!arg) {
                    ajaxManager.ajaxRequest("Rebind");
                }
                else {
                    ajaxManager.ajaxRequest("RebindAndNavigate");
                }
            }

            function refreshTab(arg) {
                $('.EDMsRadPageView' + arg + ' iframe').attr('src', $('.EDMsRadPageView' + arg + ' iframe').attr('src'));
            }

            function RowDblClick(sender, eventArgs) {
                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Customers/ViewCustomerDetails.aspx?docId=" + eventArgs.getDataKeyValue("Id"), "CustomerDialog");
                // window.radopen("Controls/Customers/ViewCustomerDetails.aspx?patientId=" + eventArgs.getDataKeyValue("Id"), "CustomerDialog");
            }

            function onNodeClicking(sender, args) {
                var folderValue = args.get_node().get_value();
                document.getElementById("<%= lblFolderId.ClientID %>").value = folderValue;
            }

            function OnClientButtonClicking(sender, args) {
                var button = args.get_item();
                var strText = button.get_text();
                var strValue = button.get_value();

                var grid = $find("<%= grdDocument.ClientID %>");
                var customerId = null;
                var customerName = "";

                if (strText.toLowerCase() == "send notifications") {
                    var grid = $find("<%=grdDocument.ClientID %>");
                    var masterTable = grid.get_masterTableView();

                    var selectedRows = masterTable.get_selectedItems();
                    if (selectedRows.length == 0) {
                        alert("Please select documents to send notification");
                    }
                    else {
                        ajaxManager.ajaxRequest("SendNotification");
                    }
                }

                if (strText.toLowerCase() == "send mail") {
                    var grid = $find("<%=grdDocument.ClientID %>");
                    var masterTable = grid.get_masterTableView();

                    var selectedRows = masterTable.get_selectedItems();
                    if (selectedRows.length == 0) {
                        alert("Please select documents to send mail");
                    }
                    else {
                        var listId = "";

                        for (var i = 0; i < selectedRows.length; i++) {
                            var item = selectedRows[i];
                            listId += item.getDataKeyValue("ID") + ",";

                        }

                        var owd = $find("<%=SendMail.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/SendMail.aspx?listDoc=" + listId, "SendMail");
                    }
                }






                if (strText == "Documents") {

                    var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                    if (selectedFolder == "") {
                        alert("Please choice one folder to create new document");
                        return false;
                    }

                    var owd = $find("<%=CustomerDialog.ClientID %>");
                    owd.Show();
                    owd.setUrl("Controls/Document/DocumentInfoEditForm.aspx?folId=" + selectedFolder, "CustomerDialog");

                }

                if (strText == "Thêm mới") {
                    return ShowInsertForm();
                }
                else if (strText == "Import dữ liệu") {
                    return ShowImportForm();
                }
                else if (strText == "Dữ liệu thô") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_1_" + customerId);
                }
                else if (strText == "Tiềm năng") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_2_" + customerId);
                }
                else if (strText == "Chưa liên hệ được") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_3_" + customerId);
                }
                else if (strText == "Không tiềm năng") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_4_" + customerId);
                }
                else if (strText == "Thông tin sai") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_5_" + customerId);
                }
                else if (strText == "Liên hệ tư vấn") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_6_" + customerId);
                }
                else if (strText == "Hẹn tư vấn") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_7_" + customerId);
                }
                else if (strText == "Đã sử dụng dịch vụ") {
                    if (customerId == null) return;
                    if (confirm("Ban có chắc chắn chuyển trạng khách hàng [" + customerName + "] sang trạng thái [" + strText + "] không ?") == false) return;
                    ajaxManager.ajaxRequest("ChangeStatus_8_" + customerId);
                }
                else {
                    var commandName = args.get_item().get_commandName();
                    if (commandName == "doSearch") {
                        var searchTextBox = sender.findButtonByCommandName("searchText").findControl("txtSearch");
                        if (searchButton.get_value() == "clear") {
                            searchTextBox.set_value("");
                            searchButton.set_imageUrl("images/search.gif");
                            searchButton.set_value("search");
                        }

                        performSearch(searchTextBox);
                    } else if (commandName == "reply") {
                        window.radopen(null, "Edit");
                    }
                }
            }

            function performSearch(searchTextBox) {
                if (searchTextBox.get_value()) {
                    searchButton.set_imageUrl("images/clear.gif");
                    searchButton.set_value("clear");
                }

                ajaxManager.ajaxRequest(searchTextBox.get_value());
            }
            function onTabSelecting(sender, args) {
                if (args.get_tab().get_pageViewID()) {
                    args.get_tab().set_postBack(false);
                }
            }


            function RowContextMenu(sender, eventArgs) {
                var menu = $find("<%=radMenu.ClientID %>");
                var evt = eventArgs.get_domEvent();

                if (evt.target.tagName == "INPUT" || evt.target.tagName == "A") {
                    return;
                }

                var index = eventArgs.get_itemIndexHierarchical();
                document.getElementById("radGridClickedRowIndex").value = index;

                var Id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblDocId.ClientID %>").value = Id;

                sender.get_masterTableView().selectItem(sender.get_masterTableView().get_dataItems()[index].get_element(), true);

                menu.show(evt);

                evt.cancelBubble = true;
                evt.returnValue = false;

                if (evt.stopPropagation) {
                    evt.stopPropagation();
                    evt.preventDefault();
                }
            }
            /* ]]> */
        </script>
    </telerik:RadCodeBlock>
</asp:Content>

<%--Tan.Le Remove here--%>
<%--<uc1:List runat="server" ID="CustomerList"/>--%>
<%-- <div id="EDMsCustomers" runat="server" />--%>
