﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchContain.aspx.cs" Inherits="EDMs.Web.SearchContain" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Avanced search</title>
    <link href="Scripts/Bootstrap/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/jquery.min.js"></script>
    <script src="Scripts/ElasticSearch/elasticsearch.jquery.min.js"></script>
</head>
<body>
     <form id="form1" runat="server">
        <div>
            <div class="row">
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <input id='txtKeyword' class="form-control" />
                </div>
                <div class="col-sm-2 col-md-2 col-xs-6">
                    <button id='btnSearch' type="button" class="btn btn-primary">Tìm kiếm</button>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-sm-12 col-md-12 col-xs-12">
                    <span id="lbResult"></span>
                </div>
            </div>
            
            <script>
                $(document).ready(function () {
                    $('#btnSearch').click(function () {
                        var json = { "query" : { "match" : { "content" : $('#txtKeyword').val() } } };
                    $.ajax({
                        url: 'http://localhost:9200/mongduong/_search',
                        type: 'POST',
                        data: JSON.stringify(json),
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        async: false,
                        success: function (msg) {
                            // msg chứa chuỗi json
                            //  $('#lbResult').html($.parseJSON(msg));
                            console.log(msg);
                            var list = msg.hits.hits;
                            if (list.length > 0) {
                                $('#lbResult').html('');
                                for (i = 0; i < list.length; i++){
                                    var source = list[i]._source;
                                    var file = '<a href="SearchContain.aspx?path=' + source.path.virtual + '&type=' + source.file.content_type + '" >' + source.file.filename + '</a>';
                                    $('#lbResult').html($('#lbResult').html() + file + '<br/>');
                                }
                            }
                            else {
                                $('#lbResult').html('Không tìm thấy kết quả');
                            }
                            var i;
                            
                           // $('#lbResult').text(files);
                        }
                    });
                    });
                    
  
                });
            </script>
        </div>
    </form>
</body>
</html>
