﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.ServiceProcess;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    using ZetaLongPaths;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class DCREditForm : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        private readonly DCRService dcrService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        private readonly DepartmentService departmentService;

        protected const string ServiceName = "EDMSFolderWatcher";

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public DCREditForm()
        {
            this.userService = new UserService();
            this.dcrService = new DCRService();
            this.departmentService = new DepartmentService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadComboData();
                if (!UserSession.Current.User.Role.Active.GetValueOrDefault())
                {
                    this.btnSave.Visible = false;
                }

                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    this.CreatedInfo.Visible = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("ViewCreatedUpdatedInfo"));

                    var dcrObj = this.dcrService.GetById(Convert.ToInt32(Request.QueryString["docId"]));
                    if (dcrObj != null)
                    {
                        this.txtDCRNumber.Text = dcrObj.DCRNumber;
                        this.txtRequestBy.Text = dcrObj.RequestBy;
                        this.ddlDepartment.SelectedValue = dcrObj.DepartmentId.ToString();
                        this.txtReason.Text = dcrObj.Reason;
                        this.txtReceivedDate.SelectedDate = dcrObj.Date;

                        var createdUser = this.userService.GetByID(dcrObj.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + dcrObj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (dcrObj.UpdatedBy != null && dcrObj.UpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(dcrObj.UpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + dcrObj.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                try
                {
                    if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                    {
                        var dcrObj = this.dcrService.GetById(Convert.ToInt32(Request.QueryString["docId"]));
                        if (dcrObj != null)
                        {
                            dcrObj.DCRNumber = this.txtDCRNumber.Text;
                            dcrObj.RequestBy = this.txtRequestBy.Text;
                            dcrObj.Deparment = this.ddlDepartment.SelectedItem.Text;
                            dcrObj.DepartmentId = Convert.ToInt32(this.ddlDepartment.SelectedValue);
                            dcrObj.Date = this.txtReceivedDate.SelectedDate;
                            dcrObj.Reason = this.txtReason.Text;
                            dcrObj.UpdatedDate = DateTime.Now;
                            dcrObj.UpdatedBy = UserSession.Current.User.Id;
                        }

                        this.dcrService.Update(dcrObj);
                    }
                    else
                    {
                        var dcrObj = new DCR()
                            {
                                DCRNumber = this.txtDCRNumber.Text,
                                RequestBy = this.txtRequestBy.Text,
                                Deparment = this.ddlDepartment.SelectedItem.Text,
                                DepartmentId = Convert.ToInt32(this.ddlDepartment.SelectedValue),
                                Date = this.txtReceivedDate.SelectedDate,
                                Reason = this.txtReason.Text,
                                CreatedDate = DateTime.Now,
                                CreatedBy = UserSession.Current.User.Id
                            };

                        this.dcrService.Insert(dcrObj);
                    }


                    this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
                }
                catch (Exception ex)
                {
                    var watcherService = new ServiceController(ServiceName);
                    if (Utility.ServiceIsAvailable(ServiceName))
                    {
                        watcherService.ExecuteCommand(129);
                    }
                }
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The rad ajax manager 1_ ajax request.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.Contains("CheckFileName"))
            {
            }
        }

        private void LoadComboData()
        {
            var departmentList = this.departmentService.GetAll().OrderBy(t => t.Name).ToList();
            departmentList.Insert(0, new Department { ID = 0 });

            this.ddlDepartment.DataSource = departmentList;
            this.ddlDepartment.DataTextField = "Name";
            this.ddlDepartment.DataValueField = "ID";
            this.ddlDepartment.DataBind();
        }
    }
}