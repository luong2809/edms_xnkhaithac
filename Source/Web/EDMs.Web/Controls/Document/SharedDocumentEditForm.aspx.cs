﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.ServiceProcess;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class SharedDocumentEditForm : Page
    {
        private readonly SharedDocumentService sharedDocumentService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        protected const string ServiceName = "EDMSFolderWatcher";

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public SharedDocumentEditForm()
        {
            this.userService = new UserService();
            this.sharedDocumentService = new SharedDocumentService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    this.CreatedInfo.Visible = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("ViewCreatedUpdatedInfo"));

                    var objDoc = this.sharedDocumentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {
                        this.txtName.Text = objDoc.Name;
                        
                        var createdUser = this.userService.GetByID(objDoc.CreatedBy.GetValueOrDefault());
                        
                        this.lblCreated.Text = "Created at " + objDoc.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (objDoc.UpdatedBy != null && objDoc.UpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(objDoc.UpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + objDoc.UpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                try
                {
                    if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                    {
                        var objDoc = this.sharedDocumentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                        objDoc.Name = this.txtName.Text.Trim();
                        objDoc.UpdatedBy = UserSession.Current.User.Id;
                        objDoc.UpdatedDate = DateTime.Now;

                        if (this.docuploader.UploadedFiles.Count > 0)
                        {
                            this.SaveUploadFile(this.docuploader, ref objDoc);
                        }

                        this.sharedDocumentService.Update(objDoc);
                    }
                    else
                    {
                        var objDoc = new SharedDocument
                        {
                            Name = this.txtName.Text.Trim(),
                            CreatedBy = UserSession.Current.User.Id,
                            CreatedDate = DateTime.Now
                        };

                        if (this.docuploader.UploadedFiles.Count > 0)
                        {
                            this.SaveUploadFile(this.docuploader, ref objDoc);
                        }
                        this.sharedDocumentService.Insert(objDoc);
                    }


                    this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
                }
                catch (Exception ex)
                {
                    var watcherService = new ServiceController(ServiceName);
                    if (Utility.ServiceIsAvailable(ServiceName))
                    {
                        watcherService.ExecuteCommand(129);
                    }
                }
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if(this.txtName.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter file name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }

        /// <summary>
        /// The rad ajax manager 1_ ajax request.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.Contains("CheckFileName"))
            {
            }
        }

        /// <summary>
        /// The docuploader_ file uploaded.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void docuploader_FileUploaded(object sender, FileUploadedEventArgs e)
        {
            if (string.IsNullOrEmpty(this.Request.QueryString["docId"]))
            {
                var fileName = e.File.FileName;
                this.txtName.Text = fileName;    
            }
            
        }

        /// <summary>
        /// The save upload file.
        /// </summary>
        /// <param name="uploadDocControl">
        /// The upload doc control.
        /// </param>
        /// <param name="objDoc">
        /// The obj Doc.
        /// </param>
        private void SaveUploadFile(RadAsyncUpload uploadDocControl, ref SharedDocument objDoc)
        {
            try
            {
                var listUpload = uploadDocControl.UploadedFiles;
                var targetFolder = "../../DocumentLibrary/SharedDocument/";
                if (listUpload.Count > 0)
                {
                    foreach (UploadedFile docFile in listUpload)
                    {
                        var docFileNameOrignal = docFile.FileName;
                        // Path file to save on server disc
                        var saveFilePath = Path.Combine(Server.MapPath(targetFolder), docFileNameOrignal);

                        // Path file to download from server
                        var serverFilePath = "/DocumentLibrary/SharedDocument/" + docFileNameOrignal;

                        objDoc.FilePath = serverFilePath;
                        ////EDMSFolderWatcher
                        var watcherService = new ServiceController(ServiceName);
                        if (Utility.ServiceIsAvailable(ServiceName))
                        {
                            watcherService.ExecuteCommand(128);
                        }

                        docFile.SaveAs(saveFilePath, true);
                    
                        if (Utility.ServiceIsAvailable(ServiceName))
                        {
                            watcherService.ExecuteCommand(129);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var watcherService = new ServiceController(ServiceName);
                if (Utility.ServiceIsAvailable(ServiceName))
                {
                    watcherService.ExecuteCommand(129);
                }
            }
        }
    }
}