﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Text.RegularExpressions;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.ServiceProcess;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Aspose.Cells;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    using ZetaLongPaths;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class Checkin : Page
    {

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly FolderService folderService;

        private readonly CheckOutInHistoryService checkOutInHistoryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public Checkin()
        {
            this.folderService = new FolderService();
            this.documentService = new DocumentService();
            this.checkOutInHistoryService = new CheckOutInHistoryService();
        }

        /// <summary>
        /// Validation existing patient code
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        protected void ValidatePatientCode(object source, ServerValidateEventArgs arguments)
        {
        }


        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {

                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
            {
                var docID = Convert.ToInt32(this.Request.QueryString["docId"]);
                var docObj = this.documentService.GetById(docID);

                if (docObj != null)
                {
                    var folder = this.folderService.GetById(docObj.FolderID.GetValueOrDefault());
                    if (folder != null)
                    {
                        var targetFolder = "../../" + folder.DirName;
                        var serverFolder = HostingEnvironment.ApplicationVirtualPath == "/" ? "/" + folder.DirName : HostingEnvironment.ApplicationVirtualPath + "/" + folder.DirName;
                        var listUpload = docuploader.UploadedFiles;
                        if (File.Exists(Server.MapPath(docObj.FilePath)))
                        {
                            File.Delete(Server.MapPath(docObj.FilePath));
                        }
                        if (listUpload.Count > 0)
                        {
                            try
                            {
                                foreach (UploadedFile docFile in listUpload)
                                {
                                    var docFileName = docFile.FileName;
                                    var serverDocFileName = Utility.RemoveAllSpecialCharacter(docFileName);

                                    // Path file to save on server disc
                                    var saveFilePath = Path.Combine(Server.MapPath(targetFolder), serverDocFileName);
                                    var serverFilePath = serverFolder + "/" + serverDocFileName;
                                    var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);
                                    docObj.Name = docFileName;
                                    docObj.RevisionFileName = docFileName;
                                    docObj.RevisionFilePath = serverFilePath;

                                    docObj.FilePath = serverFilePath;
                                    docObj.FileExtension = fileExt;
                                    docObj.FileExtensionIcon = !string.IsNullOrEmpty(Utility.FileIcon[fileExt.ToLower()]) ? Utility.FileIcon[fileExt.ToLower()] : "images/otherfile.png";
                                    if (docObj.ParentID == null)
                                    {
                                        docObj.FileNameOriginal = docFileName;
                                    }
                                    docObj.LastUpdatedDate = DateTime.Now;
                                    docObj.LastUpdatedBy = UserSession.Current.User.Id;

                                    docObj.IsCheckOut = false;

                                    //var checkinHistory = new CheckOutInHistory()
                                    //{
                                    //    ActionName = "Checkin",
                                    //    ByUserId = UserSession.Current.User.Id,
                                    //    ByRoleId = UserSession.Current.RoleId,
                                    //    AtTime = DateTime.Now,
                                    //    DocumentId = docObj.ID,
                                    //};

                                    docFile.SaveAs(saveFilePath, true);

                                    this.documentService.Update(docObj);
                                }
                            }
                            catch (Exception ex)
                            {
                                this.blockError.Visible = true;
                                this.lblError.Text = "Have error when upload document file: <br/>'" + ex.Message + "'";
                            }
                        }
                    }
                }
                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The save upload file.
        /// </summary>
        /// <param name="uploadDocControl">
        /// The upload doc control.
        /// </param>
        /// <param name="objDoc">
        /// The obj Doc.
        /// </param>
        private void SaveUploadFile(RadAsyncUpload uploadDocControl, ref Document objDoc)
        {
            var listUpload = uploadDocControl.UploadedFiles;
            if (listUpload.Count > 0)
            {
                foreach (UploadedFile docFile in listUpload)
                {
                    var revisionFilePath = Server.MapPath(objDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                    docFile.SaveAs(revisionFilePath, true);
                }
            }
        }
    }
}