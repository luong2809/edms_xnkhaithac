﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Telerik.Web.UI;
    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class FilePermissionForm : Page
    {
        private readonly GroupFunctionService groupFunctionService;
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly FilePermissionService groupDataPermissionService;

        /// <summary>
        /// The user data permission.
        /// </summary>
       /// private readonly UserDataPermissionService userDataPermissionService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly FolderService folderService;

        /// <summary>
        /// The document service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The role service.
        /// </summary>
        private readonly RoleService roleService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;


        /// <summary>
        /// Initializes a new instance of the <see cref="FolderPermission"/> class.
        /// </summary>
        public FilePermissionForm()
        {
            this.groupFunctionService = new GroupFunctionService();
            this.groupDataPermissionService = new   FilePermissionService();
            this.folderService = new FolderService();
            this.documentService = new DocumentService();
            this.roleService = new RoleService();
            this.userService = new UserService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["DocID"]))
                {
                    this.lblFolderDirName.Text = "<font style='text-decoration: underline;'>Folder</font>: ";
                    var folderId = Request.QueryString["DocID"];
                    var folderObj = this.documentService.GetById(Convert.ToInt32(folderId));
                    this.CreatedInfo.Visible = true;
                    var createdUser = this.userService.GetByID(folderObj.CreatedBy.GetValueOrDefault());

                    this.lblCreated.Text = "Created at " + folderObj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                    if (folderObj.LastUpdatedBy != null && folderObj.LastUpdatedDate != null)
                    {
                        this.lblCreated.Text += "<br/>";
                        var lastUpdatedUser = this.userService.GetByID(folderObj.LastUpdatedBy.GetValueOrDefault());
                        this.lblUpdated.Text = "Last modified at " + folderObj.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                    }
                    else
                    {
                        this.lblUpdated.Visible = false;
                    }
                    if (folderObj != null)
                    {
                        this.lblFolderDirName.Text += folderObj.Name;
                        this.LoadComboData();
                        //this.LoadPermissionData(folderId.Trim());
                    }
                    this.liGroup.Visible = true;
                    this.liUser.Visible = false;
                    this.cbObjectGroup.Checked = true;
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["DocID"]))
            {
                var listFolderId = new List<int>();
                var selectedDocumentId = Request.QueryString["DocID"];
                var selectedFolder = this.documentService.GetById(Convert.ToInt32(selectedDocumentId));

                var selectedGroup = new List<int>();
                var selectedUser = new List<int>();

                if (selectedFolder != null)
                {
                    var categoryId = selectedFolder.CategoryID;
                 
                  
                    if (cbObjectGroup.Checked == true)
                    {
                        IList<RadListBoxItem> collection = rgListGroup.CheckedItems;
                        foreach (RadListBoxItem item in collection)
                        {
                            selectedGroup.Add(Convert.ToInt32(item.Value));
                        }
                        if (selectedGroup.Count > 0)
                        {
                            foreach (var SLGrouppitem in selectedGroup)
                            {
                               // var groupDatapermission =this.groupDataPermissionService.GetByRoleId(SLGrouppitem);

                                var addingPermission =
                                            new FilePermission()
                                            {
                                                CategoryId = categoryId,
                                                ObjectId = Convert.ToInt32(SLGrouppitem),
                                                ObjectIdName=this.groupFunctionService.GetByID(SLGrouppitem).Name,
                                                DocumentID = selectedFolder.ID,
                                                TypeID = 1,
                                                TypeName = "Group",
                                                ChangePermission=cb_filechange.Checked,
                                                Create = cb_fileCreate.Checked,
                                                Delete = cb_Filedelete.Checked,
                                                Read = cb_fileRead.Checked,
                                                Write = cb_fileWrite.Checked,
                                                NoAccess=cb_fileNoAccess.Checked,
                                                CreatedDate = DateTime.Now,
                                                CreatedBy = UserSession.Current.User.Id
                                            };
                                this.groupDataPermissionService.Insert(addingPermission);
                            }
                        }
                    }
                    else
                    {
                        IList<RadListBoxItem> collection = rgListUser.CheckedItems;
                        foreach (RadListBoxItem item in collection)
                        {
                            selectedUser.Add(Convert.ToInt32(item.Value));
                        }
                        if (selectedUser.Count > 0)
                        {
                            foreach (var SLuser in selectedUser)
                            {
                                //var userDatapermission = this.groupDataPermissionService.GetByUserId(SLuser);
                                var addingPermission =
                                             new FilePermission()
                                             {
                                                 CategoryId = categoryId,
                                                 ObjectId = Convert.ToInt32(SLuser),
                                                 ObjectIdName = this.userService.GetByID(SLuser).UserNameFullName,
                                                 DocumentID = selectedFolder.ID,
                                                 TypeID = 2,
                                                 TypeName = "User",
                                                 ChangePermission = cb_filechange.Checked,
                                                 Create = cb_fileCreate.Checked,
                                                 Delete = cb_Filedelete.Checked,
                                                 Read = cb_fileRead.Checked,
                                                 Write = cb_fileWrite.Checked,
                                                 NoAccess = cb_fileNoAccess.Checked,
                                                 CreatedDate = DateTime.Now,
                                                 CreatedBy = UserSession.Current.User.Id
                                             };
                                this.groupDataPermissionService.Insert(addingPermission);
                            }
                        }
                    }

                    this.LoadComboData();
                    this.grdPermission.Rebind();
                }
            }
        }

      

        private void LoadComboData()
        {
            var groupsInPermission = this.groupDataPermissionService.GetAllByDocumrnt(Convert.ToInt32( Request.QueryString["DocID"]),1).Select(t => t.ObjectId).Distinct().ToList();
            var listGroup = this.groupFunctionService.GetAll().Where(t => !groupsInPermission.Contains(t.ID)).OrderBy(t=> t.Name);
            this.rgListGroup.DataSource = listGroup;
            this.rgListGroup.DataTextField = "Name";
            this.rgListGroup.DataValueField = "ID";
            this.rgListGroup.DataBind();

            var usersInPermission = this.groupDataPermissionService.GetAllByDocumrnt(Convert.ToInt32(Request.QueryString["DocID"]), 2).Select(t => t.ObjectId).Distinct().ToList();
            var listUser =  this.userService.GetAll().Where(t =>t.Id != 1 &&  !usersInPermission.Contains(t.Id)).OrderBy(t => t.Username).ToList();
            this.rgListUser.DataSource = listUser;
            this.rgListUser.DataTextField = "Username";
            this.rgListUser.DataValueField = "Id";
            this.rgListUser.DataBind();
        }

        protected void grdPermission_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["DocID"]))
            {
               

                this.grdPermission.DataSource = this.groupDataPermissionService.GetAllByDocumrnt(Convert.ToInt32(Request.QueryString["DocID"]));
            }
            else
            {
                this.grdPermission.DataSource = new List<FilePermission>();
            }
        }

     
        protected void grdPermission_OnDeteleCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var permissionId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var selectedFolderId =Convert.ToInt32( Request.QueryString["DocID"]);
           // var allChildFolderSession = Session["allChildFolder"];
            //TableCell cell = item["TypeID"];
            //var TypeID =Convert.ToInt32 (cell.Text );
            var groupPermission = this.groupDataPermissionService.GetById(permissionId); 
            if (groupPermission != null)
            {
                
                this.groupDataPermissionService.Delete(groupPermission);
            }

            this.LoadComboData();
        }

        //protected void cbFull_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (cbFull.Checked == true)
        //    {
        //        this.cbChangepermission.Checked = true;
        //        this.cbCreate.Checked = true;
        //        this.cbDelete.Checked = true;
        //        this.cbWrite.Checked = true;
        //        this.cbRead.Checked = true;
        //    }
        //    else
        //    {
        //        this.cbChangepermission.Checked = false;
        //        this.cbCreate.Checked = false;
        //        this.cbDelete.Checked = false;
        //        this.cbWrite.Checked = false;
        //        this.cbRead.Checked = false;
        //    }
            
        //}
        protected void cbObjectGroup_CheckedChanged(object sender, EventArgs e)
        {
            this.liGroup.Visible = true;
            this.liUser.Visible = false;
            this.cbObjectUser.Checked = false;

        }

        protected void cbObjectUser_CheckedChanged(object sender, EventArgs e)
        {
            this.liGroup.Visible = false;
            this.liUser.Visible = true;
            this.cbObjectGroup.Checked = false;
        }

        //protected void cbWrite_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (cbWrite.Checked)
        //    {
        //        this.cbRead.Checked = true;
        //    }
        //}

        protected void cb_filefull_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_filefull.Checked == true)
            {
                this.cb_filechange.Checked = true;
                this.cb_fileCreate.Checked = true;
                this.cb_Filedelete.Checked = true;
                this.cb_fileRead.Checked = true;
                this.cb_fileWrite.Checked = true;
                this.cb_fileNoAccess.Checked = false;
            }
            else
            {
                this.cb_filechange.Checked = false;
                this.cb_fileCreate.Checked = false;
                this.cb_Filedelete.Checked = false;
                this.cb_fileRead.Checked = false;
                this.cb_fileWrite.Checked = false;
                this.cb_fileNoAccess.Checked = true;
            }

        }

        protected void cb_fileWrite_CheckedChanged(object sender, EventArgs e)
        {
            if (this.cb_fileWrite.Checked)
            {
                this.cb_fileNoAccess.Checked = false;
            }
        }

        protected void cb_filechange_CheckedChanged(object sender, EventArgs e)
        {
            if (this.cb_filechange.Checked)
            {
                this.cb_fileNoAccess.Checked = false;
            }
        }

        protected void cb_fileCreate_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_fileCreate.Checked)
            {
                this.cb_fileNoAccess.Checked = false;
            }

        }

        protected void cb_Filedelete_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_Filedelete.Checked)
            {
                this.cb_fileNoAccess.Checked = false;
            }
        }

        protected void cb_fileRead_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_fileRead.Checked)
            {
                this.cb_fileNoAccess.Checked = false;
            }
        }

        protected void cb_fileNoAccess_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_fileNoAccess.Checked == true)
            {
                this.cb_filechange.Checked = false;
                this.cb_fileCreate.Checked = false;
                this.cb_Filedelete.Checked = false;
                this.cb_fileRead.Checked = false;
                this.cb_fileWrite.Checked = false;
            }
        }

        //protected void cbCreate_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (cbCreate.Checked)
        //    {
        //        this.cb_fileCreate.Checked = true;
        //    }
        //}

        //protected void cbDelete_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (cbDelete.Checked)
        //    {
        //        this.cb_Filedelete.Checked = true;
        //    }
        //}
    }
}