﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.ServiceProcess;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    using ZetaLongPaths;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class RetrieveDocumentForm : Page
    {
        

        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly StatusService statusService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService;

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly LanguageService languageService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly FolderService folderService;

        protected const string ServiceName = "EDMSFolderWatcher";

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public RetrieveDocumentForm()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.statusService = new StatusService();
            this.disciplineService = new DisciplineService();
            this.receivedFromService = new ReceivedFromService();
            this.languageService = new LanguageService();
            this.documentService = new DocumentService();
            this.folderService = new FolderService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadComboData();

                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {
                        this.txtName.Text = objDoc.Name;
                        this.txtDocumentNumber.Text = objDoc.DocumentNumber;
                        this.txtTitle.Text = objDoc.Title;
                        //this.ddlRevision.SelectedValue = objDoc.RevisionID.ToString();
                        this.ddlDocumentType.SelectedValue = objDoc.DocumentTypeID.ToString();
                        //  this.ddlStatus.SelectedValue = objDoc.StatusID.ToString();
                        // this.ddlDiscipline.SelectedValue = objDoc.DisciplineID.ToString();
                        //this.ddlReceivedFrom.SelectedValue = objDoc.ReceivedFromID.ToString();

                        // this.ddlLanguage.SelectedValue = objDoc.LanguageID.ToString();
                        //  this.txtWell.Text = objDoc.Well;
                        this.txtKeywords.Text = objDoc.KeyWords;
                        //this.txtRemark.Text = objDoc.Remark;
                        //this.txtTransmittalNumber.Text = objDoc.TransmittalNumber;
                    }
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                try
                {
                    if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                    {
                        var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));

                        var newDoc = new Document()
                        {
                            Name = this.txtName.Text,
                            DocumentNumber = this.txtDocumentNumber.Text,
                            Title = this.txtTitle.Text,
                            RevisionID = Convert.ToInt32(this.ddlRevision.SelectedValue),
                            DocumentTypeID = Convert.ToInt32(this.ddlDocumentType.SelectedValue),
                           
                            RevisionName = this.ddlRevision.SelectedItem.Text,
                            DocumentTypeName = this.ddlDocumentType.SelectedItem.Text,
                            KeyWords = this.txtKeywords.Text,
                            FolderID = objDoc.FolderID,
                            CategoryID = objDoc.CategoryID,
                            DocIndex = objDoc.DocIndex + 1,
                            CreatedBy = UserSession.Current.User.Id,
                            CreatedDate = DateTime.Now,
                            IsLeaf = false,
                            IsDelete = false
                        };

                        newDoc.ParentID = objDoc.ParentID ?? objDoc.ID;
                        this.SaveUploadFile(this.docuploader, ref newDoc);

                        this.documentService.Insert(newDoc);
                        var ListNodeExpanded = (List<int>)Session["ListNodeExpanded"];
                        ListNodeExpanded.Add(objDoc.FolderID.GetValueOrDefault());
                        UpdatePropertisFolder(objDoc.CategoryID.GetValueOrDefault(), ListNodeExpanded);
                    }

                    this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
                }
                catch (Exception ex)
                {
                    var watcherService = new ServiceController(ServiceName);
                    if (Utility.ServiceIsAvailable(ServiceName))
                    {
                        watcherService.ExecuteCommand(129);
                    }
                }
            }
        }
        private void UpdatePropertisFolder(int categori, List<int> ListNodeCurrent)
        {
            //var listFolder = this.folderService.GetAllByCategory(categori).OrderBy
            //    (t=> t.ID).ToList();
            //var listFolder= listFolder
            foreach (var folderItem in ListNodeCurrent.OrderByDescending(t => t))
            {
                var objFolder = this.folderService.GetById(folderItem);
                // List<int> listTemp = new List<int>();
                var subfolderList = this.folderService.GetAllByParentId(folderItem);
                objFolder.NumberOfSubfolder = subfolderList.Count + subfolderList.Sum(t => t.NumberOfSubfolder);
                ////subfolderList.Add(objFolder);
                var docList = this.documentService.GetAllByFolder(objFolder.ID);
                var folderSize = docList.Sum(t => t.FileSize) + subfolderList.Sum(t => t.FolderSize);
                objFolder.NumberOfDocument = docList.Count + subfolderList.Sum(t => t.NumberOfDocument);
                objFolder.FolderSize = folderSize;
                this.folderService.Update(objFolder);
            }
        }
        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if(this.txtName.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter file name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
            else if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var docId = Convert.ToInt32(Request.QueryString["docId"]);
                var folderId = Convert.ToInt32(Request.QueryString["folId"]);
                this.fileNameValidator.ErrorMessage = "The specified name is already in use.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = !this.documentService.IsDocumentExistUpdate(folderId, this.txtName.Text.Trim(), docId);
            }
        }

        /// <summary>
        /// The rad ajax manager 1_ ajax request.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.Contains("CheckFileName"))
            {
                var fileName = e.Argument.Split('$')[1];
                var folderId = Convert.ToInt32(Request.QueryString["folId"]);
                
                if(this.documentService.IsDocumentExist(folderId, fileName))
                {
                    var docObjLeaf = this.documentService.GetSpecificDocument(folderId, fileName);
                    if (docObjLeaf != null)
                    {
                        this.txtDocumentNumber.Text = docObjLeaf.DocumentNumber;
                        this.txtTitle.Text = docObjLeaf.Title;
                        this.ddlDocumentType.SelectedValue = docObjLeaf.DocumentTypeID.GetValueOrDefault().ToString();
                       
                        this.txtKeywords.Text = docObjLeaf.KeyWords;

                        this.docUploadedIsExist.Value = "true";
                        this.docIdUpdateUnIsLeaf.Value = docObjLeaf.ID.ToString();
                    }
                }
            }
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {
            var categoryId = 0;

            if (!string.IsNullOrEmpty(this.Request.QueryString["categoryId"]))
            {
                categoryId = Convert.ToInt32(this.Request.QueryString["categoryId"]);
            }
            
            var revisionList = this.revisionService.GetAll();
            revisionList.Insert(0, new Revision() { Name = string.Empty });
            this.ddlRevision.DataSource = revisionList;
            this.ddlRevision.DataValueField = "ID";
            this.ddlRevision.DataTextField = "Name";
            this.ddlRevision.DataBind();

            var documentTypeList = this.documentTypeService.GetAllByCategory(categoryId).OrderBy(t => t.Name).ToList();
            documentTypeList.Insert(0, new DocumentType() { Name = string.Empty });
            this.ddlDocumentType.DataSource = documentTypeList;
            this.ddlDocumentType.DataValueField = "ID";
            this.ddlDocumentType.DataTextField = "Name";
            this.ddlDocumentType.DataBind();
        }

        /// <summary>
        /// The save upload file.
        /// </summary>
        /// <param name="uploadDocControl">
        /// The upload doc control.
        /// </param>
        /// <param name="objDoc">
        /// The obj Doc.
        /// </param>
        private void SaveUploadFile(RadAsyncUpload uploadDocControl, ref Document objDoc)
        {
            try
            {
                var listUpload = uploadDocControl.UploadedFiles;
                var folder = this.folderService.GetById(objDoc.FolderID.GetValueOrDefault());
                var targetFolder = "../../" + folder.DirName;
                //var revisionPath = "../../DocumentLibrary/RevisionHistory/";
                var serverFolder = HostingEnvironment.ApplicationVirtualPath == "/" ? "/" + folder.DirName : HostingEnvironment.ApplicationVirtualPath + "/" + folder.DirName;
               // var serverRevisionFolder = HostingEnvironment.ApplicationVirtualPath == "/" ? "/DocumentLibrary/RevisionHistory/" : HostingEnvironment.ApplicationVirtualPath + "/DocumentLibrary/RevisionHistory/";
                if (listUpload.Count > 0)
                {
                    foreach (UploadedFile docFile in listUpload)
                    {
                        var docRevisionFileName = string.Empty;
                        var docFileNameOrignal = docFile.FileName;
                        if (!string.IsNullOrEmpty(objDoc.RevisionName))
                        {
                            docRevisionFileName = objDoc.RevisionName + "_" + docFile.FileName;
                        }
                        else
                        {
                            docRevisionFileName = docFile.FileName;
                        }

                        var revisionServerFileName = DateTime.Now.ToString("ddMMyyhhmmss") + "_" + docRevisionFileName;

                        // Path file to save on server disc
                        var saveFileRevisionPath = Path.Combine(Server.MapPath(targetFolder), revisionServerFileName);

                        // Path file to download from server
                        var serverFilePath = serverFolder + "/" + docFileNameOrignal;
                       // var revisionFilePath = serverRevisionFolder + revisionServerFileName;

                        var fileExt = docFileNameOrignal.Substring(docFileNameOrignal.LastIndexOf(".") + 1, docFileNameOrignal.Length - docFileNameOrignal.LastIndexOf(".") - 1);

                        objDoc.RevisionFileName = docRevisionFileName;
                        objDoc.FilePath = serverFilePath;
                        objDoc.RevisionFilePath = serverFilePath;
                        objDoc.FileExtension = fileExt;
                        objDoc.FileExtensionIcon = Utility.FileIcon.ContainsKey(fileExt.ToLower()) ? Utility.FileIcon[fileExt.ToLower()] : "images/otherfile.png";
                        objDoc.FileNameOriginal = docFileNameOrignal;
                        objDoc.DirName = folder.DirName;

                        docFile.SaveAs(saveFileRevisionPath, true);
                    }
                }
            }
            catch (Exception ex)
            {
                var watcherService = new ServiceController(ServiceName);
                if (Utility.ServiceIsAvailable(ServiceName))
                {
                    watcherService.ExecuteCommand(129);
                }
            }
        }
    }
}