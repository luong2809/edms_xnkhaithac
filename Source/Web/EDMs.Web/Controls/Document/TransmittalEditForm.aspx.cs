﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Configuration;
    using System.Collections.Generic;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using System.Linq;

    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class TransmittalEditForm : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        /// <summary>
        /// The to list service.
        /// </summary>
        private readonly ToListService toListService;

        /// <summary>
        /// The attention service.
        /// </summary>
        private readonly AttentionService attentionService;

        /// <summary>
        /// The transmittal service.
        /// </summary>
        private readonly TransmittalService transmittalService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public TransmittalEditForm()
        {
            this.documentService = new DocumentService();
            this.userService = new UserService();

            this.transmittalService = new TransmittalService();
            this.toListService = new ToListService();
            this.attentionService = new AttentionService();

        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadComboData();

                if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
                {
                    this.CreatedInfo.Visible = true;

                    var objTran = this.transmittalService.GetById(Convert.ToInt32(this.Request.QueryString["tranId"]));
                    if (objTran != null)
                    {
                        
                        this.txtContractName.Text = objTran.ContractName;
                        this.txtNoOfficialletter.Text = objTran.NoOfficialletter;

                        this.txtRemark.Text = objTran.Remark;
                        this.txtTransmittalNumber.Text = objTran.TransmittalNumber;

                        if (objTran.ReceivedDate != null)
                        {
                            this.txtReceivedDate.SelectedDate = objTran.ReceivedDate;
                        }

                        this.ddlToList.Text = objTran.ToList;
                        if (!string.IsNullOrEmpty(objTran.ToList))
                        {
                            var listAttentionTotal = new List<Attention>();
                            foreach (var text in objTran.ToList.Split(','))
                            {
                                var toListId = Convert.ToInt32(this.ddlToList.FindItemByText(text.Trim()).Value);
                                var listAttention = this.attentionService.GetAllByToListId(toListId);
                                listAttentionTotal = listAttentionTotal.Union(listAttention).ToList();
                            }

                           // listAttentionTotal.Insert(0, new Attention() { FullName = string.Empty });
                            this.ddlAttention.DataSource = listAttentionTotal;
                            this.ddlAttention.DataValueField = "ID";
                            this.ddlAttention.DataTextField = "FullName";
                            this.ddlAttention.DataBind();

                            var categoryIds = objTran.AttentionList.Trim().Split(',').Where(k => !string.IsNullOrEmpty(k)).Select(t => t).ToList();
                         
                            foreach (RadComboBoxItem item in this.ddlAttention.Items)
                            {
                                if (categoryIds.Contains(item.Text))
                                {

                                    item.Checked = true;

                                }
                                else if (categoryIds.Contains(" " + item.Text))
                                {
                                     item.Checked = true;
                                }
                                
                            }
                            this.ddlAttention.Text = objTran.AttentionList;
                        }

                        var createdUser = this.userService.GetByID(objTran.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + objTran.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (objTran.LastUpdatedBy != null && objTran.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(objTran.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + objTran.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                var categoryIds = string.Empty;
               categoryIds= this.ddlAttention.CheckedItems.Aggregate(categoryIds, (current, t) => current + t.Text + ", ");
                if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
                {
                    var tranId = Convert.ToInt32(this.Request.QueryString["tranId"]);
                    var objTran = this.transmittalService.GetById(tranId);
                    if (objTran != null)
                    {
                        
                        objTran.TransmittalNumber = this.txtTransmittalNumber.Text.Trim();
                        objTran.NoOfficialletter = this.txtNoOfficialletter.Text;
                        objTran.Remark = this.txtRemark.Text.Trim();
                        objTran.ContractName = this.txtContractName.Text.Trim();
                        objTran.ToList = this.ddlToList.Text.Trim();
                        if (!string.IsNullOrEmpty(this.ddlToList.Text))
                        {
                            objTran.ToListGroup = this.ddlToList.Text.Split(',')[0].Trim();
                        }

                        objTran.AttentionList = categoryIds;
                        objTran.ReceivedDate = this.txtReceivedDate.SelectedDate;

                        objTran.LastUpdatedBy = UserSession.Current.User.Id;
                        objTran.LastUpdatedDate = DateTime.Now;

                        this.transmittalService.Update(objTran);
                    }
                }
                else
                {
                    var objTran = new Transmittal()
                    {
                        TransmittalNumber = this.txtTransmittalNumber.Text.Trim(),
                        NoOfficialletter=this.txtNoOfficialletter.Text,
                        Remark = this.txtRemark.Text.Trim(),
                        ContractName = this.txtContractName.Text.Trim(),
                        ToList = this.ddlToList.Text.Trim(),
                        AttentionList = categoryIds,
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now,
                        FromList=UserSession.Current.User.Role.Name,
                        AttentionFrom=UserSession.Current.User.FullName,
                        ReceivedDate = this.txtReceivedDate.SelectedDate
                    };

                    if (!string.IsNullOrEmpty(this.ddlToList.Text))
                    {
                        objTran.ToListGroup = this.ddlToList.Text.Split(',')[0].Trim();
                    }
                    objTran.CCList = ConfigurationManager.AppSettings["FullNameCC"];

                    this.transmittalService.Insert(objTran);
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if(this.txtTransmittalNumber.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter transmittal name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
            //else if (!string.IsNullOrEmpty(Request.QueryString["tranId"]))
            //{
            //    var tranId = Convert.ToInt32(Request.QueryString["tranId"]);
            //    var folderId = Convert.ToInt32(Request.QueryString["folId"]);
            //    this.fileNameValidator.ErrorMessage = "The specified name is already in use.";
            //    this.divFileName.Style["margin-bottom"] = "-26px;";
            //    args.IsValid = !this.documentService.IsDocumentExistUpdate(folderId, this.txtTransmittalNumber.Text.Trim(), tranId);
            //}
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {
            var toList = this.toListService.GetAll();
            toList.Insert(0, new ToList { Name = string.Empty });
            this.ddlToList.DataSource = toList;
            this.ddlToList.DataValueField = "ID";
            this.ddlToList.DataTextField = "Name";
            this.ddlToList.DataBind();
        }

        protected void ddlToList_SelectecIndexChange(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            ddlToList.Focus();
            var comboText = e.Text;
            var listAttentionTotal = new List<Attention>();
            foreach (var text in comboText.Split(','))
            {
                var toListId = Convert.ToInt32(this.ddlToList.FindItemByText(text.Trim()).Value);
                var listAttention = this.attentionService.GetAllByToListId(toListId);
                listAttentionTotal = listAttentionTotal.Union(listAttention).ToList();
            }

           // listAttentionTotal.Insert(0, new Attention() { FullName = string.Empty });
            this.ddlAttention.DataSource = listAttentionTotal;
            this.ddlAttention.DataValueField = "ID";
            this.ddlAttention.DataTextField = "FullName";
            this.ddlAttention.DataBind();
        }
    }
}