﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web;
    using System.Web.UI;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class FolderPermission_Bak : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly GroupDataPermissionService groupDataPermissionService;

        /// <summary>
        /// The user data permission.
        /// </summary>
        private readonly UserDataPermissionService userDataPermissionService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly FolderService folderService;

        /// <summary>
        /// The document service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The role service.
        /// </summary>
        private readonly RoleService roleService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        /// <summary>
        /// The service name.
        /// </summary>
        protected const string ServiceName = "EDMSFolderWatcher";

        private List<int> AdminGroup
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("GroupAdminList").Split(',').Select(t => Convert.ToInt32(t)).ToList();
            }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="FolderPermission"/> class.
        /// </summary>
        public FolderPermission_Bak()
        {
            this.groupDataPermissionService = new GroupDataPermissionService();
            this.folderService = new FolderService();
            this.documentService = new DocumentService();
            this.roleService = new RoleService();
            this.userService = new UserService();
            this.userDataPermissionService = new UserDataPermissionService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["folderId"]))
                {
                    this.lblFolderDirName.Text = "<font style='text-decoration: underline;'>Folder</font>: ";
                    var folderId = Request.QueryString["folderId"];
                    var folderObj = this.folderService.GetById(Convert.ToInt32(folderId));
                    this.CreatedInfo.Visible = true;
                    var createdUser = this.userService.GetByID(folderObj.CreatedBy.GetValueOrDefault());

                    this.lblCreated.Text = "Created at " + folderObj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                    if (folderObj.LastUpdatedBy != null && folderObj.LastUpdatedDate != null)
                    {
                        this.lblCreated.Text += "<br/>";
                        var lastUpdatedUser = this.userService.GetByID(folderObj.LastUpdatedBy.GetValueOrDefault());
                        this.lblUpdated.Text = "Last modified at " + folderObj.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                    }
                    else
                    {
                        this.lblUpdated.Visible = false;
                    }
                    if (folderObj != null)
                    {
                        this.lblFolderDirName.Text += folderObj.DirName;
                        this.LoadComboData();
                        //this.LoadPermissionData(folderId.Trim());
                    }
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["folderId"]))
            {
                var listFolderId = new List<int>();
                var selectedFolderId = Request.QueryString["folderId"];
                var selectedFolder = this.folderService.GetById(Convert.ToInt32(selectedFolderId));

                var selectedGroup = this.ddlGroup.SelectedValue;
                var selectedUser = this.ddlUser.SelectedValue;

                if (selectedFolder != null)
                {
                    var categoryId = selectedFolder.CategoryID;
                    var listParentFolderId = this.GetAllParentID(Convert.ToInt32(selectedFolderId), listFolderId);
                    listParentFolderId.Add(Convert.ToInt32(selectedFolderId));
                    ////var cookie = Request.Cookies["allchildfolder"];
                    var allChildFolderSession = Session["allChildFolder"];

                    //var allChildFolderWithoutNativeFileFolderSession = Session["allChildFolderWithoutNativeFileFolder"];

                    if (selectedUser == "0")
                    {
                        var groupDatapermission =
                            this.groupDataPermissionService.GetByRoleId(Convert.ToInt32(selectedGroup));

                        var addingPermission =
                            listParentFolderId.Where(
                                t => !groupDatapermission.Select(x => x.FolderIdList).Contains(t.ToString())).Select(
                                    t =>
                                    new GroupDataPermission()
                                    {
                                        CategoryIdList = categoryId.ToString(),
                                        RoleId = Convert.ToInt32(selectedGroup),
                                        FolderIdList = t.ToString(),
                                        ReadFile = this.cbRead.Checked,
                                        ModifyFile = this.cbModify.Checked,
                                        DeleteFile = this.cbDelete.Checked,
                                        CreateFolder = this.cbCreate.Checked,
                                        IsFullPermission = this.cbFull.Checked,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = UserSession.Current.User.Id
                                    }).ToList();

                        if (this.cbApplyAll.Checked)
                        {
                            if (allChildFolderSession != null)
                            {
                                //var allChildNodes = this.cbFull.Checked
                                //                    ? (List<string>)allChildFolderSession
                                //                    : (List<string>)allChildFolderWithoutNativeFileFolderSession;

                                var allChildNodes = (List<string>)allChildFolderSession;

                                addingPermission.AddRange(
                                    allChildNodes.Select(
                                        t =>
                                        new GroupDataPermission()
                                        {
                                            CategoryIdList = categoryId.ToString(),
                                            RoleId = Convert.ToInt32(selectedGroup),
                                            FolderIdList = t,
                                            ReadFile = this.cbRead.Checked,
                                            ModifyFile = this.cbModify.Checked,
                                            DeleteFile = this.cbDelete.Checked,
                                            CreateFolder = this.cbCreate.Checked,
                                            IsFullPermission = this.cbFull.Checked,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = UserSession.Current.User.Id
                                        }));

                                addingPermission = addingPermission.Distinct().ToList();
                            }
                        }

                        this.groupDataPermissionService.AddGroupDataPermissions(addingPermission.ToList());

                        var userPermission = this.userDataPermissionService.GetByUserInRole(Convert.ToInt32(selectedGroup), Convert.ToInt32(selectedFolderId));
                        foreach (var itemPermission in userPermission)
                        {
                            if (itemPermission != null)
                            {
                                var userDatapermission = this.userDataPermissionService.GetByUserId(itemPermission.UserId.GetValueOrDefault());
                                var deletePermission = userDatapermission.Where(t => listParentFolderId.Contains(t.FolderId.GetValueOrDefault()) || t.FolderId.GetValueOrDefault() == Convert.ToInt32(selectedFolderId));
                                this.userDataPermissionService.DeleteUserDataPermission(deletePermission.ToList());
                            }
                        }
                    }
                    else
                    {
                        var userDatapermission =
                            this.userDataPermissionService.GetByUserId(Convert.ToInt32(selectedUser));
                        var addingPermission =
                            listParentFolderId.Where(
                                t => !userDatapermission.Select(x => x.FolderId.ToString()).Contains(t.ToString())).Select(
                                    t =>
                                    new UserDataPermission()
                                    {
                                        CategoryId = categoryId,
                                        RoleId = Convert.ToInt32(selectedGroup),
                                        FolderId = t,
                                        UserId = Convert.ToInt32(selectedUser),
                                        ReadFile = this.cbRead.Checked,
                                        ModifyFile = this.cbModify.Checked,
                                        DeleteFile = this.cbDelete.Checked,
                                        CreateFolder = this.cbCreate.Checked,
                                        IsFullPermission = this.cbFull.Checked,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = UserSession.Current.User.Id
                                    }).ToList();

                        if (this.cbApplyAll.Checked)
                        {
                            if (allChildFolderSession != null)
                            {
                                //var allChildNodes = this.cbFull.Checked
                                //                    ? (List<string>)allChildFolderSession
                                //                    : (List<string>)allChildFolderWithoutNativeFileFolderSession;
                                var allChildNodes = (List<string>)allChildFolderSession;

                                addingPermission.AddRange(
                                    allChildNodes.Select(
                                        t =>
                                        new UserDataPermission()
                                        {
                                            CategoryId = categoryId,
                                            RoleId = Convert.ToInt32(selectedGroup),
                                            FolderId = Convert.ToInt32(t),
                                            UserId = Convert.ToInt32(selectedUser),
                                            ReadFile = this.cbRead.Checked,
                                            ModifyFile = this.cbModify.Checked,
                                            DeleteFile = this.cbDelete.Checked,
                                            CreateFolder = this.cbCreate.Checked,
                                            IsFullPermission = this.cbFull.Checked,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = UserSession.Current.User.Id
                                        }));
                            }
                        }

                        this.userDataPermissionService.AddUserDataPermissions(addingPermission.ToList());
                    }

                    this.LoadComboData();
                    this.grdPermission.Rebind();

                    ////Session.Remove("allChildFolder");
                    ////Session.Remove("allChildFolderWithoutNativeFileFolder");
                }
            }
        }

        private List<int> GetAllParentID(int folderId, List<int> listFolderId)
        {
            var folder = this.folderService.GetById(folderId);
            if (folder.ParentID != null)
            {
                listFolderId.Add(folder.ParentID.Value);
                this.GetAllParentID(folder.ParentID.Value, listFolderId);
            }

            return listFolderId;
        }

        private void LoadComboData()
        {
            var groupsInPermission = this.groupDataPermissionService.GetAllByFolder(Request.QueryString["folderId"]).Select(t => t.RoleId).Distinct().ToList();
            var listGroup = this.roleService.GetAll().Where(t => !groupsInPermission.Contains(t.Id) && !this.AdminGroup.Contains(t.Id));
            this.ddlGroup.DataSource = listGroup;
            this.ddlGroup.DataTextField = "Name";
            this.ddlGroup.DataValueField = "Id";
            this.ddlGroup.DataBind();

            var usersInPermission = this.userDataPermissionService.GetAllByFolder(Convert.ToInt32(Request.QueryString["folderId"])).Select(t => t.UserId).Distinct().ToList();
            var listUser = new List<User>();
            if (this.ddlGroup.SelectedItem != null)
            {
                listUser = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroup.SelectedValue)).Where(t => !usersInPermission.Contains(t.Id)).OrderBy(t => t.FullName).ToList();
            }

            listUser.Insert(0, new User { Id = 0, FullName = string.Empty });

            this.ddlUser.DataSource = listUser;
            this.ddlUser.DataTextField = "Username";
            this.ddlUser.DataValueField = "Id";
            this.ddlUser.DataBind();
        }

        protected void grdPermission_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["folderId"]))
            {
                var groupsInPermission = this.groupDataPermissionService.GetAllByFolder(Request.QueryString["folderId"])
                                            .Where(t => !this.AdminGroup.Contains(t.RoleId.GetValueOrDefault()));
                var usersInPermission = this.userDataPermissionService.GetAllByFolder(Convert.ToInt32(Request.QueryString["folderId"]));

                var listDataPermission = usersInPermission
                                        .Select(t => new DataPermission
                                        {
                                            ID = t.ID,
                                            ReadFile = t.ReadFile.GetValueOrDefault(),
                                            ModifyFile = t.ModifyFile.GetValueOrDefault(),
                                            DeleteFile = t.DeleteFile.GetValueOrDefault(),
                                            CreateFolder = t.CreateFolder.GetValueOrDefault(),
                                            IsFullPermission = t.IsFullPermission.GetValueOrDefault(),
                                            Name = t.UserFullName,
                                            IsGroup = false
                                        }).ToList();

                listDataPermission = listDataPermission.Union(
                    groupsInPermission.Select(
                        t =>
                        new DataPermission
                        {
                            ID = t.ID,
                            ReadFile = t.ReadFile.GetValueOrDefault(),
                            ModifyFile = t.ModifyFile.GetValueOrDefault(),
                            DeleteFile = t.DeleteFile.GetValueOrDefault(),
                            CreateFolder = t.CreateFolder.GetValueOrDefault(),
                            IsFullPermission = t.IsFullPermission.GetValueOrDefault(),
                            Name = t.GroupName,
                            IsGroup = true
                        })).ToList();

                this.grdPermission.DataSource = listDataPermission;
            }
            else
            {
                this.grdPermission.DataSource = new List<DataPermission>();
            }
        }

        protected void ddlGroup_SelectedIndexChange(object sender, EventArgs e)
        {
            var usersInPermission = this.userDataPermissionService.GetAllByFolder(Convert.ToInt32(Request.QueryString["folderId"])).Select(t => t.UserId).Distinct().ToList();
            var listUser = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroup.SelectedValue)).Where(t => !usersInPermission.Contains(t.Id)).OrderBy(t => t.FullName).ToList();

            listUser.Insert(0, new User { Id = 0, FullName = string.Empty });

            this.ddlUser.DataSource = listUser;
            this.ddlUser.DataTextField = "FullName";
            this.ddlUser.DataValueField = "Id";
            this.ddlUser.DataBind();
        }

        protected void grdPermission_OnDeteleCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var permissionId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var isGroup = Convert.ToBoolean(item["IsGroup"].Text);
            var selectedFolderId = Request.QueryString["folderId"];
            var allChildFolderSession = Session["allChildFolder"];

            ////var cookie = Request.Cookies["allchildfolder"];

            if (isGroup)
            {
                var groupPermission = this.groupDataPermissionService.GetById(permissionId);
                if (groupPermission != null)
                {
                    var groupDatapermission = this.groupDataPermissionService.GetByRoleId(groupPermission.RoleId.GetValueOrDefault());
                    if (allChildFolderSession != null)
                    {
                        var allChildNodes = (List<string>)allChildFolderSession;
                        ////allChildNodes.Add(selectedFolderId);

                        var deletePermission = groupDatapermission.Where(t => allChildNodes.Contains(t.FolderIdList) || t.FolderIdList == selectedFolderId);
                        this.groupDataPermissionService.DeleteGroupDataPermission(deletePermission.ToList());
                    }
                }
            }
            else
            {
                var userPermission = this.userDataPermissionService.GetById(permissionId);
                if (userPermission != null)
                {
                    var userDatapermission = this.userDataPermissionService.GetByUserId(userPermission.UserId.GetValueOrDefault());
                    if (allChildFolderSession != null)
                    {
                        var allChildNodes = (List<string>)allChildFolderSession;
                        ////allChildNodes.Add(selectedFolderId);

                        var deletePermission = userDatapermission.Where(t => allChildNodes.Contains(t.FolderId.ToString()) || t.FolderId.ToString() == selectedFolderId);
                        this.userDataPermissionService.DeleteUserDataPermission(deletePermission.ToList());
                    }
                }
            }

            this.LoadComboData();
        }

        protected void cbFull_CheckedChanged(object sender, EventArgs e)
        {
            if (cbFull.Checked)
            {
                cbRead.Checked = true;
                cbModify.Checked = true;
                cbDelete.Checked = true;
                cbCreate.Checked = true;
            }
            else
            {
                cbRead.Checked = false;
                cbModify.Checked = false;
                cbDelete.Checked = false;
                cbCreate.Checked = false;
            }
        }

        protected void cbModify_CheckedChanged(object sender, EventArgs e)
        {
            if (cbModify.Checked)
            {
                cbRead.Checked = true;
            }
        }

        protected void cbDelete_CheckedChanged(object sender, EventArgs e)
        {
            if (cbDelete.Checked)
            {
                cbRead.Checked = true;
            }
        }
    }
}