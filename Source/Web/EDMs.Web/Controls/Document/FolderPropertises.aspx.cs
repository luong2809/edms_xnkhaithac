﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web;
    using System.Web.UI;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class FolderPropertises : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly GroupDataPermissionService groupDataPermissionService;

        /// <summary>
        /// The user data permission.
        /// </summary>
        private readonly UserDataPermissionService userDataPermissionService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly FolderService folderService;

        /// <summary>
        /// The document service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The role service.
        /// </summary>
        private readonly RoleService roleService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        /// <summary>
        /// The service name.
        /// </summary>
        protected const string ServiceName = "EDMSFolderWatcher";

        private List<int> AdminGroup
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("GroupAdminList").Split(',').Select(t => Convert.ToInt32(t)).ToList();
            }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="FolderPermission"/> class.
        /// </summary>
        public FolderPropertises()
        {
            this.groupDataPermissionService = new GroupDataPermissionService();
            this.folderService = new FolderService();
            this.documentService = new DocumentService();
            this.roleService = new RoleService();
            this.userService = new UserService();
            this.userDataPermissionService = new UserDataPermissionService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["folderId"]))
                {
                    this.lblFolderDirName.Text = "<font style='text-decoration: underline;'>Folder</font>: ";
                    var folderId = Request.QueryString["folderId"];
                    var folderObj = this.folderService.GetById(Convert.ToInt32(folderId));
                    this.lblName.Text = folderObj.Name;
                    this.lblSubFolder.Text = folderObj.NumberOfSubfolder.ToString();
                    this.lblFile.Text = folderObj.NumberOfDocument.ToString();
                    this.lblFolderSize.Text = SizeSuffix(folderObj.FolderSize.HasValue ? Convert.ToInt64(folderObj.FolderSize.Value) : 0, 1);
                    this.CreatedInfo.Visible = true;
                    var createdUser = this.userService.GetByID(folderObj.CreatedBy.GetValueOrDefault());

                    this.lblCreated.Text = "Created at " + folderObj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                    if (folderObj.LastUpdatedBy != null && folderObj.LastUpdatedDate != null)
                    {
                        this.lblCreated.Text += "<br/>";
                        var lastUpdatedUser = this.userService.GetByID(folderObj.LastUpdatedBy.GetValueOrDefault());
                        this.lblUpdated.Text = "Last modified at " + folderObj.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                    }
                    else
                    {
                        this.lblUpdated.Visible = false;
                    }
                    if (folderObj != null)
                    {
                        this.lblFolderDirName.Text += folderObj.DirName;
                    }
                }
            }
        }

        protected readonly string[] SizeSuffixes =
                   { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
        public string SizeSuffix(Int64 value, int decimalPlaces = 1)
        {
            if (decimalPlaces < 0) { throw new ArgumentOutOfRangeException("decimalPlaces"); }
            if (value < 0) { return "-" + SizeSuffix(-value); }
            if (value == 0) { return string.Format("{0:n" + decimalPlaces + "} bytes", 0); }

            // mag is 0 for bytes, 1 for KB, 2, for MB, etc.
            int mag = (int)Math.Log(value, 1024);

            // 1L << (mag * 10) == 2 ^ (10 * mag) 
            // [i.e. the number of bytes in the unit corresponding to mag]
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            // make adjustment when the value is large enough that
            // it would round up to 1000 or more
            if (Math.Round(adjustedSize, decimalPlaces) >= 1000)
            {
                mag += 1;
                adjustedSize /= 1024;
            }

            return string.Format("{0:n" + decimalPlaces + "} {1}",
                adjustedSize,
                SizeSuffixes[mag]);
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>

        private List<int> GetAllParentID(int folderId, List<int> listFolderId)
        {
            var folder = this.folderService.GetById(folderId);
            if (folder.ParentID != null)
            {
                listFolderId.Add(folder.ParentID.Value);
                this.GetAllParentID(folder.ParentID.Value, listFolderId);
            }

            return listFolderId;
        }


        protected void grdPermission_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["folderId"]))
            {
                var groupsInPermission = this.groupDataPermissionService.GetAllByFolder(Request.QueryString["folderId"])
                                            .Where(t => !this.AdminGroup.Contains(t.RoleId.GetValueOrDefault()));
                var usersInPermission = this.userDataPermissionService.GetAllByFolder(Convert.ToInt32(Request.QueryString["folderId"]));

                var listDataPermission = usersInPermission
                                        .Select(t => new DataPermission
                                        {
                                            ID = t.ID,
                                            ReadFile = t.ReadFile.GetValueOrDefault(),
                                            ModifyFile = t.ModifyFile.GetValueOrDefault(),
                                            DeleteFile = t.DeleteFile.GetValueOrDefault(),
                                            CreateFolder = t.CreateFolder.GetValueOrDefault(),
                                            IsFullPermission = t.IsFullPermission.GetValueOrDefault(),
                                            Name = t.UserFullName,
                                            IsGroup = false
                                        }).ToList();

                listDataPermission = listDataPermission.Union(
                    groupsInPermission.Select(
                        t =>
                        new DataPermission
                        {
                            ID = t.ID,
                            ReadFile = t.ReadFile.GetValueOrDefault(),
                            ModifyFile = t.ModifyFile.GetValueOrDefault(),
                            DeleteFile = t.DeleteFile.GetValueOrDefault(),
                            CreateFolder = t.CreateFolder.GetValueOrDefault(),
                            IsFullPermission = t.IsFullPermission.GetValueOrDefault(),
                            Name = t.GroupName,
                            IsGroup = true
                        })).ToList();

                this.grdPermission.DataSource = listDataPermission;
            }
            else
            {
                this.grdPermission.DataSource = new List<DataPermission>();
            }
        }


        protected void grdPermission_OnDeteleCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var permissionId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var isGroup = Convert.ToBoolean(item["IsGroup"].Text);
            var selectedFolderId = Request.QueryString["folderId"];
            var allChildFolderSession = Session["allChildFolder"];

            ////var cookie = Request.Cookies["allchildfolder"];

            if (isGroup)
            {
                var groupPermission = this.groupDataPermissionService.GetById(permissionId);
                if (groupPermission != null)
                {
                    var groupDatapermission = this.groupDataPermissionService.GetByRoleId(groupPermission.RoleId.GetValueOrDefault());
                    if (allChildFolderSession != null)
                    {
                        var allChildNodes = (List<string>)allChildFolderSession;
                        ////allChildNodes.Add(selectedFolderId);

                        var deletePermission = groupDatapermission.Where(t => allChildNodes.Contains(t.FolderIdList) || t.FolderIdList == selectedFolderId);
                        this.groupDataPermissionService.DeleteGroupDataPermission(deletePermission.ToList());
                    }
                }
            }
            else
            {
                var userPermission = this.userDataPermissionService.GetById(permissionId);
                if (userPermission != null)
                {
                    var userDatapermission = this.userDataPermissionService.GetByUserId(userPermission.UserId.GetValueOrDefault());
                    if (allChildFolderSession != null)
                    {
                        var allChildNodes = (List<string>)allChildFolderSession;
                        ////allChildNodes.Add(selectedFolderId);

                        var deletePermission = userDatapermission.Where(t => allChildNodes.Contains(t.FolderId.ToString()) || t.FolderId.ToString() == selectedFolderId);
                        this.userDataPermissionService.DeleteUserDataPermission(deletePermission.ToList());
                    }
                }
            }
        }
    }
}