﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// Class customer
    /// </summary>
    public partial class DocumentBin : Page
    {
        private readonly PermissionService permissionService = new PermissionService();

        private readonly DocumentService documentService = new DocumentService();
        private readonly FolderService folderService = new FolderService();

        private readonly GroupDataPermissionService groupDataPermissionService = new GroupDataPermissionService();

        private readonly CategoryService categoryService = new CategoryService();

        private readonly UserDataPermissionService userDataPermissionService = new UserDataPermissionService();

        private List<int> AdminGroup
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("GroupAdminList").Split(',').Select(t => Convert.ToInt32(t)).ToList();
            }
        }
        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var categoryPermission = this.groupDataPermissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault()).Select(t => t.CategoryIdList).ToList();
                categoryPermission = categoryPermission.Union(this.userDataPermissionService.GetByUserId(UserSession.Current.User.Id).Select(t => t.CategoryId.ToString())).Distinct().ToList();
                var listCategory = this.AdminGroup.Contains(UserSession.Current.RoleId)
                                    ? this.categoryService.GetAll()
                                    : this.categoryService.GetAll().Where(t => categoryPermission.Any(x => x == t.ID.ToString())).ToList();
                if (listCategory.Any())
                {
                    foreach (var category in listCategory)
                    {
                        category.ParentId = -1;
                    }

                    listCategory.Insert(0, new Category() { ID = -1, Name = "DOCUMENT GROUP" });

                    this.radPbCategories.DataSource = listCategory;
                    this.radPbCategories.DataFieldParentID = "ParentId";
                    this.radPbCategories.DataFieldID = "Id";
                    this.radPbCategories.DataValueField = "Id";
                    this.radPbCategories.DataTextField = "Name";
                    this.radPbCategories.DataBind();
                    this.radPbCategories.Items[0].Expanded = true;

                    foreach (RadPanelItem item in this.radPbCategories.Items[0].Items)
                    {
                        item.ImageUrl = @"Images/category2.png";
                        item.NavigateUrl = "../../DocumentsHome.aspx?doctype=" + item.Value;
                    }
                }

                this.LoadListPanel();
                this.LoadSystemPanel();
            }
        }

        /// <summary>
        /// The search customer.
        /// </summary>
        /// <param name="search">
        /// The search.
        /// </param>
        /// <param name="isbind">
        /// The isbind.
        /// </param>
        protected void SearchDocument(string search, bool isbind = false)
        {
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.Rebind();
            }

            else
            {
                SearchDocument(e.Argument, true);
            }
        }

        /// <summary>
        /// Handles the OnNeedDataSource event of the grdRoles control.
        /// </summary>
        /// <param name="source">The source of the event.</param>
        /// <param name="e">The <see cref="GridNeedDataSourceEventArgs"/> instance containing the event data.</param>
        protected void grdRoles_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            LoadDocuments(false);
        }

        protected void LoadDocuments(bool isbind = false)
        {
            var docList = this.documentService.GetAllDelete();
            var childFolders = this.folderService.GetAllMainFolderDelete();
            var listFolderdoc = childFolders.Select(t => new Document()
            {
                ID = Convert.ToInt32(t.ID),
                Name = t.Name,
                Title = t.Description,
                FileExtensionIcon = "Images/folderdir16.png",
                LastUpdatedDate = t.LastUpdatedDate,
                IsFolder = true
            }).ToList();
            listFolderdoc = listFolderdoc.Union(docList).ToList();
            this.grdDocument.DataSource = listFolderdoc;
            if (isbind)
            {
                this.grdDocument.DataBind();
            }
        }

        /// <summary>
        /// Handles the ItemCommand event of the grdRoles control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="GridCommandEventArgs"/> instance containing the event data.</param>
        protected void grdRoles_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if(e.CommandName == "RebindGrid")
            {
                this.grdDocument.Rebind();
            }
            if (e.CommandName == "DeleteCommand")
            {
                var item = (GridDataItem)e.Item;
                if (item["IsFolder"].Text == "True")
                {
                    var folderId = Convert.ToInt32(item.GetDataKeyValue("Id").ToString());
                    this.folderService.Delete(folderId);
                    this.grdDocument.Rebind();
                }
                else
                {
                    var docId = Convert.ToInt32(item.GetDataKeyValue("Id").ToString());
                    this.documentService.Delete(docId);
                    this.grdDocument.Rebind();
                }
            }
            if (e.CommandName == "RestoreCommand")
            {
                var item = (GridDataItem)e.Item;
                if (item["IsFolder"].Text == "True")
                {
                    var folderId = Convert.ToInt32(item.GetDataKeyValue("Id").ToString());
                    var folderObj = this.folderService.GetById(folderId);
                    var docListMainFolder = this.documentService.GetAllDeleteByFolder(folderId);
                    List<Folder> childNodes = new List<Folder>();
                    var childFolderList = GetAllChildren(folderId, ref childNodes);

                    //restore main folder and file in main folder
                    folderObj.IsBin = false;
                    folderObj.Isdelete = false;
                    this.folderService.Update(folderObj);
                    foreach (var docItemMainFolder in docListMainFolder)
                    {
                        if (docItemMainFolder.IsDelete.GetValueOrDefault())
                        {
                            docItemMainFolder.IsLeaf = true;
                            docItemMainFolder.IsDelete = false;
                            docItemMainFolder.LastUpdatedBy = UserSession.Current.User.Id;
                            docItemMainFolder.LastUpdatedDate = DateTime.Now;
                            this.documentService.Update(docItemMainFolder);
                        }
                    }

                    foreach (var folderItem in childFolderList)
                    {
                        if (folderItem.Isdelete.GetValueOrDefault() && !folderItem.IsBin.GetValueOrDefault())
                        {
                            folderItem.Isdelete = false;
                            folderItem.IsBin = false;
                            folderItem.LastUpdatedBy = UserSession.Current.User.Id;
                            folderItem.LastUpdatedDate = DateTime.Now;
                            this.folderService.Update(folderItem);
                            var docList = this.documentService.GetAllDeleteByFolder(folderItem.ID);
                            foreach (var docItem in docList)
                            {
                                if (docItem.IsDelete.GetValueOrDefault())
                                {
                                    docItem.IsLeaf = true;
                                    docItem.IsDelete = false;
                                    docItem.LastUpdatedBy = UserSession.Current.User.Id;
                                    docItem.LastUpdatedDate = DateTime.Now;
                                    this.documentService.Update(docItem);
                                }
                            }
                        }
                    }
                }
                else
                {
                    int docID = Convert.ToInt32(item.GetDataKeyValue("Id").ToString());
                    var docObj = this.documentService.GetById(docID);
                    if (docObj != null)
                    {
                        docObj.IsLeaf = true;
                        docObj.IsDelete = false;
                        docObj.LastUpdatedBy = UserSession.Current.User.Id;
                        docObj.LastUpdatedDate = DateTime.Now;
                        this.documentService.Update(docObj);
                        this.grdDocument.Rebind();
                    }
                }
                grdDocument.Rebind();
            }
        }

        private List<Folder> GetAllChildren(int parent, ref List<Folder> childNodes)
        {
            var childFolderList = this.folderService.GetAllDeleteByParentId(parent).ToList();
            if (childFolderList.Any())
            {
                childNodes.AddRange(childFolderList);
                foreach (var childFolder in childFolderList)
                {
                    this.GetAllChildren(childFolder.ID, ref childNodes);
                }
            }

            return childNodes.Distinct().ToList();
        }

        /// <summary>
        /// Handles the OnItemDataBound event of the grdRoles control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="GridItemEventArgs"/> instance containing the event data.</param>
        protected void grdRoles_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            var currentItem = e.Item as GridDataItem;
        }

        private void LoadListPanel()
        {
            var listId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("ListID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), listId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "LIST" });

                this.radPbList.DataSource = permissions;
                this.radPbList.DataFieldParentID = "ParentId";
                this.radPbList.DataFieldID = "Id";
                this.radPbList.DataValueField = "Id";
                this.radPbList.DataTextField = "MenuName";
                this.radPbList.DataBind();
                this.radPbList.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbList.Items[0].Items)
                {
                    item.ImageUrl = @"Images/listmenu.png";
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                }
            }
        }

        private void LoadSystemPanel()
        {
            var systemId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("SystemID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), systemId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "SYSTEM" });

                this.radPbSystem.DataSource = permissions;
                this.radPbSystem.DataFieldParentID = "ParentId";
                this.radPbSystem.DataFieldID = "Id";
                this.radPbSystem.DataValueField = "Id";
                this.radPbSystem.DataTextField = "MenuName";
                this.radPbSystem.DataBind();
                this.radPbSystem.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbSystem.Items[0].Items)
                {
                    if (item.Text == "Users" || item.Text == "Groups" || item.Text == "Menu Permission")
                    {
                        item.NavigateUrl = "../../" + permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                    }
                    else
                    {
                        item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;

                    }
                    item.ImageUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Icon;
                    if (item.Text == "Bin")
                    {
                        item.Selected = true;
                    }
                }
            }
        }
    }
}

