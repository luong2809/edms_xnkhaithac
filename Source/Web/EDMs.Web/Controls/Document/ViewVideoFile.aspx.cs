﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web.UI;
    using EDMs.Business.Services;
    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ViewVideoFile : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly DCRService dcrService;

        private readonly CheckOutInHistoryService checkOutInHistoryService;

        protected const string ServiceName = "EDMSFolderWatcher";

        private List<int> AdminGroup
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("GroupAdminList").Split(',').Select(t => Convert.ToInt32(t)).ToList();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ViewVideoFile()
        {
            this.documentService = new DocumentService();
            this.dcrService = new DCRService();
            this.checkOutInHistoryService = new CheckOutInHistoryService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
                {
                    var docId = Convert.ToInt32(Request.QueryString["docId"]);
                    //ConfigureMediaPlayer(GetVideoFiles(docId));

                }
            }
        }

        

        /// <summary>
        /// The rad ajax manager 1_ ajax request.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.Contains("CheckFileName"))
            {
            }
        }

        //private MediaPlayerVideoFile GetVideoFiles(int docId)
        //{
        //    var docObj = this.documentService.GetById(docId);
        //    this.Videopath.HRef = docObj.FilePath;
        //    this.Videopath.InnerText = docObj.Title;
        //    this.lblDocNo.Text = "Document: " + docObj.DocumentNumber;
        //    var data = new MediaPlayerVideoFile
        //    {
        //        Title = docObj.Title,
        //        Path = docObj.Title,
        //    };

        //    data.Sources.Add(new MediaPlayerSource() { Path = docObj.FilePath });
        //    return data;
        //}

        //private void ConfigureMediaPlayer(MediaPlayerFile file)
        //{
        //    RadMediaPlayer1.Sources.Clear();
        //    RadMediaPlayer1.StartTime = 0;
        //    RadMediaPlayer1.Muted = false;
        //    RadMediaPlayer1.AutoPlay = false;
        //    RadMediaPlayer1.Title = file.Title;
        //    //RadMediaPlayer1.Poster = file.Title == "AppBuilder" ? "images/appBuilderPoster.png" : "";

        //    foreach (MediaPlayerSource source in file.Sources)
        //    {
        //        RadMediaPlayer1.Sources.Add(source);
        //        source.Path = source.Path;
        //        source.MimeType = source.MimeType;

        //        MediaPlayerSource hdSource = new MediaPlayerSource();
        //        RadMediaPlayer1.Sources.Add(hdSource);
        //        hdSource.MimeType = source.MimeType;
        //        hdSource.Path = source.Path;
        //        hdSource.IsHD = true;
        //    }
        //}
    }
}