﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewVideoFile.aspx.cs" Inherits="EDMs.Web.Controls.Document.ViewVideoFile" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        html, body, form {
            overflow: auto;
        }

        .RadComboBoxDropDown_Default .rcbHovered {
            background-color: #46A3D3;
            color: #fff;
        }

        .RadComboBoxDropDown .rcbItem, .RadComboBoxDropDown .rcbHovered, .RadComboBoxDropDown .rcbDisabled, .RadComboBoxDropDown .rcbLoading, .RadComboBoxDropDown .rcbCheckAllItems, .RadComboBoxDropDown .rcbCheckAllItemsHovered {
            margin: 0 0px;
        }

        .RadComboBox .rcbInputCell .rcbInput {
            border-left-color: #FF0000 !important;
            border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;
            border-style: solid;
            border-width: 1px 1px 1px 5px;
            color: #000000;
            float: left;
            font: 12px "segoe ui";
            margin: 0;
            padding: 2px 5px 3px;
            vertical-align: middle;
            width: 100px;
        }

        .RadComboBox table td.rcbInputCell, .RadComboBox .rcbInputCell .rcbInput {
            padding-left: 0px !important;
        }

        div.rgEditForm label {
            float: right;
            text-align: right;
            width: 72px;
        }

        .rgEditForm {
            text-align: right;
        }

        .RadComboBox {
            width: 115px !important;
            border-bottom: none !important;
        }

        .RadUpload .ruFileWrap {
            overflow: visible !important;
        }

        div.qlcbFormItem select.min25Percent {
            max-width: 500px !important;
        }

        /*#grdContactHistory
        {
            height: 300px !important;
        }

        #grdContactHistory_GridData
        {
            height: 250px !important;
        }*/
    </style>

    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function CloseAndRebind(args) {
                GetRadWindow().BrowserWindow.refreshGrid(args);
                GetRadWindow().close();
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

                return oWindow;
            }

            function CancelEdit() {
                GetRadWindow().close();
            }

            function CloseVideo() {
                $('video').trigger('pause');
            }

            <%--function CloseVideo() {
            var mediaplayer = $find("<%=RadMediaPlayer1.ClientID %>");
            mediaplayer.stop();
        }--%>

        </script>
    </telerik:RadCodeBlock>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />

                <div style="width: 100%">
            <ul style="list-style-type: none">
                <div class="qlcbFormItem">
                    <div class="dnnFormMessage dnnFormInfo">
                        <div class="dnnFormItem dnnFormHelp dnnClear">
                            <p class="dnnFormRequired" style="float: left;">
                                <asp:Label runat="server" ID="lblDocNo"></asp:Label> (<a runat="server" ID="Videopath" target="_blank"></a>)
                            </p>
                            <br />
                        </div>
                    </div>
                </div>
            </ul>
        </div>

       <%-- <div style="width: 100%" runat="server" ID="divContent">
            <ul style="list-style-type: none">
                
                <li style="width: 500px;">
                    <telerik:RadMediaPlayer ID="RadMediaPlayer1" runat="server" Width="700px" BackColor="Black"
                        StartVolume="100" Height="394px">
                    </telerik:RadMediaPlayer>
                  <%--  <video width="700" height="394" runat="server" controls autoplay>
                          <source  id="plmp4" runat="server" type='video/mp4; codecs="avc1.4D401E, mp4a.40.2"'>
                          <source   id="plogg" runat="server" type="video/ogg">
                          <source  id="plwebm" runat="server" type='video/webm; codecs="vp8.0, vorbis"'>
                          <source  id="plogv" runat="server" type='video/ogv; codecs="theora, vorbis"'>
                        Your browser does not support the video tag.
                        </video>--%
                </li>
                
            </ul>
        </div>--%>
          <div id="divVideo" runat="server" style="width: 100%">
        </div>
        <asp:HiddenField runat="server" ID="docUploadedIsExist"/>
        <asp:HiddenField runat="server" ID="docIdUpdateUnIsLeaf"/>
        
        <telerik:RadAjaxManager runat="Server" ID="ajaxDocument">
            <AjaxSettings> 
                <telerik:AjaxSetting AjaxControlID="ajaxDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContent" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>

        <telerik:RadScriptBlock runat="server">
            <script type="text/javascript">
                var ajaxManager;
                function OnClientFilesUploaded(sender, args) {
                    var name = args.get_fileName();
                    document.getElementById("txtName").value = name;
                }

                function pageLoad() {
                    ajaxManager = $find("<%=ajaxDocument.ClientID %>");
                }

            </script>
        </telerik:RadScriptBlock>
    </form>
</body>
</html>
