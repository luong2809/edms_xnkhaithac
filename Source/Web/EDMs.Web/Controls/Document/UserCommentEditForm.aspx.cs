﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using EDMs.Data.Entities;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Configuration;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Web.Utilities.Sessions;
    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class UserCommentEditForm : Page
    {
        private readonly UserCommentService userCommentService;
        private readonly UserService userService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public UserCommentEditForm()
        {
            this.userCommentService = new UserCommentService();
            this.userService = new UserService();
        }

        /// <summary>
        /// Validation existing patient code
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        protected  void ValidatePatientCode(object source, ServerValidateEventArgs arguments)
        {
        }


        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
                {
                    var commentId = Convert.ToInt32(Request.QueryString["docId"]);
                    var commentObj = this.userCommentService.GetById(commentId);
                    if (commentObj != null)
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["isReply"]))
                        {
                            this.divContent.Visible = true;
                            this.lblComment.Text = commentObj.Comment;
                            this.txtComment.Text = commentObj.Reply;
                        }
                        else
                        {
                            this.divContent.Visible = false;
                            this.txtComment.Text = commentObj.Comment;
                        }
                    }
                }
            }
        }

        protected void SendMailMenu_OnButtonClick(object sender, RadToolBarEventArgs e)
        {
            ////if (this.IsValid)
            ////{
            ////    var smtpClient = new SmtpClient
            ////    {
            ////        DeliveryMethod = SmtpDeliveryMethod.Network,
            ////        UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
            ////        EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
            ////        Host = ConfigurationManager.AppSettings["Host"],
            ////        Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
            ////        Credentials = new NetworkCredential(ConfigurationManager.AppSettings["Email"], ConfigurationManager.AppSettings["EmailPass"])
            ////    };

            ////    var message = new MailMessage();
            ////    message.From = new MailAddress(ConfigurationManager.AppSettings["Email"], ConfigurationManager.AppSettings["EmailName"]);
            ////    message.Subject = this.txtSubject.Text.Trim();
            ////    message.BodyEncoding = new UTF8Encoding();
            ////    message.IsBodyHtml = true;
            ////    message.Body = this.txtEmailBody.Content;

            ////    if (!string.IsNullOrEmpty(this.ddlEmail.Text))
            ////    {
            ////        var toList = this.ddlEmail.Text.Split(';').Where(t => !string.IsNullOrEmpty(t));
            ////        var ccList = this.ddlEmailCC.Text.Split(';').Where(t => !string.IsNullOrEmpty(t));

            ////        foreach (var to in toList)
            ////        {
            ////            message.To.Add(new MailAddress(to));
            ////        }

            ////        foreach (var cc in ccList)
            ////        {
            ////            message.CC.Add(new MailAddress(cc));
            ////        }

            ////        smtpClient.Send(message);
            ////    }

            ////    this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "Close();", true);
            ////}
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var commentId = Convert.ToInt32(Request.QueryString["docId"]);
                var commentObj = this.userCommentService.GetById(commentId);
                if (commentObj != null)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["isReply"]))
                    {
                        commentObj.Reply = this.txtComment.Text;
                        commentObj.ReplyBy = UserSession.Current.User.Id;
                        commentObj.ReplyName = UserSession.Current.User.FullName;
                        commentObj.ReplyDate = DateTime.Now;
                        if (ConfigurationManager.AppSettings["EnableSendNotificationComment"] == "true")
                        {
                            this.SendNotificationReply(commentObj);
                        }
                    }
                    else
                    {
                        commentObj.Comment = this.txtComment.Text;
                        commentObj.UpdatedBy = UserSession.Current.User.Id;
                        commentObj.UpdatedDate = DateTime.Now;
                    }

                    this.userCommentService.Update(commentObj);

                    this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "Close();", true);
                }
            }
            else
            {
                var commentObj = new UserComment()
                {
                    UserId = UserSession.Current.User.Id,
                    UserName = UserSession.Current.User.FullName,
                    Comment = this.txtComment.Text,
                    IsReply = false,
                    CreatedBy = UserSession.Current.User.Id,
                    CreatedDate = DateTime.Now
                };

                this.userCommentService.Insert(commentObj);
                if (ConfigurationManager.AppSettings["EnableSendNotificationComment"] == "true")
                {
                    this.SendNotification(commentObj);
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "Close();", true);
            }
        }

        private void SendNotification(UserComment userComment)
        {
            var smtpClient = new SmtpClient
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                    EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                    Host = ConfigurationManager.AppSettings["Host"],
                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                    Credentials = new NetworkCredential(ConfigurationManager.AppSettings["Email"], ConfigurationManager.AppSettings["EmailPass"])
                };

            var message = new MailMessage();
            message.From = new MailAddress(ConfigurationManager.AppSettings["Email"], ConfigurationManager.AppSettings["EmailName"]);
            message.Subject = userComment.UserName + " had comment about the DocLib System";
            message.BodyEncoding = new UTF8Encoding();
            message.IsBodyHtml = true;
            message.Body = "*****************************************************************<br/>"
                + userComment.Comment
                + "<br/>*****************************************************************<br/>";


            //if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["EmailNotificationComment"]))
            //{
            //    var toList = ConfigurationManager.AppSettings["EmailNotificationComment"].Split(';').Where(t => !string.IsNullOrEmpty(t));
            //    foreach (var to in toList)
            //    {
            //        message.To.Add(new MailAddress(to));
            //    }

                message.To.Add(new MailAddress(ConfigurationManager.AppSettings["AdminEmail"]));
                
                smtpClient.Send(message);
            //}
        }

        private void SendNotificationReply(UserComment userComment)
        {
            var smtpClient = new SmtpClient
            {
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                Host = ConfigurationManager.AppSettings["Host"],
                Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                Credentials = new NetworkCredential(ConfigurationManager.AppSettings["Email"], ConfigurationManager.AppSettings["EmailPass"])
            };

            var message = new MailMessage();
            message.From = new MailAddress(ConfigurationManager.AppSettings["Email"], ConfigurationManager.AppSettings["EmailName"]);
            message.Subject = "Admin had reply you'e comment about the DocLib System";
            message.BodyEncoding = new UTF8Encoding();
            message.IsBodyHtml = true;
            message.Body = "******************************** Comment *********************************<br/>"
                + userComment.Comment
                + "<br/>*****************************************************************<br/>"
                + "<div><span class='Apple - tab - span' style='white - space: pre; '>	</span><b>Admin Reply</b></div>"
                   + "<br/>" + userComment.Comment
                ;

            var userCM = this.userService.GetByID(userComment.CreatedBy.GetValueOrDefault());
            if (!string.IsNullOrEmpty(userCM.Email))
            {
            message.To.Add(new MailAddress(userCM.Email));

            smtpClient.Send(message);
            }
        }
    }
}