﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RevisionHistory.aspx.cs" Inherits="EDMs.Web.Controls.Document.RevisionHistory" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Revision history</title>
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    <script type="text/javascript">
        
        function CloseAndRefreshGrid() {
            var oWin = GetRadWindow();
            var parentWindow = oWin.BrowserWindow;
            $(oWin).ready(function () {
                oWin.close();
            });
            parentWindow.refreshGrid();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including classic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }


    </script>
    <style type="text/css">
        .RadGrid .rgRow td, .RadGrid .rgAltRow td, .RadGrid .rgEditRow td, .RadGrid .rgFooter td, .RadGrid .rgFilterRow td, .RadGrid .rgHeader, .RadGrid .rgResizeCol, .RadGrid .rgGroupHeader td {
            padding-left: 1px !important;
            padding-right: 1px !important;
        }

        /*Hide change page size control*/
        div.RadGrid .rgPager .rgAdvPart     
        {     
        display:none;        
        }    
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="content">
            <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
            <telerik:RadGrid ID="grdDocument" runat="server" AllowPaging="True"
                AutoGenerateColumns="False" CellPadding="0" CellSpacing="0"
                 GridLines="None" Skin="Windows7" Height="550"
                OnDeleteCommand="grdDocument_DeleteCommand"
                OnUpdateCommand="grdDocument_UpdateCommand" 
                OnItemDataBound="grdDocument_ItemDataBound"
                OnItemCreated="grdDocument_ItemCreated"
                OnNeedDataSource="grdDocument_OnNeedDataSource" PageSize="50" Style="outline: none">
                <MasterTableView ClientDataKeyNames="ID" DataKeyNames="ID" Width="100%" EditMode="InPlace" Font-Size="8pt">
                    <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; Documents." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <Columns>
                        <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />

                        <%--<telerik:GridTemplateColumn>
                            <HeaderStyle Width="3%"  />
                            <ItemStyle HorizontalAlign="Center" Width="3%"  />
                            <ItemTemplate>
                                <asp:Image ID="EditLink" runat="server" ImageUrl="~/Images/edit.png" Style="cursor: pointer;" AlternateText="Edit properties" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>--%>
                        
                        <telerik:GridTemplateColumn AllowFiltering="false" UniqueName="IsLeaf">
                                    <HeaderStyle Width="2%" />
                                    <ItemStyle HorizontalAlign="Center" Width="2%" />
                                    <ItemTemplate>
                                        <asp:RadioButton ID="rbtnDefaultDoc" runat="server" GroupName="DefaultDoc" 
                                            onclick="MyClick(this,event)" 
                                            AutoPostBack="True" OnCheckedChanged="rbtnDefaultDoc_CheckedChanged" 
                                            Checked='<%# DataBinder.Eval(Container.DataItem, "IsLeaf") %>'/>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                        <telerik:GridEditCommandColumn UniqueName="EditColumn" ButtonType="ImageButton" EditImageUrl="~/Images/edit.png" UpdateImageUrl="~/Images/ok.png" CancelImageUrl="~/Images/delete.png" >
                            <HeaderStyle Width="4%"  />
                            <ItemStyle HorizontalAlign="Center" Width="4%"/>
                        </telerik:GridEditCommandColumn>

                        <telerik:GridButtonColumn CommandName="Delete" UniqueName="DeleteColumn" 
                            ConfirmText="Do you want to delete document?" ButtonType="ImageButton" ImageUrl="~/Images/delete.png">
                            <HeaderStyle Width="2%" />
                            <ItemStyle HorizontalAlign="Center" Width="2%"  />
                        </telerik:GridButtonColumn>

                        <telerik:GridTemplateColumn>
                            <HeaderStyle Width="2%" />
                            <ItemStyle HorizontalAlign="Center" Width="2%"/>
                            <ItemTemplate>
                                <a download='<%# DataBinder.Eval(Container.DataItem, "RevisionFileName") %>' 
                                    href='<%# this.Eval("IsLeaf").ToString().ToLower() == "true" ? "../../" + DataBinder.Eval(Container.DataItem, "FilePath") : DataBinder.Eval(Container.DataItem, "RevisionFilePath") %>' target="_blank">
                                    <asp:Image ID="CallLink" runat="server" ImageUrl='<%# "../../" +  DataBinder.Eval(Container.DataItem, "FileExtensionIcon") %>' 
                                        Style="cursor: pointer;" AlternateText="Download document" /> 
                                </a>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                       <%-- <telerik:GridBoundColumn DataField="IsLeaf" HeaderText="IsLeaf" UniqueName="IsLeaf">
                            <HeaderStyle HorizontalAlign="Center" Width="5%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>--%>

                        <telerik:GridTemplateColumn UniqueName="ReUpload">
                            <HeaderStyle Width="2%" />
                            <ItemStyle HorizontalAlign="Center" Width="2%"/>
                            <ItemTemplate>
                                <asp:Image ID="uploadLink" runat="server" ImageUrl="../../Images/upload.png" 
                                    Style="cursor: pointer;" AlternateText="Upload new document" /> 
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>

                        <%--3--%>
                        <telerik:GridTemplateColumn UniqueName="RevisionFileName" HeaderText="Name">
                            <HeaderStyle Width="10%" HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" Width="10%"/>
                            <ItemTemplate>
                                 <%# Eval("RevisionFileName") %> 
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="RevisionFileName" runat="server" Value='<%# Eval("RevisionFileName") %>'/>
                                                    
                                <asp:TextBox ID="txtRevisionFileName" runat="server" Width="100%"></asp:TextBox>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                                            
                        <telerik:GridTemplateColumn HeaderText="Document Number" UniqueName="DocumentNumber">
                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" Width="10%" />
                            <ItemTemplate>
                                <%# Eval("DocumentNumber") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="DocumentNumber" runat="server" Value='<%# Eval("DocumentNumber") %>'/>
                                                    
                                <asp:TextBox ID="txtDocumentNumber" runat="server" Width="100%"></asp:TextBox>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        
                        <telerik:GridTemplateColumn HeaderText="Title" UniqueName="Title">
                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" Width="10%"  />
                            <ItemTemplate>
                                <%# Eval("Title") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="Title" runat="server" Value='<%# Eval("Title") %>'/>
                                                    
                                <asp:TextBox ID="txtTitle" runat="server" Width="100%"></asp:TextBox>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        
                        <telerik:GridTemplateColumn HeaderText="Revision" UniqueName="Revision">
                            <HeaderStyle HorizontalAlign="Center" Width="5%" />
                            <ItemStyle HorizontalAlign="Center" Width="5%"/>
                            <ItemTemplate>
                                <%# Eval("Revision.Name") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="RevisionName" runat="server" Value='<%# Eval("RevisionName") %>'/>
                                                    
                                <%--<telerik:RadComboBox ID="ddlRevision" runat="server"  DropDownWidth="100px" MaxHeight="150px" Width="100%" />--%>
                                <asp:Label ID="txtRevisionName" runat="server" Width="100%"></asp:Label>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        
                        <%--<telerik:GridDropDownColumn DataField="RevisionID"
                            HeaderText="Category" ListTextField="Name" ListValueField="ID"
                            UniqueName="RevisionID" ColumnEditorID="ddlRevision">
                        </telerik:GridDropDownColumn>--%>
                        
                        <telerik:GridTemplateColumn HeaderText="Status" UniqueName="Status">
                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" Width="10%"/>
                            <ItemTemplate>
                                <%# Eval("Status.Name") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="StatusID" runat="server" Value='<%# Eval("StatusID") %>'/>
                                                    
                                <telerik:RadComboBox ID="ddlStatus" runat="server" Skin="Windows7" DropDownWidth="200px" MaxHeight="150px" Width="100%"/>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        
                        <telerik:GridTemplateColumn HeaderText="Discipline" UniqueName="Discipline">
                            <HeaderStyle HorizontalAlign="Center" Width="9%" />
                            <ItemStyle HorizontalAlign="Left" Width="9%"/>
                            <ItemTemplate>
                                <%# Eval("Discipline.Name") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="DisciplineID" runat="server" Value='<%# Eval("DisciplineID") %>'/>
                                                    
                                <telerik:RadComboBox ID="ddlDiscipline" runat="server" Skin="Windows7" DropDownWidth="200px" MaxHeight="150px" Width="100%"/>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridTemplateColumn HeaderText="Document Type" UniqueName="DocumentType">
                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" Width="10%"/>
                            <ItemTemplate>
                                <%# Eval("DocumentType.Name") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="DocumentTypeID" runat="server" Value='<%# Eval("DocumentTypeID") %>'/>
                                                    
                                <telerik:RadComboBox ID="ddlDocumentType" runat="server" Skin="Windows7" DropDownWidth="200px" MaxHeight="150px" Width="100%"/>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        
                        <telerik:GridTemplateColumn HeaderText="Expiry Date" UniqueName="ValidDate">
                            <HeaderStyle HorizontalAlign="Center" Width="6%" />
                            <ItemStyle HorizontalAlign="Center" Width="6%"/>
                            <ItemTemplate>
                                <%# Eval("ValidDate","{0:dd/MM/yyyy}") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="ValidDate" runat="server" Value='<%# Eval("ValidDate") %>'/>
                                <telerik:RadDatePicker ID="txtValidDate"  Width="100%" runat="server" Skin="Windows7" 
                                        ShowPopupOnFocus="True" PopupDirection="BottomRight">
                                    <DateInput ID="txtValidDateInput" runat="server" DateFormat="dd/MM/yyyy" ShowButton="False"/>
                                </telerik:RadDatePicker>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        
                        <telerik:GridTemplateColumn HeaderText="Received From" UniqueName="ReceivedFrom">
                            <HeaderStyle HorizontalAlign="Center" Width="6%" />
                            <ItemStyle HorizontalAlign="Center" Width="6%"/>
                            <ItemTemplate>
                                <%# Eval("ReceivedFrom.Name") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="ReceivedFromID" runat="server" Value='<%# Eval("ReceivedFromID") %>'/>

                                <telerik:RadComboBox ID="ddlReceivedFrom" runat="server" Skin="Windows7" DropDownWidth="200px" MaxHeight="150px" Width="100%"/>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        
                        <telerik:GridTemplateColumn HeaderText="Trans No." UniqueName="TransmittalNumber">
                            <HeaderStyle HorizontalAlign="Center" Width="8%" />
                            <ItemStyle HorizontalAlign="Center" Width="8%"/>
                            <ItemTemplate>
                                <%# Eval("TransmittalNumber") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="TransmittalNumber" runat="server" Value='<%# Eval("TransmittalNumber") %>'/>

                                <asp:TextBox ID="txtTransmittalNumber" runat="server" Width="100%"></asp:TextBox>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridTemplateColumn HeaderText="Received Date" UniqueName="ReceivedDate">
                            <HeaderStyle HorizontalAlign="Center" Width="6%" />
                            <ItemStyle HorizontalAlign="Center" Width="6%"/>
                            <ItemTemplate>
                                <%# Eval("ReceivedDate","{0:dd/MM/yyyy}") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="ReceivedDate" runat="server" Value='<%# Eval("ReceivedDate") %>'/>
                                <telerik:RadDatePicker ID="txtReceivedDate"  Width="100%" runat="server" Skin="Windows7" 
                                        ShowPopupOnFocus="True" PopupDirection="BottomRight">
                                                        
                                    <DateInput ID="txtReceivedDateInput" runat="server" DateFormat="dd/MM/yyyy" ShowButton="False"/>
                                </telerik:RadDatePicker>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                                            
                        <telerik:GridTemplateColumn HeaderText="Remark" UniqueName="Remark">
                            <HeaderStyle HorizontalAlign="Center" Width="6%" />
                            <ItemStyle HorizontalAlign="Left" Width="6%"/>
                            <ItemTemplate>
                                <%# Eval("Remark") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="Remark" runat="server" Value='<%# Eval("Remark") %>'/>
                                                    
                                <asp:TextBox ID="txtRemark" runat="server" Width="100%"></asp:TextBox>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                        
                        <telerik:GridTemplateColumn HeaderText="Well" UniqueName="Well" Display="False">
                            <HeaderStyle HorizontalAlign="Center" Width="6%" />
                            <ItemStyle HorizontalAlign="Left" Width="6%"/>
                            <ItemTemplate>
                                <%# Eval("Well") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="Well" runat="server" Value='<%# Eval("Well") %>'/>
                                                    
                                <asp:TextBox ID="txtWell" runat="server" Width="100%"></asp:TextBox>
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings Selecting-AllowRowSelect="true" AllowColumnHide="True">
                    <Resizing EnableRealTimeResize="True" ResizeGridOnColumnResize="True" ClipCellContentOnResize="false"></Resizing>
                    <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="350" UseStaticHeaders="True" />
                    <ClientEvents OnRowDblClick="RowDblClick"></ClientEvents>
                </ClientSettings>
            </telerik:RadGrid>
            
            <telerik:GridDropDownListColumnEditor ID="ddlRevision" runat="server"
                    DropDownStyle-Width="110px">
            </telerik:GridDropDownListColumnEditor>

        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
        <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="grdDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
            

            <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" >
        <Windows>
            <telerik:RadWindow ID="DocDialog" runat="server" Title="Reupload document file"
                VisibleStatusbar="false" Height="100" Width="420" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
        </div>
        <telerik:RadCodeBlock runat="server">
            <script type="text/javascript">
                
                function RowDblClick(sender, eventArgs) {
                    sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
                }

                var ajaxManager;

                function pageLoad() {
                ajaxManager = $find("<%=ajaxCustomer.ClientID %>");
            }


                function ShowUploadForm(id) {
                    var owd = $find("<%=DocDialog.ClientID %>");
                    owd.Show();
                    owd.setUrl("Upload.aspx?docId=" + id, "DocDialog");
                    // window.parent.radopen("Controls/Document/DocumentInfoEditForm.aspx?docId=" + id, "DocDialog");
                    // return false;
                }
                
                function ShowInsertForm() {
                    window.radopen("Controls/Customers/CustomerEditForm.aspx", "DocDialog");
                    return false;
                }
                
                function refreshGrid(arg) {
                    //alert(arg);
                    if (!arg) {
                        ajaxManager.ajaxRequest("Rebind");
                    }
                    else {
                        ajaxManager.ajaxRequest("RebindAndNavigate");
                    }
                }
                
                function MyClick(sender, eventArgs) {
                    var inputs = document.getElementById("<%= grdDocument.MasterTableView.ClientID %>").getElementsByTagName("input");
                    for (var i = 0, l = inputs.length; i < l; i++) {
                        var input = inputs[i];
                        if (input.type != "radio" || input == sender)
                            continue;
                        input.checked = false;
                    }
                }
                
                function SelectMeOnly(objRadioButton, grdName) {

                    var i, obj;
                    for (i = 0; i < document.all.length; i++) {
                        obj = document.all(i);

                        if (obj.type == "radio") {

                            if (objRadioButton.id.substr(0, grdName.length) == grdName)
                                if (objRadioButton.id == obj.id)
                                    obj.checked = true;
                                else
                                    obj.checked = false;
                        }
                    }
                }
            </script>
        </telerik:RadCodeBlock>
    </form>
</body>
</html>
