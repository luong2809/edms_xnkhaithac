﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    using OfficeHelper.Utilities.Data;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class TransmittalAttachDocument : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService;

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly TransmittalService transmittalService;
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly FolderService folderService = new FolderService();
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The category service.
        /// </summary>
        private readonly CategoryService categoryService;

        /// <summary>
        /// The group data permission service.
        /// </summary>
        private readonly GroupDataPermissionService groupDataPermissionService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        private readonly UserDataPermissionService userDataPermissionService = new UserDataPermissionService();

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string unreadPattern = @"\(\d+\)";

        private List<int> AdminGroup
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("GroupAdminList").Split(',').Select(t => Convert.ToInt32(t)).ToList();
            }
        }

        private int CategoryID
        {
            get
            {
                if (!string.IsNullOrEmpty(this.ddlCategory.SelectedValue))
                {
                    return Convert.ToInt32(this.ddlCategory.SelectedValue);
                }

                return 0;
            }
        }

        private string Name
        {
            get
            {
                return this.txtName.Text.Trim();
            }
        }

        private string DocTitle
        {
            get
            {
                return this.txtTitle.Text.Trim();
            }
        }

        private string DocumentNumber
        {
            get { return this.txtDocumentNumber.Text.Trim(); }
        }

        private int RevisionID
        {
            get { return Convert.ToInt32(this.ddlRevision.SelectedValue); }
        }

        //private int ReceiveFromID
        //{
        //    get { return Convert.ToInt32(this.ddlReceivedFrom.SelectedValue); }
        //}


        //private DateTime? DateFrom
        //{
        //    get { return this.txtDateFrom.SelectedDate; }
        //}

        //private DateTime? DateTo
        //{
        //    get { return this.txtDateTo.SelectedDate; }
        //}

        /// <summary>
        /// Initializes a new instance of the <see cref="TransmittalAttachDocument"/> class.
        /// </summary>
        public TransmittalAttachDocument()
        {
            this.revisionService = new RevisionService();
            this.documentService = new DocumentService();
            this.receivedFromService = new ReceivedFromService();
            this.transmittalService = new TransmittalService();
            this.categoryService = new CategoryService();
            this.userService = new UserService();
            this.groupDataPermissionService = new GroupDataPermissionService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                this.LoadComboData();
            }
        }

        /// <summary>
        /// Load all document by folder
        /// </summary>
        /// <param name="isbind">
        /// The isbind.
        /// </param>
        protected void LoadDocuments(int pageSize, int startingRecordNumber, bool isbind = false)
        {
            
            if (isbind)
            {
                this.grdDocument.DataBind();
            }
        }
        
        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.MasterTableView.CurrentPageIndex = this.grdDocument.MasterTableView.PageCount - 1;
                this.grdDocument.Rebind();
            }
        }
        
        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (this.ddlCategory.SelectedItem != null)
            {
                var pageSize = this.grdDocument.PageSize;
                var currentPage = this.grdDocument.CurrentPageIndex;
                var startingRecordNumber = currentPage * pageSize;
                var categoryId = Convert.ToInt32(this.ddlCategory.SelectedValue);
                var name = this.txtName.Text.Trim();
                var title = this.txtTitle.Text.Trim();
                var docNumber = this.txtDocumentNumber.Text.Trim();
                var keyword = string.Empty;
                var revisionId = Convert.ToInt32(this.ddlRevision.SelectedValue);
                var docTypeId = 0;
                var statusId = 0;
                var folderPermission = new List<int>();
               // var receiveFromId = Convert.ToInt32(this.ddlReceivedFrom.SelectedValue);
                var rtvOptionalTypeDetail = (RadTreeView)this.ddlParent.Items[0].FindControl("rtvOptionalTypeDetail");
                if (rtvOptionalTypeDetail != null && rtvOptionalTypeDetail.SelectedNode != null)
                {
                   // var nodeselect = Convert.ToInt32(rtvOptionalTypeDetail.SelectedNode.Value);
                    folderPermission =
                       rtvOptionalTypeDetail.SelectedNode.GetAllNodes().Select(t => Convert.ToInt32(t.Value)).ToList();
                    folderPermission.Insert(0, Convert.ToInt32(rtvOptionalTypeDetail.SelectedNode.Value));
                }
                var disciplineId = 0;
                var languageId = 0;
               // var dateFrom = this.txtDateFrom.SelectedDate;
               // var dateTo = this.txtDateTo.SelectedDate;

               
                //if (!this.AdminGroup.Contains(UserSession.Current.RoleId))
                //{
                //    folderPermission = this.groupDataPermissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault())
                //    .Where(t => t.CategoryIdList == categoryId.ToString()
                //            && !string.IsNullOrEmpty(t.FolderIdList))
                //    .Select(t => Convert.ToInt32(t.FolderIdList))
                //    .OrderBy(t => t).ToList();

                //    folderPermission = folderPermission
                //                    .Union(this.userDataPermissionService.GetByUserId(UserSession.Current.User.Id)
                //                            .Where(t => t.CategoryId == categoryId)
                //                            .Select(t => t.FolderId.GetValueOrDefault()))
                //                            .Distinct().ToList();
                //}

                //var lisDoc = this.documentService.SearchDocumentV1(
                //    categoryId,
                //    new List<int>(),
                //    name,
                //    title,
                //    docNumber,
                //    keyword,
                //    revisionId,
                //    docTypeId,
                //    statusId,
                //    disciplineId,
                //    languageId,
                //    string.Empty,
                //    pageSize,
                //    startingRecordNumber,
                //    string.Empty,
                //    string.Empty,
                //    folderPermission, new List<int>(), null ,null);

                //this.grdDocument.VirtualItemCount = this.documentService.GetItemCountV1(
                //    this.CategoryID,
                //    new List<int>(),
                //    this.Name,
                //    this.DocTitle,
                //    this.DocumentNumber,
                //    string.Empty,
                //    this.RevisionID,
                //    0,
                //    0,
                //    0,
                //    0,
                //    string.Empty,
                //    string.Empty,
                //    string.Empty,
                //    folderPermission, new List<int>(), null, null);
                
                this.grdDocument.DataSource = new List<Document>();
            }
        }

        /// <summary>
        /// The rad menu_ item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void radMenu_ItemClick(object sender, RadMenuEventArgs e)
        {
            throw new NotImplementedException();
        }
        protected void rtvOptionalTypeDetail_OnNodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = "~/Images/folder16.png";
        }
        /// <summary>
        /// The btn search_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.ddlCategory.SelectedItem != null)
            {
                this.grdDocument.Rebind();
            }
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {
            var categoryPermission =  this.groupDataPermissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault()).Select(t => t.CategoryIdList).ToList();
            var listCategory =UserSession.Current.User.IsAdmin.GetValueOrDefault()? this.categoryService.GetAll(): this.categoryService.GetAll().Where(t => categoryPermission.Any(x => x == t.ID.ToString())).OrderBy(t => t.Name).ToList();
            this.ddlCategory.DataSource = listCategory;
            this.ddlCategory.DataValueField = "ID";
            this.ddlCategory.DataTextField = "Name";
            this.ddlCategory.DataBind();

            var revisionList = this.revisionService.GetAll();
            revisionList.Insert(0, new Revision() { Name = string.Empty });
            this.ddlRevision.DataSource = revisionList;
            this.ddlRevision.DataValueField = "ID";
            this.ddlRevision.DataTextField = "Name";
            this.ddlRevision.DataBind();

            //var receivedFromList = this.receivedFromService.GetAll().OrderBy(t => t.Name).ToList();
            //receivedFromList.Insert(0, new ReceivedFrom() { Name = string.Empty });
            //this.ddlReceivedFrom.DataSource = receivedFromList;
            //this.ddlReceivedFrom.DataValueField = "ID";
            //this.ddlReceivedFrom.DataTextField = "Name";
            //this.ddlReceivedFrom.DataBind();
                var categoryId = Convert.ToInt32(this.ddlCategory.SelectedValue);
                var listOptionalTypeDetail = this.folderService.GetAllByCategory(categoryId);
                var rtvOptionalTypeDetail = (RadTreeView)this.ddlParent.Items[0].FindControl("rtvOptionalTypeDetail");
                if (rtvOptionalTypeDetail != null)
                {
                    rtvOptionalTypeDetail.DataSource = listOptionalTypeDetail;
                    rtvOptionalTypeDetail.DataFieldParentID = "ParentId";
                    rtvOptionalTypeDetail.DataTextField = "Name";
                    rtvOptionalTypeDetail.DataValueField = "ID";
                    rtvOptionalTypeDetail.DataFieldID = "ID";
                    rtvOptionalTypeDetail.DataBind();

                   //this.rtvtemplate.DataSource = listOptionalTypeDetail;
                   //this.rtvtemplate.DataFieldParentID = "ParentId";
                   //this.rtvtemplate.DataTextField = "Name";
                   //this.rtvtemplate.DataValueField = "ID";
                   //this.rtvtemplate.DataFieldID = "ID";
                   //this.rtvtemplate.DataBind();
                }
        }

        /// <summary>
        /// The btn save_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var tranId = Convert.ToInt32(Request.QueryString["tranId"]);
            var tranObj = this.transmittalService.GetById(tranId);
            var userObj = this.userService.GetByID(tranObj.CreatedBy.Value);
            var listTran = new List<Transmittal> { tranObj };
            var transInfo = Utility.ConvertToDataTable(listTran);

            transInfo.Columns.AddRange(
                new[]
                    {
                        new DataColumn("CreatedByName", Type.GetType("System.String")),
                        new DataColumn("ReceivedDateShort", Type.GetType("System.String"))
                    });

            transInfo.Rows[0]["CreatedByName"] =userObj!= null? userObj.FullName :"";
            if (!string.IsNullOrEmpty(transInfo.Rows[0]["ReceivedDate"].ToString()))
            {
                transInfo.Rows[0]["ReceivedDateShort"] =
                Convert.ToDateTime(transInfo.Rows[0]["ReceivedDate"]).ToString("dd/MM/yyyy");
            }

            var docDt = new DataTable();
            var ds = new DataSet();
            var listColumn = new DataColumn[]
                {
                    new DataColumn("Index", Type.GetType("System.String")),
                    new DataColumn("DocumentNumber", Type.GetType("System.String")),
                    new DataColumn("RevisionName", Type.GetType("System.String")),
                    new DataColumn("Description", Type.GetType("System.String")),
                    new DataColumn("DocumentType", Type.GetType("System.String")),
                    //new DataColumn("Remark", Type.GetType("System.String"))
                };

            docDt.Columns.AddRange(listColumn);
            var count = 0;
            var docIdList = string.Empty;
            foreach (GridDataItem item in this.grdDocument.MasterTableView.Items)
            {
                var cboxSelected = (CheckBox)item["IsSelected"].FindControl("cboxSelectDocTransmittal");
                if (cboxSelected.Checked)
                {
                    var dataItem = docDt.NewRow();
                    count += 1;
                    docIdList += item.GetDataKeyValue("ID") + ";";
                    dataItem["Index"] = count;
                    dataItem["DocumentNumber"] = item["DocumentNumber"].Text != @"&nbsp;"
                                                     ? item["DocumentNumber"].Text
                                                     : string.Empty;
                    dataItem["RevisionName"] = item["RevisionName"].Text != @"&nbsp;"
                                                   ? item["RevisionName"].Text
                                                   : string.Empty;
                    dataItem["Description"] = item["Title"].Text != @"&nbsp;"
                                                  ? item["Title"].Text
                                                  : string.Empty;
                    dataItem["DocumentType"] = item["DocumentType"].Text != @"&nbsp;"
                                                  ? item["DocumentType"].Text
                                                  : string.Empty;
                  //  dataItem["Remark"] = item["Remark"].Text != @"&nbsp;" ? item["Remark"].Text : string.Empty;

                    docDt.Rows.Add(dataItem);
                }
            }

            ds.Tables.Add(docDt);
            ds.Tables[0].TableName = "Table";

            var rootPath = Server.MapPath("../../DocumentResource/TransmittalGenerate/");
            const string WordPath = @"Template\";
            const string WordPathExport = @"Generated\";
            const string StrTemplateFileName = "TransmittalTemplate.doc";
            var strOutputFileName = tranObj.TransmittalNumber + "_" + DateTime.Now.ToBinary() + ".doc";
            var isSuccess = OfficeCommon.ExportToWordWithRegion(
                rootPath, WordPath, WordPathExport, StrTemplateFileName, strOutputFileName, transInfo, ds);

            if (isSuccess)
            {
                var serverPath = "DocumentResource/TransmittalGenerate/Generated/";
                tranObj.GeneratePath = serverPath + strOutputFileName;
                tranObj.IsGenerate = true;
                tranObj.DocList = docIdList.Substring(0, docIdList.Length - 1);
                this.transmittalService.Update(tranObj);
            }

            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            var categoryId = Convert.ToInt32(this.ddlCategory.SelectedValue);
            var listOptionalTypeDetail = this.folderService.GetAllByCategory(categoryId);
            var rtvOptionalTypeDetail = (RadTreeView)this.ddlParent.Items[0].FindControl("rtvOptionalTypeDetail");
            if (rtvOptionalTypeDetail != null)
            {
                rtvOptionalTypeDetail.DataSource = listOptionalTypeDetail;
                rtvOptionalTypeDetail.DataFieldParentID = "ParentId";
                rtvOptionalTypeDetail.DataTextField = "Name";
                rtvOptionalTypeDetail.DataValueField = "ID";
                rtvOptionalTypeDetail.DataFieldID = "ID";
                rtvOptionalTypeDetail.DataBind();

                //this.rtvtemplate.DataSource = listOptionalTypeDetail;
                //this.rtvtemplate.DataFieldParentID = "ParentId";
                //this.rtvtemplate.DataTextField = "Name";
                //this.rtvtemplate.DataValueField = "ID";
                //this.rtvtemplate.DataFieldID = "ID";
                //this.rtvtemplate.DataBind();
            }
        }
    }
}