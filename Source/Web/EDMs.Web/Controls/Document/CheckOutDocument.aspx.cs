﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class CheckOutDocument : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly DCRService dcrService;

        private readonly CheckOutInHistoryService checkOutInHistoryService;

        protected const string ServiceName = "EDMSFolderWatcher";

        private List<int> AdminGroup
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("GroupAdminList").Split(',').Select(t => Convert.ToInt32(t)).ToList();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public CheckOutDocument()
        {
            this.documentService = new DocumentService();
            this.dcrService = new DCRService();
            this.checkOutInHistoryService = new CheckOutInHistoryService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!UserSession.Current.User.Role.Active.GetValueOrDefault())
                {
                    this.btnSave.Visible = false;
                }

                var listDCR = this.AdminGroup.Contains(UserSession.Current.RoleId)
                    ? this.dcrService.GetAll()
                    : this.dcrService.GetAllByOwner(UserSession.Current.User.Id);

                this.ddlDCR.DataSource = listDCR;
                this.ddlDCR.DataTextField = "DCRNumber";
                this.ddlDCR.DataValueField = "ID";
                this.ddlDCR.DataBind();
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
                {
                    var docIds = Request.QueryString["docId"].Split(',').Where(t => !string.IsNullOrEmpty(t));
                    foreach (var docId in docIds)
                    {
                        var docObj = this.documentService.GetById(Convert.ToInt32(docId));
                        if (docObj != null && !docObj.IsCheckOut)
                        {
                            docObj.IsCheckOut = true;
                            docObj.CurrentCheckoutByRole = UserSession.Current.RoleId;
                            docObj.CurrentCheckoutByUser = UserSession.Current.User.Id;
                            docObj.CurrentDCRId = Convert.ToInt32(this.ddlDCR.SelectedValue);
                            docObj.CurrentDCRNumber = this.ddlDCR.SelectedItem.Text;
                            var checkoutHistory = new CheckOutInHistory()
                            {
                                ActionName = "Checkout",
                                ByUserId = UserSession.Current.User.Id,
                                ByRoleId = UserSession.Current.RoleId,
                                AtTime = DateTime.Now,
                                DocumentId = docObj.ID,
                                DCRId = Convert.ToInt32(this.ddlDCR.SelectedValue)
                            };

                            this.documentService.Update(docObj);
                            this.checkOutInHistoryService.Insert(checkoutHistory);
                        }
                    }
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// The rad ajax manager 1_ ajax request.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.Contains("CheckFileName"))
            {
            }
        }
    }
}