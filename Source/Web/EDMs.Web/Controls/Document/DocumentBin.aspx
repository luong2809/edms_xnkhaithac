﻿<%@ Page Title="EDMS - Nhiệt điện Mông Dương" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="DocumentBin.aspx.cs" Inherits="EDMs.Web.Controls.Document.DocumentBin" EnableViewState="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        /*Hide change page size control*/
        div.RadGrid .rgPager .rgAdvPart {
            display: none;
        }

        a.tooltip {
            outline: none;
            text-decoration: none;
        }

            a.tooltip strong {
                line-height: 30px;
            }

            a.tooltip:hover {
                text-decoration: none;
            }

            a.tooltip span {
                z-index: 10;
                display: none;
                padding: 14px 20px;
                margin-top: -30px;
                margin-left: 5px;
                width: 240px;
                line-height: 16px;
            }

            a.tooltip:hover span {
                display: inline;
                position: absolute;
                color: #111;
                border: 1px solid #DCA;
                background: #fffAF0;
            }

        .callout {
            z-index: 20;
            position: absolute;
            top: 30px;
            border: 0;
            left: -12px;
        }

        /*CSS3 extras*/
        a.tooltip span {
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            -moz-box-shadow: 5px 5px 8px #CCC;
            -webkit-box-shadow: 5px 5px 8px #CCC;
            box-shadow: 5px 5px 8px #CCC;
        }

        .rgMasterTable {
            table-layout: auto;
        }

        #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_grdDocumentPanel, #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_divContainerPanel {
            height: 100% !important;
        }

        #ctl00_ContentPlaceHolder2_RadPageView1, #ctl00_ContentPlaceHolder2_RadPageView2,
        #ctl00_ContentPlaceHolder2_RadPageView3, #ctl00_ContentPlaceHolder2_RadPageView4,
        #ctl00_ContentPlaceHolder2_RadPageView5 {
            height: 100% !important;
        }

        #divContainerLeft {
            width: 25%;
            float: left;
            margin: 5px;
            height: 99%;
            border-right: 1px dotted green;
            padding-right: 5px;
        }

        #divContainerRight {
            width: 100%;
            float: right;
            margin-top: 5px;
            height: 99%;
        }

        .dotted {
            border: 1px dotted #000;
            border-style: none none dotted;
            color: #fff;
            background-color: #fff;
        }

        .exampleWrapper {
            width: 100%;
            height: 100%;
            /*background: transparent url(images/background.png) no-repeat top left;*/
            position: relative;
        }

        .tabStrip {
            position: absolute;
            top: 0px;
            left: 0px;
        }

        .multiPage {
            position: absolute;
            top: 30px;
            left: 0px;
            color: white;
            width: 100%;
            height: 100%;
        }

        /*Fix RadMenu and RadWindow z-index issue*/
        .radwindow {
            z-index: 8000 !important;
        }

        .TemplateMenu {
            z-index: 10;
        }
    </style>
    <telerik:RadPanelBar ID="radPbSearch" runat="server" Width="100%" Visible="false">
        <Items>
            <telerik:RadPanelItem Text="ADVANCE SEARCH" runat="server" Expanded="True" Width="100%" Visible="false">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Advance Search" ImageUrl="Images/search.gif" NavigateUrl="Search.aspx" />
                </Items>
            </telerik:RadPanelItem>
        </Items>
    </telerik:RadPanelBar>
    <telerik:RadPanelBar ID="radPbCategories" runat="server" OnClientItemClicking="radPbCategories_OnClientItemClicking" Width="100%"></telerik:RadPanelBar>

    <telerik:RadPanelBar ID="RadPanelBar1" runat="server" Width="100%">
        <Items>
            <telerik:RadPanelItem Text="TRANSMITTAL" runat="server" Expanded="True" Width="100%" Visible="false">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Transmittal" ImageUrl="Images/Transmittal.png" NavigateUrl="Transmittal.aspx" />
                </Items>
            </telerik:RadPanelItem>
        </Items>
    </telerik:RadPanelBar>

    <telerik:RadPanelBar ID="radPbList" runat="server" Width="100%" />
    <telerik:RadPanelBar ID="radPbSystem" runat="server" Width="100%" />

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
                        <telerik:RadGrid ID="grdDocument"
                            runat="server" AllowPaging="True" AutoGenerateColumns="False" CellSpacing="0" CellPadding="0"
                            PageSize="100" Height="100%" GridLines="None"
                            AllowFilteringByColumn="True"
                            OnNeedDataSource="grdRoles_OnNeedDataSource"
                            OnItemCommand="grdRoles_ItemCommand"
                            OnItemDataBound="grdRoles_OnItemDataBound">
                            <GroupingSettings CaseSensitive="False"></GroupingSettings>
                            <MasterTableView DataKeyNames="Id" ClientDataKeyNames="Id" CommandItemDisplay="Top" Width="100%">
                                <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; Groups." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                                <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Refresh Data" ShowExportToExcelButton="false" />
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Id" UniqueName="Id" Display="False" />
                                    <telerik:GridBoundColumn DataField="IsFolder" UniqueName="IsFolder" Display="False" />
                                    <telerik:GridTemplateColumn AllowFiltering="False" Visible="false">
                                        <HeaderStyle Width="30" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle Width="30" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton runat="server" Style="padding-left: 7px; cursor: pointer;" ID="btnRestore" ImageUrl="~/Images/ok.png" CommandName="RestoreCommand"
                                                Visible='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "Id")) != 1%>'
                                                OnClientClick="return confirm('Do you want to restore?');" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn AllowFiltering="False">
                                        <HeaderStyle Width="30" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle Width="30" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton runat="server" Style="padding-left: 7px; cursor: pointer;" ID="btnDelete" ImageUrl="~/Images/delete.png" CommandName="DeleteCommand"
                                                Visible='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "Id")) != 1%>'
                                                OnClientClick="return confirm('Do you want to delete?');" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn AllowFiltering="false" UniqueName="DownloadColumn">
                                        <HeaderStyle Width="30" />
                                        <ItemStyle HorizontalAlign="Center" Width="30" />
                                        <ItemTemplate>
                                            <div runat="server" id="DocumentFile" visible='<%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon")!=null %>'>
                                                <asp:Image ID="CallLink" runat="server"
                                                    ImageUrl=' <%#"~/" + DataBinder.Eval(Container.DataItem, "FileExtensionIcon") %>'
                                                    Style="cursor: pointer;" AlternateText="Download document" />
                                            </div>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="Name" HeaderText="Name" UniqueName="Name"
                                        ShowFilterIcon="False" FilterControlWidth="97%" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                        <HeaderStyle Width="30%" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle Width="30%" HorizontalAlign="Left"></ItemStyle>
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn DataField="Title" HeaderText="Title" UniqueName="Title" AllowFiltering="False">
                                        <HeaderStyle Width="27%" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle Width="27%" HorizontalAlign="Left"></ItemStyle>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="LastUpdatedDate" HeaderText="Date" UniqueName="LastUpdatedDate" AllowFiltering="False">
                                        <HeaderStyle Width="27%" HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle Width="27%" HorizontalAlign="Left"></ItemStyle>
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <CommandItemStyle Height="25px"></CommandItemStyle>
                            </MasterTableView>
                            <ClientSettings>
                                <Selecting AllowRowSelect="true"></Selecting>
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" ScrollHeight="500"></Scrolling>
                            </ClientSettings>
                        </telerik:RadGrid>

    <span style="display: none">
        <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="grdDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
    </span>

    <asp:HiddenField runat="server" ID="lblCategoryId"/>

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script src="../../Scripts/jquery-1.7.1.js"></script>
        <script type="text/javascript">

            function refreshGrid() {
                alert("1");
                var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView();
                masterTable.rebind();
            }

            function onColumnHidden(sender) {

                var masterTableView = sender.get_masterTableView().get_element();
                masterTableView.style.tableLayout = "auto";
                //window.setTimeout(function () { masterTableView.style.tableLayout = "auto"; }, 0);
            }

        </script>
        <script type="text/javascript">
            /* <![CDATA[ */
            var ajaxManager;

            function pageLoad() {
                $('iframe').load(function () { //The function below executes once the iframe has finished loading<---true dat, althoug Is coppypasta from I don't know where
                    //alert($('iframe').contents());
                });

                ajaxManager = $find("<%=ajaxCustomer.ClientID %>");

                $telerik.$(".searchtextbox")
                    .bind("keypress", function (e) {
                        searchButton.set_imageUrl("images/search.gif");
                        searchButton.set_value("search");
                    });
            }

            function radPbCategories_OnClientItemClicking(sender, args)
            {
                var item = args.get_item();
                var categoryId = item.get_value();
                document.getElementById("<%= lblCategoryId.ClientID %>").value = categoryId;
                
            }

            function refreshGrid(arg) {
                alert("1");
                if (!arg) {
                    ajaxManager.ajaxRequest("Rebind");
                }
                else {
                    ajaxManager.ajaxRequest("RebindAndNavigate");
                }
            }

        </script>
    </telerik:RadCodeBlock>
</asp:Content>
