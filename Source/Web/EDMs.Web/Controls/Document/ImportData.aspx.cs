﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Text.RegularExpressions;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.ServiceProcess;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Aspose.Cells;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    using ZetaLongPaths;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportData : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;


        private readonly FolderService folderService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ImportData()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.disciplineService = new DisciplineService();
            this.documentService = new DocumentService();
            this.folderService = new FolderService();
        }

        /// <summary>
        /// Validation existing patient code
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        protected void ValidatePatientCode(object source, ServerValidateEventArgs arguments)
        {
        }


        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {

                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentFileName = string.Empty;
            foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
            {
                var extension = docFile.GetExtension();
                if (extension == ".xls" || extension == ".xlsx")
                {
                    var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") +
                                     "_" + docFile.FileName;
                    docFile.SaveAs(importPath);

                    // Instantiate a new workbook
                    var workbook = new Workbook();
                    workbook.Open(importPath);
                    var wsData = workbook.Worksheets[0];
                    // Create a datatable
                    var totalDataTable = new DataTable();
                    var dataTable = new DataTable();

                    // var insertNewDoc = new DataTable();
                    // Export worksheet data to a DataTable object by calling either ExportDataTable or ExportDataTableAsString method of the Cells class	
                    totalDataTable = wsData.Cells.ExportDataTableAsString(4, 0, wsData.Cells.MaxRow, 15);
                    var haveEdit = totalDataTable.AsEnumerable().Any(t => t["Column1"].ToString().ToLower() == "1" || t["Column1"].ToString().ToLower() == "true");
                    dataTable = haveEdit
                        ? totalDataTable.AsEnumerable()
                            .Where(t => t["Column1"].ToString().ToLower() == "1" || t["Column1"].ToString().ToLower() == "true")
                            .CopyToDataTable()
                        : new DataTable();
                    var strError = "Have error: </br>";
                    foreach (DataRow dataRow in dataTable.Rows)
                    {
                        if (!string.IsNullOrEmpty(dataRow["Column2"].ToString()) && dataRow["Column2"].ToString() != "-1")
                        {
                            currentFileName = dataRow["Column4"].ToString();
                            var documentId = Convert.ToInt32(dataRow["Column2"].ToString());
                            var documentObj = this.documentService.GetById(documentId);
                            if (documentObj != null)
                            {
                                documentObj.DocumentNumber = dataRow["Column5"].ToString();
                                documentObj.Title = dataRow["Column6"].ToString();
                                documentObj.RevisionName = dataRow["Column7"].ToString();
                                var strDate = dataRow["Column8"].ToString();
                                if (!string.IsNullOrEmpty(strDate))
                                {
                                    var date = new DateTime();
                                    if (Utility.ConvertStringToDateTimeddMMyyyy(strDate, ref date))
                                    {
                                        documentObj.Date = date;
                                    }
                                    else
                                    {
                                        strError += "Doc: <b>" + currentFileName + "</b> have invalid 'Date' value.";
                                    }
                                }
                                else
                                {
                                    documentObj.Date = null;
                                }
                                documentObj.FromTo = dataRow["Column9"].ToString();
                                documentObj.DisciplineName = dataRow["Column10"].ToString();
                                documentObj.DocumentTypeName = dataRow["Column11"].ToString();
                                documentObj.Platform = dataRow["Column12"].ToString();
                                documentObj.Remark = dataRow["Column13"].ToString();
                                documentObj.Contractor = dataRow["Column14"].ToString();
                                documentObj.Tag = dataRow["Column15"].ToString();
                                documentObj.LastUpdatedBy = UserSession.Current.User.Id;
                                documentObj.LastUpdatedDate = DateTime.Now;
                                this.documentService.Update(documentObj);
                            }
                        }
                    }
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The save upload file.
        /// </summary>
        /// <param name="uploadDocControl">
        /// The upload doc control.
        /// </param>
        /// <param name="objDoc">
        /// The obj Doc.
        /// </param>
        private void SaveUploadFile(RadAsyncUpload uploadDocControl, ref Document objDoc)
        {
            var listUpload = uploadDocControl.UploadedFiles;
            if (listUpload.Count > 0)
            {
                foreach (UploadedFile docFile in listUpload)
                {
                    var revisionFilePath = Server.MapPath(objDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                    docFile.SaveAs(revisionFilePath, true);
                }
            }
        }
    }
}