﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Configuration;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.ServiceProcess;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    using ZetaLongPaths;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class MoveAndFolder : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
       // private readonly GroupDataPermissionService groupDataPermissionService;

        private readonly FolderService folderService;

        private readonly DocumentService documentService;
        private readonly GroupFunctionService _GroupfunctionService = new GroupFunctionService();
        private readonly FolderPermissionService _FolderPermisstionService = new FolderPermissionService();

        protected const string ServiceName = "EDMSFolderWatcher";

        private List<int> GroupFunctionList
        {
            get
            {
                return this._GroupfunctionService.GetByRoleId(UserSession.Current.User.Id).Select(t => t.ID).ToList();
            }
        }
        private List<int> AdminGroup
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("GroupAdminList").Split(',').Select(t => Convert.ToInt32(t)).ToList();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MoveAndFolder"/> class.
        /// </summary>
        public MoveAndFolder()
        {
            //  this.groupDataPermissionService = new GroupDataPermissionService();
            this.folderService = new FolderService();
            this.documentService = new DocumentService();
            //  this.userDataPermissionService = new UserDataPermissionService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {


                if (!string.IsNullOrEmpty(Request.QueryString["folderId"]))
                {
                    this.lblFolderDirName.Text = "<font style='text-decoration: underline;'>Move folder</font>: ";
                    var folderNeedToMove = this.folderService.GetById(Convert.ToInt32(Request.QueryString["folderId"]));
                    if (folderNeedToMove != null)
                    {
                        this.lblFolderDirName.Text += folderNeedToMove.Name;

                        //var folderPermission = this._FolderPermisstionService.GetFolderByRoleIdList(Convert.ToInt32(Request.QueryString["doctype"]), GroupFunctionList)
                        //                            .Where(t => t.Folder_CreateSubFolder.GetValueOrDefault() || t.Folder_IsFullPermission.GetValueOrDefault()).Select(t => t.FolderId.GetValueOrDefault()).ToList();
                        //     folderPermission = folderPermission.Union(this._FolderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"]))
                        //                                                     .Where(t => t.Folder_CreateSubFolder.GetValueOrDefault() || t.Folder_IsFullPermission.GetValueOrDefault()).Select(t => t.FolderId.GetValueOrDefault())).Distinct().ToList();
                        var folderPermission = this._FolderPermisstionService.GetByRoleId(Convert.ToInt32(Request.QueryString["categoryId"]), UserSession.Current.User.RoleId.GetValueOrDefault())
                                               .Where(t => t.FolderId != null && (t.Folder_CreateSubFolder.GetValueOrDefault() || t.Folder_IsFullPermission.GetValueOrDefault()))
                                               .Select(t => t.FolderId.GetValueOrDefault()).ToList();
                        folderPermission = folderPermission.Union(this._FolderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["categoryId"]))
                                                                         .Where(t => t.FolderId != null && (t.Folder_CreateSubFolder.GetValueOrDefault() || t.Folder_IsFullPermission.GetValueOrDefault()))
                                                                         .Select(t => t.FolderId.GetValueOrDefault()))
                                                                        .Distinct().ToList();

                        var listFolder = this.AdminGroup.Contains(UserSession.Current.RoleId)
                                            ? this.folderService.GetAllByCategory(folderNeedToMove.CategoryID.GetValueOrDefault())
                                            : this.folderService.GetSpecificFolderStatic(folderPermission);

                        var rtvFolder = (RadTreeView)this.ddlFolder.Items[0].FindControl("rtvFolder");
                        if (rtvFolder != null)
                        {
                            rtvFolder.DataSource = listFolder;
                            rtvFolder.DataFieldParentID = "ParentId";
                            rtvFolder.DataTextField = "Name";
                            rtvFolder.DataValueField = "ID";
                            rtvFolder.DataFieldID = "ID";
                            rtvFolder.DataBind();

                            this.RestoreExpandStateTreeView("expandedNodesrtvFolder", rtvFolder);
                            Utility.SortNodes(rtvFolder.Nodes);
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["docIds"]))
                {
                    var listDocId = Request.QueryString["docIds"].Split(',').Where(t => !string.IsNullOrEmpty(t)).Select(t => Convert.ToInt32(t)).ToList();

                    var folderId = Convert.ToInt32(Request.QueryString["selectedFolder"]);
                    var categoryId = Request.QueryString["categoryId"];
                    this.lblFolderDirName.Text = "<font style='text-decoration: underline;'>Move files</font>: <br/>";

                    var listDoc = this.documentService.GetSpecificDocument(listDocId);
                    foreach (var document in listDoc)
                    {
                        this.lblFolderDirName.Text += document.Name + "  ; ";
                    }

                    //var folderPermission = this._FolderPermisstionService.GetFolderByRoleIdList(Convert.ToInt32(Request.QueryString["doctype"]), GroupFunctionList)
                    //                           .Where(t => t.Folder_Write.GetValueOrDefault() || t.Folder_IsFullPermission.GetValueOrDefault()).Select(t => t.FolderId.GetValueOrDefault()).ToList();
                    //folderPermission = folderPermission.Union(this._FolderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"]))
                    //                                                .Where(t => t.Folder_Write.GetValueOrDefault() || t.Folder_IsFullPermission.GetValueOrDefault()).Select(t => t.FolderId.GetValueOrDefault())).Distinct().ToList();

                    var folderPermission = this._FolderPermisstionService.GetByRoleId(Convert.ToInt32(Request.QueryString["categoryId"]), UserSession.Current.User.RoleId.GetValueOrDefault())
                                               .Where(t => t.FolderId != null && (t.Folder_CreateSubFolder.GetValueOrDefault() || t.Folder_IsFullPermission.GetValueOrDefault()))
                                               .Select(t => t.FolderId.GetValueOrDefault()).ToList();
                    folderPermission = folderPermission.Union(this._FolderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["categoryId"]))
                                                                     .Where(t => t.FolderId != null && (t.Folder_CreateSubFolder.GetValueOrDefault() || t.Folder_IsFullPermission.GetValueOrDefault()))
                                                                     .Select(t => t.FolderId.GetValueOrDefault()))
                                                                    .Distinct().ToList();


                    var listFolder = this.AdminGroup.Contains(UserSession.Current.RoleId)
                                                ? this.folderService.GetAllByCategory(Convert.ToInt32(categoryId))
                                                : this.folderService.GetSpecificFolderStatic(folderPermission);

                    var rtvFolder = (RadTreeView)this.ddlFolder.Items[0].FindControl("rtvFolder");
                    if (rtvFolder != null)
                    {
                        rtvFolder.DataSource = listFolder;
                        rtvFolder.DataFieldParentID = "ParentId";
                        rtvFolder.DataTextField = "Name";
                        rtvFolder.DataValueField = "ID";
                        rtvFolder.DataFieldID = "ID";
                        rtvFolder.DataBind();

                        this.RestoreExpandStateTreeView("expandedNodesrtvFolder", rtvFolder);
                        Utility.SortNodes(rtvFolder.Nodes);
                    }
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            this.blockWarning.Visible = false;

            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["folderId"]))
                {
                    var folderNeedToMove = this.folderService.GetById(Convert.ToInt32(Request.QueryString["folderId"]));
                    if (folderNeedToMove != null)
                    {
                        var rtvFolder = (RadTreeView)this.ddlFolder.Items[0].FindControl("rtvFolder");
                        if (rtvFolder != null && rtvFolder.SelectedNode != null && rtvFolder.SelectedNode.Value != Request.QueryString["folderId"])
                        {
                            var folderDesination = this.folderService.GetById(Convert.ToInt32(rtvFolder.SelectedNode.Value));
                            folderNeedToMove.ParentID = folderDesination.ID;

                            // Update dirnamr for moved folder and subfolder
                            this.folderService.Update(folderNeedToMove);
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["docIds"]) && !string.IsNullOrEmpty(Request.QueryString["selectedFolder"]))
                {
                    var listDocId = Request.QueryString["docIds"].Split(',').Where(t => !string.IsNullOrEmpty(t)).Select(t => Convert.ToInt32(t)).ToList();
                    var rtvFolder = (RadTreeView)this.ddlFolder.Items[0].FindControl("rtvFolder");
                    if (rtvFolder != null && rtvFolder.SelectedNode != null && rtvFolder.SelectedNode.Value != Request.QueryString["selectedFolder"])
                    {
                        var flag = false;
                        var folderDesination = this.folderService.GetById(Convert.ToInt32(rtvFolder.SelectedNode.Value));

                        this.lblWarning.Text = "<font style='text-decoration: underline;'>Warning</font>: Files is already exist in destination folder '";
                        this.lblWarning.Text += folderDesination.DirName + "': <br/>";

                        var newDirName = folderDesination.DirName;


                        foreach (var docId in listDocId)
                        {
                            var objDoc = this.documentService.GetById(docId);
                            var existDocInDestinationFolder = this.documentService.GetSpecificDocument(folderDesination.ID, objDoc.Name);

                            if (objDoc != null && existDocInDestinationFolder == null)
                            {
                                var oldDirName = objDoc.DirName;
                                var newFilePath = Server.MapPath("../../" + newDirName + "/" + objDoc.Name);
                                var oldFilePath = Server.MapPath("../../" + objDoc.FilePath);

                                //var watcherService = new ServiceController(ServiceName);
                                //if (Utility.ServiceIsAvailable(ServiceName))
                                //{
                                //    watcherService.ExecuteCommand(128);
                                //}

                                if (File.Exists(oldFilePath))
                                {
                                    File.Move(oldFilePath, newFilePath);
                                }

                                //if (Utility.ServiceIsAvailable(ServiceName))
                                //{
                                //    watcherService.ExecuteCommand(129);
                                //}

                                var listDoc =
                                    this.documentService.GetAllDocRevision(
                                        objDoc.ParentID == null ? objDoc.ID : objDoc.ParentID.GetValueOrDefault());

                                foreach (var document in listDoc)
                                {
                                    document.FolderID = folderDesination.ID;
                                    //document.CategoryID = folderDesination.CategoryID;
                                    document.DirName = document.DirName.Replace(oldDirName, newDirName);
                                    document.FilePath = document.FilePath.Replace(oldDirName, newDirName);
                                    document.LastUpdatedBy = UserSession.Current.User.Id;
                                    document.LastUpdatedDate = DateTime.Now;

                                    this.documentService.Update(document);
                                }
                            }
                            else if (existDocInDestinationFolder != null)
                            {
                                flag = true;
                                this.lblWarning.Text += "_ " + existDocInDestinationFolder.Name + "<br/>";
                            }
                        }

                        if (flag)
                        {
                            this.blockWarning.Visible = true;
                        }
                        else
                        {
                            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebindGrid();", true);
                        }
                    }
                }
                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
            catch (Exception ex)
            {
                //var watcherService = new ServiceController("EDMSFolderWatcher");
                //if (Utility.ServiceIsAvailable("EDMSFolderWatcher"))
                //{
                //    watcherService.ExecuteCommand(129);
                //}
            }
        }

        protected void rtvFolder_OnNodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = "~/Images/folderdir16.png";
        }

        /// <summary>
        /// The restore expand state tree view.
        /// </summary>
        private void RestoreExpandStateTreeView(string cookiesName, RadTreeView rtv)
        {
            // Restore expand state of tree folder
            HttpCookie cookie = Request.Cookies[cookiesName];
            if (cookie != null)
            {
                var expandedNodeValues = cookie.Value.Split('*');
                foreach (var nodeValue in expandedNodeValues)
                {
                    RadTreeNode expandedNode = rtv.FindNodeByValue(HttpUtility.UrlDecode(nodeValue));
                    if (expandedNode != null)
                    {
                        expandedNode.Expanded = true;
                    }
                }
            }
        }
    }
}