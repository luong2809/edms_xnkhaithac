﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;

namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class CategoryEditForm : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly GroupDataPermissionService groupDataPermissionService;

        /// <summary>
        /// The category service.
        /// </summary>
        private readonly CategoryService categoryService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly FolderService folderService;

        private readonly RoleService roleService = new RoleService();

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public CategoryEditForm()
        {
            this.userService = new UserService();
            this.groupDataPermissionService = new GroupDataPermissionService();
            this.categoryService = new CategoryService();
            this.folderService = new FolderService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                LoadCombobox();
                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    this.CreatedInfo.Visible = true;

                    var objDis = this.categoryService.GetById(Convert.ToInt32(this.Request.QueryString["disId"]));
                    if (objDis != null)
                    {
                        this.txtName.Text = objDis.Name;
                        this.txtDescription.Text = objDis.Description;
                        this.ddlRoles.SelectedValue = objDis.DepartmentID.ToString();
                        var createdUser = this.userService.GetByID(objDis.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + objDis.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (objDis.LastUpdatedBy != null && objDis.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(objDis.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + objDis.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        private void LoadCombobox()
        {
            var roles = this.roleService.GetAll();
            roles.Insert(0, new Role() { Id = 0, Name = string.Empty });
            this.ddlRoles.DataSource = roles;
            this.ddlRoles.DataValueField = "Id";
            this.ddlRoles.DataTextField = "Name";
            this.ddlRoles.DataBind();
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    var disId = Convert.ToInt32(this.Request.QueryString["disId"]);
                    var obj = this.categoryService.GetById(disId);
                    if (obj != null)
                    {
                        obj.Name = this.txtName.Text.Trim();
                        obj.Description = this.txtDescription.Text.Trim();
                        int dept;
                        int.TryParse(ddlRoles.SelectedValue,out dept);
                        obj.DepartmentID = dept;
                        obj.DepartmentName = ddlRoles.SelectedItem.Text;
                        obj.LastUpdatedBy = UserSession.Current.User.Id;
                        obj.LastUpdatedDate = DateTime.Now;
                        obj.IsActive = this.cbIsActive.Checked;
                        this.categoryService.Update(obj);

                        var folderRoot = this.folderService.GetAllByCategory(obj.ID).Find(t => t.ParentID == null);
                        if(folderRoot != null)
                        {
                            folderRoot.Name = this.txtName.Text;
                            folderRoot.Description = this.txtName.Text;
                            this.folderService.Update(folderRoot);
                        }
                    }
                }
                else
                {
                    var obj = new Category();
                    obj.Name = this.txtName.Text.Trim();
                    obj.Description = this.txtDescription.Text.Trim();
                    int dept;
                    int.TryParse(ddlRoles.SelectedValue, out dept);
                    obj.DepartmentID = dept;
                    obj.DepartmentName = ddlRoles.SelectedItem.Text;
                    obj.CreatedBy = UserSession.Current.User.Id;
                    obj.CreatedDate = DateTime.Now;
                    obj.IsActive = this.cbIsActive.Checked;

                    var categoryId = this.categoryService.Insert(obj);
                    var folderGUID = Guid.NewGuid();
                    var folder = new Folder()
                    {
                        Name = this.txtName.Text,
                        Description = this.txtName.Text,
                        CategoryID = categoryId,
                        DirName = "DocLib" + "/" + folderGUID,
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now
                    };

                    var folId = this.folderService.Insert(folder);
                    Directory.CreateDirectory(Server.MapPath("~/" + folder.DirName));
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if (this.txtName.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter Category name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
            else
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    var categoryId = Convert.ToInt32(this.Request.QueryString["disId"]);
                    if (this.categoryService.IsExist(categoryId, this.txtName.Text.Trim()))
                    {
                        this.fileNameValidator.ErrorMessage = "Category name is exist.";
                        this.divFileName.Style["margin-bottom"] = "-26px;";
                        args.IsValid = false;
                    }
                }
                else
                {
                    if (this.categoryService.IsExist(this.txtName.Text.Trim()))
                    {
                        this.fileNameValidator.ErrorMessage = "Category name is exist.";
                        this.divFileName.Style["margin-bottom"] = "-26px;";
                        args.IsValid = false;
                    }
                }
            }
        }
    }
}