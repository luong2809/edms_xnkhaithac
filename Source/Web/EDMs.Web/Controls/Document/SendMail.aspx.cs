﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Configuration;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Web.Utilities.Sessions;
    using Telerik.Web.UI;
    using System.Net.Mime;
    using System.IO;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class SendMail : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        private readonly CategoryService categoryService;

        private readonly FolderService folderService;
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public SendMail()
        {
            this.documentService = new DocumentService();
            this.userService = new UserService();
            this.categoryService = new CategoryService();
            this.folderService = new FolderService();
        }

        /// <summary>
        /// Validation existing patient code
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        protected void ValidatePatientCode(object source, ServerValidateEventArgs arguments)
        {
        }


        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadComboData();
                this.ddlEmail.Focus();

                if (!string.IsNullOrEmpty(this.Request.QueryString["listDoc"]))
                {
                    var categoryId = 0;
                    var count = 0;
                    var listDocId =
                        this.Request.QueryString["listDoc"].Split(',').Where(t => !string.IsNullOrEmpty(t)).Select(
                            t => Convert.ToInt32(t));
                    var bodyContent = @"<html><body>Dear All,<br><br>
                                        Please be informed that the following documents are now available on the Document Library System for your information and implementation.<br>

                                        <table border='1' cellspacing='0'>
	                                        <tr>
		                                        <th style='text-align:center; width:40px'>No.</th>
		                                        <th style='text-align:center; width:330px'>Document Number</th>
		                                        <th style='text-align:center; width:330px'>Document Title</th>
		                                        <th style='text-align:center; width:120px'>Document Type</th>
	                                        </tr>";
                    foreach (var docId in listDocId)
                    {
                        var document = this.documentService.GetById(docId);

                        if (document != null)
                        {
                            categoryId = document.CategoryID.GetValueOrDefault();
                            count += 1;
                            //bodyContent += @"<tr>
                            //    <td>" + count + @"</td>
                            //    <td><a href='" + ConfigurationSettings.AppSettings.Get("WebAddress")
                            //               + document.FilePath + "' download='" + document.DocumentNumber != null ? document.DocumentNumber : document.Name + "'>"
                            //               + document.DocumentNumber != null ? document.DocumentNumber : document.Name + @"</a></td>
                            //    <td>"
                            //               + document.Title + @"</td>
                            //    <td>"
                            //               + document.DocumentTypeName + @"</td></tr>";
                            bodyContent += @"<tr>
                                <td style='text-align: center;'>" + count + @"</td>
                                <td>" + document.Name + @"</td>" +
                                "<td>" + document.Title + @"</td>" +
                                "<td>" + document.RevisionName + @"</td>" +
                                "</tr>";
                        }
                    }
                    bodyContent += @"</table><br>";
                    var type = this.Request.QueryString["type"];
                    if (type == "1")
                    {
                        bodyContent += @" Access file with link system below: <br>";
                        bodyContent += @"" + ConfigurationManager.AppSettings["WebAddress"] + "DocumentsHome.aspx?doctype=" + this.Request.QueryString["categoryId"] + "<br>";
                    }
                    bodyContent += @"Thanks and regards,<br><br>
                                        " + UserSession.Current.User.FullName + ". <br/></body></html>";
                    this.FreeTextBox1.Text = bodyContent;

                    var category = this.categoryService.GetById(categoryId);

                    this.txtSubject.Text = "Availability of New/Updated " + (category != null ? category.Name : string.Empty) + " Documents on EDMS System";
                }
            }
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {
            var listUser = this.userService.GetAll().Where(t => !string.IsNullOrEmpty(t.Email.Trim())).OrderBy(t => t.Email);
            this.ddlEmail.DataSource = listUser;
            this.ddlEmail.DataTextField = "Email";
            this.ddlEmail.DataValueField = "Email";
            this.ddlEmail.DataBind();

            this.ddlEmailCC.DataSource = listUser;
            this.ddlEmailCC.DataTextField = "Email";
            this.ddlEmailCC.DataValueField = "Email";
            this.ddlEmailCC.DataBind();
        }

        protected void SendMailMenu_OnButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (this.IsValid)
            {
                try
                {
                    var smtpClient = new SmtpClient
                    {
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                        EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                        Host = ConfigurationManager.AppSettings["Host"],
                        Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                        Credentials = new NetworkCredential(ConfigurationManager.AppSettings["Email"], ConfigurationManager.AppSettings["EmailPass"])
                    };
                    var message = new MailMessage();
                    message.From = new MailAddress(ConfigurationManager.AppSettings["Email"], ConfigurationManager.AppSettings["Email"]);
                    message.Subject = this.txtSubject.Text.Trim();
                    message.BodyEncoding = new UTF8Encoding();
                    message.IsBodyHtml = true;
                    message.Body = this.FreeTextBox1.Text;
                    string stmessage = string.Empty;
                    var listDocId = this.Request.QueryString["listDoc"].Split(',').Where(t => !string.IsNullOrEmpty(t)).Select(t => Convert.ToInt32(t)).ToList();
                    var type = this.Request.QueryString["type"];
                    if (type == "0")
                    {
                        var documentlist = this.documentService.GetSpecificDocument(listDocId);
                        if (documentlist.Sum(t => t.FileSize) <= 20971520)
                        {
                            foreach (var document in documentlist)
                            {
                               // var document = this.documentService.GetById(docId);

                                if (document != null)
                                {
                                    FileInfo file = new FileInfo(Server.MapPath("../../"+document.FilePath));
                                    if (file.Exists)
                                    {
                                        Attachment data = new Attachment(file.FullName, MediaTypeNames.Application.Octet);
                                        message.Attachments.Add(data);
                                    }

                                }
                            }
                        }
                        else
                        {
                            stmessage = "Please Attach file less than 20MB.";
                        }
                    }

                    if (string.IsNullOrEmpty(stmessage)) {
                        if (!string.IsNullOrEmpty(this.ddlEmail.Text))
                        {
                            var toList = this.ddlEmail.Text.Split(',').Where(t => !string.IsNullOrEmpty(t));
                            var ccList = this.ddlEmailCC.Text.Split(',').Where(t => !string.IsNullOrEmpty(t));

                            foreach (var to in toList)
                            {
                                message.To.Add(new MailAddress(to));
                            }

                            foreach (var cc in ccList)
                            {
                                message.CC.Add(new MailAddress(cc));
                            }

                            smtpClient.Send(message);



                            lblNotification.Text = "Send email successfully.";
                            RadNotification1.Title = "Message: ";
                            RadNotification1.Show();
                           // this.SendMailMenu.Visible = false;
                            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "Close();", true);
                        }       }
                    else
                    {
                        lblNotification.Text = stmessage;
                        RadNotification1.Title = "WARING";
                        RadNotification1.ContentIcon = "~/Images/error.png";
                        RadNotification1.Show();
                    }
                }
                catch (Exception ex)
                {
                    //ScriptManager.RegisterStartupScript(this.Page,this.GetType(), "myalert", "alert(Error: '" + ex.Message + "');", true);

                    lblNotification.Text = "Error :"+ ex.Message;
                    RadNotification1.Title = "Error";
                    RadNotification1.ContentIcon = "~/Images/error.png";
                    RadNotification1.Show();

                }
            }
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationEmptyEmailAddress(object source, ServerValidateEventArgs args)
        {
            if (this.ddlEmail.Text.Trim().Length == 0)
            {
                this.selectEmailValidate.ErrorMessage = "Please enter email address.";
                args.IsValid = false;
            }
        }
    }
}