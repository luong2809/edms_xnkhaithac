﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI;
    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class CheckOutInHistoryList : Page
    {
        /// <summary>
        /// The document service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly CheckOutInHistoryService checkOutInHistoryService;
        private readonly AttachFileService attachFileService;
        private readonly DCRService dcrService;


        /// <summary>
        /// Initializes a new instance of the <see cref="RevisionHistory"/> class.
        /// </summary>
        public CheckOutInHistoryList()
        {
            this.documentService = new DocumentService();
            this.checkOutInHistoryService = new CheckOutInHistoryService();
            this.attachFileService = new AttachFileService();
            this.dcrService = new DCRService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (Request.QueryString["docId"] != null)
            {
                var docId = Convert.ToInt32(Request.QueryString["docId"]);
                var listHistory = this.checkOutInHistoryService.GetAllByDocId(docId);
                this.grdDocument.DataSource = listHistory;
            }
        }

        protected void grdDocument_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            switch (e.DetailTableView.Name)
            {
                case "DocDetail":
                {
                    var historyId = Convert.ToInt32(dataItem.GetDataKeyValue("ID").ToString());
                    var history = this.checkOutInHistoryService.GetByID(historyId);
                    if (history != null && history.ActionName != "Checkin")
                    {
                        var dcrObj = this.dcrService.GetById(history.DCRId.GetValueOrDefault());
                        if (dcrObj != null)
                        {
                            e.DetailTableView.DataSource = this.attachFileService.GetSpecific(dcrObj.ID);
                        }
                    }
                    else
                    {
                        e.DetailTableView.DataSource = new List<AttachFile>();
                    }
                    break;
                }
            }
        }
    }
}