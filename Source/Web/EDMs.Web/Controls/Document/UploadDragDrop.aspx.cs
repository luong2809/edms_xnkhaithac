﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.ServiceProcess;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    using ZetaLongPaths;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class UploadDragDrop : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly FolderService folderService;
        private readonly AttachFileService attachFileService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public UploadDragDrop()
        {
            this.documentService = new DocumentService();
            this.folderService = new FolderService();
            this.attachFileService = new AttachFileService();
        }

        /// <summary>
        /// Validation existing patient code
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        protected  void ValidatePatientCode(object source, ServerValidateEventArgs arguments)
        {
        }


        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {
                        
                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            this.Session.Remove("IsFillData");
            Document objDoc;
            if (this.Page.IsValid)
            {
                try
                {   var error = "";
                    if (!string.IsNullOrEmpty(this.Request.QueryString["folId"]))
                    {
                        var folId = Convert.ToInt32(this.Request.QueryString["folId"]);
                        var folder = this.folderService.GetById(folId);
                        var targetFolder = "../../" + folder.DirName;
                        //var revisionPath = "../../DocumentLibrary/RevisionHistory/";
                        var serverFolder = HostingEnvironment.ApplicationVirtualPath == "/" ? "/" + folder.DirName : HostingEnvironment.ApplicationVirtualPath + "/" + folder.DirName;
                        //var serverRevisionFolder = HostingEnvironment.ApplicationVirtualPath == "/" ? "/DocumentLibrary/RevisionHistory/" : HostingEnvironment.ApplicationVirtualPath + "/DocumentLibrary/RevisionHistory/";
                        var listUpload = docuploader.UploadedFiles;
                        if (listUpload.Count > 0)
                        {
                            foreach (UploadedFile docFile in listUpload)
                            {
                                var docFileName = docFile.FileName;
                                var docFileNameOrignal = docFile.FileName;

                                objDoc = new Document();

                                var docObjLeaf = this.documentService.GetSpecificDocument(folId, docFileNameOrignal);
                                if (docObjLeaf != null)
                                {
                                //    objDoc.DocumentNumber = docObjLeaf.DocumentNumber;
                                //    objDoc.Title = docObjLeaf.Title;
                                //    objDoc.DocumentTypeID = docObjLeaf.DocumentTypeID;
                                //    objDoc.DocumentTypeName = docObjLeaf.DocumentTypeName;                          
                                //    objDoc.Well = docObjLeaf.Well;
                                //    objDoc.KeyWords = docObjLeaf.KeyWords;
                                //    objDoc.FileNameOriginal = docObjLeaf.FileNameOriginal;
                                //    objDoc.ParentID = docObjLeaf.ParentID;

                                //    docObjLeaf.IsLeaf = false;
                                //    this.documentService.Update(docObjLeaf);

                                    error += docFileName + " ;";
                                }

                                objDoc.FolderID = folId;
                                objDoc.CreatedBy = UserSession.Current.User.Id;
                                objDoc.CreatedDate = DateTime.Now;
                                objDoc.IsLeaf = true;
                                objDoc.IsDelete = false;

                                //var docRevisionFileName = string.Empty;
                                //if (!string.IsNullOrEmpty(objDoc.RevisionName))
                                //{
                                //    docRevisionFileName = objDoc.RevisionName + "_" + docFile.FileName;
                                //}
                                //else
                                //{
                                //    docRevisionFileName = docFile.FileName;
                                //}

                               // var revisionServerFileName = DateTime.Now.ToString("ddMMyyhhmmss") + "_" + docRevisionFileName;
                                var serverDocFileName = Utility.RemoveAllSpecialCharacter(docFileName);

                                // Path file to save on server disc
                                var saveFilePath = Path.Combine(Server.MapPath(targetFolder), serverDocFileName);
                               // var saveFileRevisionPath = Path.Combine(Server.MapPath(revisionPath), revisionServerFileName);

                                // Path file to download from server
                                var serverFilePath = serverFolder + "/" + serverDocFileName;
                              //  var revisionFilePath = serverRevisionFolder + revisionServerFileName;
                                var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);
                                objDoc.FileSize = docFile.ContentLength;
                                objDoc.RevisionFileName = docFileName;
                                objDoc.RevisionFilePath = serverFilePath;
                                objDoc.FilePath = serverFilePath;
                                objDoc.FileExtension = fileExt;
                                if (!Utility.FileIcon.ContainsKey(fileExt.ToLower()))
                                {
                                    Utility.FileIcon.Add(fileExt.ToLower(), "images/otherfile.png");
                                    //objDoc.FileExtensionIcon =  "images/otherfile.png";
                                }
                                objDoc.FileExtensionIcon = !string.IsNullOrEmpty(Utility.FileIcon[fileExt.ToLower()]) ? Utility.FileIcon[fileExt.ToLower()] : "images/otherfile.png";
                                objDoc.Name = docFileNameOrignal;
                                objDoc.CategoryID = folder.CategoryID;
                                objDoc.DirName = folder.DirName;

                                if (objDoc.ParentID == null)
                                {
                                    objDoc.FileNameOriginal = docFileName;
                                }

                                //var serviceName = ConfigurationManager.AppSettings.Get("ServiceName");
                                //var watcherService = new ServiceController(serviceName);
                                //if (Utility.ServiceIsAvailable(serviceName))
                                //{
                                //    watcherService.ExecuteCommand(128);
                                //}

                                docFile.SaveAs(saveFilePath, true);

                                //if (Utility.ServiceIsAvailable(serviceName))
                                //{
                                //    watcherService.ExecuteCommand(129);
                                //}

                                if (File.Exists(saveFilePath))
                                {
                                    ////var fileinfo = new FileInfo(saveFilePath);
                                    ////fileinfo.CopyTo(saveFileRevisionPath, true);


                                    this.documentService.Insert(objDoc);

                                    //var attachFile = new AttachFile()
                                    //{
                                    //    RefId = objDoc.ID,
                                    //    FileName = docFileName,
                                    //    Extension = fileExt,
                                    //    FilePath = serverFilePath,
                                    //    ExtensionIcon = !string.IsNullOrEmpty(Utility.FileIcon[fileExt]) ? "~/"+Utility.FileIcon[fileExt] : "~/images/otherfile.png",
                                    //    FileSize = (double)docFile.ContentLength / 1024,
                                    //    CreatedBy = UserSession.Current.User.Id,
                                    //    CreatedDate = DateTime.Now
                                    //};

                                    //this.attachFileService.Insert(attachFile);
                                }
                            }
                            var ListNodeExpanded = (List<int>)Session["ListNodeExpanded"];
                            ListNodeExpanded.Add(folId);
                            UpdatePropertisFolder(folder.CategoryID.GetValueOrDefault(), ListNodeExpanded); 
                        }
                    }

                    if (error != "")
                    {
                        this.blockError.Visible = true;
                        this.lblError.Text = "Have error when upload document file: <br/>'" + error + "'<br/> Already exists.";
                    }
                    else
                    {
                        this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
                    }
                }
                catch (Exception ex)
                {
                    this.blockError.Visible = true;
                    this.lblError.Text = "Have error when upload document file: <br/>'" + ex.Message + "'";

                    //var watcherService = new ServiceController("EDMSFolderWatcher");
                    //if (Utility.ServiceIsAvailable("EDMSFolderWatcher"))
                    //{
                    //    watcherService.ExecuteCommand(129);
                    //}
                }
            }
        }
        private List<int> GetAllChildren(int parent, List<Folder> folderList, ref List<int> childNodes)
        {
            var childFolderList = folderList.Where(t => t.ParentID == parent).ToList();
            if (childFolderList.Any())
            {
                childNodes.AddRange(childFolderList.Select(t => t.ID));
                foreach (var childFolder in childFolderList)
                {
                    this.GetAllChildren(childFolder.ID, folderList, ref childNodes);
                }
            }

            return childNodes.Distinct().ToList();
        }
       private void UpdatePropertisFolder(int categori, List<int> ListNodeCurrent)
        {
            //var listFolder = this.folderService.GetAllByCategory(categori).OrderBy
            //    (t=> t.ID).ToList();
            //var listFolder= listFolder
            foreach (var folderItem in ListNodeCurrent.OrderByDescending(t => t))
            {
                var objFolder = this.folderService.GetById(folderItem);
                // List<int> listTemp = new List<int>();
                var subfolderList = this.folderService.GetAllByParentId(folderItem);
                objFolder.NumberOfSubfolder = subfolderList.Count + subfolderList.Sum(t => t.NumberOfSubfolder);
                ////subfolderList.Add(objFolder);
                var docList = this.documentService.GetAllByFolder(objFolder.ID);
                var folderSize = docList.Sum(t => t.FileSize) + subfolderList.Sum(t => t.FolderSize);
                objFolder.NumberOfDocument = docList.Count + subfolderList.Sum(t => t.NumberOfDocument);
                objFolder.FolderSize = folderSize;
                this.folderService.Update(objFolder);
            }
        }
        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The save upload file.
        /// </summary>
        /// <param name="uploadDocControl">
        /// The upload doc control.
        /// </param>
        /// <param name="objDoc">
        /// The obj Doc.
        /// </param>
        private void SaveUploadFile(RadAsyncUpload uploadDocControl, ref Document objDoc)
        {
            
        }
    }
}