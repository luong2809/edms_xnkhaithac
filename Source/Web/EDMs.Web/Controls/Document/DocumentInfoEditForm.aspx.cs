﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.ServiceProcess;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    using ZetaLongPaths;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class DocumentInfoEditForm : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly StatusService statusService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService;

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly LanguageService languageService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        private readonly FolderService folderService;

        /// <summary>
        /// The doc properties view service.
        /// </summary>
        private readonly DocPropertiesViewService docPropertiesViewService;

        protected const string ServiceName = "EDMSFolderWatcher";

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public DocumentInfoEditForm()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.statusService = new StatusService();
            this.disciplineService = new DisciplineService();
            this.receivedFromService = new ReceivedFromService();
            this.languageService = new LanguageService();
            this.documentService = new DocumentService();
            this.userService = new UserService();
            this.folderService = new FolderService();
            this.docPropertiesViewService = new DocPropertiesViewService();
        }

        /// <summary>
        /// Validation existing patient code
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        protected void ValidatePatientCode(object source, ServerValidateEventArgs arguments)
        {
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
              //  this.LoadComboData();
                LoadViewPropertiesConfig();
                var folder =
                    this.folderService.GetById(!string.IsNullOrEmpty(Request.QueryString["folId"])
                        ? Convert.ToInt32(Request.QueryString["folId"])
                        : 0);

                //  this.ExpiryDate.Visible = folder != null && folder.DirName.ToLower().Contains("certificate");

                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    this.UploadControl.Visible = false;
                    this.CreatedInfo.Visible = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("ViewCreatedUpdatedInfo"));

                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {
                        this.txtName.Enabled = false;
                        this.txtName.Text = objDoc.Name;
                        this.txtDocumentNumber.Text = objDoc.DocumentNumber;
                        this.txtTitle.Text = objDoc.Title;
                        this.txtRevision.Text = objDoc.RevisionName;
                        this.txtDate.SelectedDate = objDoc.Date;
                        this.txtFromto.Text = objDoc.FromTo;
                        this.txtDiscipline.Text = objDoc.DisciplineName;
                        this.txtDocumentType.Text = objDoc.DocumentTypeName;
                        this.txtFlatform.Text = objDoc.Platform;
                        this.txtRemark.Text = objDoc.Remark;
                        this.txtContractor.Text = objDoc.Contractor;
                        this.txtTag.Text = objDoc.Tag;

                        this.lblFileSize.Text = "File size " + SizeSuffix(objDoc.FileSize.HasValue ? Convert.ToInt64(objDoc.FileSize.Value) : 0, 1);

                        var createdUser = this.userService.GetByID(objDoc.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + objDoc.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty) + "<br/>";

                        if (objDoc.LastUpdatedBy != null && objDoc.LastUpdatedDate != null)
                        {
                            var lastUpdatedUser = this.userService.GetByID(objDoc.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + objDoc.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty) + "<br/>";
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.UploadControl.Visible = true;
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        protected readonly string[] SizeSuffixes =
                   { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
        public string SizeSuffix(Int64 value, int decimalPlaces = 1)
        {
            if (decimalPlaces < 0) { throw new ArgumentOutOfRangeException("decimalPlaces"); }
            if (value < 0) { return "-" + SizeSuffix(-value); }
            if (value == 0) { return string.Format("{0:n" + decimalPlaces + "} bytes", 0); }

            // mag is 0 for bytes, 1 for KB, 2, for MB, etc.
            int mag = (int)Math.Log(value, 1024);

            // 1L << (mag * 10) == 2 ^ (10 * mag) 
            // [i.e. the number of bytes in the unit corresponding to mag]
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            // make adjustment when the value is large enough that
            // it would round up to 1000 or more
            if (Math.Round(adjustedSize, decimalPlaces) >= 1000)
            {
                mag += 1;
                adjustedSize /= 1024;
            }

            return string.Format("{0:n" + decimalPlaces + "} {1}",
                adjustedSize,
                SizeSuffixes[mag]);
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            this.Session.Remove("IsFillData");
            Document objDoc;

            if (this.Page.IsValid)
            {
                try
                {
                    if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                    {
                        objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                        ////var currentRevision = objDoc.RevisionID;
                       
                        var oldName = objDoc.Name;
                        ////var filePath = Server.MapPath(objDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                        ////var revisionFilePath = Server.MapPath(objDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                        var filePath = Server.MapPath(HostingEnvironment.ApplicationVirtualPath == "/" ? objDoc.FilePath : objDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                        var revisionFilePath = Server.MapPath(HostingEnvironment.ApplicationVirtualPath == "/" ? objDoc.RevisionFilePath : objDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                        if (File.Exists(filePath) && !File.Exists(revisionFilePath))
                        {
                            File.Copy(filePath, revisionFilePath, true);
                        }

                        if (objDoc.Name != this.txtName.Text.Trim())
                        {
                            if (File.Exists(filePath))
                            {
                                File.Move(filePath, filePath.Replace(oldName, this.txtName.Text.Trim()));
                            }

                            var listDocRename = this.documentService.GetSpecificDocument(oldName, objDoc.DirName);
                            foreach (var document in listDocRename)
                            {
                                document.Name = this.txtName.Text.Trim();
                                document.FileNameOriginal = this.txtName.Text.Trim();
                                if (!string.IsNullOrEmpty(document.RevisionName))
                                {
                                    document.RevisionFileName = document.RevisionName + "_" + this.txtName.Text.Trim();
                                }
                                else
                                {
                                    document.RevisionFileName = this.txtName.Text.Trim();
                                }

                                document.FilePath = document.FilePath.Replace(oldName, this.txtName.Text.Trim());

                                if (document.ID == objDoc.ID)
                                {
                                    document.DocumentNumber = this.txtDocumentNumber.Text;
                                    document.Title = this.txtTitle.Text;
                                    document.RevisionName = this.txtRevision.Text;
                                    document.Date = this.txtDate.SelectedDate;
                                    document.FromTo = this.txtFromto.Text;
                                    document.DisciplineName = this.txtDiscipline.Text;
                                    document.DocumentTypeName = this.txtDocumentType.Text;
                                    document.Platform = this.txtFlatform.Text;
                                    document.Remark = this.txtRemark.Text;
                                    document.Contractor = this.txtContractor.Text;
                                    document.Tag = this.txtTag.Text;


                                    document.LastUpdatedBy = UserSession.Current.User.Id;
                                    document.LastUpdatedDate = DateTime.Now;
                                }

                                this.documentService.Update(document);
                            }
                        }
                        else
                        {
                            objDoc.Name = this.txtName.Text;
                            objDoc.DocumentNumber = this.txtDocumentNumber.Text;
                            objDoc.Title = this.txtTitle.Text;
                            objDoc.RevisionName = this.txtRevision.Text;
                            objDoc.Date = this.txtDate.SelectedDate;
                            objDoc.FromTo = this.txtFromto.Text;
                            objDoc.DisciplineName = this.txtDiscipline.Text;
                            objDoc.DocumentTypeName = this.txtDocumentType.Text;
                            objDoc.Platform = this.txtFlatform.Text;
                            objDoc.Remark = this.txtRemark.Text;
                            objDoc.Contractor = this.txtContractor.Text;
                            objDoc.Tag = this.txtTag.Text;
                           

                            if (objDoc.CreatedBy == null)
                            {
                                objDoc.CreatedBy = UserSession.Current.User.Id;
                            }

                            objDoc.LastUpdatedBy = UserSession.Current.User.Id;
                            objDoc.LastUpdatedDate = DateTime.Now;

                            this.documentService.Update(objDoc);
                        }
                    }
                    else
                    {
                        var listFile = this.docuploader.UploadedFiles;
                        if (listFile.Count > 0)
                        {
                            var folder = this.folderService.GetById(Convert.ToInt32(Request.QueryString["folId"]));

                            objDoc = new Document()
                            {
                                Name = this.txtName.Text,
                                DocumentNumber = this.txtDocumentNumber.Text,
                                Title = this.txtTitle.Text,
                               
                                Remark = this.txtRemark.Text,
                               
                                CreatedBy = UserSession.Current.User.Id,
                                CreatedDate = DateTime.Now,
                                IsLeaf = true,
                                IsDelete = false
                            };
                            objDoc.RevisionName = this.txtRevision.Text;
                            objDoc.Date = this.txtDate.SelectedDate;
                            objDoc.FromTo = this.txtFromto.Text;
                            objDoc.DisciplineName = this.txtDiscipline.Text;
                            objDoc.DocumentTypeName = this.txtDocumentType.Text;
                            objDoc.Platform = this.txtFlatform.Text;
                            objDoc.Contractor = this.txtContractor.Text;
                            objDoc.Tag = this.txtTag.Text;

                            if (folder != null)
                            {
                                objDoc.CategoryID = folder.CategoryID;

                                UploadedFile docFile = this.docuploader.UploadedFiles[0];
                                var serverFolder = HostingEnvironment.ApplicationVirtualPath == "/" ? "/" + folder.DirName : HostingEnvironment.ApplicationVirtualPath + "/" + folder.DirName;
                                var docFileName = docFile.FileName;

                                var serverFilePath = serverFolder + "/" + docFileName;
                                var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);
                                objDoc.FileSize = docFile.ContentLength;
                                objDoc.FolderID = folder.ID;
                                objDoc.FilePath = serverFilePath;
                                objDoc.RevisionFilePath = serverFilePath;
                                objDoc.FileExtension = fileExt.ToLower();
                                objDoc.FileExtensionIcon = !string.IsNullOrEmpty(Utility.FileIcon[fileExt.ToLower()]) ? Utility.FileIcon[fileExt.ToLower()] : "images/otherfile.png";
                                objDoc.DirName = folder.DirName;
                            }
                            var isDiffRev = false;
                            var isUpdateOldRev = false;
                            if (this.docUploadedIsExist.Value == "true")
                            {
                                var docIdUpdate = Convert.ToInt32(this.docIdUpdateUnIsLeaf.Value);
                                var docUpdate = this.documentService.GetById(docIdUpdate);
                                if (docUpdate != null)
                                {
                                    isDiffRev = docUpdate.RevisionID != objDoc.RevisionID;
                                    isUpdateOldRev = docUpdate.RevisionID > objDoc.RevisionID;
                                    objDoc.DocIndex = docUpdate.DocIndex + 1;
                                    if (docUpdate.ParentID == null)
                                    {
                                        objDoc.ParentID = docUpdate.ID;
                                    }
                                    else
                                    {
                                        objDoc.ParentID = docUpdate.ParentID;
                                    }

                                    if (isUpdateOldRev)
                                    {
                                        objDoc.IsLeaf = false;
                                    }
                                    else
                                    {
                                        docUpdate.IsLeaf = false;
                                        objDoc.IsLeaf = true;
                                        this.documentService.Update(docUpdate);
                                    }
                                }
                            }
                            else
                            {
                                objDoc.DocIndex = 1;
                            }

                            this.SaveUploadFile(this.docuploader, ref objDoc, isUpdateOldRev);

                            this.documentService.Insert(objDoc);
                            var ListNodeExpanded = (List<int>)Session["ListNodeExpanded"];
                            ListNodeExpanded.Add(folder.ID);
                            UpdatePropertisFolder(folder.CategoryID.GetValueOrDefault(), ListNodeExpanded);
                        }
                    }

                    this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
                }
                catch (Exception ex)
                {

                }
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if (this.txtName.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter file name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
            else if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var docId = Convert.ToInt32(Request.QueryString["docId"]);
                var folderId = Convert.ToInt32(Request.QueryString["folId"]);
                this.fileNameValidator.ErrorMessage = "The specified name is already in use.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = !this.documentService.IsDocumentExistUpdate(folderId, this.txtName.Text.Trim(), docId);
            }
        }

        /// <summary>
        /// The rad ajax manager 1_ ajax request.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.Contains("CheckFileName"))
            {
                var fileName = e.Argument.Split('$')[1];
                var folderId = Convert.ToInt32(Request.QueryString["folId"]);

                if (this.documentService.IsDocumentExist(folderId, fileName))
                {
                    var docObjLeaf = this.documentService.GetSpecificDocument(folderId, fileName);
                    if (docObjLeaf != null)
                    {
                        this.txtDocumentNumber.Text = docObjLeaf.DocumentNumber;
                        this.txtTitle.Text = docObjLeaf.Title;
                       // this.ddlDocumentType.SelectedValue = docObjLeaf.DocumentTypeID.GetValueOrDefault().ToString();
                        // this.ddlStatus.SelectedValue = docObjLeaf.StatusID.GetValueOrDefault().ToString();
                        //  this.ddlDiscipline.SelectedValue = docObjLeaf.DisciplineID.GetValueOrDefault().ToString();
                        //  this.ddlReceivedFrom.SelectedValue = docObjLeaf.ReceivedFromID.GetValueOrDefault().ToString();
                        //  this.txtReceivedDate.SelectedDate = docObjLeaf.ReceivedDate.GetValueOrDefault();
                      //  this.txtWell.Text = docObjLeaf.Well;
                      //  this.txtKeywords.Text = docObjLeaf.KeyWords;

                        this.docUploadedIsExist.Value = "true";
                        this.docIdUpdateUnIsLeaf.Value = docObjLeaf.ID.ToString();
                    }
                }
            }
        }

        /// <summary>
        /// The docuploader_ file uploaded.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void docuploader_FileUploaded(object sender, FileUploadedEventArgs e)
        {
            var folderId = Convert.ToInt32(Request.QueryString["folId"]);
            var fileName = e.File.FileName;

            this.txtName.Text = fileName;
            if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
            {
                this.txtName.ReadOnly = false;
            }
            else
            {
                this.txtName.ReadOnly = true;
            }

            if (this.documentService.IsDocumentExist(folderId, fileName))
            {
                var docObjLeaf = this.documentService.GetSpecificDocument(folderId, fileName);
                if (docObjLeaf != null)
                {
                    if (this.Session["IsFillData"] == null || this.Session["IsFillData"].ToString() != "false")
                    {
                        this.txtDocumentNumber.Text = docObjLeaf.DocumentNumber;
                        this.txtTitle.Text = docObjLeaf.Title;
                        //this.ddlDocumentType.SelectedValue = docObjLeaf.DocumentTypeID.GetValueOrDefault().ToString();
                        // this.ddlStatus.SelectedValue = docObjLeaf.StatusID.GetValueOrDefault().ToString();
                        // this.ddlDiscipline.SelectedValue = docObjLeaf.DisciplineID.GetValueOrDefault().ToString();
                        // this.ddlReceivedFrom.SelectedValue = docObjLeaf.ReceivedFromID.GetValueOrDefault().ToString();
                        // this.txtReceivedDate.SelectedDate = docObjLeaf.ReceivedDate;
                        // this.ddlLanguage.SelectedValue = docObjLeaf.LanguageID.GetValueOrDefault().ToString();
                        // this.txtWell.Text = docObjLeaf.Well;
                      //  this.txtKeywords.Text = docObjLeaf.KeyWords;
                      //  this.txtRemark.Text = docObjLeaf.Remark;
                        // this.txtTransmittalNumber.Text = docObjLeaf.TransmittalNumber;
                    }

                    this.Session.Add("IsFillData", "false");

                    this.docUploadedIsExist.Value = "true";
                    this.docIdUpdateUnIsLeaf.Value = docObjLeaf.ID.ToString();
                }
            }
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {
            var categoryId = 0;

            if (!string.IsNullOrEmpty(this.Request.QueryString["categoryId"]))
            {
                categoryId = Convert.ToInt32(this.Request.QueryString["categoryId"]);
            }

            var revisionList = this.revisionService.GetAll();
            revisionList.Insert(0, new Revision() { Name = string.Empty });
            //this.ddlRevision.DataSource = revisionList;
            //this.ddlRevision.DataValueField = "ID";
            //this.ddlRevision.DataTextField = "Name";
            //this.ddlRevision.DataBind();

            var documentTypeList = this.documentTypeService.GetAllByCategory(categoryId).OrderBy(t => t.Name).ToList();
            documentTypeList.Insert(0, new DocumentType() { Name = string.Empty });
            //this.ddlDocumentType.DataSource = documentTypeList;
            //this.ddlDocumentType.DataValueField = "ID";
            //this.ddlDocumentType.DataTextField = "Name";
            //this.ddlDocumentType.DataBind();

            //var statusList = this.statusService.GetAllByCategory(categoryId).OrderBy(t => t.Name).ToList();
            //statusList.Insert(0, new Status { Name = string.Empty });
            //this.ddlStatus.DataSource = statusList;
            //this.ddlStatus.DataValueField = "ID";
            //this.ddlStatus.DataTextField = "Name";
            //this.ddlStatus.DataBind();

            //var receivedFromList = this.receivedFromService.GetAllByCategory(categoryId).OrderBy(t => t.Name).ToList();
            //receivedFromList.Insert(0, new ReceivedFrom() { Name = string.Empty });
            //this.ddlReceivedFrom.DataSource = receivedFromList;
            //this.ddlReceivedFrom.DataValueField = "ID";
            //this.ddlReceivedFrom.DataTextField = "Name";
            //this.ddlReceivedFrom.DataBind();

            //var disciplineList = this.disciplineService.GetAllByCategory(categoryId).OrderBy(t => t.Name).ToList();
            //disciplineList.Insert(0, new Discipline() { Name = string.Empty });
            //this.ddlDiscipline.DataSource = disciplineList;
            //this.ddlDiscipline.DataValueField = "ID";
            //this.ddlDiscipline.DataTextField = "Name";
            //this.ddlDiscipline.DataBind();

            //var languageList = this.languageService.GetAll().OrderBy(t => t.Name).ToList();
            //languageList.Insert(0, new Language() { Name = string.Empty });
            //this.ddlLanguage.DataSource = languageList;
            //this.ddlLanguage.DataValueField = "ID";
            //this.ddlLanguage.DataTextField = "Name";
            //this.ddlLanguage.DataBind();
        }
        private void UpdatePropertisFolder(int categori, List<int> ListNodeCurrent)
        {
            //var listFolder = this.folderService.GetAllByCategory(categori).OrderBy
            //    (t=> t.ID).ToList();
            //var listFolder= listFolder
            foreach (var folderItem in ListNodeCurrent.OrderByDescending(t => t))
            {
                var objFolder = this.folderService.GetById(folderItem);
                // List<int> listTemp = new List<int>();
                var subfolderList = this.folderService.GetAllByParentId(folderItem);
                objFolder.NumberOfSubfolder = subfolderList.Count + subfolderList.Sum(t => t.NumberOfSubfolder);
                ////subfolderList.Add(objFolder);
                var docList = this.documentService.GetAllByFolder(objFolder.ID);
                var folderSize = docList.Sum(t => t.FileSize) + subfolderList.Sum(t => t.FolderSize);
                objFolder.NumberOfDocument = docList.Count + subfolderList.Sum(t => t.NumberOfDocument);
                objFolder.FolderSize = folderSize;
                this.folderService.Update(objFolder);
            }
        }
        /// <summary>
        /// The save upload file.
        /// </summary>
        /// <param name="uploadDocControl">
        /// The upload doc control.
        /// </param>
        /// <param name="objDoc">
        /// The obj Doc.
        /// </param>
        private void SaveUploadFile(RadAsyncUpload uploadDocControl, ref Document objDoc, bool isUpdateOldRev)
        {
            try
            {
                var listUpload = uploadDocControl.UploadedFiles;
                var folder = this.folderService.GetById(objDoc.FolderID.GetValueOrDefault());
                var targetFolder = "../../" + folder.DirName;
                var revisionPath = "../../DocumentLibrary/RevisionHistory/";
                var serverFolder = HostingEnvironment.ApplicationVirtualPath == "/" ? "/" + folder.DirName : HostingEnvironment.ApplicationVirtualPath + "/" + folder.DirName;
                var serverRevisionFolder = HostingEnvironment.ApplicationVirtualPath == "/" ? "/DocumentLibrary/RevisionHistory/" : HostingEnvironment.ApplicationVirtualPath + "/DocumentLibrary/RevisionHistory/";
                if (listUpload.Count > 0)
                {
                    foreach (UploadedFile docFile in listUpload)
                    {
                        var docRevisionFileName = string.Empty;
                        var docFileNameOrignal = docFile.FileName;
                        if (!string.IsNullOrEmpty(objDoc.RevisionName))
                        {
                            docRevisionFileName = objDoc.RevisionName + "_" + docFile.FileName;
                        }
                        else
                        {
                            docRevisionFileName = docFile.FileName;
                        }

                        var revisionServerFileName = DateTime.Now.ToString("ddMMyyhhmmss") + "_" + docRevisionFileName;

                        // Path file to save on server disc
                        var saveFilePath = Path.Combine(Server.MapPath(targetFolder), docFileNameOrignal);
                        var saveFileRevisionPath = Path.Combine(Server.MapPath(revisionPath), revisionServerFileName);

                        // Path file to download from server
                        var serverFilePath = serverFolder + "/" + docFileNameOrignal;
                        var revisionFilePath = serverRevisionFolder + revisionServerFileName;

                        var fileExt = docFileNameOrignal.Substring(docFileNameOrignal.LastIndexOf(".") + 1, docFileNameOrignal.Length - docFileNameOrignal.LastIndexOf(".") - 1);

                        objDoc.RevisionFileName = docRevisionFileName;
                        objDoc.FilePath = serverFilePath;
                        objDoc.RevisionFilePath = revisionFilePath;
                        objDoc.FileExtension = fileExt;
                        objDoc.FileExtensionIcon = Utility.FileIcon.ContainsKey(fileExt.ToLower()) ? Utility.FileIcon[fileExt.ToLower()] : "images/otherfile.png";
                        objDoc.FileNameOriginal = docFileNameOrignal;
                        objDoc.DirName = folder.DirName;
                        ////EDMSFolderWatcher
                        //var watcherService = new ServiceController(ServiceName);
                        //if (Utility.ServiceIsAvailable(ServiceName))
                        //{
                        //    watcherService.ExecuteCommand(128);
                        //}

                        if (isUpdateOldRev)
                        {
                            docFile.SaveAs(saveFileRevisionPath, true);
                        }
                        else
                        {
                            docFile.SaveAs(saveFilePath, true);
                            var fileinfo = new FileInfo(saveFilePath);
                            fileinfo.CopyTo(saveFileRevisionPath, true);
                        }

                        //if (Utility.ServiceIsAvailable(ServiceName))
                        //{
                        //    watcherService.ExecuteCommand(129);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                //var watcherService = new ServiceController(ServiceName);
                //if (Utility.ServiceIsAvailable(ServiceName))
                //{
                //    watcherService.ExecuteCommand(129);
                //}
            }
        }

        private void LoadViewPropertiesConfig()
        {
            var isViewByGroup = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("ViewSettingByGroup"));
            var selectedProperty = new List<string>();
            var folder = this.folderService.GetById(Convert.ToInt32(Request.QueryString["folId"]));
            if(folder!= null && folder.CategoryID != null) {
                var categoryId = folder.CategoryID.GetValueOrDefault();
                var docPropertiesView = this.docPropertiesViewService.GetByCategory(categoryId);
                if (docPropertiesView != null && docPropertiesView.PropertyIndex != "")
                {
                    var temp =
                        docPropertiesView.PropertyIndex.Split(',').Where(t => !string.IsNullOrEmpty(t.Trim())).Select(
                            t => t.Trim()).ToList();
                    selectedProperty.AddRange(temp);
                }

                selectedProperty = selectedProperty.Distinct().ToList();
                var totalProperty = Convert.ToInt32(ConfigurationManager.AppSettings.Get("TotalProperty"));
                for (int i = 1; i <= totalProperty; i++)
                {
                    var property = (HtmlGenericControl)this.divContent.FindControl("Index" + i);
                    if (property != null)
                    {
                        property.Visible = selectedProperty.Contains(i.ToString());
                    }
                }
            }
        }
    }
}