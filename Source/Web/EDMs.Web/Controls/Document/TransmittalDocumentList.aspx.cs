﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class TransmittalDocumentList : Page
    {
        /// <summary>
        /// The transmittal service.
        /// </summary>
        private readonly TransmittalService transmittalService;

        private readonly DocumentService documentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="RevisionHistory"/> class.
        /// </summary>
        public TransmittalDocumentList()
        {
            this.documentService = new DocumentService();
            this.transmittalService = new TransmittalService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if(Request.QueryString["tranId"] != null)
            {
                var tranId = Convert.ToInt32(Request.QueryString["tranId"]);
                var objTran = this.transmittalService.GetById(tranId);
                ////var docListId = new List<int>();

                var listDocument = new List<Document>();

                if (objTran != null)
                {
                    if(!string.IsNullOrEmpty(objTran.DocList))
                    {
                        foreach (var docId in objTran.DocList.Split(';'))
                        {
                            if (!string.IsNullOrEmpty(docId))
                            {
                                ////docListId.Add(Convert.ToInt32(docId));
                                var objDoc = this.documentService.GetById(Convert.ToInt32(docId));
                                if (objDoc != null)
                                {
                                    listDocument.Add(objDoc);
                                }
                            }
                        }

                        this.grdDocument.DataSource = listDocument;
                    }
                    else
                    {
                        this.grdDocument.DataSource = new List<Document>();
                    }
                }
                else
                {
                    this.grdDocument.DataSource = new List<Document>(); 
                }
            }
        }
    }
}