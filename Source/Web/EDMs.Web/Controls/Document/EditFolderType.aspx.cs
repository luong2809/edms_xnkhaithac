﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.ServiceProcess;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    using ZetaLongPaths;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class EditFolderType : Page
    {
        private readonly FolderTypeService folderTypeService = new FolderTypeService();

        private readonly FolderService folderService = new FolderService();

        private readonly DocumentTypeService documentTypeService = new DocumentTypeService();
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public EditFolderType()
        {

        }

        /// <summary>
        /// Validation existing patient code
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        protected void ValidatePatientCode(object source, ServerValidateEventArgs arguments)
        {
        }


        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["folderId"]))
                {
                    var objFol = this.folderService.GetById(Convert.ToInt32(this.Request.QueryString["folderId"]));
                    if (objFol != null)
                    {
                        var listType = this.documentTypeService.GetAll();
                        listType.Insert(0, new DocumentType() { ID = 0, Name = string.Empty });
                        this.cboType.DataSource = listType;
                        this.cboType.DataTextField = "Name";
                        this.cboType.DataValueField = "Id";
                        this.cboType.DataBind();
                        this.cboType.SelectedIndex = 0;
                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                try
                {
                    if (!string.IsNullOrEmpty(this.Request.QueryString["folderId"]))
                    {
                        var objFol = this.folderService.GetById(Convert.ToInt32(this.Request.QueryString["folderId"]));
                        if (objFol != null)
                        {
                            var folderType = this.folderTypeService.GetByFolder(objFol.ID);
                            if (folderType != null)
                            {
                                folderType.FolderID = objFol.ID;
                                folderType.DocTypeID = Convert.ToInt32(cboType.SelectedValue);
                                this.folderTypeService.Update(folderType);
                            }
                            else
                            {
                                FolderType newFolderType = new FolderType();
                                newFolderType.FolderID = objFol.ID;
                                newFolderType.DocTypeID = Convert.ToInt32(cboType.SelectedValue);
                                this.folderTypeService.Insert(newFolderType);
                            }
                        }
                    }
                    this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
                }
                catch (Exception ex)
                {
                    this.blockError.Visible = true;
                    this.lblError.Text = ex.Message;
                }
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

    }
}