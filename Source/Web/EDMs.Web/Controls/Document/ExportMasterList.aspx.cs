﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.ServiceProcess;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Aspose.Cells;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    using ZetaLongPaths;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ExportMasterList : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly StatusService statusService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly TransmittalService transmittalService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ExportMasterList()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.statusService = new StatusService();
            this.disciplineService = new DisciplineService();
            this.receivedFromService = new ReceivedFromService();
            this.transmittalService = new TransmittalService();
            this.documentService = new DocumentService();
        }

        /// <summary>
        /// Validation existing patient code
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        protected  void ValidatePatientCode(object source, ServerValidateEventArgs arguments)
        {
        }


        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {
                        
                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["tranId"]))
            {
                var transID = Convert.ToInt32(this.Request.QueryString["tranId"]);
                var transObj = this.transmittalService.GetById(transID);

                if (transObj != null)
                {
                    foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                    {
                        var extension = docFile.GetExtension();
                        if (extension == ".doc" || extension == ".docx")
                        {
                            var filePath = Server.MapPath("../../" + transObj.GeneratePath);
                            docFile.SaveAs(filePath);

                            transObj.LastUpdatedBy = UserSession.Current.User.Id;
                            transObj.LastUpdatedDate = DateTime.Now;

                            this.transmittalService.Update(transObj);
                        }
                    }
                }
            }
            else
            {
                try
                {
                    foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                    {
                        var extension = docFile.GetExtension();
                        if (extension == ".xls" || extension == ".xlsx")
                        {
                            var importPath = Server.MapPath("../../DocumentResource/TransmittalGenerate/Generated") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                            docFile.SaveAs(importPath);

                            // Instantiate a new workbook
                            var workbook = new Workbook();
                            workbook.Open(importPath);

                            // Get the first worksheet in the workbook
                            Worksheet worksheet = workbook.Worksheets[0];

                            // Export worksheet data to a DataTable object by calling either ExportDataTable or ExportDataTableAsString method of the Cells class		 	
                            var dataTable = worksheet.Cells.ExportDataTable(1, 0, worksheet.Cells.MaxRow, worksheet.Cells.MaxColumn + 1)
                                                        .AsEnumerable()
                                                        .Where(t => t["Column1"].ToString() == "TRUE")
                                                        .CopyToDataTable();

                            var watcherService = new ServiceController("EDMSFolderWatcher");

                            foreach (DataRow dataRow in dataTable.Rows)
                            {
                                var isEdited = !string.IsNullOrEmpty(dataRow[0].ToString()) && Convert.ToBoolean(dataRow[0]);

                                if (isEdited)
                                {
                                    var isDuplicate = !string.IsNullOrEmpty(dataRow[6].ToString()) && Convert.ToBoolean(dataRow[6]);

                                    var docId = Convert.ToInt32(dataRow[1]);
                                    var objDoc = this.documentService.GetById(docId);
                                    if (objDoc != null)
                                    {
                                        var currentRevision = objDoc.RevisionID;
                                        var newRevision = 0;
                                        var objRev = this.revisionService.GetByName(dataRow[5].ToString());
                                        var objStatus = this.statusService.GetByName(dataRow[7].ToString());
                                        var objDiscipline = this.disciplineService.GetByName(dataRow[8].ToString());
                                        var objDocumentType = this.documentTypeService.GetByName(dataRow[9].ToString());
                                        var objReceivedFrom = this.receivedFromService.GetByName(dataRow[10].ToString());
                                        if (objRev != null)
                                        {
                                            newRevision = objRev.ID;
                                        }

                                        if (currentRevision != 0 && newRevision != 0 && currentRevision != null
                                            && currentRevision != newRevision)
                                        {
                                            var newObjDoc = new Document
                                                {
                                                    CategoryID = objDoc.CategoryID,
                                                    DocIndex = objDoc.DocIndex + 1,
                                                    ParentID = objDoc.ParentID,
                                                    FolderID = objDoc.FolderID,
                                                    DirName = objDoc.DirName,
                                                    FilePath = objDoc.FilePath,
                                                    RevisionFilePath = objDoc.RevisionFilePath,
                                                    FileExtension = objDoc.FileExtension,
                                                    FileExtensionIcon = objDoc.FileExtensionIcon,
                                                    FileNameOriginal = objDoc.FileNameOriginal,
                                                    KeyWords = objDoc.KeyWords,
                                                    IsDelete = false,
                                                    CreatedDate = DateTime.Now,
                                                    CreatedBy = UserSession.Current.User.Id,
                                                    Name = objDoc.Name,
                                                    DocumentNumber = dataRow[3].ToString(),
                                                    Title = dataRow[4].ToString(),
                                                    RevisionID = newRevision,
                                                    RevisionName = dataRow[5].ToString(),
                                                    StatusID = objStatus != null ? objStatus.ID : 0,
                                                    StatusName = objStatus != null ? objStatus.Name : string.Empty,
                                                    DisciplineID = objDiscipline != null ? objDiscipline.ID : 0,
                                                    DisciplineName = objDiscipline != null ? objDiscipline.Name : string.Empty,
                                                    DocumentTypeID = objDocumentType != null ? objDocumentType.ID : 0,
                                                    DocumentTypeName =
                                                        objDocumentType != null ? objDocumentType.Name : string.Empty,
                                                    ReceivedFromID = objReceivedFrom != null ? objReceivedFrom.ID : 0,
                                                    ReceivedFromName =
                                                        objReceivedFrom != null ? objReceivedFrom.Name : string.Empty,
                                                    ////ReceivedDate = DateTime.ParseExact(dataRow[12].ToString(), "dd/MM/yyyy", null),
                                                    Remark = dataRow[13].ToString(),
                                                    Well = dataRow[14].ToString(),
                                                    TransmittalNumber = dataRow[11].ToString()
                                                };

                                            if (!string.IsNullOrEmpty(dataRow[12].ToString()))
                                            {
                                                newObjDoc.ReceivedDate = DateTime.ParseExact(
                                                    dataRow[12].ToString(), "dd/MM/yyyy", null);
                                            }

                                            if (!string.IsNullOrEmpty(newObjDoc.RevisionName))
                                            {
                                                newObjDoc.RevisionFileName = newObjDoc.RevisionName + "_" + newObjDoc.Name;
                                            }
                                            else
                                            {
                                                newObjDoc.RevisionFileName = newObjDoc.Name;
                                            }


                                            newObjDoc.RevisionFilePath = newObjDoc.RevisionFilePath.Substring(
                                                0, newObjDoc.RevisionFilePath.LastIndexOf('/')) + "/"
                                                                         + DateTime.Now.ToString("ddMMyyhhmmss") + "_"
                                                                         + newObjDoc.RevisionFileName;

                                            // Allway check revision file is exist when update
                                            ////var filePath = Server.MapPath(newObjDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                                            ////var revisionFilePath = Server.MapPath(newObjDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                                            var filePath =
                                                Server.MapPath(
                                                    HostingEnvironment.ApplicationVirtualPath == "/"
                                                        ? newObjDoc.FilePath
                                                        : newObjDoc.FilePath.Replace(
                                                            "/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                                            var revisionFilePath =
                                                Server.MapPath(
                                                    HostingEnvironment.ApplicationVirtualPath == "/"
                                                        ? newObjDoc.RevisionFilePath
                                                        : newObjDoc.RevisionFilePath.Replace(
                                                            "/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                                            if (File.Exists(filePath) && !File.Exists(revisionFilePath))
                                            {
                                                File.Copy(filePath, revisionFilePath, true);
                                            }
                                            // End check

                                            if (currentRevision < newRevision)
                                            {
                                                objDoc.IsLeaf = false;
                                                newObjDoc.IsLeaf = true;
                                                if (objDoc.ParentID == null)
                                                {
                                                    newObjDoc.ParentID = objDoc.ID;
                                                }

                                                this.documentService.Update(objDoc);

                                                this.documentService.Insert(newObjDoc);
                                            }
                                            else
                                            {
                                                newObjDoc.IsLeaf = false;
                                                if (objDoc.ParentID == null)
                                                {
                                                    newObjDoc.ParentID = objDoc.ID;
                                                }
                                                ////filePath = Server.MapPath(newObjDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                                                ////revisionFilePath = Server.MapPath(objDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                                                filePath =
                                                    Server.MapPath(
                                                        HostingEnvironment.ApplicationVirtualPath == "/"
                                                            ? newObjDoc.FilePath
                                                            : newObjDoc.FilePath.Replace(
                                                                "/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                                                revisionFilePath =
                                                    Server.MapPath(
                                                        HostingEnvironment.ApplicationVirtualPath == "/"
                                                            ? objDoc.RevisionFilePath
                                                            : objDoc.RevisionFilePath.Replace(
                                                                "/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                                                if (Utility.ServiceIsAvailable("EDMSFolderWatcher"))
                                                {
                                                    watcherService.ExecuteCommand(128);
                                                }

                                                if (File.Exists(revisionFilePath))
                                                {
                                                    File.Copy(revisionFilePath, filePath, true);
                                                }

                                                if (Utility.ServiceIsAvailable("EDMSFolderWatcher"))
                                                {
                                                    watcherService.ExecuteCommand(129);
                                                }

                                                // IsDuplicate=true => allway insert new, IsDuplicate=false => recheck if have old rev -> Update, if not -> Insert new
                                                if (isDuplicate)
                                                {
                                                    this.documentService.Insert(newObjDoc);
                                                }
                                                else
                                                {
                                                    var oldRevToUpdate =
                                                        this.documentService.GetAllDocRevision(
                                                            newObjDoc.ParentID.GetValueOrDefault()).FirstOrDefault(t => t.RevisionID == newRevision);
                                                    if (oldRevToUpdate != null)
                                                    {
                                                        oldRevToUpdate.DocumentNumber = newObjDoc.DocumentNumber;
                                                        oldRevToUpdate.Title = newObjDoc.Title;
                                                        oldRevToUpdate.RevisionID = newObjDoc.RevisionID;
                                                        oldRevToUpdate.RevisionName = newObjDoc.RevisionName;
                                                        oldRevToUpdate.StatusID = newObjDoc.StatusID;
                                                        oldRevToUpdate.StatusName = newObjDoc.StatusName;
                                                        oldRevToUpdate.DisciplineID = newObjDoc.DisciplineID;
                                                        oldRevToUpdate.DisciplineName = newObjDoc.DisciplineName;
                                                        oldRevToUpdate.ReceivedFromID = newObjDoc.ReceivedFromID;
                                                        oldRevToUpdate.ReceivedFromName = newObjDoc.ReceivedFromName;
                                                        oldRevToUpdate.TransmittalNumber = newObjDoc.TransmittalNumber;
                                                        oldRevToUpdate.ReceivedDate = newObjDoc.ReceivedDate;
                                                        oldRevToUpdate.Remark = newObjDoc.Remark;
                                                        oldRevToUpdate.Well = newObjDoc.Well;
                                                        oldRevToUpdate.RevisionFilePath = newObjDoc.RevisionFilePath;
                                                        oldRevToUpdate.RevisionFileName = newObjDoc.RevisionFileName;

                                                        this.documentService.Update(oldRevToUpdate);
                                                    }
                                                    else
                                                    {
                                                        this.documentService.Insert(newObjDoc);
                                                    }
                                                }
                                            } 
                                        }
                                        else
                                        {
                                            // Allway check revision file is exist when update
                                            ////var filePath =
                                            ////    Server.MapPath(
                                            ////        objDoc.FilePath.Replace(
                                            ////            "/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                                            ////var revisionFilePath =
                                            ////    Server.MapPath(
                                            ////        objDoc.RevisionFilePath.Replace(
                                            ////            "/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                                            if (isDuplicate)
                                            {
                                                var newObjDoc = new Document
                                                    {
                                                        CategoryID = objDoc.CategoryID,
                                                        DocIndex = objDoc.DocIndex + 1,
                                                        ParentID = objDoc.ParentID,
                                                        FolderID = objDoc.FolderID,
                                                        DirName = objDoc.DirName,
                                                        FilePath = objDoc.FilePath,
                                                        RevisionFilePath = objDoc.RevisionFilePath,
                                                        FileExtension = objDoc.FileExtension,
                                                        FileExtensionIcon = objDoc.FileExtensionIcon,
                                                        FileNameOriginal = objDoc.FileNameOriginal,
                                                        KeyWords = objDoc.KeyWords,
                                                        IsDelete = false,
                                                        CreatedDate = DateTime.Now,
                                                        CreatedBy = UserSession.Current.User.Id,
                                                        Name = objDoc.Name,
                                                        DocumentNumber = dataRow[3].ToString(),
                                                        Title = dataRow[4].ToString(),
                                                        RevisionID = newRevision,
                                                        RevisionName = dataRow[5].ToString(),
                                                        StatusID = objStatus != null ? objStatus.ID : 0,
                                                        StatusName = objStatus != null ? objStatus.Name : string.Empty,
                                                        DisciplineID = objDiscipline != null ? objDiscipline.ID : 0,
                                                        DisciplineName = objDiscipline != null ? objDiscipline.Name : string.Empty,
                                                        DocumentTypeID = objDocumentType != null ? objDocumentType.ID : 0,
                                                        DocumentTypeName =
                                                            objDocumentType != null ? objDocumentType.Name : string.Empty,
                                                        ReceivedFromID = objReceivedFrom != null ? objReceivedFrom.ID : 0,
                                                        ReceivedFromName =
                                                            objReceivedFrom != null ? objReceivedFrom.Name : string.Empty,
                                                        ////ReceivedDate = DateTime.ParseExact(dataRow[12].ToString(), "dd/MM/yyyy", null),
                                                        Remark = dataRow[13].ToString(),
                                                        Well = dataRow[14].ToString(),
                                                        TransmittalNumber = dataRow[11].ToString()
                                                    };

                                                if (!string.IsNullOrEmpty(dataRow[12].ToString()))
                                                {
                                                    newObjDoc.ReceivedDate = DateTime.ParseExact(
                                                        dataRow[12].ToString(), "dd/MM/yyyy", null);
                                                }

                                                if (!string.IsNullOrEmpty(newObjDoc.RevisionName))
                                                {
                                                    newObjDoc.RevisionFileName = newObjDoc.RevisionName + "_" + newObjDoc.Name;
                                                }
                                                else
                                                {
                                                    newObjDoc.RevisionFileName = newObjDoc.Name;
                                                }


                                                newObjDoc.RevisionFilePath = newObjDoc.RevisionFilePath.Substring(
                                                    0, newObjDoc.RevisionFilePath.LastIndexOf('/')) + "/"
                                                                             + DateTime.Now.ToString("ddMMyyhhmmss") + "_"
                                                                             + newObjDoc.RevisionFileName;

                                                // Allway check revision file is exist when update
                                                ////var filePath = Server.MapPath(newObjDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                                                ////var revisionFilePath = Server.MapPath(newObjDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                                                var filePath =
                                                    Server.MapPath(
                                                        HostingEnvironment.ApplicationVirtualPath == "/"
                                                            ? newObjDoc.FilePath
                                                            : newObjDoc.FilePath.Replace(
                                                                "/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                                                var revisionFilePath =
                                                    Server.MapPath(
                                                        HostingEnvironment.ApplicationVirtualPath == "/"
                                                            ? newObjDoc.RevisionFilePath
                                                            : newObjDoc.RevisionFilePath.Replace(
                                                                "/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                                                if (File.Exists(filePath) && !File.Exists(revisionFilePath))
                                                {
                                                    File.Copy(filePath, revisionFilePath, true);
                                                }

                                                objDoc.IsLeaf = false;
                                                newObjDoc.IsLeaf = true;
                                                if (objDoc.ParentID == null)
                                                {
                                                    newObjDoc.ParentID = objDoc.ID;
                                                }

                                                this.documentService.Update(objDoc);
                                                this.documentService.Insert(newObjDoc);
                                            }
                                            else
                                            {
                                                var filePath =
                                                    Server.MapPath(
                                                        HostingEnvironment.ApplicationVirtualPath == "/"
                                                            ? objDoc.FilePath
                                                            : objDoc.FilePath.Replace(
                                                                "/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                                                var revisionFilePath =
                                                    Server.MapPath(
                                                        HostingEnvironment.ApplicationVirtualPath == "/"
                                                            ? objDoc.RevisionFilePath
                                                            : objDoc.RevisionFilePath.Replace(
                                                                "/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                                                if (File.Exists(filePath) && !File.Exists(revisionFilePath))
                                                {
                                                    File.Copy(filePath, revisionFilePath, true);
                                                }

                                                ////objDoc.Name = dataRow[1].ToString();
                                                objDoc.DocumentNumber = dataRow[3].ToString();
                                                objDoc.Title = dataRow[4].ToString();
                                                objDoc.RevisionID = objRev != null ? objRev.ID : 0;
                                                objDoc.RevisionName = objRev != null ? objRev.Name: string.Empty;
                                                objDoc.StatusID = objStatus != null ? objStatus.ID : 0;
                                                objDoc.StatusName = objStatus != null ? objStatus.Name : string.Empty;
                                                objDoc.DisciplineID = objDiscipline != null ? objDiscipline.ID : 0;
                                                objDoc.DisciplineName = objDiscipline != null
                                                                            ? objDiscipline.Name
                                                                            : string.Empty;
                                                objDoc.DocumentTypeID = objDocumentType != null ? objDocumentType.ID : 0;
                                                objDoc.DocumentTypeName = objDocumentType != null
                                                                              ? objDocumentType.Name
                                                                              : string.Empty;
                                                objDoc.ReceivedFromID = objReceivedFrom != null ? objReceivedFrom.ID : 0;
                                                objDoc.ReceivedFromName = objReceivedFrom != null
                                                                              ? objReceivedFrom.Name
                                                                              : string.Empty;
                                                ////objDoc.ReceivedDate = DateTime.ParseExact(dataRow[10].ToString(), "dd/MM/yyyy", null);
                                                objDoc.Remark = dataRow[13].ToString();
                                                objDoc.Well = dataRow[14].ToString();
                                                objDoc.TransmittalNumber = dataRow[11].ToString();
                                                if (!string.IsNullOrEmpty(objDoc.RevisionName))
                                                {
                                                    objDoc.RevisionFileName = objDoc.RevisionName + "_" + objDoc.Name;
                                                }
                                                else
                                                {
                                                    objDoc.RevisionFileName = objDoc.Name;
                                                }

                                                if (!string.IsNullOrEmpty(dataRow[12].ToString()))
                                                {
                                                    objDoc.ReceivedDate = DateTime.ParseExact(
                                                        dataRow[12].ToString(), "dd/MM/yyyy", null);
                                                }

                                                objDoc.LastUpdatedBy = UserSession.Current.User.Id;
                                                objDoc.LastUpdatedDate = DateTime.Now;

                                                this.documentService.Update(objDoc);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var watcherService = new ServiceController("EDMSFolderWatcher");
                    if (Utility.ServiceIsAvailable("EDMSFolderWatcher"))
                    {
                        watcherService.ExecuteCommand(129);
                    }
                }
            }
            

            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The save upload file.
        /// </summary>
        /// <param name="uploadDocControl">
        /// The upload doc control.
        /// </param>
        /// <param name="objDoc">
        /// The obj Doc.
        /// </param>
        private void SaveUploadFile(RadAsyncUpload uploadDocControl, ref Document objDoc)
        {
            var listUpload = uploadDocControl.UploadedFiles;
            if (listUpload.Count > 0)
            {
                foreach (UploadedFile docFile in listUpload)
                {
                    var revisionFilePath = Server.MapPath(objDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                    docFile.SaveAs(revisionFilePath, true);
                }
            }
        }
    }
}