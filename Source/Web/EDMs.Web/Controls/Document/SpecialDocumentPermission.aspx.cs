﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web.UI;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class SpecialDocumentPermission : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly GroupDataPermissionService groupDataPermissionService;

        /// <summary>
        /// The user data permission.
        /// </summary>
        private readonly UserDataPermissionService userDataPermissionService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly FolderService folderService;

        /// <summary>
        /// The document service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The role service.
        /// </summary>
        private readonly RoleService roleService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        /// <summary>
        /// The service name.
        /// </summary>
        protected const string ServiceName = "EDMSFolderWatcher";

        private readonly SpecialDocPermissionService specialDocPermissionService;

        private List<int> AdminGroup
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("GroupAdminList").Split(',').Select(t => Convert.ToInt32(t)).ToList();
            }
        } 


        /// <summary>
        /// Initializes a new instance of the <see cref="SpecialDocumentPermission"/> class.
        /// </summary>
        public SpecialDocumentPermission()
        {
            this.groupDataPermissionService = new GroupDataPermissionService();
            this.folderService = new FolderService();
            this.documentService = new DocumentService();
            this.roleService = new RoleService();
            this.userService = new UserService();
            this.userDataPermissionService = new UserDataPermissionService();
            this.specialDocPermissionService = new SpecialDocPermissionService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
                {
                    this.lblFolderDirName.Text = "<font style='text-decoration: underline;'>Document</font>: ";
                    var docId = Request.QueryString["docId"];
                    var docObj = this.documentService.GetById(Convert.ToInt32(docId));
                    if (docObj != null)
                    {
                        this.lblFolderDirName.Text += "<b>" + docObj.DocumentNumber + "</b> (" + docObj.Title + ")";
                        this.LoadComboData(docObj);
                        //this.LoadPermissionData(folderId.Trim());
                    }
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var selectedDocId = Request.QueryString["docId"];
                var selectedDoc = this.documentService.GetById(Convert.ToInt32(selectedDocId));
                if (selectedDoc != null)
                {
                    foreach (RadListBoxItem user in this.ddlUser.SelectedItems)
                    {
                        var addingPermission = new SpecialDocPermission()
                        {
                            DocId = selectedDoc.ID,
                            UserId = Convert.ToInt32(user.Value),
                            FolderId = selectedDoc.FolderID
                        };

                        this.specialDocPermissionService.Insert(addingPermission);
                    }

                    if (this.ddlUser.SelectedItems.Count > 0)
                    {
                        selectedDoc.HasSpecialPermission = true;
                        this.documentService.Update(selectedDoc);
                    }

                    var listUser = new List<User>();
                    var usersInPermission = this.specialDocPermissionService.GetAllByDocId(Convert.ToInt32(Request.QueryString["docId"])).Select(t => t.UserId).Distinct().ToList();
                    if (this.ddlGroup.SelectedValue == "0")
                    {
                        var userIds = this.userDataPermissionService.GetAllByFolder(Convert.ToInt32(Request.QueryString["folderId"])).Select(t => t.UserId).Distinct().Where(t => !usersInPermission.Contains(t)).ToList();
                        listUser = this.userService.GetAll().Where(t => userIds.Contains(t.Id)).ToList();
                    }
                    else
                    {
                        listUser = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroup.SelectedValue)).Where(t => !usersInPermission.Contains(t.Id)).ToList();
                        //listUser.Insert(0, new User { Id = 0, FullName = string.Empty });
                    }

                    this.ddlUser.DataSource = listUser.OrderBy(t => t.FullName);
                    this.ddlUser.DataTextField = "FullName";
                    this.ddlUser.DataValueField = "Id";
                    this.ddlUser.DataBind();

                    this.grdPermission.Rebind();
                }
            }
        }

        private void LoadComboData(Document docObj)
        {
            var groupInPermission = this.groupDataPermissionService.GetAllByFolder(docObj.FolderID.ToString()).Select(t => t.RoleId).ToList();

            var listGroup = this.roleService.GetAll().Where(t => t.Id != 1 && groupInPermission.Contains(t.Id)).ToList();
            listGroup.Insert(0, new Role {Id = 0});
            this.ddlGroup.DataSource = listGroup;
            this.ddlGroup.DataTextField = "Name";
            this.ddlGroup.DataValueField = "Id";
            this.ddlGroup.DataBind();

            var usersInPermission = this.specialDocPermissionService.GetAllByDocId(Convert.ToInt32(Request.QueryString["docId"])).Select(t => t.UserId).Distinct().ToList();
            //var listUser = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroup.SelectedValue)).Where(t => !usersInPermission.Contains(t.Id)).ToList();

            var userIds = this.userDataPermissionService.GetAllByFolder(Convert.ToInt32(Request.QueryString["folderId"])).Select(t => t.UserId).Distinct().Where(t => !usersInPermission.Contains(t)).ToList();
            var listUser = this.userService.GetAll().Where(t => userIds.Contains(t.Id)).ToList();

            //listUser.Insert(0, new User { Id = 0, FullName = string.Empty });

            this.ddlUser.DataSource = listUser.OrderBy(t => t.FullName);
            this.ddlUser.DataTextField = "FullName";
            this.ddlUser.DataValueField = "Id";
            this.ddlUser.DataBind();
        }

        protected void grdPermission_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var usersInPermission = this.specialDocPermissionService.GetAllByDocId(Convert.ToInt32(Request.QueryString["docId"]));

                var listDataPermission = usersInPermission
                                        .Select(t => new DataPermission
                                                { 
                                                    ID = t.ID,
                                                    Name = t.UserObj.FullName, 
                                                    IsGroup = false 
                                                }).ToList();

                this.grdPermission.DataSource = listDataPermission;
            }
            else
            {
                this.grdPermission.DataSource = new List<DataPermission>();    
            }
        }

        protected void ddlGroup_SelectedIndexChange(object sender, EventArgs e)
        {
            var listUser = new List<User>();
            var usersInPermission = this.specialDocPermissionService.GetAllByDocId(Convert.ToInt32(Request.QueryString["docId"])).Select(t => t.UserId).Distinct().ToList();
            if (this.ddlGroup.SelectedValue == "0")
            {
                var userIds = this.userDataPermissionService.GetAllByFolder(Convert.ToInt32(Request.QueryString["folderId"])).Select(t => t.UserId).Distinct().Where(t => !usersInPermission.Contains(t)).ToList();
                listUser = this.userService.GetAll().Where(t => userIds.Contains(t.Id)).ToList();
            }
            else
            {
                listUser = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroup.SelectedValue)).Where(t => !usersInPermission.Contains(t.Id)).ToList();
                //listUser.Insert(0, new User { Id = 0, FullName = string.Empty });
            }

            this.ddlUser.DataSource = listUser.OrderBy(t => t.FullName);
            this.ddlUser.DataTextField = "FullName";
            this.ddlUser.DataValueField = "Id";
            this.ddlUser.DataBind();
        }

        protected void grdPermission_OnDeteleCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var permissionId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var isGroup = Convert.ToBoolean(item["IsGroup"].Text);

            if (isGroup)
            {
            }
            else
            {
                var perObj = this.specialDocPermissionService.GetById(permissionId);
                var docId = perObj.DocId;
                this.specialDocPermissionService.Delete(permissionId);

                var listPer = this.specialDocPermissionService.GetAllByDocId(docId.GetValueOrDefault());
                if (listPer == null || listPer.Count == 0)
                {
                    var docObj = this.documentService.GetById(perObj.DocId.GetValueOrDefault());
                    docObj.HasSpecialPermission = false;

                    this.documentService.Update(docObj);
                }
            }

            var usersInPermission = this.specialDocPermissionService.GetAllByDocId(Convert.ToInt32(Request.QueryString["docId"])).Select(t => t.UserId).Distinct().ToList();
            var listUser = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroup.SelectedValue)).Where(t => !usersInPermission.Contains(t.Id)).ToList();

            //listUser.Insert(0, new User { Id = 0, FullName = string.Empty });

            this.ddlUser.DataSource = listUser;
            this.ddlUser.DataTextField = "FullName";
            this.ddlUser.DataValueField = "Id";
            this.ddlUser.DataBind();
        }
    }
}