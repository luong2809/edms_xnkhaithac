﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.ServiceProcess;
using EDMs.Web.Utilities;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;
    using Aspose.Cells;
    using System.Data;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class UploadMetadata : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        protected const string ServiceName = "EDMSFolderWatcher";

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public UploadMetadata()
        {
            this.documentService = new DocumentService();
        }

        /// <summary>
        /// Validation existing patient code
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        protected void ValidatePatientCode(object source, ServerValidateEventArgs arguments)
        {
        }


        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var objDoc = this.documentService.GetById(Convert.ToInt32(this.Request.QueryString["docId"]));
                    if (objDoc != null)
                    {

                    }
                }
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Document objDoc;
            if (this.Page.IsValid)
            {
                var listUpload = docuploader.UploadedFiles;
                if (listUpload.Count > 0)
                {
                    foreach (UploadedFile docFile in listUpload)
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") +
                                         "_" + docFile.FileName;
                        docFile.SaveAs(importPath);
                        var workbook = new Workbook();
                        workbook.Open(importPath);
                        var wsData = workbook.Worksheets[0];
                        var totalDataTable = new DataTable();
                        var dataTable = new DataTable();
                        var strError = "";
                        // Export worksheet data to a DataTable object by calling either ExportDataTable or ExportDataTableAsString method of the Cells class	
                        totalDataTable = wsData.Cells.ExportDataTable(1, 2, wsData.Cells.MaxDataRow, 25);
                        foreach (DataRow dtRow in totalDataTable.Rows)
                        {
                            if (!string.IsNullOrEmpty(dtRow["Column1"].ToString()))
                            {
                                var docObj = this.documentService.GetByFileName(dtRow["Column1"].ToString().Trim());
                                if (docObj != null)
                                {
                                    docObj.DocumentNumber = dtRow["Column3"].ToString().Trim();
                                    docObj.Title = dtRow["Column4"].ToString().Trim();
                                    docObj.DocumentTypeName = dtRow["Column5"].ToString().Trim();
                                    if(!string.IsNullOrEmpty(dtRow["Column8"].ToString().Trim()))
                                    {
                                        docObj.DisciplineID = Convert.ToInt32(dtRow["Column8"].ToString().Trim());
                                    }
                                    
                                    if (strError == "")
                                    {
                                        this.documentService.Update(docObj);
                                    }
                                }
                            }
                        }
                        if (strError != "")
                        {
                            lblError.Visible = true;
                            lblError.Text = strError;
                        }
                    }
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The save upload file.
        /// </summary>
        /// <param name="uploadDocControl">
        /// The upload doc control.
        /// </param>
        /// <param name="objDoc">
        /// The obj Doc.
        /// </param>
        private void SaveUploadFile(RadAsyncUpload uploadDocControl, ref Document objDoc)
        {
            var listUpload = uploadDocControl.UploadedFiles;
            if (listUpload.Count > 0)
            {
                foreach (UploadedFile docFile in listUpload)
                {
                    ////var revisionFilePath = Server.MapPath(objDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                    if (!objDoc.IsLeaf.GetValueOrDefault())
                    {
                        var revisionFilePath =
                            Server.MapPath(HostingEnvironment.ApplicationVirtualPath == "/"
                                ? objDoc.RevisionFilePath
                                : objDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath,
                                    "../.."));

                        docFile.SaveAs(revisionFilePath, true);
                    }
                    else
                    {
                        var filePath =
                           Server.MapPath(HostingEnvironment.ApplicationVirtualPath == "/"
                               ? objDoc.FilePath
                               : objDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath,
                                   "../.."));
                        var watcherService = new ServiceController(ServiceName);
                        if (Utility.ServiceIsAvailable(ServiceName))
                        {
                            watcherService.ExecuteCommand(128);
                        }

                        docFile.SaveAs(filePath, true);

                        if (Utility.ServiceIsAvailable(ServiceName))
                        {
                            watcherService.ExecuteCommand(129);
                        }
                    }

                }
            }
        }
    }
}