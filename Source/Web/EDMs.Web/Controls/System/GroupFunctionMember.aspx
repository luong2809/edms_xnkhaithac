﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GroupFunctionMember.aspx.cs" Inherits="EDMs.Web.GroupFunctionMember" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../../Content/styles.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    <style type="text/css">
        #emailValidator
        {
            display: inline-grid !important;
        }
        span.valerror[style*="inline"]
        {
            display: inline-grid !important
        }
         .rgMasterTable {
            table-layout: auto;
        }
         .RadGrid .rgRow td, .RadGrid .rgAltRow td, .RadGrid .rgEditRow td, .RadGrid .rgFooter td, .RadGrid .rgFilterRow td, .RadGrid .rgHeader, .RadGrid .rgResizeCol, .RadGrid .rgGroupHeader td {
            padding-left: 1px !important;
            padding-right: 1px !important;
        }

        /*Hide change page size control*/
        div.RadGrid .rgPager .rgAdvPart     
        {     
        display:none;        
        }    
        #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_grdUsersPanel, #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_divContainerPanel
        {
            height: 100% !important;
        }

        #ctl00_ContentPlaceHolder2_RadPageView1, #ctl00_ContentPlaceHolder2_RadPageView2,
        #ctl00_ContentPlaceHolder2_RadPageView3, #ctl00_ContentPlaceHolder2_RadPageView4,
        #ctl00_ContentPlaceHolder2_RadPageView5
        {
            height: 100% !important;
        }

</style>

    <script type="text/javascript">
        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refreshGrid(args);
            GetRadWindow().close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CancelEdit() {
            GetRadWindow().close();
        }
    </script>
    <title></title>
</head>
<body>
    <form id="frmRoleEdit" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
         <div style="width: 100%">
            <ul style="list-style-type: none">
                <div class="qlcbFormItem">
                    <div class="dnnFormMessage dnnFormInfo">
                        <div class="dnnFormItem dnnFormHelp dnnClear">
                            <p class="dnnFormRequired" style="float: left;">
                                <span style="text-decoration: underline;">Notes</span>: All fields marked with a red are required.
                            </p>
                            <br />
                        </div>
                    </div>
                </div>
            </ul>
        </div>
        <div style="width: 100%" runat="server" ID="divContent">
          <telerik:RadGrid ID="grdUsers"
    AllowCustomPaging="True"
    runat="server"
    AllowPaging="True"
    AutoGenerateColumns="False"
   AllowMultiRowSelection="true"
 AllowFilteringByColumn="false"
    CellSpacing="0"
    CellPadding="0" Skin="Windows7"
    PageSize="1000" Height="550"
    GridLines="None" 
    OnNeedDataSource="grdUsers_NeedDataSource"
    OnItemDataBound="grdUsers_ItemDataBound"
    Style="outline: none">
    <MasterTableView DataKeyNames="Id" ClientDataKeyNames="Id" CommandItemDisplay="Top" Width="100%">
         <CommandItemSettings  ShowAddNewRecordButton="false" RefreshText="Refresh Data" ShowExportToExcelButton="false"/>
        <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; Users." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
        <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
        <Columns>
            <telerik:GridBoundColumn DataField="Id" HeaderText="Mã" UniqueName="Id" Visible="False" />
            <telerik:GridBoundColumn DataField="User.Id" HeaderText="Mã người dùng" UniqueName="colUserId" Visible="False" />
              <telerik:GridClientSelectColumn UniqueName="IsSelected">
                                            <HeaderStyle Width="40" />
                                            <ItemStyle HorizontalAlign="Center" Width="40" />
                                        </telerik:GridClientSelectColumn>
            <telerik:GridBoundColumn DataField="Username" HeaderText="User name" UniqueName="colUsername" 
                FilterControlWidth="100%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" >
                <HeaderStyle HorizontalAlign="Center"  Width="30%" />
                <ItemStyle HorizontalAlign="Left" Width="30%"/>
            </telerik:GridBoundColumn>
            
            <telerik:GridBoundColumn DataField="FullName" HeaderText="Full name" UniqueName="colNameFullName" 
                FilterControlWidth="100%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                <HeaderStyle HorizontalAlign="Center"  Width="60%" />
                <ItemStyle HorizontalAlign="Left" Width="60%"/>
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
    <ClientSettings>
        <Selecting AllowRowSelect="true"></Selecting>
        <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" ScrollHeight="500"></Scrolling>
    </ClientSettings>
</telerik:RadGrid>
        </div>

        <table width="100%">
            <tr>
                <td colspan="2" align="center">
                    <telerik:RadButton ID="btnCapNhat" runat="server" Text="Save" OnClick="btnCapNhat_Click" ValidationGroup="grpRole">
                        <Icon PrimaryIconUrl="../../Images/save.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>
                    <telerik:RadButton ID="btncancel" runat="server" Text="Cancel"
                        OnClick="btncancel_Click">
                        <Icon PrimaryIconUrl="../../Images/Cancel.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>
                </td>
            </tr>
        </table>
        <asp:HiddenField runat="server" ID="LbListIDMember" />
          <telerik:RadAjaxManager runat="Server" ID="ajaxDocument">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ajaxDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContent"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
         <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script src="Scripts/jquery-1.7.1.js"></script>
        <script type="text/javascript">

            var radDocuments;
            var ajaxManager;
            function refreshGrid() {
                var masterTable = $find("<%=grdUsers.ClientID%>").get_masterTableView();
                masterTable.rebind();
            }

            function refreshGrid(arg) {
                grdUsers.get_masterTableView().rebind();
            }
            function pageLoad() {
                ajaxManager = $find("<%=ajaxDocument.ClientID %>");
              }
            function GetGridObject(sender, eventArgs) {
                radDocuments = sender;
            }
   </script>
    </telerik:RadCodeBlock>
    </form>
</body>
</html>
