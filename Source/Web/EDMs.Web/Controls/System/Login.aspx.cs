﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;
using EDMs.Business.Services;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using System.Security;
using System.Security.Principal;
namespace EDMs.Web
{
    public partial class Login : Page
    {
        #region Fields
        private readonly UserService _userService;
        #endregion

        public Login()
        {
            _userService = new UserService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            txtUsername.Focus();
            //if (!IsPostBack)
            //{
            //    try
            //    {
            //        string strName = HttpContext.Current.User.Identity.Name.ToString();
            //        txtUsername.Text = strName.Replace(@"\", "#").Split('#')[1];
            //        var ussername = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
            //        IIdentity WinId = HttpContext.Current.User.Identity;
            //        WindowsIdentity wi = (WindowsIdentity)WinId;
            //        WindowsImpersonationContext wic = wi.Impersonate();
            //        try
            //        {
            //            // Access resources while impersonating.
            //            var user = _userService.GetUserByUsername(System.Environment.UserName.ToString());
            //            if (user != null)
            //            {
            //                UserSession.CreateSession(user);
            //                FormsAuthentication.RedirectFromLoginPage(user.Username, false);

            //                if (Session["ReturnURL"] != null)
            //                {
            //                    var returnUrl = Session["ReturnURL"].ToString();
            //                    Session.Remove("ReturnURL");
            //                    Response.Redirect("~" + returnUrl);
            //                }
            //                else
            //                {
            //                    Response.Redirect("~/Default.aspx");
            //                }
            //            }
            //            else
            //            {
            //                lblMessage.Text = "Username does not exist. ";
            //            }

            //        }
            //        catch
            //        {
            //            // Prevent exceptions propagating.
            //        }
            //        finally
            //        {
            //            // Revert impersonation.
            //            wic.Undo();
            //        }
            //    }
            //    catch { }

            //}
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string username = txtUsername.Text;
            string password = txtPassword.Text;
            var user = _userService.GetUserByUsername(username);
            if (ConfigurationManager.AppSettings["EnableLDAP"] == "true")
            {
                if (username == "admin")
                {
                    if (user != null && user.Password == Utility.GetMd5Hash(password))
                    {
                        UserSession.CreateSession(user);
                        FormsAuthentication.RedirectFromLoginPage(user.Username, false);

                        if (Session["ReturnURL"] != null)
                        {
                            var returnUrl = Session["ReturnURL"].ToString();
                            Session.Remove("ReturnURL");
                            Response.Redirect("~" + returnUrl);
                        }
                        else
                        {
                            Response.Redirect("~/Default.aspx");
                        }
                    }
                    else
                    {
                        lblMessage.Text = "Username or password is incorrect. ";
                    }
                }
                else
                {
                    var adPath = ConfigurationManager.AppSettings["LDAPPath"];
                    var domain = ConfigurationManager.AppSettings["Domain"];
                    var adAuth = new LdapAuthentication(adPath);
                    if (adAuth.IsAuthenticated(domain, username, password))
                    {
                        if (user != null)
                        {
                            UserSession.CreateSession(user);
                            FormsAuthentication.RedirectFromLoginPage(user.Username, false);

                            if (Session["ReturnURL"] != null)
                            {
                                var returnUrl = Session["ReturnURL"].ToString();
                                Session.Remove("ReturnURL");
                                Response.Redirect("~" + returnUrl);
                            }
                            else
                            {
                                Response.Redirect("~/Default.aspx");
                            }
                        }
                        else
                        {
                            var newUser = new User()
                            {
                                Username = username,
                                Password = Utility.GetMd5Hash(password),
                                FullName = adAuth.FilterAttribute,
                                Email = username + "@" + domain
                            };

                            var isSuccess = this._userService.Insert(newUser);
                            if (isSuccess)
                            {
                                UserSession.CreateSession(newUser);
                                Response.Redirect("~/Default.aspx");
                            }
                        }
                    }
                    else
                    {
                        lblMessage.Text = "Username or password is incorrect. ";
                    }
                }
            }
            else
            {
                if (user != null && user.Password == Utility.GetMd5Hash(password))
                {
                    UserSession.CreateSession(user);
                    FormsAuthentication.RedirectFromLoginPage(user.Username, false);

                    if (Session["ReturnURL"] != null)
                    {
                        var returnUrl = Session["ReturnURL"].ToString();
                        Session.Remove("ReturnURL");
                        Response.Redirect("~" + returnUrl);
                    }
                    else
                    {
                        Response.Redirect("~/Default.aspx");
                    }
                }
                else
                {
                    lblMessage.Text = "Username or password is incorrect. ";
                }
            }


        }
    }
}