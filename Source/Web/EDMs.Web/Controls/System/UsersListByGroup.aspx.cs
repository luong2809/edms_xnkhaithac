﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.ServiceProcess;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    using ZetaLongPaths;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class UsersListByGroup : Page
    {
        /// <summary>
        /// The document service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly StatusService statusService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        private readonly UserService userService;

        private readonly ResourceService resourceService;

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService;

        protected const string ServiceName = "EDMSFolderWatcher";

        /// <summary>
        /// Initializes a new instance of the <see cref="UsersListByGroup"/> class.
        /// </summary>
        public UsersListByGroup()
        {
            this.documentService = new DocumentService();
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.statusService = new StatusService();
            this.disciplineService = new DisciplineService();
            this.receivedFromService = new ReceivedFromService();
            this.userService = new UserService();
            this.resourceService = new ResourceService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                try
                {
                    if (!UserSession.Current.User.Role.Active.GetValueOrDefault())
                    {
                        ////this.grdDocument.MasterTableView.GetColumn("EditColumn").Display = false;
                        ////this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Display = false;
                        ////this.grdDocument.MasterTableView.GetColumn("ReUpload").Display = false;
                        ////this.grdDocument.MasterTableView.GetColumn("IsLeaf").Display = false;
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["groupId"]))
                    {
                        this.LoadUserData(Convert.ToInt32(Request.QueryString["groupId"]));
                    }
                }
                catch (Exception)
                {
                    UserSession.DestroySession();
                    Response.Redirect("~/Controls/System/Login.aspx");
                }
            }
        }

        private void LoadUserData(int roleId)
        {
            var listUserIdInGroup = this.userService.GetAllByRoleId(roleId).Select(t => t.Id).ToList();

            var listUserAvailable = this.userService.GetAll().Where(t => !listUserIdInGroup.Contains(t.Id)).OrderBy(t => t.Username).ToList();
            listUserAvailable.Insert(0, new User { Id = 0 });
            this.ddlUser.DataSource = listUserAvailable;
            this.ddlUser.DataValueField = "Id";
            this.ddlUser.DataTextField = "UserNameFullName";
            this.ddlUser.DataBind();
        }

        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (Request.QueryString["groupId"] != null)
            {
                var roleId = Convert.ToInt32(Request.QueryString["groupId"]);
                var listUser = this.userService.GetAllByRoleId(roleId).OrderBy(t => t.Username);
                this.grdDocument.DataSource = listUser;
            }
            else
            {
                this.grdDocument.DataSource = new List<User>();
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                grdDocument.MasterTableView.SortExpressions.Clear();
                grdDocument.MasterTableView.GroupByExpressions.Clear();
                grdDocument.Rebind();
            }
        }

        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var userId = Convert.ToInt32(item.GetDataKeyValue("Id").ToString());
            var objUser = this.userService.GetByID(userId);
            try
            {
                if (objUser != null)
                {
                    var roleId = objUser.RoleId.GetValueOrDefault();
                    objUser.RoleId = 0;
                    this.userService.Update(objUser);

                    this.LoadUserData(roleId);
                    this.grdDocument.Rebind();
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// The grd document_ selected index changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridDataItem item = (GridDataItem)grdDocument.SelectedItems[0];
            if (item != null)
            {
                int docId = int.Parse(item["ID"].Text);
                ////Patient kh = patientService.GetByID(customerID);

                //Bind to controls
                ////lblCustomerID.Value = customerID.ToString();
                ////lblTenKhachHang.Text = kh.FullName;
                ////lblMaKH.Text = kh.SSN;
                ////lblNgheNghiep.Text = kh.Occupation;
                ////lblDiaChi.Text = kh.Address1;
                ////lblDienThoai.Text = kh.CellPhone;
                ////lblEmail.Text = kh.Email;
                ////lblCMND.Text = kh.IdentityCard;
                ////lblNgayCap.Text = "N/A";
                ////lblNoiCap.Text = "N/A";
                ////lblTinhTrang.Text = kh.PatientStatus.Name;
                //////Ghi chu cuoi cung cua Customer
                ////Patient_DescriptionService descriptionService = new Patient_DescriptionService();
                ////Description ds = descriptionService.Find(x => x.CustomerID.Value == customerID).OrderByDescending(x => x.ID).FirstOrDefault();
                ////lblGhiChu.Text = ds==null?"N/A":ds.Content;
                ////if (lblGhiChu.Text != "N/A")
                ////{
                ////    lblGhiChu.ForeColor = System.Drawing.Color.Red;
                ////    lblGhiChu.Font.Bold = true;
                ////}
                ////else
                ////{
                ////    lblGhiChu.ForeColor = System.Drawing.Color.Black;
                ////    lblGhiChu.Font.Bold = false;
                ////}
                //End Ghi chu
            }
        }

        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (!e.Item.IsInEditMode && e.Item is GridDataItem)
            {
                ////var item = e.Item as GridDataItem;
                ////if (DataBinder.Eval(item.DataItem, "ParentId") == null)
                ////{
                ////    item["DeleteColumn"].Controls[0].Visible = false;
                ////}
            }
            else if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                var item = e.Item as GridEditableItem;
                var txtRevisionName = item.FindControl("txtRevisionName") as Label;
                var ddlStatus = item.FindControl("ddlStatus") as RadComboBox;
                var ddlDiscipline = item.FindControl("ddlDiscipline") as RadComboBox;
                var ddlDocumentType = item.FindControl("ddlDocumentType") as RadComboBox;
                var ddlReceivedFrom = item.FindControl("ddlReceivedFrom") as RadComboBox;
                var txtReceivedDate = item.FindControl("txtReceivedDate") as RadDatePicker;
                var txtRevisionFileName = item.FindControl("txtRevisionFileName") as TextBox;
                var txtDocumentNumber = item.FindControl("txtDocumentNumber") as TextBox;
                var txtTitle = item.FindControl("txtTitle") as TextBox;
                var txtRemark = item.FindControl("txtRemark") as TextBox;
                var txtWell = item.FindControl("txtWell") as TextBox;
                var txtTransmittalNumber = item.FindControl("txtTransmittalNumber") as TextBox;

                txtReceivedDate.DatePopupButton.Visible = false;

                var revisionFileName = (item.FindControl("RevisionFileName") as HiddenField).Value;
                var revisionName = (item.FindControl("RevisionName") as HiddenField).Value;
                var statusId = (item.FindControl("StatusID") as HiddenField).Value;
                var disciplineId = (item.FindControl("DisciplineID") as HiddenField).Value;
                var documentTypeId = (item.FindControl("DocumentTypeID") as HiddenField).Value;
                var receivedFromId = (item.FindControl("ReceivedFromID") as HiddenField).Value;
                var receivedDate = (item.FindControl("ReceivedDate") as HiddenField).Value;
                var remark = (item.FindControl("Remark") as HiddenField).Value;
                var well = (item.FindControl("Well") as HiddenField).Value;
                var title = (item.FindControl("Title") as HiddenField).Value;
                var documentNumber = (item.FindControl("DocumentNumber") as HiddenField).Value;
                var transmittalNumber = (item.FindControl("TransmittalNumber") as HiddenField).Value;

                if (!string.IsNullOrEmpty(receivedDate))
                {
                    txtReceivedDate.SelectedDate = Convert.ToDateTime(receivedDate);
                }

                txtRevisionFileName.Text = revisionFileName;
                txtTitle.Text = title;
                txtRemark.Text = remark;
                txtWell.Text = well;
                txtDocumentNumber.Text = documentNumber;
                txtTransmittalNumber.Text = transmittalNumber;

                txtRevisionName.Text = revisionName;

                ////var revisionList = this.revisionService.GetAll();
                ////revisionList.Insert(0, new Revision() { Name = string.Empty });
                ////ddlRevision.DataSource = revisionList;
                ////ddlRevision.DataValueField = "ID";
                ////ddlRevision.DataTextField = "Name";
                ////ddlRevision.DataBind();
                ////ddlRevision.SelectedValue = revisionId;

                var categoryId = 0;

                if (!string.IsNullOrEmpty(this.Request.QueryString["categoryId"]))
                {
                    categoryId = Convert.ToInt32(this.Request.QueryString["categoryId"]);
                }

                var documentTypeList = this.documentTypeService.GetAllByCategory(categoryId);
                documentTypeList.Insert(0, new DocumentType() { Name = string.Empty });
                ddlDocumentType.DataSource = documentTypeList;
                ddlDocumentType.DataValueField = "ID";
                ddlDocumentType.DataTextField = "Name";
                ddlDocumentType.DataBind();
                ddlDocumentType.SelectedValue = documentTypeId;

                var statusList = this.statusService.GetAllByCategory(categoryId);
                statusList.Insert(0, new Status { Name = string.Empty });
                ddlStatus.DataSource = statusList;
                ddlStatus.DataValueField = "ID";
                ddlStatus.DataTextField = "Name";
                ddlStatus.DataBind();
                ddlStatus.SelectedValue = statusId;

                var receivedFromList = this.receivedFromService.GetAllByCategory(categoryId);
                receivedFromList.Insert(0, new ReceivedFrom() { Name = string.Empty });
                ddlReceivedFrom.DataSource = receivedFromList;
                ddlReceivedFrom.DataValueField = "ID";
                ddlReceivedFrom.DataTextField = "Name";
                ddlReceivedFrom.DataBind();
                ddlReceivedFrom.SelectedValue = receivedFromId;

                var disciplineList = this.disciplineService.GetAllByCategory(categoryId);
                disciplineList.Insert(0, new Discipline() { Name = string.Empty });
                ddlDiscipline.DataSource = disciplineList;
                ddlDiscipline.DataValueField = "ID";
                ddlDiscipline.DataTextField = "Name";
                ddlDiscipline.DataBind();
                ddlDiscipline.SelectedValue = disciplineId;
            }
        }

        protected void grdDocument_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                var item = e.Item as GridEditableItem;
                var ddlStatus = item.FindControl("ddlStatus") as RadComboBox;
                var ddlDiscipline = item.FindControl("ddlDiscipline") as RadComboBox;
                var ddlDocumentType = item.FindControl("ddlDocumentType") as RadComboBox;
                var ddlReceivedFrom = item.FindControl("ddlReceivedFrom") as RadComboBox;
                var txtReceivedDate = item.FindControl("txtReceivedDate") as RadDatePicker;
                var txtRevisionFileName = item.FindControl("txtRevisionFileName") as TextBox;
                var txtDocumentNumber = item.FindControl("txtDocumentNumber") as TextBox;
                var txtTitle = item.FindControl("txtTitle") as TextBox;
                var txtRemark = item.FindControl("txtRemark") as TextBox;
                var txtWell = item.FindControl("txtWell") as TextBox;
                var txtTransmittalNumber = item.FindControl("txtTransmittalNumber") as TextBox;

                var docId = Convert.ToInt32(item.GetDataKeyValue("ID"));
                var objDoc = this.documentService.GetById(docId);
                if (objDoc != null)
                {
                    objDoc.StatusID = Convert.ToInt32(ddlStatus.SelectedValue);
                    objDoc.DisciplineID = Convert.ToInt32(ddlDiscipline.SelectedValue);
                    objDoc.DocumentTypeID = Convert.ToInt32(ddlDocumentType.SelectedValue);
                    objDoc.ReceivedFromID = Convert.ToInt32(ddlReceivedFrom.SelectedValue);
                    objDoc.ReceivedDate = txtReceivedDate.SelectedDate;
                    objDoc.RevisionFileName = txtRevisionFileName.Text.Trim();
                    objDoc.DocumentNumber = txtDocumentNumber.Text.Trim();
                    objDoc.Title = txtTitle.Text.Trim();
                    objDoc.Remark = txtRemark.Text.Trim();
                    objDoc.Well = txtWell.Text.Trim();
                    objDoc.TransmittalNumber = txtTransmittalNumber.Text.Trim();

                    this.documentService.Update(objDoc);    
                }
            }
        }

        protected void rbtnDefaultDoc_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["docId"] != null)
                {
                    ((GridItem)((RadioButton)sender).Parent.Parent).Selected = ((RadioButton)sender).Checked;
                    var item = ((RadioButton)sender).Parent.Parent as GridDataItem;
                    var watcherService = new ServiceController("EDMSFolderWatcher");

                    var oldDocId = Convert.ToInt32(Request.QueryString["docId"]);
                    var oldDocObj = this.documentService.GetById(oldDocId);
                
                    var selectedDocId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
                    var selectedDoc = this.documentService.GetById(selectedDocId);
                    if (selectedDoc != null && oldDocObj != null)
                    {
                        selectedDoc.IsLeaf = true;
                        oldDocObj.IsLeaf = false;

                        var filePath =
                            Server.MapPath(
                                HostingEnvironment.ApplicationVirtualPath == "/"
                                    ? oldDocObj.FilePath
                                    : oldDocObj.FilePath.Replace(
                                        "/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                        ////var revisionFilePath =
                        ////    Server.MapPath(
                        ////        HostingEnvironment.ApplicationVirtualPath == "/"
                        ////            ? oldDocObj.RevisionFilePath
                        ////            : oldDocObj.RevisionFilePath.Replace(
                        ////                "/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                        var revisionFilePath = Server.MapPath("/DocumentLibrary/RevisionHistory/" + DateTime.Now.ToString("ddMMyyhhmmss") + "_"
                                                              + oldDocObj.RevisionFileName);

                        oldDocObj.RevisionFilePath = "/DocumentLibrary/RevisionHistory/" + DateTime.Now.ToString("ddMMyyhhmmss") + "_"
                                                     + oldDocObj.RevisionFileName;

                        if (File.Exists(filePath))
                        {
                            File.Copy(filePath, revisionFilePath, true);
                        }

                        filePath =
                            Server.MapPath(
                                HostingEnvironment.ApplicationVirtualPath == "/"
                                    ? selectedDoc.FilePath
                                    : selectedDoc.FilePath.Replace(
                                        "/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                        revisionFilePath =
                            Server.MapPath(
                                HostingEnvironment.ApplicationVirtualPath == "/"
                                    ? selectedDoc.RevisionFilePath
                                    : selectedDoc.RevisionFilePath.Replace(
                                        "/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                        if (Utility.ServiceIsAvailable("EDMSFolderWatcher"))
                        {
                            watcherService.ExecuteCommand(128);
                        }

                        if (File.Exists(revisionFilePath))
                        {
                            File.Copy(revisionFilePath, filePath, true);
                        }

                        if (Utility.ServiceIsAvailable("EDMSFolderWatcher"))
                        {
                            watcherService.ExecuteCommand(129);
                        }


                        this.documentService.Update(selectedDoc);
                        this.documentService.Update(oldDocObj);

                        this.grdDocument.Rebind();
                    }
                }
            }
            catch (Exception ex)
            {
                var watcherService = new ServiceController("EDMSFolderWatcher");
                if (Utility.ServiceIsAvailable("EDMSFolderWatcher"))
                {
                    watcherService.ExecuteCommand(129);
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.ddlUser.SelectedValue != "0" && !string.IsNullOrEmpty(Request.QueryString["groupId"]))
            {
                var roleId = Convert.ToInt32(Request.QueryString["groupId"]);
                var userObj = this.userService.GetByID(Convert.ToInt32(this.ddlUser.SelectedValue));
                if (userObj != null)
                {
                    userObj.RoleId = roleId;
                    this.userService.Update(userObj);

                    this.LoadUserData(roleId);
                    this.grdDocument.Rebind();
                }
            }
        }

        protected void grdDocument_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "ResetPass")
            {
                var item = (GridDataItem)e.Item;
                var userId = Convert.ToInt32(item.GetDataKeyValue("Id").ToString());
                var objUser = this.userService.GetByID(userId);
                if (objUser != null)
                {
                    objUser.Password = "d56171087a3dcb9972919b28051e08f0";
                    this.userService.Update(objUser);
                }
            }
        }
    }
}