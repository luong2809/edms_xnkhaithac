﻿namespace EDMs.Web
{
    using System;
    using System.Configuration;
    using System.Drawing;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities;
    using EDMs.Web.Utilities.Sessions;

    public partial class UserEditForm : Page
    {

        #region Fields
        private readonly ResourceService _resourceService;
        private readonly UserService _userService;
        private readonly RoleService _roleService;

        private const string ResourceParameterKey = "id";
        private const string UserParameterKey = "userid";
        #endregion

        #region Properties
        /// <summary>
        /// Gets the user id.
        /// </summary>
        /// <value>
        /// The user id.
        /// </value>
        private int? ResourceId
        {
            get
            {
                if (String.IsNullOrEmpty(Request[ResourceParameterKey])) return null;

                int outValue;
                if (int.TryParse(Request[ResourceParameterKey], out outValue))
                    return outValue;
                return null;
            }
        }

        /// <summary>
        /// Gets the user id.
        /// </summary>
        /// <value>
        /// The user id.
        /// </value>
        private int? UserId
        {
            get
            {
                if (String.IsNullOrEmpty(Request[UserParameterKey])) return null;

                int outValue;
                if (int.TryParse(Request[UserParameterKey], out outValue))
                    return outValue;
                return null;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has user account.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has user account; otherwise, <c>false</c>.
        /// </value>
        private bool HasUserAccount
        {
            get { return !string.IsNullOrEmpty(txtUsername.Text.Trim()); }
        }
        #endregion

        #region Helpers

        /// <summary>
        /// Loads the data.
        /// </summary>
        private void LoadData()
        {
            var user = _userService.GetByID(UserId.Value);
            this.txtUsername.Text = user.Username;
            this.txtFullName.Text = user.FullName;
            this.txtEmail.Text = user.Email;
            ddlRoles.SelectedValue = user.RoleId.ToString();
        }

        /// <summary>
        /// Loads the data to combo.
        /// </summary>
        private void LoadDataToCombo()
        {
            var roles = this._roleService.GetAll();
            roles.Insert(0, new Role() { Id = 0, Name = string.Empty });
            this.ddlRoles.DataSource = roles;
            this.ddlRoles.DataValueField = "Id";
            this.ddlRoles.DataTextField = "Name";
            this.ddlRoles.DataBind();
        }

        #endregion

        #region Initializes
        
        public UserEditForm()
        {
            _resourceService = new ResourceService();
            _userService=new UserService();
            _roleService = new RoleService();
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.txtUsername.Focus();

                if (ConfigurationManager.AppSettings["EnableLDAP"] == "true")
                {
                    //this.txtUsername.ReadOnly = true;
                    //this.txtFullName.ReadOnly = true;
                    //this.txtEmail.ReadOnly = true;
                }

                this.LoadDataToCombo();

                if (!string.IsNullOrEmpty(Request.QueryString["groupId"]))
                {
                    this.ddlRoles.SelectedValue = Request.QueryString["groupId"];
                }

                if (this.UserId != null)
                {
                    this.LoadData();
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the btnCapNhat control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void btnCapNhat_Click(object sender, EventArgs e)
        {
            User user = null;

            if (Page.IsValid)
            {
                var groupAdminList = ConfigurationSettings.AppSettings.Get("GroupAdminList").Split(',').ToList();

                if (this.HasUserAccount)
                {
                    ////if (_userService.CheckExists(UserId, txtUsername.Text.Trim()))
                    ////{
                    ////    lblUsernameHasExists.Visible = true;
                    ////    return;
                    ////}

                    user = new User
                        {
                            Username = this.txtUsername.Text.Trim(),
                            RoleId = (int?)Utility.ConvertStringToType<int?>(this.ddlRoles.SelectedValue),
                        };

                    if (groupAdminList.Count > 0 && groupAdminList.Any(t => t == this.ddlRoles.SelectedValue))
                    {
                        user.IsAdmin = true;
                    }
                    else
                    {
                        user.IsAdmin = false;
                    }
                }

                if (this.UserId == null)
                {
                    // Insert
                    if (user != null)
                    {
                        user.Password = Utility.GetMd5Hash(GlobalVariables.Current.DefaultPasswordForNewUser);
                        user.FullName = this.txtFullName.Text.Trim();
                        user.Email = this.txtEmail.Text.Trim();
                        this._userService.Insert(user);
                    }
                }
                else
                {
                    // Update
                    if (user != null)
                    {
                        if (this.UserId != null)
                        {
                            user.Id = this.UserId.Value;
                            user.FullName = this.txtFullName.Text.Trim();
                            user.Email = this.txtEmail.Text.Trim();
                            this._userService.Update(user);
                        }
                        else
                        {
                            //user.ResourceId = this.ResourceId.Value;
                            user.Password = Utility.GetMd5Hash(GlobalVariables.Current.DefaultPasswordForNewUser);
                            user.FullName = this.txtFullName.Text.Trim();
                            user.Email = this.txtEmail.Text.Trim();
                            _userService.Insert(user);
                        }

                    }
                }

                ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CancelEdit();", true);
        }

        #endregion

        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            
        }

        protected void ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (this.txtUsername.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter User name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
            else if (this.HasUserAccount)
            {
                this.fileNameValidator.ErrorMessage = "The user name is already in use.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = !this._userService.CheckExists(this.UserId, this.txtUsername.Text.Trim());
            }
        }
    }
}