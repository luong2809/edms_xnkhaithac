﻿using System;
using System.Web.UI;
using EDMs.Business.Services;
using EDMs.Data.Entities;
using EDMs.Web.Utilities.Sessions;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
namespace EDMs.Web
{
    using System.Web.UI.WebControls;

    public partial class GroupFunctionMember : Page
    {

        #region Fields
        private readonly GroupFunctionService GroupFunctionService;
        private readonly UserService Userservice;
        #endregion

        #region Initializes

        public GroupFunctionMember()
        {
            GroupFunctionService = new GroupFunctionService();
            this.Userservice = new UserService();
        }

        #endregion
        #region Properties
        private List<int> listIDMember
        {
            get
            {
                var GroupObj = this.GroupFunctionService.GetByID(Convert.ToInt32(this.Request.QueryString["roleId"]));
                if (GroupObj != null)
                {
                    return GroupObj.MemberID != null ? GroupObj.MemberID.Split('$').Where(t => !string.IsNullOrEmpty(t)).Select(t => Convert.ToInt32(t)).ToList() : new List<int>();
                }
                else
                {
                    return new List<int>();
                }
            }
        }
        #endregion
        #region Helpers

        private void LoadData()
        {

            var userList = this.Userservice.GetAll().Where(t=> t.Id!= 1).OrderBy(t => t.FullName);
            this.grdUsers.DataSource = userList;
            // this.grdUsers.DataBind();
         
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //  LoadData();
                ///  BinDataToGrid();
               // Session["ListIdUser"] = listIDMember;
            }
        }

        protected void btnCapNhat_Click(object sender, EventArgs e)
        {
            var GroupObj = this.GroupFunctionService.GetByID(Convert.ToInt32(this.Request.QueryString["roleId"]));
            if (GroupObj != null)
            {
                var ID = string.Empty;
                var Name = string.Empty;
                foreach (GridDataItem row in this.grdUsers.SelectedItems) // loops through each rows in RadGrid
                {

                    ID += row.GetDataKeyValue("Id").ToString() + "$";
                    TableCell cell = row["colUsername"];
                    Name += cell .Text+ ", ";
                }
                GroupObj.MemberID = ID;
                GroupObj.MemberName = Name;
                this.GroupFunctionService.Update(GroupObj);
            }
            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind();", true);
        }

       protected void BinDataToGrid()
        {
            var GroupObj = this.GroupFunctionService.GetByID(Convert.ToInt32(this.Request.QueryString["roleId"]));
            if(GroupObj!= null)
            {
                var listID = GroupObj.MemberID != null ? GroupObj.MemberID.Split('$').Where(t => !string.IsNullOrEmpty(t)).Select(t => Convert.ToInt32(t)).ToList() : new List<int>();
                foreach (GridDataItem row in this.grdUsers.Items) // loops through each rows in RadGrid
                {
                    if (listID.Contains(Convert.ToInt32(row.GetDataKeyValue("Id").ToString()))){
                        row.Selected = true;
                    }
                }
            }
        }
        protected void btncancel_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CancelEdit();", true);
        }

        #endregion

        protected void grdUsers_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            LoadData();
           
        }

        protected void grdUsers_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                if (listIDMember .Contains(Convert.ToInt32(item.GetDataKeyValue("Id").ToString())))
                {
                    item.Selected = true;
                }
            }
        }
    }
}