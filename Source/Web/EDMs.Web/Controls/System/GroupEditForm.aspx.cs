﻿using System;
using System.Web.UI;
using EDMs.Business.Services;
using EDMs.Data.Entities;
using EDMs.Web.Utilities.Sessions;

namespace EDMs.Web
{
    using System.Web.UI.WebControls;

    public partial class GroupEditForm : Page
    {

        #region Fields
        private readonly GroupFunctionService _roleService;
        private const string RoleParameterKey = "roleid";
        #endregion

        #region Initializes

        public GroupEditForm()
        {
            _roleService = new GroupFunctionService();
        }

        #endregion

        #region Properties
        private int? RoleId
        {
            get
            {
                if (String.IsNullOrEmpty(Request[RoleParameterKey])) return null;

                int outValue;
                if (int.TryParse(Request[RoleParameterKey], out outValue))
                    return outValue;
                return null;
            }
        }
        #endregion

        #region Helpers

        private void LoadData()
        {
            if (RoleId == null) return;

            var role = this._roleService.GetByID(RoleId.Value);
            this.txtRoleName.Text = role.Name;
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (RoleId != null)
                {
                    LoadData();
                }
            }
        }

        protected void btnCapNhat_Click(object sender, EventArgs e)
        {
            var role = new GroupFunction
            {
                Name = txtRoleName.Text.Trim(),
                
            };

            if (RoleId == null)
            {
                //Insert
                role.CreatedBy = UserSession.Current.User.Id;
                role.CreatedDate = DateTime.Now;
                _roleService.Insert(role);
            }
            else
            {
                //Update
                role.LastUpdatedBy = UserSession.Current.User.Id;
                role.LastUpdatedDate = DateTime.Now;
                role.ID = RoleId.Value;
                _roleService.Update(role);
            }

            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind();", true);
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CancelEdit();", true);
        }

        #endregion

        protected void ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (this.txtRoleName.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter Group name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }

    }
}