﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UsersListByGroup.aspx.cs" Inherits="EDMs.Web.UsersListByGroup" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>User List</title>
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    <script type="text/javascript">
        
        function CloseAndRefreshGrid() {
            var oWin = GetRadWindow();
            var parentWindow = oWin.BrowserWindow;
            $(oWin).ready(function () {
                oWin.close();
            });
            parentWindow.refreshGrid();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including classic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }


    </script>
    <style type="text/css">
        .RadGrid .rgRow td, .RadGrid .rgAltRow td, .RadGrid .rgEditRow td, .RadGrid .rgFooter td, .RadGrid .rgFilterRow td, .RadGrid .rgHeader, .RadGrid .rgResizeCol, .RadGrid .rgGroupHeader td {
            padding-left: 1px !important;
            padding-right: 1px !important;
        }
        div.qlcbFormItem select.min25Percent {
            max-width: 500px !important;
           }
        /*Hide change page size control*/
        div.RadGrid .rgPager .rgAdvPart     
        {     
        display:none;        
        }    
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%" runat="server" ID="divContent">
            <ul style="list-style-type: none">
                <li style="width: 100%;">
                    <div>
                        <label style="width: 250px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Available Users
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <asp:DropDownList ID="ddlUser" runat="server" CssClass="min25Percent" Width="400px"/>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
                
                <li style="width: 100%; padding-top: 10px; padding-bottom: 3px; text-align: center">
                    <telerik:RadButton ID="btnSave" runat="server" Text="Add User" OnClick="btnSave_Click" 
                        Width="135" style="text-align: center">
                        <Icon PrimaryIconUrl="../../Images/save.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>
                </li>
                
                <li style="width: 100%;">
                    
                </li>

            </ul>
        </div>
        <div class="content">
            <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
            <telerik:RadGrid ID="grdDocument" runat="server" AllowPaging="True"
                AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" AllowFilteringByColumn="True"
                 GridLines="None" Skin="Windows7" Height="340"
                OnDeleteCommand="grdDocument_DeleteCommand"
                OnItemCommand="grdDocument_ItemCommand"
                OnNeedDataSource="grdDocument_OnNeedDataSource" PageSize="100" Style="outline: none">
                <GroupingSettings CaseSensitive="False"></GroupingSettings>
                <MasterTableView DataKeyNames="Id" ClientDataKeyNames="Id" Width="100%">
                    <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" 
                        NextPagesToolTip="Next page" NextPageToolTip="Next page" 
                        PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; Users."
                        PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <Columns>
                        <telerik:GridTemplateColumn AllowFiltering="False" Display="False">
                            <HeaderStyle Width="3%" HorizontalAlign="Center"  />
                            <ItemStyle HorizontalAlign="Center" Width="3%"/>
                            <ItemTemplate>
                                <asp:ImageButton runat="server" Style="padding-left: 7px; cursor: pointer;" 
                                    Visible='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "Id")) != 1%>'
                                    ID="EditLink" ImageUrl="~/Images/edit.png"/>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridButtonColumn UniqueName="ResetPass" CommandName="ResetPass" ConfirmText="Do you want to Reset password of this user?" ButtonType="ImageButton" ImageUrl="~/Images/reset.png">
                            <HeaderStyle Width="2%" />
                                <ItemStyle HorizontalAlign="Center" Width="2%"  />
                        </telerik:GridButtonColumn>
                        
                        <telerik:GridButtonColumn UniqueName="DeleteColumn" CommandName="Delete" ConfirmText="Do you want to move user out of group?" ButtonType="ImageButton" ImageUrl="~/Images/delete.png">
                            <HeaderStyle Width="2%" />
                                <ItemStyle HorizontalAlign="Center" Width="2%"  />
                        </telerik:GridButtonColumn>

            
                        <telerik:GridBoundColumn DataField="Username" HeaderText="User name" UniqueName="colUsername" FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" >
                            <HeaderStyle HorizontalAlign="Center"  Width="15%" />
                            <ItemStyle HorizontalAlign="Left" Width="15%"/>
                        </telerik:GridBoundColumn>
            
                        <telerik:GridBoundColumn DataField="FullName" HeaderText="Full name" UniqueName="colNameFullName" FilterControlWidth="98%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                            <HeaderStyle HorizontalAlign="Center"  Width="25%" />
                            <ItemStyle HorizontalAlign="Left" Width="25%"/>
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="Email" HeaderText="Email" UniqueName="Email" FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                            <HeaderStyle HorizontalAlign="Center"  Width="20%" />
                            <ItemStyle HorizontalAlign="Left" Width="20%"/>
                        </telerik:GridBoundColumn>
                    </Columns>
                    <CommandItemStyle Height="25px"></CommandItemStyle>
                </MasterTableView>
                <ClientSettings>
                    <Selecting AllowRowSelect="true"></Selecting>
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" ScrollHeight="500"></Scrolling>
                </ClientSettings>
            </telerik:RadGrid>
            
        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
        <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                
                <telerik:AjaxSetting AjaxControlID="grdDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="ddlUser" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        </div>
        <telerik:RadCodeBlock runat="server">
            <script type="text/javascript">
                
                function RowDblClick(sender, eventArgs) {
                    sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
                }

                var ajaxManager;

                function pageLoad() {
                ajaxManager = $find("<%=ajaxCustomer.ClientID %>");
            }


                function ShowInsertForm() {
                    window.radopen("Controls/Customers/CustomerEditForm.aspx", "DocDialog");
                    return false;
                }
                
                function refreshGrid(arg) {
                    //alert(arg);
                    if (!arg) {
                        ajaxManager.ajaxRequest("Rebind");
                    }
                    else {
                        ajaxManager.ajaxRequest("RebindAndNavigate");
                    }
                }
                
                function MyClick(sender, eventArgs) {
                    var inputs = document.getElementById("<%= grdDocument.MasterTableView.ClientID %>").getElementsByTagName("input");
                    for (var i = 0, l = inputs.length; i < l; i++) {
                        var input = inputs[i];
                        if (input.type != "radio" || input == sender)
                            continue;
                        input.checked = false;
                    }
                }
                
                function SelectMeOnly(objRadioButton, grdName) {

                    var i, obj;
                    for (i = 0; i < document.all.length; i++) {
                        obj = document.all(i);

                        if (obj.type == "radio") {

                            if (objRadioButton.id.substr(0, grdName.length) == grdName)
                                if (objRadioButton.id == obj.id)
                                    obj.checked = true;
                                else
                                    obj.checked = false;
                        }
                    }
                }
            </script>
        </telerik:RadCodeBlock>
    </form>
</body>
</html>
