﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.IO;
    using System.Net;
    using System.Net.Mail;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;
    using Aspose.Cells;
    using System.Data;

    /// <summary>
    /// Class customer
    /// </summary>
    public partial class Transmittal : Page
    {
        private readonly PermissionService permissionService = new PermissionService();

        private readonly DocumentService documentService = new DocumentService();
        private readonly TransmittalService transmittalService = new TransmittalService();
        private readonly UserService userService = new UserService();
        private readonly AttentionService attentionService = new AttentionService();
        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string unreadPattern = @"\(\d+\)";

        private readonly GroupDataPermissionService groupDataPermissionService = new GroupDataPermissionService();

        private readonly CategoryService categoryService = new CategoryService();

        private readonly UserDataPermissionService userDataPermissionService = new UserDataPermissionService();

        private List<int> AdminGroup
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("GroupAdminList").Split(',').Select(t => Convert.ToInt32(t)).ToList();
            }
        }

        
        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var categoryPermission = this.groupDataPermissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault()).Select(t => t.CategoryIdList).ToList();
                categoryPermission = categoryPermission.Union(this.userDataPermissionService.GetByUserId(UserSession.Current.User.Id).Select(t => t.CategoryId.ToString())).Distinct().ToList();
                var listCategory = this.AdminGroup.Contains(UserSession.Current.RoleId)
                                    ? this.categoryService.GetAll()
                                    : this.categoryService.GetAll().Where(t => categoryPermission.Any(x => x == t.ID.ToString())).ToList();

                if (listCategory.Any())
                {
                    foreach (var category in listCategory)
                    {
                        category.ParentId = -1;
                    }

                    listCategory.Insert(0, new Category() { ID = -1, Name = "DOCUMENT GROUP" });

                    this.radPbCategories.DataSource = listCategory;
                    this.radPbCategories.DataFieldParentID = "ParentId";
                    this.radPbCategories.DataFieldID = "Id";
                    this.radPbCategories.DataValueField = "Id";
                    this.radPbCategories.DataTextField = "Name";
                    this.radPbCategories.DataBind();
                    this.radPbCategories.Items[0].Expanded = true;

                    foreach (RadPanelItem item in this.radPbCategories.Items[0].Items)
                    {
                        item.ImageUrl = @"Images/category2.png";
                        item.NavigateUrl = "DocumentsHome.aspx?doctype=" + item.Value;
                    }
                }

                this.LoadListPanel();
                this.LoadSystemPanel();
            }
        }

        /// <summary>
        /// Load all document by folder
        /// </summary>
        /// <param name="isbind">
        /// The isbind.
        /// </param>
        protected void LoadDocuments(bool isbind = false)
        {
            this.grdDocument.DataSource = this.transmittalService.GetAll();
            
            if (isbind)
            {
                this.grdDocument.DataBind();
            }
        }

        /// <summary>
        /// The search customer.
        /// </summary>
        /// <param name="search">
        /// The search.
        /// </param>
        /// <param name="isbind">
        /// The isbind.
        /// </param>
        protected void SearchDocument(string search, bool isbind = false)
        {
            ////if (this.radTreeFolder.SelectedNode != null)
            ////{
            ////    var folderId = Convert.ToInt32(this.radTreeFolder.SelectedNode.Value);
            ////    if(!string.IsNullOrEmpty(search))
            ////    {
            ////        this.grdDocument.DataSource = this.documentService.QuickSearch(search, folderId);    
            ////    }
            ////    else
            ////    {
            ////        this.grdDocument.DataSource =  this.documentService.GetAllByFolder(folderId);
            ////    }

                
            ////    if (isbind)
            ////    {
            ////        this.grdDocument.DataBind();
            ////    }
            ////}
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                grdDocument.MasterTableView.SortExpressions.Clear();
                grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                grdDocument.MasterTableView.SortExpressions.Clear();
                grdDocument.MasterTableView.GroupByExpressions.Clear();
                grdDocument.MasterTableView.CurrentPageIndex = grdDocument.MasterTableView.PageCount - 1;
                grdDocument.Rebind();
            }
            else if (e.Argument.Contains("SendTrans"))
            {
                var objId = Convert.ToInt32(e.Argument.Split('_')[1]);
                var objTrans = this.transmittalService.GetById(objId);


                var smtpClient = new SmtpClient
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                    EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                    Host = ConfigurationManager.AppSettings["Host"],
                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                    Credentials = new NetworkCredential(ConfigurationManager.AppSettings["Email"], ConfigurationManager.AppSettings["EmailPass"])
                };

                var message = new MailMessage();
                message.From = new MailAddress(ConfigurationManager.AppSettings["Email"], ConfigurationManager.AppSettings["EmailName"]);
                message.Subject = "New Transmittal (" + objTrans.TransmittalNumber + ") has been created by " + UserSession.Current.User.FullName;
                message.BodyEncoding = new UTF8Encoding();
                message.IsBodyHtml = true;
                var count = 0;
                var listDocId = objTrans.DocList.Split(',').Where(t => !string.IsNullOrEmpty(t)).Select(
                            t => Convert.ToInt32(t));
                var bodyContent = @"Dear All,<br/><br/>

                                       please be informed that the following document of transmittal.<br/><br/>

                                        <table border='1' cellspacing='0'>
	                                        <tr>
		                                        <th style='text-align:center; width:40px'>No.</th>
		                                        <th style='text-align:center; width:330px'>Document.No</th>
                                                <th style='text-align:center; width:330px'>Revision</th>
		                                        <th style='text-align:center; width:330px'>Title</th>
		                                        <th style='text-align:center; width:120px'>Document Type</th>
	                                        </tr>";
                foreach (var docId in listDocId)
                {
                    var document = this.documentService.GetById(docId);

                    if (document != null)
                    {

                        count += 1;
                        bodyContent += @"<tr>
                                <td>" + count + @"</td>
                                <td><a href='" + ConfigurationSettings.AppSettings.Get("WebAddress")
                                       + document.FilePath + "' download='" + document.DocumentNumber != null ? document.DocumentNumber : document.Name + "'>"
                                       + document.DocumentNumber != null ? document.DocumentNumber : document.Name + @"</a></td>
                                 <td>"
                                       + document.RevisionName + @"</td>
                                <td>"
                                       + document.Title + @"</td>
                                <td>"
                                       + document.DocumentTypeName + @"</td></tr>";
                    }
                }

                bodyContent += @"</table>
                                        <br/>
                                        Thanks and regards,<br/><br/>
                                        " + UserSession.Current.User.FullName + ". <br/>";


                message.Body = bodyContent;

                if (!string.IsNullOrEmpty(objTrans.AttentionList))
                {
                    var toList = objTrans.AttentionList.Split(',').Where(t => !string.IsNullOrEmpty(t));
                    var ccList = ConfigurationManager.AppSettings["EmailNotificationCC"].Split(',').Where(t => !string.IsNullOrEmpty(t));

                    foreach (var to in toList)
                    {
                        var user = this.attentionService.GetByFullName(to);
                        if (user != null && !string.IsNullOrEmpty(user.Email)) message.To.Add(new MailAddress(to));
                    }

                    foreach (var cc in ccList)
                    {
                        message.CC.Add(new MailAddress(cc));
                    }

                    smtpClient.Send(message);

                    objTrans.IsSend = true;
                    this.transmittalService.Update(objTrans);
                }
            }
            else if (e.Argument == "Export")
            {
                var filePath = Server.MapPath(@"DocumentResource\Exports") + @"\";
                var workbook = new Workbook();
                workbook.Open(filePath + @"Template\TransmittalListReport.xls");
                var projectName = string.Empty;
                var dtFull = new DataTable();
                //var projectID = Convert.ToInt32(this.lblProjectId.Value);
                dtFull.Columns.AddRange(new[]
                    {                       
                       new DataColumn("NoIndex", typeof(String)),
                        new DataColumn("Date", typeof(String)),
                        new DataColumn("Contractor", typeof(String)),
                        new DataColumn("NoOfficeLetter", typeof(String)),                     
                        new DataColumn("DocList", typeof(String)),     
                        new DataColumn("To", typeof(String)),
                        new DataColumn("Remark", typeof(String)),

                    });

                List<int> ListId = new List<int>();

                this.grdDocument.AllowPaging = false;
                this.grdDocument.Rebind();
                foreach (GridDataItem row in this.grdDocument.Items) // loops through each rows in RadGrid
                {
                    ListId.Add(Convert.ToInt32(row.GetDataKeyValue("ID").ToString()));
                }
                this.grdDocument.AllowPaging = true;
                this.grdDocument.Rebind();
                var docListFilter = this.transmittalService.GetAllDocList(ListId);

                if (docListFilter.Count > 0)
                {


                    var sheets = workbook.Worksheets;
                    var wsSummary = sheets[0];
                    var count = 1;
                    foreach (var trans in docListFilter)
                    {
                        var data = dtFull.NewRow();


                        data["NoIndex"] = count;
                        data["Date"] = trans.ReceivedDate != null ? trans.ReceivedDate.GetValueOrDefault().ToString("dd/MM/yyyy") : string.Empty;
                        data["Contractor"] = trans.ToList;
                        data["NoOfficeLetter"] = trans.NoOfficialletter;
                        data["DocList"] =trans.DocList!= null? this.GetListAttachFile(trans.DocList):string.Empty;
                        data["To"] = trans.AttentionList;
                        data["Remark"] = trans.Remark;
                        count++;
                        dtFull.Rows.Add(data);
                    }


                    wsSummary.Cells["A1"].PutValue ( count + 1);
                    wsSummary.Cells.ImportDataTable(dtFull, false, 1, 1, dtFull.Rows.Count, dtFull.Columns.Count - 1);
                    wsSummary.AutoFitRows();     
                    var filename = "Transmittal_Report " + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                    workbook.Save(filePath + filename);
                    this.DownloadByWriteByte(filePath + filename, filename, true);

                }
            }
            else
            {
                SearchDocument(e.Argument, true);
            }
        }
        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                FileStream fs = new FileStream(strFileName, FileMode.Open);
                int streamLength = Convert.ToInt32(fs.Length);
                byte[] data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            this.LoadDocuments();
        }

        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var tranId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            this.transmittalService.Delete(tranId);

            this.grdDocument.Rebind();
        }

        /// <summary>
        /// The grd document_ item command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string abc = e.CommandName;
        }

        protected void RadTabStrip1_TabClick(object sender, RadTabStripEventArgs e)
        {
            //AddPageView(e.Tab);
            e.Tab.PageView.Selected = true;
        }

        protected void radMenu_ItemClick(object sender, RadMenuEventArgs e)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The grd document_ item data bound.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                var newIcon = (Image)e.Item.FindControl("newicon");

                if ((DateTime.Now - Convert.ToDateTime(DataBinder.Eval(item.DataItem, "CreatedDate"))).TotalHours < 24)
                {
                    newIcon.Visible = true;
                }
            }
        }

        private void LoadListPanel()
        {
            var listId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("ListID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), listId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "LIST" });

                this.radPbList.DataSource = permissions;
                this.radPbList.DataFieldParentID = "ParentId";
                this.radPbList.DataFieldID = "Id";
                this.radPbList.DataValueField = "Id";
                this.radPbList.DataTextField = "MenuName";
                this.radPbList.DataBind();
                this.radPbList.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbList.Items[0].Items)
                {
                    item.ImageUrl = @"Images/listmenu.png";
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                }
            }
        }
        /// <summary>
        /// Get File list of doc Id.
        /// </summary>
        /// <param name="docId"></param>
        /// <returns></returns>
        public string GetListAttachFile(string ListId)
        {
            string tempFileName = "";
            foreach (var docId in ListId.Split(';').Where(t=> !string.IsNullOrEmpty(t)))
            {
                if (!string.IsNullOrEmpty(docId))
                {
                  
                    var objDoc = this.documentService.GetById(Convert.ToInt32(docId));
                    if (objDoc != null)
                    {
                        tempFileName = tempFileName + (!string.IsNullOrEmpty(objDoc.Title)? objDoc.Title :objDoc.DocumentNumber) + ";     ";
                    }
                }
            }
          
            return tempFileName;
        }
        private void LoadSystemPanel()
        {
            var systemId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("SystemID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), systemId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "SYSTEM" });

                this.radPbSystem.DataSource = permissions;
                this.radPbSystem.DataFieldParentID = "ParentId";
                this.radPbSystem.DataFieldID = "Id";
                this.radPbSystem.DataValueField = "Id";
                this.radPbSystem.DataTextField = "MenuName";
                this.radPbSystem.DataBind();
                this.radPbSystem.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbSystem.Items[0].Items)
                {
                    item.ImageUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Icon;
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                }
            }
        }
    }
}

