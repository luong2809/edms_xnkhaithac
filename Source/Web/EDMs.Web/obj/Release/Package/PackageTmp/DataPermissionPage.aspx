﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DataPermissionPage.aspx.cs" Inherits="EDMs.Web.DataPermissionPage" EnableViewState="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        div.qlcbFormItem select.min25Percent {
            max-width: 100% !important;
        }

        .rtPlus, .rtMinus {
            background-image: url('Images/plus-minus-icon1.png') !important;
            cursor: pointer;
        }

        .rtIn {
            cursor: pointer;
        }

        a.tooltip {
            outline: none;
            text-decoration: none;
        }

            a.tooltip strong {
                line-height: 30px;
            }

            a.tooltip:hover {
                text-decoration: none;
            }

            a.tooltip span {
                z-index: 10;
                display: none;
                padding: 14px 20px;
                margin-top: -30px;
                margin-left: 5px;
                width: 240px;
                line-height: 16px;
            }

            a.tooltip:hover span {
                display: inline;
                position: absolute;
                color: #111;
                border: 1px solid #DCA;
                background: #fffAF0;
            }

        .callout {
            z-index: 20;
            position: absolute;
            top: 30px;
            border: 0;
            left: -12px;
        }

        /*CSS3 extras*/
        a.tooltip span {
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            -moz-box-shadow: 5px 5px 8px #CCC;
            -webkit-box-shadow: 5px 5px 8px #CCC;
            box-shadow: 5px 5px 8px #CCC;
        }

        .rgMasterTable {
            table-layout: auto;
        }

        #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_grdDocumentPanel, #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_divContainerPanel {
            height: 100% !important;
        }

        #ctl00_ContentPlaceHolder2_RadPageView1, #ctl00_ContentPlaceHolder2_RadPageView2,
        #ctl00_ContentPlaceHolder2_RadPageView3, #ctl00_ContentPlaceHolder2_RadPageView4,
        #ctl00_ContentPlaceHolder2_RadPageView5 {
            height: 100% !important;
        }

        #divContainerLeft {
            width: 25%;
            float: left;
            margin: 5px;
            height: 99%;
            border-right: 1px dotted green;
            padding-right: 5px;
        }

        #divContainerRight {
            width: 100%;
            float: right;
            margin-top: 5px;
            height: 99%;
        }

        .dotted {
            border: 1px dotted #000;
            border-style: none none dotted;
            color: #fff;
            background-color: #fff;
        }

        .exampleWrapper {
            width: 100%;
            height: 100%;
            /*background: transparent url(images/background.png) no-repeat top left;*/
            position: relative;
        }

        .RadComboBox {
            width: 115px !important;
            border-bottom: none !important;
        }

        .RadComboBoxDropDown_Windows7 {
            width: 380px !important;
            height: 200px !important;
        }

        .RadComboBoxDropDown .rcbScroll {
            height: 199px !important;
        }

        .RadComboBoxDropDown_Default .rcbHovered {
            background-color: #46A3D3;
            color: #fff;
        }

        .RadComboBoxDropDown .rcbItem, .RadComboBoxDropDown .rcbHovered, .RadComboBoxDropDown .rcbDisabled, .RadComboBoxDropDown .rcbLoading, .RadComboBoxDropDown .rcbCheckAllItems, .RadComboBoxDropDown .rcbCheckAllItemsHovered {
            margin: 0 0px;
        }

        .RadComboBox .rcbInputCell .rcbInput {
            border-left-color: #46A3D3 !important;
            border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;
            border-style: solid;
            border-width: 1px 1px 1px 5px;
            color: #000000;
            float: left;
            font: 12px "segoe ui";
            margin: 0;
            padding: 2px 5px 3px;
            vertical-align: middle;
            width: 379px;
        }

        .RadComboBox table td.rcbInputCell, .RadComboBox .rcbInputCell .rcbInput {
            padding-left: 0px !important;
            padding-right: 0px !important;
        }

        .tabStrip {
            position: absolute;
            top: 0px;
            left: 0px;
        }

        .multiPage {
            position: absolute;
            top: 30px;
            left: 0px;
            color: white;
            width: 100%;
            height: 100%;
        }

        /*Fix RadMenu and RadWindow z-index issue*/
        .radwindow {
            z-index: 8000 !important;
        }

        .TemplateMenu {
            z-index: 10;
        }
    </style>
    <telerik:RadPanelBar ID="radPbSearch" runat="server" Width="100%" Visible="false">
        <Items>
            <telerik:RadPanelItem Text="ADVANCE SEARCH" runat="server" Expanded="True" Width="100%" Visible="false">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Advance Search" ImageUrl="Images/search.gif" NavigateUrl="Search.aspx" />
                </Items>
            </telerik:RadPanelItem>
        </Items>
    </telerik:RadPanelBar>
    <telerik:RadPanelBar ID="radPbCategories" runat="server" OnClientItemClicking="radPbCategories_OnClientItemClicking" Width="100%"></telerik:RadPanelBar>

    <telerik:RadPanelBar ID="RadPanelBar1" runat="server" Width="100%">
        <Items>
            <telerik:RadPanelItem Text="TRANSMITTAL" runat="server" Expanded="True" Width="100%" Visible="false">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Transmittal" ImageUrl="Images/Transmittal.png" NavigateUrl="Transmittal.aspx" />
                </Items>
            </telerik:RadPanelItem>
        </Items>
    </telerik:RadPanelBar>

    <telerik:RadPanelBar ID="radPbList" runat="server" Width="100%" />
    <telerik:RadPanelBar ID="radPbSystem" runat="server" Width="100%" />

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <telerik:RadSplitter ID="RadSplitter4" runat="server" Orientation="Horizontal" Width="100%">
        <%--<telerik:RadPane ID="RadPane3" runat="server" Height="30px" Scrollable="false" Scrolling="None">
            <div style="padding-top: 4px; padding-left:10px">
            <telerik:RadButton ID="btnSave" OnClick="btnSave_Click" runat="server" Text="Save" >
                <Icon PrimaryIconUrl="Images/save.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="20"></Icon>
            </telerik:RadButton>
        </div>

        </telerik:RadPane>--%>
        <telerik:RadPane ID="RadPane2" runat="server" Scrollable="false" Scrolling="None">
            <telerik:RadSplitter ID="Radsplitter3" runat="server" Orientation="Horizontal">
                <telerik:RadPane ID="Radpane4" runat="server" Scrolling="None">

                    <telerik:RadSplitter ID="Radsplitter10" runat="server" Orientation="Vertical">
                        <telerik:RadPane ID="Radpane6" runat="server" Scrolling="Both" Width="40%">
                            <div style="padding-top: 4px; padding-left: 10px; width: 97%">
                                Document Categories:
                                 <div style="" class="qlcbFormItem">
                                     <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="True" CssClass="min25Percent" Width="100%" OnSelectedIndexChanged="ddlCategory_SelectecIndexChange" />
                                 </div>
                                <br />
                                <br />
                                Folder Tree:
                                 <telerik:RadTreeView ID="radTreeFolder" runat="server" Style="padding-top: 10px;"
                                     Width="100%" Height="100%" ShowLineImages="False"
                                     OnNodeClick="radTreeFolder_NodeClick"
                                     OnNodeDataBound="radTreeFolder_OnNodeDataBound">
                                     <DataBindings>
                                         <telerik:RadTreeNodeBinding Expanded="false"></telerik:RadTreeNodeBinding>
                                     </DataBindings>
                                 </telerik:RadTreeView>
                            </div>
                        </telerik:RadPane>

                        <telerik:RadPane ID="Radpane1" runat="server" Scrolling="Both" Width="60%">
                            <div style="width: 100%" runat="server" id="divContent">
                                <ul style="list-style-type: none">
                                    <li style="width: 100%;">
                                        <div>
                                            <label style="width: 60px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                                <span style="color: #2E5689; text-align: right;"></span>
                                            </label>
                                            <div style="float: left; padding-top: 5px; padding-left: 6px;" class="qlcbFormItem">
                                                <asp:CheckBox ID="cbApplyAll" runat="server" Text="Apply for all subfolder" Checked="True" />
                                            </div>
                                        </div>
                                        <div style="clear: both; font-size: 0;"></div>
                                    </li>
                                    <li style="width: 500px;">
                                        <div>
                                            <label style="width: 60px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                                <span style="color: #2E5689; text-align: right;">Permission
                                                </span>
                                            </label>
                                            <div style="float: left; padding-top: 5px; padding-left: 6px;" class="qlcbFormItem">
                                                <div>
                                                    <asp:CheckBox ID="cbRead" runat="server" Text="Read" Value="0" Checked="true"></asp:CheckBox>
                                                </div>
                                                <div>
                                                    <asp:CheckBox ID="cbModify" runat="server" Text="Modify" Value="1" AutoPostBack="true" OnCheckedChanged="cbModify_CheckedChanged"></asp:CheckBox>
                                                </div>
                                                <div>
                                                    <asp:CheckBox ID="cbDelete" runat="server" Text="Delete" Value="2" AutoPostBack="true" OnCheckedChanged="cbDelete_CheckedChanged"></asp:CheckBox>
                                                </div>
                                                <div>
                                                    <asp:CheckBox ID="cbCreate" runat="server" Text="Create Folder" Value="3"></asp:CheckBox>
                                                </div>
                                                <div>
                                                    <asp:CheckBox ID="cbFull" runat="server" Text="Full Control" Value="4" AutoPostBack="true" OnCheckedChanged="cbFull_CheckedChanged"></asp:CheckBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="clear: both; font-size: 0;"></div>
                                    </li>
                                    <li style="width: 100%;">
                                        <div>
                                            <label style="width: 60px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                                <span style="color: #2E5689; text-align: right;">Groups
                                                </span>
                                            </label>
                                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                                <asp:DropDownList ID="ddlGroup" runat="server" CssClass="min25Percent" Width="400px"
                                                    AutoPostBack="True" OnSelectedIndexChanged="ddlGroup_SelectedIndexChange" />
                                            </div>
                                        </div>
                                        <div style="clear: both; font-size: 0;"></div>
                                    </li>

                                    <li style="width: 100%;">
                                        <div>
                                            <label style="width: 60px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                                                <span style="color: #2E5689; text-align: right;">Users
                                                </span>
                                            </label>
                                            <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                                                <telerik:RadComboBox ID="ddlUser" runat="server" Skin="Windows7"
                                                    CheckBoxes="true" EnableCheckAllItemsCheckBox="true">
                                                </telerik:RadComboBox>
                                            </div>
                                        </div>
                                        <div style="clear: both; font-size: 0;"></div>
                                    </li>

                                    <li style="width: 100%; padding-top: 10px; padding-bottom: 3px; text-align: center">
                                        <telerik:RadButton ID="btnSave" runat="server" Text="Add Permission" OnClick="btnSave_Click"
                                            Width="135" Style="text-align: center">
                                            <Icon PrimaryIconUrl="~/Images/save.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                                        </telerik:RadButton>
                                    </li>

                                    <li style="width: 100%;"></li>
                                </ul>
                            </div>
                            <div style="width: 100%;">
                                <telerik:RadGrid ID="grdPermission"
                                    runat="server" AllowPaging="True" AutoGenerateColumns="False" CellSpacing="0" CellPadding="0"
                                    PageSize="50" Height="500" GridLines="None"
                                    Skin="Windows7"
                                    OnNeedDataSource="grdPermission_OnNeedDataSource"
                                    OnDeleteCommand="grdPermission_OnDeteleCommand">
                                    <GroupingSettings CaseSensitive="False"></GroupingSettings>
                                    <MasterTableView DataKeyNames="ID" ClientDataKeyNames="ID" Width="95%">
                                        <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; records." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                                        <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="IsGroup" UniqueName="IsGroup" Display="False" />

                                            <telerik:GridButtonColumn UniqueName="DeleteColumn" CommandName="Delete"
                                                ConfirmText="Do you want to delete item?" ButtonType="ImageButton" ImageUrl="~/Images/delete.png">
                                                <HeaderStyle Width="30" />
                                                <ItemStyle HorizontalAlign="Center" Width="5%" />
                                            </telerik:GridButtonColumn>

                                            <telerik:GridBoundColumn DataField="Name" HeaderText="Groups/Users" UniqueName="colName"
                                                ShowFilterIcon="False" FilterControlWidth="97%" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                                <HeaderStyle Width="55%" HorizontalAlign="Center"></HeaderStyle>
                                                <ItemStyle Width="55%" HorizontalAlign="Left"></ItemStyle>
                                            </telerik:GridBoundColumn>

                                            <telerik:GridTemplateColumn HeaderText="Read" UniqueName="ReadFile" AllowFiltering="False">
                                                <HeaderStyle Width="20%" HorizontalAlign="Center"></HeaderStyle>
                                                <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Image ID="Image1" runat="server"
                                                        Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "ReadFile"))%>'
                                                        ImageUrl='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "ReadFile")) ? "~/Images/ok.png" : "" %>' />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridTemplateColumn HeaderText="Modify" UniqueName="ModifyFile" AllowFiltering="False">
                                                <HeaderStyle Width="20%" HorizontalAlign="Center"></HeaderStyle>
                                                <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Image ID="Image2" runat="server"
                                                        Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "ModifyFile"))%>'
                                                        ImageUrl='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "ModifyFile")) ? "~/Images/ok.png" : "" %>' />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridTemplateColumn HeaderText="Delete" UniqueName="DeleteFile" AllowFiltering="False">
                                                <HeaderStyle Width="20%" HorizontalAlign="Center"></HeaderStyle>
                                                <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Image ID="Image3" runat="server"
                                                        Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "DeleteFile"))%>'
                                                        ImageUrl='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "DeleteFile")) ? "~/Images/ok.png" : "" %>' />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridTemplateColumn HeaderText="Create" UniqueName="CreateFolder" AllowFiltering="False">
                                                <HeaderStyle Width="20%" HorizontalAlign="Center"></HeaderStyle>
                                                <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Image ID="Image4" runat="server"
                                                        Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "CreateFolder"))%>'
                                                        ImageUrl='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "CreateFolder")) ? "~/Images/ok.png" : "" %>' />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridTemplateColumn HeaderText="Full Permission" UniqueName="IsFullPermission" AllowFiltering="False">
                                                <HeaderStyle Width="20%" HorizontalAlign="Center"></HeaderStyle>
                                                <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Image ID="Image5" runat="server"
                                                        Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsFullPermission"))%>'
                                                        ImageUrl='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsFullPermission")) ? "~/Images/ok.png" : "" %>' />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <CommandItemStyle Height="25px"></CommandItemStyle>
                                    </MasterTableView>
                                    <ClientSettings>
                                        <Selecting AllowRowSelect="true"></Selecting>
                                        <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" ScrollHeight="500"></Scrolling>
                                    </ClientSettings>
                                </telerik:RadGrid>

                            </div>
                        </telerik:RadPane>

                    </telerik:RadSplitter>

                </telerik:RadPane>
            </telerik:RadSplitter>
        </telerik:RadPane>
    </telerik:RadSplitter>

    <span style="display: none">


        <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="radTreeFolder">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdPermission" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="ddlUser" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="ddlGroup" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="ddlCategory">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="radTreeFolder" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="grdPermission" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="ddlUser" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="ddlGroup" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ddlGroup">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="ddlUser" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="grdPermission">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdPermission" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="ddlUser" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="ddlGroup" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="btnSave">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdPermission" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="ddlUser" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="ddlGroup" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
    </span>

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="CustomerDialog" runat="server" Title="Document Status Information"
                VisibleStatusbar="false" Height="350" Width="610"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="AttachDoc" runat="server" Title="Attach documents"
                VisibleStatusbar="false" Height="555" Width="1100" MinHeight="555" MinWidth="1100" MaxHeight="555" MaxWidth="1100"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>

            <telerik:RadWindow ID="DocList" runat="server" Title="Transmittal - Document List"
                VisibleStatusbar="false" Height="418" Width="1100" MinHeight="418" MinWidth="1100" MaxHeight="418" MaxWidth="1100"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" />
    <asp:HiddenField runat="server" ID="FolderContextMenuAction" />
    <asp:HiddenField runat="server" ID="lblFolderId" />
    <asp:HiddenField runat="server" ID="lblDocId" />
    <asp:HiddenField runat="server" ID="lblCategoryId" />
    <input type="hidden" id="radGridClickedRowIndex" name="radGridClickedRowIndex" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script src="Scripts/jquery-1.7.1.js"></script>
        <script type="text/javascript">

            var radDocuments;


            function GetGridObject(sender, eventArgs) {
                radDocuments = sender;
            }

        </script>
        <script type="text/javascript">
            /* <![CDATA[ */
            var toolbar;
            var searchButton;
            var ajaxManager;

            function pageLoad() {
                $('iframe').load(function () { //The function below executes once the iframe has finished loading<---true dat, althoug Is coppypasta from I don't know where
                    //alert($('iframe').contents());
                });

                ajaxManager = $find("<%=ajaxCustomer.ClientID %>");
            }
            function radPbCategories_OnClientItemClicking(sender, args) {
                var item = args.get_item();
                var categoryId = item.get_value();
                document.getElementById("<%= lblCategoryId.ClientID %>").value = categoryId;
            }
            /* ]]> */
        </script>
    </telerik:RadCodeBlock>
</asp:Content>

<%--Tan.Le Remove here--%>
<%--<uc1:List runat="server" ID="CustomerList"/>--%>
<%-- <div id="EDMsCustomers" runat="server" />--%>
