﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserCommentEditForm.aspx.cs" Inherits="EDMs.Web.Controls.Document.UserCommentEditForm" validateRequest="false"%>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox, Version=3.3.0.22838, Culture=neutral, PublicKeyToken=5962a4e684a48b87" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />
    
    <style type="text/css">
        html, body, form {
	        overflow:auto;
        }
        
        #selectEmailValidate{
            margin-top: 5px;
            margin-bottom: -3px
        }

        .RadComboBoxDropDown_Windows7 .rcbHovered {
            background: coral !important;
        }

        .RadComboBoxDropDown_Default .rcbHovered {
               background-color: #46A3D3;
               color: #fff;
           }
           .RadComboBoxDropDown .rcbItem, .RadComboBoxDropDown .rcbHovered, .RadComboBoxDropDown .rcbDisabled, .RadComboBoxDropDown .rcbLoading, .RadComboBoxDropDown .rcbCheckAllItems, .RadComboBoxDropDown .rcbCheckAllItemsHovered {
               margin: 0 0px;
           }
           .RadComboBox .rcbInputCell .rcbInput{
            border-left-color: #46A3D3 !important;
            border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;
            border-style: solid;
            border-width: 1px 1px 1px 5px;
            color: #000000;
            float: left;
            font: 12px "segoe ui";
            margin: 0;
            padding: 2px 5px 3px;
            vertical-align: middle;
            width: 99%
           }
           .RadComboBox table td.rcbInputCell, .RadComboBox .rcbInputCell .rcbInput {
               padding-left: 0px !important;
           }
              div.rgEditForm label {
               float: right;
            text-align: right;
            width: 72px;
           }
           .rgEditForm {
               text-align: right;
           }
           .RadComboBox {
               border-bottom: none !important;
           }
           .RadUpload .ruFileWrap {
               overflow: visible !important;
           }

        /*#grdContactHistory
        {
            height: 300px !important;
        }

        #grdContactHistory_GridData
        {
            height: 250px !important;
        }*/
    </style>

    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
   
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
        <div style="width: 100%" runat="server" ID="divContent" Visible="False">
            <ul style="list-style-type: none">
                <li style="width: 500px;">
                    <div>
                        <label style="width: 125px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right; ">Comment Content
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem" runat="server" id="divFileName">
                            <asp:Label ID="lblComment" runat="server" Style="width: 300px;"/>
                        </div>
                    </div>
                    <div style="clear: both; font-size: 0;"></div>
                </li>
            </ul>
        </div>

        <table style="width: 100%">
            <%--<tr>
                <td style="width: 100%">
                    <telerik:RadToolBar ID="SendMailMenu" runat="server" Width="100%" OnButtonClick="SendMailMenu_OnButtonClick">
                        <Items>
                            <telerik:RadToolBarButton runat="server" Text="Send" Value="SendMail" ImageUrl="~/Images/save.png"></telerik:RadToolBarButton>
                            <telerik:RadToolBarButton runat="server" IsSeparator="true"/>
                        </Items>
                    </telerik:RadToolBar>
                </td>
            </tr>--%>
            <tr>
                <td style="width: 100%">
                    <FTB:FreeTextBox id="txtComment" runat="server" Width="850px" Height="230px"/>
                    <%--<telerik:RadEditor ID="txtComment" runat="server" Width="950px" Height="280px" Skin="Windows7"
                        ToolbarMode="Default" ToolsFile="~/Controls/Document/XML/ToolsFile.xml" EnableResize="False">
                    </telerik:RadEditor>--%>
                </td>
            </tr>
            
            <tr>
                <td style="width: 100%" align="center">
                    <telerik:RadButton ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" Width="70px" style="text-align: center">
                        <Icon PrimaryIconUrl="../../Images/save.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                    </telerik:RadButton>
                </td>
            </tr>
        </table>  
        
        <telerik:RadAjaxManager runat="Server" ID="ajaxDocument">
            
        </telerik:RadAjaxManager>

        <telerik:RadScriptBlock runat="server">
            <script type="text/javascript">
                var ajaxManager;
                function pageLoad() {
                    ajaxManager = $find("<%=ajaxDocument.ClientID %>");
                }


                function Close() {
                    GetRadWindow().BrowserWindow.refreshGrid();
                    GetRadWindow().close();
                }

                function GetRadWindow() {
                    var oWindow = null;
                    if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

                    return oWindow;
                }
            </script>
        </telerik:RadScriptBlock>
    </form>
</body>
</html>
