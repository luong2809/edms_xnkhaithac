﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CheckOutInHistoryList.aspx.cs" Inherits="EDMs.Web.Controls.Document.CheckOutInHistoryList" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Checkin/out history</title>
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="content">
            <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
            <telerik:RadGrid ID="grdDocument" runat="server" AllowPaging="True"
                AutoGenerateColumns="False" CellPadding="0" CellSpacing="0"
                GridLines="None" Skin="Windows7" Height="450"
                OnNeedDataSource="grdDocument_OnNeedDataSource" 
                OnDetailTableDataBind="grdDocument_DetailTableDataBind"
                PageSize="5" Style="outline: none">
                <MasterTableView ClientDataKeyNames="ID" DataKeyNames="ID" Width="100%">
                    <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; Documents." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <DetailTables>
                            <telerik:GridTableView DataKeyNames="ID" Name="DocDetail" Width="100%"
                                AllowPaging="True" PageSize="10">
                                <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Trang &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Tổng cộng:  &lt;strong&gt;{5}&lt;/strong&gt; File tài liệu." PageSizeLabelText="Dòng/trang: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                                <Columns>
                                    <telerik:GridTemplateColumn AllowFiltering="false" UniqueName="DownloadColumn">
                                        <HeaderStyle Width="5%" />
                                        <ItemStyle HorizontalAlign="Center" Width="5%"/>
                                        <ItemTemplate>
                                            <a href='<%# DataBinder.Eval(Container.DataItem, "FilePath") %>' 
                                                download='<%# DataBinder.Eval(Container.DataItem, "FileName") %>' target="_blank">
                                                <asp:Image ID="Image1" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "ExtensionIcon") %>'
                                                    Style="cursor: pointer;" ToolTip="Download document" /> 
                                            </a>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                            
                                    <telerik:GridBoundColumn DataField="FileName" HeaderText="File name" UniqueName="FileName">
                                        <HeaderStyle HorizontalAlign="Center" Width="60%" />
                                        <ItemStyle HorizontalAlign="Left" Width="60%" />
                                    </telerik:GridBoundColumn>
                                
                                    <telerik:GridBoundColumn DataField="CreatedDate" HeaderText="Upload time" UniqueName="CreatedDate"
                                        DataFormatString="{0:dd/MM/yyyy hh:mm tt}" >
                                        <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                        <ItemStyle HorizontalAlign="Left" Width="20%" />
                                    </telerik:GridBoundColumn>
                                
                                    <telerik:GridBoundColumn DataField="FileSize" HeaderText="File size(Kb)" UniqueName="FileSize" DataFormatString="{0:0,0.00}">
                                        <HeaderStyle HorizontalAlign="Center" Width="13%" />
                                        <ItemStyle HorizontalAlign="Left" Width="13%" />
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </telerik:GridTableView>
                        </DetailTables>
                    <Columns>
                        
                        <telerik:GridTemplateColumn Display="False">
                            <HeaderStyle Width="2%" />
                            <ItemStyle HorizontalAlign="Center" Width="2%"/>
                            <ItemTemplate>
                                <a download='<%# DataBinder.Eval(Container.DataItem, "DCRObj.FileName") %>' 
                                    href='<%# DataBinder.Eval(Container.DataItem, "DCRObj.FilePath") %>' target="_blank">
                                    <asp:Image ID="CallLink" runat="server" ImageUrl='~/Images/generate.png' 
                                        Visible='<%# DataBinder.Eval(Container.DataItem, "ActionName").ToString() == "Checkout" %>'
                                        Style="cursor: pointer;" AlternateText="Download DCR" /> 
                                </a>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--3--%>
                        <telerik:GridBoundColumn DataField="DCRObj.DCRNumber" HeaderText="DCR Number" UniqueName="DCRNumber">
                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" Width="10%"/>
                        </telerik:GridBoundColumn>
                                            
                        <telerik:GridBoundColumn DataField="ActionName" HeaderText="Action" UniqueName="ActionName">
                            <HeaderStyle HorizontalAlign="Center" Width="7%" />
                            <ItemStyle HorizontalAlign="Left" Width="7%" />
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn DataField="AtTime" HeaderText="Time" UniqueName="AtTime" DataFormatString="{0:dd/MM/yyyy hh:mm tt}" >
                            <HeaderStyle HorizontalAlign="Center" Width="8%" />
                            <ItemStyle HorizontalAlign="Center" Width="8%"/>
                        </telerik:GridBoundColumn>

                        <telerik:GridTemplateColumn HeaderText="By User - Group" UniqueName="ByUser">
                            <HeaderStyle HorizontalAlign="Center" Width="25%"  />
                            <ItemStyle HorizontalAlign="Left" Width="25%"/>
                            <ItemTemplate>
                                <div>
                                    <%# DataBinder.Eval(Container.DataItem, "FullUserName") + " - " + DataBinder.Eval(Container.DataItem, "GroupName")  %>
                                </div>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        
                        
                    </Columns>
                </MasterTableView>
                <ClientSettings Selecting-AllowRowSelect="true" AllowColumnHide="True">
                    <Resizing EnableRealTimeResize="True" ResizeGridOnColumnResize="True" ClipCellContentOnResize="false"></Resizing>
                    <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="200" UseStaticHeaders="True" />
                </ClientSettings>
            </telerik:RadGrid>
            
            <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" >
        <Windows>
            <telerik:RadWindow ID="CustomerDialog" runat="server" Title="Document Information"
                VisibleStatusbar="true" Height="690" Width="650" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
        </div>
        <telerik:RadCodeBlock runat="server">
        </telerik:RadCodeBlock>
    </form>
</body>
</html>
