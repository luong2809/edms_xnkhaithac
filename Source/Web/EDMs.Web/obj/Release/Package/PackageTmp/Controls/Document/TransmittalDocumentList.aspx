﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TransmittalDocumentList.aspx.cs" Inherits="EDMs.Web.Controls.Document.TransmittalDocumentList" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../../Content/styles.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    <style type="text/css">
        
        html, body, form {
	        overflow:auto;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="content">
            <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
            <telerik:RadGrid ID="grdDocument" runat="server" AllowPaging="True" Height="332px"
                AutoGenerateColumns="False" CellPadding="0" CellSpacing="0"
                GridLines="None" 
                Skin="Windows7"
                OnNeedDataSource="grdDocument_OnNeedDataSource" 
                PageSize="30" Style="outline: none">
                <MasterTableView ClientDataKeyNames="ID" DataKeyNames="ID" Width="100%">
                    <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; Documents." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <Columns>
                        <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />
                       
                          <telerik:GridTemplateColumn AllowFiltering="false" UniqueName="DownloadColumn">
                                   <HeaderStyle Width="3%"/>
                                   <ItemStyle HorizontalAlign="Center" Width="3%" />
                                  <ItemTemplate>
                                 <a download='<%# DataBinder.Eval(Container.DataItem, "Name") %>' 
                                   href='<%# DataBinder.Eval(Container.DataItem, "FilePath") %>' target="_blank">
                               <asp:Image ID="CallLink" runat="server" ImageUrl='<%# "../../"+ DataBinder.Eval(Container.DataItem, "FileExtensionIcon") %>' Style="cursor: pointer;" AlternateText="Download document" /> 
                                                            </a>
                                                    </ItemTemplate>

                          </telerik:GridTemplateColumn>
                        
                         <telerik:GridBoundColumn DataField="Name" HeaderText="Name" UniqueName="Name">
                            <HeaderStyle HorizontalAlign="Center" Width="25%" />
                            <ItemStyle HorizontalAlign="Left" Width="25%" />
                        </telerik:GridBoundColumn>
                                            
                        <telerik:GridBoundColumn DataField="DocumentNumber" HeaderText="Document Number" UniqueName="DocumentNumber">
                            <HeaderStyle HorizontalAlign="Center" Width="12%" />
                            <ItemStyle HorizontalAlign="Left" Width="12%"/>
                        </telerik:GridBoundColumn>
                        
                        <telerik:GridBoundColumn DataField="Title" HeaderText="Title" UniqueName="Title">
                            <HeaderStyle HorizontalAlign="Center" Width="20%" />
                            <ItemStyle HorizontalAlign="Left" Width="20%"/>
                        </telerik:GridBoundColumn>
                        
                        <telerik:GridBoundColumn DataField="Revision.Name" HeaderText="Revision" UniqueName="RevisionName">
                            <HeaderStyle HorizontalAlign="Center" Width="6%" />
                            <ItemStyle HorizontalAlign="Left" Width="6%"/>
                        </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="DocumentTypeName" HeaderText="Document Type" UniqueName="DocumentType" 
                                                FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                                <HeaderStyle HorizontalAlign="Center" Width="150" />
                                                <ItemStyle HorizontalAlign="Left"/>
                                            </telerik:GridBoundColumn>
                        <%--<telerik:GridBoundColumn DataField="Status.Name" HeaderText="Status" UniqueName="Status">
                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                            <ItemStyle HorizontalAlign="Left" />
                        </telerik:GridBoundColumn>--%>

                      <%--  <telerik:GridBoundColumn DataField="ReceivedFrom.Name" HeaderText="Received from" UniqueName="ReceivedFrom">
                            <HeaderStyle HorizontalAlign="Center" Width="7%" />
                            <ItemStyle HorizontalAlign="Left" Width="7%"/>
                        </telerik:GridBoundColumn>--%>
                        
                     <%--   <telerik:GridBoundColumn DataField="ReceivedDate" HeaderText="Date" UniqueName="ReceivedDate" 
                            DataFormatString="{0:dd/MM/yyyy}" >
                            <HeaderStyle HorizontalAlign="Center" Width="8%" />
                            <ItemStyle HorizontalAlign="Center" Width="8%"/>
                        </telerik:GridBoundColumn>--%>
                        
                    </Columns>
                </MasterTableView>
                <ClientSettings >
                    <Resizing EnableRealTimeResize="True" ResizeGridOnColumnResize="True" ClipCellContentOnResize="false"></Resizing>
                    <Selecting AllowRowSelect="true" />
                    <Scrolling AllowScroll="True" SaveScrollPosition="True" UseStaticHeaders="True" />
                </ClientSettings>
            </telerik:RadGrid>
        </div>
        <telerik:RadCodeBlock runat="server">
        </telerik:RadCodeBlock>
    </form>
</body>
</html>
