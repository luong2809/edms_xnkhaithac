﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CopyPath.aspx.cs" Inherits="EDMs.Web.Controls.Document.CopyPath" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        html, body, form {
            overflow: auto;
        }

        #divContent {
            width: 450px;
            margin: 0 auto;
            padding: 30px 0 0 0;
        }
    </style>
    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>

        <script type="text/javascript">
            function CloseAndRebind(args) {
                GetRadWindow().BrowserWindow.refreshGrid(args);
                GetRadWindow().close();
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

                return oWindow;
            }

            function CancelEdit() {
                GetRadWindow().close();
            }
        </script>
    </telerik:RadScriptBlock>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
        <div id="divContent">
            <div style="text-align:center; margin-bottom:10px">
                <asp:TextBox ID="txtPath" Width="300px" TextMode="MultiLine" runat="server"></asp:TextBox>
            </div>
            <div style="text-align:center">
                <telerik:RadButton ID="btnCopy" runat="server" Text="Copy to Clipboard"></telerik:RadButton>
            </div>
        </div>

        <telerik:RadAjaxManager runat="Server" ID="ajaxDocument">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ajaxDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContent" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadScriptBlock ID="RadScriptBlock2" runat="server">
            <script>
                $(function () {
                    $("#btnCopy").click(function () {
                        var id = "#" + "<%= txtPath.ClientID %>";
                        try {
                            $(id).select();
                            document.execCommand("copy");
                        }
                        catch (e) {
                            alert('Copy operation failed');
                        }
                    });
                });
            </script>
        </telerik:RadScriptBlock>
    </form>
</body>
</html>
