﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditFolderType.aspx.cs" Inherits="EDMs.Web.Controls.Document.EditFolderType" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        html, body, form {
            overflow: auto;
        }

        .RadComboBoxDropDown_Default .rcbHovered {
            background-color: #46A3D3;
            color: #fff;
        }

        .RadComboBoxDropDown .rcbItem, .RadComboBoxDropDown .rcbHovered, .RadComboBoxDropDown .rcbDisabled, .RadComboBoxDropDown .rcbLoading, .RadComboBoxDropDown .rcbCheckAllItems, .RadComboBoxDropDown .rcbCheckAllItemsHovered {
            margin: 0 0px;
        }

        .RadComboBox .rcbInputCell .rcbInput {
            border-left-color: #FF0000 !important;
            border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;
            border-style: solid;
            border-width: 1px 1px 1px 5px;
            color: #000000;
            float: left;
            font: 12px "segoe ui";
            margin: 0;
            padding: 2px 5px 3px;
            vertical-align: middle;
            width: 200px;
        }

        .RadComboBox table td.rcbInputCell, .RadComboBox .rcbInputCell .rcbInput {
            padding-left: 0px !important;
        }

        div.rgEditForm label {
            float: right;
            text-align: right;
            width: 72px;
        }

        .rgEditForm {
            text-align: right;
        }

        .RadComboBox {
            width: 215px !important;
            border-bottom: none !important;
        }

        .RadUpload .ruFileWrap {
            overflow: visible !important;
        }

        /*#grdContactHistory
        {
            height: 300px !important;
        }

        #grdContactHistory_GridData
        {
            height: 250px !important;
        }*/
    </style>

    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>

    <script type="text/javascript">
        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refreshGrid(args);
            GetRadWindow().close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CancelEdit() {
            GetRadWindow().close();
        }


    </script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
        <div style="width: 100%">
            <ul style="list-style-type: none">
                <li style="width: 500px;" runat="server" id="UploadControl">
                    <div>
                        <label style="width: 110px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                            <span style="color: #2E5689; text-align: right;">Select Type Folder
                            </span>
                        </label>
                        <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                            <telerik:RadComboBox ID="cboType" runat="server" Skin="Windows7" Width="200px"></telerik:RadComboBox>
                        </div>
                    </div>
                </li>
            </ul>
        </div>



        <div style="width: 100%; text-align: center; padding-top: 70px">
            <telerik:RadButton ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" Width="70px" Style="text-align: center">
                <Icon PrimaryIconUrl="../../Images/save.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
            </telerik:RadButton>
        </div>
        <div style="width: 100%;" runat="server" id="blockError" visible="False">
            <div>
                <label style="width: 60px; float: left; padding-top: 5px; padding-right: 10px; text-align: right;">
                    <span style="color: red; text-align: right;">Warning:
                    </span>
                </label>
                <div style="float: left; padding-top: 5px;" class="qlcbFormItem">
                    <asp:Label runat="server" ID="lblError" Width="500"></asp:Label>
                </div>
            </div>
            <div style="clear: both; font-size: 0;"></div>
        </div>
        <asp:HiddenField runat="server" ID="docUploadedIsExist" />
        <asp:HiddenField runat="server" ID="docIdUpdateUnIsLeaf" />

        <telerik:RadAjaxManager runat="Server" ID="ajaxDocument">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ajaxDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContent" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>

        <telerik:RadScriptBlock runat="server">
            <script type="text/javascript">
                var ajaxManager;

                function fileUploading(sender, args) {
                    var name = args.get_fileName();
                    document.getElementById("txtName").value = name;

                    ajaxManager.ajaxRequest("CheckFileName$" + name);
                }

                //<![CDATA[
                var $ = $telerik.$;
                function pageLoad() {
                    ajaxManager = $find("<%=ajaxDocument.ClientID %>");

                    if (!Telerik.Web.UI.RadAsyncUpload.Modules.FileApi.isAvailable()) {
                        $(".qsf-demo-canvas").html("<strong>Your browser does not support Drag and Drop. Please take a look at the info box for additional information.</strong>");
                    }
                    else {
                        $(document).bind({ "drop": function (e) { e.preventDefault(); } });

                        var dropZone1 = $(document).find(".DropZone1");
                        dropZone1.bind({ "dragenter": function (e) { dragEnterHandler(e, dropZone1); } })
                                 .bind({ "dragleave": function (e) { dragLeaveHandler(e, dropZone1); } })
                                 .bind({ "drop": function (e) { dropHandler(e, dropZone1); } });

                    }
                }

                function dropHandler(e, dropZone) {
                    dropZone[0].style.backgroundColor = "#357A2B";
                }

                function dragEnterHandler(e, dropZone) {
                    var dt = e.originalEvent.dataTransfer;
                    var isFile = (dt.types != null && (dt.types.indexOf ? dt.types.indexOf('Files') != -1 : dt.types.contains('application/x-moz-file')));
                    if (isFile || $telerik.isSafari5 || $telerik.isIE10Mode || $telerik.isOpera)
                        dropZone[0].style.backgroundColor = "#000000";
                }

                function dragLeaveHandler(e, dropZone) {
                    if (!$telerik.isMouseOverElement(dropZone[0], e.originalEvent))
                        dropZone[0].style.backgroundColor = "#357A2B";
                }

                //]]>
            </script>

        </telerik:RadScriptBlock>
    </form>
</body>
</html>
