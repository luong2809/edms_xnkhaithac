﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// Class customer
    /// </summary>
    public partial class DataPermissionPage : Page
    {
        private readonly PermissionService permissionService = new PermissionService();

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string unreadPattern = @"\(\d+\)";

        private readonly GroupDataPermissionService groupDataPermissionService = new GroupDataPermissionService();

        private readonly CategoryService categoryService = new CategoryService();

        private readonly FolderService folderService = new FolderService();

        private readonly RoleService roleService = new RoleService();

        private readonly UserDataPermissionService userDataPermissionService = new UserDataPermissionService();

        private readonly UserService userService = new UserService();

        private List<int> AdminGroup
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("GroupAdminList").Split(',').Select(t => Convert.ToInt32(t)).ToList();
            }
        }
        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var categoryPermission = this.groupDataPermissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault()).Select(t => t.CategoryIdList).ToList();

                categoryPermission = categoryPermission.Union(this.userDataPermissionService.GetByUserId(UserSession.Current.User.Id).Select(t => t.CategoryId.ToString())).Distinct().ToList();
                var listCategory = this.AdminGroup.Contains(UserSession.Current.RoleId)
                                    ? this.categoryService.GetAll()
                                    : this.categoryService.GetAll().Where(t => categoryPermission.Any(x => x == t.ID.ToString())).ToList();
                if (listCategory.Any())
                {
                    foreach (var category in listCategory)
                    {
                        category.ParentId = -1;
                    }
                    this.ddlCategory.DataSource = listCategory;
                    this.ddlCategory.DataTextField = "Name";
                    this.ddlCategory.DataValueField = "ID";
                    this.ddlCategory.DataBind();

                    listCategory.Insert(0, new Category() { ID = -1, Name = "DOCUMENT GROUP" });
                    this.radPbCategories.DataSource = listCategory;
                    this.radPbCategories.DataFieldParentID = "ParentId";
                    this.radPbCategories.DataFieldID = "Id";
                    this.radPbCategories.DataValueField = "Id";
                    this.radPbCategories.DataTextField = "Name";
                    this.radPbCategories.DataBind();
                    this.radPbCategories.Items[0].Expanded = true;

                    foreach (RadPanelItem item in this.radPbCategories.Items[0].Items)
                    {
                        item.ImageUrl = @"Images/category2.png";
                        item.NavigateUrl = "DocumentsHome.aspx?doctype=" + item.Value;
                    }
                }
                this.LoadFolderTree();
                this.LoadListPanel();
                this.LoadSystemPanel();
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
            }
        }
        
        private void LoadListPanel()
        {
            var listId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("ListID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), listId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "LIST" });

                this.radPbList.DataSource = permissions;
                this.radPbList.DataFieldParentID = "ParentId";
                this.radPbList.DataFieldID = "Id";
                this.radPbList.DataValueField = "Id";
                this.radPbList.DataTextField = "MenuName";
                this.radPbList.DataBind();
                this.radPbList.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbList.Items[0].Items)
                {
                    item.ImageUrl = @"Images/listmenu.png";
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                    if (item.Text == "Data Permission")
                    {
                        item.Selected = true;
                    }
                }
            }
        }

        private void LoadSystemPanel()
        {
            var systemId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("SystemID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), systemId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "SYSTEM" });
                this.radPbSystem.DataSource = permissions;
                this.radPbSystem.DataFieldParentID = "ParentId";
                this.radPbSystem.DataFieldID = "Id";
                this.radPbSystem.DataValueField = "Id";
                this.radPbSystem.DataTextField = "MenuName";
                this.radPbSystem.DataBind();
                this.radPbSystem.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbSystem.Items[0].Items)
                {
                    item.ImageUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Icon;
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.radTreeFolder.SelectedNode != null)
            {
                var listFolderId = new List<int>();
                var selectedFolderId = this.radTreeFolder.SelectedNode.Value;
                var selectedFolder = this.folderService.GetById(Convert.ToInt32(selectedFolderId));

                var selectedGroup = this.ddlGroup.SelectedValue;
                var selectedUser = string.Empty;
                selectedUser = this.ddlUser.CheckedItems.Aggregate(selectedUser, (current, t) => current + t.Value + ", ");

                if (selectedFolder != null)
                {
                    var categoryId = selectedFolder.CategoryID;
                    
                    var listParentFolderId = this.GetAllParentID(Convert.ToInt32(selectedFolderId), listFolderId);
                    listParentFolderId.Add(Convert.ToInt32(selectedFolderId));

                    var allChildNodes = this.radTreeFolder.SelectedNode.GetAllNodes().Select(t => t.Value).ToList();
                    
                    if (string.IsNullOrEmpty(selectedUser))
                    {
                        var groupDatapermission =
                            this.groupDataPermissionService.GetByRoleId(Convert.ToInt32(selectedGroup));

                        var addingPermission =
                            listParentFolderId.Where(
                                t => !groupDatapermission.Select(x => x.FolderIdList).Contains(t.ToString())).Select(
                                    t =>
                                    new GroupDataPermission()
                                    {
                                        CategoryIdList = categoryId.ToString(),
                                        RoleId = Convert.ToInt32(selectedGroup),
                                        FolderIdList = t.ToString(),
                                        ReadFile = this.cbRead.Checked,
                                        ModifyFile = this.cbModify.Checked,
                                        DeleteFile = this.cbDelete.Checked,
                                        CreateFolder = this.cbCreate.Checked,
                                        IsFullPermission = this.cbFull.Checked,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = UserSession.Current.User.Id
                                    }).ToList();

                        if (this.cbApplyAll.Checked)
                        {
                            addingPermission.AddRange(
                                allChildNodes.Select(
                                    t =>
                                    new GroupDataPermission()
                                    {
                                        CategoryIdList = categoryId.ToString(),
                                        RoleId = Convert.ToInt32(selectedGroup),
                                        FolderIdList = t,
                                        ReadFile = this.cbRead.Checked,
                                        ModifyFile = this.cbModify.Checked,
                                        DeleteFile = this.cbDelete.Checked,
                                        CreateFolder = this.cbCreate.Checked,
                                        IsFullPermission = this.cbFull.Checked,
                                        CreatedDate = DateTime.Now,
                                        CreatedBy = UserSession.Current.User.Id
                                    }));
                        }

                        this.groupDataPermissionService.AddGroupDataPermissions(addingPermission.ToList());
                    }
                    else
                    {
                       // var listuser=selectedUser.Split(',').Select(Convert.ToInt32).ToList();
                        foreach (var userid in this.ddlUser.CheckedItems)
                        {
                            var userDatapermission =
                           this.userDataPermissionService.GetByUserId(Convert.ToInt32(userid.Value));
                            var addingPermission =
                                listParentFolderId.Where(
                                    t => !userDatapermission.Select(x => x.FolderId.ToString()).Contains(t.ToString())).Select(
                                        t =>
                                        new UserDataPermission()
                                        {
                                            CategoryId = categoryId,
                                            RoleId = Convert.ToInt32(selectedGroup),
                                            FolderId = t,
                                            UserId = Convert.ToInt32(selectedUser),
                                            ReadFile = this.cbRead.Checked,
                                            ModifyFile = this.cbModify.Checked,
                                            DeleteFile = this.cbDelete.Checked,
                                            CreateFolder = this.cbCreate.Checked,
                                            IsFullPermission = this.cbFull.Checked,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = UserSession.Current.User.Id
                                        }).ToList();

                            if (this.cbApplyAll.Checked)
                            {
                                addingPermission.AddRange(
                                    allChildNodes.Select(
                                        t =>
                                        new UserDataPermission()
                                        {
                                            CategoryId = categoryId,
                                            RoleId = Convert.ToInt32(selectedGroup),
                                            FolderId = Convert.ToInt32(t),
                                            UserId = Convert.ToInt32(selectedUser),
                                            ReadFile = this.cbRead.Checked,
                                            ModifyFile = this.cbModify.Checked,
                                            DeleteFile = this.cbDelete.Checked,
                                            CreateFolder = this.cbCreate.Checked,
                                            IsFullPermission = this.cbFull.Checked,
                                            CreatedDate = DateTime.Now,
                                            CreatedBy = UserSession.Current.User.Id
                                        }));
                            }

                            this.userDataPermissionService.AddUserDataPermissions(addingPermission.ToList());
                        }
                       
                    }

                    this.LoadPermissionData(selectedFolderId);
                    this.grdPermission.Rebind();
                }
            }
        }

        protected void ddlCategory_SelectecIndexChange(object sender, EventArgs e)
        {
            this.LoadFolderTree();
            this.ddlGroup.Items.Clear();
            this.ddlUser.Items.Clear();
            this.grdPermission.Rebind();
        }

        protected void radTreeFolder_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            this.LoadPermissionData(e.Node.Value);
            this.grdPermission.Rebind();
        }

        protected void radTreeFolder_OnNodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = "Images/folderdir16.png";
        }

        private List<int> GetAllParentID(int folderId, List<int> listFolderId)
        {
            var folder = this.folderService.GetById(folderId);
            if (folder.ParentID != null)
            {
                listFolderId.Add(folder.ParentID.Value);
                this.GetAllParentID(folder.ParentID.Value, listFolderId);
            }

            return listFolderId;
        }

        private void LoadPermissionData(string folderId)
        {
            var groupsInPermission = this.groupDataPermissionService.GetAllByFolder(folderId).Select(t => t.RoleId).Distinct().ToList();
            var listGroup = this.roleService.GetAll().Where(t => !groupsInPermission.Contains(t.Id) && !this.AdminGroup.Contains(t.Id)).ToList();
            listGroup.Insert(0, new Role { Id = 0 });
            this.ddlGroup.DataSource = listGroup;
            this.ddlGroup.DataTextField = "Name";
            this.ddlGroup.DataValueField = "Id";
            this.ddlGroup.DataBind();

            var usersInPermission = this.userDataPermissionService.GetAllByFolder(Convert.ToInt32(this.radTreeFolder.SelectedNode.Value)).Select(t => t.UserId).Distinct().ToList();
            var listUser = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroup.SelectedValue)).Where(t => !usersInPermission.Contains(t.Id)).ToList();

          //  listUser.Insert(0, new User { Id = 0, FullName = string.Empty });

            this.ddlUser.DataSource = listUser;
            this.ddlUser.DataTextField = "FullName";
            this.ddlUser.DataValueField = "Id";
            this.ddlUser.DataBind();
        }

        private void LoadFolderTree()
        {
            this.radTreeFolder.Nodes.Clear();
            var listFolder = new List<Folder>();
            if (this.AdminGroup.Contains(UserSession.Current.RoleId))
            {
                listFolder = this.folderService.GetAllByCategory(Convert.ToInt32(this.ddlCategory.SelectedValue));
            }
            else
            {
                var folderPermission =
                this.groupDataPermissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault())
                .Where(t => t.CategoryIdList == this.ddlCategory.SelectedValue
                        && !string.IsNullOrEmpty(t.FolderIdList))
                .Select(t => Convert.ToInt32(t.FolderIdList)).OrderBy(t => t).ToList();

                listFolder = this.folderService.GetSpecificFolderStatic(folderPermission);
            }
            

            this.radTreeFolder.DataTextField = "Name";
            this.radTreeFolder.DataValueField = "ID";
            this.radTreeFolder.DataFieldID = "ID";
            this.radTreeFolder.DataFieldParentID = "ParentID";

            this.radTreeFolder.DataSource = listFolder;
            this.radTreeFolder.DataBind();
            this.radTreeFolder.Nodes[0].Expanded = true;

            this.SortNodes(this.radTreeFolder.Nodes);
        }

        //SortNodes is a recursive method enumerating and sorting all node levels 
        private void SortNodes(RadTreeNodeCollection collection)
        {
            Sort(collection);
            foreach (RadTreeNode node in collection)
            {
                if (node.Nodes.Count > 0)
                {
                    SortNodes(node.Nodes);
                }
            }
        }

        //The Sort method is called for each node level sorting the child nodes 
        public void Sort(RadTreeNodeCollection collection)
        {
            RadTreeNode[] nodes = new RadTreeNode[collection.Count];
            collection.CopyTo(nodes, 0);
            Array.Sort(nodes, new TreeNodeComparer());
            collection.Clear();
            collection.AddRange(nodes);
        }

        protected void grdPermission_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if(this.radTreeFolder.SelectedNode != null)
            {
                var groupsInPermission = this.groupDataPermissionService.GetAllByFolder(this.radTreeFolder.SelectedNode.Value)
                                            .Where(t => !this.AdminGroup.Contains(t.RoleId.GetValueOrDefault()));
                var usersInPermission = this.userDataPermissionService.GetAllByFolder(Convert.ToInt32(this.radTreeFolder.SelectedNode.Value));

                var listDataPermission = usersInPermission
                                        .Select(t => new DataPermission
                                        {
                                            ID = t.ID,
                                            ReadFile = t.ReadFile.GetValueOrDefault(),
                                            ModifyFile = t.ModifyFile.GetValueOrDefault(),
                                            DeleteFile = t.DeleteFile.GetValueOrDefault(),
                                            CreateFolder = t.CreateFolder.GetValueOrDefault(),
                                            IsFullPermission = t.IsFullPermission.GetValueOrDefault(),
                                            Name = t.UserFullName,
                                            IsGroup = false
                                        }).ToList();

                listDataPermission = listDataPermission.Union(
                    groupsInPermission.Select(
                        t =>
                        new DataPermission
                        {
                            ID = t.ID,
                            ReadFile = t.ReadFile.GetValueOrDefault(),
                            ModifyFile = t.ModifyFile.GetValueOrDefault(),
                            DeleteFile = t.DeleteFile.GetValueOrDefault(),
                            CreateFolder = t.CreateFolder.GetValueOrDefault(),
                            IsFullPermission = t.IsFullPermission.GetValueOrDefault(),
                            Name = t.GroupName,
                            IsGroup = true
                        })).ToList();

                this.grdPermission.DataSource = listDataPermission;
            }
            else
            {
                this.grdPermission.DataSource = new List<DataPermission>();    
            }
            
        }

        protected void grdPermission_OnDeteleCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var permissionId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var isGroup = Convert.ToBoolean(item["IsGroup"].Text);
            var selectedFolderId = this.radTreeFolder.SelectedNode.Value;

            var allChildNodes = this.radTreeFolder.SelectedNode.GetAllNodes().Select(t => t.Value).ToList();
            allChildNodes.Add(selectedFolderId);

            if (isGroup)
            {
                var groupPermission = this.groupDataPermissionService.GetById(permissionId);
                if (groupPermission != null)
                {
                    var groupDatapermission = this.groupDataPermissionService.GetByRoleId(groupPermission.RoleId.GetValueOrDefault());
                    

                    var deletePermission = groupDatapermission.Where(t => allChildNodes.Contains(t.FolderIdList));
                    this.groupDataPermissionService.DeleteGroupDataPermission(deletePermission.ToList());
                }
            }
            else
            {
                var userPermission = this.userDataPermissionService.GetById(permissionId);
                if (userPermission != null)
                {
                    var userDatapermission = this.userDataPermissionService.GetByUserId(userPermission.UserId.GetValueOrDefault());
                    var deletePermission = userDatapermission.Where(t => allChildNodes.Contains(t.FolderId.ToString()));
                    this.userDataPermissionService.DeleteUserDataPermission(deletePermission.ToList());
                }
            }

            this.LoadPermissionData(selectedFolderId);
        }

        protected void ddlGroup_SelectedIndexChange(object sender, EventArgs e)
        {
            var usersInPermission = this.userDataPermissionService.GetAllByFolder(Convert.ToInt32(this.radTreeFolder.SelectedNode.Value)).Select(t => t.UserId).Distinct().ToList();
            var listUser = this.userService.GetAllByRoleId(Convert.ToInt32(this.ddlGroup.SelectedValue)).Where(t => !usersInPermission.Contains(t.Id)).ToList();

           /// listUser.Insert(0, new User { Id = 0, FullName = string.Empty });

            this.ddlUser.DataSource = listUser;
            this.ddlUser.DataTextField = "FullName";
            this.ddlUser.DataValueField = "Id";
            this.ddlUser.DataBind();
        }

        protected void cbFull_CheckedChanged(object sender, EventArgs e)
        {
            if (cbFull.Checked)
            {
                cbRead.Checked = true;
                cbModify.Checked = true;
                cbDelete.Checked = true;
                cbCreate.Checked = true;
            }
            else
            {
                cbRead.Checked = false;
                cbModify.Checked = false;
                cbDelete.Checked = false;
                cbCreate.Checked = false;
            }
        }

        protected void cbModify_CheckedChanged(object sender, EventArgs e)
        {
            if (cbModify.Checked)
            {
                cbRead.Checked = true;
            }
        }

        protected void cbDelete_CheckedChanged(object sender, EventArgs e)
        {
            if (cbDelete.Checked)
            {
                cbRead.Checked = true;
            }
        }
    }
}

