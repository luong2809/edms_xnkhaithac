﻿using EDMs.Business.Services;
using EDMs.Web.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EDMs.Web.Utilities.Sessions;

namespace EDMs.Web.Controls.Appointments
{
    /// <summary>
    /// The rad scheduler advanced form advanced form mode.
    /// </summary>
    public enum RadSchedulerAdvancedFormAdvancedFormMode
    {
        Insert,
        Edit
    }

    /// <summary>
    /// The advanced form.
    /// </summary>
    public partial class AdvancedForm : System.Web.UI.UserControl
    {
        /// <summary>
        /// The _patient service.
        /// </summary>
        private readonly PatientService _patientService;

        /// <summary>
        /// The _appointment status service.
        /// </summary>
        private readonly AppointmentStatusService _appointmentStatusService;

        /// <summary>
        /// The _resource service.
        /// </summary>
        private readonly ResourceService _resourceService;

        /// <summary>
        /// The _room service.
        /// </summary>
        private readonly RoomService _roomService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedForm"/> class.
        /// </summary>
        public AdvancedForm()
        {
            _patientService = new PatientService();
            _appointmentStatusService = new AppointmentStatusService();
            _resourceService = new ResourceService();
            _roomService = new RoomService();
        }

        #region Private members

        /// <summary>
        /// Gets or sets a value indicating whether form initialized.
        /// </summary>
        private bool FormInitialized
        {
            get
            {
                return (bool)(ViewState["FormInitialized"] ?? false);
            }

            set
            {
                ViewState["FormInitialized"] = value;
            }
        }

        /// <summary>
        /// The mode.
        /// </summary>
        private RadSchedulerAdvancedFormAdvancedFormMode mode = RadSchedulerAdvancedFormAdvancedFormMode.Insert;

        #endregion

        #region Protected properties
        protected RadScheduler Owner
        {
            get
            {
                return Appointment.Owner;
            }
        }

        protected Appointment Appointment
        {
            get
            {
                SchedulerFormContainer container = (SchedulerFormContainer)BindingContainer;
                return container.Appointment;
            }
        }

        #endregion

        #region Public properties

        /// <summary>
        /// Gets or sets the mode.
        /// </summary>
        public RadSchedulerAdvancedFormAdvancedFormMode Mode
        {
            get
            {
                return mode;
            }
            set
            {
                mode = value;
            }
        }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public string Code
        {
            get
            {
                return txtCode.Text;
            }

            set
            {
                if (Mode == RadSchedulerAdvancedFormAdvancedFormMode.Insert)
                {
                    txtCode.Text = Utility.GenerateApptCode();
                }
                else
                {
                    txtCode.Text = value;
                }
                
            }
        }

        /// <summary>
        /// Gets or sets the set display name.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public string SetDisplayName
        {
            get
            {
                return txtName.Text;
            }

            set
            {
                txtName.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets the patient id.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public string PatientId
        {
            get
            {
                if (!string.IsNullOrEmpty(cboPatient.SelectedValue))

                    foreach (RadComboBoxItem item in cboPatient.Items)
                    {
                        if (item.Text == cboPatient.Text)
                        {
                            return item.Value;
                        }
                    }                  

                return string.Empty;
            }

            set
            {
                if (!string.IsNullOrEmpty(value))
                    cboPatient.SelectedValue = value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the next call.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public string NextCall
        {
            get
            {
                return dteNextCall.SelectedDate.ToString();
            }

            set
            {
                if (!string.IsNullOrEmpty(value) && !value.Contains("1/1/0001"))
                    dteNextCall.SelectedDate = DateTime.Parse(value);
            }
        }

        /// <summary>
        /// Gets or sets the last call.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public string LastCall
        {
            get
            {
                return dteLastCall.SelectedDate.ToString();
            }

            set
            {
                if (!string.IsNullOrEmpty(value) && !value.Contains("1/1/0001"))
                    dteLastCall.SelectedDate = DateTime.Parse(value);
            }
        }

        /// <summary>
        /// Gets or sets the number call.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public string NumberCall
        {
            get
            {
                return numCalled.Value.ToString();
            }

            set
            {
                numCalled.Value = string.IsNullOrEmpty(value) ? 0 : double.Parse(value);
            }
        }

        /// <summary>
        /// Gets or sets the service type id.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public string ServiceTypeId
        {
            get
            {
                var ids = string.Empty;

                foreach (RadListBoxItem item in cboService.Items)
                {
                    if (item.Checked)
                    {
                        if (ids == string.Empty)
                        {
                            ids += item.Value;
                        }
                        else
                        {
                            ids += "," + item.Value;
                        }
                    }
                }
                return ids;
            }
            set
            { 
                if (!string.IsNullOrEmpty(value))
                {
                    List<string> selectedIds = value.Split(',').ToList();
                    foreach (RadListBoxItem item in cboService.Items)
                    {
                        if (selectedIds.Contains(item.Value))
                        {
                            item.Checked = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the appointment status id.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public string AppointmentStatusId
        {
            get
            {
                if (!string.IsNullOrEmpty(cboApptStatus.SelectedValue))
                {
                    foreach (RadComboBoxItem item in cboApptStatus.Items)
                    {
                        if (item.Text == cboApptStatus.Text)
                        {
                            return item.Value;
                        }
                    }            
                }
                return string.Empty;
            }

            set
            {
                if (!string.IsNullOrEmpty(value))
                    cboApptStatus.SelectedValue = value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the resource id.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public string ResourceId
        {
            get
            {
                if (!string.IsNullOrEmpty(cboResources.SelectedValue))
                {
                    foreach (RadComboBoxItem item in cboResources.Items)
                    {
                        if (item.Text == cboResources.Text)
                        {
                            return item.Value;
                        }
                    }            
                }
                return string.Empty;
            }

            set
            {
                if (!string.IsNullOrEmpty(value))
                    cboResources.SelectedValue = value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the start.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public DateTime Start
        {
            get
            {
                DateTime result = StartDate.SelectedDate.Value.Date;
                TimeSpan time = StartTime.SelectedDate.Value.TimeOfDay;
                result = result.Add(time);

                return Owner.DisplayToUtc(result);
            }
            set
            {
                StartDate.SelectedDate = value;
                StartTime.SelectedDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the end.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public DateTime End
        {
            get
            {
                DateTime result = EndDate.SelectedDate.Value.Date;
                TimeSpan time = EndTime.SelectedDate.Value.TimeOfDay;
                result = result.Add(time);

                return result;
            }
            set
            {
                EndDate.SelectedDate = value;
                EndTime.SelectedDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public string Description
        {
            get
            {
                return txtDescription.Text.Trim();
            }

            set
            {
                txtDescription.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets the room id.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public string RoomId
        {
            get
            {
                if (!string.IsNullOrEmpty(this.cboRoom.SelectedValue))
                {
                    foreach (RadComboBoxItem item in cboRoom.Items)
                    {
                        if (item.Text == cboRoom.Text)
                        {
                            return item.Value;
                        }
                    }
                }
                return string.Empty;
            }

            set
            {
                if (!string.IsNullOrEmpty(value))
                    this.cboRoom.SelectedValue = value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the reminder.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public string Reminder
        {
            get
            {
                if (ReminderDropDown.SelectedValue != string.Empty)
                {
                    return ReminderDropDown.SelectedValue;
                }
                return string.Empty;
            }

            set
            {
                RadComboBoxItem item = ReminderDropDown.Items.FindItemByValue(value.ToString());
                if (item != null)
                {
                    item.Selected = true;
                }
            }
        }

        /// <summary>
        /// Gets or sets the reminder id.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public string ReminderId
        {
            get
            {
                if (ReminderDropDown.SelectedValue != string.Empty)
                {
                    return ReminderDropDown.SelectedValue;
                }
                return string.Empty;
            }

            set
            {
                RadComboBoxItem item = ReminderDropDown.Items.FindItemByValue(value.ToString());
                if (item != null)
                {
                    item.Selected = true;
                }
            }
        }

        /// <summary>
        /// Gets or sets the recurrence rule text.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public string RecurrenceRuleText
        {
            get
            {
                AppointmentRecurrenceEditor.StartDate = Start;
                AppointmentRecurrenceEditor.EndDate = End;

                RecurrenceRule rrule = AppointmentRecurrenceEditor.RecurrenceRule;

                if (rrule == null)
                {
                    return string.Empty;
                }

                RecurrenceRule originalRule;
                if (RecurrenceRule.TryParse(OriginalRecurrenceRule.Value, out originalRule))
                {
                    rrule.Exceptions = originalRule.Exceptions;
                }

                if (rrule.Range.RecursUntil != DateTime.MaxValue)
                {
                    rrule.Range.RecursUntil = Owner.DisplayToUtc(rrule.Range.RecursUntil);
                }
                return rrule.ToString();
            }
            set
            {
                RecurrenceRule rrule;
                RecurrenceRule.TryParse(value, out rrule);

                if (rrule != null)
                {
                    if (rrule.Range.RecursUntil != DateTime.MaxValue)
                    {
                        DateTime recursUntil = Owner.UtcToDisplay(rrule.Range.RecursUntil);

                        if (!IsAllDayAppointment(Appointment))
                        {
                            recursUntil = recursUntil.AddDays(-1);
                        }

                        rrule.Range.RecursUntil = recursUntil;
                    }

                    AppointmentRecurrenceEditor.RecurrenceRule = rrule;

                    OriginalRecurrenceRule.Value = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public string CreatedBy
        {
            get
            {
                return CreatedByHidden.Value;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    CreatedByHidden.Value = UserSession.Current.User.Id.ToString();
                }
                else
                {
                    CreatedByHidden.Value = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the update by.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public string UpdateBy
        {
            get
            {
                return UpdateByHidden.Value;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    UpdateByHidden.Value = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the last update.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public string LastUpdate
        {
            get
            {
                return this.LastUpdateHidden.Value;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    LastUpdateHidden.Value = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the is block.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public string IsBlock
        {
            get
            {
                if (string.IsNullOrEmpty(this.IsBlockHidden.Value))
                {
                    return "False";
                }
                else
                {
                    return "True";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    IsBlockHidden.Value = value;
                }

            }
        }

        /// <summary>
        /// Gets or sets the resource others.
        /// Y Ta va Tu van vien
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public string ResourceOthers
        {
            get
            {
                var ids = string.Empty;

                foreach (RadListBoxItem item in listResourceOthers.CheckedItems)
                {
                    if (item.Checked)
                    {
                        if (ids == string.Empty)
                        {
                            ids += item.Value;
                        }
                        else
                        {
                            ids += "," + item.Value;
                        }
                    }
                }
                return ids;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    List<string> selectedIds = value.Split(',').ToList();
                    foreach (RadListBoxItem item in listResourceOthers.Items)
                    {
                        if (selectedIds.Contains(item.Value))
                        {
                            item.Checked = true;
                        }
                    }

                    this.ResourceOthersOrginal = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the parent appointment id.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public string ParentAppointmentId
        {
            get
            {
                return ParentAppointmentIdHidden.Value;
            }

            set
            {
                ParentAppointmentIdHidden.Value = value;
            }
        }

        /// <summary>
        /// Gets or sets the resource others orginal.
        /// </summary>
        [Bindable(BindableSupport.Yes, BindingDirection.TwoWay)]
        public string ResourceOthersOrginal
        {
            get
            {
                return ResourceOthersOrginalHidden.Value;
            }
            set
            {
                this.ResourceOthersOrginalHidden.Value = value;
            }
        }
        #endregion
        
        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            UpdateButton.ValidationGroup = Owner.ValidationGroup;
            CancelButton.ValidationGroup = Owner.ValidationGroup;

            txtCode.ValidationGroup = Owner.ValidationGroup;
            txtName.ValidationGroup = Owner.ValidationGroup;
            cboService.ValidationGroup = Owner.ValidationGroup;

            UpdateButton.CommandName = Mode == RadSchedulerAdvancedFormAdvancedFormMode.Edit ? "Update" : "Insert";

            InitializeStrings();
            InitializeRecurrenceEditor();

            // load Patient
            this.cboPatient.DataSource = _patientService.GetAll();
            this.cboPatient.DataTextField = "ExtensionName";
            this.cboPatient.DataValueField = "Id";
            this.cboPatient.DataBind();

            // load appt status
            this.cboApptStatus.DataSource = _appointmentStatusService.GetAll();
            this.cboApptStatus.DataTextField = "Description";
            this.cboApptStatus.DataValueField = "Id";
            this.cboApptStatus.DataBind();

            // load Resources
            this.cboResources.DataSource = _resourceService.FindByIsResource(true);
            this.cboResources.DataTextField = "FullName";
            this.cboResources.DataValueField = "Id";
            this.cboResources.DataBind();

            // load Room
            this.cboRoom.DataSource = _roomService.GetAll();
            this.cboRoom.DataTextField = "Name";
            this.cboRoom.DataValueField = "Id";
            this.cboRoom.DataBind();

            this.LoadResourceOthers();
        }

        /// <summary>
        /// The on pre render.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (!FormInitialized)
            {
                if (IsAllDayAppointment(Appointment))
                {
                    var selectedDate = this.EndDate.SelectedDate;
                    if (selectedDate != null)
                    {
                        this.EndDate.SelectedDate = selectedDate.Value.AddDays(-1);
                    }
                }

                FormInitialized = true;
            }
            this.LoadThongTinNguoiTaoAppt();
            
            // Neu Edit appt ko cho them/xoa Tu van vien/Y Ta
            if (this.Mode == RadSchedulerAdvancedFormAdvancedFormMode.Edit)
            {
               
                if (!string.IsNullOrEmpty(this.Appointment.Attributes["ParentAppointmentId"]))
                {
                    this.txtCode.Enabled = false;
                    this.txtName.Enabled = false;

                    this.dteLastCall.Enabled = false;
                    this.dteNextCall.Enabled = false;
                    this.numCalled.Enabled = false;

                    this.StartDate.Enabled = false;
                    this.StartTime.Enabled = false;

                    this.EndDate.Enabled = false;
                    this.EndTime.Enabled = false;

                    this.cboService.Enabled = false;
                    this.txtDescription.Enabled = false;
                    this.ReminderDropDown.Enabled = false;
                    this.AppointmentRecurrenceEditor.Enabled = false;

                    this.cboPatient.Enabled = false;
                    this.cboRoom.Enabled = false;
                    this.cboResources.Enabled = false;
                    this.cboApptStatus.Enabled = false;
                    this.listResourceOthers.Enabled = false;
                    this.lblNguoiSua.Enabled = false;

                    this.UpdateButton.Enabled = false;
                }
            }
        }

        /// <summary>
        /// The basic controls panel_ data binding.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void BasicControlsPanel_DataBinding(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// The duration validator_ on server validate.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        protected void DurationValidator_OnServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = (End - Start) > TimeSpan.Zero;
        }

        /// <summary>
        /// The initialize strings.
        /// </summary>
        private void InitializeStrings()
        {
            CodeValidator.ValidationGroup = Owner.ValidationGroup;
            NameValidator.ValidationGroup = Owner.ValidationGroup;

            StartDateValidator.ErrorMessage = Owner.Localization.AdvancedStartDateRequired;
            StartDateValidator.ValidationGroup = Owner.ValidationGroup;

            StartTimeValidator.ErrorMessage = Owner.Localization.AdvancedStartTimeRequired;
            StartTimeValidator.ValidationGroup = Owner.ValidationGroup;

            EndDateValidator.ErrorMessage = Owner.Localization.AdvancedEndDateRequired;
            EndDateValidator.ValidationGroup = Owner.ValidationGroup;

            EndTimeValidator.ErrorMessage = Owner.Localization.AdvancedEndTimeRequired;
            EndTimeValidator.ValidationGroup = Owner.ValidationGroup;

            // RequiredFieldValidator1_cboService.ValidationGroup = Owner.ValidationGroup;
            SharedCalendar.FastNavigationSettings.OkButtonCaption = Owner.Localization.AdvancedCalendarOK;
            SharedCalendar.FastNavigationSettings.CancelButtonCaption = Owner.Localization.AdvancedCalendarCancel;
            SharedCalendar.FastNavigationSettings.TodayButtonCaption = Owner.Localization.AdvancedCalendarToday;
        }

        /// <summary>
        /// The initialize recurrence editor.
        /// </summary>
        private void InitializeRecurrenceEditor()
        {
            AppointmentRecurrenceEditor.SharedCalendar = SharedCalendar;
            AppointmentRecurrenceEditor.Culture = Owner.Culture;
            AppointmentRecurrenceEditor.StartDate = Appointment.Start;
            AppointmentRecurrenceEditor.EndDate = Appointment.End;
        }
        
        /// <summary>
        /// The is all day appointment.
        /// </summary>
        /// <param name="appointment">
        /// The appointment.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool IsAllDayAppointment(Appointment appointment)
        {
            DateTime displayStart = appointment.Start;
            DateTime displayEnd = appointment.End;
            return displayStart.CompareTo(displayStart.Date) == 0 && displayEnd.CompareTo(displayEnd.Date) == 0;
        }

        /// <summary>
        /// The load thong tin nguoi tao appt.
        /// </summary>
        protected void LoadThongTinNguoiTaoAppt()
        {
            var userId = this.Mode == RadSchedulerAdvancedFormAdvancedFormMode.Edit ? Convert.ToInt16(this.UpdateByHidden.Value) : UserSession.Current.User.Id;
            string text = "Người sửa: ";

            var userService = new UserService();
            var resourceService = new ResourceService();
            var currentUser = userService.GetByID(userId);

            if (currentUser != null)
            {
                var resource = resourceService.GetByID(currentUser.ResourceId.Value);
                if (resource != null)
                {
                    text = text + resource.FullName;

                    if (!string.IsNullOrEmpty(this.LastUpdateHidden.Value))
                    {
                        text += " - " + Convert.ToDateTime(this.LastUpdateHidden.Value).ToString("dd/M/yyyy") + " " + Convert.ToDateTime(this.LastUpdateHidden.Value).ToShortTimeString();
                    }
                    this.lblNguoiSua.Text = text;
                }
                else
                {
                    this.lblNguoiSua.Text = text + UserSession.Current.User.Username;
                }
            }
            else
            {
                this.lblNguoiSua.Text = text + UserSession.Current.User.Username;
            }
        }

        /// <summary>
        /// The load resource others.
        /// </summary>
        private void LoadResourceOthers()
        {
            var listOthers = _resourceService.GetByResourceGroup(6);

            listOthers.AddRange(_resourceService.GetByResourceGroup(5));

            foreach (var item in listOthers)
            {
                var radItem = new RadListBoxItem();
                radItem.Value = item.Id.ToString();
                radItem.Text = item.FullName;

                this.listResourceOthers.Items.Add(radItem);
            }
        }
    }
}