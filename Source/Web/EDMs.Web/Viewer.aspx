﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Viewer.aspx.cs" Inherits="EDMs.Web.Viewer" %>
<!DOCTYPE html>

<html>
<head runat="server">
     <script type="text/javascript">
        //function CloseAndRebind(args) {
        //    GetRadWindow().BrowserWindow.refreshGrid(args);
        //    GetRadWindow().close();
        //}
        //function CloseWindow() {
        //    window.close();
        //}
        //function GetRadWindow() {
        //    var oWindow = null;
        //    if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
        //    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

        //    return oWindow;
        //}

        //function CancelEdit() {
        //    GetRadWindow().close();
        //}
        //function showWin() {
        //    $find("RadWindow1").show();
        //}
        //function GetRadWindow() {
        //    var oWindow = null;
        //    if (window.radWindow)
        //        oWindow = window.radWindow;
        //    else if (window.frameElement && window.frameElement.radWindow)
        //        oWindow = window.frameElement.radWindow;
        //    return oWindow;
        //}
        //function CloseModal(args) {
        //    var oWnd = GetRadWindow();
        //    if (oWnd) {
        //        oWnd.BrowserWindow.refreshGrid(args);
        //        setTimeout(function () { oWnd.close(); }, 0);
        //    }
        //}

            </script>
    <style type="text/css">
/*#documentViewer
{

    height: 700px;
    width: 98% !important;
}*/
    </style>
    <title>Overview</title>
</head>
<body style="margin: 0px;">
 
    <div id="divViewerPDF" runat="server" style=" text-align:center; background-color: white; width: 100%; height: 590px;">
    <iframe id="ViewPDF" runat="server" style=" width: 98%; height: 99%;"></iframe> 
</div>

     <div id="divViewerVideo" runat="server" style=" align-items:center !important; text-align:center !important; padding-top:30px; background-color: white; width: 100%; height: 590px;">
      <video width="700" height="500" runat="server" id="playvideo" controls autoplay></video>
    </div>
      <div id="divViewImage"  runat="server" style="align-items:center; text-align:center !important; background-color: white; width: 100%; height: 590px;">
      <input type="image" runat="server" id="ImageControl"  width="1080" height="550"/>
    </div>
</body>
</html>