﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Resources;
    using System.ServiceProcess;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Management;
    using System.Drawing;

    using Aspose.Cells;

    using Business.Services;
    using Data.Entities;
    using Utilities;
    using Utilities.Sessions;

    using Telerik.Web.UI;

    using CheckBox = System.Web.UI.WebControls.CheckBox;
    using Label = System.Web.UI.WebControls.Label;
    using TextBox = System.Web.UI.WebControls.TextBox;
    using Telerik.Windows.Zip;
    using System.Net.Mime;
    using Nest;

    public class document //class object json from elastic
    {
        public string content { get; set; }
        public file file { get; set; }
    }

    public class file
    {
        public string filename { get; set; }
    }

    /// <summary>
    /// Class customer
    /// </summary>
    public partial class DocumentsHome : System.Web.UI.Page
    {
        /// <summary>
        /// The permission service.
        /// </summary>
        private readonly PermissionService permissionService = new PermissionService();

        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService = new RevisionService();

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService = new DocumentTypeService();

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly StatusService statusService = new StatusService();

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService = new DisciplineService();

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService = new ReceivedFromService();

        private readonly SpecialDocPermissionService specialDocPermissionService = new SpecialDocPermissionService();

        private readonly CheckOutInHistoryService checkOutInHistoryService = new CheckOutInHistoryService();

        private readonly FolderTypeService folderTypeService = new FolderTypeService();

        private List<int> AdminGroup
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("GroupAdminList").Split(',').Select(t => Convert.ToInt32(t)).ToList();
            }
        }

        private List<int> GroupFunctionList
        {
            get
            {
                return this._GroupfunctionService.GetByRoleId(UserSession.Current.User.Id).Select(t => t.ID).ToList();
            }
        }
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly FolderService folderService = new FolderService();

        private readonly DocumentService documentService = new DocumentService();

        private readonly NotificationRuleService notificationRuleService = new NotificationRuleService();

        //  private readonly GroupDataPermissionService groupDataPermissionService = new GroupDataPermissionService();

        private readonly CategoryService categoryService = new CategoryService();

        private readonly UserService userService = new UserService();

        private readonly DocPropertiesViewService docPropertiesViewService = new DocPropertiesViewService();

        private readonly GroupFunctionService _GroupfunctionService = new GroupFunctionService();
        private readonly FolderPermissionService folderPermisstionService = new FolderPermissionService();
        private readonly FilePermissionService _FilePermissionService = new FilePermissionService();

        protected const string ServiceName = "EDMSFolderWatcher";

        public static RadTreeNode editedNode = null;

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string UnreadPattern = @"\(\d+\)";

        private const string RegexValidateEmail =
            @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";

        /// <summary>
        /// The list folder id.
        /// </summary>
        private List<int> listFolderId = new List<int>();

        //   private readonly UserDataPermissionService userDataPermissionService = new UserDataPermissionService();
        private readonly RoleService roleService = new RoleService();

        ConnectionSettings connectionSettings;
        ElasticClient elasticClient;

        private void InitializeComponent()
        {
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            connectionSettings = new ConnectionSettings()
                .DefaultMappingFor<document>(i => i
                .IndexName("mongduong")
                .TypeName("_doc"))
                .EnableDebugMode()
                .PrettyJson()
                .RequestTimeout(TimeSpan.FromMinutes(2));
            elasticClient = new ElasticClient(connectionSettings);
            Session.Remove("documentSearchList");

            this.CurrentRoleId.Value = UserSession.Current.RoleId.ToString();
            this.IsAdminGroup.Value = this.AdminGroup.Contains(UserSession.Current.RoleId) ? "true" : "false";
            this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = UserSession.Current.User.Role.Id == 1;
            var totalProperty = Convert.ToInt32(ConfigurationManager.AppSettings.Get("TotalProperty"));
            for (int i = 1; i <= totalProperty; i++)
            {
                var column = this.grdDocument.MasterTableView.GetColumn("Index" + i);
                //var revisionColumn = this.grdDocument.MasterTableView.GetColumn("RevisionColumn");
                if (column != null)
                {
                    column.Visible = false;
                }
            }
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!Page.IsPostBack)
            {
                Session.Add("IsListAll", false);

                var categoryPermission = this.folderPermisstionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault()).Select(t => t.CategoryId.GetValueOrDefault()).ToList();
                categoryPermission = categoryPermission.Union(this.folderPermisstionService.GetCategoryByUsser(UserSession.Current.User.Id).Select(t => t.CategoryId.GetValueOrDefault())).Distinct().ToList();
                //categoryPermission = this._FolderPermisstionService.GetCategoryByUsser(UserSession.Current.User.Id).Select(t => t.CategoryId.GetValueOrDefault()).Distinct().ToList();
                var listCategory = this.AdminGroup.Contains(UserSession.Current.RoleId)
                                    ? this.categoryService.GetAll()
                                    : this.categoryService.GetAll().Where(t => categoryPermission.Any(x => x == t.ID)).ToList();

                if (listCategory.Any())
                {
                    foreach (var category in listCategory)
                    {
                        category.ParentId = -1;
                    }
                    listCategory.Insert(0, new Category() { ID = -1, Name = "DOCUMENT GROUP" });
                    this.radPbCategories.DataSource = listCategory;
                    this.radPbCategories.DataFieldParentID = "ParentId";
                    this.radPbCategories.DataFieldID = "Id";
                    this.radPbCategories.DataValueField = "Id";
                    this.radPbCategories.DataTextField = "Name";
                    this.radPbCategories.DataBind();
                    this.radPbCategories.Items[0].Expanded = true;
                    foreach (RadPanelItem item in this.radPbCategories.Items[0].Items)
                    {
                        item.ImageUrl = @"Images/category2.png";
                        item.NavigateUrl = "DocumentsHome.aspx?doctype=" + item.Value;
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["doctype"]) && this.radPbCategories.Items.Count > 0)
                    {
                        var doctype = Convert.ToInt32(Request.QueryString["doctype"]);
                        this.lblCategoryId.Value = Request.QueryString["doctype"];
                        if (!string.IsNullOrEmpty(Request.QueryString["folId"]))
                        {
                            var folderObj = this.folderService.GetById(Convert.ToInt32(Request.QueryString["folId"]));
                            this.ReLoadTreeFolder(Convert.ToInt32(Request.QueryString["doctype"]), folderObj.ParentID.GetValueOrDefault(), folderObj.ID);
                            List<int> listTemp = new List<int>();
                            var listFolder = this.GetAllChildren(Convert.ToInt32(Request.QueryString["folId"]), ref listTemp);
                            Session["allChildFolder"] = listFolder.Distinct().ToList();
                            listFolder.Add(Convert.ToInt32(Request.QueryString["folId"]));
                            Session["ListIdFolder"] = listFolder.Distinct().ToList();
                            this.grdDocument.CurrentPageIndex = 0;
                            this.LoadDocuments(true, false, listFolder, false);
                        }
                        else
                        {
                            this.LoadTreeFolder(doctype);
                        }

                        var temp = this.radPbCategories.Items[0].Items.FindItemByValue(doctype.ToString());
                        if (temp != null)
                        {
                            temp.Selected = true;
                        }
                        this.radPbSearch.Items[0].Items[0].NavigateUrl = "Search.aspx?ctgr=" + Request.QueryString["doctype"] ;
                    }
                }

                this.LoadListPanel();
                radMenuFolder.Visible = false;
                CustomerMenu.Visible = false;
                ///  this.LoadSystemPanel();
            }
            LoadDocConfigGridView();
        }

        private void LoadDocConfigGridView()
        {
            var isViewByGroup = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("ViewSettingByGroup"));
            var selectedProperty = new List<string>();

            //var ddlCategory = (RadComboBox)this.rpbObjTree.FindItemByValue("ObjTree").FindControl("ddlCategory");
            //if (this.radTreeFolder.SelectedNode != null)
            //{
            var totalProperty = Convert.ToInt32(ConfigurationManager.AppSettings.Get("TotalProperty"));
            //var folderId = Convert.ToInt32(this.radTreeFolder.SelectedNode.Value);
            //var categoryId = Convert.ToInt32(ddlCategory.SelectedValue);
            //var deparmentId = isViewByGroup && !UserSession.Current.User.IsAdmin.GetValueOrDefault()
            //? UserSession.Current.RoleId
            //: 0;
            //var folderTypeObj = this.folderTypeService.GetByFolder(folderId);
            //if (folderTypeObj != null)
            //{
            //    var docPropertiesView = this.docPropertiesViewService.GetByType(folderTypeObj.DocTypeID);
            //    if (docPropertiesView != null && docPropertiesView.PropertyIndex != "")
            //    {
            //        var temp =
            //            docPropertiesView.PropertyIndex.Split(',').Where(t => !string.IsNullOrEmpty(t.Trim())).Select(
            //                t => t.Trim()).ToList();
            //        selectedProperty.AddRange(temp);
            //    }

            //    selectedProperty = selectedProperty.Distinct().ToList();

            //    for (int i = 1; i <= totalProperty; i++)
            //    {
            //        var column = this.grdDocument.MasterTableView.GetColumn("Index" + i);
            //        if (column != null)
            //        {
            //            column.Visible = selectedProperty.Contains(i.ToString());
            //        }
            //    }
            //}
            //else
            //{
            if (!string.IsNullOrEmpty(Request.QueryString["doctype"]) && this.radPbCategories.Items.Count > 0)
            {
                var category = Convert.ToInt32(Request.QueryString["doctype"]);
                var docPropertiesView = this.docPropertiesViewService.GetByCategory(category);
                if (docPropertiesView != null && docPropertiesView.PropertyIndex != "")
                {
                    var temp =
                        docPropertiesView.PropertyIndex.Split(',').Where(t => !string.IsNullOrEmpty(t.Trim())).Select(
                            t => t.Trim()).ToList();
                    selectedProperty.AddRange(temp);
                }

                selectedProperty = selectedProperty.Distinct().ToList();

                for (int i = 1; i <= totalProperty; i++)
                {
                    var column = this.grdDocument.MasterTableView.GetColumn("Index" + i);
                    if (column != null)
                    {
                        column.Visible = selectedProperty.Contains(i.ToString());
                    }
                }
            }
            //}
            //}
        }

        /// <summary>
        /// The rad tree view 1_ node click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void radTreeFolder_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            this.lbUserId.Value = UserSession.Current.User.Id.ToString();
            this.lbRoleId.Value = UserSession.Current.User.RoleId.ToString();
            var folder = this.folderService.GetById(Convert.ToInt32(e.Node.Value));
            this.lbFolderSize.Value = (folder.FolderSize / 1024 / 1024).ToString();
            var categoryId = Convert.ToInt32(this.lblCategoryId.Value);
            //var folderPermission = this._FolderPermisstionService.GetFolderByRoleIdList(Convert.ToInt32(Request.QueryString["doctype"]), GroupFunctionList)
            //                                   .Where(t => t.FolderId != null).ToList();
            var folderPermission = this.folderPermisstionService.GetByRoleId(Convert.ToInt32(Request.QueryString["doctype"]), UserSession.Current.User.RoleId.GetValueOrDefault())
                                               .Where(t => t.FolderId != null).ToList();
            folderPermission = folderPermission.Union(this.folderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"]))
                                                             .Where(t => t.FolderId != null)).ToList();

            var ThisfolderPermission = folderPermission.Where(t => t.FolderId == folder.ID).ToList();

            Session.Remove("ThisfolderPermission");
            ChangeTooltip(e.Node, ThisfolderPermission);
            Session["ThisfolderPermission"] = ThisfolderPermission;
            Session.Remove("allChildFolder");
            Session.Remove("ListIdFolder");
            // this.LoadActionPermission(e.Node.Value);
            e.Node.Expanded = true;
            Session.Remove("ListNodeExpanded");

            List<int> ListNodeExpanded = new List<int>();
            GetListNodeExpanded(ref ListNodeExpanded, e.Node.ParentNode);
            Session["ListNodeExpanded"] = ListNodeExpanded;
            //var temp = (RadToolBarButton)this.CustomerMenu.FindItemByText("View explorer");
            //if (temp != null && folder != null)
            //{
            //    temp.NavigateUrl = ConfigurationManager.AppSettings["ServerName"] + folder.DirName;
            //}
            if (ThisfolderPermission.Where(t => t.Folder_Read.GetValueOrDefault()).Any() || this.AdminGroup.Contains(UserSession.Current.RoleId))
            {
                var listFolderPermission = this.AdminGroup.Contains(UserSession.Current.RoleId)
                                ? this.folderService.GetAllByCategory(Convert.ToInt32(Request.QueryString["doctype"]))
                                : this.folderService.GetSpecificFolderStatic(folderPermission.Where(t => t.Folder_Read.GetValueOrDefault()).Select(t => t.FolderId.GetValueOrDefault()).Distinct().ToList());
                var folderlist = listFolderPermission.Where(t => t.ParentID.GetValueOrDefault() == folder.ID).OrderBy(t => t.Name).ToList();
                if (folderlist.Any())
                {
                    e.Node.Nodes.Clear();
                    foreach (var folderitem in folderlist)
                    {
                        var nodechild = new RadTreeNode();
                        nodechild.Text = folderitem.Name;
                        nodechild.Value = folderitem.ID.ToString();
                        nodechild.ImageUrl = "~/Images/folderdir16.png";
                        nodechild.Target = e.Node.Target;
                        if (listFolderPermission.Count(t => t.ParentID.GetValueOrDefault() == folderitem.ID) > 0)
                        {
                            nodechild.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                        }
                        e.Node.Nodes.Add(nodechild);
                    }

                    e.Node.Expanded = true;
                }
                //load data folder properties
                UpdatePropertiesFolder(folder);

                List<int> listTemp = new List<int>();
                var listFolder = this.GetAllChildren(Convert.ToInt32(e.Node.Value), listFolderPermission, ref listTemp);
                Session["allChildFolder"] = listFolder.Distinct().ToList();
                listFolder.Add(Convert.ToInt32(e.Node.Value));
                Session["ListIdFolder"] = listFolder.Distinct().ToList();
                this.grdDocument.CurrentPageIndex = 0;
                this.LoadDocuments(true, false, listFolder, false);
            }
            else
            {
                lblNotification.Text = "Access denied Folder.";
                RadNotification1.Title = "Warning";
                RadNotification1.Show();
            }

        }

        private void UpdatePropertiesFolder(Folder folder)
        {
            this.lblDocumentCount.Visible = true;
            this.txtDocumentCount.Visible = true;
            this.lblParentFolder.Visible = true;
            this.txtParentFolder.Visible = true;
            this.lblFolderCount.Visible = true;
            this.txtFolderCount.Visible = true;
            var creater = this.userService.GetByID(folder.CreatedBy.GetValueOrDefault());
            var uploader = this.userService.GetByID(folder.LastUpdatedBy.GetValueOrDefault());
            this.lblCreatedBy.Text = creater != null ? creater.UserNameFullName : "-";
            this.lblCreatedTime.Text = folder.CreatedDate.ToString();
            this.lblUpdatedBy.Text = uploader != null ? uploader.UserNameFullName : "-";
            this.lblUpdatedTime.Text = folder.LastUpdatedDate.ToString();
            this.txtDocumentCount.Text = folder.NumberOfDocument.GetValueOrDefault().ToString();
            this.lblDiskUsage.Text = SizeSuffix(folder.FolderSize.HasValue ? Convert.ToInt64(folder.FolderSize.Value) : 0, 1);
            this.txtParentFolder.Text = folder.ParentName;
            this.txtFolderCount.Text = folder.NumberOfSubfolder.GetValueOrDefault().ToString();
        }

        protected void radTreeFolder_NodeExpand(object sender, RadTreeNodeEventArgs e)
        {
            var categoryId = Convert.ToInt32(this.lblCategoryId.Value);
            var folderId = Convert.ToInt32(e.Node.Value);
            e.Node.Nodes.Clear();
            //var folderPermission = this._FolderPermisstionService.GetFolderByRoleIdList(Convert.ToInt32(Request.QueryString["doctype"]), GroupFunctionList)
            //                                   .Where(t => t.FolderId != null).ToList();
            var folderPermission = this.folderPermisstionService.GetByRoleId(Convert.ToInt32(Request.QueryString["doctype"]), UserSession.Current.User.RoleId.GetValueOrDefault())
                                               .Where(t => t.FolderId != null).ToList();
            folderPermission = folderPermission.Union(this.folderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"]))
                                                             .Where(t => t.FolderId != null)).ToList();
            if (folderPermission.Where(t => t.FolderId == folderId && t.Folder_Read.GetValueOrDefault()).Any() || this.AdminGroup.Contains(UserSession.Current.RoleId))
            {
                var listFolder = this.AdminGroup.Contains(UserSession.Current.RoleId)
                            ? this.folderService.GetAllByCategory(Convert.ToInt32(Request.QueryString["doctype"]))
                            : this.folderService.GetSpecificFolderStatic(folderPermission.Select(t => t.FolderId.GetValueOrDefault()).ToList());


                var folderlist = listFolder.Where(t => t.ParentID.GetValueOrDefault() == folderId).OrderBy(t => t.Name).ToList();
                foreach (var folder in folderlist)
                {
                    var nodechild = new RadTreeNode();
                    nodechild.Text = folder.Name;
                    nodechild.Value = folder.ID.ToString();
                    nodechild.ImageUrl = "~/Images/folderdir16.png";
                    nodechild.Target = e.Node.Target;
                    if (listFolder.Count(t => t.ParentID.GetValueOrDefault() == folder.ID) > 0)
                    {
                        nodechild.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                    }
                    e.Node.Nodes.Add(nodechild);
                }

                e.Node.Expanded = true;
            }
            else
            {
                lblNotification.Text = "Access denied Folder.";
                RadNotification1.Title = "Warning";
                RadNotification1.Show();
            }
        }

        /// <summary>
        /// The rad tree folder_ node edit.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void radTreeFolder_NodeEdit(object sender, RadTreeNodeEditEventArgs e)
        {
            e.Node.Text = e.Text;
            var categoryId = Convert.ToInt32(this.lblCategoryId.Value);
            var folder = new Folder();
            List<int> ListNodeExpanded = new List<int>();
            GetListNodeExpanded(ref ListNodeExpanded, e.Node.ParentNode);
            if (!string.IsNullOrEmpty(Session["FolderMenuAction"].ToString()) && Session["FolderMenuAction"].ToString() == "New")
            {
                try
                {
                    var parentFol = this.folderService.GetById(Convert.ToInt32(e.Node.ParentNode.Value));
                    var folderGUID = Guid.NewGuid();
                    folder = new Folder()
                    {
                        Name = e.Text,
                        Description = Utility.RemoveAllSpecialCharacter(e.Text),
                        CategoryID = categoryId,
                        ParentID = Convert.ToInt32(e.Node.ParentNode.Value),
                        DirName = "DocLib" + "/" + folderGUID,
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now
                    };

                    Directory.CreateDirectory(Server.MapPath(folder.DirName));

                    var ownerGroupIds = this.roleService.GetAllOwner().Select(t => t.Id);
                    var ownerUserIds = this.userService.GetAll().Where(t => ownerGroupIds.Contains(t.RoleId.GetValueOrDefault())).Select(t => t.Id);

                    var parentFolderGroupPermission = this.folderPermisstionService.GetAllByFolder(Convert.ToInt32(e.Node.ParentNode.Value));

                    var folId = this.folderService.Insert(folder);

                    var groupPermission =
                        parentFolderGroupPermission.Select(
                            t =>
                            new FolderPermission()
                            {
                                CategoryId = t.CategoryId,
                                ObjectId = t.ObjectId,
                                ObjectIdName = t.ObjectIdName,
                                FolderId = folder.ID,
                                TypeID = t.TypeID,
                                TypeName = t.TypeName,
                                Folder_IsFullPermission = t.Folder_IsFullPermission,
                                Folder_ChangePermission = t.Folder_ChangePermission,
                                Folder_CreateSubFolder = t.Folder_CreateSubFolder,
                                Folder_Delete = t.Folder_Delete,
                                Folder_Read = t.Folder_Read,
                                Folder_Write = t.Folder_Write,
                                File_FullPermission = t.File_FullPermission,
                                File_ChangePermission = t.File_ChangePermission,
                                File_Create = t.File_Create,
                                File_Delete = t.File_Delete,
                                File_Read = t.File_Read,
                                File_Write = t.File_Write,
                                File_NoAccess = t.File_NoAccess,
                                CreatedDate = DateTime.Now,
                                CreatedBy = UserSession.Current.User.Id
                            }).ToList();
                    this.folderPermisstionService.AddGroupDataPermissions(groupPermission);
                    UpdatePropertisFolder(categoryId, ListNodeExpanded);
                }
                catch (Exception ex)
                {
                }
            }
            else if (!string.IsNullOrEmpty(Session["FolderMenuAction"].ToString()) && Session["FolderMenuAction"].ToString() == "Rename")
            {
                try
                {
                    folder = this.folderService.GetById(Convert.ToInt32(e.Node.Value));

                    //var oldDirName = folder.DirName;
                    //var newDirName = folder.DirName.Substring(0, folder.DirName.LastIndexOf('/') + 1) + Utility.RemoveUnicode(Utility.RemoveAllSpecialCharacter(e.Text));
                    //var oldPath = Server.MapPath(folder.DirName);
                    //var newPath = Server.MapPath(newDirName);
                    folder.Name = e.Text;
                    //folder.DirName = newDirName;
                    //var watcherService = new ServiceController(ServiceName);
                    //if (Utility.ServiceIsAvailable(ServiceName))
                    //{
                    //    watcherService.ExecuteCommand(128);
                    //}
                    //if (oldPath != newPath)
                    //{
                    //    Directory.Move(oldPath, newPath);
                    //}
                    // if (Utility.ServiceIsAvailable(ServiceName))
                    //{
                    //    watcherService.ExecuteCommand(129);
                    //}

                    this.folderService.Update(folder);

                    //foreach (var childNode in e.Node.GetAllNodes())
                    //{
                    //    var childFolder = this.folderService.GetById(Convert.ToInt32(childNode.Value));
                    //    if (childFolder != null)
                    //    {
                    //        childFolder.DirName = childFolder.DirName.Replace(oldDirName, newDirName);
                    //        this.folderService.Update(childFolder);
                    //    }
                    //}

                    var selectedFolder = this.radTreeFolder.FindNodeByValue(e.Node.Value);
                    var tempListFolderId = new List<int>();

                    ////var folderPermissionTmp =
                    ////    this.groupDataPermissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault())
                    ////        .Where(t => t.CategoryIdList == categoryId.ToString() && !string.IsNullOrEmpty(t.FolderIdList))
                    ////        .Select(t => Convert.ToInt32(t.FolderIdList)).ToList();
                    ////if (selectedFolder.GetAllNodes().Count > 0)
                    ////{
                    ////    tempListFolderId.AddRange(
                    ////        from folderNode in selectedFolder.GetAllNodes()
                    ////        where folderPermissionTmp.Contains(Convert.ToInt32(folderNode.Value))
                    ////        select Convert.ToInt32(folderNode.Value));
                    ////}

                    //tempListFolderId.AddRange(selectedFolder.GetAllNodes().Select(t => Convert.ToInt32(t.Value)));
                    //tempListFolderId.Add(folder.ID);
                    //var listDocuments = this.documentService.GetAllByFolderForMoveFolder(tempListFolderId);
                    //foreach (var document in listDocuments)
                    //{
                    //    document.DirName = document.DirName.Replace(oldDirName, newDirName);
                    //    document.FilePath = document.FilePath.Replace(oldDirName, newDirName);
                    //    document.LastUpdatedBy = UserSession.Current.User.Id;
                    //    document.LastUpdatedDate = DateTime.Now;
                    //    this.documentService.Update(document);
                    //}
                }
                catch (Exception ex)
                {
                    //var watcherService = new ServiceController("EDMSFolderWatcher");
                    //if (Utility.ServiceIsAvailable("EDMSFolderWatcher"))
                    //{
                    //    watcherService.ExecuteCommand(129);
                    //}
                }
            }

            Session.Remove("FolderMenuAction");

            //this.LoadTreeFolder(categoryId);
            this.ReLoadTreeFolder(categoryId, e.Node.ParentNode, folder.ID);
            this.grdDocument.CurrentPageIndex = 0;
            this.grdDocument.Rebind();
        }



        /// Load all document by folder
        /// </summary>
        /// <param name="isbind">
        /// The isbind.
        /// </param>
        protected void LoadDocuments(bool isbind, bool isListAllCategory, List<int> listFolder, bool isAdvanceSearch)
        {
            try
            {
                //RadSearchBox txtSearch = (RadSearchBox)CustomerMenu.Items[3].FindControl("txtSearch");
                var categoryID = Convert.ToInt32(Request.QueryString["doctype"]);
                var isSystemAdmin = this.AdminGroup.Contains(UserSession.Current.RoleId);
                LoadDocConfigGridView();
                if (this.radTreeFolder.SelectedNode != null)
                {
                    var folderId = Convert.ToInt32(this.radTreeFolder.SelectedNode.Value);
                    var selectedFolder = this.radTreeFolder.SelectedNode;
                    var specialDocPer = new List<SpecialDocPermission>();
                    var listFolderdoc = new List<Document>();
                    var listDocuments = new List<Document>();
                    //GET THEO TAT CA FOLDEON NODE CLICK
                    //var folderPermission = this._FolderPermisstionService.GetFolderByRoleIdList(Convert.ToInt32(Request.QueryString["doctype"]), GroupFunctionList)
                    //                      .Where(t => t.FolderId != null && (t.File_Read.GetValueOrDefault() || t.File_Write.GetValueOrDefault()) && !t.File_NoAccess.GetValueOrDefault())
                    //                      .ToList();
                    var folderPermission = this.folderPermisstionService.GetByRoleId(Convert.ToInt32(Request.QueryString["doctype"]), UserSession.Current.User.RoleId.GetValueOrDefault())
                                           .Where(t => t.FolderId != null && (t.Folder_Read.GetValueOrDefault() || t.Folder_Write.GetValueOrDefault())).ToList();
                    folderPermission = folderPermission.Union(this.folderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"])).Where(t => t.FolderId != null && (t.Folder_Read.GetValueOrDefault() || t.Folder_Write.GetValueOrDefault())).Distinct()).ToList();
                    var listfolderpermission = folderPermission.Select(t => t.FolderId.GetValueOrDefault()).ToList();
                    var folderfilder = isSystemAdmin ? listFolder.ToList() : listFolder.Where(t => listfolderpermission.Contains(t)).ToList();

                    //if (isAdvanceSearch)
                    //{
                    //    //Search query to retrieve info
                    //    var response = elasticClient.Search<document>(s => s
                    //        .Query(q => q
                    //        .Match(m => m
                    //        .Field(f => f.content).Query(txtSearch.Text + "*")))
                    //        .Size(10000));
                    //    List<string> docList = new List<string>();
                    //    foreach (var hit in response.Hits)
                    //    {
                    //        //string doc = hit.Source.file.filename.Substring(0, hit.Source.file.filename.LastIndexOf('.'));
                    //        docList.Add(hit.Source.file.filename);
                    //        //lblResult.Text += doc + "<br/>";
                    //    }
                    //    var listDocFilter = this.documentService.AdvanceSearch(categoryID, docList);
                    //    if (listDocFilter.Count > 0)
                    //    {
                    //        listDocuments = listDocFilter.Where(t => folderfilder.Contains(t.FolderID.GetValueOrDefault())).ToList();
                    //    }
                    //}
                    //else
                    //{
                    //}
                    //17/3/2020-getalldocchildfolder
                    //listDocuments = this.documentService.GetAllByFolderPermission(folderfilder);
                    //var childFoldersInPermission = this.folderService.GetAllSpecificFolder(folderfilder);
                    //listFolderdoc = childFoldersInPermission.Select(t => new Document()
                    //{
                    //    ID = Convert.ToInt32(t.ID),
                    //    Name = t.Name,
                    //    FileExtensionIcon = "Images/folderdir16.png",
                    //    IsFolder = true,
                    //    FilePath = ""
                    //}).ToList();
                    //17/3/2020-getalldocselectfolder
                    //listDocuments = this.documentService.GetAllByFolder(folderfilder);
                    listDocuments = this.documentService.GetAllByFolder(folderId);
                    var childFolders = this.folderService.GetAllByParentId(Convert.ToInt32(selectedFolder.Value));
                    listFolderdoc = childFolders.Select(t => new Document()
                    {
                        ID = Convert.ToInt32(t.ID),
                        Name = t.Name,
                        FileExtensionIcon = "Images/folderdir16.png",
                        IsFolder = true,
                        FilePath = ""
                    }).ToList();
                    listFolderdoc = listFolderdoc.Union(listDocuments).ToList();

                    // check permission file
                    //var filepermission = this._FilePermissionService.GetByRoleIdListCategory(GroupFunctionList, Convert.ToInt32(Request.QueryString["doctype"])).ToList();
                    //filepermission = filepermission.Union(this._FilePermissionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"]))).ToList();
                    //var cheklist = filepermission.Where(t => listDocuments.Select(k => k.ID).Contains(t.DocumentID.GetValueOrDefault())).Select(k => k.DocumentID.GetValueOrDefault()).Distinct().ToList();
                    //if (cheklist.Any())
                    //{
                    //    List<int> removeid = new List<int>();
                    //    foreach (var item in cheklist)
                    //    {
                    //        if (filepermission.Where(t => t.DocumentID == item).Where(k => !k.Create.GetValueOrDefault() && k.ChangePermission.GetValueOrDefault() && !k.Delete.GetValueOrDefault() && !k.Read.GetValueOrDefault() && !k.Write.GetValueOrDefault()).Any())
                    //        {
                    //            removeid.Add(item);
                    //        }
                    //    }
                    //    if (removeid.Count > 0)
                    //    {
                    //        listDocuments = listDocuments.Where(t => !removeid.Contains(t.ID)).ToList();
                    //    }
                    //}
                    //else
                    //{
                    //    var folderPermission = (List<FolderPermission>)Session["ThisfolderPermission"];
                    //    //this._FolderPermisstionService.GetByRoleIdList(GroupFunctionList, folderId);
                    //    //folderPermission.Add(this._FolderPermisstionService.GetByFolderUserId(UserSession.Current.User.Id, folderId));
                    //    if (folderPermission.Where(t => t.Folder_Read.GetValueOrDefault() || t.Folder_Write.GetValueOrDefault()).Any() || isSystemAdmin)
                    //    {
                    //        listDocuments = this.documentService.GetAllByFolder(folderId);
                    //        var childFolders = this.folderService.GetAllByParentId(Convert.ToInt32(selectedFolder.Value));
                    //        listFolderdoc = childFolders.Select(t => new Document()
                    //        {
                    //            ID = Convert.ToInt32(t.ID),
                    //            Name = t.Name,
                    //            FileExtensionIcon = "Images/folderdir16.png",
                    //            IsFolder = true,
                    //            FilePath = ""
                    //        }).ToList();
                    //        //listDocuments = listDocuments.Union(listFolderdoc).ToList();
                    //        listFolderdoc = listFolderdoc.Union(listDocuments).ToList();
                    //    }
                    //    else
                    //    {
                    //        lblNotification.Text = "Access denied documents.";
                    //        RadNotification1.Title = "Warning";
                    //        RadNotification1.Show();
                    //    }
                    //}

                    this.grdDocument.DataSource = listFolderdoc;
                    if (isbind)
                    {
                        this.grdDocument.DataBind();
                    }
                }
                else
                {
                    this.grdDocument.DataSource = new List<Document>();
                    if (isbind)
                    {
                        this.grdDocument.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                var st = ex.Message.ToString();
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.Rebind();
            }
            //else if (e.Argument.Contains("DowloadZipFile_"))
            //{
            //    var selectfolder =Convert.ToInt32( e.Argument.Split('_')[1]);
            //    var folderObj = this.folderService.GetById(selectfolder);
            //    if (folderObj != null)
            //    {
            //        var ListChildFodler = (List<int>)Session["ListIdFolder"];
            //        var docList = this.documentService.GetAllByFolder(ListChildFodler);
            //        var folderSize = docList.Sum(t => t.FileSize);
            //        if (folderSize <= 2147483648)
            //        {
            //            var filename = DateTime.Now.ToBinary() + "_DocPack.zip";
            //            var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + filename);
            //            //ICSharpCode.SharpZipLib.Zip.ZipOutputStream zip = new ICSharpCode.SharpZipLib.Zip.ZipOutputStream(File.Create(serverTotalDocPackPath));
            //            //zip.SetLevel(9);
            //            //ZipFolder(folderObj, zip, folderPermission);
            //            //zip.Finish();
            //            //zip.Close();
            //            using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile())
            //            {
            //                zip.AlternateEncoding = System.Text.Encoding.Unicode;
            //                zip.AddDirectory(Server.MapPath(folderObj.DirName));
            //                zip.Save(serverTotalDocPackPath);
            //            };
            //            // this.loading.Visible = false;
            //            this.Download_File(serverTotalDocPackPath);

            //        }
            //    }
            //}
            else if (e.Argument.Contains("ReloadFolder"))
            {
                var fodlerId = Convert.ToInt32(e.Argument.Split('_')[1]);
                var folderobj = this.folderService.GetById(fodlerId);
                if (folderobj != null)
                {
                    var categoryId = folderobj.CategoryID.GetValueOrDefault();
                    var eNode = radTreeFolder.SelectedNode;
                    eNode.ParentNode.Expanded = false;
                    eNode.Expanded = false;
                    eNode.Nodes.Clear();
                    var listFolder = this.folderService.GetAllByCategory(categoryId);
                    var folderlist = listFolder.Where(t => t.ParentID.GetValueOrDefault() == fodlerId).OrderBy(t => t.Name).ToList();
                    foreach (var folder in folderlist)
                    {
                        var nodechild = new RadTreeNode();
                        nodechild.Text = folder.Name;
                        nodechild.Value = folder.ID.ToString();
                        nodechild.ImageUrl = "~/Images/folderdir16.png";
                        nodechild.Target = categoryId.ToString();
                        if (listFolder.Count(t => t.ParentID.GetValueOrDefault() == folder.ID) > 0)
                        {
                            nodechild.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                        }
                        eNode.Nodes.Add(nodechild);
                    }
                    eNode.ParentNode.Expanded = true;
                    eNode.Expanded = true;
                    this.radTreeFolder.DataBind();

                }
            }
            else if (e.Argument == "ExportData")
            {
                var filePath = Server.MapPath(@"Exports") + @"\";
                var folderId = Convert.ToInt32(this.radTreeFolder.SelectedNode.Value);
                //var list = (List<int>)Session["ListIdFolder"];
                var selectedFolder = this.radTreeFolder.SelectedNode;

                // Export master List
                if (this.radTreeFolder.SelectedNode != null)
                {
                    var dtFull = new DataTable();
                    dtFull.Columns.AddRange(new[]
                    {
                        new DataColumn("DocId", typeof(String)),
                        new DataColumn("NoIndex", typeof(String)),
                        new DataColumn("FileName", typeof(String)),
                        new DataColumn("DocNo", typeof(String)),
                        new DataColumn("DocTitle", typeof(String)),
                        new DataColumn("Rev", typeof(String)),
                        new DataColumn("Date", typeof(String)),
                        new DataColumn("From/To", typeof(String)),
                        new DataColumn("Discipline", typeof(String)),
                        new DataColumn("DocumentType", typeof(String)),
                        new DataColumn("Platform", typeof(String)),
                        new DataColumn("Remark", typeof(String)),
                          new DataColumn("Contractor", typeof(String)),
                            new DataColumn("Tag", typeof(String)),
                    });

                    var listDocuments = this.documentService.GetAllByFolder(folderId);
                    var count = 1;
                    var workbook = new Workbook();
                    workbook.Open(filePath + @"Template\DocumentList.xls");
                    var sheets = workbook.Worksheets;

                    foreach (var documentPackage in listDocuments)
                    {
                        var dataRow = dtFull.NewRow();
                        dataRow["DocId"] = documentPackage.ID;
                        dataRow["NoIndex"] = count;
                        dataRow["FileName"] = documentPackage.Name;
                        dataRow["DocNo"] = documentPackage.DocumentNumber;
                        dataRow["DocTitle"] = documentPackage.Title;
                        dataRow["Rev"] = documentPackage.RevisionName;
                        dataRow["Date"] = documentPackage.Date != null
                            ? documentPackage.Date.Value.ToString("dd/MM/yyyy")
                            : string.Empty;
                        dataRow["From/To"] = documentPackage.FromTo;
                        dataRow["Discipline"] = documentPackage.DisciplineName;
                        dataRow["DocumentType"] = documentPackage.DocumentTypeName;
                        dataRow["Platform"] = documentPackage.Platform;
                        dataRow["Remark"] = documentPackage.Remark;
                        dataRow["Contractor"] = documentPackage.Contractor;
                        dataRow["Tag"] = documentPackage.Tag;
                        count += 1;
                        dtFull.Rows.Add(dataRow);
                    }

                    //An cot theo docproperty view
                    var selectedProperty = new List<string>();
                    var totalProperty = Convert.ToInt32(ConfigurationManager.AppSettings.Get("TotalProperty"));
                    var category = Convert.ToInt32(Request.QueryString["doctype"]);
                    var docPropertiesView = this.docPropertiesViewService.GetByCategory(category);
                    if (docPropertiesView != null && docPropertiesView.PropertyIndex != "")
                    {
                        var temp =
                            docPropertiesView.PropertyIndex.Split(',').Where(t => !string.IsNullOrEmpty(t.Trim())).Select(
                                t => t.Trim()).ToList();
                        selectedProperty.AddRange(temp);
                    }
                    selectedProperty = selectedProperty.Distinct().ToList();
                    for (int i = 1; i <= totalProperty; i++)
                    {
                        if (!selectedProperty.Contains(i.ToString()))
                        {
                            sheets[0].Cells.Columns[i + 3].IsHidden = true;
                        }
                    }
                    sheets[0].Cells["A4"].PutValue(dtFull.Rows.Count);
                    sheets[0].Cells.ImportDataTable(dtFull, false, 4, 1, dtFull.Rows.Count, 15, true);
                    sheets[1].Cells.ImportDataTable(dtFull, false, 4, 1, dtFull.Rows.Count, 15, true);
                    sheets[0].AutoFitRows();
                    sheets[1].IsVisible = false;
                    var filename = Utility.MakeValidFileName(this.radTreeFolder.SelectedNode.Text) + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                    var saveFileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + fileName;
                    filename = filename.Replace(@"\", " ").Replace("/", " ");
                    workbook.Save(filePath + filename);
                    this.DownloadByWriteByte(filePath + filename, filename, true);
                }
            }
            else if (e.Argument.Contains("CheckInDocument"))
            {
                var docId = Convert.ToInt32(this.lblDocId.Value);
                var docObj = this.documentService.GetById(docId);
                if (docObj != null)
                {
                    docObj.IsCheckOut = false;

                    var checkinHistory = new CheckOutInHistory()
                    {
                        ActionName = "Checkin",
                        ByUserId = UserSession.Current.User.Id,
                        ByRoleId = UserSession.Current.RoleId,
                        AtTime = DateTime.Now,
                        DocumentId = docObj.ID,
                    };

                    this.documentService.Update(docObj);
                    this.checkOutInHistoryService.Insert(checkinHistory);

                    this.grdDocument.Rebind();
                }
            }
            else if (e.Argument.Contains("CheckinMultiDocument"))
            {
                foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
                {
                    var docItem = new Document();
                    docItem = this.documentService.GetById(Convert.ToInt32(selectedItem.GetDataKeyValue("ID")));
                    if (docItem != null && docItem.IsCheckOut)
                    {
                        docItem.IsCheckOut = false;

                        var checkinHistory = new CheckOutInHistory()
                        {
                            ActionName = "Checkin",
                            ByUserId = UserSession.Current.User.Id,
                            ByRoleId = UserSession.Current.RoleId,
                            AtTime = DateTime.Now,
                            DocumentId = docItem.ID,
                        };

                        this.documentService.Update(docItem);
                        this.checkOutInHistoryService.Insert(checkinHistory);
                    }
                }
                this.grdDocument.Rebind();

            }

            else if (e.Argument == "DownloadMulti")
            {

                var filename = DateTime.Now.ToBinary() + "_DocPack.zip";
                var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + filename);

                using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile())
                {
                    zip.AlternateEncoding = System.Text.Encoding.Unicode;
                    foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
                    {

                        zip.AddFile(Server.MapPath(selectedItem["FilePath"].Text));
                        selectedItem.Selected = false;
                    }
                    zip.Save(serverTotalDocPackPath);
                };
                Response.ClearContent();
                Response.Clear();
                Response.ContentType = "application/zip";
                Response.AddHeader("Content-Disposition",
                                  "attachment; filename=" + filename);
                Response.TransmitFile(serverTotalDocPackPath);
                Response.Flush();
                System.IO.File.Delete(serverTotalDocPackPath);
                Response.End();
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "DeleteMulti")
            {
                foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
                {
                    var docId = Convert.ToInt32(selectedItem.GetDataKeyValue("ID").ToString());
                    var docObj = this.documentService.GetById(docId);
                    if (docObj != null)
                    {
                        docObj.IsLeaf = false;
                        docObj.IsDelete = true;
                        docObj.LastUpdatedBy = UserSession.Current.User.Id;
                        docObj.LastUpdatedDate = DateTime.Now;
                        this.documentService.Update(docObj);
                    }
                }
                this.grdDocument.Rebind();

            }
            else if (e.Argument == "FileSize")
            {
                var listDoc = this.documentService.GetAll();
                foreach (var item in listDoc)
                {
                    if (!string.IsNullOrEmpty(item.FilePath) && (item.FilePath.Length + 11) < 248)
                    {
                        FileInfo file = new FileInfo(Server.MapPath(item.FilePath));
                        if (file.Exists)
                        {
                            item.FileSize = file.Length;
                            this.documentService.Update(item);

                        }
                    }
                }
            }

            else if (e.Argument == "FolderPropertise")
            {
                var listFolder = this.folderService.GetAll();

                foreach (var folderItem in listFolder)
                {
                    List<int> listTemp = new List<int>();
                    var subfolderList = this.GetAllChildren(folderItem.ID, listFolder, ref listTemp);
                    folderItem.NumberOfSubfolder = subfolderList.Count;
                    subfolderList.Add(folderItem.ID);
                    var docList = this.documentService.GetAllByFolder(subfolderList);
                    var folderSize = docList.Sum(t => t.FileSize);
                    folderItem.NumberOfDocument = docList.Count;
                    folderItem.FolderSize = folderSize;
                    this.folderService.Update(folderItem);
                }
            }

            else if (e.Argument == "UpFolder")
            {
                var error = "";
                var serverFolder = "";
                var path = ConfigurationManager.AppSettings["PathFolder"];
                var dataFolder = ConfigurationManager.AppSettings["DataFolder"];

                const Int32 BufferSize = 128;
                var fileStream = File.OpenRead(@path);
                var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, BufferSize);
                serverFolder = streamReader.ReadLine();

                DirectoryInfo DirInfo = new DirectoryInfo(@serverFolder);
                var subFolder = DirInfo.GetDirectories("*", SearchOption.AllDirectories);
                FileInfo[] subFileRoot = DirInfo.GetFiles("*", SearchOption.TopDirectoryOnly);

                var folderRoot = this.folderService.GetByName(DirInfo.Name);
                int idRoot = 0;
                if (folderRoot == null)
                {
                    var obj = new Category()
                    {
                        Name = DirInfo.Name,
                        Description = DirInfo.Name,
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now,
                        IsActive = true
                    };
                    var categoryId = this.categoryService.Insert(obj);
                    var folder = new Folder()
                    {
                        Name = DirInfo.Name,
                        Description = DirInfo.Name,
                        CategoryID = categoryId,
                        DirName = "DocLib" + "/" + Utility.RemoveAllSpecialCharacter(DirInfo.Name),
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now
                    };
                    idRoot = Convert.ToInt32(this.folderService.Insert(folder));
                }
                foreach (var itemFile in subFileRoot)
                {
                    var b = itemFile.FullName;
                    var fol = this.folderService.GetById(idRoot == 0 ? folderRoot.ID : idRoot);
                    var targetFolder = fol.DirName;
                    var serverFol = HostingEnvironment.ApplicationVirtualPath == "/" ? "/" + fol.DirName : HostingEnvironment.ApplicationVirtualPath + "/" + fol.DirName;
                    var objDoc = new Document();

                    var docObjLeaf = this.documentService.GetSpecificDocument(fol.ID, itemFile.Name);
                    if (docObjLeaf != null)
                    {
                        error += itemFile.Name + " ;";
                    }
                    else
                    {
                        objDoc.FolderID = fol.ID;
                        objDoc.CreatedBy = UserSession.Current.User.Id;
                        objDoc.CreatedDate = DateTime.Now;
                        objDoc.IsLeaf = true;
                        objDoc.IsDelete = false;
                        var serverDocFileName = Utility.RemoveAllSpecialCharacter(itemFile.Name);
                        var saveFilePath = Path.Combine(Server.MapPath(targetFolder), serverDocFileName);
                        var serverFilePath = serverFol + "/" + serverDocFileName;
                        var fileExt = "";
                        if (itemFile.Name.Contains("."))
                        {
                            fileExt = itemFile.Name.Substring(itemFile.Name.LastIndexOf(".") + 1, itemFile.Name.Length - itemFile.Name.LastIndexOf(".") - 1);
                        }
                        else
                        {
                            fileExt = "null";
                        }
                        objDoc.RevisionFileName = itemFile.Name;
                        objDoc.RevisionFilePath = serverFilePath;
                        objDoc.FilePath = serverFilePath;
                        objDoc.FileExtension = fileExt;
                        objDoc.FileExtensionIcon = !string.IsNullOrEmpty(Utility.FileIcon[fileExt.ToLower()]) ? Utility.FileIcon[fileExt.ToLower()] : "images/otherfile.png";
                        objDoc.Name = itemFile.Name;
                        objDoc.CategoryID = fol.CategoryID;
                        objDoc.DirName = fol.DirName;
                        if (objDoc.ParentID == null)
                        {
                            objDoc.FileNameOriginal = itemFile.Name;
                        }
                        this.documentService.Insert(objDoc);
                    }
                }
                foreach (var itemFolder in subFolder)
                {
                    FileInfo[] subFile = itemFolder.GetFiles("*", SearchOption.TopDirectoryOnly);
                    var nameFolderValid = Utility.RemoveUnicode(Utility.RemoveAllSpecialCharacter(itemFolder.Name));
                    var parentFol = new Folder();
                    if (itemFolder.Parent.Parent.Name != dataFolder)
                    {
                        parentFol = this.folderService.GetByParentAndName(itemFolder.Parent.Parent.Name, itemFolder.Parent.Name);
                    }
                    else
                    {
                        parentFol = this.folderService.GetByName(itemFolder.Parent.Name);
                    }
                    var folExist = this.folderService.GetByParentAndName(parentFol != null ? parentFol.ID : 0, itemFolder.Name);
                    if (folExist == null)
                    {
                        var folObj = new Folder()
                        {
                            Name = itemFolder.Name,
                            Description = Utility.RemoveAllSpecialCharacter(itemFolder.Name),
                            CategoryID = parentFol != null ? parentFol.CategoryID : 0,
                            ParentID = parentFol != null ? parentFol.ID : 0,
                            ParentName = parentFol != null ? parentFol.Name : string.Empty,
                            DirName = parentFol != null ? parentFol.DirName + "/" + nameFolderValid : "DocLib" + "/" + nameFolderValid,
                            CreatedBy = UserSession.Current.User.Id,
                            CreatedDate = DateTime.Now
                        };
                        this.folderService.Insert(folObj);
                        //if (!Directory.Exists(Server.MapPath(folObj.DirName)))
                        //{
                        //    Directory.CreateDirectory(Server.MapPath(folObj.DirName));
                        //}
                    }
                    foreach (var itemFile in subFile)
                    {
                        var fol = this.folderService.GetByParentAndName(parentFol.ID, itemFolder.Name);
                        var targetFolder = fol.DirName;
                        var serverFol = HostingEnvironment.ApplicationVirtualPath == "/" ? "/" + fol.DirName : HostingEnvironment.ApplicationVirtualPath + "/" + fol.DirName;
                        var objDoc = new Document();

                        var docObjLeaf = this.documentService.GetSpecificDocument(fol.ID, itemFile.Name);
                        if (docObjLeaf != null)
                        {
                            error += itemFile.Name + " ;";
                        }
                        else
                        {
                            objDoc.FolderID = fol.ID;
                            objDoc.CreatedBy = UserSession.Current.User.Id;
                            objDoc.CreatedDate = DateTime.Now;
                            objDoc.IsLeaf = true;
                            objDoc.IsDelete = false;
                            var serverDocFileName = Utility.RemoveAllSpecialCharacter(itemFile.Name);
                            // Path file to save on server disc
                            var saveFilePath = Path.Combine(Server.MapPath(targetFolder), serverDocFileName);
                            // var saveFileRevisionPath = Path.Combine(Server.MapPath(revisionPath), revisionServerFileName);

                            // Path file to download from server
                            var serverFilePath = serverFol + "/" + serverDocFileName;
                            //  var revisionFilePath = serverRevisionFolder + revisionServerFileName;
                            var fileExt = "";
                            if (itemFile.Name.Contains("."))
                            {
                                fileExt = itemFile.Name.Substring(itemFile.Name.LastIndexOf(".") + 1, itemFile.Name.Length - itemFile.Name.LastIndexOf(".") - 1);
                            }
                            else
                            {
                                fileExt = "null";
                            }
                            objDoc.RevisionFileName = itemFile.Name;
                            objDoc.RevisionFilePath = serverFilePath;
                            objDoc.FilePath = serverFilePath;
                            objDoc.FileExtension = fileExt;
                            objDoc.FileExtensionIcon = !string.IsNullOrEmpty(Utility.FileIcon[fileExt.ToLower()]) ? Utility.FileIcon[fileExt.ToLower()] : "images/otherfile.png";
                            objDoc.Name = itemFile.Name;
                            objDoc.CategoryID = fol.CategoryID;
                            objDoc.DirName = fol.DirName;
                            if (objDoc.ParentID == null)
                            {
                                objDoc.FileNameOriginal = itemFile.Name;
                            }
                            this.documentService.Insert(objDoc);

                            //File.Copy(itemFile.FullName, saveFilePath, true);
                            //if (File.Exists(saveFilePath))
                            //{
                            //    objDoc.IsActive = false;
                            //    this.documentService.Insert(objDoc);
                            //}
                        }
                    }
                }
                streamReader.Close();
            }

            else if (e.Argument.Contains("ShowChildFolder"))
            {
                var folderId = e.Argument.Split('_')[1];
                var folder = this.folderService.GetById(Convert.ToInt32(folderId));
                var selectedFolder = this.radTreeFolder.FindNodeByValue(folderId);
                if (selectedFolder != null)
                {
                    // this.LoadActionPermission(selectedFolder.Value);
                    var categoryId = Convert.ToInt32(this.lblCategoryId.Value);
                    //var folderPermission = this._FolderPermisstionService.GetFolderByRoleIdList(Convert.ToInt32(Request.QueryString["doctype"]), GroupFunctionList)
                    //                           .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault())
                    //                           .Select(t => t.FolderId.GetValueOrDefault()).ToList();
                    var folderPermission = this.folderPermisstionService.GetByRoleId(Convert.ToInt32(Request.QueryString["doctype"]), UserSession.Current.User.RoleId.GetValueOrDefault())
                                               .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault())
                                               .Select(t => t.FolderId.GetValueOrDefault()).ToList();
                    folderPermission = folderPermission.Union(this.folderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"]))
                                                                     .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault())
                                                                     .Select(t => t.FolderId.GetValueOrDefault()))
                                                                    .Distinct().ToList();

                    var listFolderPermission = this.AdminGroup.Contains(UserSession.Current.RoleId)
                                    ? this.folderService.GetAllByCategory(Convert.ToInt32(Request.QueryString["doctype"]))
                                    : this.folderService.GetSpecificFolderStatic(folderPermission);
                    selectedFolder.Selected = true;
                    UpdatePropertiesFolder(folder);
                    var folderlist = listFolderPermission.Where(t => t.ParentID.GetValueOrDefault() == Convert.ToInt32(folderId)).OrderBy(t => t.Name).ToList();
                    if (folderlist.Any())
                    {
                        selectedFolder.Nodes.Clear();
                        foreach (var folderitem in folderlist)
                        {
                            var nodechild = new RadTreeNode();
                            nodechild.Text = folderitem.Name;
                            nodechild.Value = folderitem.ID.ToString();
                            nodechild.ImageUrl = "~/Images/folderdir16.png";
                            nodechild.Target = selectedFolder.Target;
                            if (listFolderPermission.Count(t => t.ParentID.GetValueOrDefault() == folderitem.ID) > 0)
                            {
                                nodechild.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                            }
                            selectedFolder.Nodes.Add(nodechild);
                        }
                    }
                    selectedFolder.Expanded = true;

                    List<int> listTemp = new List<int>();
                    var listFolder = this.GetAllChildren(Convert.ToInt32(selectedFolder.Value), listFolderPermission, ref listTemp);
                    listFolder.Add(Convert.ToInt32(selectedFolder.Value));
                    Session["ListIdFolder"] = listFolder;
                    this.grdDocument.CurrentPageIndex = 0;
                    this.LoadDocuments(true, false, listFolder, false);
                }
            }

            else if (e.Argument == "RebindAndNavigate")
            {
                grdDocument.MasterTableView.SortExpressions.Clear();
                grdDocument.MasterTableView.GroupByExpressions.Clear();
                ////grdDocument.MasterTableView.CurrentPageIndex = grdDocument.MasterTableView.PageCount - 1;
                grdDocument.Rebind();
            }
            else if (e.Argument.Contains("ListAllDocuments"))
            {
                ////Session.Add("IsListAll", true);

                this.RadSlidingPane1.DockOnOpen = false;
                if (this.radTreeFolder.SelectedNode != null && this.radTreeFolder.SelectedNode.GetAllNodes().Count > 0)
                {
                    // this.LoadActionPermission(this.radTreeFolder.SelectedNode.Value);
                    this.grdDocument.CurrentPageIndex = 0;
                    this.LoadDocuments(true, false, new List<int>(), false);
                }
            }
            else if (e.Argument.Contains("TreeView"))
            {
                ////Session.Add("IsListAll", false);
                this.RadSlidingPane1.DockOnOpen = true;
                if (this.radTreeFolder.SelectedNode != null && this.radTreeFolder.SelectedNode.GetAllNodes().Count > 0)
                {
                    this.grdDocument.MasterTableView.GetColumn("IsSelected").Display = false;
                    this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Display = false;

                    var childFolders = this.radTreeFolder.SelectedNode.GetAllNodes().Where(t => t.Level == this.radTreeFolder.SelectedNode.Level + 1).ToList();
                    var listFolder = childFolders.Select(t => new Document()
                    {
                        ID = Convert.ToInt32(t.Value),
                        Name = t.Text,
                        FileExtensionIcon = "Images/folderdir16.png",
                        IsFolder = true
                    });

                    this.grdDocument.CurrentPageIndex = 0;
                    this.grdDocument.DataSource = listFolder;
                    this.grdDocument.DataBind();
                }
            }

            else if (e.Argument == "CheckoutDoc")
            {
                var docObj = this.documentService.GetById(Convert.ToInt32(this.lblDocId.Value));
                if (docObj != null)
                {
                    docObj.IsCheckOut = true;
                    docObj.CurrentCheckoutByUser = UserSession.Current.User.Id;
                    this.documentService.Update(docObj);
                    this.grdDocument.Rebind();
                }
            }
            //17/3/2020
            //else if (e.Argument.Contains("RowClick"))
            //{
            //    var docObj = this.documentService.GetById(Convert.ToInt32(e.Argument.Split('_')[1]));
            //    if (docObj != null)
            //    {
            //        var btnEdit = (RadButton)this.radMenuFile.Items[0].FindControl("btnEdit");
            //        var btnFileDelete = (RadButton)this.radMenuFile.Items[0].FindControl("btnFileDelete");
            //        var btnFileDownload = (RadButton)this.radMenuFile.Items[0].FindControl("btnFileDownload");
            //        var btnFileView = (RadButton)this.radMenuFile.Items[0].FindControl("btnFileView");
            //        var btnFileCheckout = (RadButton)this.radMenuFile.Items[0].FindControl("btnFileCheckout");
            //        var btnFileRevisionHistory = (RadButton)this.radMenuFile.Items[0].FindControl("btnFileRevisionHistory");
            //        var btnFileDuplicateRevision = (RadButton)this.radMenuFile.Items[0].FindControl("btnFileDuplicateRevision");
            //        var filepermission = (RadButton)this.radMenuFile.Items[0].FindControl("filepermission");
            //        if (docObj.IsCheckOut)
            //        {
            //            btnEdit.Enabled = false;
            //            btnFileDelete.Enabled = false;
            //            btnFileDownload.Enabled = false;
            //            btnFileView.Enabled = false;
            //            btnFileCheckout.Enabled = false;
            //            btnFileRevisionHistory.Enabled = false;
            //            btnFileDuplicateRevision.Enabled = false;
            //            filepermission.Enabled = false;
            //        }
            //        else
            //        {
            //            btnEdit.Enabled = true;
            //            btnFileDelete.Enabled = true;
            //            btnFileDownload.Enabled = true;
            //            btnFileView.Enabled = true;
            //            btnFileCheckout.Enabled = true;
            //            btnFileRevisionHistory.Enabled = true;
            //            btnFileDuplicateRevision.Enabled = true;
            //            filepermission.Enabled = true;
            //        }
            //    }
            //}
        }

        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            ////var isListAll = this.Session["IsListAll"] != null && Convert.ToBoolean(this.Session["IsListAll"]);
            var pageSize = this.grdDocument.PageSize;
            var currentPage = this.grdDocument.CurrentPageIndex;
            var startingRecordNumber = currentPage * pageSize;

            this.LoadDocuments(false, false, new List<int>(), false);

            ////var expression = new GridGroupByExpression();
            ////var gridGroupByField = new GridGroupByField { FieldAlias = "Folder", FieldName = "DirName" };

            ////expression.GroupByFields.Add(gridGroupByField);
            ////this.grdDocument.MasterTableView.GroupByExpressions.Add(expression);
        }

        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());

            var docObj = this.documentService.GetById(docId);
            if (docObj != null)
            {
                docObj.IsLeaf = false;
                docObj.IsDelete = true;
                docObj.LastUpdatedBy = UserSession.Current.User.Id;
                docObj.LastUpdatedDate = DateTime.Now;
                this.documentService.Update(docObj);

                List<int> ListNodeExpanded = new List<int>();
                GetListNodeExpanded(ref ListNodeExpanded, this.radTreeFolder.SelectedNode);
                UpdatePropertisFolder(docObj.CategoryID.GetValueOrDefault(), ListNodeExpanded);
                //if (this.documentService.GetById(docObj.ID))
                //{
                //    docObj.IsDelete = true;
                //    docObj.LastUpdatedBy = UserSession.Current.User.Id;
                //    docObj.LastUpdatedDate = DateTime.Now;
                //    this.documentService.Update(docObj);
                //}
                //else
                //{
                //    var filePathServer = string.Empty;
                //    var filePath = string.Empty;
                //    var listRelateDoc = this.documentService.GetAllRelateDocument(docId);
                //    if (listRelateDoc != null)
                //    {
                //        foreach (var document in listRelateDoc)
                //        {
                //            ////filePath = Server.MapPath(objDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                //            filePath = document.FilePath;
                //            filePathServer = Server.MapPath(HostingEnvironment.ApplicationVirtualPath == "/"
                //                                           ? document.FilePath
                //                                           : document.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                //            document.IsDelete = true;
                //            document.LastUpdatedBy = UserSession.Current.User.Id;
                //            document.LastUpdatedDate = DateTime.Now;
                //            this.documentService.Update(document);
                //        }
                //        if (File.Exists(filePathServer))
                //        {
                //            File.Delete(filePathServer);
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// The grd document_ item command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.RebindGridCommandName)
            {
                this.grdDocument.Rebind();
            }
            else if (e.CommandName == RadGrid.ExportToExcelCommandName)
            {
                if (this.radTreeFolder.SelectedNode != null)
                {
                    //var filePath = Server.MapPath(@"DocumentResource\Exports") + @"\";
                    //var fileName = this.radTreeFolder.SelectedNode.ParentNode != null
                    //               ? Utility.RemoveSpecialCharacter(this.radTreeFolder.SelectedNode.ParentNode.Text) + " - "
                    //                 + Utility.RemoveSpecialCharacter(this.radTreeFolder.SelectedNode.Text)
                    //               : Utility.RemoveSpecialCharacter(this.radTreeFolder.SelectedNode.Text);

                    var folderId = Convert.ToInt32(this.lblFolderId.Value);
                    var folderObj = this.folderService.GetById(folderId);
                    var tempListFolderId = new List<int>();

                    var dtFull = new DataTable();
                    dtFull.Columns.AddRange(new[]
                    {
                        new DataColumn("DocId", typeof(String)),
                        new DataColumn("NoIndex", typeof(String)),
                        new DataColumn("FileName", typeof(String)),
                        new DataColumn("Title", typeof(String)),
                        new DataColumn("DocumentNumber", typeof(String)),
                        new DataColumn("Revision", typeof(String)),
                        new DataColumn("Area", typeof(String)),
                        new DataColumn("Discipline", typeof(String)),
                        new DataColumn("KKSCode", typeof(String)),
                        new DataColumn("KKSName", typeof(String)),
                        new DataColumn("UASCode", typeof(String)),
                        new DataColumn("UASName", typeof(String))
                    });

                    var filePath = Server.MapPath("~/Exports") + @"\";
                    var workbook = new Workbook();
                    workbook.Open(filePath + @"Template\DocumentList.xls");
                    var sheets = workbook.Worksheets;

                    // Export master List
                    var docInFolder = this.documentService.GetAllByFolder(folderId);
                    int count = 0;
                    foreach (var docItem in docInFolder)
                    {
                        count++;
                        DataRow dr = dtFull.NewRow();
                        dr[0] = docItem.ID.ToString();
                        dr[1] = count.ToString();
                        dr[2] = docItem.Name;
                        dr[3] = docItem.Title;
                        dr[4] = docItem.DocumentNumber;
                        dr[5] = docItem.RevisionName;
                        dr[7] = docItem.DisciplineName;
                        dtFull.Rows.Add(dr);
                    }

                    sheets[1].Cells.ImportDataTable(dtFull, false, 4, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
                    sheets[2].Cells.ImportDataTable(dtFull, false, 4, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
                    sheets[1].Cells["A4"].PutValue(docInFolder.Count);
                    sheets[1].Name = folderObj.Name;
                    //worksheet1.FreezePanes(1, 1, 1, 1);
                    //worksheet1.Cells.HideColumn(0);
                    //worksheet1.Cells.HideColumn(1);
                    ////worksheet1.AutoFilter.Range = "A1:O" + listDocuments.Count + 1;
                    var fileName = folderObj.Name;
                    var saveFileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + fileName;
                    workbook.Save(filePath + saveFileName + ".xls");
                    this.DownloadByWriteByte(filePath + saveFileName + ".xls", fileName + ".xls", false);
                }
                //else
                //{
                //    var listDocuments = new List<Document>();

                //    ////var folder = this.folderService.GetById(folderId);
                //    tempListFolderId.Add(folderId);

                //    #region "Code apply when use ListAll fucntion for export edit file"
                //    ////if (isListAll)
                //    ////{
                //    ////    var folder = this.folderService.GetById(folderId);
                //    ////    tempListFolderId.Add(folderId);
                //    ////    var folderPermission =
                //    ////        this.groupDataPermissionService.GetByRoleId(
                //    ////            UserSession.Current.User.RoleId.GetValueOrDefault()).Where(
                //    ////                t => t.CategoryIdList == folder.CategoryID.ToString() && !string.IsNullOrEmpty(t.FolderIdList)).Select(
                //    ////                    t => Convert.ToInt32(t.FolderIdList)).ToList();

                //    ////    if (selectedFolder.GetAllNodes().Count > 0)
                //    ////    {
                //    ////        tempListFolderId.AddRange(
                //    ////        from folderNode in selectedFolder.GetAllNodes()
                //    ////        where folderPermission.Contains(Convert.ToInt32(folderNode.Value))
                //    ////        select Convert.ToInt32(folderNode.Value));

                //    ////        tempListFolderId.Add(folderId);
                //    ////    }
                //    ////    else
                //    ////    {
                //    ////        tempListFolderId.Add(folderId);
                //    ////    }
                //    ////}
                //    #endregion

                //    strFolderPermission = tempListFolderId.Aggregate(strFolderPermission, (current, t) => current + t + ",");

                //    var categoryId = Convert.ToInt32(this.radPbCategories.SelectedItem.Value);
                //    var revisionList = this.revisionService.GetAll();
                //    var statusList = this.statusService.GetAllByCategory(categoryId).OrderBy(t => t.Name).ToList();
                //    var disciplineList = this.disciplineService.GetAllByCategory(categoryId).OrderBy(t => t.Name).ToList();
                //    var documentTypeList = this.documentTypeService.GetAllByCategory(categoryId).OrderBy(t => t.Name).ToList();
                //    var receivedFromList = this.receivedFromService.GetAllByCategory(categoryId).OrderBy(t => t.Name).ToList();

                //    var workbook = new Workbook();
                //    workbook.Open(filePath + @"Template\EditDataFileTemplate.xls");

                //    // Get the first worksheet.
                //    var worksheet1 = workbook.Worksheets[0];

                //    worksheet1.Cells[0, 0].PutValue("IsEdited");
                //    worksheet1.Cells[0, 1].PutValue("ID");
                //    worksheet1.Cells[0, 2].PutValue("File Name");
                //    worksheet1.Cells[0, 3].PutValue("Document No.");
                //    worksheet1.Cells[0, 4].PutValue("Title");
                //    //worksheet1.Cells[0, 5].PutValue("Revision");
                //    //worksheet1.Cells[0, 6].PutValue("Duplicate");

                //    worksheet1.Cells[0, 7].PutValue("OrderNo.");
                //    worksheet1.Cells[0, 8].PutValue("Dispatch Date");
                //    //sworksheet1.Cells[0, 9].PutValue("Date Out");
                //    worksheet1.Cells[0, 5].PutValue("Document Type");
                //    //worksheet1.Cells[0, 10].PutValue("Expiry Date");
                //    //worksheet1.Cells[0, 11].PutValue("Received From");
                //    //worksheet1.Cells[0, 12].PutValue("Transmittal No.");
                //    //worksheet1.Cells[0, 13].PutValue("Received Date");
                //    worksheet1.Cells[0, 6].PutValue("Remark");
                //    // worksheet1.Cells[0, 15].PutValue("Well");


                //    var dataset = new DataSet();
                //    using (var conn = new SqlConnection(ConfigurationSettings.AppSettings["SiteSqlServer"]))
                //    {
                //        using (var cmd = new SqlCommand("GetAllDocBySpecialFolder", conn))
                //        {
                //            cmd.CommandType = CommandType.StoredProcedure;
                //            cmd.Parameters.AddWithValue("@p_FolderList", strFolderPermission);
                //            conn.Open();
                //            using (var da = new SqlDataAdapter())
                //            {
                //                da.SelectCommand = cmd;
                //                da.Fill(dataset);
                //            }

                //            conn.Close();
                //        }
                //    }

                //    if (dataset.Tables.Count > 0)
                //    {
                //        // Add a new worksheet and access it.
                //        ////var i = workbook.Worksheets.Add();

                //        var worksheet2 = workbook.Worksheets[1];

                //        // Create a range in the second worksheet.
                //        // var rangeRevisionList = worksheet2.Cells.CreateRange("A1", "A" + revisionList.Count);
                //        // var rangeStatusList = worksheet2.Cells.CreateRange("B1", "B" + statusList.Count);
                //        //   var rangeDisciplineList = worksheet2.Cells.CreateRange("C1", "C" + disciplineList.Count);
                //        var rangeDocumentTypeList = worksheet2.Cells.CreateRange("D1", "D" + documentTypeList.Count);
                //        //  var rangeReceivedFromList = worksheet2.Cells.CreateRange("E1", "E" + receivedFromList.Count);
                //        //  var rangeDuplicateList = worksheet2.Cells.CreateRange("F1", "F1");

                //        // Name the range.
                //        //rangeRevisionList.Name = "RevisionList";
                //        //rangeStatusList.Name = "StatusList";
                //        //rangeDisciplineList.Name = "DisciplineList";
                //        rangeDocumentTypeList.Name = "DocumentTypeList";
                //        // rangeReceivedFromList.Name = "ReceivedFromList";
                //        //  rangeDuplicateList.Name = "DuplicateList";

                //        // Fill different cells with data in the range.
                //        //for (int j = 0; j < revisionList.Count; j++)
                //        //{
                //        //    rangeRevisionList[j, 0].PutValue(revisionList[j].Name);
                //        //}

                //        //for (int j = 0; j < statusList.Count; j++)
                //        //{
                //        //    rangeStatusList[j, 0].PutValue(statusList[j].Name);
                //        //}

                //        //for (int j = 0; j < disciplineList.Count; j++)
                //        //{
                //        //    rangeDisciplineList[j, 0].PutValue(disciplineList[j].Name);
                //        //}

                //        for (int j = 0; j < documentTypeList.Count; j++)
                //        {
                //            rangeDocumentTypeList[j, 0].PutValue(documentTypeList[j].Name);
                //        }

                //        //for (int j = 0; j < receivedFromList.Count; j++)
                //        //{
                //        //    rangeReceivedFromList[j, 0].PutValue(receivedFromList[j].Name);
                //        //}

                //        // Get the validations collection.
                //        var validations = worksheet1.Validations;
                //        //  this.CreateValidation("RevisionList", validations, 1, dataset.Tables[0].Rows.Count, 5, 5);
                //        //  this.CreateValidation("DuplicateList", validations, 1, dataset.Tables[0].Rows.Count, 6, 6);
                //        //   this.CreateValidation("StatusList", validations, 1, dataset.Tables[0].Rows.Count, 7, 7);
                //        //     this.CreateValidation("DisciplineList", validations, 1, dataset.Tables[0].Rows.Count, 8, 8);
                //        this.CreateValidation("DocumentTypeList", validations, 1, dataset.Tables[0].Rows.Count, 5, 5);
                //        //  this.CreateValidation("ReceivedFromList", validations, 1, dataset.Tables[0].Rows.Count, 11, 11);

                //        worksheet2.IsVisible = false;
                //        var worksheet3 = workbook.Worksheets[2];

                //        worksheet1.Cells.ImportDataTable(dataset.Tables[0], false, 1, 1, dataset.Tables[0].Rows.Count, 16, true);
                //        worksheet3.Cells.ImportDataTable(dataset.Tables[0], false, 1, 1, dataset.Tables[0].Rows.Count, 16, true);

                //        worksheet3.IsVisible = false;
                //    }

                //    worksheet1.FreezePanes(1, 3, 1, 3);
                //    worksheet1.Cells.HideColumn(0);
                //    worksheet1.Cells.HideColumn(1);
                //    worksheet1.AutoFilter.Range = "A1:P" + listDocuments.Count + 1;
                //    worksheet1.AutoFitRows();
                //    var saveFileName = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + fileName;
                //    workbook.Save(filePath + saveFileName + ".xls");
                //    this.DownloadByWriteByte(filePath + saveFileName + ".xls", fileName + ".xls", false);
                //}
            }
            //17/3/2020
            else if (e.CommandName == "RowClick")
            {
                //if (isFolder.Value == "False")
                //{
                //    this.radMenuFile.Enabled = true;
                //    var docObj = this.documentService.GetById(Convert.ToInt32(this.lblDocId.Value));
                //    if (docObj != null)
                //    {
                //        var btnEdit = (RadButton)this.radMenuFile.Items[0].FindControl("btnEdit");
                //        var btnFileDelete = (RadButton)this.radMenuFile.Items[0].FindControl("btnFileDelete");
                //        var btnFileDownload = (RadButton)this.radMenuFile.Items[0].FindControl("btnFileDownload");
                //        var btnFileView = (RadButton)this.radMenuFile.Items[0].FindControl("btnFileView");
                //        var btnFileCheckout = (RadButton)this.radMenuFile.Items[0].FindControl("btnFileCheckout");
                //        var btnFileCheckin = (RadButton)this.radMenuFile.Items[0].FindControl("btnFileCheckin");
                //        var btnFileRevisionHistory = (RadButton)this.radMenuFile.Items[0].FindControl("btnFileRevisionHistory");
                //        var btnFileDuplicateRevision = (RadButton)this.radMenuFile.Items[0].FindControl("btnFileDuplicateRevision");
                //        var filepermission = (RadButton)this.radMenuFile.Items[0].FindControl("filepermission");
                //        if (docObj.IsCheckOut)
                //        {
                //            btnEdit.Enabled = false;
                //            btnFileDelete.Enabled = false;
                //            btnFileDownload.Enabled = false;
                //            btnFileView.Enabled = false;
                //            btnFileCheckout.Enabled = false;
                //            btnFileRevisionHistory.Enabled = false;
                //            btnFileDuplicateRevision.Enabled = false;
                //            filepermission.Enabled = false;
                //        }
                //        else
                //        {

                //            if (!this.AdminGroup.Contains(UserSession.Current.RoleId))
                //            {
                //                //var filePermission = this._FolderPermisstionService.GetByRoleIdList(GroupFunctionList, docObj.ID).ToList();
                //                //filePermission.Add(this._FilePermissionService.GetByUserI(UserSession.Current.User.Id, docObj.ID));
                //                var folderPermission = (List<FolderPermission>)Session["ThisfolderPermission"];
                //                //replace filePermission = folderPermission
                //                if (folderPermission.Any())
                //                {
                //                    if (!folderPermission.Where(t => t.File_Create.GetValueOrDefault()).Any())
                //                    {
                //                        this.CustomerMenu.Items[0].Enabled = false;
                //                        btnFileDuplicateRevision.Enabled = false;
                //                    }

                //                    if (!folderPermission.Where(t => t.File_Delete.GetValueOrDefault()).Any())
                //                    { btnFileDelete.Enabled = false; }
                //                    if (!folderPermission.Where(t => t.File_Read.GetValueOrDefault()).Any())
                //                    {
                //                        btnFileView.Enabled = false; btnFileDownload.Enabled = false;
                //                        btnFileRevisionHistory.Enabled = false;
                //                    }
                //                    if (!folderPermission.Where(t => t.File_ChangePermission.GetValueOrDefault()).Any())
                //                    { filepermission.Enabled = false; }
                //                    if (!folderPermission.Where(t => t.File_Write.GetValueOrDefault()).Any())
                //                    {
                //                        btnFileCheckin.Enabled = false; btnFileCheckout.Enabled = false;
                //                        btnEdit.Enabled = false;
                //                    }
                //                }
                //                else
                //                {
                //                    //var folderPermission = (List<FolderPermission>)Session["ThisfolderPermission"];
                //                    if (!folderPermission.Where(t => t.File_Create.GetValueOrDefault()).Any())
                //                    {
                //                        this.CustomerMenu.Items[0].Enabled = false;
                //                        btnFileDuplicateRevision.Enabled = false;
                //                    }

                //                    if (!folderPermission.Where(t => t.File_Delete.GetValueOrDefault()).Any())
                //                    { btnFileDelete.Enabled = false; }
                //                    if (!folderPermission.Where(t => t.File_Read.GetValueOrDefault()).Any())
                //                    {
                //                        btnFileView.Enabled = false; btnFileDownload.Enabled = false;
                //                        btnFileRevisionHistory.Enabled = false;
                //                    }
                //                    if (!folderPermission.Where(t => t.File_ChangePermission.GetValueOrDefault()).Any())
                //                    { filepermission.Enabled = false; }
                //                    if (!folderPermission.Where(t => t.File_Write.GetValueOrDefault()).Any())
                //                    {
                //                        btnFileCheckin.Enabled = false; btnFileCheckout.Enabled = false;
                //                        btnEdit.Enabled = false;
                //                    }
                //                }
                //            }
                //            else
                //            {
                //                btnEdit.Enabled = true;
                //                btnFileDelete.Enabled = true;
                //                btnFileDownload.Enabled = true;
                //                btnFileView.Enabled = true;
                //                btnFileCheckout.Enabled = true;
                //                btnFileRevisionHistory.Enabled = true;
                //                btnFileDuplicateRevision.Enabled = true;
                //                filepermission.Enabled = true;
                //            }


                //        }

                //        this.lblDocumentCount.Visible = false;
                //        this.txtDocumentCount.Visible = false;
                //        this.lblParentFolder.Visible = false;
                //        this.txtParentFolder.Visible = false;
                //        this.lblFolderCount.Visible = false;
                //        this.txtFolderCount.Visible = false;

                //        var creater = this.userService.GetByID(docObj.CreatedBy.GetValueOrDefault());
                //        var uploader = this.userService.GetByID(docObj.LastUpdatedBy.GetValueOrDefault());
                //        this.lblCreatedBy.Text = creater != null ? creater.UserNameFullName : "-";
                //        this.lblCreatedTime.Text = docObj.CreatedDate.ToString();
                //        this.lblUpdatedBy.Text = uploader != null ? uploader.UserNameFullName : "-";
                //        this.lblUpdatedTime.Text = docObj.LastUpdatedDate.ToString();
                //        this.lblDiskUsage.Text = SizeSuffix(docObj.FileSize.HasValue ? Convert.ToInt64(docObj.FileSize.Value) : 0, 1);

                //        // check extension file
                //        List<string> extensionfile = new List<string>() { "exe", "dwg", "rar", "inf", "zip", "7z", "lnk" };
                //        if (extensionfile.Contains(docObj.FileExtension.ToLower())) btnFileView.Visible = false;
                //    }
                //}
                /* delete code click foler*/

                /* if (isFolder.Value.ToLower() == "true")
                 {
                     //this.radMenuFile.Enabled = false;
                     var folderId = Convert.ToInt32(this.lblDocId.Value);
                     var folder = this.folderService.GetById(folderId);
                     var selectedFolder = this.radTreeFolder.FindNodeByValue(this.lblDocId.Value);
                     if (selectedFolder != null)
                     {
                         this.lbFolderSize.Value = (folder.FolderSize / 1024 / 1024).ToString();
                         this.LoadActionPermission(selectedFolder.Value);
                         var categoryId = Convert.ToInt32(this.lblCategoryId.Value);
                         //var folderPermission = this._FolderPermisstionService.GetFolderByRoleIdList(Convert.ToInt32(Request.QueryString["doctype"]), GroupFunctionList)
                         //                                                   .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault());
                         var folderPermission = this.folderPermisstionService.GetByRoleId(Convert.ToInt32(Request.QueryString["doctype"]), UserSession.Current.User.RoleId.GetValueOrDefault())
                                                .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault()).ToList();

                         folderPermission = folderPermission.Union(this.folderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"]))
                                                                                                  .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault()))
                                                                                                  .ToList();
                         var folderPermissionID = folderPermission.Where(t => t.FolderId == folderId).ToList();
                         var listFolderPermission = this.AdminGroup.Contains(UserSession.Current.RoleId)
                                                                 ? this.folderService.GetAllByCategory(Convert.ToInt32(Request.QueryString["doctype"]))
                                                                 : this.folderService.GetSpecificFolderStatic(folderPermission.Select(t => t.FolderId.GetValueOrDefault())
                                                                                                 .Distinct().ToList());
                         selectedFolder.Selected = true;
                         UpdatePropertiesFolder(folder);
                         var folderlist = listFolderPermission.Where(t => t.ParentID.GetValueOrDefault() == Convert.ToInt32(folderId)).OrderBy(t => t.Name).ToList();
                         if (folderlist.Any())
                         {
                             selectedFolder.Nodes.Clear();
                             foreach (var folderitem in folderlist)
                             {
                                 var nodechild = new RadTreeNode();
                                 nodechild.Text = folderitem.Name;
                                 nodechild.Value = folderitem.ID.ToString();
                                 nodechild.ImageUrl = "~/Images/folderdir16.png";
                                 nodechild.Target = selectedFolder.Target;
                                 if (listFolderPermission.Count(t => t.ParentID.GetValueOrDefault() == folderitem.ID) > 0)
                                 {
                                     nodechild.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                                 }
                                 selectedFolder.Nodes.Add(nodechild);
                             }
                         }
                         selectedFolder.Expanded = true;

                         List<int> listTemp = new List<int>();
                         var listFolder = this.GetAllChildren(Convert.ToInt32(selectedFolder.Value), listFolderPermission, ref listTemp);
                         listFolder.Add(Convert.ToInt32(selectedFolder.Value));
                         Session["ListIdFolder"] = listFolder;
                         this.grdDocument.CurrentPageIndex = 0;
                         this.LoadDocuments(true, false, listFolder, false);
                         ChangeTooltip(selectedFolder, folderPermissionID);
                     }
                 }*/
            }
        }

        protected readonly string[] SizeSuffixes =
                   { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
        public string SizeSuffix(Int64 value, int decimalPlaces = 1)
        {
            if (decimalPlaces < 0) { throw new ArgumentOutOfRangeException("decimalPlaces"); }
            if (value < 0) { return "-" + SizeSuffix(-value); }
            if (value == 0) { return string.Format("{0:n" + decimalPlaces + "} bytes", 0); }

            // mag is 0 for bytes, 1 for KB, 2, for MB, etc.
            int mag = (int)Math.Log(value, 1024);

            // 1L << (mag * 10) == 2 ^ (10 * mag) 
            // [i.e. the number of bytes in the unit corresponding to mag]
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            // make adjustment when the value is large enough that
            // it would round up to 1000 or more
            if (Math.Round(adjustedSize, decimalPlaces) >= 1000)
            {
                mag += 1;
                adjustedSize /= 1024;
            }

            return string.Format("{0:n" + decimalPlaces + "} {1}",
                adjustedSize,
                SizeSuffixes[mag]);
        }

        private void ReLoadTreeFolder(int categoryId, RadTreeNode ParrentNodeSelect, int idFodler)
        {
            //var folderPermission = this._FolderPermisstionService.GetFolderByRoleIdList(Convert.ToInt32(Request.QueryString["doctype"]), GroupFunctionList)
            //                                  .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault())
            //                                  .Select(t => t.FolderId.GetValueOrDefault()).ToList();
            var folderPermission = this.folderPermisstionService.GetByRoleId(Convert.ToInt32(Request.QueryString["doctype"]), UserSession.Current.User.RoleId.GetValueOrDefault())
                                               .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault())
                                               .Select(t => t.FolderId.GetValueOrDefault()).ToList();
            folderPermission = folderPermission.Union(this.folderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"]))
                                                             .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault())
                                                             .Select(t => t.FolderId.GetValueOrDefault()))
                                                            .Distinct().ToList();
            var listFolder = this.AdminGroup.Contains(UserSession.Current.RoleId)
                            ? this.folderService.GetAllByCategory(Convert.ToInt32(Request.QueryString["doctype"]))
                            : this.folderService.GetSpecificFolderStatic(folderPermission);
            if (listFolder.Count > 0)
            {
                var nodeRoot = listFolder.Find(t => t.ParentID == null);

                var root = new RadTreeNode();
                root.Text = nodeRoot.Name;
                root.ImageUrl = "~/Images/folderdir16.png";
                root.Value = nodeRoot.ID.ToString();
                root.Target = nodeRoot.CategoryID.ToString();
                root.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                root.Expanded = true;

                List<int> ListNodeExpanded = new List<int>();
                ListNodeExpanded.Add(idFodler);
                GetListNodeExpanded(ref ListNodeExpanded, ParrentNodeSelect);
                AddNode(ref root, listFolder, ListNodeExpanded, nodeRoot);
                this.radTreeFolder.Nodes.Clear();
                this.radTreeFolder.Nodes.Add(root);
            }
        }
        private void ReLoadTreeFolder(int categoryId, int ParrentNodeSelect, int idFodler)
        {
            //var folderPermission = this._FolderPermisstionService.GetFolderByRoleIdList(Convert.ToInt32(Request.QueryString["doctype"]), GroupFunctionList)
            //                                  .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault())
            //                                  .Select(t => t.FolderId.GetValueOrDefault()).ToList();
            var folderPermission = this.folderPermisstionService.GetByRoleId(Convert.ToInt32(Request.QueryString["doctype"]), UserSession.Current.User.RoleId.GetValueOrDefault())
                                               .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault())
                                               .Select(t => t.FolderId.GetValueOrDefault()).ToList();
            folderPermission = folderPermission.Union(this.folderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"]))
                                                             .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault())
                                                             .Select(t => t.FolderId.GetValueOrDefault()))
                                                            .Distinct().ToList();
            var listFolder = this.AdminGroup.Contains(UserSession.Current.RoleId)
                            ? this.folderService.GetAllByCategory(Convert.ToInt32(Request.QueryString["doctype"]))
                            : this.folderService.GetSpecificFolderStatic(folderPermission);
            if (listFolder.Count > 0)
            {
                var nodeRoot = listFolder.Find(t => t.ParentID == null);

                var root = new RadTreeNode();
                root.Text = nodeRoot.Name;
                root.ImageUrl = "~/Images/folderdir16.png";
                root.Value = nodeRoot.ID.ToString();
                root.Target = nodeRoot.CategoryID.ToString();
                root.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                root.Expanded = true;

                List<int> ListNodeExpanded = new List<int>();
                ListNodeExpanded.Add(idFodler);
                GetListNodeExpanded(ref ListNodeExpanded, ParrentNodeSelect);
                AddNode(ref root, listFolder, ListNodeExpanded, nodeRoot);
                this.radTreeFolder.Nodes.Clear();
                this.radTreeFolder.Nodes.Add(root);
                var selectedFolder = this.radTreeFolder.FindNodeByValue(idFodler.ToString());
                if (selectedFolder != null)
                {
                    var folder = this.folderService.GetById(Convert.ToInt32(idFodler));
                    selectedFolder.Selected = true;
                    UpdatePropertiesFolder(folder);
                    selectedFolder.Expanded = true;
                    this.lbFolderSize.Value = (folder.FolderSize / 1024 / 1024).ToString();
                }


            }
        }
        private void AddNode(ref RadTreeNode Root, List<Folder> ListFolder, List<int> CurrentNodeTree, Folder currentfolder)
        {
            var listNode = ListFolder.Where(t => t.ParentID.GetValueOrDefault() == currentfolder.ID).OrderBy(t => t.Name);
            foreach (var folder in listNode)
            {

                RadTreeNode node = new RadTreeNode();
                node.Text = folder.Name;
                node.Value = folder.ID.ToString();
                node.ImageUrl = "~/Images/folderdir16.png";
                node.Target = currentfolder.CategoryID.ToString();
                node.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                if (CurrentNodeTree.Contains(folder.ID))
                {
                    Root.Expanded = true;
                    AddNode(ref node, ListFolder, CurrentNodeTree, folder);

                }
                Root.Nodes.Add(node);
            }
        }

        private void GetListNodeExpanded(ref List<int> ListNode, RadTreeNode currentFolder)
        {
            if (currentFolder != null) ListNode.Add(Convert.ToInt32(currentFolder.Value));
            if (currentFolder != null && currentFolder.ParentNode != null)
            {
                GetListNodeExpanded(ref ListNode, currentFolder.ParentNode);
            }
        }
        private void GetListNodeExpanded(ref List<int> ListNode, int _currentFolder)
        {
            var currentFolder = this.folderService.GetById(_currentFolder);
            if (currentFolder != null) ListNode.Add(Convert.ToInt32(currentFolder.ID));
            if (currentFolder != null && currentFolder.ParentID != null)
            {
                GetListNodeExpanded(ref ListNode, currentFolder.ParentID.GetValueOrDefault());
            }
        }
        private void LoadTreeFolder(int categoryId)
        {
            //var folderPermission = this._FolderPermisstionService.GetFolderByRoleIdList(Convert.ToInt32(Request.QueryString["doctype"]), GroupFunctionList)
            //                                    .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault())
            //                                    .Select(t => t.FolderId.GetValueOrDefault()).ToList();
            var folderPermission = this.folderPermisstionService.GetByRoleId(Convert.ToInt32(Request.QueryString["doctype"]), UserSession.Current.User.RoleId.GetValueOrDefault())
                                               .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault())
                                               .Select(t => t.FolderId.GetValueOrDefault()).ToList();
            folderPermission = folderPermission.Union(this.folderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"]))
                                                             .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault())
                                                             .Select(t => t.FolderId.GetValueOrDefault()))
                                                            .Distinct().ToList();

            var listFolder = this.AdminGroup.Contains(UserSession.Current.RoleId)
                            ? this.folderService.GetAllByCategory(Convert.ToInt32(Request.QueryString["doctype"]))
                            : this.folderService.GetSpecificFolderStatic(folderPermission);
            if (listFolder.Count > 0)
            {
                var nodeRoot = listFolder.Find(t => t.ParentID == null);

                var root = new RadTreeNode();
                root.Text = nodeRoot.Name;
                root.ImageUrl = "~/Images/folderdir16.png";
                root.Value = nodeRoot.ID.ToString();
                root.Target = nodeRoot.CategoryID.ToString();
                root.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                root.Expanded = true;

                var listNode = listFolder.Where(t => t.ParentID.GetValueOrDefault() == nodeRoot.ID).OrderBy(t => t.Name);
                foreach (var folder in listNode)
                {
                    RadTreeNode node = new RadTreeNode();
                    node.Text = folder.Name;
                    node.Value = folder.ID.ToString();
                    node.ImageUrl = "~/Images/folderdir16.png";
                    node.Target = nodeRoot.CategoryID.ToString();
                    node.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                    root.Nodes.Add(node);
                }

                this.radTreeFolder.Nodes.Clear();
                this.radTreeFolder.Nodes.Add(root);
            }
            //this.SortNodes(this.radTreeFolder.Nodes);
        }

        protected void radPbCategories_ItemClick(object sender, RadPanelBarEventArgs e)
        {
            this.radTreeFolder.Nodes.Clear();

            //var folderPermission = this._FolderPermisstionService.GetFolderByRoleIdList(Convert.ToInt32(Request.QueryString["doctype"]), GroupFunctionList)
            //                                    .Where(t => t.FolderId != null)
            //                                    .Select(t => t.FolderId.GetValueOrDefault()).ToList();
            var folderPermission = this.folderPermisstionService.GetByRoleId(Convert.ToInt32(Request.QueryString["doctype"]), UserSession.Current.User.RoleId.GetValueOrDefault())
                                               .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault())
                                               .Select(t => t.FolderId.GetValueOrDefault()).ToList();
            folderPermission = folderPermission.Union(this.folderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"]))
                                                             .Where(t => t.FolderId != null)
                                                             .Select(t => t.FolderId.GetValueOrDefault()))
                                                            .Distinct().ToList();

            var listFolder = this.AdminGroup.Contains(UserSession.Current.RoleId)
                                    ? this.folderService.GetAllByCategory(Convert.ToInt32(e.Item.Value))
                                    : this.folderService.GetSpecificFolderStatic(folderPermission);

            this.radTreeFolder.DataTextField = "Name";
            this.radTreeFolder.DataValueField = "ID";
            this.radTreeFolder.DataFieldID = "ID";
            this.radTreeFolder.DataFieldParentID = "ParentID";

            this.radTreeFolder.DataSource = listFolder;
            this.radTreeFolder.DataBind();
            this.radTreeFolder.Nodes[0].Expanded = true;

            this.RestoreExpandStateTreeView();
            this.SortNodes(this.radTreeFolder.Nodes);

            this.grdDocument.Rebind();
        }

        /// <summary>
        /// The grd document_ item data bound.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                //if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
                //{
                //    var id = Request.QueryString["docId"].ToString();
                //    if (id == item.GetDataKeyValue("ID").ToString())
                //    {
                //        item.Selected = true;
                //    }
                //}
            }
        }

        protected void grdDocument_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                var item = e.Item as GridEditableItem;
                var ddlRevision = item.FindControl("ddlRevision") as RadComboBox;
                var ddlStatus = item.FindControl("ddlStatus") as RadComboBox;
                var ddlDiscipline = item.FindControl("ddlDiscipline") as RadComboBox;
                var ddlDocumentType = item.FindControl("ddlDocumentType") as RadComboBox;
                var ddlReceivedFrom = item.FindControl("ddlReceivedFrom") as RadComboBox;
                var txtReceivedDate = item.FindControl("txtReceivedDate") as RadDatePicker;
                var txtName = item.FindControl("txtName") as TextBox;
                var txtDocumentNumber = item.FindControl("txtDocumentNumber") as TextBox;
                var txtTitle = item.FindControl("txtTitle") as TextBox;
                var txtRemark = item.FindControl("txtRemark") as TextBox;
                var txtWell = item.FindControl("txtWell") as TextBox;
                var txtTransmittalNumber = item.FindControl("txtTransmittalNumber") as TextBox;

                var docId = Convert.ToInt32(item.GetDataKeyValue("ID"));

                var objDoc = this.documentService.GetById(docId);
                var oldName = objDoc.Name;
                var currentRevision = objDoc.RevisionID;
                var newRevision = Convert.ToInt32(ddlRevision.SelectedValue);
                var watcherService = new ServiceController(ServiceName);
                if (currentRevision != 0 && currentRevision != null && currentRevision != newRevision)
                {
                    var newObjDoc = new Document
                    {
                        DocIndex = objDoc.DocIndex + 1,
                        ParentID = objDoc.ParentID,
                        FolderID = objDoc.FolderID,
                        DirName = objDoc.DirName,
                        FilePath = objDoc.FilePath,
                        RevisionFilePath = objDoc.RevisionFilePath,
                        FileExtension = objDoc.FileExtension,
                        FileExtensionIcon = objDoc.FileExtensionIcon,
                        FileNameOriginal = objDoc.FileNameOriginal,
                        KeyWords = objDoc.KeyWords,
                        IsDelete = false,
                        CreatedDate = DateTime.Now,
                        CreatedBy = UserSession.Current.User.Id,

                        Name = txtName.Text.Trim(),
                        DocumentNumber = txtDocumentNumber.Text.Trim(),
                        Title = txtTitle.Text.Trim(),
                        RevisionID = Convert.ToInt32(ddlRevision.SelectedValue),

                        RevisionName = ddlRevision.Text,
                        DocumentTypeName = ddlDocumentType.SelectedItem.Text,
                        StatusName = ddlStatus.SelectedItem.Text,
                        DisciplineName = ddlDiscipline.SelectedItem.Text,
                        ReceivedFromName = ddlReceivedFrom.SelectedItem.Text,

                        StatusID = Convert.ToInt32(ddlStatus.SelectedValue),
                        DisciplineID = Convert.ToInt32(ddlDiscipline.SelectedValue),
                        DocumentTypeID = Convert.ToInt32(ddlDocumentType.SelectedValue),
                        ReceivedFromID = Convert.ToInt32(ddlReceivedFrom.SelectedValue),
                        ReceivedDate = txtReceivedDate.SelectedDate,
                        Remark = txtRemark.Text.Trim(),
                        Well = txtWell.Text.Trim(),
                        TransmittalNumber = txtTransmittalNumber.Text.Trim()
                    }; ////Utilities.Utility.Clone(objDoc);

                    if (!string.IsNullOrEmpty(newObjDoc.RevisionName))
                    {
                        newObjDoc.RevisionFileName = newObjDoc.RevisionName + "_" + newObjDoc.Name;
                    }
                    else
                    {
                        newObjDoc.RevisionFileName = newObjDoc.Name;
                    }


                    newObjDoc.RevisionFilePath = newObjDoc.RevisionFilePath.Substring(
                        0, newObjDoc.RevisionFilePath.LastIndexOf('/')) + "/" + DateTime.Now.ToString("ddMMyyhhmmss") + "_" + newObjDoc.RevisionFileName;

                    // Allway check revision file is exist when update
                    ////var filePath = Server.MapPath(newObjDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                    ////var revisionFilePath = Server.MapPath(newObjDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                    var filePath = Server.MapPath(HostingEnvironment.ApplicationVirtualPath == "/"
                                                   ? newObjDoc.FilePath
                                                   : newObjDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                    var revisionFilePath = Server.MapPath(HostingEnvironment.ApplicationVirtualPath == "/"
                                                ? newObjDoc.RevisionFilePath
                                                : newObjDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                    if (File.Exists(filePath) && !File.Exists(revisionFilePath))
                    {
                        File.Copy(filePath, revisionFilePath, true);
                    }
                    // End check

                    if (currentRevision < newRevision)
                    {
                        objDoc.IsLeaf = false;
                        newObjDoc.IsLeaf = true;
                        if (objDoc.ParentID == null)
                        {
                            newObjDoc.ParentID = objDoc.ID;
                        }

                        this.documentService.Update(objDoc);
                    }
                    else
                    {
                        newObjDoc.IsLeaf = false;

                        ////filePath = Server.MapPath(newObjDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                        ////revisionFilePath = Server.MapPath(objDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                        filePath = Server.MapPath(HostingEnvironment.ApplicationVirtualPath == "/"
                                                   ? newObjDoc.FilePath
                                                   : newObjDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                        revisionFilePath = Server.MapPath(HostingEnvironment.ApplicationVirtualPath == "/"
                                                    ? objDoc.RevisionFilePath
                                                    : objDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                        if (Utility.ServiceIsAvailable(ServiceName))
                        {
                            watcherService.ExecuteCommand(128);
                        }

                        if (File.Exists(revisionFilePath))
                        {
                            File.Copy(revisionFilePath, filePath, true);
                        }

                        if (Utility.ServiceIsAvailable(ServiceName))
                        {
                            watcherService.ExecuteCommand(129);
                        }
                    }

                    this.documentService.Insert(newObjDoc);

                    if (objDoc.Name != txtName.Text.Trim())
                    {
                        if (Utility.ServiceIsAvailable(ServiceName))
                        {
                            watcherService.ExecuteCommand(128);
                        }

                        if (File.Exists(filePath))
                        {
                            File.Move(filePath, filePath.Replace(oldName, txtName.Text.Trim()));
                        }

                        if (Utility.ServiceIsAvailable(ServiceName))
                        {
                            watcherService.ExecuteCommand(129);
                        }

                        var listDocRename = this.documentService.GetSpecificDocument(oldName, objDoc.DirName);
                        foreach (var document in listDocRename)
                        {
                            document.Name = txtName.Text.Trim();
                            document.FileNameOriginal = txtName.Text.Trim();
                            if (!string.IsNullOrEmpty(document.RevisionName))
                            {
                                document.RevisionFileName = document.RevisionName + "_" + txtName.Text.Trim();
                            }
                            else
                            {
                                document.RevisionFileName = txtName.Text.Trim();
                            }

                            document.FilePath = document.FilePath.Replace(oldName, txtName.Text.Trim());

                            this.documentService.Update(document);
                        }
                    }
                }
                else
                {
                    // Allway check revision file is exist when update
                    ////var filePath = Server.MapPath(objDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                    ////var revisionFilePath = Server.MapPath(objDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                    var filePath = Server.MapPath(HostingEnvironment.ApplicationVirtualPath == "/" ? objDoc.FilePath : objDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                    var revisionFilePath = Server.MapPath(HostingEnvironment.ApplicationVirtualPath == "/" ? objDoc.RevisionFilePath : objDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                    if (File.Exists(filePath) && !File.Exists(revisionFilePath))
                    {
                        File.Copy(filePath, revisionFilePath, true);
                    }
                    // End check

                    if (objDoc.Name != txtName.Text.Trim())
                    {
                        if (Utilities.Utility.ServiceIsAvailable(ServiceName))
                        {
                            watcherService.ExecuteCommand(128);
                        }

                        if (File.Exists(filePath))
                        {
                            File.Move(filePath, filePath.Replace(oldName, txtName.Text.Trim()));
                        }

                        if (Utilities.Utility.ServiceIsAvailable(ServiceName))
                        {
                            watcherService.ExecuteCommand(129);
                        }

                        var listDocRename = this.documentService.GetSpecificDocument(oldName, objDoc.DirName);
                        foreach (var document in listDocRename)
                        {
                            document.Name = txtName.Text.Trim();
                            document.FileNameOriginal = txtName.Text.Trim();
                            if (!string.IsNullOrEmpty(document.RevisionName))
                            {
                                document.RevisionFileName = document.RevisionName + "_" + txtName.Text.Trim();
                            }
                            else
                            {
                                document.RevisionFileName = txtName.Text.Trim();
                            }

                            document.FilePath = document.FilePath.Replace(oldName, txtName.Text.Trim());

                            if (document.ID == objDoc.ID)
                            {
                                document.DocumentNumber = txtDocumentNumber.Text;
                                document.Title = txtTitle.Text;
                                document.RevisionID = Convert.ToInt32(ddlRevision.SelectedValue);
                                document.RevisionName = ddlRevision.SelectedItem.Text;

                                document.DocumentTypeName = ddlDocumentType.SelectedItem.Text;
                                document.StatusName = ddlStatus.SelectedItem.Text;
                                document.DisciplineName = ddlDiscipline.SelectedItem.Text;
                                document.ReceivedFromName = ddlReceivedFrom.SelectedItem.Text;

                                document.DocumentTypeID = Convert.ToInt32(ddlDocumentType.SelectedValue);
                                document.StatusID = Convert.ToInt32(ddlStatus.SelectedValue);
                                document.DisciplineID = Convert.ToInt32(ddlDiscipline.SelectedValue);
                                document.ReceivedFromID = Convert.ToInt32(ddlReceivedFrom.SelectedValue);
                                document.ReceivedDate = txtReceivedDate.SelectedDate;
                                document.Remark = txtRemark.Text.Trim();
                                document.Well = txtWell.Text.Trim();
                                document.TransmittalNumber = txtTransmittalNumber.Text.Trim();

                                document.LastUpdatedBy = UserSession.Current.User.Id;
                                document.LastUpdatedDate = DateTime.Now;
                            }

                            this.documentService.Update(document);
                        }
                    }
                    else
                    {
                        objDoc.Name = txtName.Text.Trim();
                        objDoc.DocumentNumber = txtDocumentNumber.Text.Trim();
                        objDoc.Title = txtTitle.Text.Trim();
                        objDoc.RevisionID = Convert.ToInt32(ddlRevision.SelectedValue);

                        objDoc.RevisionName = ddlRevision.Text;
                        objDoc.DocumentTypeName = ddlDocumentType.SelectedItem.Text;
                        objDoc.StatusName = ddlStatus.SelectedItem.Text;
                        objDoc.DisciplineName = ddlDiscipline.SelectedItem.Text;
                        objDoc.ReceivedFromName = ddlReceivedFrom.SelectedItem.Text;

                        objDoc.StatusID = Convert.ToInt32(ddlStatus.SelectedValue);
                        objDoc.DisciplineID = Convert.ToInt32(ddlDiscipline.SelectedValue);
                        objDoc.DocumentTypeID = Convert.ToInt32(ddlDocumentType.SelectedValue);
                        objDoc.ReceivedFromID = Convert.ToInt32(ddlReceivedFrom.SelectedValue);
                        objDoc.ReceivedDate = txtReceivedDate.SelectedDate;
                        objDoc.Remark = txtRemark.Text.Trim();
                        objDoc.Well = txtWell.Text.Trim();
                        objDoc.TransmittalNumber = txtTransmittalNumber.Text.Trim();
                        if (!string.IsNullOrEmpty(objDoc.RevisionName))
                        {
                            objDoc.RevisionFileName = objDoc.RevisionName + "_" + objDoc.Name;
                        }
                        else
                        {
                            objDoc.RevisionFileName = objDoc.Name;
                        }

                        objDoc.LastUpdatedBy = UserSession.Current.User.Id;
                        objDoc.LastUpdatedDate = DateTime.Now;

                        this.documentService.Update(objDoc);
                    }
                }
            }
        }

        protected void ckbEnableFilter_OnCheckedChanged(object sender, EventArgs e)
        {
            this.grdDocument.AllowFilteringByColumn = ((CheckBox)sender).Checked;
            this.grdDocument.Rebind();
        }

        protected void radTreeFolder_OnNodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            //  e.Node.ImageUrl = "Images/folderdir16.png";

            ////if (this.AdminGroup.Contains(UserSession.Current.RoleId))
            ////{
            ////    e.Node.Category = "FullPermission";
            ////}
            ////else
            ////{
            ////    var groupPermission = this.groupDataPermissionService.GetByRoleId(UserSession.Current.RoleId, this.lblCategoryId.Value, e.Node.Value);
            ////    if (groupPermission != null && groupPermission.IsFullPermission.GetValueOrDefault())
            ////    {
            ////        e.Node.Category = "FullPermission";
            ////    }
            ////    else
            ////    {
            ////        var userPermission = this.userDataPermissionService.GetByUserId(
            ////            UserSession.Current.User.Id,
            ////            Convert.ToInt32(this.lblCategoryId.Value),
            ////            Convert.ToInt32(e.Node.Value));
            ////        if (userPermission != null && userPermission.IsFullPermission.GetValueOrDefault())
            ////        {
            ////            e.Node.Category = "FullPermission";
            ////        }
            ////        else
            ////        {
            ////            e.Node.Category = "Read";
            ////        }
            ////    }
            ////}
        }
        private List<int> GetAllChildren(int parent, List<Folder> folderList, ref List<int> childNodes)
        {
            var childFolderList = folderList.Where(t => t.ParentID == parent).ToList();
            if (childFolderList.Any())
            {
                childNodes.AddRange(childFolderList.Select(t => t.ID));
                foreach (var childFolder in childFolderList)
                {
                    this.GetAllChildren(childFolder.ID, folderList, ref childNodes);
                }
            }

            return childNodes.Distinct().ToList();
        }

        private List<int> GetAllChildren(int parent, ref List<int> childNodes)
        {
            var childFolderList = this.folderService.GetAllByParentId(parent).ToList();
            if (childFolderList.Any())
            {
                childNodes.AddRange(childFolderList.Select(t => t.ID));
                foreach (var childFolder in childFolderList)
                {
                    this.GetAllChildren(childFolder.ID, ref childNodes);
                }
            }

            return childNodes.Distinct().ToList();
        }

        private void UpdatePropertisFolder(int categori, List<int> ListNodeCurrent)
        {
            //var listFolder = this.folderService.GetAllByCategory(categori).OrderBy
            //    (t=> t.ID).ToList();
            //var listFolder= listFolder
            foreach (var folderItem in ListNodeCurrent.OrderByDescending(t => t))
            {
                var objFolder = this.folderService.GetById(folderItem);
                // List<int> listTemp = new List<int>();
                var subfolderList = this.folderService.GetAllByParentId(folderItem);
                objFolder.NumberOfSubfolder = subfolderList.Count + subfolderList.Sum(t => t.NumberOfSubfolder);
                ////subfolderList.Add(objFolder);
                var docList = this.documentService.GetAllByFolder(objFolder.ID);
                var folderSize = docList.Sum(t => t.FileSize) + subfolderList.Sum(t => t.FolderSize);
                objFolder.NumberOfDocument = docList.Count + subfolderList.Sum(t => t.NumberOfDocument);
                objFolder.FolderSize = folderSize;
                this.folderService.Update(objFolder);
            }
        }
        /// <summary>
        /// The load action permission.
        /// </summary>
        /// <param name="folderId">
        /// The folder id.
        /// </param>
        private void LoadActionPermission(string folderId)
        {
            //    if (this.AdminGroup.Contains(UserSession.Current.RoleId))
            //    {
            //        this.CustomerMenu.Visible = true;
            //        this.radMenuFolder.Visible = true;
            //    }
            //    else
            //    {
            //        this.CustomerMenu.Items[0].Visible = false;
            //        this.CustomerMenu.Items[1].Visible = false;
            //        this.grdDocument.MasterTableView.GetColumn("EditColumn").Display = false;
            //        this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Display = false;
            //        var groupPermission = this.folderPermisstionService.GetByRoleIdList(UserSession.Current.RoleId, Convert.ToInt32( this.lblCategoryId.Value), Convert.ToInt32( folderId));
            //        if (groupPermission != null)
            //        {
            //            if (groupPermission.ReadFile.GetValueOrDefault())
            //            {
            //                this.grdDocument.MasterTableView.GetColumn("EditColumn").Display = false;
            //                this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Display = false;
            //            }
            //            if (groupPermission.ModifyFile.GetValueOrDefault())
            //            {
            //                this.CustomerMenu.Items[0].Visible = true;
            //                this.CustomerMenu.Items[1].Visible = true;
            //                this.grdDocument.MasterTableView.GetColumn("EditColumn").Display = true;
            //            }
            //            if (groupPermission.DeleteFile.GetValueOrDefault())
            //            {
            //                this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Display = true;
            //            }

            //        }
            //        else
            //        {
            //            this.CustomerMenu.Items[0].Visible = false;
            //            this.CustomerMenu.Items[1].Visible = false;
            //            this.grdDocument.MasterTableView.GetColumn("EditColumn").Display = false;
            //            this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Display = false;
            //            var userPermission = this.userDataPermissionService.GetByUserId(
            //                UserSession.Current.User.Id,
            //                Convert.ToInt32(this.lblCategoryId.Value),
            //                Convert.ToInt32(folderId));
            //            if (userPermission != null)
            //            {
            //                if (userPermission.ReadFile.GetValueOrDefault())
            //                {

            //                    this.grdDocument.MasterTableView.GetColumn("EditColumn").Display = false;
            //                    this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Display = false;
            //                }
            //                if (userPermission.ModifyFile.GetValueOrDefault())
            //                {
            //                    this.CustomerMenu.Items[0].Visible = true;
            //                    this.CustomerMenu.Items[1].Visible = true;
            //                    this.grdDocument.MasterTableView.GetColumn("EditColumn").Display = true;
            //                }
            //                if (userPermission.DeleteFile.GetValueOrDefault())
            //                {
            //                    this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Display = true;
            //                }
            //            }
            //        }
            //    }
        }
        //SortNodes is a recursive method enumerating and sorting all node levels 
        private void SortNodes(RadTreeNodeCollection collection)
        {
            Sort(collection);
            foreach (RadTreeNode node in collection)
            {
                if (node.Nodes.Count > 0)
                {
                    SortNodes(node.Nodes);
                }
            }
        }

        //The Sort method is called for each node level sorting the child nodes 
        public void Sort(RadTreeNodeCollection collection)
        {
            RadTreeNode[] nodes = new RadTreeNode[collection.Count];
            collection.CopyTo(nodes, 0);
            Array.Sort(nodes, new TreeNodeComparer());
            collection.Clear();
            collection.AddRange(nodes);
        }

        private void StartNodeInEditMode(string nodeValue)
        {
            //find the node by its Value and edit it when page loads
            string js = "Sys.Application.add_load(editNode); function editNode(){ ";
            js += "var tree = $find(\"" + radTreeFolder.ClientID + "\");";
            js += "var node = tree.findNodeByValue('" + nodeValue + "');";
            js += "if (node) node.startEdit();";
            js += "Sys.Application.remove_load(editNode);};";

            RadScriptManager.RegisterStartupScript(Page, Page.GetType(), "nodeEdit", js, true);
        }

        /// <summary>
        /// The restore expand state tree view.
        /// </summary>
        private void RestoreExpandStateTreeView()
        {
            // Restore expand state of tree folder
            HttpCookie cookie = Request.Cookies["expandedNodes"];
            if (cookie != null)
            {
                var expandedNodeValues = cookie.Value.Split('*');
                foreach (var nodeValue in expandedNodeValues)
                {
                    RadTreeNode expandedNode = this.radTreeFolder.FindNodeByValue(HttpUtility.UrlDecode(nodeValue));
                    if (expandedNode != null)
                    {
                        expandedNode.Expanded = true;
                    }
                }
            }
        }

        /// <summary>
        /// The custom folder tree.
        /// </summary>
        /// <param name="radTreeView">
        /// The rad tree view.
        /// </param>
        private void CustomFolderTree(RadTreeNode radTreeView)
        {
            foreach (var node in radTreeView.Nodes)
            {
                var nodetemp = (RadTreeNode)node;
                if (nodetemp.Nodes.Count > 0)
                {
                    this.CustomFolderTree(nodetemp);
                }

                nodetemp.ImageUrl = "Images/folderdir16.png";
            }
        }

        private void LoadListPanel()
        {
            var listId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("ListID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), listId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "LIST" });

                this.radPbList.DataSource = permissions;
                this.radPbList.DataFieldParentID = "ParentId";
                this.radPbList.DataFieldID = "Id";
                this.radPbList.DataValueField = "Id";
                this.radPbList.DataTextField = "MenuName";
                this.radPbList.DataBind();
                this.radPbList.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbList.Items[0].Items)
                {
                    item.ImageUrl = @"Images/listmenu.png";
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                }
            }
        }

        private void LoadSystemPanel()
        {
            var systemId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("SystemID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), systemId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "SYSTEM" });

                this.radPbSystem.DataSource = permissions;
                this.radPbSystem.DataFieldParentID = "ParentId";
                this.radPbSystem.DataFieldID = "Id";
                this.radPbSystem.DataValueField = "Id";
                this.radPbSystem.DataTextField = "MenuName";
                this.radPbSystem.DataBind();
                this.radPbSystem.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbSystem.Items[0].Items)
                {
                    item.ImageUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Icon;
                    if (item.Text == "Users" || item.Text == "Groups" || item.Text == "Menu Permission")
                    {
                        item.NavigateUrl = "../../" + permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                    }
                    else
                    {
                        item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;

                    }
                }
            }
        }

        private void CreateValidation(string formular, ValidationCollection objValidations, int startRow, int endRow, int startColumn, int endColumn)
        {
            // Create a new validation to the validations list.
            Validation validation = objValidations[objValidations.Add()];

            // Set the validation type.
            validation.Type = Aspose.Cells.ValidationType.List;

            // Set the operator.
            validation.Operator = OperatorType.None;

            // Set the in cell drop down.
            validation.InCellDropDown = true;

            // Set the formula1.
            validation.Formula1 = "=" + formular;

            // Enable it to show error.
            validation.ShowError = true;

            // Set the alert type severity level.
            validation.AlertStyle = ValidationAlertType.Stop;

            // Set the error title.
            validation.ErrorTitle = "Error";

            // Set the error message.
            switch (formular)
            {
                case "RevisionList":
                    validation.ErrorMessage = "Please select a Revision from the list";
                    break;
                case "StatusList":
                    validation.ErrorMessage = "Please select a Status from the list";
                    break;
                case "DisciplineList":
                    validation.ErrorMessage = "Please select a Discipline from the list";
                    break;
                case "DocumentTypeList":
                    validation.ErrorMessage = "Please select a Document type from the list";
                    break;
                case "ReceivedFromList":
                    validation.ErrorMessage = "Please select a Received from from the list";
                    break;
            }

            // Specify the validation area.
            CellArea area;
            area.StartRow = startRow;
            area.EndRow = endRow;
            area.StartColumn = startColumn;
            area.EndColumn = endColumn;

            // Add the validation area.
            validation.AreaList.Add(area);

            ////return validation;
        }
        private void Download_File(string FilePath)
        {
            //Response.ContentType = ContentType;
            //Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(FilePath));
            //Response.WriteFile(FilePath);
            //Response.End();
            Response.Redirect("DownloadFile.ashx?type=1&filepath=" + FilePath);
        }
        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                FileStream fs = new FileStream(strFileName, FileMode.Open);
                int streamLength = Convert.ToInt32(fs.Length);
                byte[] data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        public class MyTemplateNode : ITemplate
        {
            private ResourceManager _resources;
            private string _culture;
            private string textNode;

            public MyTemplateNode(RadTreeNode node)
            {
                textNode = node.Text;
            }


            public void InstantiateIn(System.Web.UI.Control container)
            {

                Label label1 = new Label();
                label1.ID = "ItemLabel";
                label1.Text = textNode;
                label1.Font.Size = 10;
                label1.Font.Bold = true;
                label1.DataBinding += new EventHandler(label1_DataBinding);
                container.Controls.Add(label1);

                CustomValidator cv = new CustomValidator();
                cv.ID = "CustomValidator1";

                cv.ErrorMessage = "name already taken";
                cv.ServerValidate += Validate;

                container.Controls.Add(cv);
            }

            private void Validate(object source, ServerValidateEventArgs args)
            {
                bool isUsed = false;

                var node = (RadTreeNode)((Label)source).Parent;
                RadTreeView tv = node.TreeView;

                for (int i = 0; i < tv.GetAllNodes().Count; i++)
                {
                    if (node == editedNode && tv.GetAllNodes()[i].Text == node.Text && tv.GetAllNodes()[i] != node)
                    {
                        isUsed = true;
                        break;
                    }
                }
                args.IsValid = !isUsed;
            }

            private void label1_DataBinding(object sender, EventArgs e)
            {
                Label target = (Label)sender;
                RadTreeNode node = (RadTreeNode)target.BindingContainer;
                string nodeText = (string)DataBinder.Eval(node, node.Text);
                target.Text = nodeText;
            }
        }

        protected void ckbEnableFilter_CheckedChange(object sender, EventArgs e)
        {
            this.grdDocument.AllowFilteringByColumn = ((CheckBox)sender).Checked;
            this.grdDocument.Rebind();
        }

        protected void txtSearch_Search(object sender, SearchBoxEventArgs e)
        {
            var list = (List<int>)Session["ListIdFolder"];
            LoadDocuments(true, false, list, false);
        }

        private void ChangeTooltip(RadTreeNode st, List<FolderPermission> folderPermission)
        {
            this.radMenuFolder.Visible = true;
            this.CustomerMenu.Visible = true;
            RadButton radButton = (RadButton)this.radMenuFolder.Items[1].FindControl("btrename");
            if (radButton != null)
            {
                radButton.ToolTip = "Rename Folder '" + st.Text + "'";
            }
            RadButton radButton1 = (RadButton)this.radMenuFolder.Items[1].FindControl("btDelete");
            if (radButton1 != null)
            {
                radButton1.ToolTip = "Delete Folder '" + st.Text + "'";
            }
            RadButton radButton2 = (RadButton)this.radMenuFolder.Items[1].FindControl("btpermission");
            if (radButton2 != null)
            {
                radButton2.ToolTip = "Change Permission '" + st.Text + "'";
            }
            RadButton radButton3 = (RadButton)this.radMenuFolder.Items[1].FindControl("btProperties");
            if (radButton3 != null)
            {
                radButton3.ToolTip = "Properties Folder '" + st.Text + "'";
            }
            RadButton radButton4 = (RadButton)this.radMenuFolder.Items[1].FindControl("btmove");
            if (radButton4 != null)
            {
                radButton4.ToolTip = "Move folder '" + st.Text + "'";
            }
            RadButton radButton5 = (RadButton)this.radMenuFolder.Items[1].FindControl("btDownloadfolder");
            if (radButton5 != null)
            {
                radButton5.ToolTip = "Download folder '" + st.Text + "'";
            }
            //check function btDownloadfolder

            if (!this.AdminGroup.Contains(UserSession.Current.RoleId))
            {
                RadButton radButtonbtnAddFolder = (RadButton)this.radMenuFolder.Items[1].FindControl("btnAddFolder");
                //  RadToolBarButton toolbar = (RadToolBarButton)this.CustomerMenu.FindItemByValue("3");
                RadToolBarButton toolbar5 = (RadToolBarButton)this.CustomerMenu.FindItemByValue("5");

                if (!folderPermission.Where(t => t.Folder_CreateSubFolder.GetValueOrDefault()).Any())
                {
                    this.CustomerMenu.Items[0].Visible = false;
                    radButtonbtnAddFolder.Visible = false;
                    toolbar5.Visible = false;
                    //  toolbar.Visible = false;
                }

                if (!folderPermission.Where(t => t.Folder_Delete.GetValueOrDefault()).Any())
                { radButton1.Visible = false; }

                if (!folderPermission.Where(t => t.Folder_ChangePermission.GetValueOrDefault()).Any())
                { radButton2.Visible = false; }
                if (!folderPermission.Where(t => t.Folder_Write.GetValueOrDefault()).Any())
                {
                    radButton.Visible = false; radButton4.Visible = false; radButtonbtnAddFolder.Visible = false;
                    toolbar5.Visible = false;
                }
                if (!folderPermission.Where(t => t.Folder_Read.GetValueOrDefault()).Any())
                {
                    radButton3.Visible = false; radButton5.Visible = false;
                    this.CustomerMenu.Items[0].Visible = false;
                    this.CustomerMenu.Items[2].Visible = false;
                    this.grdDocument.MasterTableView.GetColumn("EditColumn").Display = false;
                    this.grdDocument.MasterTableView.GetColumn("DownloadColumn").Display = false;
                    this.grdDocument.MasterTableView.GetColumn("Viewfile").Display = false;
                }
            }
        }

        protected void btnAddFolder_Click(object sender, EventArgs e)
        {
            this.radMenuFolder.Visible = false;
            this.CustomerMenu.Visible = false;
            RadTreeNode clickedNode = this.radTreeFolder.SelectedNode;
            Session.Add("FolderMenuAction", "New");
            //// set node's value so we can find it in startNodeInEditMode
            //newFolder.Value = newFolder.GetFullPath("/");
            RadTreeNode newFolder = new RadTreeNode(string.Format("New Folder {0}", clickedNode.Nodes.Count + 1));
            newFolder.Selected = true;
            newFolder.ImageUrl = clickedNode.ImageUrl;
            clickedNode.Nodes.Add(newFolder);
            clickedNode.Expanded = true;
            //update the number in the brackets
            if (Regex.IsMatch(clickedNode.Text, UnreadPattern))
                clickedNode.Text = Regex.Replace(clickedNode.Text, UnreadPattern, "(" + clickedNode.Nodes.Count.ToString() + ")");
            else
                clickedNode.Text += string.Format(" ({0})", clickedNode.Nodes.Count);
            clickedNode.Font.Bold = true;
            //set node's value so we can find it in startNodeInEditMode
            newFolder.Value = newFolder.GetFullPath("/");
            StartNodeInEditMode(newFolder.Value);
        }

        protected void btrename_Click(object sender, EventArgs e)
        {
            RadTreeNode clickedNode = this.radTreeFolder.SelectedNode;
            Session.Add("FolderMenuAction", "Rename");
            this.StartNodeInEditMode(clickedNode.Value);
        }

        protected void btDelete_Click(object sender, EventArgs e)
        {
            RadTreeNode clickedNode = this.radTreeFolder.SelectedNode;
            Session.Add("FolderMenuAction", "Delete");
            var categoryId = Convert.ToInt32(this.lblCategoryId.Value);
            var folDelete = this.folderService.GetById(Convert.ToInt32(clickedNode.Value));
            var folderIDs = new List<int>();
            folderIDs = this.GetAllChildren(Convert.ToInt32(clickedNode.Value), ref folderIDs);
            
            if (folDelete != null)
            {
                // Delete main folder
                var dataPermission = this.folderPermisstionService.GetAllByFolder(folDelete.ID);
                this.folderPermisstionService.DeletePermission(dataPermission);
                folDelete.Isdelete = true;
                folDelete.IsBin = true;
                folDelete.LastUpdatedBy = UserSession.Current.User.Id;
                folDelete.LastUpdatedDate = DateTime.Now;
                this.folderService.Update(folDelete);

                // Delete all child Folder
                foreach (var childNode in folderIDs)
                {
                    var childFolder = this.folderService.GetById(childNode);
                    if (childFolder != null)
                    {
                        dataPermission = this.folderPermisstionService.GetAllByFolder(childNode);
                        this.folderPermisstionService.DeletePermission(dataPermission);
                        childFolder.Isdelete = true;
                        childFolder.IsBin = false;
                        childFolder.LastUpdatedBy = UserSession.Current.User.Id;
                        childFolder.LastUpdatedDate = DateTime.Now;
                        this.folderService.Update(childFolder);
                    }
                }
                folderIDs.Add(Convert.ToInt32(clickedNode.Value));
                var docList = this.documentService.GetAllByFolder(folderIDs);
                foreach (var document in docList)
                {
                    document.IsLeaf = false;
                    document.IsDelete = true;
                    this.documentService.Update(document);
                }

                ////if (Directory.Exists(Server.MapPath(folDelete.DirName)))
                ////{
                ////    Directory.Delete(Server.MapPath(folDelete.DirName), true);
                ////}    
            }

            // this.LoadTreeFolder(categoryId);
            // 
            List<int> ListNodeExpanded = new List<int>();
            GetListNodeExpanded(ref ListNodeExpanded, clickedNode.ParentNode);
            clickedNode.Remove();
            UpdatePropertisFolder(categoryId, ListNodeExpanded);
            this.grdDocument.CurrentPageIndex = 0;
            this.grdDocument.Rebind();
        }

        protected void grdDocument_PreRender(object sender, EventArgs e)
        {
            foreach (GridDataItem item in grdDocument.MasterTableView.Items)
            {
                if (item["IsFolder"].Text.ToLower() == "true")
                {
                    item.SelectableMode = GridItemSelectableMode.None;
                }
                else
                {
                    item.SelectableMode = GridItemSelectableMode.ServerAndClientSide;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
                {
                    var id = Request.QueryString["docId"].ToString();
                    if (id == item.GetDataKeyValue("ID").ToString())
                    {
                        item.Selected = true;
                    }
                }
            }
        }

        protected void radTreeFolder_ContextMenuItemClick(object sender, RadTreeViewContextMenuEventArgs e)
        {

        }

        protected void btProperties_Click(object sender, EventArgs e)
        {
            var folderObj = this.folderService.GetById(Convert.ToInt32(this.lblFolderId.Value));
            if (folderObj != null)
            {
                var creater = this.userService.GetByID(folderObj.CreatedBy.GetValueOrDefault());
                var uploader = this.userService.GetByID(folderObj.LastUpdatedBy.GetValueOrDefault());
                this.lblDocumentCount.Text = folderObj.NumberOfDocument.GetValueOrDefault().ToString();
                this.lblDiskUsage.Text = folderObj.FolderSize.GetValueOrDefault().ToString();
                this.lblCreatedBy.Text = creater != null ? creater.UserNameFullName : "-";
                this.lblCreatedTime.Text = folderObj.CreatedDate.ToString();
                this.lblUpdatedBy.Text = uploader != null ? uploader.UserNameFullName : "-";
                this.lblUpdatedTime.Text = folderObj.LastUpdatedDate.ToString();
                this.lblParentFolder.Text = folderObj.ParentName;
            }
        }


        protected void btnFileDelete_Click(object sender, EventArgs e)
        {
            if (this.lblDocId.Value != "")
            {
                var docId = Convert.ToInt32(this.lblDocId.Value);

                var docObj = this.documentService.GetById(docId);
                if (docObj != null)
                {
                    docObj.IsLeaf = false;
                    docObj.IsDelete = true;
                    docObj.LastUpdatedBy = UserSession.Current.User.Id;
                    docObj.LastUpdatedDate = DateTime.Now;
                    this.documentService.Update(docObj);

                    List<int> ListNodeExpanded = new List<int>();
                    GetListNodeExpanded(ref ListNodeExpanded, this.radTreeFolder.SelectedNode);
                    UpdatePropertisFolder(docObj.CategoryID.GetValueOrDefault(), ListNodeExpanded);
                    grdDocument.Rebind();
                }
            }
            else
            {
                lblNotification.Text = "Please select file";
                RadNotification1.Title = "Warning";
                RadNotification1.Show();
            }
        }

        protected void btnFileCheckout_Click(object sender, EventArgs e)
        {
            var docObj = this.documentService.GetById(Convert.ToInt32(this.lblDocId.Value));
            if (docObj != null)
            {
                docObj.IsCheckOut = true;
                docObj.CurrentCheckoutByUser = UserSession.Current.User.Id;
                docObj.LastUpdatedBy = UserSession.Current.User.Id;
                docObj.LastUpdatedDate = DateTime.Now;
                this.documentService.Update(docObj);
                this.grdDocument.Rebind();
            }
        }

        protected void btnGotoFolder_Click(object sender, EventArgs e)
        {
            if (this.isFolder.Value != "True")
            {
                var docObj = this.documentService.GetById(Convert.ToInt32(this.lblDocId.Value));
                var selectedFolder = this.folderService.GetById(docObj.FolderID.GetValueOrDefault());

                ReLoadTreeFolder(docObj.CategoryID.GetValueOrDefault(), selectedFolder.ParentID.GetValueOrDefault(), selectedFolder.ID);

                //    var selectedFolder = this.radTreeFolder.FindNodeByValue(docObj.FolderID.GetValueOrDefault());
                //    if (selectedFolder != null)
                //    {
                //        this.LoadActionPermission(selectedFolder.Value);
                //        var categoryId = Convert.ToInt32(this.lblCategoryId.Value);
                //        var folderPermission = this._FolderPermisstionService.GetFolderByRoleIdList(Convert.ToInt32(Request.QueryString["doctype"]), GroupFunctionList)
                //                                   .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault());

                //        folderPermission = folderPermission.Union(this._FolderPermisstionService.GetByUserId(UserSession.Current.User.Id, Convert.ToInt32(Request.QueryString["doctype"]))
                //                                                         .Where(t => t.FolderId != null && t.Folder_Read.GetValueOrDefault()))
                //                                                         .ToList();
                //        var folderPermissionID = folderPermission.Where(t => t.FolderId == folderId).ToList();
                //        var listFolderPermission = this.AdminGroup.Contains(UserSession.Current.RoleId)
                //                        ? this.folderService.GetAllByCategory(Convert.ToInt32(Request.QueryString["doctype"]))
                //                        : this.folderService.GetSpecificFolderStatic(folderPermission.Select(t => t.FolderId.GetValueOrDefault())
                //                                                        .Distinct().ToList());
                //        selectedFolder.Selected = true;
                //        var folderlist = listFolderPermission.Where(t => t.ParentID.GetValueOrDefault() == Convert.ToInt32(folderId)).OrderBy(t => t.Name).ToList();
                //        if (folderlist.Any())
                //        {
                //            selectedFolder.Nodes.Clear();
                //            foreach (var folderitem in folderlist)
                //            {
                //                var nodechild = new RadTreeNode();
                //                nodechild.Text = folderitem.Name;
                //                nodechild.Value = folderitem.ID.ToString();
                //                nodechild.ImageUrl = "~/Images/folderdir16.png";
                //                nodechild.Target = selectedFolder.Target;
                //                if (listFolderPermission.Count(t => t.ParentID.GetValueOrDefault() == folderitem.ID) > 0)
                //                {
                //                    nodechild.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                //                }
                //                selectedFolder.Nodes.Add(nodechild);
                //            }
                //        }
                //        selectedFolder.Expanded = true;

                //        List<int> listTemp = new List<int>();
                //        var listFolder = this.GetAllChildren(Convert.ToInt32(selectedFolder.Value), listFolderPermission, ref listTemp);
                //        listFolder.Add(Convert.ToInt32(selectedFolder.Value));
                //        Session["ListIdFolder"] = listFolder;
                //        this.grdDocument.CurrentPageIndex = 0;
                //        this.LoadDocuments(true, false, listFolder);
                //        ChangeTooltip(selectedFolder, folderPermissionID);
                //    }
                //}

            }
        }

        protected void btDownloadfolder_Click(object sender, EventArgs e)
        {
            RadTreeNode clickedNode = this.radTreeFolder.SelectedNode;
            var selectfolder = Convert.ToInt32(clickedNode.Value);

            var folderObj = this.folderService.GetById(selectfolder);
            if (folderObj != null)
            {
                var ListChildFodler = (List<int>)Session["ListIdFolder"];
                var docList = this.documentService.GetAllByFolder(ListChildFodler);
                var folderSize = docList.Sum(t => t.FileSize);
                if (folderSize <= 2147483648)
                {
                    var filename = DateTime.Now.ToBinary() + "_" + folderObj.Name + "_DocPack.zip";
                    var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + filename);

                    using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile())
                    {
                        zip.AlternateEncoding = System.Text.Encoding.Unicode;
                        zip.AddDirectory(Server.MapPath(folderObj.DirName));
                        zip.Save(serverTotalDocPackPath);
                    };
                    System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Guid.NewGuid().ToString(), "alert('Hello');", true);
                    //        //this.Download_File(serverTotalDocPackPath);
                    // Response.Redirect("DownloadFile.ashx?type=3&path=" + filename);
                    Response.ClearContent();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("Content-Disposition",
                                      "attachment; filename=" + folderObj.Name + ".zip;");
                    Response.TransmitFile(serverTotalDocPackPath);
                    Response.Flush();

                    System.IO.File.Delete(serverTotalDocPackPath);

                    System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Guid.NewGuid().ToString(), "CloseImageLoading();", true);
                    System.IO.File.Delete(serverTotalDocPackPath);
                    Response.End();


                }
            }

        }

        protected void btnAdvanceSearch_Click(object sender, EventArgs e)
        {
            var list = (List<int>)Session["ListIdFolder"];
            LoadDocuments(true, false, list, true);
        }
    }

    //The TreeNodeComparer class defines the sorting criteria 
    class TreeNodeComparer : IComparer
    {
        #region IComparer Members

        public int Compare(object x, object y)
        {
            RadTreeNode firstNode = (RadTreeNode)x;
            RadTreeNode secondNode = (RadTreeNode)y;

            return firstNode.Text.CompareTo(secondNode.Text);
        }

        #endregion
    }
}

