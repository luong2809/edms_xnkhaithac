﻿

namespace EDMs.Web
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.Services;
    using System.IO;
    using Business.Services;
    using Data.Entities;
    using Utilities;
    using Utilities.Sessions;
    public partial class UploadHandler : Page
    {
        /// private readonly GroupDataPermissionService groupDataPermissionService = new GroupDataPermissionService();
        // private readonly UserDataPermissionService userDataPermissionService = new UserDataPermissionService();
        private readonly FolderPermissionService _FolderPermisstionService = new FolderPermissionService();
        private readonly CategoryService categoryService = new CategoryService();
        private readonly FolderService folderService = new FolderService();
        private readonly RoleService roleService = new RoleService();
        private readonly UserService userService = new UserService();
        private readonly DocumentService documentService = new DocumentService();
        private List<string> Serverpath = new List<string>();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.Files.Count > 0)
            {
                try

                {
                    var FodlerId = Convert.ToInt32(Session["FolderId"]);
                    var CartegoryId = Convert.ToInt32(Session["categoryId"]);
                    var folderObj = this.folderService.GetById(FodlerId);
                    var ListNodeExpanded = (List<int>)Session["ListNodeExpanded"];
                    if (folderObj != null)
                    {
                        HttpFileCollection files = Request.Files;
                        for (int i = 0; i < files.Count; i++)
                        {
                            var stString = "";

                            if (!Directory.Exists(string.Format("{0}/{1}", Server.MapPath(folderObj.DirName), getPath(files[i].FileName))))
                            {
                                Directory.CreateDirectory(string.Format("{0}/{1}", Server.MapPath(folderObj.DirName), getPath(files[i].FileName)));
                                stString = getPath(files[i].FileName).Replace(@"/", "#");// Path.Combine(Server.MapPath(folderObj.DirName), getPath(files[i].FileName));
                                if (!Serverpath.Any(T => T.ToLower().Contains(stString.Split('#')[0].ToLower())))
                                {
                                    Serverpath.Add(stString.Split('#')[0]);
                                }
                            }
                            var pathfile = Path.Combine(Server.MapPath(folderObj.DirName), files[i].FileName);
                            files[i].SaveAs(string.Format("{0}/{1}", Server.MapPath(folderObj.DirName), files[i].FileName));
                            // Response.Write(files[i]);
                        }
                        foreach (var tem in Serverpath)
                        {
                            var temp = string.Format("{0}/{1}", Server.MapPath(folderObj.DirName), tem);


                            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(temp);

                            WalkDirectoryTree(di, folderObj, CartegoryId, ref ListNodeExpanded);
                        }
                        UpdatePropertisFolder(CartegoryId, ListNodeExpanded);
                        Response.Write("OK");
                    }

                    /// Session.Remove("FolderId");
                    //  Session.Remove("categoryId");
                    //  Session.Remove("ListNodeExpanded");

                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }


            }
        }
        private void UpdatePropertisFolder(int categori, List<int> ListNodeCurrent)
        {
            //var listFolder = this.folderService.GetAllByCategory(categori).OrderBy
            //    (t=> t.ID).ToList();
            //var listFolder= listFolder
            foreach (var folderItem in ListNodeCurrent.OrderByDescending(t => t))
            {
                var objFolder = this.folderService.GetById(folderItem);
                // List<int> listTemp = new List<int>();
                var subfolderList = this.folderService.GetAllByParentId(folderItem);
                objFolder.NumberOfSubfolder = subfolderList.Count + subfolderList.Sum(t => t.NumberOfSubfolder);
                ////subfolderList.Add(objFolder);
                var docList = this.documentService.GetAllByFolder(objFolder.ID);
                var folderSize = docList.Sum(t => t.FileSize) + subfolderList.Sum(t => t.FolderSize);
                objFolder.NumberOfDocument = docList.Count + subfolderList.Sum(t => t.NumberOfDocument);
                objFolder.FolderSize = folderSize;
                this.folderService.Update(objFolder);
            }
        }
        private string getPath(string p)
        {
            string path = p.Substring(0, p.LastIndexOf("/") + 1);

            return path;
        }
        private void WalkDirectoryTree(System.IO.DirectoryInfo root, Folder parentFol, int categoryId, ref List<int> ListNodeExpanded)
        {
            System.IO.FileInfo[] files = null;
            System.IO.DirectoryInfo[] subDirs = null;
            // Create folder
            try
            {
                // var parentFol = folderService.GetById(Convert.ToInt32());
                var folder = new Folder()
                {
                    Name = root.Name,
                    Description = root.Name,
                    CategoryID = categoryId,
                    ParentID = parentFol.ID,
                    DirName = parentFol.DirName + "/" + root.Name,
                    CreatedBy = UserSession.Current.User.Id,
                    CreatedDate = DateTime.Now
                };

                ///Directory.CreateDirectory(Serverpath + "/" + folder.DirName);

                var ownerGroupIds = this.roleService.GetAllOwner().Select(t => t.Id);
                var ownerUserIds = this.userService.GetAll().Where(t => ownerGroupIds.Contains(t.RoleId.GetValueOrDefault())).Select(t => t.Id);

                var parentFolderGroupPermission = this._FolderPermisstionService.GetAllByFolder(parentFol.ID);

                //var parentFolderUserPermission = this.userDataPermissionService.GetAllByFolder(parentFol.ID);

                var folId = this.folderService.Insert(folder);
                ListNodeExpanded.Add(folder.ID);
                // var targetFolder = Serverpath + "/" + folder.DirName;

                var groupPermission =
                      parentFolderGroupPermission.Select(
                          t =>
                          new FolderPermission()
                          {
                              CategoryId = t.CategoryId,
                              ObjectId = t.ObjectId,
                              ObjectIdName = t.ObjectIdName,
                              FolderId = folder.ID,
                              TypeID = t.TypeID,
                              TypeName = t.TypeName,
                              Folder_IsFullPermission = t.Folder_IsFullPermission,
                              Folder_ChangePermission = t.Folder_ChangePermission,
                              Folder_CreateSubFolder = t.Folder_CreateSubFolder,
                              Folder_Delete = t.Folder_Delete,
                              Folder_Read = t.Folder_Read,
                              Folder_Write = t.Folder_Write,
                              File_FullPermission = t.File_FullPermission,
                              File_ChangePermission = t.File_ChangePermission,
                              File_Create = t.File_Create,
                              File_Delete = t.File_Delete,
                              File_Read = t.File_Read,
                              File_Write = t.File_Write,
                              File_NoAccess = t.File_NoAccess,
                              CreatedDate = DateTime.Now,
                              CreatedBy = UserSession.Current.User.Id
                          }).ToList();
                this._FolderPermisstionService.AddGroupDataPermissions(groupPermission);
                // First, process all the files directly under this folder
                try
                {
                    files = root.GetFiles("*.*");
                }

                // than the application provides.
                catch (UnauthorizedAccessException e)
                {

                    // MessageBox.Show(e.Message);
                }

                catch (System.IO.DirectoryNotFoundException e)
                {
                    Console.WriteLine(e.Message);
                }

                if (files != null)
                {
                    foreach (System.IO.FileInfo fi in files)
                    {

                        var docFileName = fi.Name;
                        var docFileNameOrignal = fi.Name;

                        var objDoc = new Document();

                        var docObjLeaf = this.documentService.GetSpecificDocument(folder.ID, docFileNameOrignal);
                        if (docObjLeaf == null)
                        {

                            objDoc.FolderID = folder.ID;
                            objDoc.CreatedBy = UserSession.Current.User.Id;
                            objDoc.CreatedDate = DateTime.Now;
                            objDoc.IsLeaf = true;
                            objDoc.IsDelete = false;


                            var serverDocFileName = docFileName;

                            // Path file to save on server disc
                            //var saveFilePath = Path.Combine(targetFolder, serverDocFileName);
                            // var saveFileRevisionPath = Path.Combine(Server.MapPath(revisionPath), revisionServerFileName);

                            // Path file to download from server
                            var serverFilePath = folder.DirName + "/" + Utility.RemoveAllSpecialCharacter(serverDocFileName);
                            //  var revisionFilePath = serverRevisionFolder + revisionServerFileName;
                            var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);
                            objDoc.FileSize = fi.Length;
                            objDoc.RevisionFileName = docFileName;
                            objDoc.RevisionFilePath = serverFilePath;
                            objDoc.FilePath = serverFilePath;
                            objDoc.FileExtension = fileExt;
                            if (!Utility.FileIcon.ContainsKey(fileExt.ToLower()))
                            {
                                Utility.FileIcon.Add(fileExt.ToLower(), "images/otherfile.png");
                                //objDoc.FileExtensionIcon =  "images/otherfile.png";
                            }
                            objDoc.FileExtensionIcon = !string.IsNullOrEmpty(Utility.FileIcon[fileExt.ToLower()]) ? Utility.FileIcon[fileExt.ToLower()] : "images/otherfile.png";
                            objDoc.Name = docFileNameOrignal;
                            objDoc.CategoryID = folder.CategoryID;
                            objDoc.DirName = folder.DirName;

                            if (objDoc.ParentID == null)
                            {
                                objDoc.FileNameOriginal = docFileName;
                            }

                            this.documentService.Insert(objDoc);
                            File.Move(fi.FullName, Server.MapPath(serverFilePath));
                        }

                    }

                    // Now find all the subdirectories under this directory.
                    subDirs = root.GetDirectories();

                    foreach (System.IO.DirectoryInfo dirInfo in subDirs)
                    {
                        // Resursive call for each subdirectory.
                        WalkDirectoryTree(dirInfo, folder, categoryId, ref ListNodeExpanded);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        private void UpdatePropertisFolder(int categori)
        {
            var listFolder = this.folderService.GetAllByCategory(categori).OrderBy
                 (t => t.ID).ToList();

            foreach (var folderItem in listFolder)
            {
                List<int> listTemp = new List<int>();
                var subfolderList = this.GetAllChildren(folderItem.ID, listFolder, ref listTemp);
                var docList = this.documentService.GetAllByFolder(subfolderList);
                var folderSize = docList.Sum(t => t.FileSize);
                folderItem.NumberOfSubfolder = subfolderList.Count;
                folderItem.NumberOfDocument = docList.Count;
                folderItem.FolderSize = folderSize;
                this.folderService.Update(folderItem);
            }
        }
        private List<int> GetAllChildren(int parent, List<Folder> folderList, ref List<int> childNodes)
        {
            var childFolderList = folderList.Where(t => t.ParentID == parent).ToList();
            if (childFolderList.Any())
            {
                childNodes.AddRange(childFolderList.Select(t => t.ID));
                foreach (var childFolder in childFolderList)
                {
                    this.GetAllChildren(childFolder.ID, folderList, ref childNodes);
                }
            }

            return childNodes.Distinct().ToList();
        }
    }
}