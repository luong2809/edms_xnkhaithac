﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services;
using Aspose.Words;
using Aspose.Words.Saving;
using Aspose.Cells;
//using Aspose.Slides;
using Aspose.Imaging;
using Aspose.Slides.Export;
using iTextSharp.text.pdf;

namespace EDMs.Web
{

    public partial class Viewer : System.Web.UI.Page
    {
        protected string InputFormat;
        protected string ConvertHandlerUrl;
        private readonly DocumentService _AttachfileService = new DocumentService();
        protected void Page_Load(object sender, EventArgs e)
        {
            var indexFile = Convert.ToInt32(Request.QueryString["IndexId"]);
            var fileObj = this._AttachfileService.GetById(indexFile);
            var inputDocument = Server.MapPath(fileObj.FilePath);// exampleFileSelector.SelectedFile;
            var fileInfo = new FileInfo(inputDocument);

            var filePath = fileObj.FilePath;
            var Output = filePath.Substring(0, filePath.LastIndexOf('.'));
            List<string> ImageExtensions = new List<string> { "jpg", "jpe", "bmp", "gif", "png","svg" };
            List<string> videoExtensions = new List<string> {"AVI", "MP4", "DIVX", "WMV","MPG","M1V","MKV"};
            ViewPDF.Attributes["name"] = fileObj.Name;
            if (fileInfo.Extension.ToLower().Contains("pdf"))
            {
                this.divViewerPDF.Visible = true;
                this.divViewImage.Visible = false;
                this.divViewerVideo.Visible = false;
                ViewPDF.Attributes["src"] = fileObj.FilePath;
            }
            else if (fileInfo.Extension.ToLower().Contains("doc"))
            {
                this.divViewerPDF.Visible = true;
                this.divViewImage.Visible = false;
                this.divViewerVideo.Visible = false;
                if (!string.IsNullOrEmpty(fileObj.FilePathViewer))
                {
                    ViewPDF.Attributes["src"] = fileObj.FilePathViewer;
                }
                else
                {
                    Output += ".pdf";
                    Aspose.Words.Document doc = new Aspose.Words.Document(inputDocument);
                    Aspose.Words.Saving.PdfSaveOptions option = new Aspose.Words.Saving.PdfSaveOptions();
                    // var Output = FileNameNew;// "/DocumentLibrary/DocumentCache" + "/" + DateTime.Now.ToBinary() + ".pdf";
                    doc.Save(Server.MapPath(Output), option);
                    fileObj.FilePathViewer = Output;
                    this._AttachfileService.Update(fileObj);
                    ViewPDF.Attributes["src"] = Output;
                }

            }
            else if (fileInfo.Extension.ToLower().Contains("xls"))
            {
                this.divViewerPDF.Visible = true;
                this.divViewImage.Visible = false;
                this.divViewerVideo.Visible = false;
                if (!string.IsNullOrEmpty(fileObj.FilePathViewer))
                {
                    ViewPDF.Attributes["src"] = fileObj.FilePathViewer;
                }
                else
                {
                    Output += ".pdf";
                    Workbook workbook = new Workbook(inputDocument);
                    Aspose.Cells.PdfSaveOptions options = new Aspose.Cells.PdfSaveOptions(Aspose.Cells.SaveFormat.Pdf);
                    // var Output = FileNameNew;// "/DocumentLibrary/DocumentCache" + "/" + DateTime.Now.ToBinary() + ".pdf";
                    options.OnePagePerSheet = true;
                    // Save the document in PDF format
                    workbook.Save(Server.MapPath(Output), options);
                    fileObj.FilePathViewer = Output;
                    this._AttachfileService.Update(fileObj);
                    ViewPDF.Attributes["src"] = Output;
                }
            }
            else if (fileInfo.Extension.ToLower().Contains("ppt"))
            {
                this.divViewerPDF.Visible = true;
                this.divViewImage.Visible = false;
                this.divViewerVideo.Visible = false;
                if (!string.IsNullOrEmpty(fileObj.FilePathViewer))
                {
                    ViewPDF.Attributes["src"] = fileObj.FilePathViewer;
                }
                else
                {
                    Output += ".pdf";
                    Aspose.Slides.Presentation presentation = new Aspose.Slides.Presentation(inputDocument);

                    // Save the presentation to PDF with default options
                    presentation.Save(Server.MapPath(Output), Aspose.Slides.Export.SaveFormat.Pdf);
                    fileObj.FilePathViewer = Output;
                    this._AttachfileService.Update(fileObj);
                    ViewPDF.Attributes["src"] = Output;
                }
            }
            else if (fileInfo.Extension.ToLower().Contains("psd"))
            {
                this.divViewerPDF.Visible = true;
                this.divViewImage.Visible = false;
                this.divViewerVideo.Visible = false;
                if (!string.IsNullOrEmpty(fileObj.FilePathViewer))
                {
                    ViewPDF.Attributes["src"] = fileObj.FilePathViewer;
                }
                else
                {
                    Output += ".pdf";
                    using (Aspose.Imaging.Image image = Aspose.Imaging.Image.Load(inputDocument))
                    {
                        Aspose.Imaging.FileFormats.Psd.PsdImage psdImage = (Aspose.Imaging.FileFormats.Psd.PsdImage)image;

                        //PsdImage psdImage = (Aspose.Imaging.FileFormats.Psd.PsdImage)image;


                        Aspose.Imaging.FileFormats.Pdf.PdfDocumentInfo exportOptions = new Aspose.Imaging.FileFormats.Pdf.PdfDocumentInfo();

                        psdImage.Save(Server.MapPath(Output));
                    }
                    fileObj.FilePathViewer = Output;
                    this._AttachfileService.Update(fileObj);
                    ViewPDF.Attributes["src"] = Output;
                }

            }
            else if (fileInfo.Extension.ToLower().Contains("tif"))
            {
                this.divViewerPDF.Visible = true;
                this.divViewImage.Visible = false;
                this.divViewerVideo.Visible = false;
                if (!string.IsNullOrEmpty(fileObj.FilePathViewer))
                {
                    ViewPDF.Attributes["src"] = fileObj.FilePathViewer;
                }
                else
                {
                    Output += ".pdf";
                    using (var stream = new FileStream(Server.MapPath(Output), FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        iTextSharp.text.Document document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 0, 0, 0, 0);
                        var writer = PdfWriter.GetInstance(document, stream);
                        var bitmap = new System.Drawing.Bitmap(inputDocument);
                        var pages = bitmap.GetFrameCount(System.Drawing.Imaging.FrameDimension.Page);

                        document.Open();
                        iTextSharp.text.pdf.PdfContentByte cb = writer.DirectContent;
                        for (int i = 0; i < pages; ++i)
                        {
                            bitmap.SelectActiveFrame(System.Drawing.Imaging.FrameDimension.Page, i);
                            iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(bitmap, System.Drawing.Imaging.ImageFormat.Bmp);
                            // scale the image to fit in the page 
                            img.ScalePercent(72f / img.DpiX * 100);
                            img.SetAbsolutePosition(0, 0);
                            cb.AddImage(img);
                            document.NewPage();
                        }
                        document.Close();
                    }
                    fileObj.FilePathViewer = Output;
                    this._AttachfileService.Update(fileObj);
                    ViewPDF.Attributes["src"] = Output;
                }
            }
            else if(videoExtensions.Contains(fileObj.FileExtension.ToUpper()))
            { this.divViewerPDF.Visible = false;
                this.divViewImage.Visible = false;
                this.divViewerVideo.Visible = true;
                Output += ".mp4";
                if (fileObj.FileExtension.ToLower() == "mp4")
                {
                    this.playvideo.Attributes["src"] = fileObj.FilePath;
                }
                else if (!string.IsNullOrEmpty(fileObj.FilePathViewer))
                {
                    this.playvideo.Attributes["src"] = fileObj.FilePathViewer;
                }
                else if (fileObj.FileExtension.ToLower() == "avi")
                {
                    File.Copy(inputDocument,Server.MapPath( Output));
                    fileObj.FilePathViewer = Output;
                    this._AttachfileService.Update(fileObj);
                    this.playvideo.Attributes["src"] = Output;
                }
                else
                {
                    var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
                    ffMpeg.ConvertMedia(inputDocument, Server.MapPath( Output), NReco.VideoConverter.Format.mp4);
                    fileObj.FilePathViewer = Output;
                    this._AttachfileService.Update(fileObj);
                    this.playvideo.Attributes["src"] = Output;
                }
            }else if (ImageExtensions.Contains(fileObj.FileExtension.ToLower()) )
                {
                this.divViewerPDF.Visible = false;
                this.divViewImage.Visible = true;
                this.divViewerVideo.Visible = false;

                this.ImageControl.Src = fileObj.FilePath;
            }




        }
    }
}