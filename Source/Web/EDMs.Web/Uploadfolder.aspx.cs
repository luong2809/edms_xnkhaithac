﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EDMs.Web
{
    public partial class Uploadfolder : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.lbCategory.Text = this.Request.QueryString["categoryId"];
                this.lbFolder.Text = this.Request.QueryString["FolderId"];
                Session.Add("FolderId", this.Request.QueryString["FolderId"]);
                Session.Add("categoryId", this.Request.QueryString["categoryId"]);
                var ListNodeExpanded = (List<int>)Session["ListNodeExpanded"];
                ListNodeExpanded.Add(Convert.ToInt32(this.Request.QueryString["FolderId"]));
                Session.Add("ListNodeExpanded", ListNodeExpanded);

            }
        }
    }
}