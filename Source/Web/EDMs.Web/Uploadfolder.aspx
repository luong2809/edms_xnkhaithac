﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Uploadfolder.aspx.cs" Inherits="EDMs.Web.Uploadfolder" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>UploadFolder</title>
    <meta charset="utf-8" />
    <style>
        body {
            font-family: sans-serif;
        }

        fieldset {
            display: inline-block;
            width: 80%;
            min-height: 100px;
            background-image: url("Images/uploadfolder.jpg");
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default"></telerik:RadAjaxLoadingPanel>
        <asp:Panel ID="Panel1" runat="server">
            <div style="width: 100%;" id="divID">
                <fieldset>
                    <legend style="color: Highlight; font: 500;">Drag  and drop the folder this here</legend>
                    <%--<input class="input" type="file" multiple directory webkitdirectory allowdirs/>--%>
                </fieldset>
                <%--<p>Kéo thả thư mục vào khung. Các đường dẫn tập tin sẽ được hiển thị bên dưới ...</p>--%>
                <asp:Label ID="lbFolder" Text="" runat="server" Visible="false"></asp:Label>
                <asp:Label ID="lbCategory" Text="" runat="server" Visible="false"></asp:Label>
                <pre class="output"></pre>
            </div>
        </asp:Panel>
        <telerik:RadCodeBlock runat="server">
            <script src="Scripts/uppie.js"></script>
            <script>
                var uppie = new Uppie();
                /* Xử lý sự kiện khi người dùng kéo vào thả */
                uppie(document.documentElement, function (event, formData, files) {
                    // In lên màn hình
                    if (files.length <= 0) {
                        alert("no format");
                    } else {
                        document.querySelector(".output").textContent = files.join("\n");
                        var currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                        currentLoadingPanel.show("<%= Panel1.ClientID %>");
                        //Gửi file đi
                        files.forEach(function (path) {
                            formData.append("paths[]", path);
                        });
                        var Textreturn = "";
                        var xhr = new XMLHttpRequest();
                        xhr.open('POST', 'UploadHandler.aspx', true);
                        xhr.onreadystatechange = function () {

                            if (this.readyState == 4 && this.status == 200) {
                                var div = document.getElementById('divID');
                                if (this.responseText.substring(0, 2) == "OK") {
                                    alert("Upload Completed");
                                    div.innerHTML += "<p>Message: <span style='color:blue; font-family: Arial, Helvetica, sans-serif;font-size: 25px;'>Upload Completed</span></p>"
                                    currentLoadingPanel.hide("<%= Panel1.ClientID %>");
                                } else {
                                    var lt = this.responseText.indexOf("<!");
                                    var Masage = this.responseText.substring(0, lt - 1);
                                    alert("ERROR: \n" + Masage);
                                    div.innerHTML += " <p style='color:red;'>Error: <span style='color:red; font-family: Arial, Helvetica, sans-serif;font-size: 25px;'>" + Masage + "</span></p>"
                                }
                            }
                        };
                        xhr.send(formData);
                    }
                });

<%--           $.post("UploadHandler.aspx","",
                    function (data) {
                        if (data.substring(0, 2) == "OK") {
                            alert("Upload Completed");
                            var currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                            currentLoadingPanel.hide("<%= Panel1.ClientID %>");
                        }
                    });
                    if (xhr.status == 200) {
                        alert("Upload Completed");
                        currentLoadingPanel.hide("<%= Panel1.ClientID %>");
                    }--%>
            </script>
        </telerik:RadCodeBlock>
    </form>
</body>
</html>
