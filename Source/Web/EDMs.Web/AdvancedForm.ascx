﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdvancedForm.ascx.cs" Inherits="EDMs.Web.Controls.Appointments.AdvancedForm" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<style type="text/css">
    .auto-style4 {
        width: 100%;
    }
</style>



<div class="rsAdvancedEdit rsAdvancedModal" style="position: relative">
	<div class="rsModalBgTopLeft">
	</div>
	<div class="rsModalBgTopRight">
	</div>
	<div class="rsModalBgBottomLeft">
	</div>
	<div class="rsModalBgBottomRight">
	</div>
	<div class="rsAdvTitle">
		<h1 class="rsAdvInnerTitle">
			<%= (this.Mode.ToString() == "Edit") ? Owner.Localization.AdvancedEditAppointment : Owner.Localization.AdvancedNewAppointment %></h1>
		<asp:LinkButton runat="server" ID="AdvancedEditCloseButton" CssClass="rsAdvEditClose"
			CommandName="Cancel" CausesValidation="false" ToolTip='<%# Owner.Localization.AdvancedClose %>'>
            <%= Owner.Localization.AdvancedClose %>
		</asp:LinkButton>
	</div>
	<div class="rsAdvContentWrapper">
		<div class="rsAdvOptionsScroll">
			<table>
                <tr>
                    <td class="auto-style2">
                        <label style="width:100px">Mã</label>
                        <telerik:RadTextBox runat="server" 
                            ID="txtCode"                            
                            Enabled="false"
						    EnableSingleInputRendering="false" 
                            MaxLength="10"/>
					    <asp:RequiredFieldValidator runat="server" ID="CodeValidator" 
                            ControlToValidate="txtCode" 
                            ErrorMessage="Bắt buột nhập"
                            ForeColor="Red"
						    CssClass="rsValidatorMsg" />
                    </td>
                    <td>
                         <label style="width:80px">Bệnh nhân</label>
                        <telerik:RadComboBox ID="cboPatient" runat="server" Width="220px" 
                            AllowCustomText="True" MarkFirstMatch="True" Filter="Contains" NoWrap="True">
                        </telerik:RadComboBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <label style="width:100px">Tên</label>
                        <telerik:RadTextBox runat="server" ID="txtName" Width="160px"
						    EnableSingleInputRendering="false" MaxLength="255"/>
					    <asp:RequiredFieldValidator runat="server" ID="NameValidator" 
                            ControlToValidate="txtName" 
                            ErrorMessage="Bắt buột nhập"
                            ForeColor="Red"
						    CssClass="rsValidatorMsg" />                        
                    </td>
                    <td>
                        <label style="width:80px">Phòng</label>
                        <telerik:RadComboBox ID="cboRoom" runat="server" Width="220px" />
                    </td>
                </tr>
                 <tr>
                    <td class="auto-style2">
                        <label style="width:100px">Cuộc gọi cuối</label>
                        <telerik:RadDatePicker ID="dteLastCall" runat="server" Calendar-CultureInfo="vi-VN"/>      
                    </td>
                    <td>
                        <label style="width:80px">Bác sĩ</label>
                        <telerik:RadComboBox ID="cboResources" runat="server" Width="220px" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        <label style="width:100px">Cuội gọi tiếp theo</label>
                        <telerik:RadDatePicker ID="dteNextCall" runat="server" Calendar-CultureInfo="vi-VN"/>
                    </td>
                    <td >
                        <label style="width:80px">Trạng thái</label>
                        <telerik:RadComboBox ID="cboApptStatus" runat="server" Width="220px" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <table class="auto-style4">
                            <tr>
                                <td>
                                    <label style="width:100px">Số lần gọi</label>
                                    <telerik:RadNumericTextBox ID="numCalled" runat="server" 
                                        DataType="int" 
                                        NumberFormat-DecimalDigits="0" 
                                        NumberFormat-DecimalSeparator="0"
                                        MinValue="0" 
                                        MaxLength="2" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label style="width:100px">Ngày bắt đầu</label>
                                    <telerik:RadDatePicker runat="server" 
                                        ID="StartDate" 
                                        SharedCalendarID="SharedCalendar"
							            MinDate="2011-01-01"
                                        Calendar-CultureInfo="vi-VN"
                                        DateInput-DisplayDateFormat="dd/MM/yyyy">
						            </telerik:RadDatePicker>

                                    <telerik:RadTimePicker runat="server" 
                                        ID="StartTime"
                                        Width="90"
                                        CssClass="rsAdvTimePicker">

                                        <TimeView ID="TimeView1" runat="server" Columns="2" ShowHeader="false" StartTime="07:00"
									            EndTime="20:00" Interval="00:30" TimeFormat="HH:mm"/>
						            </telerik:RadTimePicker>
                          
                                        <asp:RequiredFieldValidator runat="server" ID="StartDateValidator" ControlToValidate="StartDate" ForeColor="Red"
						                    EnableClientScript="true" Display="Dynamic" CssClass="rsValidatorMsg" /> 
                                        <asp:RequiredFieldValidator runat="server" ID="StartTimeValidator" ControlToValidate="StartTime" ForeColor="Red"
				                            EnableClientScript="true" Display="Dynamic" CssClass="rsValidatorMsg" />  
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <label style="width:100px">Ngày kết thúc</label>
                                    <telerik:RadDatePicker runat="server" 
                                        ID="EndDate" 
                                        CssClass="rsAdvDatePicker"
                                        SharedCalendarID="SharedCalendar"
								        MinDate="2011-01-01"
                                        Calendar-CultureInfo="vi-VN"
                                        DateInput-DisplayDateFormat="dd/MM/yyyy" >									
							        </telerik:RadDatePicker>
                            
                                    <telerik:RadTimePicker runat="server" 
                                        ID="EndTime" 
                                        CssClass="rsAdvTimePicker"
								        Width="90px">

								        <TimeView ID="TimeView2" runat="server" Columns="2" ShowHeader="false" StartTime="07:00"
									        EndTime="20:00" Interval="00:30" TimeFormat="HH:mm" />
							        </telerik:RadTimePicker>

                                    <asp:RequiredFieldValidator runat="server" ID="EndDateValidator" ControlToValidate="EndDate" ForeColor="Red"
				                        EnableClientScript="true" Display="Dynamic" CssClass="rsValidatorMsg" />
			                        <asp:RequiredFieldValidator runat="server" ID="EndTimeValidator" ControlToValidate="EndTime" ForeColor="Red"
				                        EnableClientScript="true" Display="Dynamic" CssClass="rsValidatorMsg" />
                                </td>
                            </tr>
                        </table>

                    </td>
                    <td>  
                    </td>
                </tr>
                <tr>
                    <td>
                        <label style="width:80px">Loại dịch vụ</label>
                        <telerik:RadListBox ID="cboService" runat="server" CheckBoxes="true" Width="180">
                            <Items>
                                <telerik:RadListBoxItem Value="1" Text="Phẫu thuật thẩm mỹ"></telerik:RadListBoxItem>
                                <telerik:RadListBoxItem Value="2" Text="Da liễu thẩm mỹ"></telerik:RadListBoxItem>
                                <telerik:RadListBoxItem Value="3" Text="Nội tiết & chống lão hóa"></telerik:RadListBoxItem>
                                <telerik:RadListBoxItem Value="4" Text="Nha khoa thẩm mỹ"></telerik:RadListBoxItem>                               
                            </Items>
                        </telerik:RadListBox>   
                        <%--<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1_cboService" 
                            ControlToValidate="cboService" 
                            ErrorMessage="Bạn phải chọn ít nhất một dịch vụ."
                            ForeColor="Red"
						    CssClass="rsValidatorMsg" />       --%> 
                    </td>
                    <td>
                        <label style="width:80px">Y tá/Tư vấn viên</label>
                        <telerik:RadListBox ID="listResourceOthers" 
                                            AutoPostBack="true" runat="server" 
                                            CheckBoxes="true" 
                                            Width="180" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2" colspan="2">
                        <label style="width:100px; vertical-align: top;">Mô tả</label>
                        <telerik:RadTextBox runat="server" ID="txtDescription" TextMode="MultiLine" 
                            MaxLength="250" Height="60px" Width="90%"/>
                    </td>                    
                </tr>
                <tr>
                    <td class="auto-style2" >
                        <label style="width:100px">Reminder</label>
                        <telerik:RadComboBox runat="server" ID="ReminderDropDown" >
                            <Items>
                                <telerik:RadComboBoxItem Text='<%# Owner.Localization.ReminderNone %>' Value="" />
                                <%--<telerik:RadComboBoxItem Text='<%# "0 " + Owner.Localization.ReminderMinutes %>'
                                    Value="0" />--%>
                                <telerik:RadComboBoxItem Text='<%# "5 " + Owner.Localization.ReminderMinutes %>'
                                    Value="5" />
                                <telerik:RadComboBoxItem Text='<%# "10 " + Owner.Localization.ReminderMinutes %>'
                                    Value="10" />
                                <telerik:RadComboBoxItem Text='<%# "15 " + Owner.Localization.ReminderMinutes %>'
                                    Value="15" />
                                <telerik:RadComboBoxItem Text='<%# "30 " + Owner.Localization.ReminderMinutes %>'
                                    Value="30" />
                                <telerik:RadComboBoxItem Text='<%# "1 " + Owner.Localization.ReminderHour %>' Value="60" />
                                <telerik:RadComboBoxItem Text='<%# "2 " + Owner.Localization.ReminderHours %>' Value="120" />
                                <telerik:RadComboBoxItem Text='<%# "3 " + Owner.Localization.ReminderHours %>' Value="180" />
                                <telerik:RadComboBoxItem Text='<%# "4 " + Owner.Localization.ReminderHours %>' Value="240" />
                                <telerik:RadComboBoxItem Text='<%# "5 " + Owner.Localization.ReminderHours %>' Value="300" />
                                <telerik:RadComboBoxItem Text='<%# "6 " + Owner.Localization.ReminderHours %>' Value="360" />
                                <telerik:RadComboBoxItem Text='<%# "7 " + Owner.Localization.ReminderHours %>' Value="420" />
                                <telerik:RadComboBoxItem Text='<%# "8 " + Owner.Localization.ReminderHours %>' Value="480" />
                                <telerik:RadComboBoxItem Text='<%# "9 " + Owner.Localization.ReminderHours %>' Value="540" />
                                <telerik:RadComboBoxItem Text='<%# "10 " + Owner.Localization.ReminderHours %>' Value="600" />
                                <telerik:RadComboBoxItem Text='<%# "11 " + Owner.Localization.ReminderHours %>' Value="660" />
                                <telerik:RadComboBoxItem Text='<%# "12 " + Owner.Localization.ReminderHours %>' Value="720" />
                                <telerik:RadComboBoxItem Text='<%# "18 " + Owner.Localization.ReminderHours %>' Value="1080" />
                                <telerik:RadComboBoxItem Text='<%# "1 " + Owner.Localization.ReminderDays %>' Value="1440" />
                                <telerik:RadComboBoxItem Text='<%# "2 " + Owner.Localization.ReminderDays %>' Value="2880" />
                                <telerik:RadComboBoxItem Text='<%# "3 " + Owner.Localization.ReminderDays %>' Value="4320" />
                                <telerik:RadComboBoxItem Text='<%# "4 " + Owner.Localization.ReminderDays %>' Value="5760" />
                                <telerik:RadComboBoxItem Text='<%# "1 " + Owner.Localization.ReminderWeek %>' Value="10080" />
                                <telerik:RadComboBoxItem Text='<%# "2 " + Owner.Localization.ReminderWeeks %>' Value="20160" />
                            </Items>
                        </telerik:RadComboBox>
                    </td>    
                    <td >
                        <asp:Label style="width:80px" ID="lblNguoiSua" runat="server" />
                    </td>                
                </tr>
                <tr>
                    <td class="auto-style2" colspan="2">
                        <telerik:RadSchedulerRecurrenceEditor runat="server" ID="AppointmentRecurrenceEditor" Culture="vi-VN" 
                            Localization-Recurrence="Lặp lại"
                            
                            Localization-Hourly="Giờ"
                            Localization-Daily="Ngày"
                            Localization-Weekly="Tuần"
                            Localization-Monthly="Tháng"                            
                            Localization-Yearly="Năm"
                            
                            Localization-Day="Ngày"
                            Localization-Hours="Giờ"
                            Localization-Every="Mỗi"
                            Localization-EveryWeekday="Vào cuối tuần"
                            Localization-RecurEvery="Vào mỗi"
                            Localization-Weeks="tuần"
                            Localization-OfEvery="của mỗi"
                            Localization-The="Lần"
                            Localization-Months="tháng"
                           
                            Localization-Days="Ngày"
                            Localization-Of="của"

                            Localization-NoEndDate="Không ngày kết thúc"
                            Localization-Occurrences="bao nhiêu lần"
                            Localization-EndAfter="Kết thúc sau"                            
                            Localization-EndByThisDate="Ngày kết thúc"
                            />      
                    </td>                    
                </tr>                
            </table>

			<asp:CustomValidator runat="server" ID="DurationValidator" ControlToValidate="StartDate"
				EnableClientScript="false" Display="Dynamic" CssClass="rsValidatorMsg rsInvalid"
				OnServerValidate="DurationValidator_OnServerValidate" />

            <asp:HiddenField runat="server" ID="OriginalRecurrenceRule" />
            <asp:HiddenField runat="server" ID="CreatedByHidden" />
            <asp:HiddenField runat="server" ID="ParentAppointmentIdHidden" />
            <asp:HiddenField runat="server" ID="UpdateByHidden" />
            <asp:HiddenField runat="server" ID="LastUpdateHidden" />
            <asp:HiddenField runat="server" ID="IsBlockHidden" />
            <asp:HiddenField runat="server" ID="ResourceOthersOrginalHidden" />

            <telerik:RadCalendar runat="server" ID="SharedCalendar" 
					CultureInfo="vi-VN" ShowRowHeaders="false" RangeMinDate="2011-01-01" />
           
		</div>

		<asp:Panel runat="server" ID="ButtonsPanel" CssClass="rsAdvancedSubmitArea">
			<div class="rsAdvButtonWrapper">
				<asp:LinkButton runat="server" ID="UpdateButton" CssClass="rsAdvEditSave">
					<span><%= Owner.Localization.Save %></span>
				</asp:LinkButton>
				<asp:LinkButton runat="server" ID="CancelButton" CssClass="rsAdvEditCancel" CommandName="Cancel"
					CausesValidation="false">
					<span><%= Owner.Localization.Cancel %></span>
				</asp:LinkButton>
			</div>
		</asp:Panel>
	</div>
    </div>