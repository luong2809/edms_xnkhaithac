﻿namespace EDMs.Web.Utilities
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Data.Objects.DataClasses;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Security.Cryptography;
    using System.ServiceProcess;
    using System.Text;
    using System.Text.RegularExpressions;

    using EDMs.Business.Services;

    using Telerik.Web.UI;

    public class Utility
    {
        /// <summary>
        /// The passphrase.
        /// </summary>
        private const string Passphrase = "MASUpilamix!987";

        /// <summary>
        /// The clone.
        /// </summary>
        /// <param name="entityObject">
        /// The entity object.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T Clone<T>(T entityObject) where T : EntityObject, new()
        {
            return EntityCloner<T>.Clone(entityObject);
        }

        public static Dictionary<string, string> FileIcon = new Dictionary<string, string>()
            {
                { "bak", "images/datasql.png" },
                { "pptx", "images/powerpointFile.png" },
                { "ppt", "images/powerpointFile.png" },
                { "pps", "images/powerpointFile.png" },
                { "doc", "images/wordfile.png" },
                { "docx", "images/wordfile.png" },
                { "dotx", "images/wordfile.png" },
                { "pdf", "images/pdffile.png" },
                { "7z", "images/7z.png" },
                { "dwg", "images/dwg.png" },
                { "dxf", "images/dxf.png" },
                { "rar", "images/rar.png" },
                { "zip", "images/zip.png" },
                { "txt", "images/txt.png" },
                { "rtf", "images/txt.png" },
                { "xml", "images/xml.png" },
                { "csv", "images/excelfile.png" },
                { "xls", "images/excelfile.png" },
                { "xlsx", "images/excelfile.png" },
                { "xlsm", "images/excelfile.png" },
            {"webp","images/bmp.png" },
                { "bmp", "images/bmp.png" },
                { "gif", "images/gif_file.png"},
                { "mp4", "images/video.png" },
                { "webm", "images/video.png" },
                { "mkv", "images/video.png" },
                { "flv", "images/video.png" },
                { "avi", "images/video.png" },
                { "mov", "images/video.png" },
                { "mp3", "images/mp3.png" },
                { "wmv", "images/video.png" },
                { "mpeg", "images/video.png" },
                { "jpg", "images/picture.png" },
                { "jpeg", "images/picture.png" },
                { "png", "images/picture.png" },
                { "tif", "images/tiffile.png" },
                { "tiff", "images/tiffile.png" },
                { "err", "images/otherfile.png" },
                { "tmp", "images/otherfile.png" },
                { "msg", "images/otherfile.png" },
                { "xer", "images/otherfile.png" },
                { "wma", "images/otherfile.png" },
                { "htm", "images/otherfile.png" },
                { "dat", "images/otherfile.png" },
                { "null", "images/otherfile.png" },
            };

        /// <summary>
        /// The clone with graph.
        /// </summary>
        /// <param name="entityObject">
        /// The entity object.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T CloneWithGraph<T>(T entityObject) where T : EntityObject, new()
        {
            return EntityCloner<T>.CloneWithGraph(entityObject);
        }

        /// <summary>
        /// The deep clone.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T DeepClone<T>(T obj)
        {
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, obj);
                stream.Position = 0;
                return (T)formatter.Deserialize(stream);
            }
        }

        /// <summary>
        /// Generate patient code
        /// </summary>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <returns>
        /// Patient code with format
        /// </returns>
        public static string GeneratePatientCode(string code)
        {
            var patientCodeFormat = System.Configuration.ConfigurationManager.AppSettings["PatientCodeFormat"];
            var result =
                (patientCodeFormat.Remove(patientCodeFormat.Length - 1 - code.Length, code.Length) + code).Replace(
                    "x", "0");

            return result;
        }

        public static string RemoveUnicode(string inputText)
        {
            string stFormD = inputText.Normalize(System.Text.NormalizationForm.FormD);
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string str = "";
            for (int i = 0; i <= stFormD.Length - 1; i++)
            {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(stFormD[i]);
                if (uc == UnicodeCategory.NonSpacingMark == false)
                {
                    if (stFormD[i] == 'đ')
                        str = "d";
                    else if (stFormD[i] == 'Đ')
                        str = "D";
                    else if (stFormD[i] == '\r' | stFormD[i] == '\n')
                        str = "";
                    else
                        str = stFormD[i].ToString();
                    sb.Append(str);
                }
            }
            return sb.ToString();
        }

        /// <summary>
        /// Converts the type of the string to.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="param">The param.</param>
        /// <returns></returns>
        public static object ConvertStringToType<T>(string param)
        {
            var underlyingType = Nullable.GetUnderlyingType(typeof(T));
            if (underlyingType == null)
                return Convert.ChangeType(param, typeof(T), CultureInfo.InvariantCulture);
            return String.IsNullOrEmpty(param)
              ? null
              : Convert.ChangeType(param, underlyingType, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// The encrypt.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string Encrypt(string message)
        {
            byte[] results;
            var utf8 = new UTF8Encoding();

            // to create the object for UTF8Encoding  class
            // TO create the object for MD5CryptoServiceProvider 
            var md5 = new MD5CryptoServiceProvider();
            byte[] deskey = md5.ComputeHash(utf8.GetBytes(Passphrase));

            // to convert to binary passkey
            // TO create the object for  TripleDESCryptoServiceProvider 
            var desalg = new TripleDESCryptoServiceProvider();
            desalg.Key = deskey; // to  pass encode key
            desalg.Mode = CipherMode.ECB;
            desalg.Padding = PaddingMode.PKCS7;
            byte[] encrypt_data = utf8.GetBytes(message);

            // to convert the string to utf encoding binary 

            try
            {
                // To transform the utf binary code to md5 encrypt    
                ICryptoTransform encryptor = desalg.CreateEncryptor();
                results = encryptor.TransformFinalBlock(encrypt_data, 0, encrypt_data.Length);
            }
            finally
            {
                // to clear the allocated memory
                desalg.Clear();
                md5.Clear();
            }

            // to convert to 64 bit string from converted md5 algorithm binary code
            return Convert.ToBase64String(results);
        }

        /// <summary>
        /// The decrypt.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string Decrypt(string message)
        {
            byte[] results;
            var utf8 = new UTF8Encoding();
            var md5 = new MD5CryptoServiceProvider();
            byte[] deskey = md5.ComputeHash(utf8.GetBytes(Passphrase));
            var desalg = new TripleDESCryptoServiceProvider();
            desalg.Key = deskey;
            desalg.Mode = CipherMode.ECB;
            desalg.Padding = PaddingMode.PKCS7;
            byte[] decrypt_data = Convert.FromBase64String(message);
            try
            {
                // To transform the utf binary code to md5 decrypt
                ICryptoTransform decryptor = desalg.CreateDecryptor();
                results = decryptor.TransformFinalBlock(decrypt_data, 0, decrypt_data.Length);
            }
            finally
            {
                desalg.Clear();
                md5.Clear();
            }

            // TO convert decrypted binery code to string
            return utf8.GetString(results);
        }

        public static string GetMd5Hash(string input)
        {
            var md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
            var sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        /// <summary>
        /// The convert to data table.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="DataTable"/>.
        /// </returns>
        public static DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }

            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }

                table.Rows.Add(row);
            }

            return table;

        }

        /// <summary>
        /// The service is available.
        /// </summary>
        /// <param name="serviceName">
        /// The service name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool ServiceIsAvailable(string serviceName)
        {
            var ctl = ServiceController.GetServices().FirstOrDefault(s => s.ServiceName == serviceName);
            if (ctl != null)
            {
                if (ctl.Status == ServiceControllerStatus.Running)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// The remove all special character.
        /// </summary>
        /// <param name="strSource">
        /// The str source.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string RemoveAllSpecialCharacter(string strSource)
        {
            var strDes = Regex.Replace(strSource, @"[\&\:\*\%\$\^\#\@\{\}\?\<\>\!\/\\]", string.Empty);
            return strDes;
        }

        public static string RemoveAllSpecialCharacterFolder(string strSource)
        {
            var strDes = Regex.Replace(strSource, @"[\&\:\*\%\$\^\#\@\{\}\?\<\>\!]", string.Empty);
            return strDes;
        }
        public static string RemoveSpecialCharacter(string input)
        {
            return Regex.Replace(input, @"[^0-9a-zA-Z]+", string.Empty);
        }

        public static string MakeValidFileName(string name)
        {
            string invalidChars = Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
            string invalidReStr = string.Format(@"[{0}]+", invalidChars);
            string replace = Regex.Replace(name, invalidReStr, "_").Replace(";", "").Replace(",", "");
            return replace;
        }

        // SortNodes is a recursive method enumerating and sorting all node levels 

        /// <summary>
        /// The sort nodes.
        /// </summary>
        /// <param name="collection">
        /// The collection.
        /// </param>
        public static void SortNodes(RadTreeNodeCollection collection)
        {
            Sort(collection);
            foreach (RadTreeNode node in collection)
            {
                if (node.Nodes.Count > 0)
                {
                    SortNodes(node.Nodes);
                }
            }
        }

        //The Sort method is called for each node level sorting the child nodes 
        private static void Sort(RadTreeNodeCollection collection)
        {
            RadTreeNode[] nodes = new RadTreeNode[collection.Count];
            collection.CopyTo(nodes, 0);
            Array.Sort(nodes, new TreeNodeComparer());
            collection.Clear();
            collection.AddRange(nodes);
        }

        public static bool ConvertStringToDateTimeddMMyyyy(string input, ref DateTime output)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var strDatetime = input.Substring(0, input.LastIndexOf("/") + 5);

                if (DateTime.TryParseExact(strDatetime, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                {
                    return true;
                }

                if (DateTime.TryParseExact(strDatetime, "d/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                {
                    return true;
                }

                if (DateTime.TryParseExact(strDatetime, "dd/M/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                {
                    return true;
                }

                if (DateTime.TryParseExact(strDatetime, "d/M/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                {
                    return true;
                }

                if (DateTime.TryParseExact(strDatetime, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                {
                    return true;
                }

                if (DateTime.TryParseExact(strDatetime, "M/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                {
                    return true;
                }

                if (DateTime.TryParseExact(strDatetime, "MM/d/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                {
                    return true;
                }

                if (DateTime.TryParseExact(strDatetime, "M/d/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out output))
                {
                    return true;
                }
            }

            return false;
        }
    }

    

    //The TreeNodeComparer class defines the sorting criteria 
    class TreeNodeComparer : IComparer
    {
        #region IComparer Members

        public int Compare(object x, object y)
        {
            RadTreeNode firstNode = (RadTreeNode)x;
            RadTreeNode secondNode = (RadTreeNode)y;

            return firstNode.Text.CompareTo(secondNode.Text);
        }

        #endregion
    }
}