﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web.UI;
    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// Class customer
    /// </summary>
    public partial class Default : Page
    {
        /// <summary>
        /// The permission service.
        /// </summary>
        private readonly PermissionService permissionService = new PermissionService();

        private readonly DocumentService documentService = new DocumentService();

        private readonly GroupDataPermissionService groupDataPermissionService = new GroupDataPermissionService();

        private readonly  CategoryService categoryService = new CategoryService();

        private readonly UserDataPermissionService userDataPermissionService = new UserDataPermissionService();

        private readonly SharedDocumentService sharedDocumentService = new SharedDocumentService();

        protected const string ServiceName = "EDMSFolderWatcher";
        private readonly GroupFunctionService _GroupfunctionService = new GroupFunctionService();
        private readonly FolderPermissionService _FolderPermisstionService = new FolderPermissionService();
        public static RadTreeNode editedNode = null;

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string UnreadPattern = @"\(\d+\)";

        private List<int> AdminGroup
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("GroupAdminList").Split(',').Select(t => Convert.ToInt32(t)).ToList();
            }
        }
        private List<int> GroupFunctionList
        {
            get
            {
                return this._GroupfunctionService.GetByRoleId(UserSession.Current.User.Id).Select(t => t.ID).ToList();
            }
        }
        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            this.txtfullname.Text = ConfigurationManager.AppSettings.Get("AdminName");
            this.txtemail.Text = ConfigurationManager.AppSettings.Get("AdminEmail");
            this.txtphone.Text = ConfigurationManager.AppSettings.Get("AdminPhone");
            if (!Page.IsPostBack)
            {
                //var categoryPermission = this._FolderPermisstionService.GetCategory(GroupFunctionList).Select(t => t.CategoryId.GetValueOrDefault()).ToList();
                var categoryPermission = this._FolderPermisstionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault()).Select(t => t.CategoryId.GetValueOrDefault()).ToList();
                categoryPermission = categoryPermission.Union(this._FolderPermisstionService.GetCategoryByUsser(UserSession.Current.User.Id).Select(t => t.CategoryId.GetValueOrDefault())).Distinct().ToList();

                var listCategory = this.AdminGroup.Contains(UserSession.Current.RoleId)
                                    ? this.categoryService.GetAll()
                                    : this.categoryService.GetAll().Where(t => categoryPermission.Any(x => x == t.ID)).ToList();

                if (!this.AdminGroup.Contains(UserSession.Current.RoleId))
                {
                    this.grdDocument.MasterTableView.GetColumn("CreatedUser").Display = false;
                    this.grdDocument.MasterTableView.GetColumn("CreatedDate").Display = false;
                }
                if (listCategory.Any())
                {
                    foreach (var category in listCategory)
                    {
                        category.ParentId = -1;
                    }

                    listCategory.Insert(0, new Category() { ID = -1, Name = "DOCUMENT GROUP" });

                    this.radPbCategories.DataSource = listCategory;
                    this.radPbCategories.DataFieldParentID = "ParentId";
                    this.radPbCategories.DataFieldID = "Id";
                    this.radPbCategories.DataValueField = "Id";
                    this.radPbCategories.DataTextField = "Name";
                    this.radPbCategories.DataBind();
                    this.radPbCategories.Items[0].Expanded = true;

                    foreach (RadPanelItem item in this.radPbCategories.Items[0].Items)
                    {
                        item.ImageUrl = @"Images/category2.png";
                        item.NavigateUrl = "DocumentsHome.aspx?doctype=" + item.Value;
                    }    
                }

                this.LoadListPanel();
                this.LoadSystemPanel();
            }
        }
        
        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (this.AdminGroup.Contains(UserSession.Current.RoleId))
            {
                this.grdDocument.DataSource = this.documentService.GetTop100New();
            }
            else
            {
                //var allFolderPermiss = this.groupDataPermissionService.GetByRoleId(UserSession.Current.RoleId).Where(t => !string.IsNullOrEmpty(t.FolderIdList)).Select(t => Convert.ToInt32(t.FolderIdList)).ToList();

                //allFolderPermiss = allFolderPermiss.Union(this.userDataPermissionService
                //                                            .GetByUserId(UserSession.Current.User.Id)
                //                                            .Select(t => t.FolderId.GetValueOrDefault()))
                //                                            .Distinct().ToList();

                //var folderPermission = this._FolderPermisstionService.GetFolderByRoleIdList(GroupFunctionList)
                //                              .Where(t => t.FolderId != null && (t.File_Read.GetValueOrDefault() || t.File_Write.GetValueOrDefault()) && !t.File_NoAccess.GetValueOrDefault())
                //                              .ToList();
                var folderPermission = this._FolderPermisstionService.GetByRoleId(Convert.ToInt32(Request.QueryString["doctype"]), UserSession.Current.User.RoleId.GetValueOrDefault())
                                               .Where(t => t.FolderId != null && (t.Folder_Read.GetValueOrDefault() || t.Folder_Write.GetValueOrDefault())).ToList();
                folderPermission = folderPermission.Union(this._FolderPermisstionService.GetByUserId(UserSession.Current.User.Id).Where(t => t.FolderId != null && (t.Folder_Read.GetValueOrDefault() || t.Folder_Write.GetValueOrDefault()))).Distinct().ToList();

                var listfolderpermission = folderPermission.Select(t => t.FolderId.GetValueOrDefault()).ToList();

                this.grdDocument.DataSource = this.documentService.GetTop100New(listfolderpermission);
            }
        }

        private void LoadListPanel()
        {
            var listId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("ListID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), listId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "LIST" });

                this.radPbList.DataSource = permissions;
                this.radPbList.DataFieldParentID = "ParentId";
                this.radPbList.DataFieldID = "Id";
                this.radPbList.DataValueField = "Id";
                this.radPbList.DataTextField = "MenuName";
                this.radPbList.DataBind();
                this.radPbList.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbList.Items[0].Items)
                {
                    item.ImageUrl = @"Images/listmenu.png";
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                }
            }
        }

        private void LoadSystemPanel()
        {
            var systemId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("SystemID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), systemId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "SYSTEM" });

                this.radPbSystem.DataSource = permissions;
                this.radPbSystem.DataFieldParentID = "ParentId";
                this.radPbSystem.DataFieldID = "Id";
                this.radPbSystem.DataValueField = "Id";
                this.radPbSystem.DataTextField = "MenuName";
                this.radPbSystem.DataBind();
                this.radPbSystem.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbSystem.Items[0].Items)
                {
                    item.ImageUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Icon;
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                }
            }
        }
    }
}

