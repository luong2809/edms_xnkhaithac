﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReviewLeaveOfResource.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class Resource
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDMs.Business.Services;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;
using Appointment = EDMs.Data.Entities.Appointment;

namespace EDMs.Web
{
    /// <summary>
    /// Class Resource
    /// </summary>
    public partial class ReviewLeaveOfResource : Page
    {
        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ((RadPane)
                 Master.FindControl("RadSplitter1").FindControl("contentPane").FindControl("RadSplitter2").FindControl(
                     "leftPane"))
                    .Collapsed = true;
            }
        }

        /// <summary>
        /// The load Resource by Resource type.
        /// </summary>
        /// <param name="isbind">
        /// The isbind.
        /// </param>
        protected void LoadResourceByResourceType(bool isbind = false)
        {
            if (UserSession.Current.User.CanApprove.GetValueOrDefault())
            {
                var appointmentService = new AppointmentService();
                grdAppointment.DataSource =
                    appointmentService.GetAll().Where(
                        t => t.IsBlock.GetValueOrDefault() && !t.IsApprove.GetValueOrDefault());
            }
            else
            {
                grdAppointment.DataSource = new List<Appointment>();
            }

            if (isbind)
            {
                grdAppointment.DataBind();
            }
        }

        /// <summary>
        /// The rad tree view 1_ node click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadTreeView1_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Node.Value))
            {
                LoadResourceByResourceType(true);
            }
        }

        /// <summary>
        /// The load control.
        /// </summary>
        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdAppointment_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            LoadResourceByResourceType();
        }

        /// <summary>
        /// The resource menu_ button click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void ResourceMenu_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            if (e.Item.Value == "Approve")
            {
                foreach (GridDataItem dataItem in grdAppointment.Items)
                {
                    if (dataItem.ItemType == GridItemType.Item || dataItem.ItemType == GridItemType.AlternatingItem)
                    {
                        var checkBox = (CheckBox) dataItem["ClientSelectColumn"].Controls[0];
                        if (checkBox.Checked)
                        {
                            var appointmentService = new AppointmentService();
                            Appointment appointment = appointmentService.GetByID(Convert.ToInt32(dataItem["Id"].Text));
                            if (appointment != null)
                            {
                                appointment.IsApprove = true;
                                appointmentService.Update(appointment);

                                var resourceService = new ResourceService();
                                var resource = resourceService.GetByID(Convert.ToInt32(appointment.ResourceId));
                                if (resource != null && resource.IsFulltime.GetValueOrDefault())
                                {
                                    resource.RemainDayLeaveOf = resource.RemainDayLeaveOf - Math.Round((appointment.End.GetValueOrDefault() - appointment.Start.GetValueOrDefault()).TotalHours / 24, 1);

                                    resourceService.Update(resource);
                                }
                            }
                        }
                    }
                }
            }

            grdAppointment.Rebind();
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                grdAppointment.MasterTableView.SortExpressions.Clear();
                grdAppointment.MasterTableView.GroupByExpressions.Clear();
                grdAppointment.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                grdAppointment.MasterTableView.SortExpressions.Clear();
                grdAppointment.MasterTableView.GroupByExpressions.Clear();
                grdAppointment.MasterTableView.CurrentPageIndex = grdAppointment.MasterTableView.PageCount - 1;
                grdAppointment.Rebind();
            }
        }
    }
}