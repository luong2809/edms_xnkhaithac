﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DocumentsHome.aspx.cs" Inherits="EDMs.Web.DocumentsHome" EnableViewState="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="Scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="Content/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="Content/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="Content/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
    <style type="text/css">
        /*Custom CSS of Grid documents for FF browser*/
        /*#ctl00_ContentPlaceHolder2_grdDocument_ctl00_Header{table-layout:auto !important;}
        #ctl00_ContentPlaceHolder2_grdDocument_ctl00{table-layout:auto !important;}*/
        /*End*/
        @-moz-document url-prefix() {
            #ctl00_ContentPlaceHolder2_grdDocument_ctl00_Header {
                table-layout: auto !important;
            }

            #ctl00_ContentPlaceHolder2_grdDocument_ctl00 {
                table-layout: auto !important;
            }
        }

        .RadGrid .rgRow td, .RadGrid .rgAltRow td, .RadGrid .rgEditRow td, .RadGrid .rgFooter td, .RadGrid .rgFilterRow td, .RadGrid .rgHeader, .RadGrid .rgResizeCol, .RadGrid .rgGroupHeader td {
            padding-left: 1px !important;
            padding-right: 2px !important;
            vertical-align: top;
        }

        .RadGrid .rgSelectedRow {
            background-image: none !important;
            background-color: coral !important;
        }

        .rtPlus, .rtMinus {
            background-image: url('Images/plus-minus-icon1.png') !important;
            cursor: pointer;
        }

        .rtIn {
            cursor: pointer;
        }

        .new1 .rtPlus, .new1 .rtMinus {
            background-image: url('Images/plus-minus-icon2.png') !important;
        }

        .new2 .rtPlus, .new2 .rtMinus {
            background-image: url('Images/plus-minus-icon3.png') !important;
        }


        a.tooltip {
            outline: none;
            text-decoration: none;
        }

            a.tooltip strong {
                line-height: 30px;
            }

            a.tooltip:hover {
                text-decoration: none;
            }

            a.tooltip span {
                z-index: 10;
                display: none;
                padding: 14px 20px;
                margin-top: -30px;
                margin-left: 5px;
                width: 240px;
                line-height: 16px;
            }

            a.tooltip:hover span {
                display: inline;
                position: absolute;
                color: #111;
                border: 1px solid #DCA;
                background: #fffAF0;
            }

        .callout {
            z-index: 20;
            position: absolute;
            top: 30px;
            border: 0;
            left: -12px;
        }

        /*CSS3 extras*/
        a.tooltip span {
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            -moz-box-shadow: 5px 5px 8px #CCC;
            -webkit-box-shadow: 5px 5px 8px #CCC;
            box-shadow: 5px 5px 8px #CCC;
        }

        .rgMasterTable {
            table-layout: auto;
        }


        #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_grdDocumentPanel, #ctl00_ContentPlaceHolder2_ctl00_ContentPlaceHolder2_divContainerPanel {
            height: 100% !important;
        }

        #ctl00_ContentPlaceHolder2_RadPageView1, #ctl00_ContentPlaceHolder2_RadPageView2,
        #ctl00_ContentPlaceHolder2_RadPageView3, #ctl00_ContentPlaceHolder2_RadPageView4,
        #ctl00_ContentPlaceHolder2_RadPageView5 {
            height: 100% !important;
        }

        #divContainerLeft {
            width: 25%;
            float: left;
            margin: 5px;
            height: 99%;
            border-right: 1px dotted green;
            padding-right: 5px;
        }

        #divContainerRight {
            width: 100%;
            float: right;
            margin-top: 5px;
            height: 99%;
        }

        #RAD_SLIDING_PANE_TAB_ctl00_ContentPlaceHolder2_RadSlidingPane1 {
            height: 21px !important;
        }

        #RAD_SLIDING_PANE_TAB_ctl00_ContentPlaceHolder2_RadSlidingPane1 {
            height: 13px !important;
            width: 0px !important;
        }

        .dotted {
            border: 1px dotted #000;
            border-style: none none dotted;
            color: #fff;
            background-color: #fff;
        }

        .exampleWrapper {
            width: 100%;
            height: 100%;
            /*background: transparent url(images/background.png) no-repeat top left;*/
            position: relative;
        }

        .tabStrip {
            position: absolute;
            top: 0px;
            left: 0px;
        }

        .multiPage {
            position: absolute;
            top: 30px;
            left: 0px;
            color: white;
            width: 100%;
            height: 100%;
        }

        /*Fix RadMenu and RadWindow z-index issue*/
        .radwindow {
            z-index: 8000 !important;
        }

        .TemplateMenu {
            z-index: 10;
            padding-bottom: 20px;
            padding-left: 223px;
        }

        .tableProperties {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        .tdProperties, .thProperties {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 5px;
        }



        /* show div*/
        /*    .loader,
        .loader:after {
            border-radius: 50%;
            width: 10em;
            height: 10em;
        }
        #loader {            
            margin: 60px auto;
            font-size: 10px;
            position: relative;
            z-index: 10000;
            position: fixed;
            right:50px;
            bottom:50px;
            text-indent: -9999em;
            border-top: 1.1em solid rgba(255, 255, 255, 0.2);
            border-right: 1.1em solid rgba(255, 255, 255, 0.2);
            border-bottom: 1.1em solid rgba(255, 255, 255, 0.2);
            border-left: 1.1em solid #ffffff;
            -webkit-transform: translateZ(0);
            -ms-transform: translateZ(0);
            transform: translateZ(0);
            -webkit-animation: load8 1.1s infinite linear;
            animation: load8 1.1s infinite linear;
        }
        @-webkit-keyframes load8 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @keyframes load8 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }*/
        #loading {
            position: absolute;
            right: 50px;
            bottom: 50px;
            width: 200px;
            height: 100px;
            border-top: 1.1em solid rgba(255, 255, 255, 0.2);
            border-right: 1.1em solid rgba(255, 255, 255, 0.2);
            border-bottom: 1.1em solid rgba(255, 255, 255, 0.2);
            border-left: 1.1em solid #ffffff;
        }

        .tdProperties {
            font-size: 8pt;
        }

        .thProperties {
            font-size: 8.5pt;
        }

        #fader {
            opacity: 0.5;
            background: black;
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            display: none;
        }
    </style>
    <telerik:RadPanelBar ID="radPbSearch" runat="server" Width="100%">
        <Items>
            <telerik:RadPanelItem Text="ADVANCE SEARCH" runat="server" Expanded="True" Width="100%">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Advance Search" ImageUrl="Images/search.gif" />
                </Items>
            </telerik:RadPanelItem>
        </Items>
    </telerik:RadPanelBar>
    <telerik:RadPanelBar ID="radPbCategories" runat="server" Width="100%" />
    <telerik:RadPanelBar ID="radPbList" runat="server" Width="100%" />
    <telerik:RadPanelBar ID="radPbSystem" runat="server" Width="100%" />
    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" Skin="Silk" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <telerik:RadSplitter ID="RadSplitter4" runat="server" Orientation="Horizontal" Width="100%" Height="100%">
        <%--<telerik:RadPane ID="RadPane3" runat="server" Height="30px" Scrollable="false" Scrolling="None">
                        <telerik:RadToolBar ID="CustomerMenu" runat="server" Width="100%" OnClientButtonClicking="OnClientButtonClicking">
                <Items>
                    <telerik:RadToolBarDropDown runat="server" Text="New" ImageUrl="~/Images/addNew.png">
                        <Buttons>
                            <telerik:RadToolBarButton runat="server" Text="Document" Value="1" ImageUrl="~/Images/addDocument.png"></telerik:RadToolBarButton>
                            <telerik:RadToolBarButton runat="server" Text="Multi documents" Value="2" ImageUrl="~/Images/addmulti.png"></telerik:RadToolBarButton>
                            <telerik:RadToolBarButton runat="server" Text="Upload folder" Value="3" ImageUrl="~/Images/upload.png" Visible="false" />
                        </Buttons>
                    </telerik:RadToolBarDropDown>
                    <telerik:RadToolBarButton runat="server" IsSeparator="true" Visible="false" />
                    <telerik:RadToolBarDropDown runat="server" Text="Action" ImageUrl="~/Images/action.png">
                        <Buttons>
                            <telerik:RadToolBarButton runat="server" Text="Send notifications" Value="3" ImageUrl="~/Images/sendnotification.png" Visible="false"></telerik:RadToolBarButton>
                            <telerik:RadToolBarButton runat="server" Text="Send mail with Attachment" Value="3" ImageUrl="~/Images/email.png"></telerik:RadToolBarButton>
                            <telerik:RadToolBarButton runat="server" Text="Send mail with Link System" Value="3" ImageUrl="~/Images/email.png"></telerik:RadToolBarButton>
                            <telerik:RadToolBarButton runat="server" IsSeparator="true" Visible="false" />
                            <telerik:RadToolBarButton runat="server" Text="Export master list" Value="5" ImageUrl="~/Images/exexcel.png" Visible="false" />
                            <telerik:RadToolBarButton runat="server" Text="Import data file" Value="7" ImageUrl="~/Images/import.png" Visible="false" />
                            <telerik:RadToolBarButton runat="server" IsSeparator="true" Visible="false" />
                            <telerik:RadToolBarButton runat="server" Text="Move files" Value="6" ImageUrl="~/Images/Move.png" Visible="false" />
                            <telerik:RadToolBarButton runat="server" Text="View explorer" Value="4" ImageUrl="~/Images/expview.png" Visible="false" />
                            <telerik:RadToolBarButton runat="server" IsSeparator="true" />
                            <telerik:RadToolBarButton runat="server" Text="Checkout selected documents" Value="8" ImageUrl="~/Images/checkout.png" Visible="false" />
                            <telerik:RadToolBarButton runat="server" Text="Checkin selected documents" Value="8" ImageUrl="~/Images/checkin.png" Visible="false" />
                            <telerik:RadToolBarButton runat="server" Text="Download multi documents" Value="3" ImageUrl="~/Images/download.png" />
                            <telerik:RadToolBarButton runat="server" IsSeparator="true" />
                            <telerik:RadToolBarButton runat="server" Text="Import data file" Value="7" ImageUrl="~/Images/import.png" Visible="false" />

                            <telerik:RadToolBarButton runat="server" Text="filesize" Value="1" Visible="false" />
                        </Buttons>
                    </telerik:RadToolBarDropDown>
                    <telerik:RadToolBarButton CssClass="searchBox">
                        <ItemTemplate>
                            <span>Search </span>
                            <telerik:RadSearchBox ID="txtSearch" runat="server" Width="300px" EmptyMessage="Type here" EnableAutoComplete="false" OnSearch="txtSearch_Search"></telerik:RadSearchBox>

                        </ItemTemplate>
                    </telerik:RadToolBarButton>
                    <telerik:RadToolBarButton runat="server" IsSeparator="true" />
                    <telerik:RadToolBarButton runat="server">
                        <ItemTemplate>
                            <telerik:RadButton ID="btnAdvanceSearch" runat="server" OnClick="btnAdvanceSearch_Click" Width="120px" Text="Advance Search" ToolTip="Advance Search" Style="text-align: right; margin-left: 5px;">
                                <Icon PrimaryIconUrl="~/Images/search.gif" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                            </telerik:RadButton>
                        </ItemTemplate>
                    </telerik:RadToolBarButton>
                </Items>
            </telerik:RadToolBar>
        </telerik:RadPane>--%>
        <telerik:RadPane ID="RadPane2" runat="server" Scrollable="false" Scrolling="None">
            <telerik:RadSplitter ID="Radsplitter10" runat="server" Orientation="Vertical">
                <telerik:RadPane ID="Radpane1" runat="server" Scrolling="Both" Width="270" MinWidth="270">
                    <telerik:RadToolBar ID="radMenuFolder" runat="server" Width="100%">
                        <Items>
                            <telerik:RadToolBarButton runat="server" IsSeparator="true" Visible="false" />
                            <telerik:RadToolBarButton runat="server" Value="ShowAll">
                                <ItemTemplate>
                                    <telerik:RadButton ID="btnAddFolder" runat="server" OnClick="btnAddFolder_Click" Width="30px" ToolTip="New folder" Style="text-align: center; margin-left: 5px;">
                                        <Icon PrimaryIconUrl="~/Images/folder-add.png" PrimaryIconLeft="3" PrimaryIconTop="2" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                    </telerik:RadButton>
                                    <telerik:RadButton ID="btrename" runat="server" OnClick="btrename_Click" Width="30px" ToolTip="Rename Folder" Style="text-align: center; margin-left: 5px;">
                                        <Icon PrimaryIconUrl="~/Images/rename.png" PrimaryIconLeft="3" PrimaryIconTop="2" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                    </telerik:RadButton>
                                    <telerik:RadButton ID="btDelete" runat="server" OnClientClicking="StandardConfirm" OnClick="btDelete_Click" Width="30px" ToolTip="Delete Folder" Style="text-align: center; margin-left: 5px;">
                                        <Icon PrimaryIconUrl="~/Images/folder-delete.png" PrimaryIconLeft="3" PrimaryIconTop="2" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                        </telerik:RadButton>
                                    <telerik:RadButton ID="btpermission" runat="server" OnClientClicked="ShowChangePermission" Width="30px" ToolTip="Change Folder Permission" Style="text-align: center; margin-left: 5px;">
                                        <Icon PrimaryIconUrl="~/Images/iconfinder_folder_block.png" PrimaryIconLeft="3" PrimaryIconTop="2" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                    </telerik:RadButton>
                                    <telerik:RadButton ID="btProperties" runat="server" Visible="false" OnClick="btProperties_Click" AutoPostBack="true" Width="30px" ToolTip="Folder Properties" Style="text-align: center; margin-left: 5px;">
                                        <Icon PrimaryIconUrl="~/Images/iconfinder_share.png" PrimaryIconLeft="3" PrimaryIconTop="2" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                    </telerik:RadButton>
                                      <telerik:RadButton ID="btmove" runat="server"  OnClientClicked="MoveFodler" Width="30px" ToolTip="Move Folder" style="text-align: center; margin-left:5px;">
                                        <Icon PrimaryIconUrl="~/Images/iconfinder_Open.png" PrimaryIconLeft="3" PrimaryIconTop="2" PrimaryIconWidth="16" PrimaryIconHeight="16"/>
                                    </telerik:RadButton>
                                        <telerik:RadButton ID="btDownloadfolder" runat="server"  OnClick="btDownloadfolder_Click"  OnClientClicking="btDownloadfolder" Width="30px" ToolTip="Download Folder" style="text-align: center; margin-left:5px;">
                                        <Icon PrimaryIconUrl="~/Images/folders-downloads.png" PrimaryIconLeft="3" PrimaryIconTop="2" PrimaryIconWidth="16" PrimaryIconHeight="16"/>
                                    </telerik:RadButton>
                                    </ItemTemplate>
                            </telerik:RadToolBarButton>
                            <telerik:RadToolBarButton runat="server" Height="35px" IsSeparator="true" Visible="false" />
                        </Items>
                    </telerik:RadToolBar>
                    <telerik:RadSlidingZone ID="SlidingZone1" runat="server" DockedPaneId="RadSlidingPane1">
                        <telerik:RadSlidingPane ID="RadSlidingPane1" runat="server"
                            Title="Folder Tree" Width="320" Height="100%">
                            <telerik:RadTreeView ID="radTreeFolder" runat="server" Width="100%" Height="100%" ShowLineImages="False"
                                OnNodeEdit="radTreeFolder_NodeEdit"
                                OnNodeClick="radTreeFolder_NodeClick"
                                OnNodeDataBound="radTreeFolder_OnNodeDataBound"
                                OnNodeExpand="radTreeFolder_NodeExpand"
                                OnClientNodeClicking="onNodeClicking">
                                <%--  <ContextMenus>
                                            <telerik:RadTreeViewContextMenu ID="MainContextMenu" runat="server">
                                                <Items>
                                                    <telerik:RadMenuItem Value="New" Text="New folder" ImageUrl="Images/addfolder.png" />
                                                    <telerik:RadMenuItem Value="Rename" Text="Rename" ImageUrl="Images/rename.png" />
                                                    <telerik:RadMenuItem Value="Delete" Text="Delete" ImageUrl="Images/deletefolder.png" />
                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                    <%--<telerik:RadMenuItem Value="MoveFolder" Text="Move" ImageUrl="Images/move.png"/>-
                                                    <telerik:RadMenuItem Value="Permission" Visible="true" Text="Permission" ImageUrl="Images/datap.png" />
                                                    <telerik:RadMenuItem IsSeparator="True" />
                                                    <%--<telerik:RadMenuItem Value="FolderType" Visible="true" Text="Folder Type" ImageUrl="Images/datap.png" />-
                                                    <telerik:RadMenuItem Value="FolderPropertises" Visible="true" Text="Folder Propertises" ImageUrl="Images/datap.png" />
                                                    <%--<telerik:RadMenuItem Value="CopyPath" Visible="true" Text="Copy File Path" ImageUrl="Images/datap.png" />-
                                                </Items>
                                            </telerik:RadTreeViewContextMenu>
                                        </ContextMenus>--%>
                                <DataBindings>
                                    <telerik:RadTreeNodeBinding Expanded="false"></telerik:RadTreeNodeBinding>
                                </DataBindings>
                            </telerik:RadTreeView>
                        </telerik:RadSlidingPane>
                    </telerik:RadSlidingZone>
                </telerik:RadPane>
                <telerik:RadSplitBar ID="Radsplitbar1" runat="server">
                </telerik:RadSplitBar>
                <telerik:RadPane ID="Radpane6" runat="server" Scrolling="None">
                    <telerik:RadSplitter ID="Radsplitter1" runat="server" Orientation="Horizontal">
                        <telerik:RadPane ID="Radpane4" runat="server" Height="30px" Scrolling="None">
                            <%--<telerik:RadToolBar ID="radMenuFile" runat="server" Height="30px" Width="100%" Enabled="false">
                                <Items>
                                    <telerik:RadToolBarButton runat="server" Value="ShowAll">
                                        <ItemTemplate>
                                            <telerik:RadButton ID="btnEdit" runat="server" OnClientClicked="ShowEditForm" Width="30px" ToolTip="Edit File" Style="text-align: center; margin-left: 5px;">
                                                <Icon PrimaryIconUrl="~/Images/edit.png" PrimaryIconLeft="3" PrimaryIconTop="2" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                            <telerik:RadButton ID="btnFileDelete" runat="server" OnClientClicking="StandardConfirm" OnClick="btnFileDelete_Click" Width="30px" ToolTip="Delete File" Style="text-align: center; margin-left: 5px;">
                                                <Icon PrimaryIconUrl="~/Images/delete.png" PrimaryIconLeft="3" PrimaryIconTop="2" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                            <telerik:RadButton ID="btnFileDownload" runat="server" OnClientClicked="FileDownload" Width="30px" ToolTip="Download File" Style="text-align: center; margin-left: 5px;">
                                                <Icon PrimaryIconUrl="~/Images/download.png" PrimaryIconLeft="3" PrimaryIconTop="2" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                            <telerik:RadButton ID="btnFileView" runat="server" OnClientClicked="FileView" Width="30px" ToolTip="View File" Style="text-align: center; margin-left: 5px;">
                                                <Icon PrimaryIconUrl="~/Images/expview.png" PrimaryIconLeft="3" PrimaryIconTop="2" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                            <telerik:RadButton ID="btnFileCheckout" runat="server" OnClientClicked="FileCheckout" OnClick="btnFileCheckout_Click" Width="30px" ToolTip="Checkout" Visible="false" Style="text-align: center; margin-left: 5px;">
                                                <Icon PrimaryIconUrl="~/Images/checkout.png" PrimaryIconLeft="3" PrimaryIconTop="2" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                            <telerik:RadButton ID="btnFileCheckin" runat="server" OnClientClicked="FileCheckin" Width="30px" ToolTip="Checkin" Visible="false" Style="text-align: center; margin-left: 5px;">
                                                <Icon PrimaryIconUrl="~/Images/checkin.png" PrimaryIconLeft="3" PrimaryIconTop="2" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                            <telerik:RadButton ID="btnFileRevisionHistory" runat="server" OnClientClicked="FileRevisionHistory" Width="30px" ToolTip="Revision History" Visible="false" Style="text-align: center; margin-left: 5px;">
                                                <Icon PrimaryIconUrl="~/Images/revision.png" PrimaryIconLeft="3" PrimaryIconTop="2" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                            <telerik:RadButton ID="btnFileDuplicateRevision" runat="server" OnClientClicked="FileDuplicateRevision" Width="30px" ToolTip="Retrieve Duplicate Revision" Visible="false" Style="text-align: center; margin-left: 5px;">
                                                <Icon PrimaryIconUrl="~/Images/retrieve.png" PrimaryIconLeft="3" PrimaryIconTop="2" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                            <telerik:RadButton ID="btnGotoFolder" runat="server" OnClick="btnGotoFolder_Click" Width="30px" ToolTip="Go to Folder" Visible="false" Style="text-align: center; margin-left: 5px;">
                                                <Icon PrimaryIconUrl="~/Images/folderdir16.png" PrimaryIconLeft="3" PrimaryIconTop="2" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                            <telerik:RadToolBarButton runat="server" IsSeparator="true" Visible="false" />
                                            <telerik:RadButton ID="filepermission" runat="server" OnClientClicked="PermissionDocument" Width="30px" ToolTip="Change File Permission" Visible="false" Style="text-align: center; margin-left: 5px;">
                                                <Icon PrimaryIconUrl="~/Images/datap.png" PrimaryIconLeft="3" PrimaryIconTop="2" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                        </ItemTemplate>
                                    </telerik:RadToolBarButton>
                                </Items>
                            </telerik:RadToolBar>--%>
                            <telerik:RadToolBar ID="CustomerMenu" runat="server" Width="100%" OnClientButtonClicking="OnClientButtonClicking">
                                <Items>
                                    <telerik:RadToolBarDropDown runat="server" Text="New" ImageUrl="~/Images/addNew.png">
                                        <Buttons>
                                            <telerik:RadToolBarButton runat="server" Text="Document" Value="1" ImageUrl="~/Images/addDocument.png"></telerik:RadToolBarButton>
                                            <telerik:RadToolBarButton runat="server" Text="Multi documents" Value="2" ImageUrl="~/Images/addmulti.png"></telerik:RadToolBarButton>
                                            <telerik:RadToolBarButton runat="server" Text="Upload folder" Value="3" ImageUrl="~/Images/upload.png" Visible="false" />
                                        </Buttons>
                                    </telerik:RadToolBarDropDown>
                                    <telerik:RadToolBarButton runat="server" IsSeparator="true" Visible="false" />
                                    <telerik:RadToolBarDropDown runat="server" Text="Action" ImageUrl="~/Images/action.png">
                                        <Buttons>
                                            <telerik:RadToolBarButton runat="server" Text="Send notifications" Value="3" ImageUrl="~/Images/sendnotification.png" Visible="false"></telerik:RadToolBarButton>
                                            <telerik:RadToolBarButton runat="server" Text="Send mail with Attachment" Value="3" ImageUrl="~/Images/email.png"></telerik:RadToolBarButton>
                                            <telerik:RadToolBarButton runat="server" Text="Send mail with Link System" Value="3" ImageUrl="~/Images/email.png"></telerik:RadToolBarButton>
                                            <telerik:RadToolBarButton runat="server" IsSeparator="true" Visible="false" />
                                            <telerik:RadToolBarButton runat="server" Text="Export data" Value="5" ImageUrl="~/Images/exexcel.png" />
                                            <telerik:RadToolBarButton runat="server" Text="Import data" Value="5" ImageUrl="~/Images/import.png" />
                                            <telerik:RadToolBarButton runat="server" IsSeparator="true" Visible="false" />
                                            <telerik:RadToolBarButton runat="server" Text="Move files" Value="5" ImageUrl="~/Images/Move.png" Visible="true" />
                                             <telerik:RadToolBarButton runat="server" Text="Delete multi documents" Value="3" ImageUrl="~/Images/delete.png" />
                                            <telerik:RadToolBarButton runat="server" Text="View explorer" Value="4" ImageUrl="~/Images/expview.png" Visible="false" />
                                            <telerik:RadToolBarButton runat="server" IsSeparator="true" />
                                            <telerik:RadToolBarButton runat="server" Text="Checkout selected documents" Value="8" ImageUrl="~/Images/checkout.png" Visible="false" />
                                            <telerik:RadToolBarButton runat="server" Text="Checkin selected documents" Value="8" ImageUrl="~/Images/checkin.png" Visible="false" />
                                            <telerik:RadToolBarButton runat="server" Text="Download multi documents" Value="3" ImageUrl="~/Images/download.png" />
                                            <telerik:RadToolBarButton runat="server" IsSeparator="true" />
                                            <telerik:RadToolBarButton runat="server" Text="Import data file" Value="7" ImageUrl="~/Images/import.png" Visible="false" />
                                            <telerik:RadToolBarButton runat="server" Text="filesize" Value="1" Visible="false" />
                                        </Buttons>
                                    </telerik:RadToolBarDropDown>
                                    <%--<telerik:RadToolBarButton CssClass="searchBox">
                                        <ItemTemplate>
                                            <span>Search </span>
                                            <telerik:RadSearchBox ID="txtSearch" runat="server" Width="300px" EmptyMessage="Type here" EnableAutoComplete="false" OnSearch="txtSearch_Search"></telerik:RadSearchBox>
                                        </ItemTemplate>
                                    </telerik:RadToolBarButton>
                                    <telerik:RadToolBarButton runat="server" IsSeparator="true" />
                                    <telerik:RadToolBarButton runat="server">
                                        <ItemTemplate>
                                            <telerik:RadButton ID="btnAdvanceSearch" runat="server" OnClick="btnAdvanceSearch_Click" Width="120px" Text="Advance Search" ToolTip="Advance Search" Style="text-align: right; margin-left: 5px;">
                                                <Icon PrimaryIconUrl="~/Images/search.gif" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16" />
                                            </telerik:RadButton>
                                        </ItemTemplate>
                                    </telerik:RadToolBarButton>--%>
                                </Items>
                            </telerik:RadToolBar>
                            </telerik:RadPane>
                        <telerik:RadPane ID="RadPane3" runat="server" Scrolling="None">
                            <telerik:RadGrid ID="grdDocument" runat="server" AllowPaging="True"
                                AutoGenerateColumns="False" CellPadding="0" CellSpacing="0"
                                GridLines="None" Height="100%" AllowMultiRowSelection="True"
                                OnDeleteCommand="grdDocument_DeleteCommand"
                                OnNeedDataSource="grdDocument_OnNeedDataSource"
                                OnItemCommand="grdDocument_ItemCommand"
                                OnItemDataBound="grdDocument_ItemDataBound"
                                OnPreRender="grdDocument_PreRender"
                                PageSize="100" Style="outline: none" AllowFilteringByColumn="true">
                                <GroupingSettings CaseSensitive="False"></GroupingSettings>
                                <MasterTableView
                                    ClientDataKeyNames="ID" DataKeyNames="ID" CommandItemDisplay="Top" EditMode="InPlace" Font-Size="8pt">
                                    <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Refresh Data" ShowExportToExcelButton="false" />
                                    <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; Documents." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <%--<GroupByExpressions>
                                            <telerik:GridGroupByExpression>
                                                <SelectFields>
                                                    <telerik:GridGroupByField FieldName="DirName"></telerik:GridGroupByField>
                                                </SelectFields>
                                                <GroupByFields>
                                                    <telerik:GridGroupByField FieldName="DirName"></telerik:GridGroupByField>
                                                </GroupByFields>
                                            </telerik:GridGroupByExpression>
                                        </GroupByExpressions>--%>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />
                                        <telerik:GridBoundColumn DataField="DisciplineID" UniqueName="DisciplineID" Visible="True" Display="False" />
                                        <telerik:GridBoundColumn DataField="IsCheckOut" UniqueName="IsCheckOut" Visible="True" Display="False" />
                                        <telerik:GridBoundColumn DataField="IsFolder" UniqueName="IsFolder" Display="False" />
                                        <telerik:GridBoundColumn DataField="FilePath" UniqueName="FilePath" Display="False" />
                                        <telerik:GridBoundColumn DataField="FileExtensionIcon" UniqueName="FileExtensionIcon" Display="False" />
                                        <telerik:GridBoundColumn DataField="Name" UniqueName="FileName" Display="False" />
                                        <telerik:GridBoundColumn DataField="CurrentCheckoutByRole" UniqueName="CurrentCheckoutByRole" Visible="True" Display="False" />
                                        <telerik:GridClientSelectColumn UniqueName="IsSelected">
                                            <HeaderStyle Width="24" />
                                            <ItemStyle HorizontalAlign="Center" Width="24" />
                                        </telerik:GridClientSelectColumn>
                                        <telerik:GridTemplateColumn AllowFiltering="False" UniqueName="EditColumn">
                                            <HeaderStyle Width="30px" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/lock1.png" ToolTip="Check out" Style="cursor: pointer;" Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsCheckOut")) %>' />
                                                <a href='javascript:ShowEditForm(<%# DataBinder.Eval(Container.DataItem, "ID") %>)' style="text-decoration: none; color: blue">
                                                    <asp:Image ID="EditLink" runat="server" ImageUrl="~/Images/edit.png" ToolTip="Edit File" Style="cursor: pointer;" AlternateText="Edit properties" Visible='<%# (!Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsCheckOut"))&&!Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsFolder"))) %>' />
                                                </a>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn AllowFiltering="False" UniqueName="DeleteColumn">
                                            <HeaderStyle Width="30px" HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton runat="server" Style="padding-left: 7px; cursor: pointer;" ID="btnDelete"
                                                    ToolTip="Delete File"
                                                    ImageUrl="~/Images/delete.png" CommandName="Delete"
                                                    OnClientClick="return confirm('Do you want to delete document?');" Visible='<%# (!Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsCheckOut"))&&!Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsFolder"))) %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn AllowFiltering="false" UniqueName="DownloadColumn">
                                            <HeaderStyle Width="30px" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <div runat="server" id="ViewForFolder" visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsFolder")) %>'>
                                                    <asp:Image ID="Image2" runat="server"
                                                        ImageUrl='<%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon") %>' />
                                                </div>
                                                <div runat="server" id="ViewForDoc" visible='<%# (!Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsFolder"))) %>'>
                                                    <div runat="server" id="DocumentFile" visible='<%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon")!=null %>'>
                                                        <a download='<%# DataBinder.Eval(Container.DataItem, "Name") %>' href='<%# DataBinder.Eval(Container.DataItem, "FilePath") %>' target="_blank">
                                                            <asp:Image ID="CallLink" runat="server" ImageUrl=' <%# DataBinder.Eval(Container.DataItem, "FileExtensionIcon") %>'
                                                            ToolTip="Download File" Style="cursor: pointer;" AlternateText="Download document" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn AllowFiltering="False" UniqueName="Viewfile">
                                            <HeaderStyle Width="30px" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <div runat="server" id="ViewFilePDF" visible='<%# (!Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsFolder"))) %>' >
                                                    <a href='javascript:FileView(<%# DataBinder.Eval(Container.DataItem, "ID") %>)' style="text-decoration: none; color: blue">
                                                        <asp:Image ID="CallLink2" runat="server" ImageUrl="~/Images/expview.png" Style="cursor: pointer; border-color: transparent" AlternateText="Viewer File PDF" ToolTip="View file" visible='<%# !Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsFolder")) && DataBinder.Eval(Container.DataItem,"FileExtension").ToString().ToLower()!= "exe" &&
                                        DataBinder.Eval(Container.DataItem,"FileExtension").ToString().ToLower()!="dwg" &&
                                        DataBinder.Eval(Container.DataItem,"FileExtension").ToString().ToLower()!="rar" &&
                                        DataBinder.Eval(Container.DataItem,"FileExtension").ToString().ToLower()!="inf" &&
                                        DataBinder.Eval(Container.DataItem,"FileExtension").ToString().ToLower()!="zip" &&
                                        DataBinder.Eval(Container.DataItem,"FileExtension").ToString().ToLower()!="zip"%>'/>
                                                        <a />
                                                </div>
                                               <%-- <div runat="server" id="VideoFile" visible='<%# (DataBinder.Eval(Container.DataItem, "FileExtensionIcon")!=null &&  DataBinder.Eval(Container.DataItem, "FileExtensionIcon").ToString() == "images/video.png" && !Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsCheckOut"))) %>'>
                                                    <a href='javascript:ShowVideoForm(<%# DataBinder.Eval(Container.DataItem, "ID") %>)' style="text-decoration: none; color: blue">
                                                        <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/expview.png" Style="cursor: pointer; border-color: transparent" AlternateText="Viewer Video" ToolTip="View Video" />
                                                    </a>
                                                </div>
                                                <div runat="server" id="PictureFile" visible='<%# (DataBinder.Eval(Container.DataItem, "FileExtensionIcon")!=null && DataBinder.Eval(Container.DataItem, "FileExtensionIcon").ToString() == "images/picture.png" && !Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsCheckOut"))) %>'>
                                                    <a href='javascript:ShowImageForm(<%# DataBinder.Eval(Container.DataItem, "ID") %>)'>
                                                        <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/expview.png" Style="cursor: pointer; border-color: transparent" AlternateText='<%# DataBinder.Eval(Container.DataItem, "Title") %>' ToolTip="View Image" />
                                                    </a>
                                                </div>--%>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn UniqueName="Name" HeaderText="File Name" AllowFiltering="true"
                                            DataField="Name" ShowFilterIcon="False" FilterControlWidth="97%" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle Width="400" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <div runat="server" id="ViewNameForFolder" visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsFolder")) %>'>
                                                  
                                                        <asp:Label ID="Label2" ForeColor="Blue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>'></asp:Label>
                                               
                                                </div>
                                                <div runat="server" id="ViewNameForDoc" visible='<%# !Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsFolder")) %>'>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>'></asp:Label>
                                                    <asp:Image ID="newicon" runat="server" ImageUrl="Images/new.png" Visible='<%# (DateTime.Now - Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "CreatedDate"))).TotalHours < 24 %>' />
                                                </div>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="DocumentNumber" HeaderText="Document Number" UniqueName="Index1" AllowFiltering="true"
                                            FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="200" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Title" HeaderText="Title" UniqueName="Index2" AllowFiltering="true"
                                            FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="300" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="RevisionName" HeaderText="Rev" UniqueName="Index3" AllowFiltering="true"
                                            FilterControlWidth="95%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center" Width="200" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Date" HeaderText="Date" UniqueName="Index4" AllowFiltering="true"
                                            FilterControlWidth="95%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center" Width="200" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="FromTo" HeaderText="From/To" UniqueName="Index5" AllowFiltering="true"
                                            FilterControlWidth="95%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center" Width="200" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="DocumentTypeName" HeaderText="Document Type" UniqueName="Index6" AllowFiltering="true"
                                            FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="150" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="DisciplineName" HeaderText="Discipline" UniqueName="Index7" AllowFiltering="true"
                                            FilterControlWidth="95%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="200" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Platform" HeaderText="Platform" UniqueName="Index8" AllowFiltering="true"
                                            FilterControlWidth="95%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center" Width="200" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Contractor" HeaderText="Contractor" UniqueName="Index10" AllowFiltering="true" FilterControlWidth="95%" ShowFilterIcon="false" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="200"/>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </telerik:GridBoundColumn>
                                         <telerik:GridBoundColumn DataField="Tag" HeaderText="Tag" UniqueName="Index11" AllowFiltering="true" FilterControlWidth="95%" ShowFilterIcon="false" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="200"/>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Remark" HeaderText="Remark" UniqueName="Index9" AllowFiltering="true"
                                            FilterControlWidth="97%" ShowFilterIcon="False" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="150" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings Selecting-AllowRowSelect="true" AllowColumnHide="True" EnablePostBackOnRowClick="true">
                                    <ClientEvents OnRowClick="RowClick" OnRowSelected="RowSelected" OnGridCreated="GetGridObject" />
                                    <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="500" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </telerik:RadPane>
                        <telerik:RadSplitBar ID="Radsplitbar2" runat="server" Visible="false">
                        </telerik:RadSplitBar>
                        <telerik:RadPane ID="RadPane5" runat="server" Height="10%" Visible="false">
                            <asp:Panel ID="pnFolder" runat="server">
                                <table class="tableProperties">
                                    <tr>
                                        <th class="thProperties" style="width: 15%">Property name</th>
                                        <th class="thProperties" style="width: 35%">Property value</th>
                                        <th class="thProperties" style="width: 15%">Property name</th>
                                        <th class="thProperties" style="width: 35%">Property value</th>
                                    </tr>
                                    <tr>
                                        <td class="tdProperties" style="font-weight: bold">Created by</td>
                                        <td class="tdProperties">
                                            <asp:Label ID="lblCreatedBy" runat="server" Text="-"></asp:Label></td>
                                        <td class="tdProperties" style="font-weight: bold">Updated by</td>
                                        <td class="tdProperties">
                                            <asp:Label ID="lblUpdatedBy" runat="server" Text="-"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="tdProperties" style="font-weight: bold">Created time</td>
                                        <td class="tdProperties">
                                            <asp:Label ID="lblCreatedTime" runat="server" Text="-"></asp:Label></td>
                                        <td class="tdProperties" style="font-weight: bold">Updated time</td>
                                        <td class="tdProperties">
                                            <asp:Label ID="lblUpdatedTime" runat="server" Text="-"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="tdProperties" style="font-weight: bold">Disk Usage</td>
                                        <td class="tdProperties">
                                            <asp:Label ID="lblDiskUsage" runat="server" Text="-"></asp:Label></td>
                                        <td class="tdProperties" style="font-weight: bold">
                                            <asp:Label ID="lblDocumentCount" runat="server" Text="Document Count"></asp:Label></td>
                                        <td class="tdProperties">
                                            <asp:Label ID="txtDocumentCount" runat="server" Text="-"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="tdProperties" style="font-weight: bold">
                                            <asp:Label ID="lblParentFolder" runat="server" Text="Parent Folder"></asp:Label></td>
                                        <td class="tdProperties">
                                            <asp:Label ID="txtParentFolder" runat="server" Text="-"></asp:Label></td>
                                        <td class="tdProperties" style="font-weight: bold">
                                            <asp:Label ID="lblFolderCount" runat="server" Text="Folder Count"></asp:Label></td>
                                        <td class="tdProperties">
                                            <asp:Label ID="txtFolderCount" runat="server" Text="-"></asp:Label></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </telerik:RadPane>
                    </telerik:RadSplitter>
                </telerik:RadPane>
            </telerik:RadSplitter>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <div>
        <img src="Images/ezgif-4.gif" id="loading" alt="loading" style="display: none;" />
    </div>
    <%--<telerik:RadContextMenu ID="radMenu" runat="server"
        EnableRoundedCorners="true" EnableShadows="true" OnClientItemClicking="gridMenuClicking" OnClientShowing="gridContextMenuShowing">
        <Items>
            <telerik:RadMenuItem Text="Edit properties" ImageUrl="~/Images/edit.png" Value="EditDocument" />
            <telerik:RadMenuItem IsSeparator="True" />
            <telerik:RadMenuItem Text="Version history" ImageUrl="~/Images/history.png" Value="VersionHistory" />
            <telerik:RadMenuItem Text="Checkin/out history" ImageUrl="~/Images/lock3.png" Value="CheckOutInHistory" />
            <telerik:RadMenuItem IsSeparator="True" Visible="False" />
            <telerik:RadMenuItem Text="Check out" ImageUrl="~/Images/checkout.png" Value="CheckOut" />
            <telerik:RadMenuItem Text="Check in" ImageUrl="~/Images/checkin.png" Value="CheckIn" />
            <telerik:RadMenuItem IsSeparator="True" />
            <telerik:RadMenuItem Text="Permission" ImageUrl="~/Images/datap.png" Value="Permission" />
            <telerik:RadMenuItem Text="Attach File" ImageUrl="~/Images/revision.png" Value="AttachFile" />
            <telerik:RadMenuItem Text="Check out" ImageUrl="~/Images/checkout.png" Value="CheckOut" />
            <telerik:RadMenuItem Text="Check in" ImageUrl="~/Images/checkin.png" Value="CheckIn" />
            <telerik:RadMenuItem IsSeparator="True" />
            <telerik:RadMenuItem Text="Revision history" ImageUrl="~/Images/revision.png" Value="RevisionHistory" />
            <telerik:RadMenuItem Text="Retrieve Duplicate Revision" ImageUrl="~/Images/retrieve.png" Value="RetrieveDoc" />
        </Items>
    </telerik:RadContextMenu>--%>
    <telerik:RadNotification RenderMode="Lightweight" Position="Center" EnableRoundedCorners="true" EnableShadow="true" Skin="Silk" ID="RadNotification1" runat="server" Width="300" Height="120" Animation="Fade">
        <ContentTemplate>
            <div style="margin: 5px 0 0 10px">
                <asp:Literal ID="lblNotification" runat="server"></asp:Literal>
            </div>
        </ContentTemplate>
    </telerik:RadNotification>
    <span style="display: none">
        <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <ClientEvents OnRequestStart="onRequestStart"></ClientEvents>
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="radTreeFolder" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="RadNotification1" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="CustomerMenu">
                    <UpdatedControls>
                       <%-- <telerik:AjaxUpdatedControl ControlID="radTreeFolder"></telerik:AjaxUpdatedControl>--%>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="radTreeFolder">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="radTreeFolder" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="RadNotification1" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="radMenuFolder" />
                        <telerik:AjaxUpdatedControl ControlID="CustomerMenu" />
                        <%--<telerik:AjaxUpdatedControl ControlID="pnFolder" LoadingPanelID="RadAjaxLoadingPanel2" />--%>
                        <telerik:AjaxUpdatedControl ControlID="lbUserId" />
                        <telerik:AjaxUpdatedControl ControlID="lbRoleId" />
                        <telerik:AjaxUpdatedControl ControlID="lbFolderSize" />
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="radPbCategories">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="radTreeFolder" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="radMenuFolder">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="radTreeFolder" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="grdDocument">
                    <UpdatedControls>
                       <%-- <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2" />--%>
                        <telerik:AjaxUpdatedControl ControlID="radMenuFile" />
                        <%--<telerik:AjaxUpdatedControl ControlID="pnFolder" LoadingPanelID="RadAjaxLoadingPanel2" />--%>
                       <%-- <telerik:AjaxUpdatedControl ControlID="radTreeFolder" LoadingPanelID="RadAjaxLoadingPanel2" />--%>
                        <telerik:AjaxUpdatedControl ControlID="radMenuFolder" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="radMenuFile">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadNotification1" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2" />
                        <telerik:AjaxUpdatedControl ControlID="radTreeFolder" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <%--<telerik:AjaxSetting AjaxControlID="radViewSettingBar">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="SlidingZone1" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>--%>
                <telerik:AjaxSetting AjaxControlID="txtSearch">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnAdvanceSearch">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
    </span>

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="CustomerDialog" runat="server" Title="Document Information"
                VisibleStatusbar="false" Height="500" Width="650"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="ViewerFile" runat="server" Title="Viewer File"
                VisibleStatusbar="false" Height="630" Width="1250" MaxHeight="630" MaxWidth="1250"
                Left="50px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="VideoView" runat="server" Title="Video Viewer" OnClientBeforeClose="OnClientBeforeClose"
                VisibleStatusbar="false" Height="550" Width="800" MaxHeight="650" MaxWidth="950"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true" OnClientShow="OnClientShow">
            </telerik:RadWindow>
            <telerik:RadWindow ID="ImageView" runat="server" Title="Image Viewer" VisibleStatusbar="false" Height="550" Width="800" MaxHeight="550" MaxWidth="800"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true" OnClientShow="OnClientShow">
            </telerik:RadWindow>
            <telerik:RadWindow ID="CheckOut" runat="server" Title="Checkout Document"
                VisibleStatusbar="false" Height="250" Width="650"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="MoveFolder" runat="server" Title="Move Files" OnClientClose="ReloadWindow"
                VisibleStatusbar="false" Height="500" Width="650"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="FolderPermission" runat="server" Title="Folder Permission"
                VisibleStatusbar="false" Height="650" Width="950"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="DocPermission" runat="server" Title="Document Permission"
                VisibleStatusbar="false" Height="650" Width="950"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="UploadMulti" runat="server" Title="Create multiple documents"
                VisibleStatusbar="true" Height="520" MinHeight="520" MaxHeight="520" Width="640" MinWidth="640" MaxWidth="640"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true" OnClientClose="refreshGrid">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RevisionDialog" runat="server" Title="Revision history" OnClientClose="refreshGrid"
                VisibleStatusbar="false" Height="600" Width="1250" MinHeight="600" MinWidth="1250" MaxHeight="600" MaxWidth="1250"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="AttachFileDialog" runat="server" Title="Attach File" OnClientClose="refreshGrid"
                VisibleStatusbar="false" Height="600" Width="1250" MinHeight="600" MinWidth="1250" MaxHeight="600" MaxWidth="1250"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="SendMail" runat="server" Title="Send mail"
                VisibleStatusbar="false" Height="560" Width="992" MinHeight="560" MinWidth="992" MaxHeight="560" MaxWidth="992"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="UploadFodler" runat="server" Title="Uploadfolder" OnClientClose="refreshGridAndFolder"
                VisibleStatusbar="false" Height="560" Width="992" MinHeight="560" MinWidth="992" MaxHeight="560" MaxWidth="992"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="DownloadFolder" runat="server" Title="Download Folder" Style="z-index: 100001;"
                VisibleStatusbar="false" Height="150" ReloadOnShow="true" ShowContentDuringLoad="false" Width="250" Modal="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="VersionHistory" runat="server" Title="Checkout/in history"
                VisibleStatusbar="false" Height="500" Width="1000" MinHeight="500" MinWidth="1000" MaxHeight="500" MaxWidth="1000"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="ImportData" runat="server" Title="Upload data file"
                VisibleStatusbar="false" Height="350" Width="480"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="ExportData" runat="server" Title="Export master list"
                VisibleStatusbar="false" Height="350" Width="480"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="CopyPath" runat="server" Title="Copy to Clipboard"
                VisibleStatusbar="false" Height="130" Width="480"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="CheckIn" runat="server" Title="Check in document"
                VisibleStatusbar="false" Height="350" Width="480"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
            <telerik:RadWindow ID="AdvanceSearch" runat="server" Title="Advance Search"
                VisibleStatusbar="false" Height="500" Width="800"
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1" />
    <asp:HiddenField runat="server" ID="IsAdminGroup" />
    <asp:HiddenField runat="server" ID="IsUpdatePermission" />
    <asp:HiddenField runat="server" ID="IsUpdateFolder" />
    <asp:HiddenField runat="server" ID="lblFolderId" />
    <asp:HiddenField runat="server" ID="lblDocId" />
    <asp:HiddenField runat="server" ID="lblCategoryId" />
    <asp:HiddenField runat="server" ID="isCheckOut" />
    <asp:HiddenField runat="server" ID="filePath" />
    <asp:HiddenField runat="server" ID="fileName" />
    <asp:HiddenField runat="server" ID="fileExtensionIcon" />
    <asp:HiddenField runat="server" ID="isFolder" />
    <asp:HiddenField runat="server" ID="CurrentRoleId" />
    <asp:HiddenField runat="server" ID="CurrentCheckoutRoleId" />
    <asp:HiddenField runat="server" ID="lbUserId" />
    <asp:HiddenField runat="server" ID="lbRoleId" />
    <asp:HiddenField runat="server" ID="lbFolderSize" />
    <input type="hidden" id="radGridClickedRowIndex" name="radGridClickedRowIndex" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <%--<script src="Scripts/jquery-1.7.1.js"></script>--%>
        <script type="text/javascript">
            function CloseImageLoading() {
                document.getElementById("loading").style.display = "none";
            }
            $(document).ready(function () {
                $("a#example7").fancybox({
                    'titlePosition': 'inside'
                });
            });

            //$(document).ready(function(){
            //    $('#btDownloadfolder').click(function () {
            //        $('#fader').css('display', 'block');
            //    });
            //});
        </script>
        <script type="text/javascript">
            //Standard Window.confirm
            function StandardConfirm(sender, args) {
                args.set_cancel(!window.confirm("Do you want delete file?"));
            }
            function setCustomPosition(sender, args) {
                sender.moveTo(sender.get_left(), sender.get_top());
            }
            var radDocuments;
            function ShowFilter(obj) {
                if (obj.checked) {
                    $find('<%=grdDocument.ClientID %>').get_masterTableView().showFilterItem();
                } else {
                    $find('<%=grdDocument.ClientID %>').get_masterTableView().hideFilterItem();
                }
            }

            function ReloadWindow() {
                window.location.reload();
            }
            function refreshGrid() {
                var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView();
                masterTable.rebind();
            }

            function refreshTreeFolder() {
                ajaxManager.ajaxRequest("RebindTreeFolder");
            }

            function ExportGrid() {
                var masterTable = $find("<%=grdDocument.ClientID %>").get_masterTableView();
                masterTable.exportToExcel('PIN_list.xls');
                return false;
            }

            function GetGridObject(sender, eventArgs) {
                radDocuments = sender;
            }

            function onRequestStart(sender, args) {
                //alert(args.get_eventArgument());
                if (args.get_eventArgument().indexOf("DownloadMulti") >= 0 || args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("btDownloadfolder") >= 0 || args.get_eventArgument().indexOf("ExportData") >= 0) {
                    args.set_enableAjax(false);

                }
            }

            function onColumnHidden(sender) {

                var masterTableView = sender.get_masterTableView().get_element();
                masterTableView.style.tableLayout = "auto";
                //window.setTimeout(function () { masterTableView.style.tableLayout = "auto"; }, 0);
            }

            // Undocked and Docked event slide bar Tree folder
            function OnClientUndocked(sender, args) {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;

                ////radDocuments.get_masterTableView().showColumn(9);
                radDocuments.get_masterTableView().showColumn(10);
                radDocuments.get_masterTableView().showColumn(11);
                radDocuments.get_masterTableView().showColumn(12);

                radDocuments.get_masterTableView().showColumn(14);
                radDocuments.get_masterTableView().showColumn(15);
                radDocuments.get_masterTableView().showColumn(16);
                //radDocuments.get_masterTableView().showColumn(16);
                radDocuments.get_masterTableView().showColumn(17);


                ////if (selectedFolder != "") {
                ////    ajaxManager.ajaxRequest("ListAllDocuments");
                ////}
            }

            function OnClientDocked(sender, args) {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;

                ////radDocuments.get_masterTableView().hideColumn(9);
                radDocuments.get_masterTableView().hideColumn(10);
                radDocuments.get_masterTableView().hideColumn(11);
                radDocuments.get_masterTableView().hideColumn(12);

                radDocuments.get_masterTableView().hideColumn(14);
                radDocuments.get_masterTableView().hideColumn(15);
                radDocuments.get_masterTableView().hideColumn(16);
                //radDocuments.get_masterTableView().hideColumn(16);
                radDocuments.get_masterTableView().hideColumn(17);

            }
            function ShowAdvanceSearch() {
                var owd = $find("<%= AdvanceSearch.ClientID%>");
                owd.Show();
                owd.setUrl("SearchContain.aspx");
            }
            function RowClick(sender, eventArgs) {
                var Id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblDocId.ClientID %>").value = Id;
                var grid = sender;
                var MasterTable = grid.get_masterTableView();
                var row = MasterTable.get_dataItems()[eventArgs.get_itemIndexHierarchical()];
                var cell = MasterTable.getCellByColumnUniqueName(row, "IsCheckOut");

                document.getElementById("<%= isCheckOut.ClientID %>").value = cell.innerHTML == "&nbsp;" ? false : cell.innerHTML;

                cell = MasterTable.getCellByColumnUniqueName(row, "IsFolder");
                document.getElementById("<%= isFolder.ClientID %>").value = cell.innerHTML;

                cell = MasterTable.getCellByColumnUniqueName(row, "FileName");
                document.getElementById("<%= fileName.ClientID %>").value = cell.innerHTML;

                cell = MasterTable.getCellByColumnUniqueName(row, "FilePath");
                document.getElementById("<%= filePath.ClientID %>").value = cell.innerHTML;

                cell = MasterTable.getCellByColumnUniqueName(row, "FileExtensionIcon");
                document.getElementById("<%= fileExtensionIcon.ClientID %>").value = cell.innerHTML;
                //ajaxManager.ajaxRequest("RowClick_" + Id);
                //var isfolder = MasterTable.getCellByColumnUniqueName(row, "IsFolder").innerHTML;
                ////alert(isfolder);
                //if(isfolder==="True")
                //{
                //    ajaxManager.ajaxRequest("ShowChildFolder_" + Id);
                //    //alert(Id);
                //}
            }

            function RowSelected(sender, eventArgs) {
                var Id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblDocId.ClientID %>").value = Id;
                var grid = sender;
                var MasterTable = grid.get_masterTableView();
                var row = MasterTable.get_dataItems()[eventArgs.get_itemIndexHierarchical()];
                var cell = MasterTable.getCellByColumnUniqueName(row, "IsCheckOut");

                document.getElementById("<%= isCheckOut.ClientID %>").value = cell.innerHTML == "&nbsp;" ? false : cell.innerHTML;

                cell = MasterTable.getCellByColumnUniqueName(row, "IsFolder");
                document.getElementById("<%= isFolder.ClientID %>").value = cell.innerHTML;

                cell = MasterTable.getCellByColumnUniqueName(row, "FileName");
                document.getElementById("<%= fileName.ClientID %>").value = cell.innerHTML;

                cell = MasterTable.getCellByColumnUniqueName(row, "FilePath");
                document.getElementById("<%= filePath.ClientID %>").value = cell.innerHTML;

                cell = MasterTable.getCellByColumnUniqueName(row, "FileExtensionIcon");
                document.getElementById("<%= fileExtensionIcon.ClientID %>").value = cell.innerHTML;
                //ajaxManager.ajaxRequest("RowClick_" + Id);
            }

            function gridMenuClicking(sender, args) {
                var itemValue = args.get_item().get_value();
                var docId = document.getElementById("<%= lblDocId.ClientID %>").value;
                var folderId = document.getElementById("<%= lblFolderId.ClientID %>").value;
                var ischeckout = document.getElementById("<%= isCheckOut.ClientID %>").value;
                if (ischeckout != "True" || itemValue == "CheckIn") {
                    switch (itemValue) {
                        case "RevisionHistory":
                            var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
                            var owd = $find("<%=RevisionDialog.ClientID %>");
                            owd.Show();
                            owd.setUrl("Controls/Document/RevisionHistory.aspx?docId=" + docId + "&categoryId=" + categoryId, "RevisionDialog");
                            break;
                        case "AttachFile":
                            var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
                            var owd = $find("<%= AttachFileDialog.ClientID %>");
                            owd.Show();
                            owd.setUrl("Controls/Document/AttachDocument.aspx?docId=" + docId + "&folderId=" + folderId, "AttachFileDialog");
                            break;
                        case "VersionHistory":
                            var owd = $find("<%=VersionHistory.ClientID %>");
                            owd.Show();
                            owd.setUrl("Controls/Document/VersionHistory.aspx?docId=" + docId, "VersionHistory");
                            break;
                        case "EditDocument":
                            ShowEditForm();
                            break;
                        case "CheckOut":
                            <%--var owd = $find("<%=CheckOut.ClientID %>");
                            owd.Show();
                            owd.setUrl("Controls/Document/CheckOutDocument.aspx?docId=" + docId, "CheckOut");--%>
                            var fileName = document.getElementById("<%= fileName.ClientID %>").value;
                            var filePath = document.getElementById("<%= filePath.ClientID %>").value;
                            var link = document.createElement("a");
                            link.download = fileName;
                            link.href = filePath;
                            link.click();
                            link.remove();
                            ajaxManager.ajaxRequest("CheckoutDoc");
                            break;
                        case "CheckIn":
                            //ajaxManager.ajaxRequest("CheckInDocument");
                            var owd = $find("<%=CheckIn.ClientID %>");
                            owd.Show();
                            owd.setUrl("Controls/Document/Checkin.aspx?docId=" + docId, "CheckIn");
                            break;
                        case "CheckOutInHistory":
                            var owd = $find("<%=VersionHistory.ClientID %>");
                            owd.Show();
                            owd.setUrl("Controls/Document/CheckOutInHistoryList.aspx?docId=" + docId, "VersionHistory");
                            break;
                        case "Permission":
                            var owd = $find("<%=DocPermission.ClientID %>");
                            owd.Show();
                            owd.setUrl("Controls/Document/SpecialDocumentPermission.aspx?docId=" + docId + "&folderId=" + folderId, "DocPermission");
                            break;
                        case "RevisionHistory":
                            var owd = $find("<%=RevisionDialog.ClientID %>");
                            owd.Show();
                            owd.setUrl("Controls/Document/RevisionHistory.aspx?docId=" + docId, "RevisionDialog");
                            break;
                        case "VersionHistory":
                            var owd = $find("<%=VersionHistory.ClientID %>");
                            owd.Show();
                            owd.setUrl("Controls/Document/VersionHistory.aspx?docId=" + docId, "VersionHistory");
                            break;
                        case "RetrieveDoc":
                            var owd = $find("<%=CustomerDialog.ClientID %>");
                            owd.Show();
                            owd.setUrl("Controls/Document/RetrieveDocumentForm.aspx?docId=" + docId, "CustomerDialog");
                            break;
                    }
                }
                else { alert("Document is check out!") }
            }
            function ShowPropertiesFodler() {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                var owd = $find("<%=FolderPermission.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/FolderPropertises.aspx?folderId=" + selectedFolder, "FolderType");
            }
            function ShowChangePermission() {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                var owd = $find("<%=FolderPermission.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/FolderPermissionForm.aspx?folderId=" + selectedFolder, "FolderPermission");
            }

            function MoveFodler() {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
                var owd = $find("<%=MoveFolder.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/MoveFileAndFolder.aspx?mode=1&folderId=" + selectedFolder + "&categoryId=" + categoryId, "MoveFolder");
            }
            function PermissionDocument() {
                var docId = document.getElementById("<%= lblDocId.ClientID %>").value;
                var owd = $find("<%=DocPermission.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/FilePermissionForm.aspx?DocID=" + docId, "DocPermission");
            }
            function FileDownload() {
                var fileName = document.getElementById("<%= fileName.ClientID %>").value;
                var filePath = document.getElementById("<%= filePath.ClientID %>").value;
                var link = document.createElement("a");
                link.download = fileName;
                link.href = filePath;
                link.click();
                link.remove();
            }
            function FileView(docId) {
             <%--   var docId = document.getElementById("<%= lblDocId.ClientID %>").value;--%>

                //Window.open("Viewer.aspx?IndexId=" + docId, "File Viewer", "'toolbar=0, location=0, directories=0, status=0, menubar=1, width=1080, height=700")
                var owd = $find("<%= ViewerFile.ClientID %>");
                owd.Show();
                owd.moveTo(60, 3);
                owd.setUrl("Viewer.aspx?IndexId=" + docId, "ViewerFile");
            }
            function CheckFileExtension(Extension) {
                const extensionDefaul = ["jpg", "jpe", "bmp", "gif", "png", "svg", "AVI", "MP4", "DIVX", "WMV", "MPG", "M1V", "MKV", "pdf", "doc", "docx", "xsl", "xlsx", "psd", "tif", "tiff"];
                if (extensionDefaul.indexOf(Extension)) { return true; }
                else { return false; }
            }
            function FileCheckout() {
                var fileName = document.getElementById("<%= fileName.ClientID %>").value;
                var filePath = document.getElementById("<%= filePath.ClientID %>").value;
                var link = document.createElement("a");
                link.download = fileName;
                link.href = filePath;
                link.click();
                link.remove();
                //ajaxManager.ajaxRequest("CheckoutDoc");
            }
            function FileCheckin() {
                var docId = document.getElementById("<%= lblDocId.ClientID %>").value;
                var owd = $find("<%=CheckIn.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/Checkin.aspx?docId=" + docId, "CheckIn");
            }
            function FileRevisionHistory() {
                var docId = document.getElementById("<%= lblDocId.ClientID %>").value;
                var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
                var owd = $find("<%=RevisionDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/RevisionHistory.aspx?docId=" + docId + "&categoryId=" + categoryId, "RevisionDialog");
            }
            function FileDuplicateRevision() {
                var docId = document.getElementById("<%= lblDocId.ClientID %>").value;
                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/RetrieveDocumentForm.aspx?docId=" + docId, "CustomerDialog");
            }

            function btDownloadfolder() {
                // $('#loading').show();
                var foldersize = parseFloat(document.getElementById("<%= lbFolderSize.ClientID %>").value) / 19;
                //alert(foldersize);
                document.getElementById("loading").style.display = "block";
                setTimeout(function () {
                    //window.location.reload(1);
                    document.getElementById("loading").style.display = "none";
                }, 1000 * foldersize);
            }
            function DownloadFolder() {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                var userid = document.getElementById("<%= lbUserId.ClientID %>").value;
                var roleid = document.getElementById("<%= lbRoleId.ClientID %>").value;
                <%--var owd = $find("<%=DownloadFolder .ClientID %>");
                owd.Show(); owd.moveTo(screen.width - 250, screen.height - 300);
                owd.setUrl("DownloadFolder.aspx?selectedFolder=" + selectedFolder, "DownloadFolder");--%>

                //$.ajax({
                //    type: 'POST',
                //    url: "DownloadFolder.aspx",
                //    dataType: "text",
                //    data: { 'type': 2, 'selectedFolder': selectedFolder, 'userId': userid, 'roleId': roleid },
                //    beforeSend: function (xmlHttpRequest) {
                //       // alert("Before Sending.");
                //        $('#loading').show();
                //    },
                //    statusCode: function (data) {
                //        alert(data);
                //    },
                //    complete: function (data) {
                //        if (data.indexOf("Ok")>=0) {
                //                $('#loading').hide();
                //        } else {
                //                 alert(data);
                //        }      
                //    },
                //    success: function (data) {
                //        if (data.indexOf("Ok") >= 0) {
                //            $('#loading').hide();
                //        } else {
                //            alert(data);
                //        } 
                //    }
                //});
                var postdata = "selectedFolder=" + selectedFolder
                var xhr = new XMLHttpRequest();
                $('#loading').show();
                xhr.open('POST', 'DownloadFolder.aspx?' + postdata, false);
                xhr.onreadystatechange = function () {

                    if (this.readyState == 4 && this.status == 200) {

                        if (this.responseText.substring(0, 2) == "Ok") {
                            // //var st = this.responseText.split('$')[1];
                            // // alert(st);
                            $('#loading').hide();
                            //// ajaxManager.ajaxRequest("DowloadZipFile_" + selectedFolder);
                        }
                    }
                };
                xhr.send(postdata);

            }
            function onClientContextMenuItemClicking(sender, args) {
                var menuItem = args.get_menuItem();
                var treeNode = args.get_node();
                menuItem.get_menu().hide();
                document.getElementById("<%= IsUpdateFolder.ClientID %>").value = "2";
                switch (menuItem.get_value()) {
                    case "Rename":
                        treeNode.startEdit();
                        break;
                    case "Delete":
                        var result = confirm("Are you sure you want to delete the folder: " + treeNode.get_text());
                        args.set_cancel(!result);
                        break;
                    case "MoveFolder":
                        var owd = $find("<%=MoveFolder.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/MoveFileAndFolder.aspx?mode=1&folderId=" + treeNode.get_value(), "MoveFolder");
                        break;
                    case "Permission":
                        var owd = $find("<%=FolderPermission.ClientID %>");
                        //owd.Show();
                        owd.setUrl("Controls/Document/FolderPermissionForm.aspx?folderId=" + treeNode.get_value(), "FolderPermission");
                        break;
                    case "FolderType":
                        var owd = $find("<%=CopyPath.ClientID %>");
                        //owd.Show();
                        owd.setUrl("Controls/Document/EditFolderType.aspx?folderId=" + treeNode.get_value(), "FolderType");
                        break;
                    case "FolderPropertises":
                        <%--var owd = $find("<%=FolderPermission.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/FolderPropertises.aspx?folderId=" + treeNode.get_value(), "FolderType");--%>
                        break;
                    <%--case "CopyPath":
                        var owd = $find("<%=CopyPath.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/CopyPath.aspx?folderId=" + treeNode.get_value(), "CopyPath");
                        break;--%>

                }
            }

            function OnClientShow(sender, args) {
                if (!sender.isPinned())
                    sender.togglePin();
            }

            function gridContextMenuShowing(menu, args) {
                var isCheckOut = document.getElementById("<%= isCheckOut.ClientID %>").value;
                var isFolder = document.getElementById("<%= isFolder.ClientID %>").value;
                var currentCheckoutRoleId = document.getElementById("<%= CurrentCheckoutRoleId.ClientID %>").value;
                var currentRoleId = document.getElementById("<%= CurrentRoleId.ClientID %>").value;
                var IsAdminGroup = document.getElementById("<%= IsAdminGroup.ClientID %>").value;


                if (isFolder == "True") {
                    menu.get_allItems()[0].hide();
                    menu.get_allItems()[1].hide();
                    menu.get_allItems()[2].hide();
                    menu.get_allItems()[3].hide();
                    menu.get_allItems()[4].hide();
                }
                else {
                    menu.get_allItems()[0].show();
                    menu.get_allItems()[1].show();
                    menu.get_allItems()[2].show();
                    menu.get_allItems()[3].show();
                    menu.get_allItems()[4].show();
                }

                if (isCheckOut == "True") {
                    menu.get_allItems()[0].disable();
                    menu.get_allItems()[1].enable();
                    menu.get_allItems()[2].disable();
                    menu.get_allItems()[3].disable();
                    menu.get_allItems()[4].disable();
                }
                else {
                    menu.get_allItems()[0].enable();
                    menu.get_allItems()[1].enable();
                    menu.get_allItems()[2].enable();
                    menu.get_allItems()[3].enable();
                    menu.get_allItems()[4].enable();
                }

                ////alert(currentRoleId);
                ////alert(currentCheckoutRoleId);
                //if (isCheckOut == "True") {


                //    if (currentCheckoutRoleId == currentRoleId) {
                //        menu.get_allItems()[0].enable();
                //        menu.get_allItems()[2].enable();
                //        menu.get_allItems()[5].disable();
                //        menu.get_allItems()[6].enable();
                //    }
                //    else {
                //        menu.get_allItems()[0].disable();
                //        menu.get_allItems()[2].disable();
                //        menu.get_allItems()[5].disable();
                //        menu.get_allItems()[6].disable();
                //    }
                //}
                //else {
                //    menu.get_allItems()[5].enable();
                //    menu.get_allItems()[6].disable();


                //}

                //if (IsAdminGroup == "true") {
                //    menu.get_allItems()[7].show();
                //    menu.get_allItems()[8].show();
                //} else {
                //    menu.get_allItems()[7].hide();
                //    menu.get_allItems()[8].hide();
                //}
            }

            function onClientContextMenuShowing(sender, args) {
                var treeNode = args.get_node();

                var allNodes = treeNode.get_allNodes();

                if (treeNode.get_level() == 0) {
                    args.get_menu().get_items().getItem(0).set_enabled(true);

                    args.get_menu().get_items().getItem(1).set_enabled(false);

                }
                else {
                    args.get_menu().get_items().getItem(0).set_enabled(true);
                    args.get_menu().get_items().getItem(1).set_enabled(true);

                }

            }

            function rtvExplore_OnNodeExpandedCollapsed(sender, eventArgs) {
                var allNodes = eventArgs._node.get_treeView().get_allNodes();
                var i;
                var selectedNodes = "";

                for (i = 0; i < allNodes.length; i++) {
                    if (allNodes[i].get_expanded())
                        selectedNodes += allNodes[i].get_value() + "*";
                }
                Set_Cookie("expandedNodes", selectedNodes, 30);
            }

            function Set_Cookie(name, value, expires, path, domain, secure) {
                // set time, it's in milliseconds
                var today = new Date();
                today.setTime(today.getTime());
                /*
                if the expires variable is set, make the correct 
                expires time, the current script below will set 
                it for x number of days, to make it for hours, 
                delete * 24, for minutes, delete * 60 * 24
                */
                if (expires) {
                    expires = expires * 1000 * 60 * 60 * 24;
                }
                var expires_date = new Date(today.getTime() + (expires));

                document.cookie = name + "=" + escape(value) +
                    ((expires) ? ";expires=" + expires_date.toGMTString() : "") +
                    ((path) ? ";path=" + path : "") +
                    ((domain) ? ";domain=" + domain : "") +
                    ((secure) ? ";secure" : "");
            }
        </script>
        <script type="text/javascript">
            /* <![CDATA[ */
            var toolbar;
            var searchButton;
            var ajaxManager;

            function pageLoad() {
                $('iframe').load(function () { //The function below executes once the iframe has finished loading<---true dat, althoug Is coppypasta from I don't know where
                    //alert($('iframe').contents());
                });

                toolbar = $find("<%= CustomerMenu.ClientID %>");
                ajaxManager = $find("<%=ajaxCustomer.ClientID %>");

                //searchButton = toolbar.findButtonByCommandName("doSearch");

                //$telerik.$(".searchtextbox")
                //    .bind("keypress", function (e) {
                //        searchButton.set_imageUrl("images/search.gif");
                //        searchButton.set_value("search");
                //    });
                var grid = $find("<%=grdDocument.ClientID%>");
                var master = grid.get_masterTableView();
                if (master.get_selectedItems().length > 0) {
                    var item = master.get_selectedItems()[0];
                    var rowElement = item.get_element();
                    var offset = rowElement.offsetTop;
                    setScroll(grid, offset);
                }
            }

            function setScroll(grid, scrollTop) {
                var dataDiv = $telerik.findElement(grid.get_element(), grid.get_id() + "_GridData");
                dataDiv.scrollTop = scrollTop;
            }

            function ShowEditForm(id) {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
               <%-- var id = document.getElementById("<%= lblDocId.ClientID %>").value;--%>
                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/DocumentInfoEditForm.aspx?docId=" + id + "&folId=" + selectedFolder + "&categoryId=" + categoryId, "CustomerDialog");
            }

            function ShowViewerFile(Id) {

                // if (path.toLowerCase().search(".pdf")) {
                var owd = $find("<%= ViewerFile.ClientID %>");
                owd.Show();
                owd.moveTo(120, 60);
                owd.setUrl("ViewerFilePDF.aspx?DocId=" + Id + "&File=0", "ViewerFile");
            }

            function ShowVideoForm(id) {
                var owd = $find("<%=VideoView.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/ViewVideoFile.aspx?docId=" + id, "VideoView");
            }
            function ShowImageForm(id) {
                var owd = $find("<%=ImageView.ClientID %>");
                owd.Show();
                owd.maximize(true);
                owd.setUrl("Controls/Document/ViewImage.aspx?docId=" + id, "ImageView");
            }
            function ShowEditPropertises(id) {
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Document/DocumentInfoEditForm.aspx?docId=" + id + "&folId=" + selectedFolder + "&categoryId=" + categoryId, "CustomerDialog");
            }
            function ShowChildFolder(folderId) {
                ajaxManager.ajaxRequest("ShowChildFolder_" + folderId);
            }

            function ShowInsertForm() {
                var owd = $find("<%=CustomerDialog.ClientID %>");
                owd.Show();
                owd.setUrl("Controls/Customers/CustomerEditForm.aspx", "CustomerDialog");
            }
            function refreshGridAndFolder(arg) {
                var idfolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                ajaxManager.ajaxRequest("ReloadFolder_" + idfolder);
                if (arg != '') {
                    $find("<%= ajaxCustomer.ClientID %>").ajaxRequest(radTreeFolder);
                    var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView();
                    masterTable.rebind();
                }
            }
            function refreshGrid(arg) {
                if (!arg) {
                    ajaxManager.ajaxRequest("Rebind");
                }
                else {
                    ajaxManager.ajaxRequest("RebindAndNavigate");
                }
            }

            function onNodeClicking(sender, args) {
                var folderValue = args.get_node().get_value();
                document.getElementById("<%= lblFolderId.ClientID %>").value = folderValue;
            }

            function radViewSettingBar_OnClientButtonClicking(sender, args) {
                var button = args.get_item();
                var strText = button.get_text();
                var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                var paneID = "<%= RadSlidingPane1.ClientID %>";
                var slidingZone = $find("<%=SlidingZone1.ClientID %>");
                var dockedPaneId = slidingZone.get_dockedPaneId();

                if (strText.toLowerCase() == "list all documents") {
                    if (selectedFolder == "") {
                        alert("Please choice one folder to view list documents");
                        return false;
                    }
                    else {
                        slidingZone.undockPane(paneID);
                        ajaxManager.ajaxRequest("ListAllDocuments");
                    }
                }

                if (strText.toLowerCase() == "tree view") {
                    if (selectedFolder == "") {
                        alert("Please choice one folder to view documents");
                        return false;
                    }
                    else {
                        if (dockedPaneId == null || dockedPaneId != paneID) {
                            slidingZone.collapsePane(paneID);
                            slidingZone.dockPane(paneID);
                        }
                        ajaxManager.ajaxRequest("TreeView");
                    }
                }
                return false;
            }

            function OnClientButtonClicking(sender, args) {
                var button = args.get_item();
                var strText = button.get_text();
                var strValue = button.get_value();

                var grid = $find("<%= grdDocument.ClientID %>");
                    var customerId = null;
                    var customerName = "";

                    //if (grid.get_masterTableView().get_selectedItems().length > 0) {
                    //    var selectedRow = grid.get_masterTableView().get_selectedItems()[0];
                    //    customerId = selectedRow.getDataKeyValue("Id");
                    //    //customerName = selectedRow.Items["FullName"]; 
                    //    //customerName = grid.get_masterTableView().getCellByColumnUniqueName(selectedRow, "FullName").innerHTML;
                    //}Send notifications window.open('file://Shared Folder/Users')

                    if (strText.toLowerCase() == "send notifications") {
                        var grid = $find("<%=grdDocument.ClientID %>");
                        var masterTable = grid.get_masterTableView();
                        var number = 0;
                        var selectedRows = masterTable.get_selectedItems();
                        for (var i = 0; i < selectedRows.length; i++) {
                            number++;
                        }
                        if (number == 0) {
                            alert("Please select documents to send notification");
                        }
                        else {
                            ajaxManager.ajaxRequest("SendNotification");
                        }
                    }

                    if (strText.toLowerCase() == "import data") {
                        var owd = $find("<%=ImportData.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/ImportData.aspx", "ImportData");
                    }

                    if (strText.toLowerCase() == "export data") {
                        ajaxManager.ajaxRequest("ExportData");
                    }

                    if (strText.toLowerCase() == "move files") {
                        var grid = $find("<%=grdDocument.ClientID %>");
                    var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                    var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
                    var masterTable = grid.get_masterTableView();
                    var number = 0;
                    var listId = "";
                    var selectedRows = masterTable.get_selectedItems();
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        number++;
                        listId += row.getDataKeyValue("ID") + ",";
                    }
                    if (number == 0) {
                        alert("Please select documents to move");
                    }
                    else {
                        var owd = $find("<%=MoveFolder.ClientID %>");
                            owd.Show();
                            owd.setUrl("Controls/Document/MoveFileAndFolder.aspx?mode=2&docIds=" + listId + "&selectedFolder=" + selectedFolder + "&categoryId=" + categoryId, "MoveFolder");
                        }
                    }

                    if (strText.toLowerCase() == "checkout selected documents") {
                        var grid = $find("<%=grdDocument.ClientID %>");
                    var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                    var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
                    var masterTable = grid.get_masterTableView();
                    var number = 0;
                    var listId = "";
                    var selectedRows = masterTable.get_selectedItems();
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        number++;
                        listId += row.getDataKeyValue("ID") + ",";
                    }
                    if (number == 0) {
                        alert("Please select documents to checkout");
                    }
                    else {
                        var owd = $find("<%=CheckOut.ClientID %>");
                            owd.Show();
                            owd.setUrl("Controls/Document/CheckOutDocument.aspx?docId=" + listId, "CheckOut");
                        }
                    }

                    if (strText.toLowerCase() == "checkin selected documents") {
                        var grid = $find("<%=grdDocument.ClientID %>");
                    var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                    var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
                        var masterTable = grid.get_masterTableView();
                        var number = 0;
                        var listId = "";
                        var selectedRows = masterTable.get_selectedItems();
                        for (var i = 0; i < selectedRows.length; i++) {
                            var row = selectedRows[i];
                            number++;
                            listId += row.getDataKeyValue("ID") + ",";
                        }
                        if (number == 0) {
                            alert("Please select documents to checkin");
                        }
                        else {
                            ajaxManager.ajaxRequest("CheckinMultiDocument");
                        }
                    }

                    if (strText.toLowerCase() == "download multi documents") {
                        var grid = $find("<%=grdDocument.ClientID %>");
                        var masterTable = grid.get_masterTableView();

                        var listId = "";

                        var selectedRows = masterTable.get_selectedItems();
                        if (selectedRows.length == 0) {
                            alert("Please select documents to download");
                        }
                        if (number == 0) {
                            alert("Please select documents to download");
                        }
                        else if (number > 100) {
                            alert("The maximum file download at once time is 100. Current you select " + number + "files.");
                        }
                        else {
                            ajaxManager.ajaxRequest("DownloadMulti");
                        }
                    }
                if (strText.toLowerCase() === "delete multi documents") {
                          var grid = $find("<%=grdDocument.ClientID %>");
                        var masterTable = grid.get_masterTableView();
                        var selectedRows = masterTable.get_selectedItems();
                        if (selectedRows.length == 0) {
                            alert("Please select documents to download");
                        } else {
                             ajaxManager.ajaxRequest("DeleteMulti");
                        }
                    }
                    if (strText.toLowerCase() == "upload folder") {
                        //ajaxManager.ajaxRequest("UpFolder");
                        var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
                    var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                    if (selectedFolder != "") {
                        var owd = $find("<%=UploadFodler.ClientID %>");
                            owd.Show();
                            owd.setUrl("../../Uploadfolder.aspx?FolderId=" + selectedFolder + "&categoryId=" + categoryId, "UploadFodler");
                        } else {
                            alert("Please choose one folder");
                            return false;
                        }

                    }

                    if (strText.toLowerCase() == "filesize") {
                        ajaxManager.ajaxRequest("FileSize");
                    }

                    if (strText.toLowerCase() == "folderpropertise") {
                        ajaxManager.ajaxRequest("FolderPropertise");
                    }

                    if (strText.toLowerCase() == "send mail with attachment") {
                        var grid = $find("<%=grdDocument.ClientID %>");
                    var masterTable = grid.get_masterTableView();
                    var number = 0;
                    var listId = "";
                    var selectedRows = masterTable.get_selectedItems();
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        number++;
                        listId += row.getDataKeyValue("ID") + ",";
                    }
                    if (number == 0) {
                        alert("Please select documents to send mail");
                    }
                    else {
                        var owd = $find("<%=SendMail.ClientID %>");
                            owd.Show();
                            owd.setUrl("Controls/Document/SendMail.aspx?type=0&listDoc=" + listId, "SendMail");
                        }
                    }

                    if (strText.toLowerCase() == "send mail with link system") {
                        var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
                        var grid = $find("<%=grdDocument.ClientID %>");
                        var masterTable = grid.get_masterTableView();
                        var number = 0;
                        var listId = "";
                        var selectedRows = masterTable.get_selectedItems();
                        for (var i = 0; i < selectedRows.length; i++) {
                            var row = selectedRows[i];
                            number++;
                            listId += row.getDataKeyValue("ID") + ",";
                        }
                        if (number == 0) {
                            alert("Please select documents to send mail");
                        }
                        else {
                            var owd = $find("<%=SendMail.ClientID %>");
                            owd.Show();
                            owd.setUrl("Controls/Document/SendMail.aspx?type=1&listDoc=" + listId + "&categoryId=" + categoryId, "SendMail");
                        }
                    }

                    if (strText.toLowerCase() == "document") {
                        var categoryId = document.getElementById("<%= lblCategoryId.ClientID %>").value;
                    var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                    if (selectedFolder == "") {
                        alert("Please choice one folder to create new document");
                        return false;
                    }

                    var owd = $find("<%=CustomerDialog.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/DocumentInfoEditForm.aspx?folId=" + selectedFolder + "&categoryId=" + categoryId, "CustomerDialog");
                    }

                    if (strText.toLowerCase() == "multi documents") {
                        var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                    if (selectedFolder == "") {
                        alert("Please choise one folder to create new document.");
                        return false;
                    }

                    var owd = $find("<%=UploadMulti.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/UploadDragDrop.aspx?folId=" + selectedFolder, "UploadMulti");
                    }

                    if (strText.toLowerCase() == "upload metadata") {
                        var owd = $find("<%=UploadMulti.ClientID %>");
                        owd.Show();
                        owd.setUrl("Controls/Document/UploadMetadata.aspx", "UploadMulti");
                    }

                    if (strText.toLowerCase() == "export excel") {
                        var selectedFolder = document.getElementById("<%= lblFolderId.ClientID %>").value;
                    if (selectedFolder == "") {
                        alert("Please choice one folder to export master list.");
                        return false;
                    }
                    else {
                        ajaxManager.ajaxRequest("ExportMasterList");
                    }
                }

                if (strText == "Thêm mới") {
                    return ShowInsertForm();
                }
                else if (strText == "Import dữ liệu") {
                    return ShowImportForm();
                }

                else {
                    var commandName = args.get_item().get_commandName();
                    if (commandName == "doSearch") {
                        var searchTextBox = sender.findButtonByCommandName("searchText").findControl("txtSearch");
                        if (searchButton.get_value() == "clear") {
                            searchTextBox.set_value("");
                            searchButton.set_imageUrl("images/search.gif");
                            searchButton.set_value("search");
                        }

                        performSearch(searchTextBox);
                    } else if (commandName == "reply") {
                        window.radopen(null, "Edit");
                    }
                }
            }

            function performSearch(searchTextBox) {
                if (searchTextBox.get_value()) {
                    searchButton.set_imageUrl("images/clear.gif");
                    searchButton.set_value("clear");
                }

                ajaxManager.ajaxRequest(searchTextBox.get_value());
            }

            <%--function RowContextMenu(sender, eventArgs) {
                var grid = sender;
                var MasterTable = grid.get_masterTableView();
                var row = MasterTable.get_dataItems()[eventArgs.get_itemIndexHierarchical()];
                var cell = MasterTable.getCellByColumnUniqueName(row, "IsCheckOut");
                var cellCurrentCheckoutRoleId = MasterTable.getCellByColumnUniqueName(row, "CurrentCheckoutByRole");

                document.getElementById("<%= isCheckOut.ClientID %>").value = cell.innerHTML == "&nbsp;" ? false : cell.innerHTML;

                //row = eventArgs.get_gridDataItem();
                cell = MasterTable.getCellByColumnUniqueName(row, "IsFolder");
                document.getElementById("<%= isFolder.ClientID %>").value = cell.innerHTML;

                cell = MasterTable.getCellByColumnUniqueName(row, "FileName");
                document.getElementById("<%= fileName.ClientID %>").value = cell.innerHTML;

                cell = MasterTable.getCellByColumnUniqueName(row, "FilePath");
                document.getElementById("<%= filePath.ClientID %>").value = cell.innerHTML;

                var CurrentCheckoutRoleId = cellCurrentCheckoutRoleId.innerHTML;
                document.getElementById("<%= CurrentCheckoutRoleId.ClientID %>").value = CurrentCheckoutRoleId;
                //alert(isCheckOut);

                var menu = $find("<%=radMenu.ClientID %>");
                var evt = eventArgs.get_domEvent();

                if (evt.target.tagName == "INPUT" || evt.target.tagName == "A") {
                    return;
                }

                var index = eventArgs.get_itemIndexHierarchical();
                document.getElementById("radGridClickedRowIndex").value = index;

                var Id = eventArgs.getDataKeyValue("ID");
                document.getElementById("<%= lblDocId.ClientID %>").value = Id;

                sender.get_masterTableView().selectItem(sender.get_masterTableView().get_dataItems()[index].get_element(), true);
                menu.show(evt);
                evt.cancelBubble = true;
                evt.returnValue = false;

                if (evt.stopPropagation) {
                    evt.stopPropagation();
                    evt.preventDefault();
                }
            }--%>

            function OnClientBeforeClose(sender, args) {
                var oWnd = $find("<%= RadWindowManager1.ClientID %>").getWindowByName("VideoView");
                oWnd.Reload();
            }
        </script>
    </telerik:RadCodeBlock>
</asp:Content>
