﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;

namespace EDMs.Web
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.ServiceProcess;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    using ZetaLongPaths;

    /// <summary>
    /// Class customer
    /// </summary>
    public partial class QuickSearch : Page
    {
        private readonly PermissionService permissionService = new PermissionService();



        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly StatusService statusService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService;

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly LanguageService languageService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The category service.
        /// </summary>
        private readonly CategoryService categoryService;

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string unreadPattern = @"\(\d+\)";

        protected const string ServiceName = "EDMSFolderWatcher";

        private readonly SpecialDocPermissionService specialDocPermissionService;

        private int CategoryID
        {
            get
            {
                if (!string.IsNullOrEmpty(this.ddlCategory.SelectedValue))
                {
                    return Convert.ToInt32(this.ddlCategory.SelectedValue);
                }

                return 0;
            }
        }

        private string Name
        {
            get
            {
                return this.txtName.Text.Trim();
            }
        }

        private string DocTitle
        {
            get
            {
                return this.txtTitle.Text.Trim();
            }
        }

        private string DocumentNumber
        {
            get { return this.txtDocumentNumber.Text.Trim(); }
        }

        private string Keywords
        {
            get { return this.txtKeyword.Text.Trim(); }
        }

        private int RevisionID
        {
            get { return Convert.ToInt32(this.ddlRevision.SelectedValue); }
        }

        private int DocumentTypeID
        {
            get { return Convert.ToInt32(this.ddlDocumentType.SelectedValue); }
        }

        private int StatusID
        {
            get { return Convert.ToInt32(this.ddlStatus.SelectedValue); }
        }

        private int ReceiveFromID
        {
            get { return Convert.ToInt32(this.ddlReceivedFrom.SelectedValue); }
        }

        private int DisciplineID
        {
            get { return Convert.ToInt32(this.ddlDiscipline.SelectedValue); }
        }

        private int LanguageID
        {
            get { return Convert.ToInt32(this.ddlLanguage.SelectedValue); }
        }

        private DateTime? DateFrom
        {
            get { return this.txtDateFrom.SelectedDate; }
        }

        private DateTime? DateTo
        {
            get { return this.txtDateTo.SelectedDate; }
        }

        private string TransmittalNumber
        {
            get { return this.txtTransmittalNumber.Text.Trim(); }
        }

        private readonly GroupDataPermissionService groupDataPermissionService;

        private readonly UserDataPermissionService userDataPermissionService;

        private readonly RoleService roleService;

        private readonly UserService userService;

        private readonly FolderService folderService;

        private const string RegexValidateEmail =
            @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";

        private List<int> AdminGroup
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("GroupAdminList").Split(',').Select(t => Convert.ToInt32(t)).ToList();
            }
        }
        private readonly DocPropertiesViewService docPropertiesViewService;

        public QuickSearch()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.documentService = new DocumentService();
            this.statusService = new StatusService();
            this.disciplineService = new DisciplineService();
            this.receivedFromService = new ReceivedFromService();
            this.languageService = new LanguageService();
            this.categoryService = new CategoryService();
            this.groupDataPermissionService = new GroupDataPermissionService();
            this.userDataPermissionService = new UserDataPermissionService();
            this.roleService = new RoleService();
            this.userService = new UserService();
            this.folderService = new FolderService();
            this.specialDocPermissionService = new SpecialDocPermissionService();
            this.docPropertiesViewService = new DocPropertiesViewService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Form.DefaultButton = this.btnSearch.UniqueID;
            var totalProperty = Convert.ToInt32(ConfigurationManager.AppSettings.Get("TotalProperty"));
            for (int i = 1; i <= totalProperty; i++)
            {
                var column = this.grdDocument.MasterTableView.GetColumn("Index" + i);
                //var revisionColumn = this.grdDocument.MasterTableView.GetColumn("RevisionColumn");
                if (column != null)
                {
                    column.Visible = false;
                }
            }
            if (!Page.IsPostBack)
            {
                this.IsAdmin.Value = this.AdminGroup.Contains(UserSession.Current.RoleId) ? "true" : "false";

                if (!UserSession.Current.User.Role.Active.GetValueOrDefault())
                {
                    // this.grdDocument.MasterTableView.GetColumn("EditColumn").Display = false;
                    this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Display = false;
                    this.grdDocument.MasterTableView.GetColumn("IsSelected").Display = false;

                    this.RadpaneSearchAction.Visible = false;
                }

                this.LoadComboData();

                var categoryPermission = this.groupDataPermissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault()).Select(t => t.CategoryIdList).ToList();
                categoryPermission = categoryPermission.Union(this.userDataPermissionService.GetByUserId(UserSession.Current.User.Id).Select(t => t.CategoryId.ToString())).Distinct().ToList();
                var listCategory = this.AdminGroup.Contains(UserSession.Current.RoleId)
                                    ? this.categoryService.GetAll()
                                    : this.categoryService.GetAll().Where(t => categoryPermission.Any(x => x == t.ID.ToString())).ToList();
                if (listCategory.Any())
                {
                    listCategory.Insert(0, new Category() { ID = 0, Name = "All" });
                    this.ddlCategory.DataSource = listCategory;
                    this.ddlCategory.DataValueField = "ID";
                    this.ddlCategory.DataTextField = "Name";
                    this.ddlCategory.DataBind();

                    if (!string.IsNullOrEmpty(this.Request.QueryString["ctgr"]))
                    {
                        this.ddlCategory.SelectedValue = this.Request.QueryString["ctgr"];
                        this.txtName.Text = this.Request.QueryString["text"];
                    }
                    else
                    {
                        this.txtSearchAll.Text = this.Request.QueryString["text"];
                    }

                    foreach (var category in listCategory)
                    {
                        category.ParentId = -1;
                    }

                    listCategory.RemoveAt(0);
                    listCategory.Insert(0, new Category() { ID = -1, Name = "DOCUMENT GROUP" });

                    this.radPbCategories.DataSource = listCategory;
                    this.radPbCategories.DataFieldParentID = "ParentId";
                    this.radPbCategories.DataFieldID = "Id";
                    this.radPbCategories.DataValueField = "Id";
                    this.radPbCategories.DataTextField = "Name";
                    this.radPbCategories.DataBind();
                    this.radPbCategories.Items[0].Expanded = true;

                    foreach (RadPanelItem item in this.radPbCategories.Items[0].Items)
                    {
                        item.ImageUrl = @"Images/category2.png";
                        item.NavigateUrl = "DocumentsHome.aspx?doctype=" + item.Value;
                    }
                }
                LoadDocConfigGridView();
                this.LoadListPanel();
                this.LoadSystemPanel();
            }
        }

        private void LoadDocConfigGridView()
        {
            var isViewByGroup = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("ViewSettingByGroup"));
            var selectedProperty = new List<string>();

            //var ddlCategory = (RadComboBox)this.rpbObjTree.FindItemByValue("ObjTree").FindControl("ddlCategory");
            //if (this.radTreeFolder.SelectedNode != null)
            //{
            var totalProperty = Convert.ToInt32(ConfigurationManager.AppSettings.Get("TotalProperty"));
            //var folderId = Convert.ToInt32(this.radTreeFolder.SelectedNode.Value);
            //var categoryId = Convert.ToInt32(ddlCategory.SelectedValue);
            //var deparmentId = isViewByGroup && !UserSession.Current.User.IsAdmin.GetValueOrDefault()
            //? UserSession.Current.RoleId
            //: 0;
            //var folderTypeObj = this.folderTypeService.GetByFolder(folderId);
            //if (folderTypeObj != null)
            //{
            //    var docPropertiesView = this.docPropertiesViewService.GetByType(folderTypeObj.DocTypeID);
            //    if (docPropertiesView != null && docPropertiesView.PropertyIndex != "")
            //    {
            //        var temp =
            //            docPropertiesView.PropertyIndex.Split(',').Where(t => !string.IsNullOrEmpty(t.Trim())).Select(
            //                t => t.Trim()).ToList();
            //        selectedProperty.AddRange(temp);
            //    }

            //    selectedProperty = selectedProperty.Distinct().ToList();

            //    for (int i = 1; i <= totalProperty; i++)
            //    {
            //        var column = this.grdDocument.MasterTableView.GetColumn("Index" + i);
            //        if (column != null)
            //        {
            //            column.Visible = selectedProperty.Contains(i.ToString());
            //        }
            //    }
            //}
            //else
            //{
            var docPropertiesView = this.docPropertiesViewService.GetByType(0);
            if (docPropertiesView != null && docPropertiesView.PropertyIndex != "")
            {
                var temp =
                    docPropertiesView.PropertyIndex.Split(',').Where(t => !string.IsNullOrEmpty(t.Trim())).Select(
                        t => t.Trim()).ToList();
                selectedProperty.AddRange(temp);
            }

            selectedProperty = selectedProperty.Distinct().ToList();

            for (int i = 1; i <= totalProperty; i++)
            {
                var column = this.grdDocument.MasterTableView.GetColumn("Index" + i);
                if (column != null)
                {
                    column.Visible = selectedProperty.Contains(i.ToString());
                }
            }
            //}
            //}
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                grdDocument.MasterTableView.SortExpressions.Clear();
                grdDocument.MasterTableView.GroupByExpressions.Clear();
                grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                grdDocument.MasterTableView.SortExpressions.Clear();
                grdDocument.MasterTableView.GroupByExpressions.Clear();
                ////grdDocument.MasterTableView.CurrentPageIndex = grdDocument.MasterTableView.PageCount - 1;
                grdDocument.Rebind();
            }
            else if (e.Argument == "SendNotification")
            {
                var listSelectedDoc = new List<Document>();

                foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
                {
                    var docId = Convert.ToInt32(selectedItem.GetDataKeyValue("ID"));
                    var docItem = this.documentService.GetById(docId);
                    if (docItem != null)
                    {
                        listSelectedDoc.Add(docItem);
                    }

                    selectedItem.Selected = false;
                }

                foreach (var groupFolder in listSelectedDoc.GroupBy(t => t.FolderID))
                {
                    var folder = this.folderService.GetById(groupFolder.Key.GetValueOrDefault());
                    if (folder != null)
                    {
                        var category = this.categoryService.GetById(folder.CategoryID.GetValueOrDefault());
                        var smtpClient = new SmtpClient
                        {
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                            EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                            Host = ConfigurationManager.AppSettings["Host"],
                            Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                            Credentials = new NetworkCredential(ConfigurationManager.AppSettings["Email"], ConfigurationManager.AppSettings["EmailPass"])
                        };

                        var message = new MailMessage();
                        message.From = new MailAddress(ConfigurationManager.AppSettings["Email"], ConfigurationManager.AppSettings["EmailName"]);
                        message.Subject = "Availability of New/Updated " + (category != null ? category.Name : string.Empty) + " Documents on DocLib";
                        //ConfigurationManager.AppSettings["SubtitleName"];
                        message.BodyEncoding = new UTF8Encoding();
                        message.IsBodyHtml = true;
                        message.Body = @"Dear users,<br/><br/>

                                Please be informed that the following documents are now available on the BDPOC Document Library System for your information and implementation.<br/><br/>

                                <table border='1' cellspacing='0'>
	                                <tr>
		                                <th style='text-align:center; width:40px'>No.</th>
		                                <th style='text-align:center; width:330px'>Document Number</th>
		                                <th style='text-align:center; width:330px'>Document Title</th>
		                                <th style='text-align:center; width:60px'>Revision</th>
		                                <th style='text-align:center; width:120px'>Status</th>
		                                <th style='text-align:center; width:120px'>Discipline</th>
		                                <th style='text-align:center; width:120px'>Document Type</th>
	                                </tr>";

                        var subBody = string.Empty;
                        var count = 0;
                        foreach (var document in groupFolder)
                        {
                            var port = ConfigurationSettings.AppSettings.Get("DocLibPort");

                            count += 1;
                            subBody += @"<tr>
                            <td>" + count + @"</td>
                            <td><a href='" + ConfigurationSettings.AppSettings.Get("WebAddress") + (!string.IsNullOrEmpty(port) ? ":" + port : string.Empty)
                                            + document.FilePath + "' download='" + document.DocumentNumber + "'>"
                                            + document.DocumentNumber + @"</a></td>
                            <td>"
                                            + document.Title + @"</td>
                            <td>"
                                            + document.RevisionName + @"</td>
                            <td>"
                                        + document.StatusName + @"</td>
                            <td>"
                                        + document.DisciplineName + @"</td>
                            <td>"
                                        + document.DocumentTypeName + @"</td>";
                        }


                        message.Body += subBody + @"</table>
                                <br/>
                                Thanks and regards,<br/><br/>
                                        " + UserSession.Current.User.FullName + ". <br/>";


                        var groupsInPermission = this.groupDataPermissionService
                                                    .GetAllByFolder(folder.ID.ToString())
                                                    .Select(t => t.RoleId).Distinct().ToList();

                        var listGroup = this.roleService
                                            .GetAll()
                                            .Where(t => groupsInPermission.Contains(t.Id)
                                                        && !this.AdminGroup.Contains(t.Id)
                                                        && !string.IsNullOrEmpty(t.Email)
                                                        && Regex.IsMatch(t.Email, RegexValidateEmail)).ToList();

                        var usersInPermission = this.userDataPermissionService
                                                    .GetAllByFolder(folder.ID)
                                                    .Select(t => t.UserId).Distinct().ToList();

                        var listUser = this.userService
                                            .GetAll()
                                            .Where(t => usersInPermission.Contains(t.Id)
                                                        && !groupsInPermission.Contains(t.RoleId)
                                                        && !string.IsNullOrEmpty(t.Email)
                                                        && Regex.IsMatch(t.Email, RegexValidateEmail)).ToList();

                        var groupEmailList = listGroup.Select(t => t.Email);
                        var userEmailList = listUser.Select(t => t.Email);

                        var emailList = groupEmailList.Union(userEmailList).Distinct();
                        foreach (var email in emailList)
                        {
                            message.To.Add(new MailAddress(email));
                        }

                        smtpClient.Send(message);
                    }
                }
            }
        }

        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            LoadDocConfigGridView();
            if (!string.IsNullOrEmpty(Request.QueryString["doclist"]))
            {
                var list = Request.QueryString["doclist"].Split(',').Select(int.Parse).ToList();
                var lisDoc = this.documentService.GetSpecificDocument(list);
                this.grdDocument.DataSource = lisDoc;
            }
            else
            {
                var pageSize = this.grdDocument.PageSize;
                var currentPage = this.grdDocument.CurrentPageIndex;
                var startingRecordNumber = currentPage * pageSize;
                var categoryPermission = new List<int>();
                if (!this.AdminGroup.Contains(UserSession.Current.RoleId))
                {
                    categoryPermission = this.groupDataPermissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault()).Select(t => Convert.ToInt32(t.CategoryIdList)).Distinct().ToList();
                    categoryPermission = categoryPermission.Union(
                            this.userDataPermissionService.GetByUserId(UserSession.Current.User.Id).Select(t => t.CategoryId.GetValueOrDefault()))
                            .Distinct()
                            .ToList();
                }
                else
                {
                    categoryPermission = this.categoryService.GetAll().Select(t => t.ID).ToList();
                }

                if (categoryPermission.Count > 0)
                {
                    var categoryId = Convert.ToInt32(this.ddlCategory.SelectedValue);
                    var name = this.txtName.Text.Trim();
                    var title = this.txtTitle.Text.Trim();
                    var docNumber = this.txtDocumentNumber.Text.Trim();
                    var keyword = this.txtKeyword.Text.Trim();
                    var revisionId = Convert.ToInt32(this.ddlRevision.SelectedValue);
                    var docTypeId = Convert.ToInt32(this.ddlDocumentType.SelectedValue);
                    var statusId = Convert.ToInt32(this.ddlStatus.SelectedValue);
                    var receiveFromId = Convert.ToInt32(this.ddlReceivedFrom.SelectedValue);
                    var disciplineId = Convert.ToInt32(this.ddlDiscipline.SelectedValue);
                    var languageId = Convert.ToInt32(this.ddlLanguage.SelectedValue);
                    var dateFrom = this.txtDateFrom.SelectedDate;
                    var dateTo = this.txtDateTo.SelectedDate;
                    var transmittalNumber = this.txtTransmittalNumber.Text.Trim();
                    var fileType = this.ddlFileType.SelectedValue;
                    var searchAll = this.txtSearchAll.Text.Trim();//

                    var folderPermission = new List<int>();
                    var specialDocIdsNotInPermission = new List<int>();
                    var specialDocIdsInPermission = new List<int>();


                    if (!this.AdminGroup.Contains(UserSession.Current.RoleId))
                    {
                        if (this.ddlCategory.SelectedValue == "0")
                        {
                            folderPermission = this.groupDataPermissionService
                                .GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault())
                                .Where(t => categoryPermission.Contains(Convert.ToInt32(t.CategoryIdList))
                                        && !string.IsNullOrEmpty(t.FolderIdList))
                                .Select(t => Convert.ToInt32(t.FolderIdList))
                                .OrderBy(t => t).ToList();

                            folderPermission = folderPermission
                                        .Union(this.userDataPermissionService.GetByUserId(UserSession.Current.User.Id)
                                                .Where(t => categoryPermission.Contains(t.CategoryId.GetValueOrDefault()))
                                                .Select(t => t.FolderId.GetValueOrDefault()))
                                                .Distinct().ToList();
                        }
                        else
                        {
                            folderPermission = this.groupDataPermissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault())
                                .Where(t => t.CategoryIdList == categoryId.ToString()
                                        && !string.IsNullOrEmpty(t.FolderIdList))
                                .Select(t => Convert.ToInt32(t.FolderIdList))
                                .OrderBy(t => t).ToList();
                            folderPermission = folderPermission
                                        .Union(this.userDataPermissionService.GetByUserId(UserSession.Current.User.Id)
                                                .Where(t => t.CategoryId == categoryId)
                                                .Select(t => t.FolderId.GetValueOrDefault()))
                                                .Distinct().ToList();
                        }

                        //specialDocIdsNotInPermission = this.specialDocPermissionService.GetAllNotByUser(UserSession.Current.User.Id)
                        //                                    .Select(t => t.DocId.GetValueOrDefault())
                        //                                    .Distinct()
                        //                                    .ToList();

                        //specialDocIdsInPermission = this.specialDocPermissionService.GetAllByUser(UserSession.Current.User.Id)
                        //                                .Select(t => t.DocId.GetValueOrDefault())
                        //                                .Distinct()
                        //                                .ToList();

                        //specialDocIdsNotInPermission = specialDocIdsNotInPermission.Where(t => !specialDocIdsInPermission.Contains(t)).ToList();

                    }

                    //var lisDoc = this.documentService.SearchDocument(
                    //    categoryId,
                    //    categoryPermission,
                    //    name,
                    //    title,
                    //    docNumber,
                    //    keyword,
                    //    revisionId,
                    //    docTypeId,
                    //    statusId,
                    //    receiveFromId,
                    //    disciplineId,
                    //    languageId,
                    //    dateFrom,
                    //    dateTo,
                    //    transmittalNumber,
                    //    pageSize,
                    //    startingRecordNumber,
                    //    searchAll,
                    //    fileType,
                    //    folderPermission,
                    //    //specialDocIdsNotInPermission,
                    //    null, null);
                    //var lisDoc = this.documentService.SearchDocument(
                    //    categoryId,
                    //    searchAll);
                    //lisDoc = lisDoc.Where(t => folderPermission.Count == 0 || folderPermission.Contains(t.FolderID.Value)).ToList();

                    //this.grdDocument.VirtualItemCount = this.documentService.GetItemCount(
                    //    categoryId,
                    //    categoryPermission,
                    //    name,
                    //    title,
                    //    docNumber,
                    //    keyword,
                    //    revisionId,
                    //    docTypeId,
                    //    statusId,
                    //    receiveFromId,
                    //    disciplineId,
                    //    languageId,
                    //    dateFrom,
                    //    dateTo,
                    //    transmittalNumber,
                    //    searchAll,
                    //    fileType,
                    //    folderPermission,
                    //    //specialDocIdsNotInPermission,
                    //    null, null);

                    this.grdDocument.DataSource = new List<Document>();
                }
                else
                {
                    this.grdDocument.DataSource = new List<Document>();
                }
            }
        }

        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());

            var listRelateDoc = this.documentService.GetAllRelateDocument(docId);
            if (listRelateDoc != null)
            {
                foreach (var objDoc in listRelateDoc)
                {
                    objDoc.IsDelete = true;
                    objDoc.LastUpdatedBy = UserSession.Current.User.Id;
                    objDoc.LastUpdatedDate = DateTime.Now;
                    this.documentService.Update(objDoc);
                }
            }
        }

        /// <summary>
        /// The grd document_ item command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string abc = e.CommandName;
        }

        /// <summary>
        /// The rad menu_ item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void radMenu_ItemClick(object sender, RadMenuEventArgs e)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The btn search_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ////var pageSize = this.grdDocument.PageSize;
            ////var currentPage = this.grdDocument.CurrentPageIndex;
            ////var startingRecordNumber = currentPage * pageSize;
            ////this.grdDocument.VirtualItemCount = this.documentService.GetItemCount(
            ////    this.CategoryID,
            ////    this.Name,
            ////    this.DocTitle,
            ////    this.DocumentNumber,
            ////    this.Keywords,
            ////    this.RevisionID,
            ////    this.DocumentTypeID,
            ////    this.StatusID,
            ////    this.ReceiveFromID,
            ////    this.DisciplineID,
            ////    this.LanguageID,
            ////    this.DateFrom,
            ////    this.DateTo,
            ////    this.TransmittalNumber);
            ////this.LoadDocuments(pageSize, startingRecordNumber, true);
            this.grdDocument.CurrentPageIndex = 0;
            this.grdDocument.Rebind();
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {
            var listFileType = ConfigurationManager.AppSettings.Get("Extension").Split(',').OrderBy(t => t).ToList();
            listFileType.Insert(0, string.Empty);
            this.ddlFileType.DataSource = listFileType;
            this.ddlFileType.DataBind();

            var revisionList = this.revisionService.GetAll();
            revisionList.Insert(0, new Revision() { Name = string.Empty });
            this.ddlRevision.DataSource = revisionList;
            this.ddlRevision.DataValueField = "ID";
            this.ddlRevision.DataTextField = "Name";
            this.ddlRevision.DataBind();

            var documentTypeList = this.documentTypeService.GetAll().OrderBy(t => t.Name).ToList();
            documentTypeList.Insert(0, new DocumentType() { Name = string.Empty });
            this.ddlDocumentType.DataSource = documentTypeList;
            this.ddlDocumentType.DataValueField = "ID";
            this.ddlDocumentType.DataTextField = "Name";
            this.ddlDocumentType.DataBind();

            var statusList = this.statusService.GetAll().OrderBy(t => t.Name).ToList();
            statusList.Insert(0, new Status { Name = string.Empty });
            this.ddlStatus.DataSource = statusList;
            this.ddlStatus.DataValueField = "ID";
            this.ddlStatus.DataTextField = "Name";
            this.ddlStatus.DataBind();

            var receivedFromList = this.receivedFromService.GetAll().OrderBy(t => t.Name).ToList();
            receivedFromList.Insert(0, new ReceivedFrom() { Name = string.Empty });
            this.ddlReceivedFrom.DataSource = receivedFromList;
            this.ddlReceivedFrom.DataValueField = "ID";
            this.ddlReceivedFrom.DataTextField = "Name";
            this.ddlReceivedFrom.DataBind();

            var disciplineList = this.disciplineService.GetAll().OrderBy(t => t.Name).ToList();
            disciplineList.Insert(0, new Discipline() { Name = string.Empty });
            this.ddlDiscipline.DataSource = disciplineList;
            this.ddlDiscipline.DataValueField = "ID";
            this.ddlDiscipline.DataTextField = "Name";
            this.ddlDiscipline.DataBind();

            var languageList = this.languageService.GetAll().OrderBy(t => t.Name).ToList();
            languageList.Insert(0, new Language() { Name = string.Empty });
            this.ddlLanguage.DataSource = languageList;
            this.ddlLanguage.DataValueField = "ID";
            this.ddlLanguage.DataTextField = "Name";
            this.ddlLanguage.DataBind();
        }

        protected void grdDocument_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                var item = e.Item as GridEditableItem;
                var ddlRevision = item.FindControl("ddlRevision") as RadComboBox;
                var ddlStatus = item.FindControl("ddlStatus") as RadComboBox;
                var ddlDiscipline = item.FindControl("ddlDiscipline") as RadComboBox;
                var ddlDocumentType = item.FindControl("ddlDocumentType") as RadComboBox;
                var ddlReceivedFrom = item.FindControl("ddlReceivedFrom") as RadComboBox;
                var txtReceivedDate = item.FindControl("txtReceivedDate") as RadDatePicker;
                var txtName = item.FindControl("txtName") as TextBox;
                var txtDocumentNumber = item.FindControl("txtDocumentNumber") as TextBox;
                var txtTitle = item.FindControl("txtTitle") as TextBox;
                var txtRemark = item.FindControl("txtRemark") as TextBox;
                var txtWell = item.FindControl("txtWell") as TextBox;

                var txtTransmittalNumber = item.FindControl("txtTransmittalNumber") as TextBox;

                var docId = Convert.ToInt32(item.GetDataKeyValue("ID"));

                var objDoc = this.documentService.GetById(docId);
                var oldName = objDoc.Name;
                var currentRevision = objDoc.RevisionID;
                var newRevision = Convert.ToInt32(ddlRevision.SelectedValue);
                var watcherService = new ServiceController("EDMSFolderWatcher");
                if (currentRevision != 0 && currentRevision != null && currentRevision != newRevision)
                {
                    var newObjDoc = new Document
                    {
                        DocIndex = objDoc.DocIndex + 1,
                        ParentID = objDoc.ParentID,
                        FolderID = objDoc.FolderID,
                        DirName = objDoc.DirName,
                        FilePath = objDoc.FilePath,
                        RevisionFilePath = objDoc.RevisionFilePath,
                        FileExtension = objDoc.FileExtension,
                        FileExtensionIcon = objDoc.FileExtensionIcon,
                        FileNameOriginal = objDoc.FileNameOriginal,
                        KeyWords = objDoc.KeyWords,
                        IsDelete = false,
                        CreatedDate = DateTime.Now,
                        CreatedBy = UserSession.Current.User.Id,

                        Name = txtName.Text.Trim(),
                        DocumentNumber = txtDocumentNumber.Text.Trim(),
                        Title = txtTitle.Text.Trim(),
                        RevisionID = Convert.ToInt32(ddlRevision.SelectedValue),

                        RevisionName = ddlRevision.Text,
                        DocumentTypeName = ddlDocumentType.SelectedItem.Text,
                        StatusName = ddlStatus.SelectedItem.Text,
                        DisciplineName = ddlDiscipline.SelectedItem.Text,
                        ReceivedFromName = ddlReceivedFrom.SelectedItem.Text,

                        StatusID = Convert.ToInt32(ddlStatus.SelectedValue),
                        DisciplineID = Convert.ToInt32(ddlDiscipline.SelectedValue),
                        DocumentTypeID = Convert.ToInt32(ddlDocumentType.SelectedValue),
                        ReceivedFromID = Convert.ToInt32(ddlReceivedFrom.SelectedValue),
                        ReceivedDate = txtReceivedDate.SelectedDate,
                        Remark = txtRemark.Text.Trim(),
                        Well = txtWell.Text.Trim(),

                        TransmittalNumber = txtTransmittalNumber.Text.Trim()
                    }; ////Utilities.Utility.Clone(objDoc);

                    if (!string.IsNullOrEmpty(newObjDoc.RevisionName))
                    {
                        newObjDoc.RevisionFileName = newObjDoc.RevisionName + "_" + newObjDoc.Name;
                    }
                    else
                    {
                        newObjDoc.RevisionFileName = newObjDoc.Name;
                    }


                    newObjDoc.RevisionFilePath = newObjDoc.RevisionFilePath.Substring(
                        0, newObjDoc.RevisionFilePath.LastIndexOf('/')) + "/" + DateTime.Now.ToString("ddMMyyhhmmss") + "_" + newObjDoc.RevisionFileName;

                    // Allway check revision file is exist when update
                    ////var filePath = Server.MapPath(newObjDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                    ////var revisionFilePath = Server.MapPath(newObjDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                    var filePath = Server.MapPath(HostingEnvironment.ApplicationVirtualPath == "/" ? newObjDoc.FilePath : newObjDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                    var revisionFilePath = Server.MapPath(HostingEnvironment.ApplicationVirtualPath == "/" ? newObjDoc.RevisionFilePath : newObjDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                    if (File.Exists(filePath) && !File.Exists(revisionFilePath))
                    {
                        File.Copy(filePath, revisionFilePath, true);
                    }
                    // End check

                    if (currentRevision < newRevision)
                    {
                        objDoc.IsLeaf = false;
                        newObjDoc.IsLeaf = true;
                        if (objDoc.ParentID == null)
                        {
                            newObjDoc.ParentID = objDoc.ID;
                        }

                        this.documentService.Update(objDoc);
                    }
                    else
                    {
                        newObjDoc.IsLeaf = false;

                        ////filePath = Server.MapPath(newObjDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                        ////revisionFilePath = Server.MapPath(objDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                        filePath = Server.MapPath(HostingEnvironment.ApplicationVirtualPath == "/"
                                                   ? newObjDoc.FilePath
                                                   : newObjDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                        revisionFilePath = Server.MapPath(HostingEnvironment.ApplicationVirtualPath == "/"
                                                    ? objDoc.RevisionFilePath
                                                    : objDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                        if (Utilities.Utility.ServiceIsAvailable(ServiceName))
                        {
                            watcherService.ExecuteCommand(128);
                        }

                        if (File.Exists(revisionFilePath))
                        {
                            File.Copy(revisionFilePath, filePath, true);
                        }

                        if (Utilities.Utility.ServiceIsAvailable(ServiceName))
                        {
                            watcherService.ExecuteCommand(129);
                        }
                    }

                    this.documentService.Insert(newObjDoc);

                    if (objDoc.Name != txtName.Text.Trim())
                    {
                        if (Utilities.Utility.ServiceIsAvailable(ServiceName))
                        {
                            watcherService.ExecuteCommand(128);
                        }

                        if (File.Exists(filePath))
                        {
                            File.Move(filePath, filePath.Replace(oldName, txtName.Text.Trim()));
                        }

                        if (Utilities.Utility.ServiceIsAvailable(ServiceName))
                        {
                            watcherService.ExecuteCommand(129);
                        }

                        var listDocRename = this.documentService.GetSpecificDocument(oldName, objDoc.DirName);
                        foreach (var document in listDocRename)
                        {
                            document.Name = txtName.Text.Trim();
                            document.FileNameOriginal = txtName.Text.Trim();
                            if (!string.IsNullOrEmpty(document.RevisionName))
                            {
                                document.RevisionFileName = document.RevisionName + "_" + txtName.Text.Trim();
                            }
                            else
                            {
                                document.RevisionFileName = txtName.Text.Trim();
                            }

                            document.FilePath = document.FilePath.Replace(oldName, txtName.Text.Trim());

                            this.documentService.Update(document);
                        }
                    }
                }
                else
                {
                    // Allway check revision file is exist when update
                    ////var filePath = Server.MapPath(objDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                    ////var revisionFilePath = Server.MapPath(objDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                    var filePath = Server.MapPath(HostingEnvironment.ApplicationVirtualPath == "/"
                                                   ? objDoc.FilePath
                                                   : objDoc.FilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));
                    var revisionFilePath = Server.MapPath(HostingEnvironment.ApplicationVirtualPath == "/"
                                                ? objDoc.RevisionFilePath
                                                : objDoc.RevisionFilePath.Replace("/" + HostingEnvironment.ApplicationVirtualPath, "../.."));

                    if (File.Exists(filePath) && !File.Exists(revisionFilePath))
                    {
                        File.Copy(filePath, revisionFilePath, true);
                    }
                    // End check

                    if (objDoc.Name != txtName.Text.Trim())
                    {
                        if (watcherService.Status == ServiceControllerStatus.Running)
                        {
                            watcherService.ExecuteCommand(128);
                        }

                        if (File.Exists(filePath))
                        {
                            File.Move(filePath, filePath.Replace(oldName, txtName.Text.Trim()));
                        }

                        if (watcherService.Status == ServiceControllerStatus.Running)
                        {
                            watcherService.ExecuteCommand(129);
                        }

                        var listDocRename = this.documentService.GetSpecificDocument(oldName, objDoc.DirName);
                        foreach (var document in listDocRename)
                        {
                            document.Name = txtName.Text.Trim();
                            document.FileNameOriginal = txtName.Text.Trim();
                            if (!string.IsNullOrEmpty(document.RevisionName))
                            {
                                document.RevisionFileName = document.RevisionName + "_" + txtName.Text.Trim();
                            }
                            else
                            {
                                document.RevisionFileName = txtName.Text.Trim();
                            }

                            document.FilePath = document.FilePath.Replace(oldName, txtName.Text.Trim());

                            if (document.ID == objDoc.ID)
                            {
                                document.DocumentNumber = txtDocumentNumber.Text;
                                document.Title = txtTitle.Text;
                                document.RevisionID = Convert.ToInt32(ddlRevision.SelectedValue);

                                document.RevisionName = ddlRevision.SelectedItem.Text;
                                document.DocumentTypeName = ddlDocumentType.SelectedItem.Text;
                                document.StatusName = ddlStatus.SelectedItem.Text;
                                document.DisciplineName = ddlDiscipline.SelectedItem.Text;
                                document.ReceivedFromName = ddlReceivedFrom.SelectedItem.Text;

                                document.DocumentTypeID = Convert.ToInt32(ddlDocumentType.SelectedValue);
                                document.StatusID = Convert.ToInt32(ddlStatus.SelectedValue);
                                document.DisciplineID = Convert.ToInt32(ddlDiscipline.SelectedValue);
                                document.ReceivedFromID = Convert.ToInt32(ddlReceivedFrom.SelectedValue);
                                document.ReceivedDate = txtReceivedDate.SelectedDate;
                                document.Remark = txtRemark.Text.Trim();
                                document.Well = txtWell.Text.Trim();
                                document.TransmittalNumber = txtTransmittalNumber.Text.Trim();

                                document.LastUpdatedBy = UserSession.Current.User.Id;
                                document.LastUpdatedDate = DateTime.Now;
                            }

                            this.documentService.Update(document);
                        }
                    }
                    else
                    {
                        objDoc.Name = txtName.Text.Trim();
                        objDoc.DocumentNumber = txtDocumentNumber.Text.Trim();
                        objDoc.Title = txtTitle.Text.Trim();
                        objDoc.RevisionID = Convert.ToInt32(ddlRevision.SelectedValue);
                        objDoc.RevisionName = ddlRevision.Text;
                        objDoc.DocumentTypeName = ddlDocumentType.SelectedItem.Text;
                        objDoc.StatusName = ddlStatus.SelectedItem.Text;
                        objDoc.DisciplineName = ddlDiscipline.SelectedItem.Text;
                        objDoc.ReceivedFromName = ddlReceivedFrom.SelectedItem.Text;
                        objDoc.StatusID = Convert.ToInt32(ddlStatus.SelectedValue);
                        objDoc.DisciplineID = Convert.ToInt32(ddlDiscipline.SelectedValue);
                        objDoc.DocumentTypeID = Convert.ToInt32(ddlDocumentType.SelectedValue);
                        objDoc.ReceivedFromID = Convert.ToInt32(ddlReceivedFrom.SelectedValue);
                        objDoc.ReceivedDate = txtReceivedDate.SelectedDate;
                        objDoc.Remark = txtRemark.Text.Trim();
                        objDoc.Well = txtWell.Text.Trim();
                        objDoc.TransmittalNumber = txtTransmittalNumber.Text.Trim();
                        if (!string.IsNullOrEmpty(objDoc.RevisionName))
                        {
                            objDoc.RevisionFileName = objDoc.RevisionName + "_" + objDoc.Name;
                        }
                        else
                        {
                            objDoc.RevisionFileName = objDoc.Name;
                        }

                        objDoc.LastUpdatedBy = UserSession.Current.User.Id;
                        objDoc.LastUpdatedDate = DateTime.Now;

                        this.documentService.Update(objDoc);
                    }
                }
            }
        }

        protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                var item = e.Item as GridEditableItem;
                var ddlRevision = item.FindControl("ddlRevision") as RadComboBox;
                var ddlStatus = item.FindControl("ddlStatus") as RadComboBox;
                var ddlDiscipline = item.FindControl("ddlDiscipline") as RadComboBox;
                var ddlDocumentType = item.FindControl("ddlDocumentType") as RadComboBox;
                var ddlReceivedFrom = item.FindControl("ddlReceivedFrom") as RadComboBox;
                var txtReceivedDate = item.FindControl("txtReceivedDate") as RadDatePicker;
                var txtName = item.FindControl("txtName") as TextBox;
                var txtDocumentNumber = item.FindControl("txtDocumentNumber") as TextBox;
                var txtTitle = item.FindControl("txtTitle") as TextBox;
                var txtRemark = item.FindControl("txtRemark") as TextBox;
                var txtWell = item.FindControl("txtWell") as TextBox;
                var txtTransmittalNumber = item.FindControl("txtTransmittalNumber") as TextBox;

                txtReceivedDate.DatePopupButton.Visible = false;

                var name = (item.FindControl("Name") as HiddenField).Value;
                var revisionId = (item.FindControl("RevisionID") as HiddenField).Value;
                var statusId = (item.FindControl("StatusID") as HiddenField).Value;
                var disciplineId = (item.FindControl("DisciplineID") as HiddenField).Value;
                var documentTypeId = (item.FindControl("DocumentTypeID") as HiddenField).Value;
                var receivedFromId = (item.FindControl("ReceivedFromID") as HiddenField).Value;
                var receivedDate = (item.FindControl("ReceivedDate") as HiddenField).Value;
                var well = (item.FindControl("Well") as HiddenField).Value;
                var remark = (item.FindControl("Remark") as HiddenField).Value;
                var title = (item.FindControl("Title") as HiddenField).Value;
                var documentNumber = (item.FindControl("DocumentNumber") as HiddenField).Value;
                var transmittalNumber = (item.FindControl("TransmittalNumber") as HiddenField).Value;

                if (!string.IsNullOrEmpty(receivedDate))
                {
                    txtReceivedDate.SelectedDate = Convert.ToDateTime(receivedDate);
                }

                txtName.Text = name;
                txtTitle.Text = title;
                txtRemark.Text = remark;
                txtWell.Text = well;
                txtDocumentNumber.Text = documentNumber;
                txtTransmittalNumber.Text = transmittalNumber;

                var revisionList = this.revisionService.GetAll();
                revisionList.Insert(0, new Revision() { Name = string.Empty });
                ddlRevision.DataSource = revisionList;
                ddlRevision.DataValueField = "ID";
                ddlRevision.DataTextField = "Name";
                ddlRevision.DataBind();
                ddlRevision.SelectedValue = revisionId;

                var documentTypeList = this.documentTypeService.GetAll();
                documentTypeList.Insert(0, new DocumentType() { Name = string.Empty });
                ddlDocumentType.DataSource = documentTypeList;
                ddlDocumentType.DataValueField = "ID";
                ddlDocumentType.DataTextField = "Name";
                ddlDocumentType.DataBind();
                ddlDocumentType.SelectedValue = documentTypeId;

                var statusList = this.statusService.GetAll();
                statusList.Insert(0, new Status { Name = string.Empty });
                ddlStatus.DataSource = statusList;
                ddlStatus.DataValueField = "ID";
                ddlStatus.DataTextField = "Name";
                ddlStatus.DataBind();
                ddlStatus.SelectedValue = statusId;

                var receivedFromList = this.receivedFromService.GetAll();
                receivedFromList.Insert(0, new ReceivedFrom() { Name = string.Empty });
                ddlReceivedFrom.DataSource = receivedFromList;
                ddlReceivedFrom.DataValueField = "ID";
                ddlReceivedFrom.DataTextField = "Name";
                ddlReceivedFrom.DataBind();
                ddlReceivedFrom.SelectedValue = receivedFromId;

                var disciplineList = this.disciplineService.GetAll();
                disciplineList.Insert(0, new Discipline() { Name = string.Empty });
                ddlDiscipline.DataSource = disciplineList;
                ddlDiscipline.DataValueField = "ID";
                ddlDiscipline.DataTextField = "Name";
                ddlDiscipline.DataBind();
                ddlDiscipline.SelectedValue = disciplineId;
            }
        }

        private void LoadListPanel()
        {
            var listId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("ListID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), listId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "LIST" });

                this.radPbList.DataSource = permissions;
                this.radPbList.DataFieldParentID = "ParentId";
                this.radPbList.DataFieldID = "Id";
                this.radPbList.DataValueField = "Id";
                this.radPbList.DataTextField = "MenuName";
                this.radPbList.DataBind();
                this.radPbList.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbList.Items[0].Items)
                {
                    item.ImageUrl = @"Images/listmenu.png";
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                }
            }
        }

        private void LoadSystemPanel()
        {
            var systemId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("SystemID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), systemId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "SYSTEM" });

                this.radPbSystem.DataSource = permissions;
                this.radPbSystem.DataFieldParentID = "ParentId";
                this.radPbSystem.DataFieldID = "Id";
                this.radPbSystem.DataValueField = "Id";
                this.radPbSystem.DataTextField = "MenuName";
                this.radPbSystem.DataBind();
                this.radPbSystem.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbSystem.Items[0].Items)
                {
                    item.ImageUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Icon;
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                }
            }
        }

        protected void btnGotoFolder_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(this.lblDocId.Value))
            {
                var docObj = this.documentService.GetById(Convert.ToInt32(this.lblDocId.Value));
                Response.Redirect("DocumentsHome.aspx?doctype=" + docObj.CategoryID + "&docId=" + lblDocId.Value + "&folId=" + docObj.FolderID);
            }
        }
    }
}

