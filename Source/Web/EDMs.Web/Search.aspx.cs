﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;

namespace EDMs.Web
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.ServiceProcess;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    using ZetaLongPaths;
    using System.Web.UI.HtmlControls;

    /// <summary>
    /// Class customer
    /// </summary>
    public partial class Search : Page
    {
        private readonly PermissionService permissionService = new PermissionService();



        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly StatusService statusService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The received from.
        /// </summary>
        private readonly ReceivedFromService receivedFromService;

        /// <summary>
        /// The language service.
        /// </summary>
        private readonly LanguageService languageService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The category service.
        /// </summary>
        private readonly CategoryService categoryService;

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string unreadPattern = @"\(\d+\)";

        protected const string ServiceName = "EDMSFolderWatcher";

        private readonly SpecialDocPermissionService specialDocPermissionService;

        private int CategoryID
        {
            get
            {
                if (!string.IsNullOrEmpty(this.ddlCategory.SelectedValue))
                {
                    return Convert.ToInt32(this.ddlCategory.SelectedValue);
                }

                return 0;
            }
        }


        private readonly DocPropertiesViewService docPropertiesViewService = new DocPropertiesViewService();

        private readonly GroupFunctionService _GroupfunctionService = new GroupFunctionService();
        private readonly FolderPermissionService _FolderPermisstionService = new FolderPermissionService();
        private readonly FilePermissionService _FilePermissionService = new FilePermissionService();

        private readonly RoleService roleService;

        private readonly UserService userService;

        private readonly FolderService folderService;

        private const string RegexValidateEmail =
            @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";

        private List<int> AdminGroup
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("GroupAdminList").Split(',').Select(t => Convert.ToInt32(t)).ToList();
            }
        }

        public Search()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.documentService = new DocumentService();
            this.statusService = new StatusService();
            this.disciplineService = new DisciplineService();
            this.receivedFromService = new ReceivedFromService();
            this.languageService = new LanguageService();
            this.categoryService = new CategoryService();
            //this.groupDataPermissionService = new GroupDataPermissionService();
            //this.userDataPermissionService = new UserDataPermissionService();
            this.roleService = new RoleService();
            this.userService = new UserService();
            this.folderService = new FolderService();
            this.specialDocPermissionService = new SpecialDocPermissionService();
        }
        private void LoadDocConfigGridView(int category)
        {
            var isViewByGroup = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("ViewSettingByGroup"));
            var selectedProperty = new List<string>();
            var totalProperty = Convert.ToInt32(ConfigurationManager.AppSettings.Get("TotalProperty"));
            var docPropertiesView = this.docPropertiesViewService.GetByCategory(category);
            if (docPropertiesView != null && docPropertiesView.PropertyIndex != "")
            {
                var temp =
                    docPropertiesView.PropertyIndex.Split(',').Where(t => !string.IsNullOrEmpty(t.Trim())).Select(
                        t => t.Trim()).ToList();
                selectedProperty.AddRange(temp);
            }
            selectedProperty = selectedProperty.Distinct().ToList();
            for (int i = 1; i <= totalProperty; i++)
            {
                var column = this.grdDocument.MasterTableView.GetColumn("Index" + i);
                if (column != null)
                {
                    column.Visible = selectedProperty.Contains(i.ToString());
                }
            }
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Form.DefaultButton = this.btnSearch.UniqueID;
            int categoryID;
            int.TryParse(this.Request.QueryString["ctgr"], out categoryID);
            if (!Page.IsPostBack)
            {
                this.IsAdmin.Value = this.AdminGroup.Contains(UserSession.Current.RoleId) ? "true" : "false";
                var categoryPermission = this._FolderPermisstionService.GetCategoryByUsser(UserSession.Current.User.Id).Select(t => t.CategoryId.GetValueOrDefault()).Distinct().ToList();
                var listCategory = this.AdminGroup.Contains(UserSession.Current.RoleId)
                                    ? this.categoryService.GetAll()
                                    : this.categoryService.GetAll().Where(t => categoryPermission.Any(x => x == t.ID)).ToList();
                if (listCategory.Any())
                {
                    listCategory.Insert(0, new Category() { ID = 0, Name = "All" });
                    this.ddlCategory.DataSource = listCategory;
                    this.ddlCategory.DataValueField = "ID";
                    this.ddlCategory.DataTextField = "Name";
                    this.ddlCategory.DataBind();
                    this.ddlCategory.SelectedValue = categoryID.ToString();
                    foreach (var category in listCategory)
                    {
                        category.ParentId = -1;
                    }
                    listCategory.RemoveAt(0);
                    listCategory.Insert(0, new Category() { ID = -1, Name = "DOCUMENT GROUP" });
                    this.radPbCategories.DataSource = listCategory;
                    this.radPbCategories.DataFieldParentID = "ParentId";
                    this.radPbCategories.DataFieldID = "Id";
                    this.radPbCategories.DataValueField = "Id";
                    this.radPbCategories.DataTextField = "Name";
                    this.radPbCategories.DataBind();
                    this.radPbCategories.Items[0].Expanded = true;
                    foreach (RadPanelItem item in this.radPbCategories.Items[0].Items)
                    {
                        item.ImageUrl = @"Images/category2.png";
                        item.NavigateUrl = "DocumentsHome.aspx?doctype=" + item.Value;
                    }
                }
                this.LoadListPanel();
                this.LoadSystemPanel();
                this.LoadDocConfigGridView(categoryID);
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                grdDocument.MasterTableView.SortExpressions.Clear();
                grdDocument.MasterTableView.GroupByExpressions.Clear();
                grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                grdDocument.MasterTableView.SortExpressions.Clear();
                grdDocument.MasterTableView.GroupByExpressions.Clear();
                ////grdDocument.MasterTableView.CurrentPageIndex = grdDocument.MasterTableView.PageCount - 1;
                grdDocument.Rebind();
            }
            //else if (e.Argument == "SendNotification")
            //{
            //    var listSelectedDoc = new List<Document>();
            //    foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
            //    {
            //        var docId = Convert.ToInt32(selectedItem.GetDataKeyValue("ID"));
            //        var docItem = this.documentService.GetById(docId);
            //        if (docItem != null)
            //        {
            //            listSelectedDoc.Add(docItem);
            //        }
            //        selectedItem.Selected = false;
            //    }
            //    foreach (var groupFolder in listSelectedDoc.GroupBy(t => t.FolderID))
            //    {
            //        var folder = this.folderService.GetById(groupFolder.Key.GetValueOrDefault());
            //        if (folder != null)
            //        {
            //            var category = this.categoryService.GetById(folder.CategoryID.GetValueOrDefault());
            //            var smtpClient = new SmtpClient
            //            {
            //                DeliveryMethod = SmtpDeliveryMethod.Network,
            //                UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
            //                EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
            //                Host = ConfigurationManager.AppSettings["Host"],
            //                Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
            //                Credentials = new NetworkCredential(ConfigurationManager.AppSettings["Email"], ConfigurationManager.AppSettings["EmailPass"])
            //            };
            //            var message = new MailMessage();
            //            message.From = new MailAddress(ConfigurationManager.AppSettings["Email"], ConfigurationManager.AppSettings["EmailName"]);
            //            message.Subject = "Availability of New/Updated " + (category != null ? category.Name : string.Empty) + " Documents on DocLib";
            //            //ConfigurationManager.AppSettings["SubtitleName"];
            //            message.BodyEncoding = new UTF8Encoding();
            //            message.IsBodyHtml = true;
            //            message.Body = @"Dear users,<br/><br/>
            //                    Please be informed that the following documents are now available on the BDPOC Document Library System for your information and implementation.<br/><br/>
            //                    <table border='1' cellspacing='0'>
            //                     <tr>
            //                      <th style='text-align:center; width:40px'>No.</th>
            //                      <th style='text-align:center; width:330px'>Document Number</th>
            //                      <th style='text-align:center; width:330px'>Document Title</th>
            //                      <th style='text-align:center; width:60px'>Revision</th>
            //                      <th style='text-align:center; width:120px'>Status</th>
            //                      <th style='text-align:center; width:120px'>Discipline</th>
            //                      <th style='text-align:center; width:120px'>Document Type</th>
            //                     </tr>";
            //            var subBody = string.Empty;
            //            var count = 0;
            //            foreach (var document in groupFolder)
            //            {
            //                var port = ConfigurationSettings.AppSettings.Get("DocLibPort");
            //                count += 1;
            //                subBody += @"<tr>
            //            <td>" + count + @"</td>
            //            <td><a href='" + ConfigurationSettings.AppSettings.Get("WebAddress") + (!string.IsNullOrEmpty(port) ? ":" + port : string.Empty)
            //                            + document.FilePath + "' download='" + document.DocumentNumber + "'>"
            //                            + document.DocumentNumber + @"</a></td>
            //            <td>"
            //                            + document.Title + @"</td>
            //            <td>"
            //                            + document.RevisionName + @"</td>
            //                    <td>"
            //                               + document.StatusName + @"</td>
            //                    <td>"
            //                               + document.DisciplineName + @"</td>
            //                    <td>"
            //                               + document.DocumentTypeName + @"</td>";
            //            }
            //            message.Body += subBody + @"</table>
            //                    <br/>
            //                    Thanks and regards,<br/><br/>
            //                            " + UserSession.Current.User.FullName + ". <br/>";
            //            var groupsInPermission = this.groupDataPermissionService
            //                                        .GetAllByFolder(folder.ID.ToString())
            //                                        .Select(t => t.RoleId).Distinct().ToList();
            //            var listGroup = this.roleService
            //                                .GetAll()
            //                                .Where(t => groupsInPermission.Contains(t.Id)
            //                                            && !this.AdminGroup.Contains(t.Id)
            //                                            && !string.IsNullOrEmpty(t.Email)
            //                                            && Regex.IsMatch(t.Email, RegexValidateEmail)).ToList();
            //            var usersInPermission = this.userDataPermissionService
            //                                        .GetAllByFolder(folder.ID)
            //                                        .Select(t => t.UserId).Distinct().ToList();
            //            var listUser = this.userService
            //                                .GetAll()
            //                                .Where(t => usersInPermission.Contains(t.Id)
            //                                            && !groupsInPermission.Contains(t.RoleId)
            //                                            && !string.IsNullOrEmpty(t.Email)
            //                                            && Regex.IsMatch(t.Email, RegexValidateEmail)).ToList();
            //            var groupEmailList = listGroup.Select(t => t.Email);
            //            var userEmailList = listUser.Select(t => t.Email);
            //            var emailList = groupEmailList.Union(userEmailList).Distinct();
            //            foreach (var email in emailList)
            //            {
            //                message.To.Add(new MailAddress(email));
            //            }
            //            smtpClient.Send(message);
            //        }
            //    }
            //}
        }

        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var categoryPermission = new List<int>();
            if (!this.AdminGroup.Contains(UserSession.Current.RoleId))
            {
                categoryPermission = this._FolderPermisstionService.GetCategoryByUsser(UserSession.Current.User.Id).Select(t => t.CategoryId.GetValueOrDefault()).Distinct().ToList();
            }
            else
            {
                categoryPermission = this.categoryService.GetAll().Select(t => t.ID).ToList();
            }
            if (categoryPermission.Count > 0)
            {
                var categoryId = Convert.ToInt32(this.ddlCategory.SelectedValue);
                var searchAll = this.txtSearchAll.Text.Trim();
                var folderPermission = new List<int>();
                var specialDocIdsNotInPermission = new List<int>();
                var specialDocIdsInPermission = new List<int>();
                if (!this.AdminGroup.Contains(UserSession.Current.RoleId))
                {
                    if (this.ddlCategory.SelectedValue == "0")
                    {
                        folderPermission = this._FolderPermisstionService.GetByUserId(UserSession.Current.User.Id)
                                       .Where(t => categoryPermission.Contains(t.CategoryId.GetValueOrDefault()) && (t.Folder_Read.GetValueOrDefault() || t.Folder_Write.GetValueOrDefault())).Select(t => t.FolderId.GetValueOrDefault()).Distinct().ToList();
                    }
                    else
                    {
                        folderPermission = this._FolderPermisstionService.GetByUserId(UserSession.Current.User.Id, categoryId)
                                       .Where(t => (t.Folder_Read.GetValueOrDefault() || t.Folder_Write.GetValueOrDefault())).Select(t => t.FolderId.GetValueOrDefault()).Distinct().ToList();
                    }
                }
                var listDoc = new List<Document>();
                if (!this.AdminGroup.Contains(UserSession.Current.RoleId))
                {
                    if (this.rbAll.Checked)
                    {
                        listDoc = this.documentService.SearchDocument(folderPermission, searchAll);
                    }
                    else if (this.rbDetail.Checked)
                    {
                        listDoc = this.documentService.SearchDocument(folderPermission, this.txtFileName.Text.Trim(), this.txtDocumentNumber.Text.Trim(), this.txtTitle.Text.Trim(), this.txtRevision.Text.Trim(), this.txtFromTo.Text.Trim(), this.txtDiscipline.Text.Trim(), this.txtDoctype.Text.Trim(), this.txtPlatform.Text.Trim(), this.txtRemark.Text.Trim(), this.txtContractor.Text.Trim(), this.txtTag.Text.Trim());
                    }
                }
                else
                {
                    if (this.rbAll.Checked)
                    {
                        listDoc = this.documentService.SearchDocumentAll(categoryId, searchAll);
                    }
                    else if (this.rbDetail.Checked)
                    {
                        listDoc = this.documentService.SearchDocumentAll(categoryId, this.txtFileName.Text.Trim(), this.txtDocumentNumber.Text.Trim(), this.txtTitle.Text.Trim(), this.txtRevision.Text.Trim(), this.txtFromTo.Text.Trim(), this.txtDiscipline.Text.Trim(), this.txtDoctype.Text.Trim(), this.txtPlatform.Text.Trim(), this.txtRemark.Text.Trim(), this.txtContractor.Text.Trim(), this.txtTag.Text.Trim());
                    }
                }

                this.grdDocument.DataSource = listDoc;
            }
            else
            {
                this.grdDocument.DataSource = new List<Document>();
            }
        }

        /// <summary>
        /// The grd khach hang_ delete command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var docId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());

            var listRelateDoc = this.documentService.GetAllRelateDocument(docId);
            if (listRelateDoc != null)
            {
                foreach (var objDoc in listRelateDoc)
                {
                    objDoc.IsDelete = true;
                    objDoc.LastUpdatedBy = UserSession.Current.User.Id;
                    objDoc.LastUpdatedDate = DateTime.Now;
                    this.documentService.Update(objDoc);
                }
            }
        }

        /// <summary>
        /// The rad menu_ item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void radMenu_ItemClick(object sender, RadMenuEventArgs e)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The btn search_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.grdDocument.Rebind();
        }

        /// <summary>
        /// Load all combo data
        /// </summary>

        private void LoadListPanel()
        {
            var listId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("ListID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), listId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "LIST" });

                this.radPbList.DataSource = permissions;
                this.radPbList.DataFieldParentID = "ParentId";
                this.radPbList.DataFieldID = "Id";
                this.radPbList.DataValueField = "Id";
                this.radPbList.DataTextField = "MenuName";
                this.radPbList.DataBind();
                this.radPbList.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbList.Items[0].Items)
                {
                    item.ImageUrl = @"Images/listmenu.png";
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                }
            }
        }

        private void LoadSystemPanel()
        {
            var systemId = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("SystemID"));
            var permissions = this.permissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault(), systemId);
            if (permissions.Any())
            {
                foreach (var permission in permissions)
                {
                    permission.ParentId = -1;
                    permission.MenuName = permission.Menu.Description;
                }

                permissions.Insert(0, new Permission() { Id = -1, MenuName = "SYSTEM" });

                this.radPbSystem.DataSource = permissions;
                this.radPbSystem.DataFieldParentID = "ParentId";
                this.radPbSystem.DataFieldID = "Id";
                this.radPbSystem.DataValueField = "Id";
                this.radPbSystem.DataTextField = "MenuName";
                this.radPbSystem.DataBind();
                this.radPbSystem.Items[0].Expanded = true;

                foreach (RadPanelItem item in this.radPbSystem.Items[0].Items)
                {
                    item.ImageUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Icon;
                    item.NavigateUrl = permissions.FirstOrDefault(t => t.Id == Convert.ToInt32(item.Value)).Menu.Url;
                }
            }
        }

        //protected void rblSearch_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (rblSearch.SelectedValue == "1")
        //    {
        //        this.rblSearch.SelectedIndex = 0;
        //        pnSearchAll.Visible = true;
        //        pnSearchDetail.Visible = false;
        //    }
        //    else if (rblSearch.SelectedValue == "2")
        //    {
        //        this.rblSearch.SelectedIndex = 1;
        //        pnSearchDetail.Visible = true;
        //        pnSearchAll.Visible = false;
        //    }
        //}

        protected void rbAll_CheckedChanged(object sender, EventArgs e)
        {
            rbDetail.Checked = false;
            pnSearchAll.Visible = true;
            pnSearchDetail.Visible = false;
        }

        protected void rbDetail_CheckedChanged(object sender, EventArgs e)
        {
            rbAll.Checked = false;
            pnSearchDetail.Visible = true;
            pnSearchAll.Visible = false;
        }
    }
}

